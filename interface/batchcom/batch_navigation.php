<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%" height="22">
<tr bgcolor="#00ffff">
<?php if (acl_check('admin', 'batchcom')) { ?>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../batchcom/batchcom.php"
 onclick="top.restoreSession()"
 title="Batch Communication and Export"><?php echo xl('BatchCom','e');?></a>&nbsp;
</td>
<?php } ?>
<?php if (acl_check('admin', 'notification')) { ?>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../batchcom/smsnotification.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('SMS Notification','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../batchcom/emailnotification.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Email Notification','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../batchcom/settingsnotification.php"
 onclick="top.restoreSession()"
 title="SMS/Email Alert Settings"><?php echo xl('SMS/Email Alert Settings','e');?></a>&nbsp;
</td>
<?php } ?>

<td width="20%">&nbsp;</td>
</tr>
</table>