<?php

/**
 *
 * Getting Facility and Providers Listings : (Swati Jain)
 * 
 * */
$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("$srcdir/acl.inc");
require_once("../drugs/drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
// Get the providers list.
$facilList = $_REQUEST['facilityId'];
if (empty($facilList) || $facilList == 'null') {
    $facilList = 0;
}
if ($GLOBALS['restrict_user_facility']) {
    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) AS completename, u.nickname, 
                    GROUP_CONCAT(DISTINCT uf.facility_id SEPARATOR ',') AS `allFacilities` 
                    FROM users_facility AS uf 
                    INNER JOIN users AS u ON u.id = uf.table_id 
                    INNER JOIN gacl_aro AS ga ON ga.value=u.username 
                    INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id 
                    INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id 
                    WHERE 1=1  ";
    if (isset($facilList) && !empty($facilList)) {
        $sql.= "AND uf.facility_id IN (" . $facilList . ") ";
    } else {
        $fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = " . $_SESSION['authId'] . " AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
        $fRes = sqlStatement($fsql);
        $facilityIds = '';
        while ($fRow = sqlFetchArray($fRes)) {
            if (!empty($facilityIds)) {
                $facilityIds .= ', ' . $fRow['facility_id'];
            } else {
                $facilityIds = $fRow['facility_id'];
            }
        }
        $sql .= " AND uf.facility_id IN (" . $facilityIds . ") ";
    }
    $sql .= "AND gag.id IN (12, 13, 18, 24, 25) ";
    $sql .= " GROUP BY u.id HAVING `allFacilities` ='$facilList'";
} else {
    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename , u.nickname ";
    $sql .= "FROM users AS u  ";
    $sql .= " INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
    $sql .= " WHERE gag.id IN (12, 13, 18, 24, 25) GROUP BY u.id DESC";
   
}
$ures = sqlStatement($sql);
$num_rows = sqlNumRows($ures);
$returnData = "<select name='form_provider' style='width:100%' class='form-control' >";
$returnData.= "<option value='-1' selected='selected'>All Providers</option>";
if ($num_rows > 0) {
    while ($urow = sqlFetchArray($ures)) {
       
        $returnData.= "<option value='" . attr($urow['id']) . "'";
        $returnData.= ">" . text($urow['nickname']);
        $returnData.= "</option>";
    }
}
$returnData.= "</select>";
echo $returnData;
?>