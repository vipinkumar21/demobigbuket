<?php
/**
 *
 * Events Listing : (Swati Jain)
 *
 **/
require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("../drugs/drugs.inc.php");
//require_once("$srcdir/event_notification_product.php");
require_once($GLOBALS['srcdir'] . "/event_notification.php");
//ini_set("display_errors","1"); ERROR_REPORTING(E_ALL);
//echo"<pre>";print_r($_POST);
//print_r($_REQUEST);
if(!isset($_REQUEST['form_facility'])){
	$_REQUEST['form_facility']  = '';
}
if(!isset($_REQUEST['catid'])){
	$_REQUEST['catid']  = '';
}
if(!isset($_REQUEST['form_provider'])){
	$_REQUEST['form_provider']  = '';
}
if(!isset($_REQUEST['form_from_date'])){
	$_REQUEST['form_from_date']  = '';
}
if(!isset($_REQUEST['form_to_date'])){
	$_REQUEST['form_to_date']  = '';
}

$facility  = $_REQUEST['form_facility']; // filters
$cat = $_REQUEST['catid'];
$doctor = $_REQUEST['form_provider'];
$from_date = fixDate($_REQUEST['form_from_date'], '');
$to_date   = fixDate($_REQUEST['form_to_date'], '');
$querString = 'form_facility='.$facility.'&catid='.$cat.'&form_provider='.$doctor.'&form_from_date='.$from_date.'&form_to_date='.$to_date.'';
?>
<script language="JavaScript">

// Move to edit events
function dodclick(id) {
 dlgopen('events_edit.php?pc_eid=' + id, '_blank', 800, 575);
}

</script>
<?php 
// Fetch the Provider based on facility 
function restrict_user_facility($facility){
	$sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	$sql .= " WHERE ";
	if (isset($facility) && $facility !== ''){
		$sql .= "uf.facility_id = $facility ";
	}else {
		$fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = ".$_SESSION['authId']." AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
		$fRes = sqlStatement($fsql);
		$facilityIds = '';
		while ($fRow = sqlFetchArray($fRes)) {
			if(!empty($facilityIds)) {
				$facilityIds .= ', '.$fRow['facility_id'];
			}else {
				$facilityIds = $fRow['facility_id'];
			}
		}
		$sql .= "uf.facility_id IN (".$facilityIds.") ";
	}
	$sql .= "AND gag.id IN (12, 13, 18) ";
	return $sql .= " GROUP BY u.id DESC";	
}
// fetch all providers list
function non_restrict_user_facility($facility){
	$sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename ";
	$sql .= "FROM users AS u  ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	return $sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";	
}

function getClinicDetail($facility){
	if($facility != '-1') {
		$sql = "SELECT name FROM facility WHERE id =".$facility;
		$res = sqlStatement($sql);
		$urow = sqlFetchArray($res);
		return $urow['name'];
	}else {
		return 'All Clinic';
	}
}

function getEventUserDetail($userId){
	if($userId != '-1'){			// All Clinics case : -1
		$user_join = "SELECT u.id AS provider_id,CONCAT_WS(' ',u.fname, u.lname) AS provider_name ,u.nickname 
					  FROM users AS u
					  WHERE u.id = " .$userId;
		$result_set = sqlQuery($user_join);
		return $result_set;	
	}else{
		return array('provider_id' => '','provider_name' => 'All Doctors');
	}
}
// Check authorization.
$thisauth = acl_check('inventory', 'iar_adj');
if (!$thisauth) die(xlt('Not authorized'));

// fetch events listing data based on filters
$res = "SELECT ope.pc_eid, ope.pc_aid,ope.pc_duration,ope.pc_catid , ope.pc_recurrspec, opc.pc_catname
, ope.pc_facility,ope.pc_endDate,
ope.pc_eventDate , ope.pc_startTime,  ope.pc_endTime,ope.pc_title ,  ope.pc_delete,ope.pc_pid
FROM openemr_postcalendar_events AS ope
INNER JOIN `openemr_postcalendar_categories` AS opc ON ope.pc_catid = opc.pc_catid
WHERE ope.pc_operatory = 0 ";
//WHERE ope.pc_pid IS NULL ";
if (empty($to_date) && empty($from_date)){
	$to_date = date('Y-m-d');
	$from_date = date('Y-m-d');
	$res .= " AND ope.pc_eventDate >= '$from_date 00:00:00' AND "."ope.pc_eventDate <= '$to_date 23:59:59' ";
}else{
	
	if(!empty($from_date)){
		$res .= " AND ope.pc_eventDate >= '$from_date 00:00:00' ";
	}
	if(!empty($to_date)){
		$res .= " AND ope.pc_eventDate <= '$to_date 23:59:59' ";
	}
}

if(!empty($facility)){	// if facility exists
	$res .= " AND (ope.pc_facility = '".$facility."'";
        $res .= " OR ope.pc_facility = '-1') ";
}

/*if(!empty($doctor)){	// if provider exists
 	$res .= " AND u.id = '".$doctor."'";
 
}*/

if(!empty($cat)){	// if category exists
 	$res .= " AND ope.pc_catid = '".$cat."'";

}

$res .= " ORDER BY ope.pc_eventDate DESC, ope.pc_startTime DESC "; // Showing data including deleted events 
//echo $res;
$rest = sqlStatement($res);
			
$cnt = 0;
$show_arr = array();
while ($row = sqlFetchArray($rest)) {					// Array manipulation in order to fetch data from users table

	$show_arr[$cnt]['pc_eid'] = $row['pc_eid'];
	$show_arr[$cnt]['pc_aid'] = $row['pc_aid'];
	$show_arr[$cnt]['pc_duration'] = $row['pc_duration'];
	$show_arr[$cnt]['pc_catid'] = $row['pc_catid'];
	$show_arr[$cnt]['pc_recurrspec'] = $row['pc_recurrspec'];
	$show_arr[$cnt]['pc_catname'] = $row['pc_catname'];
	$show_arr[$cnt]['clinicName'] = getClinicDetail($row['pc_facility']);
	$show_arr[$cnt]['pc_endDate'] = $row['pc_endDate'];
	$show_arr[$cnt]['pc_eventDate'] = $row['pc_eventDate'];
	$show_arr[$cnt]['pc_startTime'] = $row['pc_startTime'];
	$show_arr[$cnt]['pc_endTime'] = $row['pc_endTime'];
	$show_arr[$cnt]['pc_title'] = $row['pc_title'];
	$show_arr[$cnt]['pc_delete'] = $row['pc_delete'];
	$show_arr[$cnt]['pc_pid'] = $row['pc_pid'];
	$result_set = getEventUserDetail($row['pc_aid']);
	$show_arr[$cnt]['provider_id'] = $result_set['provider_id'];
	$show_arr[$cnt]['provider_name'] = $result_set['nickname'];	
	$cnt++;
}

$rows = array();		
if(!empty($doctor) && $doctor>0){		// In case if doctor filter exists
	for($i=0;$i<count($show_arr);$i++){
		if($show_arr[$i]['pc_aid'] == $doctor){
			$rows[] = $show_arr[$i];
		}
	}
}else{
	$rows = $show_arr;
}

//$num_rows = sqlNumRows(sqlStatement($res));	// total no. of rows
$num_rows = count($show_arr);	// total no. of rows
$per_page = $GLOBALS['encounter_page_size'];			// Pagination variables processing
//$per_page = 2;
$page = $_GET["page"];

if(!$_GET["page"])
{
	$page=1;
}

$prev_page = $page-1;
$next_page = $page+1;

$page_start = (($per_page*$page)-$per_page);
if($num_rows<=$per_page)
{
	$num_pages =1;
}
else if(($num_rows % $per_page)==0)
{
	$num_pages =($num_rows/$per_page) ;
}
else
{
	$num_pages =($num_rows/$per_page)+1;
	$num_pages = (int)$num_pages;
}
//$res .= " LIMIT $page_start , $per_page";

//$res = sqlStatement($res);
?>

<?php 
// Deletion of Event (Soft Delete)
if (isset($_GET["mode"])) {
	if ($_GET["mode"] == "delete") {
		if(($_GET["pc_eid"])){
			$eID = $_GET["pc_eid"];                        
			sqlStatement("update openemr_postcalendar_events set pc_delete = '1' where pc_eid = '" . $_GET["pc_eid"] . "'");
			$_SESSION['alertmsg'] = 'Event deleted successfully.';
			sendEventProviderNotification($eID,'Delete');
			header('Location: list.php?'.$querString);
			exit;
		}else {
			$_SESSION['alertmsg'] = 'You should not able delete selected Event.';
			header('Location: list.php'.$querString);
			exit;
		}
			
	}
}

?>


<html>
<head>
<?php html_header_show();?>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="<?php echo $GLOBALS['webroot'].'/css/bootstrap.min.css';?>" type="text/css">
<link rel="stylesheet" href="<?php echo $GLOBALS['webroot'].'/css/bootstrap-theme.css';?>" type="text/css">
<link rel="stylesheet" href="<?php echo $GLOBALS['webroot'].'/css/bootstrap-theme.min.css';?>" type="text/css">

<title><?php xl('Event management','e'); ?></title>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/textformat.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/jquery.searchabledropdown-1.0.8.min.js"></script>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />


<script type="text/javascript">
var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

function dosort(orderby) {
    var f = document.forms[0];
    f.form_orderby.value = orderby;
    f.submit();
    return false;
}

function refreshme() {
    document.forms[0].submit();
}

 
$(document).ready(function(){
	
	//Get Provider based on Facility 
	$("#form_facility").change(function(){
		var facilityID = $("#form_facility").val();
		$.ajax({
			url:"get_providers_listing.php?facilityId="+facilityID,
			success:function(result){
				$("#form_provider").html(result);
			}
		});
	});
	//$("#form_provider").searchable();	// for editable dropdown menu for provider
});
</script>

<style type="text/css">
/* specifically include & exclude from printing */
@media print {
	#report_parameters {
		visibility: hidden;
		display: none;
	}
	#report_parameters_daterange {
		visibility: visible;
		display: inline;
	}
	#report_results table {
		margin-top: 0px;
	}
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
}
</style>
</head>

<body class="body_top" id="eventmanagementbody">

<!-- Required for the popup date selectors -->
<div id="overDiv"></div>


<div class="panel panel-warning">
	<div class="panel-heading">
		<div class="row">
        	<div class="col-xs-3 boldtxt">
				<?php  xl('Event Management','e'); ?>      
			</div>
	        <div class="col-xs-9 text-right">
				<a class="btn btn-default btn-sm" href="events_add.php"><?php xl('Add Event','e'); ?></a>
			</div>
		</div>
	</div>
</div>


<div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($form_to_date)); ?>
</div>

<form method='post' name='theform' id='theform' action='list.php' class='emrform'>
<div id="report_parameters" class='searchReport report-form' style="padding:0px;">
<input type='hidden' name='form_refresh' id='form_refresh' value='true'/>

<!-- the great row begins --> 
<div class="row">


		
		<!-- first column starts --> 
		<div class="form_left col-xs-4 topnopad botnopad">


					<div class="form-group">
						<div class="input select">
									<label class="date-wrp"><?php xl('Facility','e'); ?>:</label>
									<?php dropdown_facility(strip_escape_custom($facility), 'form_facility' ); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="date-wrp"><?php xl('Provider','e'); ?>:</label>
							<?php
							if(empty($facility)){
								$query = "SELECT id, lname, fname FROM users WHERE "."authorized = 1 $provider_facility_filter ORDER BY lname, fname"; 
								$ures = sqlStatement($query);
								echo "   <select name='form_provider' id='form_provider' class='form-control input-sm'>\n";
								echo "    <option value=''>-- " . xl('All') . " --\n";
								while ($urow = sqlFetchArray($ures)) {
									$provid = $urow['id'];
									$option_value = htmlspecialchars($provid, ENT_QUOTES);
									$option_content = htmlspecialchars($urow['fname'], ENT_NOQUOTES);
									echo "    <option value=\"$option_value\">$option_content</option>\n";


									/*$provid = $urow['id'];
									echo "    <option value='$provid'";
									if ($provid == $_REQUEST['form_provider']) echo " selected";
									echo ">" . $urow['lname'] . ", " . $urow['fname'] . "\n";*/
								}
								echo "   </select>\n";
							} else {
								
								if ($GLOBALS['restrict_user_facility']) {
									$sql = restrict_user_facility($facility);
									$ures = sqlStatement($sql);
								}else {
									$sql = non_restrict_user_facility($facility);
									$ures = sqlStatement($sql);
								}
								
								
								$returnData = '';
								// default to the currently logged-in user
								$defaultProvider = $_SESSION['authUserID'];
								$returnData.= "<option value='-1' selected='selected'>All Providers</option>";
								while ($urow = sqlFetchArray($ures)) {
								if($facility != $urow['id']){
									$returnData.= "<option value='" . attr($urow['id']) . "'";
									if ($urow['id'] == $doctor) $returnData.= " selected";
									$returnData.= ">" . text($urow['completename']);
									//$returnData.= ">" . text($urow['fname']);
									//if ($urow['lname']) $returnData.= " " . text($urow['lname']);
									$returnData.= "</option>";
									}
								}
								echo "   <select name='form_provider' id='form_provider' class='form-control input-sm'>\n";
								echo $returnData;
								echo "   </select>\n";
								
							}
							?>
						</div>



		</div>
		<!-- first column ends --> 

		<!-- second column starts --> 
		<div class="form_left col-xs-4 topnopad botnopad">

			<div class="form-group">
				<div class="input select">
					<label class="date-wrp"><?php xl('From','e'); ?>:</label>	
				<input type='text' name='form_from_date' id="form_from_date"
					size='10' value='<?php echo $form_from_date ?>'
					onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)'
					title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
					align='absbottom' width='24' height='22' id='img_from_date'
					border='0' alt='[?]' style='cursor: pointer'
					title='<?php xl('Click here to choose a date','e'); ?>'></div>
				</div>			
				<div class="form-group">
					<div class="input select">
						<label class="date-wrp"><?php xl('To','e'); ?>:</label>
						<input type='text' name='form_to_date' id="form_to_date"
					size='10' value='<?php echo $form_to_date ?>'
					onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)'
					title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
					align='absbottom' width='24' height='22' id='img_to_date'
					border='0' alt='[?]' style='cursor: pointer'
					title='<?php xl('Click here to choose a date','e'); ?>'>
				</div>
			</div>
		</div>
		<!-- second column ends --> 

		<!-- third column starts --> 
		<div class="form_left col-xs-4 topnopad botnopad">

						<div class="form-group">

							<label class="date-wrp"><?php xl('Category','e'); ?>:</label>
							<select name='catid' id='catid' class='form-control input-sm'>
						  		<?php 
						  		if(empty($row['inv_im_catId'])){
						  		?>
						  			<option value='0' selected="selected">All Category</option>
						  		<?php 
						  		}
						  		?>
						  		<?php 
						  		$catSql = "SELECT pc_catid, pc_catname, pc_recurrtype, pc_duration, pc_end_all_day 
											FROM openemr_postcalendar_categories WHERE pc_cattype=1 ORDER BY pc_catname";
						  		$cres = sqlStatement($catSql);
						  		if(sqlNumRows($cres)){
						  			while ($crow = sqlFetchArray($cres)) {?>
											<option value='<?php echo $crow['pc_catid']?>' <?php if($crow['pc_catid']==$cat){ echo 'selected="selected"';}?>><?php echo $crow['pc_catname']?></option>
										<?php
									}
								}
						  		?>
						  	</select>
							
						</div>


			

				<div class="form-group">
	                <a href='#' class='btn btn-warning btn-sm' onclick='$("#theform").submit();'>
						<span> <?php xl('Submit','e'); ?> </span>
					</a> 		
			   </div>

		</div>
		<!-- third column ends --> 

		</div>
		<!-- the graet row ends --> 

</div>
</div>
</div>
<!-- end of search parameters --> <?php

?>
<div id="report_results">
<table class="emrtable icontable">
<?php if($num_rows > 0){?>
<tr class='head'>
	
	<thead>
		<tr>	
			<th width="10%">				
				<?php echo xlt('Date'); ?>
			</th>
			<th width="5%">				
				<?php echo xlt('Duration'); ?>
			</th>
			<th width="10%">
				<?php echo xlt('Title'); ?>
			</th>
			<th width="10%">
				<?php echo xlt('Category'); ?>
			</th>
			<th width="10%">				
				<?php echo xlt('Start Time'); ?>
			</th>
			<th width="10%">				
				<?php echo xlt('End Time'); ?>
			</th>
			<th width="10%">				
				<?php echo xlt('Facility'); ?>
			</th>		
			<th width="10%">				
				<?php echo xlt('Provider Name'); ?>
			</th>
			<!--<th width="15%">				
				<?php echo xlt('Recurrence Type'); ?>
			</th> -->
			<th width="10%">
				<?php echo xlt('Action'); ?>
			</th>  
		</tr>
                			
	</thead>
	<?php } ?>
	<tbody>
		<!-- added for better print-ability -->
	<?php


//$dateMysqlFormat = getDateDisplayFormat(1);


$lastid = "";
$srCount = 1;

if($num_rows > 0){
	//while ($row = sqlFetchArray($res)) {
	for($i=0;$i<count($rows);$i++){ 
			
		$row = $rows[$i];
			++$encount;
	   $bgcolor = "#" . (($encount & 1) ? "ddddff" : "ffdddd");
	   $lastid = $row['pc_eid'];
	   if($row['pc_delete'] == 1){
	   	echo " <tr class='detail' bgcolor='$bgcolor' style='text-decoration: line-through;'>\n";
	   }else {
	   echo " <tr class='detail' bgcolor='$bgcolor'>\n";
	   } 
	   $eventStartDate = $row['pc_duration'] == 86400 ? date('Y-m-d', strtotime($row['pc_eventDate']))
	   : date('Y-m-d'.' g:i a', strtotime($row['pc_eventDate'].' '.$row['pc_startTime'])) ;
	   
			   echo "  <td>" . $eventStartDate . "</td>\n";
			   $eventDuration = $row['pc_duration'] == 86400 ? 'All day event' : round($row['pc_duration'] / 60 ) . ' minute';
			   echo "  <td>" . $eventDuration . "</td>\n";
			   echo "  <td>" . text($row['pc_title']) . "</td>\n";
			   echo "  <td>" . $row['pc_catname'] . "</td>\n";
			   echo "  <td>" . $row['pc_startTime'] . "</td>\n";
			   echo "  <td>" . $row['pc_endTime'] . "</td>\n";
			   echo "  <td>" . $row['clinicName'] . "</td>\n";
			   echo "  <td>" . $row['provider_name'] . "</td>\n";
			   
			   $recurrspec = unserialize($row['pc_recurrspec']);//echo"<pre>";print_r($recurrspec);//die;
			   $eventRepeat = ' ';
			   //Event Repet Frequency
			   $eventRepeatFreq = $recurrspec['event_repeat_freq'];
			   foreach (array('1' => 'every', '2' => '2nd', '3' => '3rd', '4' => '4th', '5' => '5th', '6' => '6th') as $key => $value){
			   	if ($key == $eventRepeatFreq) $eventRepeat = $value;
			   }
			   //Event Repet Frequency Type
			   $repeatFreqType = $recurrspec['event_repeat_freq_type'];
			   foreach (array('0' => 'day', '4' => 'workday', '1' => 'week', '2' => 'month', '3' => 'year') as $key => $value){
			   	if ($key == $repeatFreqType && !is_null($repeatFreqType))  $eventRepeat .= ' ' . $value;
			   }
			   
			   	
			   $eventEndDate = $row['pc_endDate'] == '0000-00-00' ? ''
			   		: $eventRepeat .' / '. date('Y-m-d',strtotime($row['pc_endDate']));
			   
			   
			   
			   //echo "  <td>" . $eventEndDate . "</td>\n";
	   
	   echo "  <td>";
          $cur_datetime = date('Y-m-d H:i:s');
          $event_time =$row['pc_eventDate'].' '.$row['pc_startTime'] ;
 	   if($row['pc_delete'] == 0 && $cur_datetime <= $event_time){    //if event time get passed then didn't display edit/delete
 	   		 echo "<a class='iconanchor' href='events_edit.php?pc_eid=".attr($lastid)."&".$querString."'><span class='glyphicon glyphicon-pencil'></span></a> &nbsp; ";
			 echo "<a class='iconanchor' onclick='return jConfirm(\"Are you sure to delete?\",\"Confirm\",function(result) { if(result){location.href=\"list.php?mode=delete&pc_eid=".attr($lastid)."&".$querString."\";};});''><span class='glyphicon glyphicon-trash'></span></a> &nbsp; "; 
    		
 	   }
		echo "</td>\n";	 ?>
			</tr>
			<?php 
			$srCount++;
		}
	}else{?>
	<tr>
<td colspan="5" style="text-align:center; font-size:10pt;">No Events Found</td>
</tr>
	<?php }
	?>
<div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e' ); ?>
</div>
</form>

<?php // Pagination Displaying Section?>
<?php /* if($num_rows > 0){ ?>
<table><tbody><tr><td class="text" >
<?php if($num_rows > 0){?>Total <?php echo $num_rows;?> Records : Page :<?php } ?>
<?php
if($prev_page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?page=$prev_page&form_facility=$_REQUEST[form_facility]&catid=$_REQUEST[catid]&form_from_date=$_REQUEST[form_from_date]&form_to_date=$_REQUEST[form_to_date]&form_provider=$_REQUEST[form_provider]'><< Back</a> ";
}
if($num_rows > 0){
	for($i=1; $i<=$num_pages; $i++){
		if($i != $page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?page=$i&form_facility=$_REQUEST[form_facility]&catid=$_REQUEST[catid]&form_from_date=$_REQUEST[form_from_date]&form_to_date=$_REQUEST[form_to_date]&form_provider=$_REQUEST[form_provider]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
}
if($page!=$num_pages)
{
echo " <a href ='$_SERVER[SCRIPT_NAME]?page=$next_page&form_facility=$_REQUEST[form_facility]&catid=$_REQUEST[catid]&form_from_date=$_REQUEST[form_from_date]&form_to_date=$_REQUEST[form_to_date]&form_provider=$_REQUEST[form_provider]'>Next>></a> ";
}?>

</td></tr></tbody></table>
<?php }*/ ?>
<!-- stuff for the popup calendar -->
<style type="text/css">
    @import url(../../library/dynarch_calendar.css);
</style>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript"
	src="../../library/dynarch_calendar_setup.js"></script>
<script type="text/javascript">
	Calendar.setup({
		inputField:"form_from_date"
		,ifFormat:"%Y-%m-%d"
		,button:"img_from_date"
		,onUpdate: function() {
			var   toDate = document.getElementById('form_to_date').value,
		      fromDate = document.getElementById('form_from_date').value;

			if(toDate){
			   var   toDate = new Date(toDate),
			         fromDate = new Date(fromDate);

			   if(fromDate > toDate){
			      //alert('From date cannot be later than To date.');
			       jAlert ('From date cannot be later than To date', 'Alert');
			      document.getElementById('form_from_date').value = '';
			   }
			}
		}
	});
	Calendar.setup({
		inputField:"form_to_date"
		,ifFormat:"%Y-%m-%d"
		,button:"img_to_date"
		,onUpdate: function() {
			var   toDate = document.getElementById('form_to_date').value,
		      fromDate = document.getElementById('form_from_date').value;

			if(fromDate){
			   var   toDate = new Date(toDate),
			         fromDate = new Date(fromDate);

			   if(fromDate > toDate){
			      //alert('From date cannot be later than To date.');
			       jAlert ('From date cannot be later than To date', 'Alert');
			      document.getElementById('form_to_date').value = '';
			   }
			}
		}
	});
</script>

<?php // } ?> 
</body>
</html>
