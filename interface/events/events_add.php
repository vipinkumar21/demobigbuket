<?php
/**
 *
 * Events Add : (Swati Jain)
 *
 * */
// Report simple running errors
error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set("display_errors","2"); ERROR_REPORTING(E_ALL);
$fake_register_globals = false;
$sanitize_all_escapes = true;

require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/forms.inc");
require_once("$srcdir/calendar.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/encounter_provider_events.inc.php");
require_once("$srcdir/acl.inc");
require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");
require_once ($GLOBALS['srcdir'] . "/maviq_phone_api.php");
require_once($GLOBALS['srcdir'] . "/event_notification.php");

$_GET['prov'] = true;

$my_permission = acl_check('patients', 'appt');
if ($my_permission !== 'write' && $my_permission !== 'wsome')
    die(xl('Access not allowed'));
if (!isset($_GET['catid'])) {
    $_GET['catid'] = 0;
}
if (!isset($_GET['eid'])) {
    $_GET['eid'] = 0;
}
if (!isset($_GET['userid'])) {
    $_GET['userid'] = 0;
}
if (!isset($_GET['date'])) {
    $_GET['date'] = '';
}
$eid = $_GET['eid'];         // only for existing events
$date = $_GET['date'];        // this and below only for new events
$userid = $_GET['userid'];
$default_catid = $_GET['catid'] ? $_GET['catid'] : '5';
//
if ($date)
    $date = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6);
else
    $date = date("Y-m-d");
//
$starttimem = '00';
if (isset($_GET['starttimem']))
    $starttimem = substr('00' . $_GET['starttimem'], -2);
//
if (isset($_GET['starttimeh'])) {
    $starttimeh = $_GET['starttimeh'];
    if (isset($_GET['startampm'])) {
        if ($_GET['startampm'] == '2' && $starttimeh < 12)
            $starttimeh += 12;
    }
} else {
    $starttimeh = date("G", time());
}
$startampm = '';

$info_msg = "";
?>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.js"></script>

<?php

function InsertEventFull() {
    global $new_multiple_value, $provider, $event_date, $duration, $recurrspec, $starttime, $endtime, $locationspec;

    $args = $_POST;
    $args['new_multiple_value'] = "";
    $args['event_date'] = $event_date;
    $args['duration'] = $duration;
    $args['recurrspec'] = $recurrspec;
    $args['starttime'] = $starttime;
    $args['endtime'] = $endtime;
    $args['locationspec'] = $locationspec;
    $args['billing_facility'] = $_POST['facility'];

    /*     * ********************************************************************************************* */

    $whereClause = '';
    if ($_POST['form_all_clinic'] == 1 || !empty($_POST['facility'])) {
    if ($_POST['form_all_clinic'] == 1) {
        $clinics = '-1';
            $whereClause.= " AND pc_facility = '-1' ";
    } else {
        $clinics[] = $_POST['facility'];
            $facilities = implode(",", $clinics[0]);
            $whereClause.= " AND pc_facility IN ($facilities) ";
    }
    if ($_POST['form_provider']) {
        $doctorId = $_POST['form_provider'];
    } else {
        $doctorId = '-1';
    }
    if ($_POST['form_enddate']) {
        $enddt = $_POST['form_enddate'];
    } else {
        $enddt = '0000-00-00';
    }
    //Add or Edit Event Both
    if ($doctorId != '-1') {
        $whereClause .= " AND ( pc_aid = '" . $doctorId . "' OR pc_aid = '-1' ) ";
    }
    $checkQuery = "SELECT pc_eid, pc_facility, pc_aid, pc_eventDate, pc_endDate, pc_startTime, pc_endTime, pc_delete, pc_recurrspec 
                           FROM openemr_postcalendar_events
                    WHERE 1=1  AND (( pc_eventDate = '" . $_POST['form_date'] . "' AND pc_endDate = '0000-00-00' )
                               OR ( ('" . $_POST['form_date'] . "' >= pc_eventDate AND pc_endDate > '" . $_POST['form_date'] . "') "
                . "OR ( '" . $enddt . "' > pc_eventDate AND pc_endDate >= '" . $enddt . "' ) )
                    OR ( pc_eventDate BETWEEN '" . $_POST['form_date'] . "' AND '" . $enddt . "' ))
                    AND ( ('" . $starttime . "' >= pc_startTime AND pc_endTime >'" . $starttime . "') "
                . "OR ( '" . $endtime . "' > pc_startTime AND pc_endTime >='" . $endtime . "' ) ) "
                . "AND pc_delete = 0 ";
    $checkQuery .= $whereClause;
    $checkResult = sqlStatement($checkQuery);
    $checkNumRows = sqlNumRows($checkResult);
    // Start checking repeatition of event and appointment
    $checkFlag = false;
    if ($checkNumRows) {
        while ($res = sqlFetchArray($checkResult)) {
            $recurrspec = unserialize($res['pc_recurrspec']);
            //print_r($recurrspec);
            //Event Repet Frequency Type
            $repeatFreqType = $recurrspec['event_repeat_freq_type'];
            foreach (array('0' => 'day', '1' => 'week', '2' => 'month') as $key => $value) {
                if ($key == $repeatFreqType)
                    $repeattype = $value;
            }
            $repeatfreq = $recurrspec['event_repeat_freq'];
            if ($repeatfreq) {
                $thistime = $firstTime = strtotime($res['pc_eventDate'] . " 00:00:00");
                $currentDateTime = strtotime($_POST['form_date'] . " 00:00:00");
                $endtime = strtotime($res['pc_endDate'] . " 00:00:00") + (24 * 60 * 60);
                $repeatix = 0;
                $recurringEvent = array();
                while ($thistime < $endtime) {
                    $adate = getdate($thistime);
                    $thisymd = sprintf('%04d%02d%02d', $adate['year'], $adate['mon'], $adate['mday']);

                    if ($repeattype == 'day') {  //day
                        $adate['mday'] += 1;
                    } else if ($repeattype == 'week') { //week
                        $adate['mday'] += 7;
                    } else if ($repeattype == 'month') { //month
                        $adate['mon'] += 1;
                    }
                    $thistime = mktime(0, 0, 0, $adate['mon'], $adate['mday'], $adate['year']);
                    $recurringEvent[] = $thistime;
                }
                $recurringEvent[] = $firstTime; //for Removing the condation $firstTime == $currentDateTime section adding this section of code
                //print_r($recurringEvent);
                if (in_array($currentDateTime, $recurringEvent)) {
                    $checkFlag = true;
                    break;
                }
            } else {
                $checkFlag = true;
                break;
            }
        }
        //return $checkFlag;
    }
    // End checking repeatition of event and appointment
    if ($checkFlag) {
        $msg = "Event already created.Please choose another time slot..";
        echo "<html>\n<body>\n<script language='JavaScript'>\n";
        if ($msg)
            echo " alert('" . addslashes($msg) . "');\n";
        echo " window.close();\n";
        echo "window.location.href = 'events_add.php'";
        echo "</script>\n</body>\n</html>\n";
        exit();
    }else {
        $cur_date = date('Y-m-d');
        $cur_time = date('H:i:s');
        if ($args['form_date'] < $cur_date || ($args['form_date'] <= $cur_date && $args['starttime'] < $cur_time) || ($args['form_allday'] == 1 && $args['form_date'] == $cur_date)) { // Check for passed time/date
            $msg = "Event can't be created in past date/time. Please choose another date/time.";
            echo "<html>\n<body>\n<script language='JavaScript'>\n";
            if ($msg)
                echo " alert('" . addslashes($msg) . "');\n";
            echo " window.close();\n";
            echo "window.location.href = 'events_add.php'";
            echo "</script>\n</body>\n</html>\n";
            exit();
        }else {

            if ($_POST['form_all_clinic'] != '1') {
                    $hol_sql = "SELECT fhol_date FROM facility_holidays WHERE fhol_facility_id IN (" . $facilities . ") AND fhol_is_deleted = 0";
                $holResult = sqlStatement($hol_sql);
                while ($row = sqlFetchArray($holResult)) {
                    $hol_arr[] = $row['fhol_date'];
                }

                if (in_array($args['form_date'], $hol_arr)) {
                    $msg = "Event should not be created for Holiday.Please choose another time.";
                    echo "<html>\n<body>\n<script language='JavaScript'>\n";
                    if ($msg)
                        echo " alert('" . addslashes($msg) . "');\n";
                    echo " window.close();\n";
                    echo "window.location.href = 'events_add.php'";
                    echo "</script>\n</body>\n</html>\n";
                    exit();
                }else {
                    $msg = '';
                    $facilityList = $args['facility'];
                    unset($args['facility']);
                    mysql_query("START TRANSACTION");
                    try {
                        foreach ($facilityList as $row) {
                            $args['facility'] = $row;
                            $event_id = InsertEvent($args);
                            sendEventProviderNotification($event_id, 'Alert');
                        }
                        mysql_query("Commit");
                    } catch (Exception $ex) {
                        echo $ex->getMessage();
                        mysql_query("Rollback");
                    }
                }
            } else {
                $msg = '';
                $event_id = InsertEvent($args);
                sendEventProviderNotification($event_id, 'Alert');
            }
        }
    }
    } 
 else {
        $msg = "Please select facility or All Clinics option..";
            echo "<html>\n<body>\n<script language='JavaScript'>\n";
            if ($msg)
                echo " alert('" . addslashes($msg) . "');\n";
            echo "window.location.href = 'events_add.php'";
            echo "</script>\n</body>\n</html>\n";
            exit();
    }
    /*     * ********************************************************************************************* */
}

if ($_POST['form_action'] == "save") {
    // the starting date of the event, pay attention with this value
    // when editing recurring events -- JRM Oct-08
    $event_date = fixDate($_POST['form_date']);

    // Compute start and end time strings to be saved.
    if ($_POST['form_allday']) {
        $tmph = 0;
        $tmpm = 0;
        $duration = 24 * 60;
    } else {
        $tmph = $_POST['form_hour'] + 0;
        $tmpm = $_POST['form_minute'] + 0;
        if ($_POST['form_ampm'] == '2' && $tmph < 12)
            $tmph += 12;
        $duration = $_POST['form_duration'];
    }

    $starttime = "$tmph:$tmpm:00";
    //
    $tmpm += $duration;
    while ($tmpm >= 60) {
        $tmpm -= 60;
        ++$tmph;
    }
    $endtime = "$tmph:$tmpm:00";

    // Useless garbage that we must save.
    $locationspecs = array("event_location" => "",
        "event_street1" => "",
        "event_street2" => "",
        "event_city" => "",
        "event_state" => "",
        "event_postal" => ""
    );
    $locationspec = serialize($locationspecs);
    if ($_POST['form_repeat']) {
        // capture the recurring specifications
        $recurrspec = array("event_repeat_freq" => 1, // for every day
            //"event_repeat_freq_type" => $_POST['form_repeat_type'],
            "event_repeat_on_num" => "1",
            "event_repeat_on_day" => "0",
            "event_repeat_on_freq" => "0",
                //"exdate" => $_POST['form_repeat_exdate']
        );
    } else {
        // no recurr specs, this is used for adding a new non-recurring event
        $recurrspec = array("event_repeat_freq" => "",
            "event_repeat_freq_type" => "",
            "event_repeat_on_num" => "1",
            "event_repeat_on_day" => "0",
            "event_repeat_on_freq" => "0",
            "exdate" => ""
        );
    }
    InsertEventFull();
    ?>
    <script type="text/javascript">window.location.href = 'list.php';</script>
    <?php
}

if ($_POST['form_action'] != "") {
    // Close this window and refresh the calendar display.
    echo "<html>\n<body>\n<script language='JavaScript'>\n";
    if ($info_msg)
        echo " alert('" . addslashes($info_msg) . "');\n";
    echo " if (opener && !opener.closed && opener.refreshme) opener.refreshme();\n";
    echo " window.close();\n";
    echo "</script>\n</body>\n</html>\n";
    exit();
}

$repeats = 0; // if the event repeats
$repeattype = '0';
$repeatfreq = '0';
$patientid = '';
if ($_REQUEST['patientid'])
    $patientid = $_REQUEST['patientid'];
$patientname = xl('Click to select');
$patienttitle = "";
$hometext = "";
$row = array();
$informant = "";

// a NEW event
$eventstartdate = $date; // for repeating event stuff - JRM Oct-08
//Set default facility for a new event based on the given 'userid'
if ($userid) {

    if ($_SESSION['pc_facility']) {
        $pref_facility = sqlFetchArray(sqlStatement("
                SELECT f.id as facility_id,
                f.name as facility
                FROM facility f
                WHERE f.id = ?
              ", array($_SESSION['pc_facility'])
        ));
    } else {
        $pref_facility = sqlFetchArray(sqlStatement("
            SELECT u.facility_id, 
              f.name as facility 
            FROM users u
            LEFT JOIN facility f on (u.facility_id = f.id)
            WHERE u.id = ?
            ", array($userid)));
    }
        /*         * ********************************************************* */
    $e2f = $pref_facility['facility_id'];
    $e2f_name = $pref_facility['facility'];
}
//END of CHEMED -----------------------
// Get event categories.
$cres = sqlStatement("SELECT pc_catid, pc_catname, pc_recurrtype, pc_duration, pc_end_all_day " .
        "FROM openemr_postcalendar_categories ORDER BY pc_catname");

// Fix up the time format for AM/PM.
$startampm = '1';

if ($starttimeh >= 12) { // p.m. starts at noon and not 12:01
    $startampm = '2';
    if ($starttimeh > 12)
        $starttimeh -= 12;
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php xlt('Add New') ?> <?php echo xlt('Event'); ?></title>
        <link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>
        <link rel="stylesheet" href="<?php echo $GLOBALS['webroot'] . '/css/bootstrap.min.css'; ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['webroot'] . '/css/bootstrap-theme.css'; ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['webroot'] . '/css/bootstrap-theme.min.css'; ?>" type="text/css">

        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
        <style>
            td { font-size:0.8em; }
        </style>

        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/topdialog.js"></script>
        <script type="text/javascript" src="../../library/dialog.js"></script>
        <script type="text/javascript" src="../../library/textformat.js"></script>
        <!-- <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
        <?php //include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php");   ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script> -->
        <script language="JavaScript">

    var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

    var durations = new Array();
    // var rectypes  = new Array();
<?php
// Read the event categories, generate their options list, and get
// the default event duration from them if this is a new event.
$cattype = 0;
if ($_GET['prov'] == true) {
    $cattype = 1;
}
$cres = sqlStatement("SELECT pc_catid, pc_catname, pc_recurrtype, pc_duration, pc_end_all_day " .
        "FROM openemr_postcalendar_categories WHERE pc_cattype=? ORDER BY pc_catname", array($cattype));
$catoptions = "";
$prefcat_options = "    <option value='0'>-- " . xlt("None") . " --</option>\n";
$thisduration = 0;

while ($crow = sqlFetchArray($cres)) {
    $duration = round($crow['pc_duration'] / 60);
    if ($crow['pc_end_all_day'])
        $duration = 1440;
    echo " durations[" . attr($crow['pc_catid']) . "] = " . attr($duration) . "\n";

    $catoptions .= "    <option value='" . attr($crow['pc_catid']) . "'";

    if ($crow['pc_catid'] == $default_catid) {
        $catoptions .= " selected";
        $thisduration = $duration;
    }

    $catoptions .= ">" . text(xl_appt_category($crow['pc_catname'])) . "</option>\n";

    // This section is to build the list of preferred categories:
    if ($duration) {
        $prefcat_options .= "    <option value='" . attr($crow['pc_catid']) . "'";

        $prefcat_options .= ">" . text(xl_appt_category($crow['pc_catname'])) . "</option>\n";
    }
}
?>

<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

    // This is for callback by the find-patient popup.
    function setpatient(pid, lname, fname, dob) {
        var f = document.forms[0];
        f.form_patient.value = lname + ', ' + fname;
        f.form_pid.value = pid;
        dobstyle = (dob == '' || dob.substr(5, 10) == '00-00') ? '' : 'none';
        document.getElementById('dob_row').style.display = dobstyle;
    }

    // This invokes the find-patient popup.
    function sel_patient() {
        dlgopen('find_patient_popup.php', '_blank', 500, 400);
    }

    // Do whatever is needed when a new event category is selected.
    // For now this means changing the event title and duration.
    function set_display() {
        var f = document.forms[0];
        var s = f.form_category;
        if (s.selectedIndex >= 0) {
            var catid = s.options[s.selectedIndex].value;
            var style_apptstatus = document.getElementById('title_apptstatus').style;
            var style_prefcat = document.getElementById('title_prefcat').style;
            if (catid == '2') { // In Office
                style_apptstatus.display = 'none';
                style_prefcat.display = '';
                f.form_apptstatus.style.display = 'none';

            } else {
                style_prefcat.display = 'none';
                style_apptstatus.display = '';
                f.form_prefcat.style.display = 'none';
                f.form_apptstatus.style.display = '';
            }
        }
    }

    // Do whatever is needed when a new event category is selected.
    // For now this means changing the event title and duration.
    function set_category() {
        var f = document.forms[0];
        var s = f.form_category;
        if (s.selectedIndex >= 0) {
            var catid = s.options[s.selectedIndex].value;
            f.form_title.value = s.options[s.selectedIndex].text;
            f.form_duration.value = durations[catid];
            set_display();
        }
    }

    // Modify some visual attributes when the all-day or timed-event
    // radio buttons are clicked.
    function set_allday() {
        var f = document.forms[0];
        var color1 = '#777777';
        var color2 = '#777777';
        var disabled2 = true;
        if (document.getElementById('rballday1').checked) {
            color1 = '#000000';
        }
        if (document.getElementById('rballday2').checked) {
            color2 = '#000000';
            disabled2 = false;
        }
        document.getElementById('tdallday1').style.color = color1;
        document.getElementById('tdallday2').style.color = color2;
        document.getElementById('tdallday3').style.color = color2;
        document.getElementById('tdallday4').style.color = color2;
        document.getElementById('tdallday5').style.color = color2;
        f.form_hour.disabled = disabled2;
        f.form_minute.disabled = disabled2;
        f.form_ampm.disabled = disabled2;
        f.form_duration.disabled = disabled2;
    }

    // Modify some visual attributes when the Repeat checkbox is clicked.
    /*
     function set_repeat() {
     var f = document.forms[0];
     var isdisabled = true;
     var mycolor = '#777777';
     var myvisibility = 'hidden';
     if (f.form_repeat.checked) {
     isdisabled = false;
     mycolor = '#000000';
     myvisibility = 'visible';
     }
     f.form_repeat_type.disabled = isdisabled;
     
     f.form_enddate.disabled = isdisabled;
     document.getElementById('tdrepeat1').style.color = mycolor;
     document.getElementById('tdrepeat2').style.color = mycolor;
     
     document.getElementById('form_repeat_type').style.color = mycolor;
     document.getElementById('img_enddate').style.visibility = myvisibility;
     }
     */
    //Modify some visual attributes when the All Clinics checkbox is clicked.
    function set_all_clinics() {
        var f = document.theform;

        if (f.form_all_clinic.checked) {
            isdisabled = true;
            mycolor = '#777777';
        } else {
            isdisabled = false;
            mycolor = '#000000';
        }
        f.facility.disabled = isdisabled;       // Enabling / Disabling doctors and facilities
        f.form_provider.disabled = isdisabled;
        document.getElementById('id_prov').style.color = mycolor;
        document.getElementById('id_fac').style.color = mycolor;
    }

    // This is for callback by the find-available popup.
    function setappt(year, mon, mday, hours, minutes) {
        var f = document.forms[0];
        f.form_date.value = '' + year + '-' +
                ('' + (mon + 100)).substring(1) + '-' +
                ('' + (mday + 100)).substring(1);
        f.form_ampm.selectedIndex = (hours >= 12) ? 1 : 0;
        f.form_hour.value = (hours > 12) ? hours - 12 : hours;
        f.form_minute.value = ('' + (minutes + 100)).substring(1);
    }

    // Invoke the find-available popup.
    function find_available(extra) {
        top.restoreSession();
        // (CHEMED) Conditional value selection, because there is no <select> element
        // when making an appointment for a specific provider
        var s = document.forms[0].form_provider;
        var f = document.forms[0].facility;
<?php if ($userid != 0) { ?>
            s = document.forms[0].form_provider.value;
            f = document.forms[0].facility.value;
<?php } else { ?>
            s = document.forms[0].form_provider.options[s.selectedIndex].value;
            f = !!document.forms[0].facility.options[f.selectedIndex]? document.forms[0].facility.options[f.selectedIndex].value: "";
<?php } ?>
        var c = document.forms[0].form_category;
        var formDate = document.forms[0].form_date;
        if (document.forms[0].form_all_clinic.checked == false) {
            dlgopen('<?php echo $GLOBALS['web_root']; ?>/interface/events/find_appt_popup.php' +
                    '?providerid=' + s +
                    '&catid=' + c.options[c.selectedIndex].value +
                    '&facility=' + f +
                    '&startdate=' + formDate.value +
                    '&evdur=' + document.forms[0].form_duration.value +
                    '&eid=<?php echo 0 + $eid; ?>' +
                    extra,
                    '_blank', 500, 400);
        } else {
            top.restoreSession();
            document.forms[0].submit();
        }
        //END (CHEMED) modifications
    }

        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    </head>

    <body class="body_top" onunload='imclosing()'>

        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3 boldtxt">
                        Add event   
                    </div>

                    <a href="list.php" class="iframe btn btn-default btn-sm " style="position: absolute; right: 20px; top: 17px;"><span>
<?php xl('Event List', 'e'); ?></span>
                    </a>

                </div>
            </div>
            <div class="panel-body topnopad botnopad">




                <form method='post' name='theform' class="add-event botnomrg botnomrg" id='theform' action='events_add.php?eid=<?php echo attr($eid) ?>' />
                <!-- ViSolve : Requirement - Redirect to Create New Patient Page -->
                <input type='hidden' size='2' name='resname' value='empty' />
                <input type='hidden' name='prov' value='true' />
                <?php
                if ($_POST["resname"] == "noresult") {
                    echo '
<script language="Javascript">
            // refresh and redirect the parent window
            if (!opener.closed && opener.refreshme) opener.refreshme();
            top.restoreSession();
            opener.document.location="../new/new.php";
            // Close the window
            window.close();
</script>';
                }
                $classprov = 'current';
                $classpati = '';
                ?>
                <!-- ViSolve : Requirement - Redirect to Create New Patient Page -->
                <input type="hidden" name="form_action" id="form_action" value="">
                <input type="hidden" name="recurr_affect" id="recurr_affect" value="">
                <!-- used for recurring events -->
                <input type="hidden" name="selected_date" id="selected_date" value="<?php echo attr($date); ?>">
                <input type="hidden" name="event_start_date" id="event_start_date" value="<?php echo attr($eventstartdate); ?>">

                <table border='0' width="100%" >
                    <?php
                    $provider_class = '';
                    $normal = '';
                    if ($_GET['prov'] == true) {
                        $provider_class = "class='current'";
                    } else {
                        $normal = "class='current'";
                    }
                    ?>
                    <tr><th><ul class="tabNav">
                        <?php
                        if (!isset($_REQUEST['eid'])) {
                            $_REQUEST['eid'] = '0';
                        }
                        if (!isset($_REQUEST['startampm'])) {
                            $_REQUEST['startampm'] = '';
                        }
                        if (!isset($_REQUEST['starttimeh'])) {
                            $_REQUEST['starttimeh'] = '';
                        }
                        if (!isset($_REQUEST['userid'])) {
                            $_REQUEST['userid'] = '';
                        }
                        if (!isset($_REQUEST['starttimem'])) {
                            $_REQUEST['starttimem'] = '';
                        }
                        if (!isset($_REQUEST['date'])) {
                            $_REQUEST['date'] = '';
                        }
                        if (!isset($_REQUEST['catid'])) {
                            $_REQUEST['catid'] = '0';
                        }
                        $eid = $_REQUEST["eid"];
                        $startm = $_REQUEST["startampm"];
                        $starth = $_REQUEST["starttimeh"];
                        $uid = $_REQUEST["userid"];
                        $starttm = $_REQUEST["starttimem"];
                        $dt = $_REQUEST["date"];
                        $cid = $_REQUEST["catid"];
                        ?>
                                    <!--  <li <?php echo $normal; ?>>
                                     <a href='events_add.php?eid=<?php echo attr($eid); ?>&startampm=<?php echo attr($startm); ?>&starttimeh=<?php echo attr($starth); ?>&userid=<?php echo attr($uid); ?>&starttimem=<?php echo attr($starttm); ?>&date=<?php echo attr($dt); ?>&catid=<?php echo attr($cid); ?>'>
                        <?php echo xlt('Patient'); ?></a>
                                     </li> -->
                        <li <?php echo $provider_class; ?>>
                        <!--  <a href='events_add.php?prov=true&eid=<?php echo attr($eid); ?>&startampm=<?php echo attr($startm); ?>&starttimeh=<?php echo attr($starth); ?>&userid=<?php echo attr($uid); ?>&starttimem=<?php echo attr($starttm); ?>&date=<?php echo attr($dt); ?>&catid=<?php echo attr($cid); ?>'>-->
                            <?php //echo xlt('Provider'); ?></a>
                            <?php echo $msg; ?></a>
                        </li>
                    </ul>
                    </th></tr>
                    <tr><td colspan='10'>
                            <table border='0' width='100%'>

                                <tr>
                                    <td width='1%' nowrap>
                                        <b><?php echo ($GLOBALS['athletic_team'] ? xlt('Team/Squad') : xlt('Category')); ?>:</b>
                                    </td>
                                    <td nowrap>
                                        <select name='form_category' onchange='set_category()' class="form-control">
                                            <?php echo $catoptions ?>
                                        </select>
                                    </td>
                                    <td width='1%' nowrap>
                                        &nbsp;&nbsp;
                                        <input type='radio' name='form_allday' onclick='set_allday()' value='1' id='rballday1'
                                               <?php if ($thisduration == 1440) echo "checked " ?>/>
                                    </td>
                                    <td colspan='2' nowrap id='tdallday1'>
                                        <?php echo xlt('All day event'); ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td nowrap>
                                        <b><?php echo xlt('Date'); ?>:</b>
                                    </td>
                                    <td nowrap>
                                        <input type='text' size='10' name='form_date' id='form_date' style="width:175px;"
                                               value='<?php echo attr($date) ?>'
                                               title='<?php echo xla('yyyy-mm-dd event date or starting date'); ?>'
                                               onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'  class="form-control" />
                                        <img src='<?php echo $GLOBALS['web_root']; ?>/interface/pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                             id='img_date' border='0' alt='[?]' style='cursor:pointer;cursor:hand'
                                             title='<?php echo xla('Click here to choose a date'); ?>'>
                                    </td>
                                    <td nowrap>
                                        &nbsp;&nbsp;
                                        <input type='radio' name='form_allday'  onclick='set_allday()' value='0' id='rballday2' <?php if ($thisduration != 1440) echo "checked " ?>/>
                                    </td>
                                    <td width='1%' nowrap id='tdallday2'>
                                        <?php echo xlt('Time'); ?>
                                    </td>
                                    <td width='1%' nowrap id='tdallday3'>
                                        <input type='text' size='2' name='form_hour' value='<?php echo attr($starttimeh) ?>'
                                               title='<?php echo xla('Event start time'); ?>' onkeypress="return isNumberKey(event)"/> :
                                        <input type='text' size='2' name='form_minute' value='<?php echo attr($starttimem) ?>'
                                               title='<?php echo xla('Event start time'); ?>' onkeypress="return isNumberKey(event)" />&nbsp;
                                        <select name='form_ampm' title='<?php echo xla("Note: 12:00 noon is PM, not AM"); ?>'>
                                            <option value='1'><?php echo xlt('AM'); ?></option>
                                            <option value='2'<?php if ($startampm == '2') echo " selected" ?>><?php echo xlt('PM'); ?></option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td nowrap>
                                        <b><?php echo ($GLOBALS['athletic_team'] ? xlt('Team/Squad') : xlt('Title')); ?>:</b>
                                    </td>
                                    <td nowrap>
                                        <input type='text' size='10' name='form_title'  class="form-control" value='<?php echo attr($row['pc_title']); ?>'
                                               style='width:100%'
                                               title='<?php echo xla('Event title'); ?>' />
                                    </td>
                                    <td nowrap>&nbsp;

                                    </td>
                                    <td nowrap id='tdallday4'><?php echo xlt('duration'); ?>
                                    </td>
                                    <td nowrap id='tdallday5'>
                                        <input type='text' size='4' name='form_duration' value='<?php echo attr($thisduration) ?>' title='<?php echo xla('Event duration in minutes'); ?>' />
                                        <?php echo xlt('minutes'); ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td id="id_fac" nowrap><b><?php echo xlt('Facility'); ?>:</b></td>
                                    <td>  

                                        <select multiple="multiple" name="facility[]" id="facility" >
                                            <?php
                                            $facils = getUserFacilities($_SESSION['authId']);
                                            $countUserFacilities = 0;
                                            if (!$GLOBALS['restrict_user_facility']) {
                                                $qsql = sqlStatement("
            select id, name, color
            from facility
            where service_location != 0
        ");
                                            } else {
                                                $qsql = sqlStatement("
              select uf.facility_id as id, f.name, f.color
              from users_facility uf
              left join facility f on (uf.facility_id = f.id)
              where uf.tablename='users' 
              and uf.table_id = ? 
            ", array($_SESSION['authId']));
                                            }

                                            while ($facrow = sqlFetchArray($qsql)) {

                                                if ($_SESSION['authorizedUser'] || in_array($facrow, $facils)) {
                                                    $selected = ( $facrow['id'] == $e2f ) ? 'selected="selected"' : '';
                                                    echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";
                                                } else {
                                                    $selected = ( $facrow['id'] == $e2f ) ? 'selected="selected"' : '';
                                                    echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";
                                                }
                                                $countUserFacilities++;
                                            }
                                            ?>

                                    </td>      </select>
                                </tr>
                                    <!--<tr>
                                            <td nowrap>
                                            <b><?php echo xlt('Billing Facility'); ?>:</b>
                                            </td>
                                            <td>
<?php
billing_facility('billing_facility', $row['pc_billing_location']);
?>
                                            </td>
                                    </tr>-->
                                <?php
                                if ($_GET['prov'] != true) {
                                    ?>
                                    <tr id="patient_details">
                                        <td nowrap>
                                            <b><?php echo xlt('Patient'); ?>:</b>
                                        </td>
                                        <td nowrap>
                                            <input type='text' size='10' name='form_patient' style='width:100%;cursor:pointer;cursor:hand' value='<?php echo attr($patientname); ?>' onclick='sel_patient()' title='<?php echo xla('Click to select patient'); ?>' readonly />
                                            <input type='hidden' name='form_pid' value='<?php echo attr($patientid) ?>' />
                                            <span style="font-weight: bold;padding: 1px 1px 10px 1px;font-style: italic;"><a class="noresult" href='javascript:void(0)' onclick="fun_add_patient();";>Add New</a></span>
                                        </td>
                                        <td colspan='3' nowrap style='font-size:8pt'>
                                            &nbsp;
                                            <span class="infobox">

                                            </span>
                                        </td>
                                    </tr>
                                    <script>
                                        function fun_add_patient() {
                                            // Pass the variable to parent hidden type and submit
                                            document.theform.resname.value = "noresult";
                                            document.theform.submit();
                                        }
                                    </script>
    <?php
}
?>
                                <tr>
                                    <tr>
                                    <td nowrap>
                                        <b><?php echo xlt('All Clinics Event '); ?>:</b>
                                    </td>
                                    <td colspan='4' nowrap style='float:left'>
                                        <input style="width:auto" type='checkbox' name='form_all_clinic'  id="allClinicsCheckbox" class="form-control" onclick='set_all_clinics(this)' value='1'<?php if ($repeats) echo " checked" ?>/> 
                                    </td>
                                </tr>
                                    <td nowrap id="id_prov">
                                        <b><?php echo xlt('Provider'); ?>:</b>
                                    </td>
                                    <td nowrap><div id ="providerSelectionDataRow">

<?php
// Get the providers list.
                                            /* $singleFacilityID = '';
if (isset($e2f) && !empty($e2f)) {
    $singleFacilityID = $e2f;
} else {
    $facilList = getUserFacilitiesList($_SESSION['authId']);

    if (!empty($facilList)) {
        $facilListArr = explode(',', $facilList);
        $singleFacilityID = $facilListArr[0];
    }
}

if ($GLOBALS['restrict_user_facility']) {
    $ures = sqlStatement("SELECT id, username, fname, lname FROM users as us, users_facility as uf WHERE " .
                                              "authorized != 0 AND active = 1 and ((us.facility_id = $singleFacilityID) OR (uf.facility_id = $singleFacilityID AND uf.table_id = us.id)) GROUP BY us.id ORDER BY fname");
} else {
    $ures = sqlStatement("SELECT id, username, fname, lname FROM users WHERE " .
                                              "authorized != 0 AND active = 1 and facility_id = $singleFacilityID ORDER BY fname");
}

if (!$GLOBALS['select_multi_providers']) {



    $defaultProvider = $_SESSION['authUserID'];
    // or, if we have chosen a provider in the calendar, default to them
    // choose the first one if multiple have been selected
    if (count($_SESSION['pc_username']) >= 1) {
        // get the numeric ID of the first provider in the array
        $pc_username = $_SESSION['pc_username'];
        $firstProvider = sqlFetchArray(sqlStatement("select id from users where username=?", array($pc_username[0])));
        $defaultProvider = $firstProvider['id'];
                                              } */
    // if we clicked on a provider's schedule to add the event, use THAT.
                                            /*    if ($userid)
        $defaultProvider = $userid;
                                              } */
    echo "<select name='form_provider' id='form_provider' class='form-control' />";
    echo "<option value='-1'>All Providers</option";
                                            /* while ($urow = sqlFetchArray($ures)) {
        echo "    <option value='" . attr($urow['id']) . "'";
        if ($urow['id'] == $defaultProvider)
            echo " selected";
        echo ">" . text($urow['lname']);
        if ($urow['fname'])
            echo ", " . text($urow['fname']);
        echo "</option>\n";
                                              } */
    echo "</select>";
                                            /*                                             * ************************************************************* */
//}
?>
                                        </div>
                                    </td>
                                    <!--
                                    <td nowrap>
                                     &nbsp;&nbsp;
                                     <input type='checkbox' name='form_repeat' onclick='set_repeat(this)' value='1'<?php //if ($repeats) echo " checked"   ?>/>
                                     <input type='hidden' name='form_repeat_exdate' id='form_repeat_exdate' value='<?php //echo attr($repeatexdate);   ?>' /> <!-- dates excluded from the repeat -->
                                    <!-- </td>
                                     <td nowrap id='tdrepeat1'><?php //echo xlt('Repeats');   ?>
                                     </td>
                                     <td nowrap>
                                   
                                      <!-- <select name='form_repeat_freq' id='form_repeat_freq' title='<?php echo xla('Every, every other, every 3rd, etc.'); ?>'>
<?php
/*
  foreach (array(1 => xl('every'), 2 => xl('2nd'), 3 => xl('3rd'), 4 => xl('4th'), 5 => xl('5th'), 6 => xl('6th'))
  as $key => $value)
  {
  echo "    <option value='" . attr($key) . "'";
  if ($key == $repeatfreq) echo " selected";
  echo ">" . text($value) . "</option>\n";
  }
 * 
 */
?>
                                      </select>  -->

   <!--<select name='form_repeat_type' id='form_repeat_type'>
<?php
/*
  // See common.api.php for these:
  foreach (array(0 => xl('daily') , 1 => xl('weekly'), 2 => xl('monthly'))
  as $key => $value)
  {
  echo "    <option value='" . attr($key) . "'";
  if ($key == $repeattype) echo " selected";
  echo ">" . text($value) . "</option>\n";
  }
 * 
 */
?>
   </select>

  </td>
                                    -->
                                </tr>

 <!--<tr>
  <td nowrap>
   <span id='title_apptstatus'><b><?php //echo ($GLOBALS['athletic_team'] ? xlt('Session Type') : xlt('Status'));   ?></b></span>
   <span id='title_prefcat' style='display:none'><b><?php // echo xlt('Pref Cat');   ?></b></span>
  </td>
  <td nowrap>

<?php
generate_form_field(array('data_type' => 1, 'field_id' => 'apptstatus', 'list_id' => 'apptstat', 'empty_title' => 'SKIP'), $row['pc_apptstatus']);
?>
                                <!--
                                The following list will be invisible unless this is an In Office
                                event, in which case form_apptstatus (above) is to be invisible.
                                -->
                                <!-- <select name='form_prefcat' style='width:100%;display:none' title='<?php echo xla('Preferred Event Category'); ?>'>
<?php echo $prefcat_options ?> 
                                </select>-->

                                <!--</td>
                                <td nowrap>&nbsp;
                                 
                                </td>
                                <!--<td nowrap id='tdrepeat2'><?php //echo xlt('until');   ?>
                                </td>
                                <td nowrap>
                                 <input type='text' size='10' name='form_enddate' id='form_enddate' value='<?php //echo attr($row['pc_endDate'])   ?>' onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)' title='<?php echo xla('yyyy-mm-dd last date of this event'); ?>' />
                                 <img src='<?php //echo $GLOBALS['web_root'];   ?>/interface/pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                  id='img_enddate' border='0' alt='[?]' style='cursor:pointer;cursor:hand'
                                  title='<?php //echo xla('Click here to choose a date');  ?>'>-->
<?php
/*
  if ($repeatexdate != "") {
  $tmptitle = "The following dates are excluded from the repeating series";
  if ($multiple_value) { $tmptitle .= " for one or more providers:\n"; }
  else { $tmptitle .= "\n"; }
  $exdates = explode(",", $repeatexdate);
  foreach ($exdates as $exdate) {
  $tmptitle .= date("d M Y", strtotime($exdate))."\n";
  }
  echo "<a href='#' title='" . attr($tmptitle) . "' alt='" . attr($tmptitle) . "'><img src='../../pic/warning.gif' title='" . attr($tmptitle) . "' alt='*!*' style='border:none;'/></a>";
  }
 * 
 */
?>
                                <!-- </td>
                                -->
                                <!--</tr> --> 
                                

                                <tr>
                                    <td nowrap>
                                        <b><?php echo xlt('Comments'); ?>:</b>
                                    </td>
                                    <td colspan='4' nowrap>
                                        <input type='text' size='40' name='form_comments' class="form-control" style='width:100%' value='<?php echo attr($hometext); ?>' title='<?php echo xla('Optional information about this event'); ?>' />
                                    </td>
                                </tr>


<?php
// DOB is important for the clinic, so if it's missing give them a chance
// to enter it right here.  We must display or hide this row dynamically
// in case the patient-select popup is used.
$patient_dob = trim($prow['DOB']);
$dobstyle = ($prow && (!$patient_dob || substr($patient_dob, 5) == '00-00')) ?
        '' : 'none';
?>
                                <tr id='dob_row' style='display:<?php echo $dobstyle ?>'>
                                    <td colspan='4' nowrap>
                                        <b><font color='red'><?php echo xlt('DOB is missing, please enter if possible'); ?>:</font></b>
                                    </td>
                                    <td nowrap>
                                        <input type='text' size='10' name='form_dob' id='form_dob' title='<?php echo xla('yyyy-mm-dd date of birth'); ?>' onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' />
                                        <img src='<?php echo $GLOBALS['web_root']; ?>/interface/pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                             id='img_dob' border='0' alt='[?]' style='cursor:pointer;cursor:hand'
                                             title='<?php echo xla('Click here to choose a date'); ?>'>
                                    </td>
                                </tr>

                            </table></td></tr>
                    <tr class='text'>
                        <td>&nbsp;</td>
                        <td colspan='9' style='margin-left:8px;float:left'>
                            <p>
                                <input type='button' name='form_save' id='form_save' class="btn btn-warning btn-sm" value='<?php echo xla('Save'); ?>' />
                                &nbsp;
                                <!-- <input type='button' id='find_available' value='<?php echo xla('Find Available'); ?>' /> -->
                                &nbsp;
                                <!-- <input type='button' name='form_delete' id='form_delete' value='<?php echo xla('Delete'); ?>'<?php if (!$eid) echo " disabled" ?> /> -->
                                &nbsp;
                                <input type='button' id='cancel' class="btn btn-warning btn-sm" value='<?php echo xla('Cancel'); ?>' />
                                &nbsp;
                                <!-- <input type='button' name='form_duplicate' id='form_duplicate' value='<?php echo xla('Create Duplicate'); ?>' /> -->
                            </p></td></tr></table>
<?php if ($informant) echo "<p class='text'>" . xlt('Last update by') . " ($informant) " . xlt('on') . " " . text($row['pc_time']) . "</p>\n"; ?>

                </form>




            </div>
        </div>
        <div id="recurr_popup" style="visibility: hidden; position: absolute; top: 50px; left: 50px; width: 400px; border: 3px outset yellow; background-color: yellow; padding: 5px;">
<?php echo xlt('Apply the changes to the Current event only, to this and all Future occurrences, or to All occurrences?') ?>
            <br>
            <input type="button" name="all_events" id="all_events" value="  All  ">
            <input type="button" name="future_events" id="future_events" value="Future">
            <input type="button" name="current_event" id="current_event" value="Current">
            <input type="button" name="recurr_cancel" id="recurr_cancel" value="Cancel">
        </div>

    </body>

    <script language='JavaScript'>
<?php if ($eid) { ?>
            set_display();
<?php } else { ?>
            set_category();
<?php } ?>
        set_allday();
        //set_repeat();

        Calendar.setup({inputField: "form_date", ifFormat: "%Y-%m-%d", button: "img_date"});
        //Calendar.setup({inputField:"form_enddate", ifFormat:"%Y-%m-%d", button:"img_enddate"});
        Calendar.setup({inputField: "form_dob", ifFormat: "%Y-%m-%d", button: "img_dob"});
    </script>

    <script language="javascript">

        $(document).ready(function () {

            var mycolor = '#777777';

            //$("#form_duplicate").click(function() { validate("duplicate"); });
            $("#find_available").click(function () {

                find_available('');
            });

            //$("#form_delete").click(function() { deleteEvent(); });
            $("#cancel").click(function () {
                window.close();
                window.location.href = 'list.php';
            });

            // buttons affecting the modification of a repeating event
            $("#all_events").click(function () {
                $("#recurr_affect").val("all");
                EnableForm();
                SubmitForm();
            });

            $("#future_events").click(function () {
                $("#recurr_affect").val("future");
                EnableForm();
                SubmitForm();
            });

            $("#current_event").click(function () {
                $("#recurr_affect").val("current");
                EnableForm();
                SubmitForm();
            });

            $("#recurr_cancel").click(function () {
                $("#recurr_affect").val("");
                EnableForm();
                HideRecurrPopup();
            });

            $("#facility").change(function () {

                var facilityID = $("#facility").val();
                $.ajax({
                    url: "get_facility_provider.php?facilityId=" + facilityID,
                    success: function (result) {

                        $("#providerSelectionDataRow").html(result);

                    }
                });
            });

            $('#select_all').click( function() {

                $('#facility > option').removeAttr('selected');
            });

            $("#form_save").click(function(){
                    
                //Checking if a facility is selected 
                if(!!$("#facility").val() || $("#allClinicsCheckbox").prop('checked')){
                   //Proceed 
                } 
                else{
                    jAlert('<?php echo addslashes(xl("Please select facility or All clinics checkbox.")); ?>');
                    return false;
                }

                //Checking if Timeslot is also reserved 
                var f = document.getElementById('theform');

                if (f.rballday2.checked == true && (f.form_duration.value == '' || f.form_duration.value == '0' || f.form_hour.value == '' || f.form_hour.value == '0' || f.form_minute.value == '' || f.form_minute.value == '')) {
                    jAlert('<?php echo addslashes(xl("Please Enter time and duration.")); ?>');
                    return false;
                }

                else{

                    validate("save");
                }
            });

        });

        // Check for errors when the form is submitted.
        function validate(valu) {
            var f = document.getElementById('theform');

            if (f.rballday2.checked == true && (f.form_duration.value == '' || f.form_duration.value == '0' || f.form_hour.value == '' || f.form_hour.value == '0' || f.form_minute.value == '' || f.form_minute.value == '')) {
                jAlert('<?php echo addslashes(xl("Please Enter time and duration.")); ?>');
                //jAlert("Please Enter time and duration.");
                return false;
            }

            $('#form_action').val(valu);

                <?php if ($repeats): ?>
                // existing repeating events need additional prompt
                if ($("#recurr_affect").val() == "") {
                    DisableForm();
                    // show the current/future/all DIV for the user to choose one
                    $("#recurr_popup").css("visibility", "visible");
                    return false;
                }
                <?php endif; ?>

            return SubmitForm();
        }

        // disable all the form elements outside the recurr_popup
        function DisableForm() {

            $("#theform").children().attr("disabled", "true");
        }

        function EnableForm() {

            $("#theform").children().removeAttr("disabled");
        }

        function HideRecurrPopup() {

            $("#recurr_popup").css("visibility", "hidden");
        }

        function SubmitForm() {
            var f = document.forms[0];
            if (f.form_action.value != 'delete') {
                // Check slot availability.
                var mins = parseInt(f.form_hour.value) * 60 + parseInt(f.form_minute.value);
                if (f.form_ampm.value == '2' && mins < 720)
                    mins += 720;
                find_available('&cktime=' + mins);
            }
            else {
                top.restoreSession();
                f.submit();
            }
            return true;
        }

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>

    <!-- stuff for the popup calendar -->
    <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
    <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
    <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
    <script language="Javascript">
        Calendar.setup({inputField: "form_date", ifFormat: "%Y-%m-%d", button: "img_date"});
        // Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
    </script>
    
</html>
