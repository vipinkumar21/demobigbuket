<?php
$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("../drugs/drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 
 $clinic = $_REQUEST['facility'];
 $res = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename
		FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
 $res .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username
		INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id
		INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
 
 if ($clinic != 0) { 		
 	$res .= " WHERE uf.facility_id = ".$clinic ;
 	$res .= " AND gag.id IN (12, 13, 18) ";
 	$res .= " GROUP BY u.id DESC";
 	$res = sqlStatement($res);
 	
 	if(sqlNumRows($res)){
 		$optionString = "<option value='0' selected='selected'>Select Provider</option>";
 		while ($row = sqlFetchArray($res)) { 	 			
 			if($clinic !=$row['id'])		
 				$optionString .= "<option value='".$row['id']."'>".$row['completename']."</option>"; 			
 		}
 		echo $optionString;
 	}else{ 
 		echo "<option value='0' selected='selected'>Select Provider</option>";
 	}
 }else {
 	$fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
						FROM users_facility AS uf
						LEFT JOIN users AS us ON uf.table_id = us.id
						LEFT JOIN facility AS f ON uf.facility_id = f.id
						WHERE uf.table_id = ".$_SESSION['authId']." AND us.authorized !=0
						AND us.active = 1 ORDER BY id ";
 	$fRes = sqlStatement($fsql);
 	$facilityIds = '';
 	while ($fRow = sqlFetchArray($fRes)) {
 		if(!empty($facilityIds)) {
 			$facilityIds .= ', '.$fRow['facility_id'];
 		}else {
 			$facilityIds = $fRow['facility_id'];
 		}
 	}
 	$res .=" WHERE ";
 	$res .= "uf.facility_id IN (".$facilityIds.") ";
 	$res .= "AND gag.id IN (12, 13, 18) ";
 	$res .= " GROUP BY u.id DESC";
 	
 	$result_rowset = sqlStatement($res);
 	echo "<option value='0' selected='selected'>Select Provider</option>";
 	while ($rowset = sqlFetchArray($result_rowset)) {
 		$selected = ( $rowset['id'] == $doctor ) ? 'selected="selected"' : '' ;
 		echo "<option value='" . $rowset['id'] . "' $selected>" . text($rowset['completename']) . "</option>"; 		 		
 	}
 }