<?php
include_once("../globals.php");
include_once("../pagination.php");
include_once("$srcdir/log.inc");
include_once("$srcdir/formdata.inc.php");
require_once("$srcdir/formatting.inc.php");

?>
<html>
    <head>
        <?php html_header_show(); ?>
        <link rel="stylesheet" href='<?php echo $GLOBALS['webroot'] ?>/library/dynarch_calendar.css' type='text/css'>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dynarch_calendar.js"></script>
        <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dynarch_calendar_setup.js"></script>

        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-1.2.2.min.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <style>
            /*#logview {
                width: 100%;
            }

            #logview table{float:left;width:100%;}
            #logview table td{font-size:14px!important;} */

         /*   #logview table {
                width:100%;
                border-collapse: collapse;
                float:left;
            }
            #logview th {
                background-color: #cccccc;
                cursor: pointer; cursor: hand;
                padding: 5px 5px;
                align: left;
                text-align: left;
            }

            #logview td {
                background-color: #ffffff;
                border-bottom: 1px solid #808080;
                cursor: default;
                padding: 5px 5px;
                vertical-align: top;
            } */
            /* .highlight {
                background-color: #336699;
                color: #336699;
            } */
        </style>
        <?php echo paginationCss(); ?>
        <script>
          
         <?php echo paginationScript(); ?>
        //function to disable the event type field if the event name is disclosure
            function eventTypeChange(eventname)
            {
                if (eventname == "disclosure") {
                    document.theform.type_event.disabled = true;
                }
                else {
                    document.theform.type_event.disabled = false;
                }
            }

        // VicarePlus :: This invokes the find-patient popup.
            function sel_patient() {
                dlgopen('../main/calendar/find_patient_popup.php?pflag=0', '_blank', 500, 400);
            }

        // VicarePlus :: This is for callback by the find-patient popup.
            //function setpatient(pid, lname, fname, dob) {
        //  var f = document.theform;
            // f.form_patient.value = lname + ', ' + fname;
            // f.form_pid.value = pid;
            //}

            function setpatient(pid, fname) {
                var f = document.theform;
                f.form_patient.value = fname;
                f.form_pid.value = pid;
            }

        </script>
    </head>
    <body id="auditingviewerbody" class="body_top">
        <h3 class="emrh3"><?php xl('Auditing Viewer', 'e'); ?></h3>
        
        <?php
        $err_message = 0;
        if ($_GET["start_date"])
            $start_date = formData('start_date', 'G');

        if ($_GET["end_date"])
            $end_date = formData('end_date', 'G');

        if ($_GET["form_patient"])
            $form_patient = formData('form_patient', 'G');

        if ($_GET['start_hour'])
            $start_hour = formData('start_hour', 'G');

        if ($_GET['end_hour'])
            $end_hour = formData('end_hour', 'G');

        if ($_GET['start_minute'])
            $start_minute = formData('start_minute', 'G');

        if ($_GET['end_minute'])
            $end_minute = formData('end_minute', 'G');
        /*
         * Start date should not be greater than end date - Date Validation
         */


        if ($start_hour > 24 || $end_hour > 24) {
            echo "<table><tr class='alert'><td colspan=7>";
            xl('Hours should not be greater than 24', e);
            echo "</td></tr></table>";
            $err_message = 1;
        }
        if ($start_minute > 60 || $end_minute > 60) {
            echo "<table><tr class='alert'><td colspan=7>";
            xl('Minutes should not be greater than 60', e);
            echo "</td></tr></table>";
            $err_message = 1;
        }

        if ($start_hour == 24) {
            $shh = '23';
        } elseif (strlen($start_hour) == 1) {
            $shh = '0' . $start_hour;
        } elseif (strlen($start_hour) == 0) {
            $shh = '00';
        } else{
             $shh = $start_hour;
        }

        if ($start_minute == 60) {
            $sMM = '59';
        } elseif (strlen($start_minute) == 1) {
            $sMM = '0' . $start_minute;
        } elseif(strlen($start_minute) == 0) {
            $sMM = '00';
        }else{
            $sMM = $start_minute;
        }

        $starttime = "$shh:$sMM:00";

        if ($end_hour == 24) {
            $ehh = '23';
        } elseif (strlen($end_hour) == 1) {
            $ehh = '0' . $end_hour;
        } elseif (strlen($end_hour) == 0) {
            $ehh = '23';
        } else{
             $ehh = $end_hour;
        }

        if ($end_minute == 60) {
            $eMM = '59';
        } elseif (strlen($end_minute) == 1) {
            $eMM = '0' . $end_minute;
        } elseif (strlen($end_minute) == 0) {
            $eMM = '59';
        } else{
            $eMM = $end_minute;
        }

        $endtime = "$ehh:$eMM:00";

        $startdate_time = $start_date . ' ' . $starttime;
        $enddate_time = $end_date . ' ' . $endtime;

        if ($start_date && $end_date) {
            if ($startdate_time > $enddate_time) {

                /*echo "<table><tr class='alert'><td colspan=7>";
                xl('Start DateTime should not be greater than End DateTime', e);
                echo "</td></tr></table>";*/

                echo "<div class='text' style='position:absolute;top:226px;left:23px;'> ";
                xl('Start DateTime should not be greater than End DateTime', e);
                echo "</div>";

                /*echo "<script language='JavaScript'>\n";
                echo "jAlert('Start DateTime should not be greater than End DateTime', 'Alert');";
                echo "</script>\n";*/

                $err_message = 1;
            }
        }
        ?>
        <?php
        $form_user = formData('form_user', 'R');
        $form_pid = formData('form_pid', 'R');
        $eventname = formData('eventname', 'G');
        $type_event = formData('type_event', 'G');
        
        if ($form_patient == '')
            $form_pid = '';

        //$res = sqlStatement("select distinct LEFT(date,10) as date from log order by date desc limit 30");
        //for ($iter = 0; $row = sqlFetchArray($res); $iter++) {
       //     $ret[$iter] = $row;
       // }

// Get the users list.
        $sqlQuery = "SELECT username, fname, lname FROM users " .
                "WHERE active = 1 AND ( info IS NULL OR info NOT LIKE '%Inactive%' ) ";

        $ures = sqlStatement($sqlQuery);
        ?>

        <?php
        $get_sdate = $start_date ? $start_date : date("Y-m-d");
        $get_edate = $end_date ? $end_date : date("Y-m-d");
        ?>

        
        <form class="emrform " METHOD="GET" name="theform" id="theform" >
            <?php
            $sortby = formData('sortby', 'G');
            ?>
            <input type="hidden" name="sortby" id="sortby" value="<?php echo $sortby; ?>">
            <input type="hidden" name=csum value="">

            <div class="row">

                <!-- First Column Begins --> 
                <div class="form_left col-xs-4 topnopad newnoclear">

                    <div class="form-group"> 
                        <span class="text"><?php xl('Start Date', 'e'); ?>: </span><br/>
                        <input class="emrdate" type="text" size="10" name="start_date" id="start_date" value="<?php echo $start_date ? substr($start_date, 0, 10) : date('Y-m-d'); ?>" title="<?php xl('yyyy-mm-dd Date of service', 'e'); ?>" onkeyup="datekeyup(this, mypcc)" onblur="dateblur(this, mypcc)" />
                        <img src="../pic/show_calendar.gif" align="absbottom" width="24" height="22" id="img_begin_date" border="0" alt="[?]" style="cursor: pointer; cursor: hand" title="<?php xl('Click here to choose a date', 'e'); ?>">&nbsp;
                        <input type='text' size='2' name='start_hour' value='<?php echo attr($start_hour); ?>' maxlength="2" placeholder="HH"/> : 
                        <input type='text' size='2' name='start_minute' value='<?php echo attr($start_minute); ?>' maxlength="2" placeholder="MM"/>

                    </div> 

                    <div class="form-group"> 

                        <span class='text'><?php xl('User', 'e'); ?>: </span>
                        <?php
                            echo "<select class='emrinput' name='form_user'>\n";
                            echo " <option value=''>" . xl('All') . "</option>\n";
                            while ($urow = sqlFetchArray($ures)) {
                                if (!trim($urow['username']))
                                    continue;
                                echo " <option value='" . $urow['username'] . "'";
                                if ($urow['username'] == $form_user)
                                    echo " selected";
                                echo ">" . $urow['username'];
                                echo "</option>\n";
                            }
                            echo "</select>\n";
                        ?>

                    </div>

                    <div class="form-group"> 
                        <input type=hidden name="event" value=<?php echo $event; ?>>
                        <input type="button" class="btn btn-warning btn-sm" name="search" value="Search" onclick="javascript:document.theform.submit();">

                    </div>

                </div> 
                <!-- End First Column --> 

                <!-- Second Column Starts -->             
                <div class="form_left col-xs-4 topnopad newnoclear">

                    <div class="form-group"> 
                            <span class="text"><?php xl('End Date', 'e'); ?>: </span><br/>
                            <input class="emrdate" type="text" size="10" name="end_date" id="end_date" value="<?php echo $end_date ? substr($end_date, 0, 10) : date('Y-m-d'); ?>" title="<?php xl('yyyy-mm-dd Date of service', 'e'); ?>" onkeyup="datekeyup(this, mypcc)" onblur="dateblur(this, mypcc)" />
                            <img src="../pic/show_calendar.gif" align="absbottom" width="24" height="22" id="img_end_date" border="0" alt="[?]" style="cursor: pointer; cursor: hand" title="<?php xl('Click here to choose a date', 'e'); ?>">&nbsp;
                            <input type='text' size='2' name='end_hour' value='<?php echo attr($end_hour); ?>' maxlength="2" placeholder="HH"/> : 
                            <input type='text' size='2' name='end_minute' value='<?php echo attr($end_minute); ?>' maxlength="2" placeholder="MM"/>
                    </div>     

                    <div class="form-group"> 
                    
                    <span class='text'><?php xl('Name of Events', 'e'); ?>: </span>

                          <?php
                                $res = sqlStatement("SELECT DISTINCT event FROM log WHERE type_event IS NOT NULL ORDER BY event ASC");
                                $ename_list = array();
                                $j = 0;
                                while ($erow = sqlFetchArray($res)) {
                                    if (!trim($erow['event']))
                                        continue;
                                    $data = explode('-', $erow['event']);
                                    $data_c = count($data);
                                    $ename = $data[0];
                                    for ($i = 1; $i < ($data_c - 1); $i++) {
                                        $ename.="-" . $data[$i];
                                    }
                                    $ename_list[$j] = $ename;
                                    $j = $j + 1;
                                }
                                /*
                                $res1 = sqlStatement("select distinct event from  extended_log order by event ASC");
                                $j = 0;
                                while ($row = sqlFetchArray($res1)) {
                                    if (!trim($row['event']))
                                        continue;
                                    $new_event = explode('-', $row['event']);
                                    $no = count($new_event);
                                    $events = $new_event[0];
                                    for ($i = 1; $i < ($no - 1); $i++) {
                                        $events.="-" . $new_event[$i];
                                    }
                                    if ($events == "disclosure")
                                        $ename_list[$j] = $events;
                                    $j = $j + 1;
                                }
                                 * 
                                 */
                                $ename_list = array_unique($ename_list);
                                $ename_list = array_merge($ename_list);
                                $ecount = count($ename_list);
                                echo "<select class='emrinput' name='eventname' onchange='eventTypeChange(this.options[this.selectedIndex].value);'>\n";
                                echo " <option value=''>" . xl('All') . "</option>\n";
                                for ($k = 0; $k < $ecount; $k++) {
                                    echo " <option value='" . $ename_list[$k] . "'";
                                    if ($ename_list[$k] == $eventname && $ename_list[$k] != "")
                                        echo " selected";
                                    echo ">" . $ename_list[$k];
                                    echo "</option>\n";
                                }
                                echo "</select>\n";
                                ?>

                    </div>

                </div> 
                <!-- End Second Column --> 

                <!-- Third Column Starts --> 
                <div class="form_left col-xs-4 topnopad">

                    <div class="form-group"> 
                   
                         <span class='text'><?php echo htmlspecialchars(xl('Patient'), ENT_NOQUOTES); ?>: </span>
                         <input class="emrdate"  type='text' size='20' name='form_patient' style='width:100%;cursor:pointer;cursor:hand' value='<?php echo $form_patient ? $form_patient : htmlspecialchars(xl('Click To Select'), ENT_QUOTES); ?>' onclick='sel_patient()' title='<?php echo htmlspecialchars(xl('Click to select patient'), ENT_QUOTES); ?>' />
                        <input type='hidden' name='form_pid' value='<?php echo $form_pid; ?>' />

                    </div> 

                    <div class="form-group"> 

                        <span class='text'><?php xl('Type of Events', 'e'); ?>: </span>

                        <?php
                        //$event_types = array("select", "update", "insert", "delete");
                        $event_types = array("0"=>"Cancle", "1"=>"Change", "2"=>"Report", "3"=>"Save","4"=>"Cron");
                        $lcount = count($event_types);
                        if ($eventname == "disclosure") {
                            echo "<select  name='type_event' disabled='disabled'>\n";
                            echo " <option value=''>" . xl('All') . "</option>\n";
                            echo "</option>\n";
                        } else {
                            echo "<select class='emrinput' name='type_event'>\n";
                        }
                        echo " <option value=''>" . xl('All') . "</option>\n";
                        for ($k = 0; $k < $lcount; $k++) {
                            echo " <option value='" . $k . "'";
                            if ($k == $type_event && $type_event != ""){
                                echo " selected";
                            }
                            echo ">" . $event_types[$k];
                            echo "</option>\n";
                        }
                        echo "</select>\n";
                        ?>

                    </div> 

                </div> 
                <!-- End Third Columns --> 

            </div> 

        


<?php if ($start_date && $end_date && $err_message != 1) { ?>
            <div id="logview">
                <table class="table table-bordered emrtable">
                    <thead>
                    <tr>                     
                        <th id="sortby_date" class="" title="<?php xl('Sort by date/time', 'e'); ?>" width="15%" ><?php xl('Date', 'e'); ?></th>
                        <th id="sortby_event" class="" title="<?php xl('Sort by Event', 'e'); ?>" width="10%"><?php xl('Event', 'e'); ?></th>
                        <th id="sortby_user" class="" title="<?php xl('Sort by User', 'e'); ?>" width="10%"><?php xl('User', 'e'); ?></th>
                        <th id="sortby_pid" class="" title="<?php xl('Sort by PatientID', 'e'); ?>" width="10%"><?php xl('PatientID', 'e'); ?></th>                        
                        <th id="sortby_success" class="" title="<?php xl('Sort by Success', 'e'); ?>" width="5%"><?php xl('Success', 'e'); ?></th>
                         <th id="sortby_ipaddress" class="" title="<?php xl('Sort by IpAddress', 'e'); ?>" width="10%"><?php xl('IP Address', 'e'); ?></th>
                        <th id="sortby_comments" class="" title="<?php xl('Sort by Comments', 'e'); ?>" width="50%"><?php xl('Comments', 'e'); ?></th>
                    </tr>
                    </thead>
                    <?php
                    $tevent = $type_event;
                    $gev = $eventname;
                   
                   
                    $page = (int) (!empty($_GET["page"]) ? $_GET["page"] :1 );
                    $limit = $GLOBALS['encounter_page_size'];
                    $startpoint = ($page * $limit) - $limit;

                    $url = $_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'].'&';
                    
                    $num_rows = getEvents(array('sdate' => $startdate_time, 'edate' => $enddate_time, 'user' => $form_user, 'patient' => $form_pid, 'sortby' => $_GET['sortby'], 'levent' => $gev, 'tevent' => $tevent), $startpoint, $limit, true);

                    if ($ret = getEvents(array('sdate' => $startdate_time, 'edate' => $enddate_time, 'user' => $form_user, 'patient' => $form_pid, 'sortby' => $_GET['sortby'], 'levent' => $gev, 'tevent' => $tevent), $startpoint, $limit, false)) {


                        foreach ($ret as $iter) {
                            //translate comments
                            $patterns = array('/^success/', '/^failure/', '/ encounter/');
                            $replace = array(xl('success'), xl('failure'), xl('encounter', '', ' '));
                            $trans_comments = preg_replace($patterns, $replace, $iter["comments"]);
                            ?>
                            <TR class="oneresult"> 
                                <TD ><?php echo oeFormatShortDate(substr($iter["date"], 0, 10)) . substr($iter["date"], 10) ?></TD>
                                <TD ><?php echo xl($iter["event"]) ?></TD>
                                <TD ><?php echo $iter["user"] ?></TD>                               
                                <TD ><?php echo $iter["pubpid"] ?></TD>
                                <TD ><?php if($iter["success"] == 2){ echo 'Cron Schedule';}elseif($iter["success"] == '1'){ echo 'Yes';} else { echo 'No';} ?></TD>
                                <TD ><?php echo $iter["ipaddress"]; ?></TD>
                                
                                <TD class="text"><?php 
                                if($iter["type_event"] == 2 || $iter["type_event"] == 4){
                                  echo $iter["user_notes"];
                                } else {
                                  echo $iter["comments"];   
                                }
                                ?></TD>
           
                            </TR>

                                <?php
                                //$counter++; 
                            }
                        }
                        // Pagination Displaying Section
                        ?>
                    <table id="auditingtablepagination" style="margin-top:8px" class="emrpagination">
                            <tbody>
                                    <tr>
                                            <td class="text" >
                                                <?php	echo pagination($num_rows,$limit,$page,$url); ?>
                                            </td>
                                    </tr>
                            </tbody>                   
                    </table>
            </div>

                                <?php } ?>

    </form>

    </body>

    <script language="javascript">

    // jQuery stuff to make the page a little easier to use
        $(document).ready(function () {
            // funny thing here... good learning experience
            // the TR has TD children which have their own background and text color
            // toggling the TR color doesn't change the TD color
            // so we need to change all the TR's children (the TD's) just as we did the TR
            // thus we have two calls to toggleClass:
            // 1 - for the parent (the TR)
            // 2 - for each of the children (the TDs)
            $(".oneresult").mouseover(function () {
                $(this).toggleClass("highlight");
                $(this).children().toggleClass("highlight");
            });
            $(".oneresult").mouseout(function () {
                $(this).toggleClass("highlight");
                $(this).children().toggleClass("highlight");
            });

            // click-able column headers to sort the list
            $("#sortby_date").click(function () {
                $("#sortby").val("date");
                $("#theform").submit();
            });
            $("#sortby_event").click(function () {
                $("#sortby").val("event");
                $("#theform").submit();
            });
            $("#sortby_user").click(function () {
                $("#sortby").val("user");
                $("#theform").submit();
            });
            $("#sortby_cuser").click(function () {
                $("#sortby").val("user");
                $("#theform").submit();
            });
            $("#sortby_group").click(function () {
                $("#sortby").val("groupname");
                $("#theform").submit();
            });
            $("#sortby_pid").click(function () {
                $("#sortby").val("patient_id");
                $("#theform").submit();
            });
            $("#sortby_success").click(function () {
                $("#sortby").val("success");
                $("#theform").submit();
            });
            $("#sortby_comments").click(function () {
                $("#sortby").val("comments");
                $("#theform").submit();
            });
            $("#sortby_checksum").click(function () {
                $("#sortby").val("checksum");
                $("#theform").submit();
            });
        });


        /* required for popup calendar */
        Calendar.setup({inputField: "start_date", ifFormat: "%Y-%m-%d", button: "img_begin_date"});
        Calendar.setup({inputField: "end_date", ifFormat: "%Y-%m-%d", button: "img_end_date"});

    </script>

</html>

