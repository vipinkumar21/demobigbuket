<?php
require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("../../library/sql-ledger.inc");
require_once("../../library/invoice_summary.inc.php");
require_once("../../library/sl_eob.inc.php");
require_once("../../library/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once ("$audting_webroot/auditlog.php");

$INTEGRATED_AR = $GLOBALS['oer_config']['ws_accounting']['enabled'] === 2;

$alertmsg = '';
$bgcolor = "#fff";
$export_patient_count = 0;
$export_dollars = 0;

$today = date("Y-m-d");

$form_date = fixDate($_POST['form_date'], "");
$form_to_date = fixDate($_POST['form_to_date'], "");
$is_ins_summary = $_POST['form_category'] == 'Ins Summary';
$is_due_ins = ($_POST['form_category'] == 'Due Ins') || $is_ins_summary;
$is_due_pt = $_POST['form_category'] == 'Due Pt';
$is_all = $_POST['form_category'] == 'All';
$is_ageby_lad = strpos($_POST['form_ageby'], 'Last') !== false;
$form_facility = $_POST['form_facility'];

if ($_POST['form_refresh'] || $_POST['form_export'] || $_POST['form_csvexport']) {
    if ($is_ins_summary) {
        $form_cb_ssn = false;
        $form_cb_dob = false;
        $form_cb_pubpid = false;
        $form_cb_adate = false;
        $form_cb_policy = false;
        $form_cb_phone = false;
        $form_cb_city = false;
        $form_cb_ins1 = false;
        $form_cb_referrer = false;
        $form_cb_idays = false;
        $form_cb_err = false;
    } else {
        $form_cb_ssn = $_POST['form_cb_ssn'] ? true : false;
        $form_cb_dob = $_POST['form_cb_dob'] ? true : false;
        $form_cb_pubpid = $_POST['form_cb_pubpid'] ? true : false;
        $form_cb_adate = $_POST['form_cb_adate'] ? true : false;
        $form_cb_policy = $_POST['form_cb_policy'] ? true : false;
        $form_cb_phone = $_POST['form_cb_phone'] ? true : false;
        $form_cb_city = $_POST['form_cb_city'] ? true : false;
        $form_cb_ins1 = $_POST['form_cb_ins1'] ? true : false;
        $form_cb_referrer = $_POST['form_cb_referrer'] ? true : false;
        $form_cb_idays = $_POST['form_cb_idays'] ? true : false;
        $form_cb_err = $_POST['form_cb_err'] ? true : false;
    }
} else {
    $form_cb_ssn = true;
    $form_cb_dob = false;
    $form_cb_pubpid = false;
    $form_cb_adate = false;
    $form_cb_policy = false;
    $form_cb_phone = true;
    $form_cb_city = false;
    $form_cb_ins1 = false;
    $form_cb_referrer = false;
    $form_cb_idays = false;
    $form_cb_err = false;
}
$form_age_cols = (int) $_POST['form_age_cols'];
$form_age_inc = (int) $_POST['form_age_inc'];
if ($form_age_cols > 0 && $form_age_cols < 50) {
    if ($form_age_inc <= 0)
        $form_age_inc = 30;
} else {
    $form_age_cols = 0;
    $form_age_inc = 0;
}

$initial_colspan = 1;
if ($is_due_ins)
    ++$initial_colspan;
if ($form_cb_ssn)
    ++$initial_colspan;
if ($form_cb_dob)
    ++$initial_colspan;
if ($form_cb_pubpid)
    ++$initial_colspan;
if ($form_cb_policy)
    ++$initial_colspan;
if ($form_cb_phone)
    ++$initial_colspan;
if ($form_cb_city)
    ++$initial_colspan;
if ($form_cb_ins1)
    ++$initial_colspan;
if ($form_cb_referrer)
    ++$initial_colspan;

$final_colspan = $form_cb_adate ? 6 : 5;

$grand_total_charges = 0;
$grand_total_adjustments = 0;
$grand_total_paid = 0;
$grand_total_agedbal = array();
for ($c = 0; $c < $form_age_cols; ++$c)
    $grand_total_agedbal[$c] = 0;

if (!$INTEGRATED_AR)
    SLConnect();

function bucks($amount) {
    if ($amount)
        echo oeFormatMoney($amount); // was printf("%.2f", $amount);
}

function nonpatientDetail($nonpatientId) {
    $sql = "SELECT npd.np_id AS patientid, npd.np_name AS firstname, npd.np_mobile AS mobile, npd.np_email AS email, pd.pubpid FROM nonpatient_data AS npd LEFT OUTER JOIN patient_data AS pd ON pd.id = npd.ep_id WHERE np_id = $nonpatientId ";
    $row = sqlQuery($sql, null, $GLOBALS['adodb']['dbreadonly']);
    return $row;
}

// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=invoice_report.csv");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php if (function_exists('html_header_show')) html_header_show(); ?>
            <link rel=stylesheet href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('Collections Report', 'e') ?></title>
            <style type="text/css">

                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }

            </style>

            <script language="JavaScript">

                function checkAll(checked) {
                    var f = document.forms[0];
                    for (var i = 0; i < f.elements.length; ++i) {
                        var ename = f.elements[i].name;
                        if (ename.indexOf('form_cb[') == 0)
                            f.elements[i].checked = checked;
                    }
                }

            </script>

        </head>

        <body class="body_top">

            <span class='title'><?php xl('Report', 'e'); ?> - <?php xl('Invoice', 'e'); ?></span>

            <form method='post' action='bill_report.php' enctype='multipart/form-data' id='theform'>

                <div id="report_parameters" style="width:100%">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_export' id='form_export' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

                    <table width="100%">
                        <tr>
                            <td width="80%">
                                <div style='float:left; width: 100%'>
                                    <table width="100%">
                                        <tr>
                                            <td width="5%" class='label'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('From', 'e'); ?>:
                                            </td>
                                            <td width="25%">
                                                <input type='text' name='form_date' id="form_from_date" size='10' value='<?php echo $form_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_from_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                            <td width="3%" class='label'>
                                                <?php xl('To', 'e'); ?>:
                                            </td>
                                            <td width="24%">
                                                <input type='text' name='form_to_date' id="form_to_date" size='10' value='<?php echo $form_to_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_to_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>	
                                            <td width="10%" class='label'>
                                                <?php xl('Due Amount', 'e'); ?>:
                                            </td>
                                            <td width="8%">
                                                <input type="checkbox" name='due_amt' value='1' <?php if($_POST['due_amt']){ echo 'checked';} ?>> 
                                            </td>					
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td width="20%" align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='css_button' onclick='$("#form_refresh").attr("value", "true");$("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php
                                                if ($_POST['form_refresh'] || $_POST['form_export'] || $_POST['form_csvexport']) {
                                                    ?>
                                                    <a href='javascript:;' class='css_button'  onclick='checkAll(true)'><span><?php xl('Select All', 'e'); ?></span></a>
                                                    <a href='javascript:;' class='css_button'  onclick='checkAll(false)'><span><?php xl('Clear All', 'e'); ?></span></a>
                                                    <a href='javascript:;' class='css_button' onclick='$("#form_csvexport").attr("value", "true");$("#form_refresh").attr("value", "");
                                                            $("#theform").submit();'>
                                                        <span><?php xl('Export Selected as CSV', 'e'); ?></span>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="text">Please input search criteria above, and click Submit to view results.</div>
                <?php
            } // end not form_csvexport

            if ($_POST['form_refresh'] || $_POST['form_export'] || $_POST['form_csvexport']) {
                $rows = array();
                $where = "";
                $searchParam = '';
                
                
                if ($form_facility) {
                    //for individual facility
                    if ($form_date) {
                        //if from date selected
                        if ($form_to_date) {
                            // if $form_date && $form_to_date
                            $toDate=date_create(date($form_to_date));
                            $fromDate=date_create($form_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;   
                            
                            
                            if($days_between_from_to<=$form_facility_time_range){
                                
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.clinic_id = '$form_facility'";
                                if ($_POST['due_amt']) {
                                    if ($where) { $where .= " AND "; }
                                    $where .= "inca.due_ammount != ''";
                                }
                            }
                            else{
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                if ($_POST['due_amt']) { $dueAmount="1"; }
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="invoice_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$form_date,"toDate"=>$form_to_date,"dueAmount"=>$dueAmount, "facility"=>$form_facility)));// changable
                                $rcsl_report_description="Invoice report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;
                                } 
                            }
                        } 
                        else {
                            //only from date: no TO date
                            $form_to_date=date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                            $toDate=date_create(date($form_to_date));
                            $fromDate=date_create($form_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;             
                            
                            if($days_between_from_to<=$form_facility_time_range){
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.clinic_id = '$form_facility'";
                                if ($_POST['due_amt']) {
                                    if ($where) { $where .= " AND "; }
                                    $where .= "inca.due_ammount != ''";
                                }
                            }
                            else{
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                if ($_POST['due_amt']) { $dueAmount="1"; }
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="invoice_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$form_date,"toDate"=>$form_to_date,"dueAmount"=>$dueAmount, "facility"=>$form_facility)));// changable
                                $rcsl_report_description="Invoice report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;
                                }                   
                            }
                        }
                    } else {
                        // if from date not selected
                        if ($form_to_date) {
                            $form_date=date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                            $toDate=date_create(date($form_to_date));
                            $fromDate=date_create($form_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;
                            
                            if($days_between_from_to<=$form_facility_time_range){
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.clinic_id = '$form_facility'";
                                if ($_POST['due_amt']) {
                                    if ($where) { $where .= " AND "; }
                                    $where .= "inca.due_ammount != ''";
                                }
                            }
                            else{
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                if ($_POST['due_amt']) { $dueAmount="1"; }
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="invoice_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$form_date,"toDate"=>$form_to_date,"dueAmount"=>$dueAmount, "facility"=>$form_facility)));// changable
                                $rcsl_report_description="Invoice report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;
                                }                    
                            }
                        } else {
                            if ($where) { $where .= " AND ";}
                            $form_to_date = date("Y-m-d");
                            $form_date=date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                            $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                            if ($where) { $where .= " AND ";}
                            $where .= "inca.clinic_id = '$form_facility'";
                            if ($_POST['due_amt']) {
                                    if ($where) { $where .= " AND "; }
                                    $where .= "inca.due_ammount != ''";
                            }
                        }
                    }
                    
                    
                    $searchParam .= 'From Date = '.$form_date.' | To Date = '.$form_to_date.' | ClinicId = '.$form_facility.' | Due Amount = '.(($_POST['due_amt'])?'Yes':'No').' |'; /// Auditing Section Param
                }
                else {
                    //for all facility
                    if ($form_date) {
                        //if from date selected
                        if ($form_to_date) {
                            // if $form_date && $form_to_date
                            $toDate=date_create(date($form_to_date));
                            $fromDate=date_create($form_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;   
                            
                            if($days_between_from_to<=$form_facility_all_time_range){
                                
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($_POST['due_amt']) {
                                if ($where) { $where .= " AND "; }
                                $where .= "inca.due_ammount != ''";
                                }
                            }
                            else{
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                if ($_POST['due_amt']) { $dueAmount="1"; }
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="invoice_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$form_date,"toDate"=>$form_to_date,"dueAmount"=>$dueAmount)));// changable
                                $rcsl_report_description="Invoice report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;
                                }
                            }
                        } 
                        else {
                            //only from date: no TO date
                            $form_to_date=date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                            $toDate=date_create(date($form_to_date));
                            $fromDate=date_create($form_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;             
                            
                            if($days_between_from_to<=$form_facility_all_time_range){
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($_POST['due_amt']) {
                                if ($where) { $where .= " AND "; }
                                $where .= "inca.due_ammount != ''";
                                }
                            }
                            else{
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                if ($_POST['due_amt']) { $dueAmount="1"; }
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="invoice_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$form_date,"toDate"=>$form_to_date,"dueAmount"=>$dueAmount)));// changable
                                $rcsl_report_description="Invoice report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;
                                }
                            }
                        }
                    } 
                    else {
                        // if from date not selected
                        if ($form_to_date) {
                            $form_date=date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                            $toDate=date_create(date($form_to_date));
                            $fromDate=date_create($form_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;
                            
                            if($days_between_from_to<=$form_facility_all_time_range){
                                if ($where) { $where .= " AND ";}
                                $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($_POST['due_amt']) {
                                if ($where) { $where .= " AND "; }
                                $where .= "inca.due_ammount != ''";
                                }
                            }
                            else{
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                if ($_POST['due_amt']) { $dueAmount="1"; }
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="invoice_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$form_date,"toDate"=>$form_to_date,"dueAmount"=>$dueAmount)));// changable
                                $rcsl_report_description="Invoice report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;
                                }
                            }
                        } else {
                            if ($where) { $where .= " AND ";}
                            $form_to_date = date("Y-m-d");
                            $form_date=date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                            $where .= "inca.inv_date  BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                            if ($_POST['due_amt']) {
                                if ($where) { $where .= " AND "; }
                                $where .= "inca.due_ammount != ''";
                            }
                        }
                    }
                  $searchParam .= 'From Date = '.$form_date.' | To Date = '.$form_to_date.' | Clinic =  All | Due Amount = '.(($_POST['due_amt'])?'Yes':'No').' |'; /// Auditing Section Param  
                }

                //invoice calculation: with table invoice_calculated
                $query = "SELECT inca.inv_date,inca.inv_parent_invoice,inca.invoice_id,inca.patient_pub_id,inca.patient_name,inca.treatment_prod_name,inca.cost,inca.disc_cost,inca.net_cost,inca.due_ammount,
inca.patient_id,inca.clinic_id,inca.inv_id from invoice_calculated AS inca where " . $where;    
                $query .= " ORDER BY inv_date DESC";
                
                if (!empty($_POST['form_refresh'])) {
                    $event = "Report Invoice View";
                } elseif (!empty($_POST['form_csvexport'])) {
                    $event = "Report Invoice Export";
                }
                
                if ($msgForReportLog) {
            
                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam,2); // Cron Schedule Report Auditing Section

                } else {

                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                }
                
                
                 $eres = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);
                


                if ($_POST['form_export']) {
                    echo "<textarea rows='35' cols='100' readonly>";
                } else if ($_POST['form_csvexport']) {
                    // CSV headers:
                    if (true) {
                        echo '"S. No.",';
                        echo '"Date",';
                        echo '"Invoice",';
                        echo '"Patient ID",';
                        echo '"Patient Name",';
                        echo '" Treatments done",';
                        echo '" Parent  Invoice",';
                        echo '"Cost",';
                        echo '"Discount",';
                        echo '"Net Amount "' . "\n";
                    }
                } else {
                    ?>

                    <div id="report_results">
                        <table>
                            <?php 
                            if($msgForReportLog) {
                                ?>
                                <thead>
                                <th><div align="center"><?php echo $msgForReportLog; ?></div></th>
                                </thead>
                            <?php
                            }
                            
                            else{
                            ?>  
                                <thead>
                                <th>&nbsp;<?php xl('S. No.', 'e') ?></th>
                                <th>&nbsp;<?php xl('Date', 'e') ?></th>
                                <th>&nbsp;<?php xl('Invoice#', 'e') ?></th>
                                <th>&nbsp;<?php xl('Patient ID', 'e') ?></th>
                                <th>&nbsp;<?php xl('Patient Name', 'e') ?></th>
                                <th>&nbsp;<?php xl('Treatment/Product Name', 'e') ?></th>
                                <th>&nbsp;<?php xl('Parent  Invoice#', 'e') ?></th>
                                <th align="right"><?php xl('Cost', 'e') ?>&nbsp;</th>
                                <th align="right"><?php xl('Discount', 'e') ?>&nbsp;</th>
                                <th align="right"><?php xl('Net Cost', 'e') ?>&nbsp;</th>
                                <th>&nbsp;</th>
                                </thead>
                            <?php } ?>
                            <?php
                        } // end not export

                        $ptrow = array('insname' => '', 'pid' => 0);
                        $orow = -1;
                        $countRow = 1;
                        /*
                        foreach ($rows as $key => $row) {
                            list($insname, $ptname, $trash) = explode('|', $key);
                            list($pid, $encounter) = explode(".", $row['invnumber']);
                            */
                        while ($erow = sqlFetchArray($eres)) {
                                $row['id'] = $erow['invoice_id'];
                                $row['invnumber'] = $erow['invoice_id'];
                                $row['custid'] = $erow['patient_pub_id'];
                                $row['name'] = $erow['patient_name'];
                                $row['dos'] = $erow['inv_date'];
                                $row['pid'] = $erow['patient_pub_id'];
                                $row['pubpid'] = $erow['patient_pub_id'];
                                $row['irnumber'] = $erow['invoice_id'];
                                $row['charges'] = $erow['cost'];
                                $row['treatments'] = $erow['treatment_prod_name'];
                                $row['inv_parent_invoice'] = $erow['inv_parent_invoice'];
                                $row['adjustments'] = $erow['disc_cost'];
                                $row['paid'] = $erow['net_cost'];
                                $row['patientId'] = $erow['patient_id'];
                                $row['clinicId'] = $erow['clinic_id'];
                                $row['invoiceId'] = $erow['inv_id'];
                            if (!$is_ins_summary && !$_POST['form_export'] && !$_POST['form_csvexport']) {
                                $in_collections = stristr($row['billnote'], 'IN COLLECTIONS') !== false;
                                ?>
                                <tr>
                                    <td class="detail">
                                        &nbsp;<?php echo $countRow; ?>
                                    </td>
                                    <td class="detail">
                                        &nbsp;<?php echo $row['dos']; ?>
                                    </td>
                                    <td class="detail">
                                        <a href="#" onclick='window.open("<?php echo $GLOBALS['OE_FRENT_SITES_URL']; ?>invoicePrint.php?patientid=<?php echo base64_encode(base64_encode($row['patientId'])); ?>&clinicid=<?php echo base64_encode(base64_encode($row['clinicId'])); ?>&invid=<?php echo base64_encode(base64_encode($row['invoiceId'])); ?>");'>
                                            &nbsp;<?php echo empty($row['irnumber']) ? $row['invnumber'] : $row['irnumber']; ?>
                                        </a></td>
                                    <?php
                                    //if ($ptrow['count'] == 1) {
                                    echo "  <td class='detail'>&nbsp;" . $row['pubpid'] . "</td>\n";
                                    echo "  <td class='detail'>&nbsp;" . $row['name'] . "</td>\n";
                                    echo "  <td class='detail'>" . str_replace('\n', '<br>', $row['treatments']);
                                    echo "&nbsp;</td>\n";
                                    ?>
                                    <td class="detail" align="right">
                                        <?php 
                                        echo $row['inv_parent_invoice'];
                                        
                                        ?>&nbsp;
                                    </td>            

                                    <td class="detail" align="right">
                                        <?php echo text(number_format($row['charges'], 2)); ?>&nbsp;
                                    </td>
                                    <td class="detail" align="right">
                                        <?php echo text(number_format($row['adjustments'], 2)); ?>&nbsp;
                                    </td>
                                    <td class="detail" align="right">
                                        <?php echo text(number_format($row['charges'] - $row['adjustments'], 2)); ?>&nbsp;
                                    </td>

                                    <td class="detail" align="center">
                                    <?php
                                    echo "   <input type='checkbox' name='form_cb[" . $row['custid'] . "]' />\n";
                                    ?>
                                    </td>
                                <?php
                                if ($form_cb_err) {
                                    echo "  <td class='detail'>&nbsp;";
                                    echo $row['billing_errmsg'] . "</td>\n";
                                }
                                ?>
                                </tr>
                                <?php
                            } // end not export and not insurance summary
                            else if ($_POST['form_csvexport']) {
                                // The CSV detail line is written here.
                                $balance = $row['charges'] + $row['adjustments'] - $row['paid'];
                                $netAmmout = $row['charges'] - $row['adjustments'];
                                
                                
                                echo '"' . $countRow . '",';
                                echo '"' . $row['dos'] . '",';
                                echo '"' . (empty($row['irnumber']) ? $row['invnumber'] : $row['irnumber']) . '",';
                                echo '"' . $row['pubpid'] . '",';
                                echo '"' . $row['name'] . '",';
                                echo '"';
                                echo trim(str_replace('\n', ',', $row['treatments']), ',');
                                echo '",';
                                echo '"' . $row['inv_parent_invoice'] . '",';
                                echo '"' . oeFormatMoney($row['charges']) . '",';
                                echo '"' . oeFormatMoney($row['adjustments']) . '",';
                                echo '"' . oeFormatMoney($netAmmout) . '"' . "\n";
                            } // end $form_csvexport
                            $countRow++;
                        } // end loop

                         if (!$msgForReportLog) {
                           debugADOReportsUpdate($auditid,$GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                         }
                        if ($_POST['form_export']) {
                            echo "</textarea>\n";
                            $alertmsg .= "$export_patient_count patients with total of " .
                                    oeFormatMoney($export_dollars) . " have been exported ";
                            if ($_POST['form_without']) {
                                $alertmsg .= "but NOT flagged as in collections.";
                            } else {
                                $alertmsg .= "AND flagged as in collections.";
                            }
                        } else if ($_POST['form_csvexport']) {
                            
                        } else {
                            echo "</table>\n";
                            echo "</div>\n";
                        }
                    } // end if form_refresh

                    if (!$INTEGRATED_AR)
                        SLClose();

                    if (!$_POST['form_csvexport']) {
                        if (!$_POST['form_export']) {
                            ?>

                            <div style='margin-top:5px'></div>
        <?php
    } // end not export
    ?>
                        </form>
                        </center>
                        <script language="JavaScript">
    <?php
    if ($alertmsg) {
        echo "alert('" . htmlentities($alertmsg) . "');\n";
    }
    ?>
                        </script>
                        </body>
                        <!-- stuff for the popup calendar -->
                        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
                        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
                        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
                        <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField:"form_from_date"
                                ,ifFormat:"%Y-%m-%d"
                                ,button:"img_from_date"
                                ,onUpdate: function() {
                                    var   toDate = document.getElementById('form_to_date').value,
                                      fromDate = document.getElementById('form_from_date').value;

                                    if(toDate){
                                       var   toDate = new Date(toDate),
                                             fromDate = new Date(fromDate);

                                       if(fromDate > toDate){
                                          alert('From date cannot be later than To date.');
                                          document.getElementById('form_from_date').value = '';
                                       }
                                    }
                                }
                            });
                            Calendar.setup({
                                inputField:"form_to_date"
                                ,ifFormat:"%Y-%m-%d"
                                ,button:"img_to_date"
                                ,onUpdate: function() {
                                    var   toDate = document.getElementById('form_to_date').value,
                                      fromDate = document.getElementById('form_from_date').value;

                                    if(fromDate){
                                       var   toDate = new Date(toDate),
                                             fromDate = new Date(fromDate);

                                       if(fromDate > toDate){
                                          alert('From date cannot be later than To date.');
                                          document.getElementById('form_to_date').value = '';
                                       }
                                    }
                                }
                            });
                        </script>
                        </html>
    <?php
} // end not form_csvexport
?>
