<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report lists front office receipts for a given date range.

require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once ("$audting_webroot/auditlog.php");

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

function nonpatientDetail($nonpatientId) {
    $sql = "SELECT npd.np_id AS patientid, npd.np_name AS firstname, npd.np_mobile AS mobile, npd.np_email AS email, pd.pubpid FROM nonpatient_data AS npd LEFT OUTER JOIN patient_data AS pd ON pd.id = npd.ep_id WHERE np_id = $nonpatientId ";
    $row = sqlQuery($sql);
    return $row;
}

$from_date = fixDate($_POST['form_from_date'], date('Y-m-d'));
$to_date = fixDate($_POST['form_to_date'], date('Y-m-d'));

function bucks($amt) {
    return ($amt != 0.00) ? oeFormatMoney($amt) : '';
}

// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=payment_report.csv");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Front Office Receipts', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

            <script language="JavaScript">

    <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

                // The OnClick handler for receipt display.
                function show_receipt(pid, timestamp, encounter) {
                    dlgopen('../patient_file/front_payment.php?receipt=1&encounter=' + encounter + '&patient=' + pid +
                            '&time=' + timestamp, '_blank', 550, 400);
                }

            </script>

            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <span class='title'><?php xl('Report', 'e'); ?> - <?php xl('Payment', 'e'); ?></span>

            <div id="report_parameters_daterange">
                <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form name='theform' method='post' action='payment_report.php' id='theform'>

                <div id="report_parameters">
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='650px'>
                                <div style='float:left; width:100%;'>

                                    <table class='text'>
                                        <tr>
                                            <td class='label'>
                                                <?php xl('Payment Method', 'e'); ?>:
                                            </td>
                                            <td>
                                                <select id="form_method" class="text" name="form_method">
                                                    <option value="">Payment Method</option>
                                                    <option value="Check" <?php
                                                    if ($_POST['form_method'] == 'Check') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Check Payment</option>
                                                    <option value="Cash" <?php
                                                    if ($_POST['form_method'] == 'Cash') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Cash</option>
                                                    <option value="Card" <?php
                                                        if ($_POST['form_method'] == 'Card') {
                                                            echo 'selected';
                                                        }
                                                    ?>>Card</option>
                                                </select>

                                            </td>
                                            <td class='label'>
    <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td>
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class='label'>
    <?php xl('From', 'e'); ?>:
                                            </td>
                                            <td width="40%">
                                                <input type='text' name='form_from_date' id="form_from_date" size='10' value='<?php echo $form_from_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_from_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                            <td class='label'>
    <?php xl('To', 'e'); ?>:
                                            </td>
                                            <td width="40%">
                                                <input type='text' name='form_to_date' id="form_to_date" size='10' value='<?php echo $form_to_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_to_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                        </tr>

                                    </table>

                                </div>

                            </td>
                            <td align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='css_button' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <a href='#' class='css_button' onclick='$("#form_csvexport").attr("value", "true");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php if ($_POST['form_refresh']) { ?>
                                                    <a href='#' class='css_button' onclick='window.print()'>
                                                        <span>
        <?php xl('Print', 'e'); ?>
                                                        </span>
                                                    </a>
    <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport
            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport']) {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Date') . '",';
                    echo '"' . xl('Receipt Number') . '",';
                    echo '"' . xl('Bill Number') . '",';
                    echo '"' . xl('Patient ID') . '",';
                    echo '"' . xl('Patient Name') . '",';
                    echo '"' . xl('Mode of Payment') . '",';
                    echo '"' . xl('Amount Received Earlier') . '",';
                    echo '"' . xl('Amount Received Today') . '",';
                    echo '"' . xl('Due Amount') . '"' . "\n";
                } else {
                    ?>
                    <div id="report_results">
                        <table>
                            <thead>
                            <th> <?php xl('S. No.', 'e'); ?> </th>
                            <th> <?php xl('Date', 'e'); ?> </th>
                            <th> <?php xl('Receipt Number', 'e'); ?> </th>
                            <th> <?php xl('Bill Number', 'e'); ?> </th>
                            <th> <?php xl('Patient ID', 'e'); ?> </th>
                            <th> <?php xl('Patient Name', 'e'); ?> </th>
                            <th> <?php xl('Mode of Payment', 'e'); ?> </th>
                            <th> <?php xl('Amount Received Earlier', 'e'); ?> </th>
                            <th align='right'> <?php xl('Amount Received Today', 'e'); ?> </th>
                            <th align='right'> <?php xl('Due Amount', 'e'); ?> </th>
                            </thead>
                            <tbody>
                                <?php
                            }
                            if (true || $_POST['form_refresh']) {
                                $total1 = 0.00;
                                $total2 = 0.00;
                                $netAmountTotal = 0.00;
                                $query = "SELECT r.*, " .
                                        "r.rect_number AS reciept_no, " .
                                        "p.fname, p.mname, p.lname, p.pid, p.pubpid, inv.inv_id, inv.inv_number " .
                                        "FROM reciept AS r " .
                                        "LEFT OUTER JOIN patient_data AS p ON " .
                                        "p.id = r.rect_pid " .
                                        "LEFT OUTER JOIN invoice_reciepts AS ir ON ir.invrect_rect_id = r.rect_id " .
                                        "LEFT OUTER JOIN invoice AS inv ON inv.inv_id = ir.invrect_inv_id " .
                                        "WHERE ";
                                $conditions = 'r.rect_deletestate = 1';
                                if (isset($_POST['form_method']) && !empty($_POST['form_method'])) {
                                    if (!empty($conditions)) {
                                        $conditions.=" AND r.rect_mode='" . $_POST['form_method'] . "'";
                                    } else {
                                        $conditions = " r.rect_mode='" . $_POST['form_method'] . "'";
                                    }
                                }

                                if (isset($_POST['form_from_date']) && isset($_POST['form_to_date']) && !empty($_POST['form_from_date']) && !empty($_POST['form_to_date'])) {
                                    if (!empty($conditions)) {
                                        $conditions.=" AND r.rect_created_date >= '$from_date 00:00:00' AND r.rect_created_date <= '$to_date 23:59:59' ";
                                    } else {
                                        $conditions = "  r.rect_created_date >= '$from_date 00:00:00' AND r.rect_created_date <= '$to_date 23:59:59' ";
                                    }
                                }
                                if (isset($_POST['form_facility']) && !empty($_POST['form_facility'])) {
                                    if (!empty($conditions)) {
                                        $conditions.=" AND r.rect_clinic_id = '" . $_POST['form_facility'] . "' ";
                                    } else {
                                        $conditions = "  r.rect_clinic_id = '" . $_POST['form_facility'] . "' ";
                                    }
                                }
                                if (empty($conditions)) {
                                    $conditions = "  r.rect_created_date >= '$from_date 00:00:00' AND r.rect_created_date <= '$to_date 23:59:59' ";
                                }
                                $query .=$conditions . " ORDER BY r.rect_created_date desc, r.rect_id desc";

                                if (!empty($_POST['form_refresh'])) {
                                    $event = "Report - Payment View";
                                } elseif (!empty($_POST['form_csvexport'])) {
                                    $event = "Report - Payment Export";
                                }
                                debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser']);

                                $res = sqlStatement($query);
                                $count = 1;
                                while ($row = sqlFetchArray($res)) {
                                    $erAmount = 0.00;
                                    $dueAmount = 0.00;
                                    if (!empty($row['inv_id'])) {
                                        $tpQuery = "SELECT invit.*, bill.code_type, bill.code, bill.code_text, bill.units, bill.fee, bill.discount, bill.discount_type, pri.pr_price  
                                    FROM invoice_items AS invit INNER JOIN billing AS bill ON bill.id = invit.invit_tp_id INNER JOIN codes AS co ON co.code=bill.code INNER JOIN prices AS pri ON pri.pr_id=co.id WHERE invit.invit_inv_id = " . $row['inv_id'] . "  AND invit.invit_deleted = 0 GROUP BY invit.invit_tp_id";
                                        $tpRes = sqlStatement($tpQuery);
                                        $invoiceCost = 0;
                                        $invoiceDiscount = 0;
                                        while ($tpRow = sqlFetchArray($tpRes)) {
                                            $invoiceCost += $tpRow['pr_price'] * $tpRow['units'];
                                            if ($tpRow['discount_type'] == 'Amt') {
                                                $invoiceDiscount += $tpRow['discount'];
                                            } else {
                                                $invoiceDiscount += (($tpRow['pr_price'] * $tpRow['units']) / 100) * $tpRow['discount'];
                                            }
                                        }
                                        $sql = "SELECT i.inv_id, i.inv_number, i.inv_deletestate, i.inv_created_date, ii.invit_deleted, i.inv_clinic_id, ps.ps_discount, ROUND(ps.ps_discount) AS rounddiscount, ps.ps_discount_type, ps.ps_fee, ROUND(ps.ps_fee) AS roundfee, ps.ps_quantity, ii.invit_deleted, ps.ps_npid, ps.ps_clinic_id, invis.invist_price, invis.invist_quantity, invim.inv_im_name, ps.ps_deletestate ";
                                        $sql .= "FROM invoice AS i ";
                                        $sql .= "LEFT JOIN invoice_items AS ii ON ii.invit_inv_id = i.inv_id ";
                                        $sql .= "LEFT JOIN product_sales AS ps ON ii.invit_ps_id = ps.ps_id ";
                                        $sql .= "LEFT JOIN inv_item_stock AS invis ON ps.ps_stockid = invis.invist_id ";
                                        $sql .= "LEFT JOIN inv_item_master AS invim ON invim.inv_im_id = invis.invist_itemid ";
                                        $sql .= "WHERE i.inv_id = " . $row['inv_id'] . " AND (ii.invit_tp_id = 0 OR ii.invit_tp_id IS NULL) AND ii.invit_deleted=0 ";
                                        $proRes = sqlStatement($sql);
                                        while ($proRow = sqlFetchArray($proRes)) {
                                            if ($proRow['ps_discount_type'] == 'amt') {
                                                $invoiceDiscount += $proRow['ps_discount'];
                                            } else {
                                                $invoiceDiscount += (($proRow['invist_price'] * $proRow['ps_quantity']) / 100) * $proRow['ps_discount'];
                                            }
                                            $invoiceCost += $proRow['ps_fee'] + $invoiceDiscount;
                                        }
                                        $finalInvoiceAmount = $invoiceCost - $invoiceDiscount;
                                        $eraQuery = "SELECT SUM(rect.rect_amount) AS earlierAmount FROM invoice_reciepts AS ir INNER JOIN reciept AS rect ON rect.rect_id = ir.invrect_rect_id WHERE ir.invrect_rect_id < " . $row['rect_id'] . " AND ir.invrect_inv_id = " . $row['inv_id'];
                                        $eraRes = sqlStatement($eraQuery);
                                        $eraRow = sqlFetchArray($eraRes);
                                        $erAmount = 0.00;
                                        if (!empty($eraRow)) {
                                            if (!empty($eraRow['earlierAmount'])) {
                                                $erAmount = $eraRow['earlierAmount'];
                                            }
                                        }
                                        $dueAmount = $finalInvoiceAmount - ($erAmount + $row['rect_amount']);
                                    }
                                    if (empty($row['pubpid']) && !empty($row['rect_npid'])) {
                                        //PATIENT DETAIL			
                                        $patientProfileWidgetList = nonPatientDetail($row['rect_npid']);
                                        if (!empty($patientProfileWidgetList['pubpid'])) {
                                            $row['pubpid'] = $patientProfileWidgetList['pubpid'];
                                        } else {
                                            $row['pubpid'] = 'NA';
                                        }
                                        $row['fname'] = $patientProfileWidgetList['firstname'];
                                    }
                                    if (empty($_POST['searchparm']) || (isset($_POST['searchparm']) && !empty($_POST['searchparm']) && $_POST['searchparm'] == $dueAmount)) {
                                        // Make the timestamp URL-friendly.
                                        $timestamp = preg_replace('/[^0-9]/', '', $row['rect_created_date']);
                                        if ($_POST['form_csvexport']) {
                                            echo '"' . qescape($count) . '",';
                                            echo '"' . qescape(oeFormatShortDate(substr($row['rect_created_date'], 0, 10)) . substr($row['rect_created_date'], 10, 6)) . '",';
                                            echo '"' . qescape($row['reciept_no']) . '",';
                                            echo '"' . qescape($row['inv_number']) . '",';
                                            echo '"' . qescape($row['pubpid']) . '",';
                                            echo '"' . qescape($row['fname'] . ' ' . $row['lname']) . '",';
                                            echo '"' . qescape($row['rect_mode']) . '",';
                                            echo '"' . qescape(number_format($erAmount, 2)) . '",';
                                            echo '"' . qescape(number_format($row['rect_amount'], 2)) . '",';
                                            echo '"' . qescape(number_format($dueAmount, 2)) . '"' . "\n";
                                        } else {
                                            ?>
                                            <tr>
                                                <td nowrap>
                                                        <?php echo $count; ?>
                                                </td>
                                                <td nowrap>
                                                    <?php echo oeFormatShortDate(substr($row['rect_created_date'], 0, 10)) . substr($row['rect_created_date'], 10, 6); ?>
                                                </td>
                                                <td nowrap><a href="#" onclick='window.open("http://<?php echo $GLOBALS['_SERVER']['HTTP_HOST']; ?>/api/receiptPrint.php?patientid=<?php echo base64_encode(base64_encode($row['rect_pid'])); ?>&clinicid=<?php echo base64_encode(base64_encode($row['rect_clinic_id'])); ?>&receiptid=<?php echo base64_encode(base64_encode($row['rect_id'])); ?>");'>
                                                    <?php echo $row['reciept_no']; ?>
                                                    </a></td>
                                                <td>
                                                    <?php echo $row['inv_number']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['pubpid']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['fname'] . ' ' . $row['lname']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['rect_mode']; ?>
                                                </td>
                                                <td>
                                                    <?php echo text(number_format($erAmount, 2)); ?>
                                                </td>
                                                <td align='right'>
                                                    <?php echo text(number_format($row['rect_amount'], 2)); ?>
                                                </td>

                                                <td align='right'>
                                                    <?php
                                                    if ($dueAmount > 0) {
                                                        echo text(number_format($dueAmount, 2));
                                                    } else {
                                                        echo '0.00';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        $count++;
                                    }
                                }
                            }
                            if (!$_POST['form_csvexport']) {
                                ?>
                            </tbody>
                        </table>
                    </div> <!-- end of results -->
                    <?php
                }
            } else {
                ?>
                <div class='text'>
    <?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>
    <?php
}
if (!$_POST['form_csvexport']) {
    ?>

            </form>
        </body>
        <!-- stuff for the popup calendar -->
        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
        <script language="Javascript">
                                    Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
                                    Calendar.setup({inputField: "form_to_date", ifFormat: "%Y-%m-%d", button: "img_to_date"});
        </script>
    </html>
    <?php
}
?>