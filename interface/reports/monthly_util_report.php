<?php
// Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report lists patients that were seen within a given date
// range, or all patients if no date range is entered.

require_once("../globals.php");
require_once($GLOBALS['fileroot'] . "/library/acl.inc");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}
$month = $_POST['month'];
if (empty($month))
    $month = date('n', time());

$form_facility = empty($_POST['form_facility']) ? 0 : intval($_POST['form_facility']);
$form_region = empty($_POST['form_region']) ? 0 : intval($_POST['form_region']);
$form_zone = empty($_POST['form_zone']) ? 0 : intval($_POST['form_zone']);
$form_year = empty($_POST['form_year']) ? 0 : intval($_POST['form_year']);
$searchParam = '';
$fileName = "CapacityUtilizationMonthlyReport_" . date("Ymd_his") . ".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Patient List', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>

            <script language="JavaScript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
            </script>
            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">

                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                        margin-bottom: 10px;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                    #report_results {
                        width: 100%;
                    }
                }

            </style>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#form_region").change(function () {
                        changeRegion();
                    });
                    $("#form_zone").change(function () {
                        changeZone();
                    });
                    var selectedRegionAjax = '<?php echo $form_region; ?>';
                    var selectedZoneAjax = '<?php echo $form_zone; ?>';
                    var selectedFacilityAjax = '<?php echo $form_facility; ?>';
                    if (selectedRegionAjax == '' && selectedZoneAjax != '') {
                        changeZone();
                    } else if (selectedRegionAjax != '' && selectedZoneAjax == '')
                    {
                        changeRegion();
                    } else if (selectedRegionAjax != '' && selectedZoneAjax != '') {
                        changeZone();
                    }
                    function changeRegion()
                    {
                        var regionId = $("#form_region").val();
                        $.ajax({
                            url: "get_facility_zones.php?regionId=" + regionId + "&facilityId=" + selectedFacilityAjax,
                            success: function (result) {
                                var result = $.parseJSON(result);
                                $("#form_zone").html(result[0]);
                                $("#form_facility").html(result[1]);
                            }
                        });
                    }
                    function changeZone()
                    {
                        var zoneId = $("#form_zone").val();
                        var regionId = $("#form_region").val();
                        $.ajax({
                            url: "get_facility_by_zones.php?zoneId=" + zoneId + "&facilityId=" + selectedFacilityAjax + "&regionId=" + regionId,
                            success: function (result) {
                                var result = $.parseJSON(result);
                                $("#form_zone").html(result[0]);
                                $("#form_facility").html(result[1]);
                            }
                        });
                    }
                });

            </script>
        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <h3 class='emrh3'><?php xl('Capacity Utilization - Monthly Report', 'e'); ?></h3>

            <div id="report_parameters_daterange">
                <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form name='theform' id='theform' method='post' action='monthly_util_report.php' class='emrform topnopad'>

                <div id="report_parameters">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width: 100%;'>

                                    <table class='text'>
                                        <tr>
                                            <td width="5%" class='label'>
                                                <?php xl('Region', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdownRegions(strip_escape_custom($form_region), 'form_region'); ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('Zone', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php
                                                dropdownZones(strip_escape_custom($form_zone), 'form_zone');
                                                ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="5%" class='label'>
                                                <?php xl('Year', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdown_year(strip_escape_custom($form_year), 'form_year'); ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('Month', 'e'); ?>:
                                            </td>
                                            <td width="25%">
                                                <?php dropdown_months(strip_escape_custom($month), 'month') ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </td>
                            <td width="20%" align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>  
                                                <?php if (acl_check('admin', 'monthlyutilization_export_report')) { ?>
                                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "true");
                                                            $("#form_refresh").attr("value", "");
                                                            $("#theform").submit();'>
                                                        <span>
                                                            <?php xl('Export to CSV', 'e'); ?>
                                                        </span>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport

            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {

                if ($_POST['form_csvexport']) {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Clinic Name') . '",';
                    echo '"' . xl('Month') . '",';
                    echo '"' . xl('Year') . '",';
                    echo '"' . xl('Appointments') . '",';
                    echo '"' . xl('Total Capacity') . '",';
                    echo '"' . xl('Capacity Utilization') . '"' . "\n";
                } else {
                    ?>

                    <div id="report_results">
                        <table class='emrtable'>
                            <thead>
                            <th> <?php xl('S. No.', 'e'); ?> </th>
                            <th> <?php xl('Clinic Name', 'e'); ?> </th>
                            <th> <?php xl('Month', 'e'); ?> </th>
                            <th> <?php xl('Year', 'e'); ?> </th>
                            <th> <?php xl('Appointments', 'e'); ?> </th>
                            <th> <?php xl('Total Capacity', 'e'); ?> </th>
                            <th> <?php xl('Capacity Utilization', 'e'); ?> </th>
                            </thead>
                            <tbody>
                                <?php
                            } // end not export

                            if (!empty($_POST['form_refresh'])) {
                                $event = "Report Monthly Util View";
                            } elseif (!empty($_POST['form_csvexport'])) {
                                $event = "Report Monthly Util Export";
                            }

                            if ($form_facility) {
                                $srCount = 1;
                                if ($form_year) {
                                    $query = "SELECT f.id, f.name, f.operatory, f.start_hour, f.end_hour " .
                                            "FROM facility AS f ";
                                    if ($form_facility) {
                                        $query .= " WHERE f.id = '" . $form_facility . "' ";
                                    }
                                    if ($form_region) {
                                        $query .= " AND f.region_id = '" . $form_region . "' ";
                                    }
                                    if ($form_zone) {
                                        $query .= " AND f.zone_id = '" . $form_zone . "' ";
                                    }
                                    $query .=
                                            "ORDER BY f.id DESC";
                                    $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);
                                    while ($row = sqlFetchArray($res)) {

                                        /**
                                         *   Functionality to calculate total number of work off day of a given facility
                                         * @author  :- Brajraj Agrawal
                                         * @Date    :- 17-Dec-2015
                                         * @email   :- brajraj.agarwal@instantsys.com
                                         */
                                        $workOff = "SELECT work_off_day FROM facility WHERE weekly_off_enabled='1' AND id='" . $row['id'] . "'";
                                        $workoffDay = sqlFetchArray(sqlStatement($workOff));
                                        $dayNum = $workoffDay['work_off_day'];
                                        //switch-case is used to convert week day Mon from 0 to 1 ,as of standard (Sunday-0, Saturday-6)  
                                        switch ($dayNum) {
                                            case 0:
                                                $dayNum = 1;
                                                break;
                                            case 1:
                                                $dayNum = 2;
                                                break;
                                            case 2:
                                                $dayNum = 3;
                                                break;
                                            case 3:
                                                $dayNum = 4;
                                                break;
                                            case 4:
                                                $dayNum = 5;
                                                break;
                                            case 5:
                                                $dayNum = 6;
                                                break;
                                            case 6:
                                                $dayNum = 0;
                                                break;
                                            default:
                                                break;
                                        }
                                        // Calculate total number of weekdays
                                        $totalDays = date("t", mktime(0, 0, 0, $_POST['month'], 1, $form_year));
                                        $weekdays = 0;
                                        for ($w = 1; $w <= $totalDays; $w++) {
                                            $wd = date("w", mktime(0, 0, 0, $_POST['month'], $w, $form_year));
                                            if ($wd == $dayNum) {
                                                $weekdays++;
                                            }
                                        }
                                        /* ################ End of Week Day Calculate Functionality ############# */

                                        $sql = "SELECT COUNT(fhol_id) As numHoliday FROM facility_holidays WHERE  fhol_facility_id = '" . $row['id'] . "' AND fhol_is_deleted = 0";
                                        $aquery = "SELECT COUNT(pc_eid) AS numApp FROM openemr_postcalendar_events WHERE pc_delete = 0 AND  pc_facility = '" . $row['id'] . "' ";
                                        if (!empty($_POST['month'])) {
                                            $aquery .= " AND (MONTH(pc_eventDate) = '" . $_POST['month'] . "' AND YEAR(pc_eventDate) = '" . $form_year . "')";
                                            $sql.=" AND fhol_month = '" . $_POST['month'] . "' AND fhol_year = '" . $form_year . "'";
                                        }
                                        $eventQuery = $aquery . " " . "AND pc_alldayevent ='1'" . "AND pc_aid='-1'";
                                        $fulldayeventCount = sqlFetchArray(sqlStatement($eventQuery, null, $GLOBALS['adodb']['dbreport']));
                                        $holidaycount = sqlFetchArray(sqlStatement($sql, null, $GLOBALS['adodb']['dbreport'])); // Used to calculate numbe rof holidays (Modified by Brajraj Agrawal)
                                        $aRow = sqlFetchArray(sqlStatement($aquery, null, $GLOBALS['adodb']['dbreport']));
                                        $appointments = 0;
                                        if (!empty($aRow['numApp'])) {
                                            $appointments = $aRow['numApp'];
                                        }
                                        $month = date('F');
                                        $d = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
                                        $eventDuration = "SELECT SUM(pc_duration) AS duration FROM openemr_postcalendar_events WHERE  pc_aid='-1' AND pc_facility='" . $form_facility . "' AND pc_delete='0'";
                                        if (!empty($_POST['month'])) {
                                            $day = date('l', strtotime($from_date));
                                            $month = date('F', strtotime('1-' . $_POST['month'] . '-' . $form_year));
                                            $d = cal_days_in_month(CAL_GREGORIAN, $_POST['month'], $form_year);
                                            $eventDuration.=" AND pc_alldayevent ='0' AND MONTH(pc_eventDate) = '" . $_POST['month'] . "' AND YEAR(pc_eventDate) = '" . $form_year . "'";
                                        }
                                        $edRow = sqlFetchArray(sqlStatement($eventDuration, null, $GLOBALS['adodb']['dbreport']));
                                        if ($edRow['duration'] != '') { //Checks if the duration is not NULL
                                            $duration = $edRow['duration'] / (60 * 60); //No. of duration is in seconds and here we are converting it to number of hours.
                                            $numSlots = ceil($row['operatory'] * (($row['end_hour'] - $row['start_hour']) - $duration) * (60 / $GLOBALS['calendar_interval']));
                                        } else {

                                            $numSlots = ($row['operatory'] * ($row['end_hour'] - $row['start_hour']) * (60 / $GLOBALS['calendar_interval']));
                                        }
                                        $totalCapacity = ($d - $holidaycount['numHoliday'] - $fulldayeventCount['numApp'] - $weekdays) * $numSlots;
                                        $capacityUtill = 0;
                                        if ($appointments) {
                                            $capacityUtill = ceil(($appointments / $totalCapacity) * 100);
                                        }
                                        if ($_POST['form_csvexport']) {
                                            echo '"' . qescape($srCount) . '",';
                                            echo '"' . qescape($row['name']) . '",';
                                            echo '"' . qescape($month) . '",';
                                            echo '"' . qescape($form_year) . '",';
                                            echo '"' . qescape($appointments) . '",';
                                            echo '"' . qescape($totalCapacity) . '",';
                                            echo '"' . qescape($capacityUtill . "%") . '"' . "\n";
                                        } else {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $srCount; ?>
                                                </td>
                                                <td>
                                                    <?php echo htmlspecialchars($row['name']); ?>
                                                </td>
                                                <td>
                                                    <?php echo $month; ?>
                                                </td>
                                                <td>
                                                    <?php echo $form_year; ?>
                                                </td>
                                                <td>
                                                    <?php echo $appointments; ?>
                                                </td>
                                                <td>
                                                    <?php echo $totalCapacity; ?>
                                                </td>
                                                <td>
                                                    <?php echo $capacityUtill . "%"; ?>
                                                </td>
                                            </tr>
                                            <?php
                                        } // end not export
                                        $srCount++;
                                    } // end while
                                } // end year if
                                debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                            } else {
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "capacity_utilization_monthly_report"; // changable
                                $selectedMonth = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "October", 9 => "November", 10 => "December");
                                $months = $selectedMonth[$month];
                                $rcsl_requested_filter = addslashes(serialize(array("year" =>$form_year, "month" => $month, "noDateRange" => "For $months $form_year", "facRegionId" => $form_region, "facZoneId" => $form_zone))); // changable
                                $rcsl_report_description = "Capacity Utilization - Monthly report for month $months"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                if ($msgForReportLog) {
                                    $msgForReportLog;
                                } else {
                                    $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                }
                                ##########Cron Request####################section only for all FACILITY#########################
                                echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";

                                $searchParam .= ' Clinic =  All | Month = ' . $month . ' |';

                                debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                            }

                            if (!$_POST['form_csvexport']) {
                                ?>

                            </tbody>
                        </table>
                    </div> <!-- end of results -->
                    <?php
                } // end not export
            } // end if refresh or export

            if (!$_POST['form_refresh'] && !$_POST['form_csvexport']) {
                ?>
                <div class='text'>
                    <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                </div>
                <?php
            }

            if (!$_POST['form_csvexport']) {
                ?>

            </form>
        </body>

    </html>
    <?php
} // end not export
?>
