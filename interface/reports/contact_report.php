<?php
// Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report lists patients that were seen within a given date
// range, or all patients if no date range is entered.

require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

// $from_date = fixDate($_POST['form_from_date'], date('Y-01-01'));
// $to_date   = fixDate($_POST['form_to_date'], date('Y-12-31'));
$from_date = fixDate($_POST['form_from_date'], '');
$to_date = fixDate($_POST['form_to_date'], '');




// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=contact_report.csv");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Patient List', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

            <script language="JavaScript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
            </script>
            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">

                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                        margin-bottom: 10px;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                    #report_results {
                        width: 100%;
                    }
                }

            </style>

        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <span class='title'><?php xl('Report', 'e'); ?> - <?php xl('Contacts', 'e'); ?></span>

            <div id="report_parameters_daterange">
                <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form name='theform' id='theform' method='post' action='contact_report.php'>

                <div id="report_parameters">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width: 100%;'>

                                    <table class='text'>
                                        <tr>
                                            <td width="3.33%" class='label'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="30%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>
                                            <td width="3.33%" class='label'>
                                                <?php xl('From', 'e'); ?>:
                                            </td>
                                            <td width="30%">
                                                <input type='text' name='form_from_date' id="form_from_date" size='40' value='<?php echo $form_from_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_from_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                            <td width="3.33%" class='label'>
                                                <?php xl('To', 'e'); ?>:
                                            </td>
                                            <td width="30%">
                                                <input type='text' name='form_to_date' id="form_to_date" size='40' value='<?php echo $form_to_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_to_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </td>
                            <td align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='css_button' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <a href='#' class='css_button' onclick='$("#form_csvexport").attr("value", "true");
                                                        $("#form_refresh").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php if ($_POST['form_refresh']) { ?>
                                                    <a href='#' class='css_button' onclick='window.print()'>
                                                        <span>
                                                            <?php xl('Print', 'e'); ?>
                                                        </span>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport

            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport']) {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Name') . '",';
                    echo '"' . xl('Mobile Number') . '",';
                    echo '"' . xl('Landline Number') . '",';
                    echo '"' . xl('Email id') . '",';
                    echo '"' . xl('Preferred mode of communication') . '",';
                    echo '"' . xl('Best time to reach you') . '",';
                    echo '"' . xl('Group') . '",';
                    echo '"' . xl('Referral Source') . '"' . "\n";
                } else {
                    ?>

                    <div id="report_results">
                        <table>
                            <thead>
                            <th> <?php xl('S. No.', 'e'); ?> </th>
                            <th> <?php xl('Name', 'e'); ?> </th>
                            <th> <?php xl('Mobile Number', 'e'); ?> </th>
                            <th> <?php xl('Landline Number', 'e'); ?> </th>
                            <th> <?php xl('Email id', 'e'); ?> </th>
                            <th> <?php xl('Preferred mode of communication', 'e'); ?> </th>
                            <th> <?php xl('Best time to reach you', 'e'); ?> </th>
                            <th> <?php xl('Group', 'e'); ?> </th>  
                            <th> <?php xl('Referral Source', 'e'); ?> </th>  
                            </thead>
                            <tbody>
                                <?php
                            } // end not export
                            $totalpts = 0;
                            $query = "SELECT " .
                                    "p.fname, p.mname, p.lname, p.email, p.hipaa_allowsms, p.hipaa_voice, p.hipaa_allowemail, p.time, " .
                                    "p.group, p.phone_home, p.phone_cell, p.phone_biz, p.pid, p.id, p.pubpid " .
                                    "FROM patient_data AS p ";
                            if ($form_facility) {
                                $query .= "JOIN patient_facilities AS pf ON " .
                                        "pf.pf_pid = p.id ";
                            } else {
                                $query .= "LEFT OUTER JOIN patient_facilities AS pf ON " .
                                        "pf.pf_pid = p.id ";
                            }


                            $form_to_date = $to_date;
                            $form_date = $from_date;
                            $searchParam = '';

                            if ($form_facility) {
                                //for individual facility
                                if ($form_date) {
                                    //if from date selected
                                    if ($form_to_date) {
                                        // if $form_date && $form_to_date
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;
                                        if ($days_between_from_to <= $form_facility_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND pf.pf_fid = '$form_facility'";
                                        } else {
                                            //$msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                            if ($days_between_from_to <= $form_facility_time_range_valid) {
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "contact_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "facility" => $form_facility))); // changable
                                                $rcsl_report_description = "Contact report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                            } else {
                                                $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                                            }
                                        }
                                    } else {
                                        //only from date: no TO date
                                        $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND pf.pf_fid = '$form_facility'";
                                        } else {
                                            //$msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                            if ($days_between_from_to <= $form_facility_time_range_valid) {
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "contact_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "facility" => $form_facility))); // changable
                                                $rcsl_report_description = "Contact report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                            } else {
                                                $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                                            }
                                        }
                                    }
                                } else {
                                    // if from date not selected
                                    if ($form_to_date) {
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND pf.pf_fid = '$form_facility'";
                                        } else {
                                            //$msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                            if ($days_between_from_to <= $form_facility_time_range_valid) {
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "contact_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "facility" => $form_facility))); // changable
                                                $rcsl_report_description = "Contact report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                            } else {
                                                $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                                            }
                                        }
                                    } else {
                                        if ($where) {
                                            $where .= " AND ";
                                        }
                                        $form_to_date = date("Y-m-d");
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                        $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND pf.pf_fid = '$form_facility'";
                                    }
                                }

                                $searchParam .= 'From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility; /// Auditing Section Param
                            } else {
                                //for all facility
                                if ($form_date) {
                                    //if from date selected
                                    if ($form_to_date) {
                                        // if $form_date && $form_to_date
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_all_time_range) {

                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                        } else {
                                            if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "contact_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date))); // changable
                                                $rcsl_report_description = "Contact report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                            } else {
                                                $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                                            }
                                        }
                                    } else {
                                        //only from date: no TO date
                                        $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_all_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                        } else {
                                            if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                                                $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "contact_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date))); // changable
                                                $rcsl_report_description = "Contact report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                ##########Cron Request####################section only for all FACILITY#########################
                                            } else {
                                                $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                                            }
                                        }
                                    }
                                } else {
                                    // if from date not selected
                                    if ($form_to_date) {
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_all_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                        } else {
                                            if ($days_between_from_to <= $form_facility_all_time_range_valid) {

                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "contact_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date))); // changable
                                                $rcsl_report_description = "Contact report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                            } else {
                                                $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                                            }
                                        }
                                    } else {
                                        if ($where) {
                                            $where .= " AND ";
                                        }
                                        $form_to_date = date("Y-m-d");
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                        $where .= "p.date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                    }
                                }

                                $searchParam .= 'From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All'; /// Auditing Section Param
                            }


                            $query.= "WHERE $where ";
                            $query .=
                                    "GROUP BY p.pid " .
                                    "ORDER BY p.pid DESC";

                            if (!empty($_POST['form_refresh'])) {
                                $event = "Report Contact View";
                            } elseif (!empty($_POST['form_csvexport'])) {
                                $event = "Report Contact Export";
                            }



                            if ($msgForReportLog) {
                                $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                            } else {
                                $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section

                                $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);
                                $prevpid = 0;
                                $srCount = 1;
                                while ($row = sqlFetchArray($res)) {
                                    if ($row['pid'] == $prevpid)
                                        continue;
                                    $prevpid = $row['pid'];
                                    $name = $row['fname'];
                                    if (!empty($row['mname'])) {
                                        $name .= ' ' . $row['mname'];
                                    }
                                    if (!empty($row['lname'])) {
                                        $name .= ' ' . $row['lname'];
                                    }
                                    $mode_of_comm = "";
                                    if ($row['hipaa_voice'] == 'YES') {
                                        $mode_of_comm = 'Call';
                                    }
                                    if ($row['hipaa_allowemail'] == 'YES') {
                                        if (!empty($mode_of_comm)) {
                                            $mode_of_comm .= ' Email';
                                        } else {
                                            $mode_of_comm = ' Email';
                                        }
                                    }
                                    if ($row['hipaa_allowsms'] == 'YES') {
                                        if (!empty($mode_of_comm)) {
                                            $mode_of_comm .= ' SMS';
                                        } else {
                                            $mode_of_comm = ' SMS';
                                        }
                                    }
                                    if ($_POST['form_csvexport']) {
                                        echo '"' . qescape($srCount) . '",';
                                        echo '"' . qescape($name) . '",';
                                        echo '"' . qescape($row['phone_cell'] !='' ? $row['phone_cell'] : 'NA') . '",';
                                        echo '"' . qescape($row['phone_home'] != '' ? $row['phone_home'] : 'NA') . '",';
                                        echo '"' . qescape($row['email'] !='' ? $row['email'] : 'NA') . '",';
                                        echo '"' . qescape($mode_of_comm !='' ? $mode_of_comm : 'NA') . '",';
                                        echo '"' . qescape(trim($row['time']) != 'pleaseSelect' ? $row['time'] : 'NA') . '",';
                                        echo '"' . qescape($row['group'] !='' ? $row['group'] : 'NA') . '",';
                                        echo '"' . qescape('') . '"' . "\n";
                                    } else {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $srCount; ?>
                                            </td>
                                            <td>
                                                <?php echo ucwords(htmlspecialchars($name)); ?>
                                            </td>
                                            <td>
                                                <?php echo $row['phone_cell']!=''?$row['phone_cell']:'NA'; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['phone_home']!=''?$row['phone_home']:'NA'; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['email']!=''?$row['email']:'NA'; ?>
                                            </td>
                                            <td>
                                                <?php echo $mode_of_comm!='' ? $mode_of_comm :'NA'; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if(trim($row['time'])!='' AND trim($row['time']) != 'pleaseSelect'){
                                                    echo $row['time'];
                                                }
                                                else{
                                                    echo "NA";
                                                }
                                                ?>
                                                <?php //echo trim($row['time']) != 'pleaseSelect' ? $row['time'] : "NA"; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['group']!=''?$row['group']:'NA'; ?>
                                            </td> 
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <?php
                                    } // end not export
                                    $srCount++;
                                    ++$totalpts;
                                } // end while

                                debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                            }

                            if (!$_POST['form_csvexport']) {
                                ?>

                                <tr class="report_totals">
                                    <td colspan='9' align='center'>
                                        <?php
                                        if ($msgForReportLog) {
                                            echo $msgForReportLog;
                                        }

                                        if (empty($msgForReportLog)) {
                                            xl('Total Number of Patients: ', 'e');
                                            echo $totalpts;
                                        }
                                        ?>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div> <!-- end of results -->
                    <?php
                } // end not export
            } // end if refresh or export

            if (!$_POST['form_refresh'] && !$_POST['form_csvexport']) {
                ?>
                <div class='text'>
                    <?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>
                <?php
            }

            if (!$_POST['form_csvexport']) {
                ?>

            </form>
        </body>

        <!-- stuff for the popup calendar -->
        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
        <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
        <script language="Javascript">
                                                    Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
                                                    Calendar.setup({inputField: "form_to_date", ifFormat: "%Y-%m-%d", button: "img_to_date"});
        </script>
    </html>
    <?php
} // end not export
?>
