<?php
 // Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists front office receipts for a given date range.

require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";

 $from_date = fixDate($_POST['form_from_date'], date('Y-m-d'));
 $to_date   = fixDate($_POST['form_to_date'], date('Y-m-d'));

 function bucks($amt) {
  return ($amt != 0.00) ? oeFormatMoney($amt) : '';
 }
?>
<html>
<head>
<?php html_header_show();?>
<title><?php xl('Front Office Receipts','e'); ?></title>
<script type="text/javascript" src="../../library/overlib_mini.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

<script language="JavaScript">

<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

 var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

 // The OnClick handler for receipt display.
 function show_receipt(pid,timestamp,encounter) {
  dlgopen('../patient_file/front_payment.php?receipt=1&encounter='+encounter+'&patient=' + pid +
   '&time=' + timestamp, '_blank', 550, 400);
 }

</script>

<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<style type="text/css">
/* specifically include & exclude from printing */
@media print {
    #report_parameters {
        visibility: hidden;
        display: none;
    }
    #report_parameters_daterange {
        visibility: visible;
        display: inline;
    }
    #report_results {
       margin-top: 30px;
    }
}

/* specifically exclude some from the screen */
@media screen {
    #report_parameters_daterange {
        visibility: hidden;
        display: none;
    }
}
</style>
</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<span class='title'><?php xl('Report','e'); ?> - <?php xl('Patient Payment','e'); ?></span>

<div id="report_parameters_daterange">
<?php echo date("d F Y", strtotime($form_from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($form_to_date)); ?>
</div>

<form name='theform' method='post' action='patient_payment_report.php' id='theform'>

<div id="report_parameters">

<input type='hidden' name='form_refresh' id='form_refresh' value=''/>

<table>
 <tr>
  <td width='610px'>
	<div style='float:left'>

	<table class='text'>
		<tr>
			<td class='label'>
			   <?php xl('Payment Method','e'); ?>:
			</td>
			<td>
				<select id="form_method" class="text" name="form_method">
					<option value="">Payment Method</option>
					<option value="check_payment" <?php if($_POST['form_method'] == 'check_payment'){ echo 'selected';}?>>Check Payment</option>
					<option value="cash" <?php if($_POST['form_method'] == 'cash'){ echo 'selected';}?>>Cash</option>
					<option value="card" <?php if($_POST['form_method'] == 'card'){ echo 'selected';}?>>Card</option>
				</select>
			   
			</td>
			<td class='label'>
			   <?php xl('Facility','e'); ?>:
			</td>
			<td>
			   <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false); ?>
			  
			</td>
		</tr>
		<tr>
			<td class='label'>
			   <?php echo htmlspecialchars( xl('Search by:'), ENT_NOQUOTES); ?>
			</td>
			<td>
				<select name='searchby'>
					<option value="">Patient Search By</option>
					<option value="Name"<?php if ($_POST['searchby'] == 'Name') echo ' selected' ?>><?php echo htmlspecialchars( xl('Name'), ENT_NOQUOTES); ?></option>
					<option value="ID"<?php if ($_POST['searchby'] == 'ID') echo ' selected' ?>><?php echo htmlspecialchars( xl('ID'), ENT_NOQUOTES); ?></option>
			   </select>
			   
			</td>
			<td class='label'>
			   <?php echo htmlspecialchars( xl('for:'), ENT_NOQUOTES); ?>
			</td>
			<td>
			   <input type='text' id='searchparm' name='searchparm' size='12' value='<?php echo htmlspecialchars( $_POST['searchparm'], ENT_QUOTES); ?>'
    title='<?php echo htmlspecialchars( xl('If name, any part of lastname or lastname,firstname'), ENT_QUOTES); ?>'>
			  
			</td>
		</tr>
		<tr>
			<td class='label'>
			   <?php xl('From','e'); ?>:
			</td>
			<td>
			   <input type='text' name='form_from_date' id="form_from_date" size='10' value='<?php echo $form_from_date ?>'
				onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)' title='yyyy-mm-dd'>
			   <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
				id='img_from_date' border='0' alt='[?]' style='cursor:pointer'
				title='<?php xl('Click here to choose a date','e'); ?>'>
			</td>
			<td class='label'>
			   <?php xl('To','e'); ?>:
			</td>
			<td>
			   <input type='text' name='form_to_date' id="form_to_date" size='10' value='<?php echo $form_to_date ?>'
				onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)' title='yyyy-mm-dd'>
			   <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
				id='img_to_date' border='0' alt='[?]' style='cursor:pointer'
				title='<?php xl('Click here to choose a date','e'); ?>'>
			</td>
		</tr>
	</table>

	</div>

  </td>
  <td align='left' valign='middle' height="100%">
	<table style='border-left:1px solid; width:100%; height:100%' >
		<tr>
			<td>
				<div style='margin-left:15px'>
					<a href='#' class='css_button' onclick='$("#form_refresh").attr("value","true"); $("#theform").submit();'>
					<span>
						<?php xl('Submit','e'); ?>
					</span>
					</a>

					<?php if ($_POST['form_refresh']) { ?>
					<a href='#' class='css_button' onclick='window.print()'>
						<span>
							<?php xl('Print','e'); ?>
						</span>
					</a>
					<?php } ?>
				</div>
			</td>
		</tr>
	</table>
  </td>
 </tr>
</table>
</div> <!-- end of parameters -->

<?php
 if ($_POST['form_refresh'] || $_POST['form_orderby']) {
?>
<div id="report_results">
<table>
 <thead>
  <th> <?php xl('Time','e'); ?> </th>
  <th> <?php xl('Receipt No.','e'); ?> </th>
  <th> <?php xl('Invoice No.','e'); ?> </th>
  <th> <?php xl('Patient','e'); ?> </th>
  <th> <?php xl('ID','e'); ?> </th>
  <th> <?php xl('Method','e'); ?> </th>
  <th> <?php xl('Source','e'); ?> </th>
  <th align='right'> <?php xl('Today','e'); ?> </th>
  <!--th align='right'> <?php xl('Previous','e'); ?> </th>
  <th align='right'> <?php xl('Total','e'); ?> <th-->
  <th align='right'> <?php xl('Due Amount','e'); ?> </th>
 </thead>
 <tbody>
<?php
 if (true || $_POST['form_refresh']) {
  $total1 = 0.00;
  $total2 = 0.00;
  $netAmountTotal = 0.00;
	$query = "SELECT r.pid, r.encounter, r.dtime, " .
    "r.amount1 AS amount1, " .
    "r.amount2 AS amount2, " .
    "r.method AS method, " .
    "r.source AS source, " .
    "r.user AS user, " .
	"r.bill_no AS reciept_no, " .
    "p.fname, p.mname, p.lname, p.pid, p.pubpid, fe.invoice_refno " .
    "FROM payments AS r " .
    "LEFT OUTER JOIN patient_data AS p ON " .
    "p.pid = r.pid " .
	"JOIN form_encounter AS fe ON fe.pid = r.pid AND fe.encounter = r.encounter " .
    "WHERE ";
	$conditions='';
	if(isset($_POST['form_method']) && !empty($_POST['form_method'])) {
		if(!empty($conditions)) {
			$conditions.=" AND r.method='".$_POST['form_method']."'";
		}else {
			$conditions=" r.method='".$_POST['form_method']."'";
		}
	}
	if(isset($_POST['searchby']) && !empty($_POST['searchby']) && $_POST['searchby'] == 'Name' && isset($_POST['searchparm']) && !empty($_POST['searchparm'])) {
		if(!empty($conditions)) {
			$conditions.=" AND (p.fname LIKE '".$_POST['searchparm']."%' OR p.mname LIKE '".$_POST['searchparm']."%' OR p.lname LIKE '".$_POST['searchparm']."%')";
		}else {
			$conditions.=" (p.fname LIKE '".$_POST['searchparm']."%' OR p.mname LIKE '".$_POST['searchparm']."%' OR p.lname LIKE '".$_POST['searchparm']."%')";
		}
	}
	if(isset($_POST['searchby']) && !empty($_POST['searchby']) && $_POST['searchby'] == 'ID' && isset($_POST['searchparm']) && !empty($_POST['searchparm'])  && trim($_POST['searchparm']) != '%') {
		if(!empty($conditions)) {
			$conditions.=" AND p.pid = '".$_POST['searchparm']."'";
		}else {
			$conditions.=" p.pid = '".$_POST['searchparm']."'";
		}
	}
	if(isset($_POST['form_from_date']) && isset($_POST['form_to_date']) && !empty($_POST['form_from_date'])  && !empty($_POST['form_to_date'])){
		if(!empty($conditions)) {
			$conditions.=" AND r.dtime >= '$from_date 00:00:00' AND r.dtime <= '$to_date 23:59:59' ";
		}else {
			$conditions="  r.dtime >= '$from_date 00:00:00' AND r.dtime <= '$to_date 23:59:59' ";
		}
	}
	if(isset($_POST['form_facility'])&& !empty($_POST['form_facility'])){
		if(!empty($conditions)) {
			$conditions.=" AND fe.facility_id = '".$_POST['form_facility']."' ";
		}else {
			$conditions="  fe.facility_id = '".$_POST['form_facility']."' ";
		}
	}
	if(empty($conditions)) {
		$conditions="  r.dtime >= '$from_date 00:00:00' AND r.dtime <= '$to_date 23:59:59' ";
	}
    $query .=$conditions." ORDER BY r.dtime desc, r.pid desc";
	//echo $query;
  // echo "<!-- $query -->\n"; // debugging
  $res = sqlStatement($query);

  while ($row = sqlFetchArray($res)) {
    // Make the timestamp URL-friendly.
    $timestamp = preg_replace('/[^0-9]/', '', $row['dtime']);
?>
 <tr>
  <td nowrap>
   <a href="javascript:show_receipt(<?php echo $row['pid'] . ",'$timestamp',".$row['encounter'] ; ?>)">
   <?php echo oeFormatShortDate(substr($row['dtime'], 0, 10)) . substr($row['dtime'], 10, 6); ?>
   </a>
  </td>
  <td nowrap>
   <a href="javascript:show_receipt(<?php echo $row['pid'] . ",'$timestamp',".$row['encounter'] ; ?>)">
   <?php echo $row['reciept_no']; ?>
   </a>
  </td>
  <td>
   <?php echo empty($row['invoice_refno']) ? $row['pid'].'.'.$row['encounter'] : $row['invoice_refno']; ?>
  </td>
  <td>
   <?php echo $row['lname'] . ', ' . $row['fname'] . ' ' . $row['mname'] ?>
  </td>
  <td>
   <?php echo $row['pubpid'] ?>
  </td>
  <td>
   <?php echo $row['method'] ?>
  </td>
  <td>
   <?php echo $row['source'] ?>
  </td>
  <!--td align='right'>
   <?php //echo bucks($row['amount1']) ?>
  </td>
  <td align='right'>
   <?php //echo bucks($row['amount2']) ?>
  </td-->
  <td align='right'>
   <?php echo text(number_format($row['amount1'] + $row['amount2'], 2)); ?>
  </td>
  <?php
	//Treatment Charges
	$ResultSearchNew = sqlStatement("SELECT * FROM billing LEFT JOIN code_types ON billing.code_type=code_types.ct_key WHERE code_types.ct_fee=1 ".
		"AND billing.activity!=0 AND billing.pid =? AND encounter=? ORDER BY billing.code,billing.modifier",
	  array($row['pid'],$row['encounter']));
	$totoalCast = 0;
	while($RowSearch = sqlFetchArray($ResultSearchNew)){
		//print_r($RowSearch);
		$totoalCast += sprintf('%01.2f', $RowSearch['fee']);
	}
	
	// Product sales
	$inres = sqlStatement("SELECT s.sale_id, s.sale_date, s.fee, " .
	  "s.quantity, s.drug_id, d.name " .
	  "FROM drug_sales AS s LEFT JOIN drugs AS d ON d.drug_id = s.drug_id " .
	  // "WHERE s.pid = '$patient_id' AND s.encounter = '$encounter' AND s.fee != 0 " .
	  "WHERE s.pid = ? AND s.encounter = ? " .
	  "ORDER BY s.sale_id", array($row['pid'],$row['encounter']) );
	while ($inrow = sqlFetchArray($inres)) {
		
	  $totoalCast += sprintf('%01.2f', $inrow['fee']);
	}
	// Adjustments.
	$discounts = 0.00;
	$inres = sqlStatement("SELECT " .
	  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
	  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
	  "FROM ar_activity AS a " .
	  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
	  "a.pid = ? AND a.encounter = ? AND " .
	  "a.adj_amount != 0 " .
	  "ORDER BY s.check_date, a.sequence_no", array($row['pid'],$row['encounter']) );
	while ($inrow = sqlFetchArray($inres)) {
	  $totoalCast -= sprintf('%01.2f', $inrow['adj_amount']);
	}
	$inres = sqlStatement("SELECT " .
	  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
	  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
	  "FROM ar_activity AS a " .
	  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
	  "a.pid = ?  AND a.encounter = ? AND " .
	  "a.pay_amount != 0 AND a.post_time <= ?" .
	  "ORDER BY s.check_date, a.sequence_no", array($row['pid'], $row['encounter'], $timestamp) );
	  $charges = 0;
	while ($inrow = sqlFetchArray($inres)) {
	  $charges += sprintf('%01.2f', $inrow['pay_amount']);
	  //$totoalCast -= sprintf('%01.2f', $inrow['pay_amount']);							  
	}
	$netAmount = $totoalCast - $charges;
	$netAmountTotal +=$netAmount;
	?>
	<td align='right'>
   <?php //echo "charges=>".$charges; echo "<br>totoalCast=>".$totoalCast; echo "<br> Net Ammout=>".($totoalCast-$charges);
   //echo '<br>';
   if($netAmount > 0){
	echo text(number_format($netAmount, 2));
   }else {
	echo '0.00';
   }
   //echo empty($totoalCast) ? '0.00' : bucks($totoalCast); ?>
  </td>
 </tr>
<?php
    $total1 += $row['amount1'];
    $total2 += $row['amount2'];
  }
?>

 <tr>
  <td colspan='9'>
   &nbsp;
  </td>
 </tr>

 <tr class="report_totals">
  <td colspan='7'>
   <?php xl('Totals','e'); ?>
  </td>
  <!--td align='right'>
   <?php echo text(number_format($total1, 2)); ?>
  </td>
  <td align='right'>
   <?php echo text(number_format($total2, 2)); ?>
  </td-->
  <td align='right'>
   <?php echo text(number_format($total1 + $total2, 2)) ?>
  </td>
  <td align='right'>
   <?php echo text(number_format($netAmountTotal, 2)); ?>
  </td>
 </tr>

<?php
 }
?>
</tbody>
</table>
</div> <!-- end of results -->
<?php } else { ?>
<div class='text'>
 	<?php echo xl('Please input search criteria above, and click Submit to view results.', 'e' ); ?>
</div>
<?php } ?>

</form>
</body>
<!-- stuff for the popup calendar -->
<style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
<script language="Javascript">
 Calendar.setup({inputField:"form_from_date", ifFormat:"%Y-%m-%d", button:"img_from_date"});
 Calendar.setup({inputField:"form_to_date", ifFormat:"%Y-%m-%d", button:"img_to_date"});
</script>
</html>
