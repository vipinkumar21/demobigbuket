<?php
/**
 *
 * Treatments..... Report: (Swati Jain)
 * modified by :sunal dhawan
 * modified date:10th september 2015
 * */
// 
ob_start();
require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/sql-ledger.inc");
require_once("$srcdir/invoice_summary.inc.php");
require_once("$srcdir/sl_eob.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once ("$audting_webroot/auditlog.php");

function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$bgcolor = "#aaaaaa";
$form_facility = $_POST['form_facility'];
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
$tr_name = trim($_REQUEST['tr_name']);

$searchParam = '';
$fileName = "treatment_report_".date("Ymd_his").".csv";
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvTreatments') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>
        <head>
            <?php if (function_exists('html_header_show')) html_header_show(); ?>
            <link rel=stylesheet href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('Treatment Report', 'e') ?></title>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />


            <style type="text/css">

                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }

            </style>
        </head>

        <body class="body_top">

            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Treatment', 'e'); ?></h3>

            <form method='post' action='treatment_report.php' enctype='multipart/form-data' id='theform' class='emrform botnomrg topnopad'>

                <div id="report_parameters" class="topnopad botnopad">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width: 100%;'>
                                    <table class='text'>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class='label'>
                                                            <?php xl('Facility', 'e'); ?>:
                                                        </td>
                                                        <td>
                                                            <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                                        </td>						
                                                        <td class='label'>
                                                            <?php xl('TR. Name', 'e'); ?>:
                                                        </td>
                                                        <td>
                                                            <input class='emrdate newwidth100' type='text' name='tr_name' id="tr_name" size='10' value='<?php echo $tr_name ?>' title='Treatment Name for search'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class='label'>
                                                            <?php xl('Search by', 'e'); ?>:
                                                        </td>
                                                        <td>
                                                            <select class='emrinput' name='psearchby'>
                                                                <option value="">Select Option</option>
                                                                <option value="DoctorName"<?php if ($_POST['psearchby'] == 'DoctorName') echo ' selected' ?>><?php echo htmlspecialchars(xl('Doctor Name'), ENT_NOQUOTES); ?></option>
                                                                <option value="PatientName"<?php if ($_POST['psearchby'] == 'PatientName') echo ' selected' ?>><?php echo htmlspecialchars(xl('Patient Name'), ENT_NOQUOTES); ?></option>
                                                                <option value="PatientId"<?php if ($_POST['psearchby'] == 'PatientId') echo ' selected' ?>><?php echo htmlspecialchars(xl('Patient Id'), ENT_NOQUOTES); ?></option>
                                                            </select>
                                                        </td>
                                                        <td class='label'>
                                                            <?php xl('For', 'e'); ?>:
                                                        </td>
                                                        <td>
                                                            <input class='emrdate newwidth100' type='text' id='psearchparm' name='psearchparm' size='12' value='<?php echo htmlspecialchars($_POST['psearchparm'], ENT_QUOTES); ?>'
                                                                   title='<?php echo htmlspecialchars(xl('If name, any part of lastname or firstname'), ENT_QUOTES); ?>'>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width='10%' class='label'><?php xl('From', 'e'); ?>:</td>
                                                        <td width='40%'><input class='emrdate' type='text' name='form_from_date' id="form_from_date"
                                                                               size='10' value='<?php echo $form_date ?>'
                                                                               onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                               title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                               align='absbottom' width='24' height='22' id='img_from_date'
                                                                               border='0' alt='[?]' style='cursor: pointer'
                                                                               title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                                        <td width='10%' class='label'><?php xl('To', 'e'); ?>:</td>
                                                        <td width='40%'><input class='emrdate' type='text' name='form_to_date' id="form_to_date"
                                                                               size='10' value='<?php echo $form_to_date ?>'
                                                                               onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                               title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                               align='absbottom' width='24' height='22' id='img_to_date'
                                                                               border='0' alt='[?]' style='cursor: pointer'
                                                                               title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                                    </tr>
                                                    <tr>
                                                        <td width='10%' class='label'><?php xl('Search For', 'e'); ?>:</td>
                                                        <td width='40%'>
                                                            <select class='emrinput' name='invoicetreatmentsearchby'>
                                                                <option value="AllTreatment"<?php if ($_POST['invoicetreatmentsearchby'] == 'AllTreatment') echo ' selected' ?>><?php echo htmlspecialchars(xl('All'), ENT_NOQUOTES); ?></option>
                                                                <option value="TreatmentNotInvoiced"<?php if ($_POST['invoicetreatmentsearchby'] == 'TreatmentNotInvoiced') echo ' selected' ?>><?php echo htmlspecialchars(xl('Not Invoiced'), ENT_NOQUOTES); ?></option>
                                                                <option value=1<?php if ($_POST['invoicetreatmentsearchby'] == 1) echo ' selected' ?>><?php echo htmlspecialchars(xl('Invoiced'), ENT_NOQUOTES); ?></option>
                                                            </select>
                                                        </td>
                                                        <td width='10%' class='label'><?php xl('TR.Category', 'e'); ?>:</td>
                                                        <td width='40%'><?php dropdown_treatment_category(strip_escape_custom($form_treatment_group), 'form_treatment_group', false, true); ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </table>

                                </div>	
                            </td>
                            <td width="20%" align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px;'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>	
                                                <?php if (1) { ?>
                                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvTreatments");
                                                            $("#theform").submit();'>
                                                        <span>
                                                            <?php xl('Export to CSV', 'e'); ?>
                                                        </span>
                                                    </a>
                                                <?php } ?>					
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="text">* Please select a filter to see the results.</div>
            <?php } // end of csv section  ?>
            <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                //$where = "b.bill_type='0'";
                $psearchby = $_POST['psearchby'];
                $psearchparm = $_POST['psearchparm'];
                $invoicetreatmentsearchby = $_POST['invoicetreatmentsearchby'];
                $form_invoiced=0;
                if($_POST['invoicetreatmentsearchby']== 1){
                    $form_invoiced =1;
                }
                if ($form_facility) {
                    //for individual facility
                    if ($form_date) {
                        //if from date selected
                        if ($form_to_date) {
                            // if $form_date && $form_to_date
                            $toDate = date_create(date($form_to_date));
                            $fromDate = date_create($form_date);
                            $diff = date_diff($fromDate, $toDate);
                            $days_between_from_to = $diff->days;


                            if ($days_between_from_to <= $form_facility_time_range) {

                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                                //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                    $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                                }else {
                                    $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                }
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $where .= " b.clinic_id = '$form_facility'";
                            } else {
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "treatment_report"; // changable
                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatment_name" => $tr_name, "facility"=>$form_facility, "psearchby"=>$psearchby, "psearchparm"=>$psearchparm, "invoicetreatmentsearchby"=>$invoicetreatmentsearchby, "form_treatment_group"=>$form_treatment_group))); // changable
                                $rcsl_report_description = "Treatment report from $form_date to $form_to_date"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                }
                            }
                        } else {
                            //only from date: no TO date
                            $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                            $toDate = date_create(date($form_to_date));
                            $fromDate = date_create($form_date);
                            $diff = date_diff($fromDate, $toDate);
                            $days_between_from_to = $diff->days;

                            if ($days_between_from_to <= $form_facility_time_range) {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                                //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                    $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                                }else {
                                    $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                }
                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_clinic_id = '$form_facility'";
                                $where .= " b.clinic_id = '$form_facility'";
                            } else {
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "treatment_report"; // changable
                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatment_name" => $tr_name, "facility"=>$form_facility, "psearchby"=>$psearchby, "psearchparm"=>$psearchparm, "invoicetreatmentsearchby"=>$invoicetreatmentsearchby, "form_treatment_group"=>$form_treatment_group))); // changable
                                $rcsl_report_description = "Treatment report from $form_date to $form_to_date"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                }
                            }
                        }
                    } else {
                        // if from date not selected
                        if ($form_to_date) {
                            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                            $toDate = date_create(date($form_to_date));
                            $fromDate = date_create($form_date);
                            $diff = date_diff($fromDate, $toDate);
                            $days_between_from_to = $diff->days;

                            if ($days_between_from_to <= $form_facility_time_range) {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                                //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                    $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                                }else {
                                    $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                }
                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_clinic_id = '$form_facility'";
                                $where .= " b.clinic_id = '$form_facility'";
                            } else {
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "treatment_report"; // changable
                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatment_name" => $tr_name, "facility"=>$form_facility, "psearchby"=>$psearchby, "psearchparm"=>$psearchparm, "invoicetreatmentsearchby"=>$invoicetreatmentsearchby, "form_treatment_group"=>$form_treatment_group))); // changable
                                $rcsl_report_description = "Treatment report from $form_date to $form_to_date"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                }
                            }
                        } else {
                            if ($where) {
                                $where .= " AND ";
                            }
                            $form_to_date = date("Y-m-d");
                            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                            //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                            //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                            if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                            }else {
                                $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                            }
                            if ($where) {
                                $where .= " AND ";
                            }
                            //$where .= " i.inv_clinic_id = '$form_facility'";
                            $where .= " b.clinic_id = '$form_facility'";
                        }
                    }
                    
                    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param  
                    
                } else {
                    //for all facility
                    if ($form_date) {
                        //if from date selected
                        if ($form_to_date) {
                            // if $form_date && $form_to_date
                            $toDate = date_create(date($form_to_date));
                            $fromDate = date_create($form_date);
                            $diff = date_diff($fromDate, $toDate);
                            $days_between_from_to = $diff->days;

                            if ($days_between_from_to <= $form_facility_all_time_range) {

                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                                //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                    $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                                }else {
                                    $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                }
                            } else {
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "treatment_report"; // changable
                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatment_name" => $tr_name, "psearchby"=>$psearchby, "psearchparm"=>$psearchparm, "invoicetreatmentsearchby"=>$invoicetreatmentsearchby, "form_treatment_group"=>$form_treatment_group))); // changable
                                $rcsl_report_description = "Treatment report from $form_date to $form_to_date"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                }
                            }
                        } else {
                            //only from date: no TO date
                            $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                            $toDate = date_create(date($form_to_date));
                            $fromDate = date_create($form_date);
                            $diff = date_diff($fromDate, $toDate);
                            $days_between_from_to = $diff->days;

                            if ($days_between_from_to <= $form_facility_all_time_range) {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                                //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                    $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                                }else {
                                    $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                }
                            } else {
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "treatment_report"; // changable
                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatment_name" => $tr_name, "psearchby"=>$psearchby, "psearchparm"=>$psearchparm, "invoicetreatmentsearchby"=>$invoicetreatmentsearchby, "form_treatment_group"=>$form_treatment_group))); // changable
                                $rcsl_report_description = "Treatment report from $form_date to $form_to_date"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                }
                            }
                        }
                    } else {
                        // if from date not selected
                        if ($form_to_date) {
                            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                            $toDate = date_create(date($form_to_date));
                            $fromDate = date_create($form_date);
                            $diff = date_diff($fromDate, $toDate);
                            $days_between_from_to = $diff->days;

                            if ($days_between_from_to <= $form_facility_all_time_range) {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                    $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                                }else {
                                    $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                                }
                                //$where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                            } else {
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "treatment_report"; // changable
                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatment_name" => $tr_name, "psearchby"=>$psearchby, "psearchparm"=>$psearchparm, "invoicetreatmentsearchby"=>$invoicetreatmentsearchby, "form_treatment_group"=>$form_treatment_group))); // changable
                                $rcsl_report_description = "Treatment report from $form_date to $form_to_date"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                }
                            }
                        } else {
                            if ($where) {
                                $where .= " AND ";
                            }
                            $form_to_date = date("Y-m-d");
                            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                            //$where .= " i.inv_created_date >= '$form_date' AND i.inv_created_date <= '$form_to_date'";
                            if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                                $where .= " DATE(i.inv_created_date) BETWEEN '$form_date' AND '$form_to_date'";
                            }else {
                                $where .= " DATE(b.date) BETWEEN '$form_date' AND '$form_to_date'";
                            }
                        }
                    }
                    
                    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param  
                }



                if (!$where) {
                    $where = "1 = 1";
                }
                if (!empty($tr_name)) {
                    $where .= " AND b.code_text LIKE '%" . $tr_name . "%'";
                    $searchParam .= ' Treatment Name = '.$tr_name.' |';
                }
                // search for patient name, id and doc name
                if (isset($_POST['psearchby']) && !empty($_POST['psearchby']) && $_POST['psearchby'] == 'PatientName' && isset($_POST['psearchparm']) && !empty($_POST['psearchparm'])) {
                    $where.=" AND (pd.fname LIKE '%" . trim($_POST['psearchparm']) . "%')";
                   $searchParam .= ' Patient  Name = ' . $_POST['psearchparm'] . ' |';
                }
                if (isset($_POST['psearchby']) && !empty($_POST['psearchby']) && $_POST['psearchby'] == 'PatientId' && isset($_POST['psearchparm']) && !empty($_POST['psearchparm']) && trim($_POST['psearchparm']) != '%') {
                    $where.=" AND pd.pubpid = '" . $_POST['psearchparm'] . "'";
                    $searchParam .= ' Patient  Id = ' . $_POST['psearchparm'] . ' |';
                }
                if (isset($_POST['psearchby']) && !empty($_POST['psearchby']) && $_POST['psearchby'] == 'DoctorName' && isset($_POST['psearchparm']) && !empty($_POST['psearchparm']) && trim($_POST['psearchparm']) != '%') {
                    //$where.=" AND (u.fname LIKE '%" . trim($_POST['psearchparm']) . "%' OR u.mname LIKE '%" . trim($_POST['psearchparm']) . "%' OR u.lname LIKE '%" . trim($_POST['psearchparm']) . "%')";
                    $where.=" AND concat_ws(' ',u.fname,u.lname) LIKE '%" . trim($_POST['psearchparm']) . "%'";                     
                    $searchParam .= ' Doctor Name = ' . $_POST['psearchparm'] . ' |';
                }
                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                    $where.=" AND ((b.work_status <> 0 AND b.bill_type = '0') OR (b.work_status = 0 AND b.bill_type = '1') OR (b.work_status = 0 AND b.bill_type = '0' AND ii.invit_deleted = 1)) ";                     
                    $searchParam .= ' From Invoiced = ' . $_POST['invoicetreatmentsearchby'] . ' |';
                }elseif(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 'TreatmentNotInvoiced'){
                    $where.=" AND b.work_status = 0 ";
                    $searchParam .= ' From Treatment Invoiced But Not Start = ' . $_POST['invoicetreatmentsearchby'] . ' |';
                }elseif(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 'AllTreatment') {
                    $searchParam .= ' From All Treatment = ' . $_POST['invoicetreatmentsearchby'] . ' |';
                }
                if(isset($_POST['form_treatment_group']) && !empty($_POST['form_treatment_group'])){
                    $where.=" AND co.superbill = '$form_treatment_group' ";                     
                    $searchParam .= ' From Treatment Group = ' . $_POST['form_treatment_group'] . ' |';
                }


                /*$query = "SELECT f.name,i.inv_created_date , pd.pubpid, CONCAT_WS(' ', u.fname, u.lname) AS uname, pd.fname, pd.mname, pd.lname,b.code_text, b.remarks ,ROUND(b.unit_price) AS unit_price,b.discount,b.discount_type,b.units,i.inv_clinic_id
				FROM invoice AS i 
				INNER JOIN facility AS f ON f.id = i.inv_clinic_id
				INNER JOIN invoice_items AS ii ON i.inv_id = ii.invit_inv_id 
				INNER JOIN billing AS b ON b.id = ii.invit_tp_id 
				INNER JOIN patient_data AS pd ON b.pid = pd.id
                                INNER JOIN users AS u ON u.id = b.provider_id ";
                $query .= " WHERE " . $where . " AND i.inv_deletestate = 1 GROUP BY ii.invit_tp_id
				ORDER BY i.inv_created_date DESC";*/
                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){
                    $query = "SELECT DATE_FORMAT(i.inv_created_date, '%d %b %y') AS `trDate`,i.inv_number,CONCAT_WS(' ', u.fname, u.lname) AS uname , pd.pubpid, pd.fname, b.code_text, b.toothno AS `Tooth Number`, b.remarks, ROUND(b.unit_price*b.units) AS `ActualCost`,b.units, 
                            CASE 
                                WHEN b.discount_type = '%' THEN ROUND(ROUND(b.unit_price*b.units)*(b.discount/100))
                                ELSE b.discount
                            END AS `Discount`,
                            b.fee AS `TrCost`, f.name AS `Clinic` ,f.facility_npi AS `Clinic Code`,co.superbill AS `trtCategory`
                            FROM billing AS b 
                            INNER JOIN invoice_items AS ii ON ii.invit_tp_id = b.id
                            INNER JOIN invoice AS i ON i.inv_id = ii.invit_inv_id
                            INNER JOIN patient_data AS pd ON b.pid = pd.id 
                            INNER JOIN facility AS f ON f.id = b.clinic_id 
                            INNER JOIN users AS u ON u.id = b.provider_id 
                            INNER JOIN codes AS co ON co.code = b.code AND co.code_type = b.code_type
                            WHERE " . $where . " AND b.deletestate = 1 AND ((ii.invit_deleted = 1 AND i.inv_deletestate = 0) OR (ii.invit_deleted = 0 AND i.inv_deletestate IN (1,2))) AND i.inv_deletestate != 9 GROUP BY CONCAT(ii.invit_inv_id,',',ii.invit_tp_id) ORDER BY b.date DESC";
                }else {
                    $query = "SELECT DATE_FORMAT(b.date, '%d %b %y') AS `trDate`,CONCAT_WS(' ', u.fname, u.lname) AS uname , pd.pubpid, pd.fname, b.code_text, b.toothno AS `Tooth Number`, b.remarks, ROUND(b.unit_price*b.units) AS `ActualCost`,b.units, 
                            CASE 
                                WHEN b.discount_type = '%' THEN ROUND(ROUND(b.unit_price*b.units)*(b.discount/100))
                                ELSE b.discount
                            END AS `Discount`,
                            b.fee AS `TrCost`, f.name AS `Clinic` ,f.facility_npi AS `Clinic Code`,co.superbill AS `trtCategory`
                            FROM billing AS b  
                            INNER JOIN patient_data AS pd ON b.pid = pd.id 
                            INNER JOIN facility AS f ON f.id = b.clinic_id 
                            INNER JOIN users AS u ON u.id = b.provider_id 
                            INNER JOIN codes AS co ON co.code = b.code AND co.code_type = b.code_type
                            WHERE " . $where . " AND b.deletestate = 1 ORDER BY b.date DESC";
                    
                }
                

                if (!empty($_POST['form_refresh'])) {
                    $event = "Report Treatment View";
                } elseif (!empty($_POST['form_csvexport'])) {
                    $event = "Report Treatment Export";
                }
               

                if ($msgForReportLog) {
                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                } else {
                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                    $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);

                    // resultset array
                    $dataSet = array();
                    while ($rows = sqlFetchArray($res)) {

                        array_push($dataSet, $rows);
                    }
                    debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                } /// End else
            }
            ?>
            <?php
            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvTreatments') {
                ob_end_clean();
                echo '"' . xl('S.No.') . '",';
                echo '"' . xl('Clinic Code') .'",';
                echo '"' . xl('Clinic Name') . '",';
                echo '"' . xl('Date') . '",';
                echo '"' . xl('Doctor Name') . '",';
                if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){ 
                   echo '"' . xl('Invoice Number') . '",';             
                }
                echo '"' . xl('Patient ID') . '",';
                echo '"' . xl('Patient Name') . '",';
                echo '"' . xl('Treatment Category') . '",';
                echo '"' . xl('Treatment name') . '",';
                echo '"' . xl('Treatment Notes') . '",';
                echo '"' . xl('Quantity') . '",';             
                echo '"' . xl('Treatment Cost') . '",';
                echo '"' . xl('Discount') . '",';
                echo '"' . xl('Net Amount') . '"' . "\n";
                if (count($dataSet) > 0) {
                    $sno = 1;
                    foreach ($dataSet as $dataSetK => $dataSetV) {
                        echo '"' . qescape($sno) . '",';
                        echo '"' . qescape($dataSetV['Clinic Code']) . '",';
                        echo '"' . qescape($dataSetV['Clinic']) . '",';
                        echo '"' . qescape($dataSetV['trDate']) . '",';
                        echo '"' . qescape($dataSetV['uname']) . '",';
                        if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){ 
                           echo '"' . qescape($dataSetV['inv_number']) . '",';
                        }
                        echo '"' . qescape($dataSetV['pubpid']) . '",';
                        echo '"' . qescape($dataSetV['fname']) . '",';
                        echo '"' . qescape($dataSetV['trtCategory']) . '",';
                        echo '"' . qescape($dataSetV['code_text']) . '",';
                        echo '"' . qescape($dataSetV['remarks']) . '",';
                        echo '"' . qescape($dataSetV['units']) . '",';
                        echo '"' . qescape($dataSetV['ActualCost']) . '",';
                        echo '"' . qescape($dataSetV['Discount']) . '",';                        
                        echo '"' . qescape($dataSetV['TrCost']) . '",';                        
                        echo '"",' . "\n";
                        $sno++;
                    }
                } else {
                    echo '"No Results Found",' . "\n";
                }
            } else {
                ?>

                <div id="report_results" style="overflow:auto;">

                    <?php if ($_POST['form_refresh']) { ?>
                        <table id="treatmentreporttable" class='emrtable'>

                            <?php if (count($dataSet) > 0) { ?>
                                <thead>
                                <th>&nbsp;<?php xl('S.No.', 'e') ?></th>
                                <th>&nbsp;<?php xl('Clinic Code', 'e') ?></th> 
                                <th>&nbsp;<?php xl('Clinic name', 'e') ?></th>
                                <th>&nbsp;<?php xl('Date', 'e') ?></th>
                                <th>&nbsp;<?php xl('Doctor Name', 'e') ?></th>
                                <?php if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){ ?>
                                    <th>&nbsp;<?php xl('Invoice Number', 'e') ?></th>
                                <?php } ?>
                                <th>&nbsp;<?php xl('Patient ID', 'e') ?></th>
                                <th>&nbsp;<?php xl('Patient Name', 'e') ?></th>
                                <th>&nbsp;<?php xl('Treatment Category', 'e') ?></th>
                                <th>&nbsp;<?php xl('Treatment name', 'e') ?></th>
                                <th>&nbsp;<?php xl('Treatment Notes', 'e') ?></th>
                                <th>&nbsp;<?php xl('Quantity', 'e') ?></th>
                                <th>&nbsp;<?php xl('Treatment Cost', 'e') ?></th> 
                                <th>&nbsp;<?php xl('Discount', 'e') ?></th>
                                <th>&nbsp;<?php xl('Net Amount', 'e') ?></th>

                                </thead>

                                <?php
                                $sno= 1;
                                foreach ($dataSet as $dataSetK => $dataSetV) {
                                    /*$cost = $dataSetV['unit_price'] * $dataSetV['units'];
                                    if ($dataSetV['discount_type'] != '') {
                                        if ($dataSetV['discount_type'] == 'Amt') {
                                            $total = $cost - $dataSetV['discount'];
                                            $totalCost = qescape(round($total));
                                        } else {
                                            $calc_amt = ($cost * $dataSetV['discount']) / 100;
                                            $totalCost = $cost - $calc_amt;
                                        }
                                    } else {
                                        $totalCost = qescape(round($cost));
                                    }*/
                                    ?>
                                    <tr>
                                        <td><?php echo qescape($sno); ?></td>
                                        <td><?php echo qescape($dataSetV['Clinic Code']); ?></td>
                                        <td><?php echo qescape($dataSetV['Clinic']); ?></td>
                                        <td><?php echo qescape($dataSetV['trDate']); ?></td>
                                        <td><?php echo qescape($dataSetV['uname']); ?></td>
                                        <?php if(isset($_POST['invoicetreatmentsearchby']) && $_POST['invoicetreatmentsearchby'] == 1){ ?>
                                            <td><?php echo qescape($dataSetV['inv_number']); ?></td>
                                        <?php } ?>
                                        <td><?php echo qescape($dataSetV['pubpid']); ?></td>
                                        <td><?php echo qescape($dataSetV['fname']); ?></td>
                                        <td><?php echo qescape($dataSetV['trtCategory']); ?></td>
                                        <td><?php echo qescape($dataSetV['code_text']); ?></td>
                                        <td><?php echo qescape($dataSetV['remarks']); ?></td>
                                        <td><?php echo qescape($dataSetV['units']); ?></td>
                                        <td><?php echo qescape($dataSetV['ActualCost']); ?></td>
                                        <td><?php echo qescape($dataSetV['Discount']); ?></td>
                                        <td><?php echo qescape($dataSetV['TrCost']); ?></td>
                                    </tr>
                                    <?php
                                    $sno++;
                                }


                                echo "</table>\n";
                            } else {
                                ?>
                                <table>
                                    <tr>
                                        <td colspan="5" style="text-align:center; font-size:10pt;"> <?php echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
                                    </tr>
                                </table>
                                <?php
                            }
                        } elseif ($_POST['form_refresh']) {
                            ?>
                            <table>
                                <tr>
                                    <td colspan="5" style="text-align:center; font-size:10pt;">No Results Found</td>
                                </tr>
                            </table>
                            <?php
                        }
                        echo "</div>\n";
                        ?>

                        </form>
                        </center>
                        <!-- stuff for the popup calendar -->
                        <style type="text/css">
                            @import url(../../library/dynarch_calendar.css);
                        </style>
                        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
                        <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
                        <script type="text/javascript"
                        src="../../library/dynarch_calendar_setup.js"></script>
                        <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField: "form_from_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_from_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (toDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_from_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                                                    Calendar.setup({
                                                        inputField: "form_to_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_to_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (fromDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_to_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                        </script>
                    <?php } ?>

