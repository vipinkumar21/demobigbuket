<?php
function getAllParentsInv($invoiceId, $db) {
    $returnData = '';
    $returnDate = '';
    $sql = "SELECT inv.inv_number, DATE_FORMAT(inv.inv_created_date,'%Y-%m-%d') as inv_created_date FROM parent_invoice AS pinv INNER JOIN invoice AS inv ON inv.inv_id = pinv.pinv_parent_id WHERE pinv_inv_id = $invoiceId";
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $pLists = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    //$pLists = sqlStatement($sql);
    foreach ($pLists as $prow) {
        if (empty($returnData)) {
            $returnData = $prow['inv_number'];
        } else {
            $returnData .= ', ' . $prow['inv_number'];
        }
        if (empty($returnDate)) {
            $returnDate = $prow['inv_created_date'];
        } else {
            $returnDate .= ', ' . $prow['inv_created_date'];
        }
    }
    return array('invNumbers' => $returnData, 'invDates' => $returnDate);
}

function nonpatientDetail($nonpatientId, $db) {
    $sql = "SELECT npd.np_id AS patientid, npd.np_name AS firstname, npd.np_mobile AS mobile, npd.np_email AS email, pd.pubpid FROM nonpatient_data AS npd LEFT OUTER JOIN patient_data AS pd ON pd.id = npd.ep_id WHERE np_id = $nonpatientId ";
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    //$row = sqlQuery($sql);
    return $row;
}

function getDoctorName($invId, $db){
    $returnData = '';
    $sqlQuery = "SELECT CONCAT_WS(' ', fname, lname) AS dname FROM invoice_items AS ii INNER JOIN billing AS b ON b.id = ii.invit_tp_id INNER JOIN users AS u ON u.id = b.provider_id WHERE ii.`invit_inv_id` = ".$invId." GROUP BY b.provider_id";
    $stmt = $db->prepare($sqlQuery);
    $stmt->execute();
    $pLists = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //$pLists = sqlStatement($sqlQuery);
    foreach ($pLists as $row) {
       if (empty($returnData)) {
            $returnData = $row['dname'];
        } else {
            $returnData .= ', ' . $row['dname'];
        }
    }    
    return $returnData;
}
function getDoctorNameForProductSale($invId, $db){
    $returnData = '';
    $sqlQuery = "SELECT CONCAT_WS(' ', fname, lname) AS dname FROM invoice AS i INNER JOIN  users AS u ON u.id = i.inv_created_by WHERE i.`inv_id` = ".$invId;
    $stmt = $db->prepare($sqlQuery);
    $stmt->execute();
    $pLists = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    //$pLists = sqlStatement($sqlQuery);
    foreach ($pLists as $row) {
       if (empty($returnData)) {
            $returnData = $row['dname'];
        } else {
            $returnData .= ', ' . $row['dname'];
        }
    }    
    return $returnData;
}

function getClinicDetail($clinicId, $db){    
    $sql = "SELECT name, facility_npi FROM facility WHERE id = $clinicId ";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    //$row = sqlQuery($sql);
    return $row;      
}
function invoiceById($uid, $clinicId = NULL, $dbConn = NULL) {
    if(empty($dbConn)) {
        $db = getConnection();
    } else {
        $db = $dbConn;
    }
    $logger = Logger::getLogger("Invoices");
    $logger->info("invoiceById start");
    $paramsArray = getLogedUserDetail();    
    $rows = array();
    if(!is_array($uid)){
         $where = "inv.inv_pid='$uid' AND inv.inv_deletestate != 9";
    }else{
        $where = "inv.inv_npid='$uid[0]' AND inv.inv_deletestate != 9";
    }
    if (!$where) {
        $where = "1 = 1";
    }
    $query = "SELECT inv_id, inv_number, inv_pid, inv_npid, inv_clinic_id, inv_created_by, inv_deletestate, 
inv_created_date, is_printed, parent_inv_id, qchk_status, delete_reseon, pd.fname as pName, pd.pubpid FROM invoice AS inv "
            . "INNER JOIN patient_data AS pd ON pd.id = inv.inv_pid WHERE  " . $where;
    $query .= " UNION SELECT inv_id, inv_number, inv_pid, inv_npid, inv_clinic_id, inv_created_by, inv_deletestate, 
inv_created_date, is_printed, parent_inv_id, qchk_status, delete_reseon, pd.np_name AS pName, 'NA' AS pubpid FROM invoice AS inv "
            . "INNER JOIN nonpatient_data AS pd ON pd.np_id = inv.inv_npid WHERE " . $where;
    
    $eresStmt = $db->prepare($query);
    $eresStmt->execute();
    $eres = $eresStmt->fetchAll(PDO::FETCH_ASSOC);
    
    //$eres = sqlStatement($query);
    
    foreach ($eres as $erow) {
        $inv_type = $erow['inv_deletestate'];
        $patient_id = $erow['inv_pid'];
        $clinicId = $erow['inv_clinic_id'];
        $clinicDetails = getClinicDetail($clinicId, $db);
        $clinicName='';
        $clinicReportId='';    
        if(!empty($clinicDetails)){        
            $clinicName=$clinicDetails['name'];
            $clinicReportId=$clinicDetails['facility_npi'];
        }
        if(!is_array($uid)){
         $drName = getDoctorName($erow['inv_id'], $db);
        }else{
         $drName = getDoctorNameForProductSale($erow['inv_id'], $db);
        }
        
        $svcdate = substr($erow['inv_created_date'], 0, 10);
        $tpQuery = "SELECT invit_id,invit_inv_id,invit_tp_id,invit_ps_id,invit_deleted,invit_startworkdone, bill.code_type, bill.code, bill.code_text, bill.units, bill.fee, bill.discount,"
                . " bill.discount_type, bill.unit_price AS pr_price FROM invoice_items AS invit "
                . "INNER JOIN billing AS bill ON bill.id = invit.invit_tp_id WHERE invit.invit_inv_id = " . $erow['inv_id'];
        if($erow['inv_deletestate'] == 1){
            $tpQuery .= " AND invit.invit_deleted = 0"; 
        }
        $tpQuery .= " GROUP BY invit.invit_tp_id";
        
        $tpStmt = $db->prepare($tpQuery);
        $tpStmt->execute();
        $tpRes = $tpStmt->fetchAll(PDO::FETCH_ASSOC);
        
        //$tpRes = sqlStatement($tpQuery);
        $charges = 0;
        $adjustments = 0;
        $treatments = '';
        $paid = 0;
        $treatSep='';
        $treatSep=$GLOBALS['gbl_invoice_trt_seperator'];
        foreach ($tpRes as $tpRow) {
            $charges += $tpRow['pr_price'] * $tpRow['units'];

            if ($tpRow['discount_type'] == 'Amt') {
                $adjustments += $tpRow['discount'];
            } else {
                $adjustments += round((($tpRow['pr_price'] * $tpRow['units']) / 100) * $tpRow['discount']);
            }

            if (!empty($treatments)) {
                if(isset($tpRow['code_text'])){
                    $treatments .= $tpRow['code_text'] . "$treatSep";
                }
            } else {
                if(isset($tpRow['code_text'])){
                    $treatments = $tpRow['code_text'] . "$treatSep";
                }
            }
        }

        $sql = "SELECT i.inv_id, i.inv_number, i.inv_deletestate, i.inv_created_date, ii.invit_deleted, i.inv_clinic_id, ps.ps_discount, ROUND(ps.ps_discount) AS rounddiscount, ps.ps_discount_type, ps.ps_fee, ROUND(ps.ps_fee) AS roundfee, ps.ps_quantity, ii.invit_deleted, ps.ps_npid, ps.ps_clinic_id, invis.invist_price, invis.invist_quantity, invim.inv_im_name, ps.ps_deletestate ";
        $sql .= "FROM invoice AS i ";
        $sql .= "LEFT JOIN invoice_items AS ii ON ii.invit_inv_id = i.inv_id ";
        $sql .= "LEFT JOIN product_sales AS ps ON ii.invit_ps_id = ps.ps_id ";
        $sql .= "LEFT JOIN inv_item_stock AS invis ON ps.ps_stockid = invis.invist_id ";
        $sql .= "LEFT JOIN inv_item_master AS invim ON invim.inv_im_id = invis.invist_itemid ";
        $sql .= "WHERE i.inv_id = " . $erow['inv_id'] . " AND (ii.invit_tp_id = 0 OR ii.invit_tp_id IS NULL) ";
        
        $proSqlStmt = $db->prepare($sql);
        $proSqlStmt->execute();
        $proRes = $proSqlStmt->fetchAll(PDO::FETCH_ASSOC);
        
        //$proRes = sqlStatement($sql);
        foreach ($proRes as $proRow) {
            $invoiceDiscount = 0;
            if ($proRow['ps_discount_type'] == 'amt') {
                $invoiceDiscount += $proRow['ps_discount'];
            } else {
                $invoiceDiscount += round((($proRow['invist_price'] * $proRow['ps_quantity']) / 100) * $proRow['ps_discount']);
            }
            $adjustments += $invoiceDiscount;
            $charges += $proRow['ps_fee'] + $invoiceDiscount;
            if (!empty($treatments)) {
                if(isset($proRow["inv_im_name"])){
                    $treatments .= $proRow["inv_im_name"] . "$treatSep";
                }
            } else {
                if(isset($proRow["inv_im_name"])){
                    $treatments = $proRow["inv_im_name"] . "$treatSep";
                }
            }
        }
        $dueBalance = 0;
        $paidAmountQuery = "SELECT SUM(rep.rect_amount) AS paidAmt FROM invoice_reciepts AS ir INNER JOIN reciept AS rep ON rep.rect_id = ir.invrect_rect_id WHERE rep.rect_deletestate = 1 AND rep.rect_type != 'Credit' AND ir.invrect_inv_id = " . $erow['inv_id'];
        $paidAmountStmt = $db->prepare($paidAmountQuery);
        $paidAmountStmt->execute();
        $paidAmount = $paidAmountStmt->fetch(PDO::FETCH_ASSOC);
        
        if (!empty($paidAmount['paidAmt'])) {
            $dueBalance = ($charges - $adjustments) - $paidAmount['paidAmt'];
            $paid = $paidAmount['paidAmt'];
        } else {
            $dueBalance = $charges - $adjustments;
        }
        if (isset($_POST['due_amt']) && $_POST['due_amt'] == '1') {
            if ($dueBalance <= 0)
                continue;
        }
        if ($erow['pubpid'] == 'NA') {
            //PATIENT DETAIL
            $patientProfileWidgetList = nonPatientDetail($erow['inv_npid'], $db);
            if (!empty($patientProfileWidgetList['pubpid'])) {
                $erow['pubpid'] = $patientProfileWidgetList['pubpid'];
            } else {
                $erow['pubpid'] = 'NA';
            }
        }        
        $parentInvData = getAllParentsInv($erow['inv_id'], $db);
        $parent_id = $parentInvData['invNumbers'];
        $parentDates = $parentInvData['invDates']; 
        $inv_date = $svcdate;
        $inv_no = $erow['inv_number'];
        $inv_id = $erow['inv_id'];
        $pub_id = $erow['pubpid'];
        $p_name = $erow['pName'];
        $inv_status = $erow['qchk_status'];
        $net_cost = $charges - $adjustments;
        $due_ammount = $net_cost - $paid;
        $sqlQuery = "select invoice_id, inv_id from invoice_calculated where invoice_id = '$inv_no' AND inv_id = '$inv_id'";
        
        $mainStmt = $db->prepare($sqlQuery);
        $mainStmt->execute();
        $proRow = $mainStmt->fetch(PDO::FETCH_ASSOC);
        
        //$sqlQueryRes = sqlStatement($sqlQuery);
        //$proRow = sqlFetchArray($sqlQueryRes);
                
        if($proRow['invoice_id']==$inv_no && $proRow['invoice_id']==$inv_no){
        $query = "update invoice_calculated SET treatment_prod_name = '$treatments', cost = '$charges', disc_cost = '$adjustments', net_cost = '$net_cost',"
                . " due_ammount = '$due_ammount', inv_parent_invoice = '$parent_id', inv_parent_invoice_date = '$parentDates' WHERE invoice_id = '$inv_no' AND inv_id = '$inv_id'"; 
        }
        else{   
        $query = "insert into invoice_calculated values('$inv_date', '$inv_no', '$pub_id', '$p_name', '$treatments', '$charges', '$adjustments', "
                . "'$net_cost', '$due_ammount', '$patient_id', '$clinicId', '$inv_id', '$parent_id', '$inv_status', '$parentDates','$clinicName','$drName','$clinicReportId')";
        }
        $updateStmt = $db->prepare($query);
        $updateStmt->execute();
        //sqlQuery($query); // all record inside invoice_calculated table
        debugPDO($query,array(),'Save Invoice',3,$GLOBALS['enable_auditlog'],$paramsArray['username'],$patient_id);
    }
    // end while
    $logger->info("invoiceById end");
    if(empty($dbConn)) {
        $db = null;
    }
}
?>