<?php
/**
 * Appointment Report (Modified By : Swati(2014-08-22))
 */
require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once "$srcdir/appointments.inc.php";
require_once ("$audting_webroot/auditlog.php");

// Fetch the Provide based on facility (Clinic ID)
function restrict_user_facility($facilList){
	$sql = "SELECT u.id, CONCAT_WS(' ', u.lname, u.fname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	$sql .= " WHERE ";
	if (isset($facilList) && $facilList !== ''){
		$sql .= "uf.facility_id = $facilList ";
	}else {
		$fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = ".$userid." AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
		$fRes = sqlStatement($fsql);
		$facilityIds = '';
		while ($fRow = sqlFetchArray($fRes)) {
			if(!empty($facilityIds)) {
				$facilityIds .= ', '.$fRow['facility_id'];
			}else {
				$facilityIds = $fRow['facility_id'];
			}
		}
		$sql .= "uf.facility_id IN (".$facilityIds.") ";
	}
	$sql .= "AND gag.id IN (12, 13, 18) ";
	return $sql .= " GROUP BY u.id DESC";
	//echo $sql;
	//$ures = sqlStatement($sql);
}

function non_restrict_user_facility($facilList){
	$sql = "SELECT u.id, CONCAT_WS(' ', u.lname, u.fname) as completename ";
	$sql .= "FROM users AS u  ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	return $sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";
	//$ures = sqlStatement($sql);
}

function getWorkLogCheck($pcpid, $evt_date, $sttime, $edtime, $pceid) {
    $strQuery = "SELECT ope.pc_eid,ope.pc_pid,ope.pc_eventDate,ope.pc_startTime,ope.pc_endTime,bl.id,bl.date,wl.wl_created_date,wl_is_modified,ope.pc_delete
                                 FROM openemr_postcalendar_events AS ope 
                                 INNER JOIN billing AS bl ON bl.pid = ope.pc_pid 
                                 INNER JOIN work_log AS wl ON wl.wl_tr_id = bl.id
                                 WHERE wl.wl_is_deleted =0 AND (ope.pc_delete =0 OR ope.pc_delete =2) AND (bl.work_status = 1 OR bl.work_status = 2 OR (bl.work_status = 0 AND bl.isworkdelete=1))
                                 AND ope.pc_pid = $pcpid AND ope.pc_eventDate = '$evt_date'"
                . " AND (DATE(wl.wl_created_date) = '$evt_date')"                                
                . "GROUP BY wl_tr_id";  
        $result = sqlStatement($strQuery);
        
        if (sqlNumRows($result) > 0) {
                while ($row = sqlFetchArray($result)) {
                        //if(($sttime>=$row['pc_startTime'] && $edtime<=$row['pc_endTime'] && $row['pc_delete']==0)){
                                return "Show";	 // if workdone exists in work_log table(Means patients comes at time of appt) 
                        //}else{
                            //return "NoShow";
                        //}
                }	
        }else{
            return "NoShow";
        }	
}

// Prepare a string for CSV export.
function qescape($str) {
  $str = str_replace('\\', '\\\\', $str);
  return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$patient = $_REQUEST['patient'];

$searchParam = '';

if (!empty($_POST['form_from_date'])) {
    $from_date = $_POST['form_from_date'];
} else {
    $from_date = '';
}
if (!empty($_POST['form_to_date'])) {
    $to_date = $_POST['form_to_date'];
} else {
    $to_date = '';
}

$show_available_times = false;
$provider = $_POST['form_provider'];
$facility = $_POST['form_facility'];  //(CHEMED) facility filter
$form_orderby = getComparisonOrder($_REQUEST['form_orderby']) ? $_REQUEST['form_orderby'] : 'date';

$group = $_POST['form_group'];
$refer = $_POST['form_refer'];
$source = $_POST['form_source'];
$status = $_POST['form_status'];

if ($_POST['form_apptType'] == 1) {
    $type = 0;
} elseif ($_POST['form_apptType'] == 2) {
    $type = 1;
} elseif ($_POST['form_apptType'] == 0) {
    $type = '-1';
}
$time = $_POST['form_apptTime'];

// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'appt_report') {

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=appointment_report.csv");
    header("Content-Description: File Transfer");
} else {
    ?>
<html>
<head>
<?php html_header_show();?>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">

<title><?php xl('Appointments Report','e'); ?></title>

<script type="text/javascript" src="../../library/overlib_mini.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

<script type="text/javascript">

 var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

 function dosort(orderby) {
    var f = document.forms[0];
    f.form_orderby.value = orderby;
    f.submit();
    return false;
 }

 function oldEvt(eventid) {
    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
 }

 function refreshme() {
    // location.reload();
    document.forms[0].submit();
 }

$(document).ready(function(){
	$("#form_facility").change(function(){
		//alert($("#facility").val());
		var facilityID = $("#form_facility").val();
		$.ajax({
			url:"get_facility_provider.php?facilityId="+facilityID,
			success:function(result){
				//alert(result);
				$("#form_provider").html(result);
				//alert($("#providerSelectionDataRow").html());
			}
		});
	});
});
</script>

<style type="text/css">
/* specifically include & exclude from printing */
@media print {
	#report_parameters {
		visibility: hidden;
		display: none;
	}
	#report_parameters_daterange {
		visibility: visible;
		display: inline;
	}
	#report_results table {
		margin-top: 0px;
	}
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
}
  .popover_div,.reason_pop{position: relative;cursor: pointer;}
            .clearfix:before {
                clear: both;
                content: " ";
                display: block;
                height: 0;
                visibility: hidden;
            }
            .clearfix:after {
                clear: both;
                content: " ";
                display: block;
                height: 0;
                visibility: hidden;
            }
            .popoverDetailList {
                background: none repeat scroll 0 0 #fff;
                list-style: none outside none;
            }
            .popoverDetailList li {
                float: left;
                width: 48%;
                border-bottom: 1px solid #452311;
                padding: 1%;
            }
             .popover {
                background:#fff;
                border: 1px solid #9a460b;
                border-radius: 0.3em;
                box-shadow: 0 0 18px 1px rgba(50, 50, 50, 0.75);
                color: #000;
                display: none;  word-wrap: break-word; word-break:break-all;
                position: absolute;
                line-height:18px;
                left:-350px;
                padding: 4px;  
                text-align: center;
                width: 400px;
                z-index: 4;
            }
</style>
</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv"
	style="position: absolute; visibility: hidden; z-index: 1000;"></div>

<span class='title'><?php xl('Report','e'); ?> - <?php xl('Appointments','e'); ?></span>

<div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($form_to_date)); ?>
</div>

<form method='post' name='theform' id='theform' action='appointment_report.php'>

<div id="report_parameters">
<input type='hidden' name='form_refresh' id='form_refresh' value=''/>
<input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

<table>
	<tr>
		<td width='80%'>
		

		<table width="100%" class='text'>
			<tr>
				<td width="10%" class='label'><?php xl('Facility','e'); ?>:</td>
				<td width="40%"><?php dropdown_facility(strip_escape_custom($facility), 'form_facility', false, true); ?>
				</td>
				<td width="10%" class='label'><?php xl('Provider','e'); ?>:</td>
				<td width="40%"><?php
				if(empty($facility)){
					$query = "SELECT id, lname, fname FROM users WHERE "."authorized = 1 $provider_facility_filter ORDER BY lname, fname"; 
					$ures = sqlStatement($query);
					echo "   <select name='form_provider' id='form_provider'>\n";
					echo "    <option value=''>-- " . xl('All') . " --\n";
					while ($urow = sqlFetchArray($ures)) {
						$provid = $urow['id'];
						echo "    <option value='$provid'";
						if ($provid == $_POST['form_provider']) echo " selected";
						echo ">" . $urow['lname'] . " " . $urow['fname'] . "\n";
					}
					echo "   </select>\n";
				} else {
					/*
					// Get the providers list.
					$facilList = $facility;
					if(empty($facilList)){
						$facilList = 0;
					}
					*/

					if ($GLOBALS['restrict_user_facility']) {
						$sql = restrict_user_facility($facility);
						$ures = sqlStatement($sql);
					}else {
						$sql = non_restrict_user_facility($facility);
						$ures = sqlStatement($sql);
					}
					
					$returnData = '';
					// default to the currently logged-in user
					$defaultProvider = $_SESSION['authUserID'];
					$returnData.= "<option value='' selected='selected'>Select Provider</option>";
					while ($urow = sqlFetchArray($ures)) {
						$returnData.= "<option value='" . attr($urow['id']) . "'";
						if ($urow['id'] == $provider) $returnData.= " selected";
						$returnData.= ">" . text($urow['completename']);
						//$returnData.= ">" . text($urow['fname']);
						//if ($urow['lname']) $returnData.= " " . text($urow['lname']);
						$returnData.= "</option>";
					}
					echo "   <select name='form_provider' id='form_provider'>\n";
					echo $returnData;
					echo "   </select>\n";
					
				}
				?></td>
			</tr>
			<tr>
				<td class='label'><?php xl('From','e'); ?>:</td>
				<td><input type='text' name='form_from_date' id="form_from_date"
					size='10' value='<?php echo $form_from_date ?>'
					onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)'
					title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
					align='absbottom' width='24' height='22' id='img_from_date'
					border='0' alt='[?]' style='cursor: pointer'
					title='<?php xl('Click here to choose a date','e'); ?>'></td>
				<td class='label'><?php xl('To','e'); ?>:</td>
				<td><input type='text' name='form_to_date' id="form_to_date"
					size='10' value='<?php echo $form_to_date ?>'
					onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)'
					title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
					align='absbottom' width='24' height='22' id='img_to_date'
					border='0' alt='[?]' style='cursor: pointer'
					title='<?php xl('Click here to choose a date','e'); ?>'></td>
			</tr>
            <tr>
				<td class='label'><?php xl('Status','e'); ?>:</td>
           		<td>
           			<select name='form_status' id='form_status' class="text">
                        <option value="" <?php if ($_POST['form_status']=='') {echo " selected";}?>>All</option> 
                        <option value="0" <?php if ($_POST['form_status']==0 && $_POST['form_status'] !='') {echo " selected";}?>>Confirmed</option> 
                        <option value="1" <?php if ($_POST['form_status']==1 && $_POST['form_status'] !='') {echo " selected";}?>>Canceled</option> 
                        <option value="2" <?php if ($_POST['form_status']==2 && $_POST['form_status'] !='') {echo " selected";}?>>No Show</option>
                        <option value="3" <?php if ($_POST['form_status']==3 && $_POST['form_status'] !='') {echo " selected";}?>>Show</option>
                    </select>                                                                                                
                </td>
				<td class='label'><?php xl('Referral','e'); ?>:</td>
                <td>
                    <?php
                    $sql = "select * from list_options WHERE list_id = 'refsource'";
                    $ures = sqlStatement($sql);
                    ?>
                    <select name='form_refer' id='form_refer' class="text">
                        <option value=''>Select referral</option> 
                    <?php   while ($urow = sqlFetchArray($ures)) {
						$seq = $urow['title'];
						echo "    <option value='$seq'";
						if ($seq == $_POST['form_refer']) echo " selected";
						echo ">" . $urow['title'] . "\n";
                    } ?> 
                    </select>
                </td>
			</tr>
            <tr>
				<td class='label'><?php xl('Appt. Type','e'); ?>:</td>
                <td><select name='form_apptType' id='form_apptType' class="text">
                        <option value="0">All</option> 
                        <option value="1" <?php if ($_POST['form_apptType'] == 1){ echo " selected";} ?>>Call Center</option> 
                        <option value="2" <?php if ($_POST['form_apptType'] == 2){ echo " selected"; } ?>>Others</option>                                          
                    </select>                                                                                                
                </td>
				<td class='label'><?php xl('Appt. Time','e'); ?>:</td>
                <td><select name='form_apptTime' id='form_apptTime' class="text">
                        <option value=''>Select Slot</option> 
                        <option value='9-12'<?php if ($_POST['form_apptTime']=='9-12') echo " selected";?>>09 To 12</option> 
                        <option value='12-3'<?php if ($_POST['form_apptTime']=='12-3') echo " selected";?>>12 To 03</option>
                        <option value='3-6'<?php if ($_POST['form_apptTime']=='3-6') echo " selected";?>>03 To 06</option>
                        <option value='6-9'<?php if ($_POST['form_apptTime']=='6-9') echo " selected";?>>06 To 09</option>
                    </select>                                                                                                
                </td>
			</tr>
            <tr>
				<td class='label'><?php xl('Group','e'); ?>:</td>
				<td><input type='text' name='form_group' id="form_group"
					size='10' value='<?php echo $form_group ?>'>
				</td>
                <td class='label'><?php xl('Source','e'); ?>:</td>
                <td>
                    <?php
                    $sql_s = "select option_id, title from list_options WHERE list_id = 'Source_' ORDER BY seq";
                    $ures_s = sqlStatement($sql_s);
                    ?>
                    <select name='form_source' id='form_source' class="text">
                        <option value=''>Select source</option> 
                    <?php   while ($urow = sqlFetchArray($ures_s)) {
						$seq_s = $urow['title'];
						echo "    <option value='$seq_s'";
						if ($seq_s == $_POST['form_source']) echo " selected";
						echo ">" . $urow['title'] . "\n";
                    } ?> 
                    </select>
                </td>
			</tr>
		</table>

		

		</td>
		<td align='left' width="20%" valign='middle' height="100%">
		<table style='border-left: 1px solid; width: 100%; height: 100%'>
			<tr>
				<td>
				<div style='margin-left: 15px'>
                                <a href='#' class='css_button' onclick='$("#form_refresh").attr("value","true");$("#form_csvexport").attr("value",""); $("#theform").submit();'>
                                    
				<span> <?php xl('Submit','e'); ?> </span> </a> 
				<a href='#' class='css_button' onclick='$("#form_csvexport").attr("value","appt_report"); $("#theform").submit();'>
					<span>
						<?php xl('Export to CSV','e'); ?>
					</span>
					</a>
                                <?php /*if ($_POST['form_refresh'] || $_POST['form_orderby'] ) { ?>
				<a href='#' class='css_button' onclick='window.print()'> 
                                    <span> <?php xl('Print','e'); ?> </span> </a> 
                                <a href='#' class='css_button' onclick='window.open("../patient_file/printed_fee_sheet.php?fill=2","_blank")'> 
                                    <span> <?php xl('Superbills','e'); ?> </span> </a> 
                                <?php }*/ ?></div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</div>
<!-- end of search parameters --> <?php
} // end not form_csvexport
if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
if ($_POST['form_csvexport']&& $_POST['form_csvexport']=='appt_report') {
    // CSV headers:
    echo '"' . xl('S. No.') . '",';
    echo '"' . xl('Date') . '",';
    echo '"' . xl('Time') . '",';
    echo '"' . xl('Patient Name') . '",';
    echo '"' . xl('Patient ID') . '",';
    echo '"' . xl('Patient Contact No') . '",';
    echo '"' . xl('Patient Type') . '",';
    echo '"' . xl('Appointment Type') . '",';
    echo '"' . xl('Appointment Status') . '",';
    echo '"' . xl('Reasons') . '",';
    echo '"' . xl('Notes') . '",';
    echo '"' . xl('Doctor Name') . '",';
    echo '"' . xl('Referral') . '",';
    echo '"' . xl('Source') . '",'; 
    echo '"' . xl('Group') . '"' . "\n";
  }
  else {
	?>
<div id="report_results">
<table>

	<thead>
		<th><?php  xl('S. No.','e'); ?></th>

		<th><?php  xl('Date','e'); ?></th>

		<th><?php  xl('Time','e'); ?></th>

		<th><?php  xl('Patient Name','e'); ?></th>

		<th><?php  xl('Patient ID','e'); ?></th>
                <th><?php  xl('Patient Contact No','e'); ?></th>
                <th><?php xl('Patient Type','e');  ?></th>
                <th><?php xl('Appointment Type','e');  ?></th>
                <th><?php xl('Appointment Status','e'); ?></th>
                <th><?php  xl('Reasons','e'); ?></th> 
                <th><?php  xl('Notes','e'); ?></th> 
		<th><?php  xl('Doctor Name','e'); ?></th>

		<th><?php  xl('Referral','e'); ?></th>
                <th><?php  xl('Source','e'); ?></th>
		<th><?php  xl('Group','e'); ?></th>
	</thead>
	<tbody>
		<!-- added for better print-ability -->
	<?php
}
        if(!empty($_POST['form_csvexport'])){
            $event = "Report - Appointment Export";
        }else{
            $event = "Report - Appointment View";
        }
        debugADOReports('','',$event,$GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser']);
        
	$lastdocname = "";
	
        //$appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time );
	#######################
        if ($facility) {
                    //for individual facility
                    if ($from_date) {
                        //if from date selected
                        if ($to_date) {
                            // if $form_date && $form_to_date
                            $toDate=date_create(date($to_date));
                            $fromDate=date_create($from_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;   
                            
                            
                            if($days_between_from_to<=$form_facility_time_range){
                                
                                $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                            }
                            else{
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="appointment_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$from_date,"toDate"=>$to_date,"patient"=>$patient, "provider"=>$provider, "facility"=>$facility,"group"=>$group,"refer"=>$refer,"status"=>$status,"type"=>$type,"time"=>$time)));// changable
                                $rcsl_report_description="Appointment report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                } 
                            }
                        } 
                        else {
                            //only from date: no TO date
                            $to_date=date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($from_date)));
                            $toDate=date_create(date($to_date));
                            $fromDate=date_create($from_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;             
                            
                            if($days_between_from_to<=$form_facility_time_range){
                                $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                            }
                            else{
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                ##########Cron Request###################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="appointment_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$from_date,"toDate"=>$to_date,"patient"=>$patient, "provider"=>$provider, "facility"=>$facility,"group"=>$group,"refer"=>$refer,"status"=>$status,"type"=>$type,"time"=>$time)));// changable
                                $rcsl_report_description="Appointment report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                }                    
                            }
                        }
                    } 
                    else {
                        // if from date not selected
                        if ($to_date) {
                            $from_date=date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($to_date)));
                            $toDate=date_create(date($to_date));
                            $fromDate=date_create($from_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;
                            
                            if($days_between_from_to<=$form_facility_time_range){
                                $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                            }
                            else{
                                if($days_between_from_to<=$form_facility_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="appointment_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$from_date,"toDate"=>$to_date,"patient"=>$patient, "provider"=>$provider, "facility"=>$facility,"group"=>$group,"refer"=>$refer,"status"=>$status,"type"=>$type,"time"=>$time)));// changable
                                $rcsl_report_description="Appointment report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                }                   
                            }
                        } else {
                            $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                        }
                    }
                    
                }
                else {
                    //for all facility
                    if ($from_date) {
                        //if from date selected
                        if ($to_date) {
                            // if $form_date && $form_to_date
                            $toDate=date_create(date($to_date));
                            $fromDate=date_create($from_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;   
                            
                            if($days_between_from_to<=$form_facility_all_time_range){
                                
                                $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                            }
                            else{
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="appointment_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$from_date,"toDate"=>$to_date,"patient"=>$patient, "provider"=>$provider, "facility"=>$facility,"group"=>$group,"refer"=>$refer,"status"=>$status,"type"=>$type,"time"=>$time)));// changable
                                $rcsl_report_description="Appointment report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                }
                            }
                        } 
                        else {
                            //only from date: no TO date
                            $to_date=date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($from_date)));
                            $toDate=date_create(date($to_date));
                            $fromDate=date_create($from_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;             
                            
                            if($days_between_from_to<=$form_facility_all_time_range){
                                $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                            }
                            else{
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="appointment_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$from_date,"toDate"=>$to_date,"patient"=>$patient, "provider"=>$provider, "facility"=>$facility,"group"=>$group,"refer"=>$refer,"status"=>$status,"type"=>$type,"time"=>$time)));// changable
                                $rcsl_report_description="Appointment report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                }
                            }
                        }
                    } 
                    else {
                        // if from date not selected
                        if ($to_date) {
                            $from_date=date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($to_date)));
                            $toDate=date_create(date($to_date));
                            $fromDate=date_create($from_date);
                            $diff=date_diff($fromDate,$toDate);
                            $days_between_from_to=$diff->days;
                            
                            if($days_between_from_to<=$form_facility_all_time_range){
                                $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                            }
                            else{
                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type="appointment_report"; // changable
                                $rcsl_requested_filter=addslashes(serialize(array("fromDate"=>$from_date,"toDate"=>$to_date,"patient"=>$patient, "provider"=>$provider, "facility"=>$facility,"group"=>$group,"refer"=>$refer,"status"=>$status,"type"=>$type,"time"=>$time)));// changable
                                $rcsl_report_description="Appointment report from $form_date to $form_to_date";// changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type,$rcsl_requested_filter,$rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog = $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                    }
                                ##########Cron Request####################section only for all FACILITY#########################
                                }
                                else{
                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                }
                            }
                        } else {
                            $appointments = fetchProAppointments( $from_date, $to_date, $patient, $provider, $facility,$group,$refer,$status,$type,$time , $source );
                        }
                    }
                    
                }
                
        #######################
        //echo"<pre>";print_r($appointments);
        for($i=0;$i<count($appointments);$i++){
            if($appointments[$i]['pc_delete'] == 0){
                $appt_conf[] = $appointments[$i];
            }elseif($appointments[$i]['pc_delete'] == 3){
                $appt_show[] = $appointments[$i];
            }elseif($appointments[$i]['pc_delete'] == 1){
                $appt_cancel[] = $appointments[$i];
            }else{
                $appt_noshow[] = $appointments[$i];
            }
            
        }
        
        //echo"<pre>";print_r($appt_conf);
        $pid_list = array();  // Initialize list of PIDs for Superbill option
        $listing = array();
        $srCount = 1;
        if($status==3 && $status!=''){
            $listing = $appt_show ;
        }elseif($status==2 && $status!=''){
            $listing = $appt_noshow ;
        }elseif($status==1 && $status!=''){
            $listing = $appt_cancel ;
        }elseif($status==0 && $status!=''){
            $listing = $appt_conf ;
        }else {
            $listing = $appointments ;
        }
        //echo"<pre>";print_r($listing);
	if($msgForReportLog==""){
            foreach ( $listing as $appointment ) {
                array_push($pid_list,$appointment['pid']);
		$patient_id = $appointment['pid'];
		$docname  = $appointment['ufname'] . ' ' . $appointment['ulname'];
                
                $errmsg  = "";
		$appSataus = '';
		if($appointment['pc_delete'] == 1){
			$appSataus = 'Cancelled';
		}elseif($appointment['pc_delete'] == 2){
                        $appSataus = 'No Show';
                }elseif($appointment['pc_delete'] == 3){
                        $appSataus = 'Show';
                }else{
                    $appSataus = 'Confirmed';
                }
                if($appointment['pc_createdgroup'] == 0){
                        $aptType = 'Call Center';
                }else{
                    $aptType = 'Others';
                } 
                if(is_numeric($appointment['pc_comments_description'])){
                        $sql = "SELECT title
                                FROM list_options
                                WHERE list_id='Appt_Cancel_Noshow_reasons' AND option_id = ".$appointment['pc_comments_description'];
                        $res = sqlStatement( $sql );
                        $reason = sqlFetchArray($res);
                        $notes = $reason['title'];
                }else{
                    $notes = $appointment['pc_comments_description'];
                } 
                if($appointment['pc_referral']=='pleaseSelect'){
                    $referrral ='';
                }else{
                    $referrral = $appointment['pc_referral'];
                }
                
                if($appointment['pc_patient_source']=='pleaseSelect'){
                    $source ='';
                }else{
                    $source = $appointment['pc_patient_source'];
                }


	if ($_POST['form_csvexport']) {
    echo '"' . qescape($srCount) . '",';
    echo '"' . qescape(oeFormatShortDate($appointment['pc_eventDate'])) . '",';
    echo '"' . qescape(oeFormatTime($appointment['pc_startTime'])) . '",';
    echo '"' . qescape($appointment['fname'] . " " . $appointment['lname']) . '",';
    echo '"' . qescape($appointment['pubpid']) . '",';
    echo '"' . qescape($appointment['phone_cell']) . '",';
    echo '"' . qescape($appointment['pc_patient_status']) . '",';
    echo '"' . qescape($aptType) . '",';
    echo '"' . qescape($appSataus) . '",';
    echo '"' . qescape($notes) . '",';
    echo '"' . qescape($appointment['pc_appnotes']) . '",';
    echo '"' . qescape($docname) . '",'; 
    echo '"' . qescape($referrral) . '",'; 
    echo '"' . qescape($source) . '",';
    echo '"' . qescape($appointment['group']) . '"' . "\n";
   }
   else {
	?>

	<tr bgcolor='<?php echo $bgcolor ?>'>
		<td class="detail">&nbsp;<?php echo $srCount; ?></td>
		<td class="detail"><?php echo oeFormatShortDate($appointment['pc_eventDate']); ?></td>
		<td class="detail"><?php echo oeFormatTime($appointment['pc_startTime']); ?></td>
		<td class="detail">&nbsp;<?php echo $appointment['fname'] . " " . $appointment['lname']; ?></td>
		<td class="detail">&nbsp;<?php echo $appointment['pubpid']; ?></td>
                <td class="detail">&nbsp;<?php echo $appointment['phone_cell']; ?></td>
		<td class="detail">&nbsp;<?php echo $appointment['pc_patient_status']; ?></td>
                <td class="detail">&nbsp;<?php echo $aptType;?></td>
		<td class="detail">&nbsp;<?php echo $appSataus;?></td>
                <td class="detail">&nbsp;<?php echo $notes;?></td>
                <td class="detail reason_pop">&nbsp;<?php echo substr($appointment['pc_appnotes'], 0,6); if($appointment['pc_appnotes'] != ''){
                    ?><div class="popover"><?php echo $appointment['pc_appnotes']; ?></div>
                <?php } ?>
                </td>        
		<td class="detail">&nbsp;<?php echo $docname; ?></td>
		<td class="detail">&nbsp;<?php echo $referrral;?></td>
                <td class="detail">&nbsp;<?php echo $source;?></td>
		<td class="detail">&nbsp;<?php echo $appointment['group'];?></td>
	</tr>

	<?php
	}
	$lastdocname = $docname;
	$srCount++;
	}
        }
        else{
            
            ?>
            <tr bgcolor='<?php echo $bgcolor ?>'>
                <td colspan="13"><div align="center"><?php echo $msgForReportLog; ?></div></td>
            </tr>
            <?php
        }
	// assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
        $_SESSION['pidList'] = $pid_list;
	if (!$_POST['form_csvexport']) {
	?>
	</tbody>
</table>
</div>
<!-- end of search results --> <?php } } else { ?>
<div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e' ); ?>
</div>
	<?php } ?> 
	<?php
	if (!$_POST['form_csvexport']) {
	?>
	<input type="hidden" name="form_orderby"
	value="<?php echo $form_orderby ?>" /> <input type="hidden"
	name="patient" value="<?php echo $patient ?>" /> <input type='hidden'
	name='form_refresh' id='form_refresh' value='' /></form>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript">

<?php
if ($alertmsg) { echo " alert('$alertmsg');\n"; }
?>

</script>

</body>

<!-- stuff for the popup calendar -->
<style type="text/css">
    @import url(../../library/dynarch_calendar.css);
</style>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript"
	src="../../library/dynarch_calendar_setup.js"></script>
<script type="text/javascript">
	Calendar.setup({
		inputField:"form_from_date"
		,ifFormat:"%Y-%m-%d"
		,button:"img_from_date"
		,onUpdate: function() {
			var   toDate = document.getElementById('form_to_date').value,
		      fromDate = document.getElementById('form_from_date').value;

			if(toDate){
			   var   toDate = new Date(toDate),
			         fromDate = new Date(fromDate);

			   if(fromDate > toDate){
			      alert('From date cannot be later than To date.');
			      document.getElementById('form_from_date').value = '';
			   }
			}
		}
	});
	Calendar.setup({
		inputField:"form_to_date"
		,ifFormat:"%Y-%m-%d"
		,button:"img_to_date"
		,onUpdate: function() {
			var   toDate = document.getElementById('form_to_date').value,
		      fromDate = document.getElementById('form_from_date').value;

			if(fromDate){
			   var   toDate = new Date(toDate),
			         fromDate = new Date(fromDate);

			   if(fromDate > toDate){
			      alert('From date cannot be later than To date.');
			      document.getElementById('form_to_date').value = '';
			   }
			}
		}
	});
$('.reason_pop').hover(function () {
                    $(this).children('.popover').show();
                }, function () {
                    $(this).children('.popover').hide();
                });
</script>

</html>
<?php
}
?>