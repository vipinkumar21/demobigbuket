<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");

$zoneId = $_REQUEST['zoneId'];
$regionId = $_REQUEST['regionId'];
$facilityId = $_REQUEST['facilityId'];
if (!empty($regionId) && empty($zoneId)) {
    $zone = getRegionWiseZones($regionId);
    $facility = getRegionWiseClinics($regionId);
} elseif (empty($regionId) && !empty($zoneId)) {
    $facility = getZoneWiseClinics($zoneId);
    $zone = getAllZones();
} elseif (!empty($regionId) && !empty($zoneId)) {
    $facility = getZoneWiseClinics($zoneId);
    $zone = getRegionWiseZones($regionId);
} else {
    $zone = getAllZones();
    $facility = getFacilityList();
}
$returnDataZone = '';
$returnDataFacility = '';
$returnDataZone.= "<option value='' selected='selected'>-- All Zone --</option>";
foreach ($zone as $key => $value) {
    if ($zoneId) {
        if ($zoneId == $value['id']) {
            $returnDataZone.= "<option selected='selected' value='" . $value['id'] . "'";
        } else {
            $returnDataZone.= "<option value='" . $value['id'] . "'";
        }
    } else {
        $returnDataZone.= "<option value='" . $value['id'] . "'";
    }
    $returnDataZone.= ">" . $value['zone_title'];
    $returnDataZone.= "</option>";
}
$returnDataFacility.= "<option value=''>-- All Facilities --</option>";
foreach ($facility as $fkey => $fvalue) {
    if ($facilityId) {
        if ($facilityId == $fvalue['id']) {
            $returnDataFacility.= "<option selected='selected' value='" . $fvalue['id'] . "'";
        } else {
            $returnDataFacility.= "<option value='" . $fvalue['id'] . "'";
        }
    } else {
        $returnDataFacility.= "<option value='" . $fvalue['id'] . "'";
    }
    $returnDataFacility.= ">" . $fvalue['name'];
    $returnDataFacility.= "</option>";
}
$data = array($returnDataZone, $returnDataFacility);
echo json_encode($data);
?>