<?php
require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once ("$audting_webroot/auditlog.php");

//fetch all facility: start
$sql = "SELECT id from facility WHERE id!='-1'";
$facRes = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
$listfacility = array();
$treatSep='';
$treatSep=$GLOBALS['gbl_invoice_trt_seperator'];
while ($proRow = sqlFetchArray($facRes)) {
    array_push($listfacility, $proRow['id']);
}
$allFacilityId = implode(",", $listfacility);

//fetch all facility: end
// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

function nonpatientDetail($nonpatientId) {
    $sql = "SELECT npd.np_id AS patientid, npd.np_name AS firstname, npd.np_mobile AS mobile, npd.np_email AS email, pd.pubpid FROM nonpatient_data AS npd LEFT OUTER JOIN patient_data AS pd ON pd.id = npd.ep_id WHERE np_id = $nonpatientId ";
    $row = sqlQuery($sql);
    return $row;
}

$from_date = fixDate($_POST['form_from_date'], '');
$to_date = fixDate($_POST['form_to_date'], '');
$SearchByInvoiceNumber = $_POST['SearchByInvoiceNumber'];

function bucks($amt) {
    return ($amt != 0.00) ? oeFormatMoney($amt) : '';
}

$searchParam = '';
$fileName = "invoice_and_payment_report_".date("Ymd_his").".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Front Office Receipts', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
        <link rel="stylesheet" type="text/css" href="../../interface/themes/bootstrap.css" />
            <script language="JavaScript">

    <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

                // The OnClick handler for receipt display.
                function show_receipt(pid, timestamp, encounter) {
                    dlgopen('../patient_file/front_payment.php?receipt=1&encounter=' + encounter + '&patient=' + pid +
                            '&time=' + timestamp, '_blank', 550, 400);
                }

            </script>

            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" class='panel panel-warning' style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Invoice & Payment', 'e'); ?></h3>

            <div id="report_parameters_daterange">
                <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form class='emrform topnopad' name='theform' method='post' action='invoice_payment.php' id='theform'>

                <div id="report_parameters" style="padding-left:0px;padding-right:0px;">
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input  type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='76%'>
                                <div style='float:left; width:100%;'>

                                    <table width="100%" class='text'>
                                        <tr>
                                            <td width="13%" class='label newfontnormal newrel10'>
                                                <?php xl('Payment Method', 'e'); ?>:
                                            </td>
                                            <td width="27%">
                                                <select id="form_method" class="text emrinput" name="form_method">
                                                    <option value="">Payment Method</option>
                                                    <option value="Check" <?php
                                                    if ($_POST['form_method'] == 'Check') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Check Payment</option>
                                                    <option value="Cash" <?php
                                                    if ($_POST['form_method'] == 'Cash') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Cash</option>
                                                    <option value="Card" <?php
                                                    if ($_POST['form_method'] == 'Card') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Card</option>
                                                    <option value="Netbanking" <?php
                                                    if ($_POST['form_method'] == 'Netbanking') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Netbanking</option>
                                                </select>

                                            </td>
                                            <td width="5%" class='label newfontnormal newrel10'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="35%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>
                                            <td width="20%"><input class='emrdate' type="text" name="SearchByPatientID" value="<?php echo $_POST['SearchByPatientID']; ?>" placeholder="Search by Patient ID" style='width:100%; height:100%'></td>                                        

                                        </tr>

                                        <tr>
                                            <td class='label newfontnormal newrel10'>
                                                <?php xl('From', 'e'); ?>:
                                            </td>
                                            <td>
                                                <input class='emrdate' type='text' name='form_from_date' id="form_from_date" size='10' value='<?php echo $form_from_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_from_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                            <td class='label newfontnormal newrel10'>
                                                <?php xl('To', 'e'); ?>:
                                            </td>
                                            <td>
                                                <input class='emrdate' type='text' name='form_to_date' id="form_to_date" size='10' value='<?php echo $form_to_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_to_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                            <td><input class='emrdate' type="text" name="SearchByInvoiceNumber" value="<?php echo $_POST['SearchByInvoiceNumber']; ?>" placeholder="Search by Invoice Number" style='width:100%; height:100%'></td>                                        
                                        </tr>

                                    </table>

                                </div>

                            </td>
                            <td width="24%" align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");$("#form_csvexport").attr("value", "");
                                                            $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "true");$("#form_refresh").attr("value", "");
                                                            $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php if ($_POST['form_refresh']) { ?>
                                                    <a href='#' class='btn btn-warning btn-sm' onclick='window.print()'>
                                                        <span>
                                                            <?php xl('Print', 'e'); ?>
                                                        </span>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport
            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport']) {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Date') . '",';
                    echo '"' . xl('Clinic Name') . '",';
                    echo '"' . xl('Doctor Name') . '",';
                    echo '"' . xl('Patient ID') . '",';
                    echo '"' . xl('Patient Name') . '",';
                    echo '"' . xl('Invoice Number') . '",';
                    echo '"' . xl('Receipt Number') . '",';
                    echo '"' . xl('Treatments done') . '",';
                    echo '"' . xl('Cost') . '",';
                    echo '"' . xl('Discount') . '",';
                    echo '"' . xl('Net Amount') . '",';
                    echo '"' . xl('Paid') . '",';
                    echo '"' . xl('Payment Mode') . '"' . "\n";
                } else {
                    ?>
                    <div id="report_results" class="table-responsive">
                        <table class='emrtable table table-bordered' id='invoicepaymenttable'>
                            <thead>
                            <th width="1%"> <?php xl('S.N.', 'e'); ?> </th>
                            <th width="7%"> <?php xl('Date', 'e'); ?> </th>
                            <th width="5%"> <?php xl('Clinic Name', 'e'); ?> </th>
                            <th width="6%"> <?php xl('Doctor Name', 'e'); ?> </th>
                            <th width="6%"> <?php xl('Patient ID', 'e'); ?> </th>
                            <th width="6%"> <?php xl('Patient Name', 'e'); ?> </th>
                            <th width="5%"> <?php xl('Invoice Number', 'e'); ?> </th>
                            <th width="8%"> <?php xl('Receipt Number', 'e'); ?> </th>
                            <th width="8%"> <?php xl('Treatments done', 'e'); ?> </th>
                            <th width="5%"> <?php xl('Cost', 'e'); ?> </th>
                            <th width="5%"> <?php xl('Discount', 'e'); ?> </th>
                            <th width="5%"> <?php xl('Net Amount', 'e'); ?> </th>
                            <th width="5%"> <?php xl('Paid', 'e'); ?> </th>
                            <th width="6%"> <?php xl('Payment Mode', 'e'); ?> </th>
                            </thead>

                            <tbody>
                                <?php
                            }
                            if (true || $_POST['form_refresh']) {
                                $form_facility = $_POST['form_facility'];
                                $form_method = $_POST['form_method'];
                                $form_date = $from_date;
                                $form_to_date = $to_date;
                                //echo $from_date."------".$to_date;
                                if ($form_facility) {
                                    //for individual facility
                                    if ($form_date) {
                                        //if from date selected
                                        if ($form_to_date) {
                                            // if $form_date && $form_to_date
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;


                                            if ($days_between_from_to <= $form_facility_time_range) {

                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id = '$form_facility'";
                                            } 
                                            else {
                                                if($days_between_from_to<=$form_facility_time_range_valid){
                                                if (!empty($form_method)) { $form_method = $form_method; }    
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "invoice_payment_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "form_method"=>$form_method, "facility"=>$form_facility))); // changable
                                                $rcsl_report_description = "Invoice & Payment report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            //only from date: no TO date
                                            $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id = '$form_facility'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_time_range_valid){
                                                if (!empty($form_method)) { $form_method = $form_method; }    
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "invoice_payment_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "form_method"=>$form_method, "facility"=>$form_facility))); // changable
                                                $rcsl_report_description = "Invoice & Payment report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                                }
                                            }
                                        }
                                    } else {
                                        // if from date not selected
                                        if ($form_to_date) {
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id = '$form_facility'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_time_range_valid){
                                                if (!empty($form_method)) { $form_method = $form_method; }    
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "invoice_payment_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "form_method"=>$form_method, "facility"=>$form_facility))); // changable
                                                $rcsl_report_description = "Invoice & Payment report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            if ($conditions) {
                                                $conditions .= " AND ";
                                            }
                                            $form_to_date = date("Y-m-d");
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                            $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id = '$form_facility'";
                                        }
                                    }
                                    
                                    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param  
                                    
                                } else {
                                    //for all facility
                                    if ($form_date) {
                                        //if from date selected
                                        if ($form_to_date) {
                                            // if $form_date && $form_to_date
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;
                                                
                                            if ($days_between_from_to <= $form_facility_all_time_range) {

                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id IN ($allFacilityId)";
                                            
                                                
                                            } 
                                            else {
                                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                                if (!empty($form_method)) { $form_method = $form_method; }    
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "invoice_payment_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "form_method"=>$form_method))); // changable
                                                $rcsl_report_description = "Invoice & Payment report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            //only from date: no TO date
                                            $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_all_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id IN ($allFacilityId)";
                                            } else {
                                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                                if (!empty($form_method)) { $form_method = $form_method; }    
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "invoice_payment_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "form_method"=>$form_method))); // changable
                                                $rcsl_report_description = "Invoice & Payment report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                                }
                                            }
                                        }
                                    } else {
                                        // if from date not selected
                                        if ($form_to_date) {
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_all_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id IN ($allFacilityId)";
                                            } else {
                                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                                if (!empty($form_method)) { $form_method = $form_method; }    
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "invoice_payment_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "form_method"=>$form_method))); // changable
                                                $rcsl_report_description = "Invoice & Payment report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            if ($conditions) {
                                                $conditions .= " AND ";
                                            }
                                            $form_to_date = date("Y-m-d");
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                            $conditions .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND r.rect_clinic_id IN ($allFacilityId)";
                                        }
                                    }
                                    
                                     $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param  
                                }
                                if (!empty($form_method)) {
                                    $conditions .= " AND r.rect_mode='$form_method'";
                                    
                                    $searchParam .= ' Payment Method = ' . $_POST['form_method'] . ' |';
                                }
                                //search by Invice no
                                if ($_POST['SearchByInvoiceNumber'] != "") {
                                    $conditions = "inv.invoice_id='{$_POST['SearchByInvoiceNumber']}'";
                                    
                                    $searchParam .= ' Invoice Number = ' . $_POST['SearchByInvoiceNumber'] . ' |';
                                }
                                //search by patient id
                                if ($_POST['SearchByPatientID'] != "") {
                                    $conditions = "p.pubpid='{$_POST['SearchByPatientID']}'";
                                    
                                     $searchParam .= ' Patient ID = ' . $_POST['SearchByPatientID'] . ' |';
                                }



                                $sql = "SELECT pr.rect_number AS releasedReceiptNumber, p.pubpid, p.fname, p.mname, p.lname, inv.invoice_id, r.rect_number, inv.treatment_prod_name, 
                                inv.cost, inv.disc_cost, inv.net_cost, r.rect_amount, r.rect_mode, r.rect_clinic_id, r.rect_created_date, f.name AS facility_name, u.username AS doctor_name
                                FROM reciept AS r LEFT OUTER JOIN patient_data AS p ON p.id = r.rect_pid LEFT OUTER JOIN 
                                invoice_reciepts AS ir ON ir.invrect_rect_id = r.rect_id 
                                LEFT JOIN `reciept`AS pr ON pr.rect_id = r.rect_prect_id  
                                LEFT OUTER JOIN invoice_calculated AS inv ON 
                                inv.inv_id = ir.invrect_inv_id LEFT OUTER JOIN facility AS f ON f.id=r.rect_clinic_id 
                                LEFT OUTER JOIN users AS u ON u.id=r.rect_created_by WHERE  $conditions";

                                $sql .= " ORDER BY r.rect_created_date DESC";
                                
                                if (!empty($_POST['form_refresh'])) {
                                    $event = "Report Invoice & Payment View";
                                } elseif (!empty($_POST['form_csvexport'])) {
                                    $event = "Report Invoice & Payment Export";
                                }

                                if ($msgForReportLog) {

                                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                                } else {

                                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                                }
                                
                                if($msgForReportLog=="")
                                {
                                $proRes = sqlStatement($sql, null, $GLOBALS['adodb']['dbreport']);
                                $sno = 1;
                                while ($proRow = sqlFetchArray($proRes)) {
                                    $docarray=array('0'=>'','1'=>'');
                                    $docarray=explode('.',$proRow['doctor_name']);
                                    $proRow['doctor_name']=$docarray['0']." ".$docarray['1'];
                                    if (empty($_POST['searchparm']) || (isset($_POST['searchparm']) && !empty($_POST['searchparm']) && $_POST['searchparm'] == $dueAmount)) {
                                        if ($_POST['form_csvexport']) {
                                            if ($proRow['invoice_id']) {
                                               
                                                if($proRow['releasedReceiptNumber'] != ''){
                                                    $rect_number = $proRow['rect_number'].', Parent:'.$proRow['releasedReceiptNumber'];
                                                }else{
                                                     $rect_number = $proRow['rect_number'];
                                                }
                                                
                                                echo '"' . qescape($sno) . '",';
                                                echo '"' . qescape($proRow['rect_created_date']) . '",';
                                                echo '"' . qescape($proRow['facility_name']) . '",';
                                                echo '"' . qescape($proRow['doctor_name']) . '",';
                                                echo '"' . qescape($proRow['pubpid']) . '",';
                                                echo '"' . qescape($proRow['fname']) . '",';
                                                echo '"' . qescape($proRow['invoice_id']) . '",';
                                                echo '"' . qescape($rect_number).'",';
                                                echo '"' . qescape(trim(str_replace(array('\n',"$treatSep"), '/', $proRow['treatment_prod_name']), ',')) . '",';
                                                echo '"' . qescape($proRow['cost']) . '",';
                                                echo '"' . qescape($proRow['disc_cost']) . '",';
                                                echo '"' . qescape($proRow['net_cost']) . '",';
                                                echo '"' . qescape($proRow['rect_amount']) . '",';
                                                echo '"' . qescape($proRow['rect_mode']) . '"' . "\n";
                                            } else {
                                                if($proRow['releasedReceiptNumber'] != ''){
                                                    $rect_number = $proRow['rect_number'].', Parent:'.$proRow['releasedReceiptNumber'];
                                                }else{
                                                     $rect_number = $proRow['rect_number'];
                                                }
                                                echo '"' . qescape($sno) . '",';
                                                echo '"' . qescape($proRow['rect_created_date']) . '",';
                                                echo '"' . qescape($proRow['facility_name']) . '",';
                                                echo '"' . qescape($proRow['doctor_name']) . '",';
                                                echo '"' . qescape($proRow['pubpid']) . '",';
                                                echo '"' . qescape($proRow['fname']) . '",';
                                                echo '"' . qescape("NA") . '",';
                                                echo '"' . qescape($rect_number).'",';
                                                echo '"' . qescape("NA") . '",';
                                                echo '"' . qescape("NA") . '",';
                                                echo '"' . qescape("NA") . '",';
                                                echo '"' . qescape("NA") . '",';
                                                echo '"' . qescape($proRow['rect_amount']) . '",';
                                                echo '"' . qescape($proRow['rect_mode']) . '"' . "\n";
                                            }
                                        } else {
                                            ?>
                    <?php if ($proRow['invoice_id']) { ?>
                                                <tr>
                                                    <td><?php echo $sno; ?></td>
                                                    <td><?php echo $proRow['rect_created_date']; ?></td>
                                                    <td><?php echo $proRow['facility_name']; ?></td>
                                                    <td><?php echo $proRow['doctor_name'] ?></td>
                                                    <td><?php echo $proRow['pubpid']; ?></td>
                                                    <td><?php echo $proRow['fname'] . $proRow['mname'] . $proRow['lname']; ?></td>
                                                    <td><?php echo $proRow['invoice_id']; ?></td>
                                                    <td><?php echo $proRow['rect_number']; ?><?php echo ($proRow['releasedReceiptNumber'] !='')?'<br />Parent:'.$proRow['releasedReceiptNumber']:'' ?></td>
                                                    <td><?php echo stripslashes($proRow['treatment_prod_name']); ?></td>
                                                    <td><?php echo $proRow['cost']; ?></td>
                                                    <td><?php echo $proRow['disc_cost']; ?></td>
                                                    <td><?php echo $proRow['net_cost']; ?></td>
                                                    <td><?php echo $proRow['rect_amount']; ?></td>
                                                    <td><?php echo $proRow['rect_mode']; ?></td>
                                                </tr>
                        <?php
                    } else {
                        ?>
                                                <tr>
                                                    <td><?php echo $sno; ?></td>
                                                    <td><?php echo $proRow['rect_created_date']; ?></td>
                                                    <td><?php echo $proRow['facility_name']; ?></td>
                                                    <td><?php echo $proRow['doctor_name']; ?></td>
                                                    <td><?php echo $proRow['pubpid']; ?></td>
                                                    <td><?php echo $proRow['fname'] . $proRow['mname'] . $proRow['lname']; ?></td>
                                                    <td>NA</td>
                                                    <td><?php echo $proRow['rect_number']; ?><?php echo ($proRow['releasedReceiptNumber'] !='')?'<br />Parent:'.$proRow['releasedReceiptNumber']:'' ?></td>
                                                    <td>NA</td>
                                                    <td>NA</td>
                                                    <td>NA</td>
                                                    <td>NA</td>
                                                    <td><?php echo $proRow['rect_amount']; ?></td>
                                                    <td><?php echo $proRow['rect_mode']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        $count++;
                                    }
                                    $sno++;
                                }
                                
                                
                                    debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                              
                                
                            }  else{
                                
                                                if($_POST['form_csvexport']){
                                                echo strip_tags($msgForReportLog);
                                                }
                                                else{ 
                                                    ?>
                                                <tr><td colspan="14" align='center'><?php echo $msgForReportLog;?></td></tr>
                                                    <?php
                                                }
                            
                                }
                            }
                            if (!$_POST['form_csvexport']) {
                                ?>
                            </tbody>
                        </table>
                    </div> <!-- end of results -->
                        <?php
                    }
                } else {
                    ?>
                <div class='text'>
                <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                </div>
    <?php
}
if (!$_POST['form_csvexport']) {
    ?>

            </form>
        </body>
        <!-- stuff for the popup calendar -->
        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>

        <script type="text/javascript">
                                                        Calendar.setup({
                                                            inputField: "form_from_date"
                                                            , ifFormat: "%Y-%m-%d"
                                                            , button: "img_from_date"
                                                            , onUpdate: function () {
                                                                var toDate = document.getElementById('form_to_date').value,
                                                                        fromDate = document.getElementById('form_from_date').value;

                                                                if (toDate) {
                                                                    var toDate = new Date(toDate),
                                                                            fromDate = new Date(fromDate);

                                                                    if (fromDate > toDate) {
                                                                        jAlert('From date cannot be later than To date.');
                                                                        document.getElementById('form_from_date').value = '';
                                                                    }
                                                                }
                                                            }
                                                        });
                                                        Calendar.setup({
                                                            inputField: "form_to_date"
                                                            , ifFormat: "%Y-%m-%d"
                                                            , button: "img_to_date"
                                                            , onUpdate: function () {
                                                                var toDate = document.getElementById('form_to_date').value,
                                                                        fromDate = document.getElementById('form_from_date').value;

                                                                if (fromDate) {
                                                                    var toDate = new Date(toDate),
                                                                            fromDate = new Date(fromDate);

                                                                    if (fromDate > toDate) {
                                                                        jAlert('From date cannot be later than To date.');
                                                                        document.getElementById('form_to_date').value = '';
                                                                    }
                                                                }
                                                            }
                                                        });
        </script>
    </html>
    <?php
}
?>