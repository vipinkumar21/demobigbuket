<?php
/**
 *
 * Appointment And Shows Report: (Swati Jain)
 *
 * */
require_once("../globals.php");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once ("$audting_webroot/auditlog.php");

$form_facility = empty($_POST['form_facility']) ? 0 : intval($_POST['form_facility']);
$form_region = empty($_POST['form_region']) ? 0 : intval($_POST['form_region']);
$form_zone = empty($_POST['form_zone']) ? 0 : intval($_POST['form_zone']);
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
$form_agent = trim($_REQUEST['form_agent']);


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (($form_facility != '') && ($form_agent != '')) { // One filter can be choosen b/w facility and agent
        $msg = 'At one time,You can choose any one filter between Facility and Agent';
        echo "<html>\n<body>\n<script language='JavaScript'>\n";
        if ($msg)
            echo " alert('" . addslashes($msg) . "');\n";
        echo " window.close();\n";
        echo "window.location.href = 'appt_shows_report.php'";
        echo "</script>\n</body>\n</html>\n";
        exit();
    }else {

        $yr = date('Y'); // year is for current year
        $searchParam = '';

        if ($form_facility) { 
            //for individual facility
            if ($form_date) {
                //if from date selected
                if ($form_to_date) {
                    // if $form_date && $form_to_date
                    $toDate = date_create(date($form_to_date));
                    $fromDate = date_create($form_date);
                    $diff = date_diff($fromDate, $toDate);
                    $days_between_from_to = $diff->days;


                    if ($days_between_from_to <= $form_facility_time_range) {

                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";                        
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_facility = '$form_facility'";                        
                    } else {
                        if($days_between_from_to<=$form_facility_time_range_valid){
                        ##########Cron Request####################section only for all FACILITY#########################
                        // following value will be change according to report
                        $rcsl_name_type = "appointment_show_report"; // changable
                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "agent" => $form_agent, "facility"=>$form_facility ,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                        $rcsl_report_description = "Appointment Show report from $form_date to $form_to_date"; // changable
                        //allFacilityReports() defined with globals.php
                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                        if ($msgForReportLog) {
                            $msgForReportLog = $msgForReportLog;
                        } else {
                            $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                        }
                        ##########Cron Request####################section only for all FACILITY#########################
                        }
                        else{
                        $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                        }
                    }
                } else {
                    //only from date: no TO date
                    $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                    $toDate = date_create(date($form_to_date));
                    $fromDate = date_create($form_date);
                    $diff = date_diff($fromDate, $toDate);
                    $days_between_from_to = $diff->days;

                    if ($days_between_from_to <= $form_facility_time_range) {
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_facility = '$form_facility'";
                    } else {
                        if($days_between_from_to<=$form_facility_time_range_valid){
                        ##########Cron Request####################section only for all FACILITY#########################
                        // following value will be change according to report
                        $rcsl_name_type = "appointment_show_report"; // changable
                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "agent" => $form_agent, "facility"=>$form_facility, "facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                        $rcsl_report_description = "Appointment Show report from $form_date to $form_to_date"; // changable
                        //allFacilityReports() defined with globals.php
                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                        if ($msgForReportLog) {
                            $msgForReportLog = $msgForReportLog;
                        } else {
                            $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                        }
                        ##########Cron Request####################section only for all FACILITY#########################
                        }
                        else{
                        $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                        }
                    }
                }
            } else {
                // if from date not selected
                if ($form_to_date) {
                    $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                    $toDate = date_create(date($form_to_date));
                    $fromDate = date_create($form_date);
                    $diff = date_diff($fromDate, $toDate);
                    $days_between_from_to = $diff->days;

                    if ($days_between_from_to <= $form_facility_time_range) {
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_facility = '$form_facility'";
                    } else {
                        if($days_between_from_to<=$form_facility_time_range_valid){
                        ##########Cron Request####################section only for all FACILITY#########################
                        // following value will be change according to report
                        $rcsl_name_type = "appointment_show_report"; // changable
                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "agent" => $form_agent, "facility"=>$form_facility ,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                        $rcsl_report_description = "Appointment Show report from $form_date to $form_to_date"; // changable
                        //allFacilityReports() defined with globals.php
                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                        if ($msgForReportLog) {
                            $msgForReportLog = $msgForReportLog;
                        } else {
                            $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                        }
                        ##########Cron Request####################section only for all FACILITY#########################
                        }
                        else{
                        $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                        }
                    }
                } else {
                    if ($where) {
                        $where .= " AND ";
                    }
                    $form_to_date = date("Y-m-d");
                    $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                    $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " pc.pc_facility = '$form_facility'";
                }
            }
            
            $searchParam .= 'From Date = '.$form_date.' | To Date = '.$form_to_date.' | ClinicId = '.$form_facility; /// Auditing Section Param
            
        } else {           
            //for all facility
            if ($form_date) {
                //if from date selected
                if ($form_to_date) {
                    // if $form_date && $form_to_date
                    $toDate = date_create(date($form_to_date));
                    $fromDate = date_create($form_date);
                    $diff = date_diff($fromDate, $toDate);
                    $days_between_from_to = $diff->days;

                    if ($days_between_from_to <= $form_facility_all_time_range) {

                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                    } else {
                        if($days_between_from_to<=$form_facility_all_time_range_valid){
                        ##########Cron Request####################section only for all FACILITY#########################
                        // following value will be change according to report
                        $rcsl_name_type = "appointment_show_report"; // changable
                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "agent" => $form_agent ,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                        $rcsl_report_description = "Appointment Show report from $form_date to $form_to_date"; // changable
                        //allFacilityReports() defined with globals.php
                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                        if ($msgForReportLog) {
                            $msgForReportLog = $msgForReportLog;
                        } else {
                            $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                        }
                        ##########Cron Request####################section only for all FACILITY#########################
                        }
                        else{
                        $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                        }
                    }
                } else {
                    //only from date: no TO date
                    $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                    $toDate = date_create(date($form_to_date));
                    $fromDate = date_create($form_date);
                    $diff = date_diff($fromDate, $toDate);
                    $days_between_from_to = $diff->days;

                    if ($days_between_from_to <= $form_facility_all_time_range) {
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                    } else {
                        if($days_between_from_to<=$form_facility_all_time_range_valid){
                        ##########Cron Request####################section only for all FACILITY#########################
                        // following value will be change according to report
                        $rcsl_name_type = "appointment_show_report"; // changable
                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "agent" => $form_agent,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                        $rcsl_report_description = "Appointment Show report from $form_date to $form_to_date"; // changable
                        //allFacilityReports() defined with globals.php
                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                        if ($msgForReportLog) {
                            $msgForReportLog = $msgForReportLog;
                        } else {
                            $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                        }
                        ##########Cron Request####################section only for all FACILITY#########################
                        }
                        else{
                        $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                        }
                    }
                }
            } else {
                // if from date not selected
                if ($form_to_date) {
                    $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                    $toDate = date_create(date($form_to_date));
                    $fromDate = date_create($form_date);
                    $diff = date_diff($fromDate, $toDate);
                    $days_between_from_to = $diff->days;

                    if ($days_between_from_to <= $form_facility_all_time_range) {
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                    } else {
                        if($days_between_from_to<=$form_facility_all_time_range_valid){
                        ##########Cron Request####################section only for all FACILITY#########################
                        // following value will be change according to report
                        $rcsl_name_type = "appointment_show_report"; // changable
                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "agent" => $form_agent ,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                        $rcsl_report_description = "Appointment Show report from $form_date to $form_to_date"; // changable
                        //allFacilityReports() defined with globals.php
                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                        if ($msgForReportLog) {
                            $msgForReportLog = $msgForReportLog;
                        } else {
                            $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                        }
                        ##########Cron Request####################section only for all FACILITY#########################
                        }
                        else{
                        $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                        }
                    }
                } else {
                    if ($where) {
                        $where .= " AND ";
                    }
                    $form_to_date = date("Y-m-d");
                    $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                    $where .= " pc.pc_eventDate BETWEEN '$form_date' AND '$form_to_date'";
                }
            }
            
            $searchParam .= 'From Date = '.$form_date.' | To Date = '.$form_to_date.' | Clinic = All'; /// Auditing Section Param
        }

        if (!$where) {
            $where = "1 = 1";
        }
        // agent filters
        if ((isset($form_agent)) && ($form_agent != '') && ($form_facility == '')) {
            if ($where) {
                $where .= " AND ";
            }
            $where .= '  pc.pc_createdby =' . $form_agent;
            
           $searchParam .= ' Agent Id = '.$form_agent.' | '; 
        }
        if(!empty($form_region) && empty($form_zone))
        {
            if ($where) {
                $where .= " AND ";
            }
            $where.=" f.region_id='$form_region'";
        }
        if(!empty($form_region) && !empty($form_zone))
        {
            if ($where) {
                $where .= " AND ";
            }
            $where.=" f.region_id='$form_region' AND f.zone_id='$form_zone'";
        }
        if(empty($form_region) && !empty($form_zone))
        {
            if ($where) {
                $where .= " AND ";
            }
            $where.=" f.zone_id='$form_zone'";
        }
        $sql = " SELECT pc.pc_eid,pc.pc_pid,pc.pc_eventDate,pc.pc_facility,pc.pc_delete,pc.pc_createdgroup,pc.pc_startTime,pc.pc_endTime 
                    FROM openemr_postcalendar_events AS pc 
                    INNER JOIN facility AS f ON f.id=pc.pc_facility
                    WHERE  " . $where . " AND pc.pc_operatory != 0 AND pc.pc_delete != 1 ";
        $sql .= ' ORDER BY pc.pc_eventDate ';

        if (!empty($_POST['form_refresh'])) {
            $event = "Report Appointment Shows View";
        } elseif (!empty($_POST['form_csvexport'])) {
            $event = "Report Appointment Shows Export";
        }
        
                
        if ($msgForReportLog) {
            
            $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam,2); // Cron Schedule Report Auditing Section
             
        } else {
            
            $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
            
            $res = sqlStatement($sql, null, $GLOBALS['adodb']['dbreport']);
            while ($fac_row = sqlFetchArray($res)) {
                $facility_arr[date('M-Y', strtotime($fac_row['pc_eventDate']))][$fac_row['pc_eventDate']][$fac_row['pc_facility']][] = array("pc_eid" => $fac_row['pc_eid']
                    , "pc_pid" => $fac_row['pc_pid']
                    , "pc_eventDate" => $fac_row['pc_eventDate']
                    , "pc_facility" => $fac_row['pc_facility']
                    , "pc_createdgroup" => $fac_row['pc_createdgroup']
                    , "pc_delete" => $fac_row['pc_delete']
                    , "pc_startTime" => $fac_row['pc_startTime']
                    , "pc_endTime" => $fac_row['pc_endTime']
                );
            }
            
            debugADOReportsUpdate($auditid,$GLOBALS['enable_auditlog']); /// Update Report Auditing Section
        }
        // Calculation related cce and non cce for appt and shows	
        // pc_delete : 0-appt ; 1-delete/cancel; 2-No Show
        if (count($facility_arr) > 0) {
            foreach ($facility_arr as $k => $vupper) {
                foreach ($vupper as $kmiddle => $vmiddle) {
                    foreach ($vmiddle as $k1 => $v1) {
                        $show_new = $cc_apt = $cc_show = $cl_apt = $cl_show = $apt_new = $show_new = 0;
                        // slots wise appt/Show calculation
                        for ($i = 0; $i < count($v1); $i++) {
                            if ($v1[$i]['pc_createdgroup'] == 0) { // for CCE
                                //if ($v1[$i]['pc_delete'] == 3 || $v1[$i]['pc_delete'] == 0) { // if CCE appt
                                $cc_apt++;
                                if ($v1[$i]['pc_delete'] == 3) {
                                    $cc_show++;
                                }
                                //}
                            } else { // for Non CCE
                                //if ($v1[$i]['pc_delete'] == 3 || $v1[$i]['pc_delete'] == 0) { // if Non CCE appt
                                    $cl_apt++;
                                    if ($v1[$i]['pc_delete'] == 3) {
                                        $cl_show++;
                                    }
                                //}
                            }
                            // either appt is new or not
                            if (newPatient($v1[$i]['pc_pid'], $v1[$i]['pc_eid']) == 'New') {
                                $apt_new++;
                            }
                            // either show appt is new or not
                            //if ($v1[$i]['pc_delete'] == 3 || $v1[$i]['pc_delete'] == 0) {
                            if (newPatient($v1[$i]['pc_pid'], $v1[$i]['pc_eid']) == 'New') {
                                if ($v1[$i]['pc_delete'] == 3) {
                                    $show_new++;
                                }
                            }
                            //}
                        }

                        // Main array display
                        $main_arr[$k][] = array("Date" => $kmiddle
                            , "Clinic" => $k1
                            , "CC" => $cc_apt
                            , "CL" => $cl_apt
                            , "New" => $apt_new
                            , "CCShow" => $cc_show
                            , "CLShow" => $cl_show
                            , "NewShow" => $show_new
                        );
                    }
                }
            }//die;
        }
    }// end if of agent and facilty check
}//  end if "post check"		
// fetching clinic names

function get_facilty_name($k) {
    $query = "SELECT name from facility WHERE id=" . $k;
    $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);
    $urow = sqlFetchArray($res);
    return $urow['name'];
}

// patient is new/old
function newPatient($pid, $eid) {
    $strQuery = "SELECT pc_pid FROM openemr_postcalendar_events  WHERE pc_pid =? AND pc_eid < ? ";
    $result = sqlStatement($strQuery, array($pid, $eid), $GLOBALS['adodb']['dbreport']);
    if (sqlNumRows($result) > 0) {
        return 'Old';
    }
    return 'New';
}

//for csv purpose
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$bgcolor = "#aaaaaa";
$fileName = "appt_shows_report_".date("Ymd_his").".csv";
if ((isset($_POST['form_csvexport'])) && ($_POST['form_csvexport'] == 'csvSlots')) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>
        <head>
            <?php if (function_exists('html_header_show')) html_header_show(); ?>
            <link rel=stylesheet href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('Appointment-Show Report', 'e') ?></title>
            <script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

            <style type="text/css">

                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }

            </style>
            <script type="text/javascript"> 
            $(document).ready(function(){
                    $("#form_region").change(function(){
                        changeRegion();
                    });
                    $("#form_zone").change(function(){
                        changeZone();
                    });
                    var selectedRegionAjax='<?php echo $form_region ; ?>';
                    var selectedZoneAjax='<?php echo $form_zone; ?>';
                    var selectedFacilityAjax='<?php echo $form_facility; ?>';
                    if(selectedRegionAjax=='' && selectedZoneAjax!=''){
                        changeZone();
                    }else if(selectedRegionAjax!='' && selectedZoneAjax=='')
                    {
                        changeRegion();
                    }else if(selectedRegionAjax!='' && selectedZoneAjax!=''){
                        changeZone();
                    }
                    function changeRegion()
                    {
                        var regionId = $("#form_region").val();
                        $.ajax({
                                url:"get_facility_zones.php?regionId="+regionId+"&facilityId="+selectedFacilityAjax,
                                success:function(result){
                                        var result =  $.parseJSON(result);
                                        $("#form_zone").html(result[0]);
                                        $("#form_facility").html(result[1]);
                                }
                        });
                    }
                    function changeZone()
                    {
                        var zoneId = $("#form_zone").val();
                        var regionId = $("#form_region").val();
                        $.ajax({
                                url:"get_facility_by_zones.php?zoneId="+zoneId+"&facilityId="+selectedFacilityAjax+"&regionId="+regionId,
                                success:function(result){
                                        var result =  $.parseJSON(result);
                                            $("#form_zone").html(result[0]);
                                            $("#form_facility").html(result[1]);
                                }
                        });
                    }
            });
                
                </script>
        </head>

        <body class="body_top">

            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Appointment Shows', 'e'); ?></h3>

            <form method='post' class='emrform topnopad' action='appt_shows_report.php' enctype='multipart/form-data' id='theform'>

                <div id="report_parameters">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width: 100%;'>
                                    <table class='text'>
                                        <tr>
                                            <td>
                                                <table>
                                                        <td width="5%" class='label'>
                                                            <?php xl('Region', 'e'); ?>:
                                                        </td>
                                                        <td width="20%">
                                                            <?php dropdownRegions(strip_escape_custom($form_region), 'form_region'); ?>
                                                        </td>
                                                        <td width="5%" class='label'>
                                                            <?php xl('Zone', 'e'); ?>:
                                                        </td>
                                                        <td width="20%">
                                                            <?php
                                                            dropdownZones(strip_escape_custom($form_zone), 'form_zone');
                                                            ?>
                                                        </td>


                                            </tr>
                                            <tr>
                                                <td width="5%" class='label'>
                                                    <?php xl('Facility', 'e'); ?>:
                                                </td>
                                                <td width="20%">
                                                    <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                                </td>
                                                <td class='label'>
                                                    <?php xl('Agent', 'e'); ?>:
                                                </td>

                                                <td>
                                                    <?php
                                                    $agnt_sql = "SELECT u.id AS loggedid,u.fname,u.lname, gag.id AS groupid, gag.value AS groupname
								FROM users AS u 
								INNER JOIN gacl_aro AS ga ON ga.value=u.username
								INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id 
								INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id 
								WHERE gag.id  != 22 AND u.authorized = '1' AND u.active = '1'";

                                                    $sql_res = sqlStatement($agnt_sql, null, $GLOBALS['adodb']['dbreadonly']);
                                                    ?>
                                                    <select class='emrinput' name="form_agent" id="form_agent" class="text">
                                                        <option value="" >Select Agent</option>";
                                                        <?php while ($agnt_row = sqlFetchArray($sql_res)) { ?>
                                                            <option value="<?php echo $agnt_row['loggedid']; ?>" <?php
                                                            if ($_POST['form_agent'] == $agnt_row['loggedid']) {
                                                                echo 'selected';
                                                            }
                                                            ?>><?php echo $agnt_row['fname'] . " " . $agnt_row['lname']; ?></option>\n";
                                                                <?php } ?>
                                                    </select>			   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width='10%' class='label'><?php xl('From', 'e'); ?>:</td>
                                                <td width='40%'><input class='emrdate' type='text' name='form_from_date' id="form_from_date"
                                                                       size='10' value='<?php echo $form_date ?>'
                                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                       title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                       align='absbottom' width='24' height='22' id='img_from_date'
                                                                       border='0' alt='[?]' style='cursor: pointer'
                                                                       title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                                <td width='10%' class='label'><?php xl('To', 'e'); ?>:</td>
                                                <td width='40%'><input class='emrdate' type='text' name='form_to_date' id="form_to_date"
                                                                       size='10' value='<?php echo $form_to_date ?>'
                                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                       title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                       align='absbottom' width='24' height='22' id='img_to_date'
                                                                       border='0' alt='[?]' style='cursor: pointer'
                                                                       title='<?php xl('Click here to choose a date', 'e'); ?>'></td>

                                            </tr>
                                        </table>
                                            </td>
                                        </tr>


                                    </table>

                                </div>	
                            </td>
                            <td width='20%' align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>	
                                                <?php if (1) { ?>
                                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvSlots");
                                                            $("#theform").submit();'>
                                                        <span>
                                                            <?php xl('Export to CSV', 'e'); ?>
                                                        </span>
                                                    </a>
                                                <?php } ?>					
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="text">* Please select a filter to see the results.</div>
                
            <?php } // end of csv section  ?>
            <?php
            ?>
            <?php
            if (isset($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvSlots') {

                echo xl('Date') . ',';
                echo '"' . xl('Clinic') . '",';
                echo '"",';
                echo '"' . xl('Total appointment') . '",';
                echo '"",';
                echo '"",';
                echo '"' . xl('Total Shows') . '",';
                echo '"",' . "\n";
                echo '"",';
                echo '"",';
                echo '"' . xl('Call centre appts.') . '",';
                echo '"' . xl('Clinic appts.') . '",';
                echo '"' . xl('New appts.') . '",';
                echo '"' . xl('Call centre shows') . '",';
                echo '"' . xl('Clinic shows') . '",';
                echo '"' . xl('New appt shows') . '",' . "\n";
                $cc_t = $cl_t = $new_t = $ccshow_t = $clshow_t = $newshow_t = 0; // Total row display
                if (count($main_arr) > 0) {

                    foreach ($main_arr AS $k => $v) {
                        foreach ($v AS $k1 => $v1) {
                            echo '"' . qescape($v1['Date']) . '",';
                            echo '"' . qescape(get_facilty_name($v1['Clinic'])) . '",';
                            echo '"' . qescape($v1['CC']) . '",';
                            echo '"' . qescape($v1['CL']) . '",';
                            echo '"' . qescape($v1['New']) . '",';
                            echo '"' . qescape($v1['CCShow']) . '",';
                            echo '"' . qescape($v1['CLShow']) . '",';
                            echo '"' . qescape($v1['NewShow']) . '",' . "\n";

                            $cc_t += $v1['CC'];
                            $cl_t += $v1['CL'];
                            $new_t += $v1['New'];
                            $ccshow_t += $v1['CCShow'];
                            $clshow_t += $v1['CLShow'];
                            $newshow_t += $v1['NewShow'];
                        }
                        echo '"' . qescape($k) . '",';
                        echo '"Total",';
                        echo '"' . $cc_t . '",';
                        echo '"' . $cl_t . '",';
                        echo '"' . $new_t . '",';
                        echo '"' . $ccshow_t . '",';
                        echo '"' . $clshow_t . '",';
                        echo '"' . $newshow_t . '",' . "\n";
                    }
                } else {
                    echo '"No Results Found",' . "\n";
                }
            } else {
                ?>

                <div id="report_results">

                    <?php if (isset($_POST['form_refresh'])) { ?>
                        <table class='emrtable' border="1">
                            <?php if (count($main_arr) > 0) { ?>

                                <thead>
                                <th>&nbsp;<?php xl('Date', 'e') ?></th>
                                <th>&nbsp;<?php xl('Clinic', 'e') ?></th>
                                <th  colspan="3" style="text-align:center">&nbsp;<?php xl('Total appointment', 'e') ?></th> 
                                <th  colspan="3" style="text-align:center">&nbsp;<?php xl('Total Shows', 'e') ?></th>
                                </thead>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><b>Call centre appts.</b><br></td>
                                    <td><b>Clinic appts.</b><br></td>
                                    <td><b>New appts.</b></td>
                                    <td><b>Call centre shows</b><br></td>
                                    <td><b>Clinic shows</b><br></td>
                                    <td><b>New appt. shows</b><br></td>
                                </tr>


                                <?php
                                foreach ($main_arr AS $k => $v) {
                                    $cc_t = $cl_t = $new_t = $ccshow_t = $clshow_t = $newshow_t = 0; // Total row display  
                                    ?>

                                    <?php foreach ($v AS $k1 => $v1) { ?>

                                        <tr>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['Date']; ?>
                                            </td>

                                            <td class="detail">
                                                &nbsp;<?php echo get_facilty_name($v1['Clinic']); ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['CC']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['CL']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['New']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['CCShow']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['CLShow']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['NewShow']; ?>
                                            </td>
                                            <?php
                                            $cc_t += $v1['CC'];
                                            $cl_t += $v1['CL'];
                                            $new_t += $v1['New'];
                                            $ccshow_t += $v1['CCShow'];
                                            $clshow_t += $v1['CLShow'];
                                            $newshow_t += $v1['NewShow'];
                                            ?>
                                        </tr>

                                        <?php
                                    } // outer for loop 
                                    ?>
                                    <tr bgcolor='<?php echo $bgcolor ?>'>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $k; ?></b>
                                        </td> 
                                        <td class="detail">
                                            &nbsp;<b>Total</b>&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $cc_t; ?></b>&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $cl_t; ?></b>&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $new_t; ?></b>&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $ccshow_t; ?></b>&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $clshow_t; ?></b>&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;<b><?php echo $newshow_t; ?></b>&nbsp;
                                        </td>

                                    </tr>
                                    <?php
                                }// foreach	
                                echo "</table>\n";
                            } else {
                                ?>
                                <table>
                                    <tr>
                                        <td colspan="5" style="text-align:center; font-size:10pt;"><?php echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
                                    </tr>
                                </table>
                                <?php
                            }
                        } elseif (isset($_POST['form_refresh'])) {
                            ?>
                            <table>
                                <tr>
                                    <td colspan="5" style="text-align:center; font-size:10pt;">No Results Found</td>
                                </tr>
                            </table>
                            <?php
                        }
                        echo "</div>\n";
                        ?>

                        </form>
                        </center>
                        <!-- stuff for the popup calendar -->
                        <style type="text/css">
                            @import url(../../library/dynarch_calendar.css);
                        </style>
                        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
                        <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
                        <script type="text/javascript"
                        src="../../library/dynarch_calendar_setup.js"></script>
                        <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField: "form_from_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_from_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (toDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_from_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                                                    Calendar.setup({
                                                        inputField: "form_to_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_to_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (fromDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_to_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                        </script>
                    <?php } ?>

