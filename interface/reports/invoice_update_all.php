<?php

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2080M');
$ignoreAuth = 1;
require_once("../globals.php");

//truncate table invoice_calculated
sqlQuery("truncate table invoice_calculated");


function getAllParentsInv($invoiceId) {
    $returnData = '';
    $sql = "SELECT inv.inv_number FROM parent_invoice AS pinv INNER JOIN invoice AS inv ON inv.inv_id = pinv.pinv_parent_id WHERE pinv_inv_id = $invoiceId";
    $pLists = sqlStatement($sql);
    while ($prow = sqlFetchArray($pLists)) {
        if (empty($returnData)) {
            $returnData = $prow['inv_number'];
        } else {
            $returnData .= ', ' . $prow['inv_number'];
        }
    }
    return $returnData;
}


$rows = array();
$where = "";

if (!$where) {
    $where = "1 = 1";
}

$query = "SELECT inv.*, pd.fname as pName, pd.pubpid FROM invoice AS inv "
            . "INNER JOIN patient_data AS pd ON pd.id = inv.inv_pid WHERE  " . $where;
$query .= " UNION SELECT inv.*, pd.np_name AS pName, 'NA' AS pubpid FROM invoice AS inv "
            . "INNER JOIN nonpatient_data AS pd ON pd.np_id = inv.inv_npid WHERE " . $where;
$eres = sqlStatement($query);


while ($erow = sqlFetchArray($eres)) {
    $patient_id = $erow['inv_pid'];
    $clinicId = $erow['inv_clinic_id'];
    $inv_type = $erow['inv_deletestate'];

    $svcdate = substr($erow['inv_created_date'], 0, 10);
    $tpQuery = "SELECT invit.*, bill.code_type, bill.code, bill.code_text, bill.units, bill.fee, bill.discount, bill.discount_type, bill.unit_price AS pr_price FROM invoice_items AS invit INNER JOIN billing AS bill ON bill.id = invit.invit_tp_id WHERE invit.invit_inv_id = " . $erow['inv_id'] . " GROUP BY invit.invit_tp_id";
    $tpRes = sqlStatement($tpQuery);
    $charges = 0;
    $adjustments = 0;
    $treatments = '';
    $paid = 0;
    while ($tpRow = sqlFetchArray($tpRes)) {
        $charges += $tpRow['pr_price'] * $tpRow['units'];

        if ($tpRow['discount_type'] == 'Amt') {
            $adjustments += $tpRow['discount'];
        } else {
            $adjustments += round((($tpRow['pr_price'] * $tpRow['units']) / 100) * $tpRow['discount']);
        }

        if (!empty($treatments)) {
            $treatments .= $tpRow['code_text'] . '\n';
        } else {
            $treatments = $tpRow['code_text'] . '\n';
        }
    }

    $sql = "SELECT i.inv_id, i.inv_number, i.inv_deletestate, i.inv_created_date, ii.invit_deleted, i.inv_clinic_id, ps.ps_discount, ROUND(ps.ps_discount) AS rounddiscount, ps.ps_discount_type, ps.ps_fee, ROUND(ps.ps_fee) AS roundfee, ps.ps_quantity, ii.invit_deleted, ps.ps_npid, ps.ps_clinic_id, invis.invist_price, invis.invist_quantity, invim.inv_im_name, ps.ps_deletestate ";
    $sql .= "FROM invoice AS i ";
    $sql .= "LEFT JOIN invoice_items AS ii ON ii.invit_inv_id = i.inv_id ";
    $sql .= "LEFT JOIN product_sales AS ps ON ii.invit_ps_id = ps.ps_id ";
    $sql .= "LEFT JOIN inv_item_stock AS invis ON ps.ps_stockid = invis.invist_id ";
    $sql .= "LEFT JOIN inv_item_master AS invim ON invim.inv_im_id = invis.invist_itemid ";
    $sql .= "WHERE i.inv_id = " . $erow['inv_id'] . " AND (ii.invit_tp_id = 0 OR ii.invit_tp_id IS NULL) ";

    $proRes = sqlStatement($sql);
    while ($proRow = sqlFetchArray($proRes)) {
        $invoiceDiscount = 0;
        if ($proRow['ps_discount_type'] == 'amt') {
            $invoiceDiscount += $proRow['ps_discount'];
        } else {
            $invoiceDiscount += round((($proRow['invist_price'] * $proRow['ps_quantity']) / 100) * $proRow['ps_discount']);
        }

        $adjustments += $invoiceDiscount;
        $charges += $proRow['ps_fee'] + $invoiceDiscount;

        if (!empty($treatments)) {
            $treatments .= $proRow["inv_im_name"] . '<br>';
        } else {
            $treatments = $proRow["inv_im_name"] . '<br>';
        }
    }

    $dueBalance = 0;

    $paidAmount = sqlQuery("SELECT SUM(rep.rect_amount) AS paidAmt FROM invoice_reciepts AS ir INNER JOIN reciept AS rep ON rep.rect_id = ir.invrect_rect_id WHERE rep.rect_deletestate = 1 AND rep.rect_type != 'Credit' AND ir.invrect_inv_id = " . $erow['inv_id']);

    if (!empty($paidAmount['paidAmt'])) {
        $dueBalance = ($charges - $adjustments) - $paidAmount['paidAmt'];
        $paid = $paidAmount['paidAmt'];
    } else {
        $dueBalance = $charges - $adjustments;
    }

    if (isset($_POST['due_amt']) && $_POST['due_amt'] == '1') {
        if ($dueBalance <= 0)
            continue;
    }
    if ($erow['pubpid'] == 'NA') {
        //PATIENT DETAIL
        $patientProfileWidgetList = nonPatientDetail($erow['inv_npid']);
        if (!empty($patientProfileWidgetList['pubpid'])) {
            $erow['pubpid'] = $patientProfileWidgetList['pubpid'];
        } else {
            $erow['pubpid'] = 'NA';
        }
    }

    $row = array();

    $row['id'] = $erow['inv_id'];
    echo $parent_id = getAllParentsInv($row['id']);
    $row['invnumber'] = $erow['inv_number'];
    $row['custid'] = $patient_id;
    $row['name'] = $erow['pName'];
    $row['dos'] = $svcdate;
    $row['pid'] = $erow['inv_pid'];
    $row['pubpid'] = $erow['pubpid'];
    $row['irnumber'] = $erow['inv_number'];
    $row['charges'] = $charges;
    $row['treatments'] = $treatments;
    $row['adjustments'] = $adjustments;
    $row['paid'] = $paid;
    $rows[$erow['inv_id']] = $row;

    $inv_date = $svcdate;
    $inv_no = $erow['inv_number'];
    $inv_id = $erow['inv_id'];
    $pub_id = $erow['pubpid'];
    $p_name = $erow['pName'];

    $net_cost = $charges - $adjustments;
    $due_ammount = $net_cost - $paid;
    $row['dues'] = $due_ammount;
    $p_name = addslashes($p_name);
    $treatments = addslashes($treatments);

    $query = "insert into invoice_calculated values(
						'$inv_date',
						'$inv_no',
						'$pub_id',
						'$p_name',
						'$treatments',
						'$charges',
						'$adjustments',
						'$net_cost',
						'$due_ammount',
						'$patient_id',
						'$clinicId',
                                                '$inv_id',
                                                '$parent_id'
						)";
 
    echo $query;
    
    sqlQuery($query); // all record inside invoice_calculated table
//echo "<pre>";
//print_r($row);
//echo "</pre>";
}// end while
?>
