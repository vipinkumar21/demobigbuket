<?php
// Copyright (C) 2013 Virendra Dubey <virendra.dubey@instantsys.com>
// This ageing report lists patients that were have due amounts.

require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

/**
 * Returns an Due Payment data array
 * @param integer patient id
 * @param string start date
 * @param string end date
 * @return data array
 */
/* -------------------------CSV export start ------------------ */

$form_facility = empty($_POST['form_facility']) ? 0 : intval($_POST['form_facility']);

$searchParam = '';
$fileName = "AgeingReport_".date("Ymd_his").".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
    <?php html_header_show(); ?>
            <title><?php xl('Patient List', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

            <script language="JavaScript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
            </script>
            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">

                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                        margin-bottom: 10px;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                    #report_results {
                        width: 100%;
                    }
                }

            </style>

        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <h3 class='emrh3'><?php xl('Ageing - Report', 'e'); ?></h3>

            <div id="report_parameters_daterange">
    <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form name='theform' id='theform' method='post' action='ageing_report.php' class='emrform topnopad'>

                <div id="report_parameters" class="">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width: 100%'>
                                    <table class='text'>
                                        <tr>
                                            <td width="3.33%" class='label'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="30%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>	
                                            <td width="66.67"></td>				
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td width="20%" align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "true");
                                                        $("#form_refresh").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>					
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport

            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport']) {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Patient Name') . '",';
                    echo '"' . xl('Patient ID') . '",';
                    echo '"' . xl('Due upto 1 month') . '",';
                    echo '"' . xl('Due for 1 - 2 months') . '",';
                    echo '"' . xl('Due for 2 - 6 months') . '",';
                    echo '"' . xl('Due for more than 6 months') . '",';
                    echo '"' . xl('Treatment/product for which Amount due') . '",';
                    echo '"' . xl('Total Due Amount') . '"' . "\n";
                } else {
                    ?>

                    <div id="report_results">
                        <table class='emrtable'>
                            <thead>
                            <th> <?php xl('S.N.', 'e'); ?> </th>
                            <th> <?php xl('Patient Name', 'e'); ?> </th>
                            <th> <?php xl('Patient ID', 'e'); ?> </th>
                            <th> <?php xl('Due upto 1 month', 'e'); ?> </th>
                            <th> <?php xl('Due for 1 - 2 months', 'e'); ?> </th>
                            <th> <?php xl('Due for 2 - 6 months', 'e'); ?> </th>
                            <th> <?php xl('Due for more than 6 months', 'e'); ?> </th>
                            <th> <?php xl('Treatment/product for which Amount due', 'e'); ?> </th>
                            <th> <?php xl('Total Due Amount', 'e'); ?> </th>
                            </thead>
                            <tbody>
                                <?php
                            } // end not export
                            $totalpts = 0;
                            $ageingDataArray = array();
                            if (!empty($form_facility)) {
                                $pquery = "SELECT ar.ageing_sno, ar.patient_name,ar.ageing_pubpid,ar.next_appointment,ar.due_upto1_month,ar.due_for1_2months,ar.due_2_6months,ar.due_more_6months,ar.treatment_Product,ar.due_amount,ar.ageing_state,ar.ageing_pid FROM patient_facilities as pf INNER JOIN ageing_reports as ar ON ar.ageing_pid = pf.pf_pid WHERE pf.pf_fid = '" . $form_facility . "' and ar.ageing_state=1 ORDER BY ar.ageing_pid DESC";
                                $searchParam .= 'Clinic Id = '.$form_facility.' |';
                            } else {
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "ageing_report"; // changable
                               
                                $dateRange = date('d-M-y');
                                $rcsl_requested_filter = addslashes(serialize(array("AgeingReport" => "All","noDateRange"=>"Till $dateRange"))); // changable
                                $rcsl_report_description = "Ageing report requested with all facility"; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                if($msgForReportLog){
                                    $msgForReportLog;
                                }
                                else{
                                $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                }
                                ##########Cron Request####################section only for all FACILITY#########################
                                $searchParam .= 'Clinics = All |';
                            }

                            if (!empty($_POST['form_refresh'])) {
                                $event = "Report Ageing View";
                            } elseif (!empty($_POST['form_csvexport'])) {
                                $event = "Report Ageing Export";
                            }

                            if ($msgForReportLog) {

                                $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                            } else {

                                $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                            }
                                
                            if ($msgForReportLog == "") {                               

                                $pres = sqlStatement($pquery, null, $GLOBALS['adodb']['dbreport']);
                                while ($prow = sqlFetchArray($pres)) {

                                    $due1m = $prow['due_upto1_month'];
                                    $due12m = $prow['due_for1_2months'];
                                    $due26m = $prow['due_2_6months'];
                                    $duem6m = $prow['due_more_6months'];
                                    $dueTotal = $prow['due_amount'];
                                    $treatments = $prow['treatment_Product'];

                                    if (!empty($due1m) || !empty($due12m) || !empty($due26m) || !empty($duem6m)) {

                                        $ageingDataArray[] = array(
                                            'id' => $prow['ageing_pid'],
                                            'pname' => $prow['patient_name'],
                                            'pubpid' => $prow['ageing_pubpid'],
                                            'due1m' => $due1m,
                                            'due12m' => $due12m,
                                            'due26m' => $due26m,
                                            'duem6m' => $duem6m,
                                            'treatments' => $treatments,
                                            'dueTotal' => $dueTotal
                                        );
                                    }
                                }
                                debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section

                                $prevpid = 0;
                                $srCount = 1;
                                foreach ($ageingDataArray as $row) {
                                    if ($_POST['form_csvexport']) {
                                        echo '"' . qescape($srCount) . '",';
                                        echo '"' . qescape($row['pname']) . '",';
                                        echo '"' . qescape($row['pubpid']) . '",';
                                        echo '"' . qescape($row['due1m']) . '",';
                                        echo '"' . qescape($row['due12m']) . '",';
                                        echo '"' . qescape($row['due26m']) . '",';
                                        echo '"' . qescape($row['duem6m']) . '",';
                                        echo '"' . qescape(trim(str_replace('<br>', ',', $row['treatments']), ',')) . '",';
                                        echo '"' . qescape($row['dueTotal']) . '"' . "\n";
                                    } else {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $srCount; ?>
                                            </td>
                                            <td>
                                                <?php echo htmlspecialchars($row['pname']); ?>
                                            </td>
                                            <td>
                                                <?php echo $row['pubpid']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['due1m']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['due12m']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['due26m']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['duem6m']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['treatments']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['dueTotal']; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    } // end not export
                                    $srCount++;
                                    ++$totalpts;
                                }
                            } else {
                                if ($_POST['form_csvexport']) {
                                    echo strip_tags($msgForReportLog);
                                } else {
                                    ?>
                                    <tr><td colspan="14" align='center'><?php echo $msgForReportLog; ?></td></tr>
                                    <?php
                                }
                            }
// end while
                            if (!$_POST['form_csvexport']) {
                                ?>


                            </tbody>
                        </table>
                    </div> <!-- end of results -->
        <?php
    } // end not export
} // end if refresh or export

if (!$_POST['form_refresh'] && !$_POST['form_csvexport']) {
    ?>
                <div class='text'>
                <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                </div>
                <?php
            }

            if (!$_POST['form_csvexport']) {
                ?>

            </form>
        </body>

    </html>
    <?php
} // end not export
?>
