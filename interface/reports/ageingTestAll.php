<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2080M');
$ignoreAuth=1;
 require_once("../globals.php");
 require_once("$srcdir/patient.inc");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/options.inc.php");


$sql = "truncate table ageing_reports";
sqlQuery($sql);

function duePayment($patientId,$startDate,$endDate, $facilityId) {
	$allTreatments = '';
	$allDueAmount = 0;
	if(!empty($facilityId)){
		$invQuery = "SELECT * FROM invoice WHERE inv_deletestate = 1 AND inv_pid = ".$patientId." AND inv_clinic_id IN(".$facilityId.") AND DATE(inv_created_date) BETWEEN '".$startDate."' AND '".$endDate."'";
	}else{
		$invQuery = "SELECT * FROM invoice WHERE inv_deletestate = 1 AND inv_pid = ".$patientId." AND DATE(inv_created_date) BETWEEN '".$startDate."' AND '".$endDate."'";
	}
	$invRes = sqlStatement($invQuery);
	while($invRow = sqlFetchArray($invRes)){
		$tpQuery = "SELECT invit.*, bill.code_type, bill.code, bill.code_text, bill.units, bill.fee, bill.discount, bill.discount_type, bill.unit_price AS pr_price 
			FROM invoice_items AS invit INNER JOIN billing AS bill ON bill.id = invit.invit_tp_id WHERE invit.invit_inv_id = ".$invRow['inv_id']." AND invit.invit_deleted = 0 GROUP BY invit.invit_tp_id";
		$tpRes = sqlStatement($tpQuery);
		$treatments = '';
		$invoiceCost = 0;
		$invoiceDiscount = 0;
		while ($tpRow = sqlFetchArray($tpRes)) {
			$invoiceCost += $tpRow['pr_price']*$tpRow['units'];
			if($tpRow['discount_type'] == 'Amt') {
				$invoiceDiscount += $tpRow['discount'];
			}else {
				$invoiceDiscount += round(((($tpRow['pr_price']*$tpRow['units'])/100)*$tpRow['discount']),2);
			}
			if(!empty($treatments)){
				$treatments .= $tpRow['code_text'].'<br>';
			}else {
				$treatments = $tpRow['code_text'].'<br>';
			}
		}
		$sql = "SELECT i.inv_id, i.inv_number, i.inv_deletestate, i.inv_created_date, ii.invit_deleted, i.inv_clinic_id, ps.ps_discount, ROUND(ps.ps_discount) AS rounddiscount, ps.ps_discount_type, ps.ps_fee, ROUND(ps.ps_fee) AS roundfee, ps.ps_quantity, ii.invit_deleted, ps.ps_npid, ps.ps_clinic_id, invis.invist_price, invis.invist_quantity, invim.inv_im_name, ps.ps_deletestate ";
	    $sql .= "FROM invoice AS i ";
	    $sql .= "LEFT JOIN invoice_items AS ii ON ii.invit_inv_id = i.inv_id ";
	    $sql .= "LEFT JOIN product_sales AS ps ON ii.invit_ps_id = ps.ps_id ";
	    $sql .= "LEFT JOIN inv_item_stock AS invis ON ps.ps_stockid = invis.invist_id ";
	    $sql .= "LEFT JOIN inv_item_master AS invim ON invim.inv_im_id = invis.invist_itemid ";
	    $sql .= "WHERE i.inv_id = ".$invRow['inv_id']." AND (ii.invit_tp_id = 0 OR ii.invit_tp_id IS NULL) AND ii.invit_deleted=0 ";
	    $proRes = sqlStatement($sql);
	    while ($proRow = sqlFetchArray($proRes)) {
	      	$invoiceProDiscount = 0;
			if($proRow['ps_discount_type'] == 'amt') {
	      		$invoiceProDiscount += $proRow['ps_discount'];
	      	}else {
	      		$invoiceProDiscount += round(($proRow['invist_price']*$proRow['ps_quantity'])/100)*$proRow['ps_discount'];
	      	}
	      	$invoiceDiscount += $invoiceProDiscount;
	      	$invoiceCost += ($proRow['ps_fee']+$invoiceProDiscount);
	      	if(!empty($treatments)){
	      		$treatments .= $proRow["inv_im_name"].'<br>';
	      	}else {
	      		$treatments = $proRow["inv_im_name"].'<br>';
	      	}
	    }
		$finalInvoiceAmount = round(($invoiceCost - $invoiceDiscount), 2);
		$eraQuery = "SELECT SUM(rect.rect_amount) AS earlierAmount FROM invoice_reciepts AS ir INNER JOIN reciept AS rect ON rect.rect_id = ir.invrect_rect_id WHERE rect.rect_deletestate = 1 AND rect.rect_type != 'Credit' AND ir.invrect_inv_id = ".$invRow['inv_id'];
		$eraRes = sqlStatement($eraQuery);
		$eraRow = sqlFetchArray($eraRes);
		$erAmount = 0.00;
		if(!empty($eraRow)) {
			if(!empty($eraRow['earlierAmount'])) {
				$erAmount = $eraRow['earlierAmount'];
			}
		}
		$dueAmount = ceil($finalInvoiceAmount - $erAmount);
		if(!empty($dueAmount)){
			$allTreatments .= $treatments;
		}
		$allDueAmount += $dueAmount;
	}
	return array('dueAmt' => $allDueAmount, 'treatments' => $allTreatments);
}
/* -------------------------end due ammount function----start from line: 23 ------------------*/
   $totalpts = 0;
   $ageingDataArray = array();
   $form_facility = '';	

$pquery="SELECT pd.id, CONCAT_WS(' ', pd.fname, pd.lname) AS pname, pd.pubpid, DATE(inv.inv_created_date) AS rdate FROM patient_data as pd INNER JOIN invoice as inv ON inv.inv_pid = pd.id where inv.inv_deletestate = 1  GROUP BY inv.inv_pid ORDER BY pd.id DESC";

	$pres = sqlStatement($pquery);
	while($prow = sqlFetchArray($pres)){
		$due1m =0;
		$due12m = 0;
		$due26m = 0;
		$duem6m = 0;
		$dueTotal = 0;
		$treatments = '';
		$pInquery="SELECT DATE(inv.inv_created_date) AS rdate FROM invoice as inv where inv.inv_deletestate = 1 AND inv.inv_pid =".$prow['id'];
		$pInres = sqlStatement($pInquery);
		while($pInrow = sqlFetchArray($pInres)){
		if($prow['rdate'] <= date('Y-m-d')) {
			$due1mData = duePayment($prow['id'], date('Y-m-d', strtotime('-1 month')), date('Y-m-d'), $form_facility);
		}else {
			$due1mData = array('dueAmt' => 0, 'treatments' => '');
		}
		if($prow['rdate'] <= date('Y-m-d', strtotime('-1 month'))) {
			$due12mData = duePayment($prow['id'], date('Y-m-d', strtotime('-2 month')), date('Y-m-d', strtotime('-1 month')), $form_facility);
		}else {
			$due12mData = array('dueAmt' => 0, 'treatments' => '');
		}
		if($prow['rdate'] <= date('Y-m-d', strtotime('-2 month'))) {
			$due26mData = duePayment($prow['id'], date('Y-m-d', strtotime('-6 month')), date('Y-m-d', strtotime('-2 month')), $form_facility);
		}else {
			$due26mData = array('dueAmt' => 0, 'treatments' => '');
		}
		if($prow['rdate'] <= date('Y-m-d', strtotime('-6 month'))) {
			$duem6mData = duePayment($prow['id'], $prow['rdate'], date('Y-m-d', strtotime('-6 month')), $form_facility);
		}else {
			$duem6mData = array('dueAmt' => 0, 'treatments' => '');
		}
		
		$due1m = $due1mData['dueAmt'];
		$due12m = $due12mData['dueAmt'];
		$due26m = $due26mData['dueAmt'];
		$duem6m = $duem6mData['dueAmt'];
		$dueTotal = $due1m + $due12m + $due26m + $duem6m;
		$treatments = '';
		if(!empty($due1mData['treatments'])){
			if(!empty($treatments)){
				$treatments .= $due1mData['treatments'];
			}else {
				$treatments = $due1mData['treatments'];
			}
		}
		if(!empty($due12mData['treatments'])){
			if(!empty($treatments)){
				$treatments .= $due12mData['treatments'];
			}else {
				$treatments = $due12mData['treatments'];
			}
		}
		if(!empty($due26mData['treatments'])){
			if(!empty($treatments)){
				$treatments .= $due26mData['treatments'];
			}else {
				$treatments = $due26mData['treatments'];
			}
		}
		if(!empty($duem6mData['treatments'])){
			if(!empty($treatments)){
				$treatments .= $duem6mData['treatments'];
			}else {
				$treatments = $duem6mData['treatments'];
			}
		}
		}
		if(!empty($due1m) || !empty($due12m) || !empty($due26m) || !empty($duem6m)) {
			
			$ageing_pid=$prow['id'];
			$p_name=addslashes($prow['pname']);
			$ageing_pubid=$prow['pubpid'];
			$p_next_apoint="";
			$p_u1=$due1m;
			$p_u12=$due12m;
			$p_u26=$due26m;
			$p_u6m=$duem6m;
			$p_treat=addslashes($treatments);
			$p_total=$dueTotal;
			$sql = "insert into ageing_reports values('','$p_name','$ageing_pubid','','$p_u1','$p_u12','$p_u26','$p_u6m','$p_treat','$p_total',1,'$ageing_pid')";
			sqlQuery($sql);
		}
		
	}
// end of while
?>
