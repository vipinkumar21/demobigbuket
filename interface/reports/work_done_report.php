<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report lists front office receipts for a given date range.

require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once ("$audting_webroot/auditlog.php");

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$from_date = $_POST['form_from_date']; // Filters Input
$to_date = $_POST['form_to_date'];
$facility = $_POST['form_facility'];

function bucks($amt) {
    return ($amt != 0.00) ? oeFormatMoney($amt) : '';
}

$searchParam = '';
$fileName = "work_done_report_".date("Ymd_his").".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Front Office Receipts', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />


            <script language="JavaScript">

    <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

            </script>

            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Work Done', 'e'); ?></h3>

            <div id="report_parameters_daterange">
                <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form name='theform' method='post' action='work_done_report.php' id='theform' class='emrform topnopad'>

                <div id="report_parameters" class="">
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width:100%'>

                                    <table class='text' width='100%'>
                                        <tr>
                                            <td class='label'>
                                                <?php xl('Search By', 'e'); ?>:
                                            </td>
                                            <td>
                                                <select class='emrinput' name='searchby'>
                                                    <option value="">Doctor Search By</option>
                                                    <option value="Name"<?php if ($_POST['searchby'] == 'Name') echo ' selected' ?>><?php echo htmlspecialchars(xl('Name'), ENT_NOQUOTES); ?></option>
                                                    <option value="UserName"<?php if ($_POST['searchby'] == 'UserName') echo ' selected' ?>><?php echo htmlspecialchars(xl('Login id'), ENT_NOQUOTES); ?></option>
                                                </select>
                                            </td>
                                            <td class='label'>
                                                <?php xl('For', 'e'); ?>:
                                            </td>
                                            <td>
                                                <input class='emrdate newwidth100' type='text' id='searchparm' name='searchparm' size='12' value='<?php echo htmlspecialchars($_POST['searchparm'], ENT_QUOTES); ?>'
                                                       title='<?php echo htmlspecialchars(xl('If name, any part of lastname or lastname,firstname'), ENT_QUOTES); ?>'>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class='label'>
                                                <?php xl('Search By', 'e'); ?>:
                                            </td>
                                            <td>
                                                <select class='emrinput' name='searchbyp'>
                                                    <option value="">Patient Search By</option>
                                                    <option value="pName"<?php if ($_POST['searchbyp'] == 'pName') echo ' selected' ?>><?php echo htmlspecialchars(xl('Name'), ENT_NOQUOTES); ?></option>
                                                    <option value="pUserId"<?php if ($_POST['searchbyp'] == 'pUserId') echo ' selected' ?>><?php echo htmlspecialchars(xl('Patient Id'), ENT_NOQUOTES); ?></option>
                                                </select>
                                            </td>
                                            <td class='label'>
                                                <?php xl('For', 'e'); ?>:
                                            </td>
                                            <td>
                                                <input class='emrdate newwidth100' type='text' id='searchparmp' name='searchparmp' size='12' value='<?php echo htmlspecialchars($_POST['searchparmp'], ENT_QUOTES); ?>'
                                                       title='<?php echo htmlspecialchars(xl('If name, any part of lastname or firstname'), ENT_QUOTES); ?>'>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class='label'>
                                                <?php xl('Treatment', 'e'); ?>:
                                            </td>
                                            <td colspan='3'>
                                                <input style='width:360px' class='emrdate' type='text' id='treatmentparm' name='treatmentparm' size='64' value='<?php echo htmlspecialchars($_POST['treatmentparm'], ENT_QUOTES); ?>'
                                                       title='<?php echo htmlspecialchars(xl('Treatment name'), ENT_QUOTES); ?>'>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%" class='label'><?php xl('From', 'e'); ?>:</td>
                                            <td width="40%"><input class='emrdate' type='text' name='form_from_date' id="form_from_date"
                                                                   size='10' value='<?php echo $from_date ?>'
                                                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                   align='absbottom' width='24' height='22' id='img_from_date'
                                                                   border='0' alt='[?]' style='cursor: pointer'
                                                                   title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                            <td width="10%" class='label'><?php xl('To', 'e'); ?>:</td>
                                            <td width="40%"><input class='emrdate' type='text' name='form_to_date' id="form_to_date"
                                                                   size='10' value='<?php echo $to_date ?>'
                                                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                   align='absbottom' width='24' height='22' id='img_to_date'
                                                                   border='0' alt='[?]' style='cursor: pointer'
                                                                   title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                        </tr>
                                        <tr>
                                            <td class='label'><?php xl('Facility', 'e'); ?>:</td>
                                            <td><?php dropdown_facility(strip_escape_custom($facility), 'form_facility', false, true); ?>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                </div>

                            </td>
                            <td align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "true");
                                                        $("#form_refresh").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport
            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport']) {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Date') . '",';
                    echo '"' . xl('Doctor Name') . '",';
                    echo '"' . xl('Doctor login id') . '",';
                    echo '"' . xl('Patient Name') . '",';
                    echo '"' . xl('Patient ID') . '",';
                    echo '"' . xl('Treatment Name') . '",';
                    echo '"' . xl('Treatment Cost - Full') . '",';
                    echo '"' . xl('Treatment Cost - Discounted') . '",';
                    echo '"' . xl('Tooth Number') . '",';
                    echo '"' . xl('Date of First Sitting') . '",';
                    echo '"' . xl('Date of Last Sitting') . '",';
                    echo '"' . xl('Completion Date') . '",';
                    echo '"' . xl('Number of Sittings') . '"' . "\n";
                } else {
                    ?>
                    <div id="report_results" style="overflow-x:auto;">
                        <table id="workdonereporttable" class='emrtable'>
                            <thead>
                            <th> <?php xl('S. No.', 'e'); ?> </th>
                            <th> <?php xl('Date', 'e'); ?> </th>
                            <th> <?php xl('Doctor Name', 'e'); ?> </th>
                            <th> <?php xl('Doctor login id', 'e'); ?> </th>
                            <th> <?php xl('Patient Name', 'e'); ?> </th>
                            <th> <?php xl('Patient ID', 'e'); ?> </th>
                            <th> <?php xl('Treatment Name', 'e'); ?> </th>
                            <th> <?php xl('Treatment Cost - Full', 'e'); ?> </th>
                            <th> <?php xl('Treatment Cost - Discounted', 'e'); ?> </th>
                            <th> <?php xl('Tooth Number', 'e'); ?> </th>
                            <th> <?php xl('Date of First Sitting', 'e'); ?> </th>
                            <th> <?php xl('Date of Last Sitting', 'e'); ?> </th>
                            <th> <?php xl('Completion Date', 'e'); ?> </th>
                            <th> <?php xl('Number of Sittings', 'e'); ?> </th>
                            </thead>
                            <tbody>
                                <?php
                            }
                            if (true || $_POST['form_refresh']) {
                                $total1 = 0.00;
                                $total2 = 0.00;
                                $netAmountTotal = 0.00;
                                $query = "SELECT b.id, b.code_type, b.code_text, b.toothno, b.fee, b.discount, b.discount_type, b.work_status, wl.wl_created_date, CONCAT_WS(' ', pd.fname, pd.lname) AS pname, pd.pubpid , u.username, CONCAT_WS(' ', u.fname, u.lname) AS uname, b.unit_price AS pr_price,b.clinic_id,b.units " .
                                        "FROM billing AS b " .
                                        "INNER JOIN work_log AS wl ON wl.wl_tr_id = b.id " .
                                        "INNER JOIN patient_data AS pd ON pd.id = b.pid " .
                                        "INNER JOIN users AS u ON u.id = b.provider_id ";
                                $conditions = ' b.work_status != 0 ';

                                if (isset($_POST['searchby']) && !empty($_POST['searchby']) && $_POST['searchby'] == 'Name' && isset($_POST['searchparm']) && !empty($_POST['searchparm'])) {
                                    $conditions.=" AND concat_ws(' ',u.fname,u.lname) LIKE '%" . trim($_POST['searchparm']) . "%'";
                                    $searchParam .= ' Doctor  Name = ' . $_POST['searchparm'] . ' |';
                                }
                                if (isset($_POST['searchby']) && !empty($_POST['searchby']) && $_POST['searchby'] == 'UserName' && isset($_POST['searchparm']) && !empty($_POST['searchparm']) && trim($_POST['searchparm']) != '%') {
                                    $conditions.=" AND u.username = '" . trim($_POST['searchparm']) . "'";
                                    $searchParam .= ' Doctor  User Name = ' . $_POST['searchparm'] . ' |';
                                }
                                if (isset($_POST['treatmentparm']) && !empty($_POST['treatmentparm'])) {
                                    $conditions.=" AND b.code_text LIKE '" . trim($_POST['treatmentparm']) . "%' ";
                                    
                                    $searchParam .= ' Treatment  = ' . $_POST['treatmentparm'] . ' |';
                                }
                                // for patient search
                                if (isset($_POST['searchbyp']) && !empty($_POST['searchbyp']) && $_POST['searchbyp'] == 'pName' && isset($_POST['searchparmp']) && !empty($_POST['searchparmp'])) {
                                    $conditions.=" AND pd.fname LIKE '" . trim($_POST['searchparmp']) . "%'";
                                    $searchParam .= ' Patient  Name = ' . $_POST['searchparmp'] . ' |';
                                }
                                if (isset($_POST['searchbyp']) && !empty($_POST['searchbyp']) && $_POST['searchbyp'] == 'pUserId' && isset($_POST['searchparmp']) && !empty($_POST['searchparmp']) && trim($_POST['searchparmp']) != '%') {
                                    $conditions.=" AND pd.pubpid = '" . trim($_POST['searchparmp']) . "'";
                                    $searchParam .= ' Patient  Id = ' . $_POST['searchparmp'] . ' |';
                                }
                                
                                
                                $treatmentparm = $_POST['treatmentparm'];
                                /*                                 * ********************* Date & Clinic Filter (Swati : 2014/07/07)************************************* */
                                ############################################################################################################

                                $form_facility = $facility;
                                $form_date = $from_date;
                                $form_to_date = $to_date;
                                //echo $from_date."------".$to_date;
                                if ($form_facility) {
                                    //for individual facility
                                    if ($form_date) {
                                        //if from date selected
                                        if ($form_to_date) {
                                            // if $form_date && $form_to_date
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;


                                            if ($days_between_from_to <= $form_facility_time_range) {

                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND b.clinic_id = '$form_facility'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_time_range_valid){
                                                if (!empty($treatmentparm)) {
                                                    $treatmentparm = $treatmentparm;
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "workdone_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatmentparm" => $treatmentparm, "facility"=>$form_facility))); // changable
                                                $rcsl_report_description = "Workdone report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            //only from date: no TO date
                                            $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND b.clinic_id = '$form_facility'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_time_range_valid){
                                                if (!empty($treatmentparm)) {
                                                    $treatmentparm = $treatmentparm;
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "workdone_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatmentparm" => $treatmentparm, "facility"=>$form_facility))); // changable
                                                $rcsl_report_description = "Workdone report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                                }
                                            }
                                        }
                                    } else {
                                        // if from date not selected
                                        if ($form_to_date) {
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND b.clinic_id = '$form_facility'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_time_range_valid){
                                                if (!empty($treatmentparm)) {
                                                    $treatmentparm = $treatmentparm;
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "workdone_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatmentparm" => $treatmentparm, "facility"=>$form_facility))); // changable
                                                $rcsl_report_description = "Workdone report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            if ($conditions) {
                                                $conditions .= " AND ";
                                            }
                                            $form_to_date = date("Y-m-d");
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                            $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59' AND b.clinic_id = '$form_facility'";
                                        }
                                    }
                                    
                                    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param  
                                } else {
                                    //for all facility
                                    if ($form_date) {
                                        //if from date selected
                                        if ($form_to_date) {
                                            // if $form_date && $form_to_date
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_all_time_range) {

                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                                if (!empty($treatmentparm)) {
                                                    $treatmentparm = $treatmentparm;
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "workdone_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatmentparm" => $treatmentparm))); // changable
                                                $rcsl_report_description = "Workdone report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            //only from date: no TO date
                                            $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_all_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                                if (!empty($treatmentparm)) {
                                                    $treatmentparm = $treatmentparm;
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "workdone_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatmentparm" => $treatmentparm))); // changable
                                                $rcsl_report_description = "Workdone report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                                }
                                            }
                                        }
                                    } else {
                                        // if from date not selected
                                        if ($form_to_date) {
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                            $toDate = date_create(date($form_to_date));
                                            $fromDate = date_create($form_date);
                                            $diff = date_diff($fromDate, $toDate);
                                            $days_between_from_to = $diff->days;

                                            if ($days_between_from_to <= $form_facility_all_time_range) {
                                                if ($conditions) {
                                                    $conditions .= " AND ";
                                                }
                                                $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                            } else {
                                                if($days_between_from_to<=$form_facility_all_time_range_valid){
                                                if (!empty($treatmentparm)) {
                                                    $treatmentparm = $treatmentparm;
                                                }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                // following value will be change according to report
                                                $rcsl_name_type = "workdone_report"; // changable
                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "treatmentparm" => $treatmentparm))); // changable
                                                $rcsl_report_description = "Workdone report from $form_date to $form_to_date"; // changable
                                                //allFacilityReports() defined with globals.php
                                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                    if ($msgForReportLog) {
                                                        $msgForReportLog = $msgForReportLog;
                                                    } else {
                                                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                    }
                                                ##########Cron Request####################section only for all FACILITY#########################
                                                }
                                                else{
                                                $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                                }
                                            }
                                        } else {
                                            if ($conditions) {
                                                $conditions .= " AND ";
                                            }
                                            $form_to_date = date("Y-m-d");
                                            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                            $conditions .= " wl.wl_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                        }
                                    }
                                    
                                    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param  
                                }


                                ############################################################################################################
                                if (!empty($conditions)) {
                                    $conditions = "  WHERE  " . $conditions;
                                }
                                $query .=$conditions . " GROUP BY wl.wl_tr_id ORDER BY wl.wl_created_date ASC";
                               
                                
                                if (!empty($_POST['form_refresh'])) {
                                    $event = "Report Work Done View";
                                } elseif (!empty($_POST['form_csvexport'])) {
                                    $event = "Report Work Done Export";
                                }
                                
                                if ($msgForReportLog) {

                                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                                } else {

                                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                                }
                                
                                //echo $query ;
                                if ($msgForReportLog == "") {
                                    $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);
                                    $count = 1;
                                    while ($row = sqlFetchArray($res)) {
                                        //First Start Date
                                        $tpFQuery = "SELECT wl_id, wl_created_date FROM work_log WHERE wl_tr_id = " . $row['id'] . " ORDER BY wl_created_date ASC LIMIT 0,1";
                                        $tpFRes = sqlStatement($tpFQuery, null, $GLOBALS['adodb']['dbreport']);
                                        $tpFRow = sqlFetchArray($tpFRes);
                                        //echo '<pre>';
                                        //print_r($tpFRow);
                                        
                                        // Last Entry Date
                                        $tpQuery = "SELECT wl_id, wl_created_date FROM work_log WHERE wl_tr_id = " . $row['id'] . " ORDER BY wl_created_date DESC LIMIT 0,1";
                                        $tpRes = sqlStatement($tpQuery, null, $GLOBALS['adodb']['dbreport']);
                                        $tpRow = sqlFetchArray($tpRes);
                                        //echo '<pre>';
                                        //print_r($tpRow);
                                        // Last Entry Date
                                        $tpCQuery = "SELECT COUNT(wl_id) AS numSitting FROM work_log WHERE wl_tr_id = " . $row['id'] . " ORDER BY wl_created_date DESC LIMIT 0,1";
                                        $tpCRes = sqlStatement($tpCQuery, null, $GLOBALS['adodb']['dbreport']);
                                        $tpCRow = sqlFetchArray($tpCRes);
                                         //echo '<pre>';
                                        //print_r($tpCRow);
                                        $disAmount = 0;
                                        if ($row['discount_type'] == 'Amt') {
                                            $disAmount = $row['discount'];
                                        } else {
                                            $disAmount = ($row['pr_price'] * $row['units']) * ($row['discount'] / 100);
                                        }
                                        // Make the timestamp URL-friendly.
                                        $timestamp = preg_replace('/[^0-9]/', '', $row['rect_created_date']);
                                        if ($_POST['form_csvexport']) {
                                            echo '"' . qescape($count) . '",';
                                            echo '"' . qescape($row['wl_created_date']) . '",';
                                            echo '"' . qescape($row['uname']) . '",';
                                            echo '"' . qescape($row['username']) . '",';
                                            echo '"' . qescape($row['pname']) . '",';
                                            echo '"' . qescape($row['pubpid']) . '",';
                                            echo '"' . qescape($row['code_text']) . '",';
                                            echo '"' . qescape(number_format($row['pr_price'] * $row['units'], 2)) . '",';
                                            echo '"' . qescape(number_format($disAmount, 2)) . '",';
                                            echo '"' . qescape($row['toothno']) . '",';
                                            echo '"' . qescape(oeFormatShortDate(substr($tpFRow['wl_created_date'], 0, 10))) . '",';
                                            echo '"' . qescape(oeFormatShortDate(substr($tpRow['wl_created_date'], 0, 10))) . '",';
                                            if ($row['work_status'] == 2) {
                                                echo '"' . qescape(oeFormatShortDate(substr($tpRow['wl_created_date'], 0, 10))) . '",';
                                            } else {
                                                echo '"' . qescape('') . '",';
                                            }
                                            echo '"' . qescape($tpCRow['numSitting']) . '"' . "\n";
                                        } else {
                                            ?>
                                            <tr>
                                                <td nowrap>
                                                    <?php echo $count; ?>
                                                </td>
                                                <td nowrap>
                                                    <?php echo $row['wl_created_date']; ?>
                                                </td>
                                                <td nowrap>
                                                    <?php echo $row['uname']; ?>
                                                </td>
                                                <td nowrap>
                                                    <?php echo $row['username']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['pname']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['pubpid']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['code_text']; ?>
                                                </td>
                                                <td>
                                                    <?php echo number_format($row['pr_price'] * $row['units'], 2); ?>
                                                </td>
                                                <td>
                                                    <?php echo number_format($disAmount, 2); ?>
                                                </td>
                                                <td class="wbreak wordbreak" style="padding:4px;min-width:70px;">
                                                    <?php echo text($row['toothno']); ?>
                                                </td>

                                                <td>
                                                    <?php echo text(oeFormatShortDate(substr($tpFRow['wl_created_date'], 0, 10))); ?>
                                                </td>  
                                                <td>
                                                    <?php
                                                    echo text(oeFormatShortDate(substr($tpRow['wl_created_date'], 0, 10)));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($row['work_status'] == 2) {
                                                        echo text(oeFormatShortDate(substr($tpRow['wl_created_date'], 0, 10)));
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo text($tpCRow['numSitting']);
                                                    ?>
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                        $count++;
                                    }
                                     debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                                } else {
                                    if ($_POST['form_csvexport']) {
                                        echo strip_tags($msgForReportLog);
                                    } else {
                                        ?>
                                        <tr><td colspan="14" align='center'><?php echo $msgForReportLog; ?></td></tr>
                                        <?php
                                    }
                                }
                            }
                            if (!$_POST['form_csvexport']) {
                                ?>
                            </tbody>
                        </table>
                    </div> <!-- end of results -->
                    <?php
                }
            } else {
                ?>
                <div class='text'>
                    <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                </div>
                <?php
            }
            if (!$_POST['form_csvexport']) {
                ?>

            </form>
            <!-- stuff for the popup calendar -->
            <style type="text/css">
                @import url(../../library/dynarch_calendar.css);
            </style>
            <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
            <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
            <script type="text/javascript"
            src="../../library/dynarch_calendar_setup.js"></script>
            <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField: "form_from_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_from_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (toDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_from_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                                                    Calendar.setup({
                                                        inputField: "form_to_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_to_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (fromDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_to_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
            </script>
        </body>
    </html>
    <?php
}
?>