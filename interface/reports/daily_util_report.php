<?php
// Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report lists patients that were seen within a given date
// range, or all patients if no date range is entered.

require_once("../globals.php");
require_once($GLOBALS['fileroot']."/library/acl.inc");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

// $from_date = fixDate($_POST['form_from_date'], date('Y-01-01'));
// $to_date   = fixDate($_POST['form_to_date'], date('Y-12-31'));
$from_date = fixDate($_POST['form_from_date'], '');
if (empty($from_date))
    $from_date = date('Y-m-d');

$form_facility = empty($_POST['form_facility']) ? 0 : intval($_POST['form_facility']);
$form_region = empty($_POST['form_region']) ? 0 : intval($_POST['form_region']);
$form_zone = empty($_POST['form_zone']) ? 0 : intval($_POST['form_zone']);
$searchParam = '';
$fileName = "CapacityUtilizationDailyReport_" . date("Ymd_his") . ".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport']) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Patient List', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>

            <script language="JavaScript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
            </script>
            <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
            <style type="text/css">

                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                        margin-bottom: 10px;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                    #report_results {
                        width: 100%;
                    }
                }

            </style>
            <script type="text/javascript"> 
            $(document).ready(function(){
                    $("#form_region").change(function(){
                        changeRegion();
                    });
                    $("#form_zone").change(function(){
                        changeZone();
                    });
                    var selectedRegionAjax='<?php echo $form_region; ?>';
                    var selectedZoneAjax='<?php echo $form_zone; ?>';
                    var selectedFacilityAjax='<?php echo $form_facility; ?>';
                    if(selectedRegionAjax=='' && selectedZoneAjax!=''){
                        changeZone();
                    }else if(selectedRegionAjax!='' && selectedZoneAjax=='')
                    {
                        changeRegion();
                    }else if(selectedRegionAjax!='' && selectedZoneAjax!=''){
                        changeZone();
                    }
                    function changeRegion()
                    {
                        var regionId = $("#form_region").val();
                        $.ajax({
                                url:"get_facility_zones.php?regionId="+regionId+"&facilityId="+selectedFacilityAjax,
                                success:function(result){
                                        var result =  $.parseJSON(result);
                                        $("#form_zone").html(result[0]);
                                        $("#form_facility").html(result[1]);
                                }
                        });
                    }
                    function changeZone()
                    {
                        var zoneId = $("#form_zone").val();
                        var regionId = $("#form_region").val();
                        $.ajax({
                                url:"get_facility_by_zones.php?zoneId="+zoneId+"&facilityId="+selectedFacilityAjax+"&regionId="+regionId,
                                success:function(result){
                                        var result =  $.parseJSON(result);
                                        $("#form_zone").html(result[0]);
                                        $("#form_facility").html(result[1]);
                                }
                        });
                    }
            });
                
                </script>
        </head>

        <body class="body_top dailyUtilReport">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

            <h3 class='emrh3'><?php xl('Capacity Utilization - Daily Report', 'e'); ?></h3>

            <div id="report_parameters_daterange">
                <?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form name='theform' id='theform' method='post' action='daily_util_report.php' class="emrform topnopad">

                <div id="report_parameters">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width:100%'>

                                    <table class='text'>
                                        <tr>
                                            <td width="5%" class='label'>
                                                <?php xl('Region', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdownRegions(strip_escape_custom($form_region), 'form_region'); ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('Zone', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                    <?php
                                                         dropdownZones(strip_escape_custom($form_zone), 'form_zone'); 
                                                    ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>
                                            <td width="5%" class='label'>
                                                <?php xl('Date', 'e'); ?>:
                                            </td>
                                            <td width="30%">
                                                <input class='emrdate' type='text' name='form_from_date' id="form_from_date" size='10' value='<?php echo $form_from_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)' title='yyyy-mm-dd'>
                                                <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'
                                                     id='img_from_date' border='0' alt='[?]' style='cursor:pointer'
                                                     title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                            </td>
                                            <td width="45%"></td>			
                                        </tr>
                                    </table>

                                </div>

                            </td>
                            <td width="20%" align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                            $("#form_csvexport").attr("value", "");
                                                            $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>  
                                                <?php if (acl_check('admin', 'dailyutilization_export_report'    )){ ?>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "true");
                                                            $("#form_refresh").attr("value", "");
                                                            $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php } ?>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- end of parameters -->

                <?php
            } // end not form_csvexport

            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                $totalpts = 0;
                if (!empty($_POST['form_refresh'])) {
                    $event = "Report Daily Util View";
                } elseif (!empty($_POST['form_csvexport'])) {
                    $event = "Report Daily Util Export";
                }
                if ($form_facility) {
                    $query = "SELECT f.id, f.name, f.operatory, f.start_hour, f.end_hour " .
                            "FROM facility AS f ";
                    if ($form_facility) {
                        $query .= " WHERE f.id = '" . $form_facility . "' ";
                        $searchParam .= ' Clinic Id = ' . $form_facility . ' |';
                    }
                    if ($form_region) {
                        $query .= " AND f.region_id = '" . $form_region . "' ";
                        $searchParam .= ' Region Id = ' . $form_region . ' |';
                    }
                    if ($form_zone) {
                        $query .= " AND f.zone_id = '" . $form_zone . "' ";
                        $searchParam .= ' Zone Id = ' . $form_zone . ' |';
                    }
                    $query .= "  ORDER BY f.id DESC";
                    $searchParam .= ' Date = ' . $from_date . ' |';
                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                    $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);
                    $prevpid = 0;
                    $srCount = 1;
                    $holidayData = array();
                    while ($row = sqlFetchArray($res)) {
                        $aquery = "SELECT SUM(pc_duration) AS numApp FROM openemr_postcalendar_events WHERE pc_delete = 0 AND  pc_facility = '" . $row['id'] . "' ";
                        $eventQuery = "SELECT COUNT(pc_eid) AS FullDayEvent FROM openemr_postcalendar_events WHERE pc_delete = 0 AND  pc_facility = '" . $row['id'] . "' ";
                        $eventQuery.="AND pc_alldayevent ='1'" . " AND pc_aid='-1'";
                        $form_year = date('Y', strtotime($from_date));
                        $eventDuration = "SELECT SUM(pc_duration) AS duration FROM openemr_postcalendar_events WHERE  pc_aid='-1' AND pc_facility='" . $form_facility . "' AND pc_delete='0'";
                        if (!empty($from_date)) {

                            $dayNum = date("w", strtotime($from_date));
                            switch ($dayNum) {
                                case 0:
                                    $dayNum = 6;
                                    break;
                                case 1:
                                    $dayNum = 0;
                                    break;
                                case 2:
                                    $dayNum = 1;
                                    break;
                                case 3:
                                    $dayNum = 2;
                                    break;
                                case 4:
                                    $dayNum = 3;
                                    break;
                                case 5:
                                    $dayNum = 4;
                                    break;
                                case 6:
                                    $dayNum = 5;
                                    break;
                                default:
                                    break;
                            }
                            // echo $dayNum;
                            $weekoffQuery = "SELECT id FROM facility WHERE weekly_off_enabled='1' AND id='" . $form_facility . "' AND work_off_day='" . $dayNum . "'";
                            $wResult = sqlStatement($weekoffQuery);
                            if (sqlNumRows($wResult)) {
                                $msgForReportAlert = "This is a week off Day";
                                echo "<tr><td colspan=\"7\" align=\"center\"> $msgForReportAlert</td> </tr>";
                            } else {
                                $aquery .= " AND pc_eventDate = '" . $from_date . "'";
                                $eventQuery .= " AND pc_eventDate = '" . $from_date . "'";
                                $eventDuration.=" AND pc_alldayevent ='0' AND pc_eventDate = '" . $from_date . "'";
                                $queryHolidayCount = "SELECT `fhol_date` FROM facility_holidays WHERE `fhol_year` = '" . $form_year . "' AND `fhol_facility_id` = '" . $row['id'] . "' AND `fhol_is_deleted` = '0' ORDER BY fhol_date ASC";
                                $eres = sqlStatement($queryHolidayCount);
                                if (sqlNumRows($eres)) {
                                    while ($erow = sqlFetchArray($eres)) {
                                        $holidayData[] = $erow['fhol_date'];
                                    }
                                }
                                $aRow = sqlFetchArray(sqlStatement($aquery, null, $GLOBALS['adodb']['dbreport']));
                                $full_res = sqlFetchArray(sqlStatement($eventQuery, null, $GLOBALS['adodb']['dbreport']));
                                if ($full_res['FullDayEvent']) {
                                    $msgForReportAlert = "This day is booked for Full Day Event";
                                    echo "<tr><td colspan=\"7\" align=\"center\"> $msgForReportAlert</td> </tr>";
                                } else if (in_array($from_date, $holidayData)) {
                                    $msgForReportAlert = "This day is holiday";
                                    echo "<tr><td colspan=\"7\" align=\"center\"> $msgForReportAlert</td> </tr>";
                                } else {
                                   
                                    $appointments = 0;
                                    if (!empty($aRow['numApp'])) {
                                        $appointments = round($aRow['numApp']/(60 * $GLOBALS['calendar_interval']));
                                        
                                    }
                                    $day = date('l');
                                    if (!empty($from_date)) {
                                        $day = date('l', strtotime($from_date));
                                    }
                                   // echo $eventDuration;exit();
                                    $edRow = sqlFetchArray(sqlStatement($eventDuration, null, $GLOBALS['adodb']['dbreport']));
                                    if ($edRow['duration'] != '') {
                                        $duration = $edRow['duration'] / (60 * $GLOBALS['calendar_interval']);
                                        $totalCapacity = ceil($row['operatory'] * (($row['end_hour'] - $row['start_hour']) ) * (60 / $GLOBALS['calendar_interval']));
                                        $totalCapacity=$totalCapacity-$duration;
                                    } else {
                                        $totalCapacity = ceil(($row['operatory'] * ($row['end_hour'] - $row['start_hour']) * (60 / $GLOBALS['calendar_interval'])));
                                    }
                                    $capacityUtill = 0;
                                    if ($appointments) {
                                        $capacityUtill = ceil(($appointments / $totalCapacity) * 100);
                                    }
                                    if ($_POST['form_csvexport']) {
                                        echo '"' . xl('S. No.') . '",';
                                        echo '"' . xl('Clinic Name') . '",';
                                        echo '"' . xl('Date') . '",';
                                        echo '"' . xl('Day') . '",';
                                        echo '"' . xl('Appointments') . '",';
                                        echo '"' . xl('Total Capacity') . '",';
                                        echo '"' . xl('Capacity Utilization') . '"' . "\n";
                                        echo '"' . qescape($srCount) . '",';
                                        echo '"' . qescape($row['name']) . '",';
                                        echo '"' . qescape(oeFormatShortDate($from_date)) . '",';
                                        echo '"' . qescape($day) . '",';
                                        echo '"' . qescape($appointments) . '",';
                                        echo '"' . qescape($totalCapacity) . '",';
                                        echo '"' . qescape($capacityUtill . "%") . '"' . "\n";
                                    } else {
                                        ?>
                                        <div id="report_results">
                                            <table class='emrtable'>
                                                <thead>
                                                <th> <?php xl('S. No.', 'e'); ?> </th>
                                                <th> <?php xl('Clinic Name', 'e'); ?> </th>
                                                <th> <?php xl('Date', 'e'); ?> </th>
                                                <th> <?php xl('Day', 'e'); ?> </th>
                                                <th> <?php xl('Appointments', 'e'); ?> </th>
                                                <th> <?php xl('Total Capacity', 'e'); ?> </th>
                                                <th> <?php xl('Capacity Utilization', 'e'); ?> </th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td >
                            <?php echo $srCount; ?>
                                                        </td>
                                                        <td>
                            <?php echo htmlspecialchars($row['name']); ?>
                                                        </td>
                                                        <td>
                            <?php echo $from_date ?>
                                                        </td>
                                                        <td>
                            <?php echo $day ?>
                                                        </td>
                                                        <td>
                            <?php echo $appointments ?>
                                                        </td>
                                                        <td>
                            <?php echo $totalCapacity; ?>
                                                        </td>
                                                        <td>
                            <?php echo $capacityUtill . "%"; ?>
                                                        </td>
                                                    </tr>
                            <?php
                        } // end not export
                        $srCount++;
                        ++$totalpts;
                    }
                }
            }
        } // end while 
        debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
    } else {

        ##########Cron Request####################section only for all FACILITY#########################
        // following value will be change according to report
        $rcsl_name_type = "capacity_utilization_daily_report"; // changable
        $dateRange = date('d-M-y', strtotime($from_date));
        $rcsl_requested_filter = addslashes(serialize(array("dailydate" => $from_date, "noDateRange" => "For $dateRange","facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
        $rcsl_report_description = "Capacity Utilization - Daily report for $from_date"; // changable
        //allFacilityReports() defined with globals.php
        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
        if ($msgForReportLog) {
            $msgForReportLog;
        } else {
            $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
        }
        ##########Cron Request####################section only for all FACILITY#########################
        echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
        $searchParam .= ' Clinic =  All | Date = ' . $from_date . ' |';

        debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
    }


    if (!$_POST['form_csvexport']) {
        ?>
                            </tbody>
                        </table>
                    </div> <!-- end of results -->
        <?php
    } // end not export
} // end if refresh or export

if (!$_POST['form_refresh'] && !$_POST['form_csvexport']) {
    ?>
                <div class='text'>
                <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                </div>
                    <?php
                }

                if (!$_POST['form_csvexport']) {
                    ?>

            </form>
        </body>

        <!-- stuff for the popup calendar -->
        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
        <script language="Javascript">                                                     Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
        </script>
    </html>
    <?php
} // end not export
?>
