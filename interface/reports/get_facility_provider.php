<?php
 $fake_register_globals=false;
 $sanitize_all_escapes=true;

 require_once("../globals.php");
 require_once("$srcdir/patient.inc");
 require_once("$srcdir/forms.inc");
 require_once("$srcdir/calendar.inc");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/encounter_events.inc.php");
 require_once("$srcdir/acl.inc");
 require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");
require_once ($GLOBALS['srcdir'] . "/maviq_phone_api.php");
require_once($GLOBALS['srcdir'] . "/event_notification.php");
// Get the providers list.
$facilList = $_REQUEST['facilityId'];
if(empty($facilList)){
	$facilList = 0;
}
if ($GLOBALS['restrict_user_facility']) {
	/*
	$ures = sqlStatement("SELECT id, username, fname, lname FROM users as us, users_facility as uf WHERE " .
	"authorized != 0 AND active = 1 AND id != ".$_SESSION['authUserID']." AND ((us.facility_id = $facilList) OR (uf.facility_id = $facilList AND uf.table_id = us.id)) GROUP BY us.id ORDER BY lname, fname");
	*/
	$sql = "SELECT u.id, CONCAT_WS(' ', u.lname, u.fname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	//$sql .= " WHERE ";
	if (isset($facilList) && $facilList !== ''){
            if($facilList != 0)
		$sql .= " WHERE uf.facility_id = $facilList ";
	}else {
		$fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = ".$userid." AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
		$fRes = sqlStatement($fsql);
		$facilityIds = '';
		while ($fRow = sqlFetchArray($fRes)) {
			if(!empty($facilityIds)) {
				$facilityIds .= ', '.$fRow['facility_id'];
			}else {
				$facilityIds = $fRow['facility_id'];
			}
		}
		$sql .= " WHERE uf.facility_id IN (".$facilityIds.") ";
	}
	$sql .= "AND gag.id IN (12, 13, 18) ";
	$sql .= " GROUP BY u.id DESC ORDER BY u.lname";
	
	//echo $sql;
	
	$ures = sqlStatement($sql);
	
}else {
	/*
	$ures = sqlStatement("SELECT id, username, fname, lname, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users WHERE " .
	"authorized != 0 AND active = 1 AND id != ".$_SESSION['authUserID']." AND facility_id = $facilList ORDER BY lname, fname");
	*/
	$sql = "SELECT u.id, CONCAT_WS(' ', u.lname, u.fname) as completename "; 
	$sql .= "FROM users AS u  ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	$sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";	
}
$returnData = ''; 
// default to the currently logged-in user
$defaultProvider = $_SESSION['authUserID'];
 if($facilList != 0){
    $returnData.= "<option value='' selected='selected'>Select Provider</option>";
 }else{
    $returnData.= "<option value='' selected='selected'>--All--</option>"; 
 }
while ($urow = sqlFetchArray($ures)) {
	$returnData.= "<option value='" . attr($urow['id']) . "'";
 	//if ($urow['id'] == $defaultProvider) $returnData.= " selected";
 	$returnData.= ">" . text($urow['completename']);
 	//$returnData.= ">" . text($urow['fname']);
 	//if ($urow['lname']) $returnData.= " " . text($urow['lname']);
 	$returnData.= "</option>";
 }
 //$returnData.= "</select>";
echo $returnData;
?>