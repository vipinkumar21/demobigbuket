<?php
/**
 *
 * Appointment Slots Report: (Swati Jain)
 *
 * */
require_once("../globals.php");
require_once($GLOBALS['fileroot']."/library/acl.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once ("$audting_webroot/auditlog.php");

$form_facility = '';
$searchParam = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    if (!empty($_POST['form_refresh'])) {
        $event = "Report Appointment Slots View";
    } elseif (!empty($_POST['form_csvexport'])) {
        $event = "Report Appointment Slots Export";
    }

    $sql = " SELECT pc_eid,pc_eventDate,pc_startTime,pc_endTime,pc_facility,pc_delete,pc_aid,pc_pid
			 FROM openemr_postcalendar_events
			 WHERE pc_operatory != 0 ";
    // facility filters
    if (isset($_POST['form_facility'])) {

        $cur_server_time = date("G:i:s a"); // if 'all-clinics' selected,it must be in between 9PM to 9 AM(server time)'
        if (($_POST['form_facility'] == -1) && (($cur_server_time > '09:00:00') || ($cur_server_time > '21:00:00'))) {
            $msg = 'All clinics data will be visible between 9 PM and 9 AM only.';
            echo "<html>\n<body>\n<script language='JavaScript'>\n";
            if ($msg)
                echo " alert('" . addslashes($msg) . "');\n";
            echo " window.close();\n";
            echo "window.location.href = 'appt_slots_report.php'";
            echo "</script>\n</body>\n</html>\n";
            exit();
        } else {
            $form_facility = $_POST['form_facility'];             
        }

        $sql .= ' AND pc_facility =' . $form_facility;
        
       
    }

    $year = date('Y'); // month filter,year is for current year
    if (isset($_POST['month'])) {
        $sql .= " AND (MONTH(pc_eventDate) = '" . $_POST['month'] . "' AND YEAR(pc_eventDate) = '" . $year . "')";
         $searchParam .=  ' From Month = ' . $_POST['month'] . ' | Year = ' . $year .' |';
    }

    $sql .= 'ORDER BY pc_facility ,pc_eventDate,pc_startTime';

    if ($form_facility) {

        $searchParam .= ' Clinic Id = '.$form_facility.' | '; /// Auditing Section Param 
        
        $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
         
        $res = sqlStatement($sql, null, $GLOBALS['adodb']['dbreport']);

        // time/slots/appt/Show calculations
        $slots_data = array();
        $facility_arr = array();
        $nine_slot = 0;
        $eleven_slot = 0;
        $one_slot = 0;
        $three_slot = 0;
        $five_slot = 0;
        $seven_slot = 0;

        while ($fac_row = sqlFetchArray($res)) {
            $facility_arr[$fac_row['pc_facility']][$fac_row['pc_eventDate']][] = array("pc_eid" => $fac_row['pc_eid']
                , "pc_eventDate" => $fac_row['pc_eventDate']
                , "pc_startTime" => $fac_row['pc_startTime']
                , "pc_delete" => $fac_row['pc_delete']
                , "pc_pid" => $fac_row['pc_pid']
            );
        }
        
        debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section

        // pc_delete : 0-appt ; 1-delete/cancel; 2-No Show
        foreach ($facility_arr as $k => $v) {
            foreach ($v as $k1 => $v1) {
                $t9 = $d9 = $total9 = $show9 = $ns9 = $t11 = $d11 = $total11 = $show11 = $ns11 = 0;
                $t1 = $d1 = $total1 = $show1 = $ns1 = $t3 = $d3 = $total3 = $show3 = $ns3 = 0;
                $t5 = $d5 = $total5 = $show5 = $ns5 = $t7 = $d7 = $total7 = $show7 = $ns7 = 0;
                // slots wise appt/Show calculation
                for ($i = 0; $i < count($v1); $i++) {
                    if ($v1[$i]['pc_startTime'] >= '09:00:00' && $v1[$i]['pc_startTime'] < '11:00:00') {
                        $t9++;
                        if ($v1[$i]['pc_delete'] == 1) {
                            $d9++;
                        } elseif ($v1[$i]['pc_delete'] == 0 || $v1[$i]['pc_delete'] == 3) {
                            //$flag_shows = getWorkLogCheck($v1[$i]['pc_pid'],$v1[$i]['pc_eventDate'],$v1[$i]['pc_eid']); //For No-Show display 
                            if ($v1[$i]['pc_delete'] == 3) {
                                $ns9++;
                            }
                        }
                    } elseif ($v1[$i]['pc_startTime'] >= '11:00:00' && $v1[$i]['pc_startTime'] < '13:00:00') {
                        $t11++;
                        if ($v1[$i]['pc_delete'] == 1) {
                            $d11++;
                        } elseif ($v1[$i]['pc_delete'] == 0 || $v1[$i]['pc_delete'] == 3) {
                            if ($v1[$i]['pc_delete'] == 3) {
                                $ns11++;
                            }
                        }
                    } elseif ($v1[$i]['pc_startTime'] >= '13:00:00' && $v1[$i]['pc_startTime'] < '15:00:00') {
                        $t1++;
                        if ($v1[$i]['pc_delete'] == 1) {
                            $d1++;
                        } elseif ($v1[$i]['pc_delete'] == 0 || $v1[$i]['pc_delete'] == 3) {
                            if ($v1[$i]['pc_delete'] == 3) {
                                $ns1++;
                            }
                        }
                    } elseif ($v1[$i]['pc_startTime'] >= '15:00:00' && $v1[$i]['pc_startTime'] < '17:00:00') {
                        $t3++;
                        if ($v1[$i]['pc_delete'] == 1) {
                            $d3++;
                        } elseif ($v1[$i]['pc_delete'] == 0 || $v1[$i]['pc_delete'] == 3) {
                            if ($v1[$i]['pc_delete'] == 3) {
                                $ns3++;
                            }
                        }
                    } elseif ($v1[$i]['pc_startTime'] >= '17:00:00' && $v1[$i]['pc_startTime'] < '19:00:00') {
                        $t5++;
                        if ($v1[$i]['pc_delete'] == 1) {
                            $d5++;
                        } elseif ($v1[$i]['pc_delete'] == 0 || $v1[$i]['pc_delete'] == 3) {
                            if ($v1[$i]['pc_delete'] == 3) {
                                $ns5++;
                            }
                        }
                    } elseif ($v1[$i]['pc_startTime'] >= '19:00:00' && $v1[$i]['pc_startTime'] < '21:00:00') {
                        $t7++;
                        if ($v1[$i]['pc_delete'] == 1) {
                            $d7++;
                        } elseif ($v1[$i]['pc_delete'] == 0 || $v1[$i]['pc_delete'] == 3) {
                            if ($v1[$i]['pc_delete'] == 3) {
                                $ns7++;
                            }
                        }
                    }
                }
                // Total : total appt - deletion case 
                // Show : Total - No Show
                $total9 = $t9 - $d9;
                $show9 = $total9 - $ns9;

                $total11 = $t11 - $d11;
                $show11 = $total11 - $ns11;

                $total1 = $t1 - $d1;
                $show1 = $total1 - $ns1;

                $total3 = $t3 - $d3;
                $show3 = $total3 - $ns3;

                $total5 = $t5 - $d5;
                $show5 = $total5 - $ns5;

                $total7 = $t7 - $d7;
                $show7 = $total7 - $ns7;
                // Main array display
                $main_arr[get_facilty_name($k)][] = array("Date" => $k1
                    , "9-11" => "$t9 | $ns9"
                    , "11-1" => "$t11 | $ns11"
                    , "1-3" => "$t1 | $ns1"
                    , "3-5" => "$t3 | $ns3"
                    , "5-7" => "$t5 | $ns5"
                    , "7-9" => "$t7 | $ns7"
                );
            }
        }
    }
}//  end if "post check"		

function getWorkLogCheck($pcpid, $evt_date, $pceid) {

    $strQuery = "SELECT ope.pc_eid,ope.pc_pid,ope.pc_eventDate,ope.pc_startTime,ope.pc_endTime,bl.id,bl.date,wl.wl_created_date,wl_is_modified,ope.pc_delete
                                 FROM openemr_postcalendar_events AS ope 
                                 INNER JOIN billing AS bl ON bl.pid = ope.pc_pid 
                                 INNER JOIN work_log AS wl ON wl.wl_tr_id = bl.id
                                 WHERE wl.wl_is_deleted =0 AND (ope.pc_delete =0 OR ope.pc_delete =2) AND (bl.work_status = 1 OR bl.work_status = 2 OR (bl.work_status = 0 AND bl.isworkdelete=1))
                                 AND ope.pc_pid = $pcpid AND ope.pc_eventDate = '$evt_date'"
            . " AND (DATE(wl.wl_created_date) = '$evt_date') "
            . "GROUP BY wl_tr_id";  //using LIKE in date bcoz it consist of H:i:s also
    $result = sqlStatement($strQuery, null, $GLOBALS['adodb']['dbreport']);
    if (sqlNumRows($result) > 0) {
        while ($row = sqlFetchArray($result)) {
            return "Show";  // if workdone exists in work_log table(Means patients comes at time of appt)            
        }
    } else {
        return "NoShow";
    }
}

// fetching clinic names
function get_facilty_name($k) {
    $query = "SELECT name from facility WHERE id=" . $k;
    $res = sqlStatement($query);
    $urow = sqlFetchArray($res);
    return $urow['name'];
}

function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$bgcolor = "#aaaaaa";
$fileName = "appt_slots_report_".date("Ymd_his").".csv";
if ((isset($_POST['form_csvexport'])) && ($_POST['form_csvexport'] == 'csvSlots')) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>
        <head>
            <?php if (function_exists('html_header_show')) html_header_show(); ?>
            <link rel=stylesheet href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('Appointment Slots Report', 'e') ?></title>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
            <style type="text/css">

                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results {
                        margin-top: 30px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }

            </style>
            <script>
                $(document).ready(function () {
                    $('#overlay').remove();
                });
            </script>
        </head>

        <body class="body_top">
            <div id='overlay' style="position: absolute; width: 100%; height: 100%; background: #000; top: 0; left: 0; opacity: .5">
                <img alt="" src="../../images/ajax-loader.gif" style="left: 50%; margin-left: -50px; position: absolute; top: 25%;">
            </div>
            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Appointment Slots', 'e'); ?></h3>

            <form method='post' action='appt_slots_report.php' enctype='multipart/form-data' id='theform' class='emrform topnopad'>
                <div id="report_parameters">

                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='80%'>
                                <div style='float:left; width: 100%'>
                                    <table>
                                        <tr>
                                            <td width="5%" class='label'>
                                                <?php xl('Facility', 'e'); ?>:
                                            </td>
                                            <td width="20%">
                                                <?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                            </td>						
                                            <td width="5%" class='label'>
                                                <?php xl('Date', 'e'); ?>:
                                            </td>
                                            <td width="25%">
                                                <select class='emrinput' id="month" class="text" name="month">
                                                    <option value="1" <?php
                                                    if ($_POST['month'] == '1') {
                                                        echo 'selected';
                                                    }
                                                    ?>>January</option>
                                                    <option value="2" <?php
                                                    if ($_POST['month'] == '2') {
                                                        echo 'selected';
                                                    }
                                                    ?>>February</option>
                                                    <option value="3" <?php
                                                    if ($_POST['month'] == '3') {
                                                        echo 'selected';
                                                    }
                                                    ?>>March</option>
                                                    <option value="4" <?php
                                                    if ($_POST['month'] == '4') {
                                                        echo 'selected';
                                                    }
                                                    ?>>April</option>
                                                    <option value="5" <?php
                                                    if ($_POST['month'] == '5') {
                                                        echo 'selected';
                                                    }
                                                    ?>>May</option>
                                                    <option value="6" <?php
                                                    if ($_POST['month'] == '6') {
                                                        echo 'selected';
                                                    }
                                                    ?>>June</option>
                                                    <option value="7" <?php
                                                    if ($_POST['month'] == '7') {
                                                        echo 'selected';
                                                    }
                                                    ?>>July</option>
                                                    <option value="8" <?php
                                                    if ($_POST['month'] == '8') {
                                                        echo 'selected';
                                                    }
                                                    ?>>August</option>
                                                    <option value="9" <?php
                                                    if ($_POST['month'] == '9') {
                                                        echo 'selected';
                                                    }
                                                    ?>>September</option>
                                                    <option value="10" <?php
                                                    if ($_POST['month'] == '10') {
                                                        echo 'selected';
                                                    }
                                                    ?>>October</option>
                                                    <option value="11" <?php
                                                    if ($_POST['month'] == '11') {
                                                        echo 'selected';
                                                    }
                                                    ?>>November</option>
                                                    <option value="12" <?php
                                                    if ($_POST['month'] == '12') {
                                                        echo 'selected';
                                                    }
                                                    ?>>December</option>
                                                </select>
                                            </td>
                                            <td width="45%"></td>
                                        </tr>

                                    </table>
                                </div>	
                            </td>
                            <td align='left' valign='middle' height="100%">
                                <table style='border-left:1px solid; width:100%; height:100%' >
                                    <tr>
                                        <td>
                                            <div style='margin-left:15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                                                        $("#form_csvexport").attr("value", "");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Submit', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php if (acl_check('admin', 'appointmentslot_export_report'    )){ ?>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvSlots");
                                                        $("#theform").submit();'>
                                                    <span>
                                                        <?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="text">* Please select a filter to see the results.</div>
            <?php } // end of csv section ?>
            
            <?php
            if (isset($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvSlots') {

                echo xl('Clinic Name') . ',';
                echo '"' . xl('Date') . '",';
                echo '"' . xl('9am-11am') . '",';
                echo '"' . xl('11am-1pm') . '",';
                echo '"' . xl('1pm-3pm') . '",';
                echo '"' . xl('3pm-5pm') . '",';
                echo '"' . xl('5pm-7pm') . '",';
                echo '"' . xl('7pm-9pm') . '",' . "\n";

                if (count($main_arr) > 0) {

                    foreach ($main_arr AS $k => $v) {
                        echo '"' . qescape($k) . '",';
                        echo '"",';
                        echo '"",';
                        echo '"",';
                        echo '"",';
                        echo '"",';
                        echo '"",';
                        echo '"",' . "\n";
                        foreach ($v AS $k1 => $v1) {
                            echo '"",';
                            echo '"' . qescape($v1['Date']) . '",';
                            echo '"' . qescape($v1['9-11']) . '",';
                            echo '"' . qescape($v1['11-1']) . '",';
                            echo '"' . qescape($v1['1-3']) . '",';
                            echo '"' . qescape($v1['3-5']) . '",';
                            echo '"' . qescape($v1['5-7']) . '",';
                            echo '"' . qescape($v1['7-9']) . '",' . "\n";
                        }
                    }
                } else {
                    echo '"No Results Found",' . "\n";
                }
            } else {
                ?>

                <div id="report_results">
                    <?php if (isset($_POST['form_refresh'])) { ?>
                        <table class='emrtable'>
                            <?php if (count($main_arr) > 0) { ?>
                                <thead>

                                <th>&nbsp;<?php xl('Clinic name', 'e') ?></th>
                                <th>&nbsp;<?php xl('Date', 'e') ?></th>
                                <th>&nbsp;<?php xl('9am-11am', 'e') ?></th> 
                                <th>&nbsp;<?php xl('11am-1pm', 'e') ?></th>
                                <th>&nbsp;<?php xl('1pm-3pm', 'e') ?></th>
                                <th>&nbsp;<?php xl('3pm-5pm', 'e') ?></th>
                                <th>&nbsp;<?php xl('5pm-7pm', 'e') ?></th>
                                <th>&nbsp;<?php xl('7pm-9pm', 'e') ?></th> 

                                </thead>

                                <?php
                                foreach ($main_arr AS $k => $v) {
                                    ?>
                                    <tr bgcolor='<?php echo $bgcolor ?>'>
                                        <td class="detail">
                                            &nbsp;<?php echo $k; ?>
                                        </td>

                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="detail">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>

                                    </tr>
                                    <?php foreach ($v AS $k1 => $v1) { ?>

                                        <tr>
                                            <td class="detail">
                                                &nbsp;&nbsp;&nbsp;
                                            </td>

                                            <td class="detail">
                                                &nbsp;<?php echo $v1['Date']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['9-11']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['11-1']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['1-3']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['3-5']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['5-7']; ?>
                                            </td>
                                            <td class="detail">
                                                &nbsp;<?php echo $v1['7-9']; ?>
                                            </td>

                                        </tr>

                                        <?php
                                    } // outer for loop 
                                }// foreach	
                                echo "</table>\n";
                            } else {

                                if (empty($form_facility)) {
                                    
                                    
                                    ##########Cron Request####################section only for all FACILITY#########################
                                    // following value will be change according to report
                                    $rcsl_name_type = "appoint_slots_report"; // changable
                                    $selectedMonth = array(1=>"January",2=>"February",3=>"March",4=>"April",5=>"May",6=>"June",7=>"July",8=>"August",9=>"September",10=>"October",11=>"November",12=>"December");
                                    $months = $selectedMonth[$month];
                                    $rcsl_requested_filter = addslashes(serialize(array("month" => $_POST['month'],"noDateRange"=>"For $months"))); // changable
                                    $rcsl_report_description = "Appointment Slots report for month"; // changable
                                    //allFacilityReports() defined with globals.php
                                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if($msgForReportLog){
                                    $msgForReportLog;
                                    }
                                    else{
                                    $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                    }
                                    ##########Cron Request####################section only for all FACILITY#########################
                                    echo "<tr><td colspan=\"5\" align=\"center\">$msgForReportLog</td> </tr>";
                                    
                                    $searchParam .=  ' Clinic = All |'; 
                                     
                                    $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                                    
                                } else {
                                    ?>
                                    <table>
                                        <tr>
                                            <td colspan="5" style="text-align:center; font-size:10pt;">No Results Found</td>
                                        </tr>
                                    </table>
                                    <?php
                                }
                            }
                        }
                        ?>
                </div> 
            </form>
    </body>
    </html>
<?php } ?>
    