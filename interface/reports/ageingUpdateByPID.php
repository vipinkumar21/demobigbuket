<?php
/**
 * ageingUpdateByPID
 * @Last Modified By: Deepak Aneja
 * @Modified Date: 07-June-2016
 * @desc: Calculate the due and advance for a patient and add that to 'ageing_report' table.
 */

//Function to get global data
function getGlobalDataForAgeing($db){
    $globalsQuery = "SELECT gl_name, gl_index, gl_value FROM globals ORDER BY gl_name, gl_index";
    
    $stmt = $db->prepare($globalsQuery);
    $stmt->execute();
    $glres = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $returnGlobalData = array();
    foreach ($glres as $glrow) {
        $gl_name  = $glrow['gl_name'];
        $gl_value = $glrow['gl_value'];
        $returnGlobalData[$gl_name] = $gl_value; 
    }
    return $returnGlobalData;
}

//Function to get non patient data
function nonPatientDetailForAgeing($nonpatientId, $db) {
    $sql = "SELECT npd.np_id AS patientid, npd.np_name AS firstname, npd.np_mobile AS mobile, npd.np_email AS email, pd.pubpid FROM nonpatient_data AS npd LEFT OUTER JOIN patient_data AS pd ON pd.id = npd.ep_id WHERE np_id = $nonpatientId ";
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    //$res = sqlStatement($sql);
    //$row = sqlFetchArray($res);
    return $row;
}

//Function to get doctors name
function getDoctorNameForAgeing($invId, $db){
    $returnData = '';
    $sqlQuery = "SELECT CONCAT_WS(' ', u.fname, u.lname) AS dname FROM invoice_items AS ii INNER JOIN billing AS b ON b.id = ii.invit_tp_id INNER JOIN users AS u ON u.id = b.provider_id WHERE ii.`invit_inv_id` = ".$invId." GROUP BY b.provider_id";
    $stmt = $db->prepare($sqlQuery);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //$results = sqlStatement($sqlQuery);
    foreach ($results as $row) {
        if (empty($returnData)) {
            $returnData = $row['dname'];
        } else {
            $returnData .= ', ' . $row['dname'];
        }
    }
    return $returnData;
}

//Function to get clinic/facility details
function getClinicDetailForAgeing($clinicId, $db) {
    $sql = "SELECT name, facility_npi FROM facility WHERE id = $clinicId ";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    //$res = sqlStatement($sql);
    //$row = sqlFetchArray($res);
    return $row;
}

function getInvoiceBasedFinance($invId, $db) {
    $sql = "SELECT * FROM inv_rect_finance WHERE irf_inv_id = $invId AND irf_status = 1 ORDER BY id DESC LIMIT 1";
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    //$res = sqlStatement($sql);
    //$row = sqlFetchArray($res);
    return $row;
}

//calculateAgeingReprtByPatientId(17);
function calculateAgeingReprtByPatientId($pubpidFun, $dbConn = NULL) {
    if(empty($dbConn)) {
        $db = getConnection();
    } else {
        $db = $dbConn;
    }
    $logger = Logger::getLogger("Invoices");
    $logger->info("calculateAgeingReprtByPatientId start");
    
    $paramsArray = getLogedUserDetail();
    
    $totalpts = 0;
    $ageingDataArray = array();
    
    $sql = "delete from ageing_reports where age_pid='$pubpidFun'";
    debugPDO($sql,array(),'Change Ageing',1,$GLOBALS['enable_auditlog'],$paramsArray['username'],$pubpidFun);
    //sqlQuery($sql);
    $stmt = $db->prepare($sql);
    $stmt->execute();
    
    /**************************************** Due Section Start ********************************************************/

    //Firstly, we are calculating 'Dues' for the patients on the basis of invoices
    $where = " inv.inv_deletestate = 1 and pd.id = '$pubpidFun'"; //Active Invoices

    //Now we are fetching active invoices
    $dueQuery = "SELECT inv.*, pd.id as pid, '0' as npid, CONCAT(pd.fname,' ',pd.lname) as pName, pd.pubpid FROM invoice AS inv "
                . "INNER JOIN patient_data AS pd ON pd.id = inv.inv_pid WHERE " . $where;
    $dueQueryRes = $db->prepare($dueQuery);
    $dueQueryRes->execute();
    $dueQueryResList = $dueQueryRes->fetchAll(PDO::FETCH_ASSOC);
    
    foreach ($dueQueryResList as $dueRow) {

        $patient_id = $dueRow['pid'];
        $clinicId = $dueRow['inv_clinic_id'];
        //Get Clinic/Facility Details
        $clinicDetails = getClinicDetailForAgeing($clinicId, $db);
        $clinicId='';
        $clinicName='';
        $clinicReportId='';    
        if(!empty($clinicDetails)){        
            $clinicName=$clinicDetails['name'];
            $clinicReportId=$clinicDetails['facility_npi'];
        }
        //Get Doctor Name. Doctor can be more than one for a particular invoice.
        $drName = getDoctorNameForAgeing($dueRow['inv_id'], $db);

        //Get all the treatments details
        $dueTpQuery = "SELECT invit.*, bill.work_status, bill.code_type, bill.code, bill.code_text, bill.units, bill.fee, bill.discount, bill.discount_type, bill.unit_price AS pr_price FROM invoice_items AS invit INNER JOIN billing AS bill ON bill.id = invit.invit_tp_id WHERE invit.invit_inv_id = " . $dueRow['inv_id'];
        $dueTpQuery .= " AND invit.invit_deleted = 0"; 
        $dueTpQuery .= " GROUP BY invit.invit_tp_id";
        $dueTpStmt = $db->prepare($dueTpQuery);
        $dueTpStmt->execute();
        $dueTpRes = $dueTpStmt->fetchAll(PDO::FETCH_ASSOC);
        
        //$dueTpRes = sqlStatement($dueTpQuery);

        //Varibles For Calculation purpose
        $charges = 0;   //Total Cost
        $adjustments = 0;   //Total Discount
        $treatments = '';
        $paid = 0;      //Total Paid

        //Here we are initializing the Invoice Treatment Status as 'Completed'.
        $invCurrentStatus=2;    //In-progress=>1  OR  Completed=>2

        $globalsDataSep='';
        $globalsData = getGlobalDataForAgeing($db);
        //Get global seperator for Treatments
        $globalsDataSep=$globalsData['gbl_invoice_trt_seperator'];

        //Calculate the charges, adjustments and treatments name
        foreach ($dueTpRes as $tpRow) {
            //This is to check the status of the treatments related to that particular invoice.
            if($tpRow['work_status']=='1'){
                $invCurrentStatus=1;
            }

            $charges += $tpRow['pr_price'] * $tpRow['units'];

            if ($tpRow['discount_type'] == 'Amt') {
                $adjustments += $tpRow['discount'];
            } else {
                $adjustments += round((($tpRow['pr_price'] * $tpRow['units']) / 100) * $tpRow['discount']);
            }

            if (!empty($treatments)) {
                if(isset($tpRow['code_text'])){
                    $treatments .= $tpRow['code_text'] . "$globalsDataSep";
                }
            } else {
                if(isset($tpRow['code_text'])){
                    $treatments = $tpRow['code_text'] . "$globalsDataSep";
                }
            }
        }

        //Get the information for Product based Invoice
        $prosql = "SELECT i.inv_id, i.inv_number, i.inv_deletestate, i.inv_created_date, ii.invit_deleted, i.inv_clinic_id, ps.ps_discount, ROUND(ps.ps_discount) AS rounddiscount, ps.ps_discount_type, ps.ps_fee, ROUND(ps.ps_fee) AS roundfee, ps.ps_quantity, ii.invit_deleted, ps.ps_npid, ps.ps_clinic_id, invis.invist_price, invis.invist_quantity, invim.inv_im_name, ps.ps_deletestate ";
        $prosql .= "FROM invoice AS i ";
        $prosql .= "LEFT JOIN invoice_items AS ii ON ii.invit_inv_id = i.inv_id ";
        $prosql .= "LEFT JOIN product_sales AS ps ON ii.invit_ps_id = ps.ps_id ";
        $prosql .= "LEFT JOIN inv_item_stock AS invis ON ps.ps_stockid = invis.invist_id ";
        $prosql .= "LEFT JOIN inv_item_master AS invim ON invim.inv_im_id = invis.invist_itemid ";
        $prosql .= "WHERE i.inv_id = " . $dueRow['inv_id'] . " AND (ii.invit_tp_id = 0 OR ii.invit_tp_id IS NULL) AND ii.invit_deleted=0";
        
        $proSqlStmt = $db->prepare($prosql);
        $proSqlStmt->execute();
        $proRes = $proSqlStmt->fetchAll(PDO::FETCH_ASSOC);
        
        //$proRes = sqlStatement($prosql);

        foreach ($proRes as $proRow) {

            $invoiceDiscount = 0;
            if ($proRow['ps_discount_type'] == 'amt') {
                $invoiceDiscount += $proRow['ps_discount'];
            } else {
                 $invoiceDiscount += round((($proRow['invist_price'] * $proRow['ps_quantity']) / 100) * $proRow['ps_discount']);
            }

            //Add the discount to the previous adjustments
            $adjustments += $invoiceDiscount;
            //Add the discount to the charges as well 
            $charges += $proRow['ps_fee'] + $invoiceDiscount;

            //Add the product name with the treatment 
            if (!empty($treatments)) {
                if(isset($proRow["inv_im_name"])){
                  $treatments .= $proRow["inv_im_name"] . "$globalsDataSep";  
                }
            } else {
                if(isset($proRow["inv_im_name"])){
                    $treatments = $proRow["inv_im_name"] . "$globalsDataSep";
                }
            }
        }


        $dueBalance = 0;
        //Calculate the payments related to the invoice
        $paidAmountQuery = "SELECT SUM(rep.rect_amount) AS paidAmt FROM invoice_reciepts AS ir INNER JOIN reciept AS rep ON rep.rect_id = ir.invrect_rect_id WHERE rep.rect_deletestate = 1 AND rep.rect_type != 'Credit' AND ir.invrect_inv_id = " . $dueRow['inv_id'];
        //$paidAmount = sqlFetchArray($paidAmountRes);
        
        $paidAmountStmt = $db->prepare($paidAmountQuery);
        $paidAmountStmt->execute();
        $paidAmount = $paidAmountStmt->fetch(PDO::FETCH_ASSOC);
        
        if (!empty($paidAmount['paidAmt'])) {
            $paid = $paidAmount['paidAmt'];
        } else{
            //Paid is set to 0 initially
        }

        //Get the Non Patient Details
        if ($dueRow['pubpid'] == 'NA') {
            $patientProfile = nonPatientDetailForAgeing($dueRow['inv_npid'], $db);
            if (!empty($patientProfile['pubpid'])) {
                $dueRow['pubpid'] = $patientProfile['pubpid'];
            } else {
                $dueRow['pubpid'] = 'NA';
            }
        }

        //row array is defined for debugging purpose/ To print the result set
        
        $net_cost = $charges - $adjustments;
        $due_amount = $net_cost - $paid;
        
        //Finance Data
        $invoiceBasedFinanceData = getInvoiceBasedFinance($dueRow['inv_id'], $db);
        $age_finance_status =  $age_finance_details =  $age_finance_recovery_date = $age_finance_remarks  = $age_finance_mantis_id =  '';
        
        if(!empty($invoiceBasedFinanceData)) {
            $age_finance_status =  $invoiceBasedFinanceData['irf_finance_status'];
            $age_finance_details =  addslashes($invoiceBasedFinanceData['irf_details']);
            $age_finance_recovery_date =  $invoiceBasedFinanceData['irf_finance_recovery_date'];
            $age_finance_remarks =  addslashes($invoiceBasedFinanceData['irf_finance_remarks']);
            $age_finance_mantis_id =  $invoiceBasedFinanceData['irf_finance_mantis_id'];
        }
        
        
        $logger->debug("age_pid =".$dueRow['pid']);
        $logger->debug("age_npid =".$dueRow['npid']);
        $logger->debug("age_pubpid =".$dueRow['pubpid']);
        $logger->debug("age_patient_name =".$dueRow['pName']);
        $logger->debug("age_clinic_id =".$dueRow['inv_clinic_id']);
        $logger->debug("age_clinic_npi =".$clinicReportId);
        $logger->debug("age_clinic_name =".$clinicName);
        $logger->debug("age_doctor =".$drName);
        $logger->debug("age_inv_id =".$dueRow['inv_id']);
        $logger->debug("age_inv_number =".$dueRow['inv_number']);
        $logger->debug("age_inv_date =".$dueRow['inv_created_date']);
        $logger->debug("age_rect_id = ''");
        $logger->debug("age_rect_number = ''");
        $logger->debug("age_rect_date = ''");
        $logger->debug("age_tr_name = ".$treatments);
        $logger->debug("age_tr_workstatus = ".$invCurrentStatus);
        $logger->debug("age_due = ".$due_amount);
        $logger->debug("age_advance = ".$charges);
        $logger->debug("charges = ''");
        $logger->debug("adjustments = ".$adjustments);
        $logger->debug("paid = ".$paid);
        
        if($due_amount > 0){
            //For insertion purpose
            $age_pid = $dueRow['pid'];
            if(!empty($dueRow['npid'])){
                $age_npid=$dueRow['npid'];        
            }else{
                $age_npid = 0;        
            }
            //$age_npid = $dueRow['npid'];
            $age_pubpid = $dueRow['pubpid'];
            $age_patient_name = addslashes($dueRow['pName']);
            $age_clinic_id = $dueRow['inv_clinic_id'];
            $age_inv_id = $dueRow['inv_id']; 
            $age_inv_number = $dueRow['inv_number'];
            $age_inv_date = $dueRow['inv_created_date'];
            $age_tr_name = addslashes($treatments);

            $insertDueQuery = "insert into ageing_reports(age_pid, age_npid, age_pubpid, age_patient_name, age_clinic_id, age_clinic_npi, 
                        age_clinic_name, age_doctor, age_inv_id, age_inv_number,age_inv_date, 
                        age_tr_name, age_tr_workstatus, age_due, age_finance_status, age_finance_details, age_finance_recovery_date, age_finance_remarks, age_finance_mantis_id) 
                        values( 
                                '$age_pid',
                                '$age_npid',
                                '$age_pubpid',
                                '$age_patient_name',
                                '$age_clinic_id',
                                '$clinicReportId',
                                '$clinicName',
                                '$drName',
                                '$age_inv_id',
                                '$age_inv_number',
                                '$age_inv_date',
                                '$age_tr_name',
                                '$invCurrentStatus',
                                '$due_amount',
                                '$age_finance_status',
                                '$age_finance_details',
                                '$age_finance_recovery_date',
                                '$age_finance_remarks',
                                '$age_finance_mantis_id'
                                )";
            debugPDO($insertDueQuery,array(),'Change Due Ageing',1,$GLOBALS['enable_auditlog'],$paramsArray['username'],$pubpidFun);
            $mainQuery = $db->prepare($insertDueQuery);
            $mainQuery->execute();
            //sqlStatement($insertDueQuery);
        }
    }

    /**************************************** Due Section End ********************************************************/

    /**************************************** Advance Section start ********************************************************/

    $advanceQuery = "SELECT pd.id AS age_pid, rect.rect_npid AS age_npid, CONCAT(pd.fname,' ',pd.lname) AS age_patient_name, pd.pubpid AS age_pubpid,
                    fac.id AS age_clinic_id, fac.facility_npi AS age_clinic_npi, fac.name AS age_clinic_name, 
                    rect.rect_created_by AS doctor_id, rect.rect_id AS age_rect_id, rect.rect_number AS age_rect_number, rect.rect_created_date AS age_rect_date, rect.rect_amount AS age_advance,
                    irf.irf_finance_status, irf.irf_details, irf.irf_finance_recovery_date, irf.irf_finance_remarks, irf.irf_finance_mantis_id
                    FROM reciept AS rect
                    INNER JOIN patient_data AS pd ON pd.id = rect.rect_pid AND pd.id = '$pubpidFun'
                    LEFT JOIN facility AS fac ON fac.id = rect.rect_clinic_id
                    LEFT JOIN inv_rect_finance AS irf ON irf.irf_rect_id = rect.rect_id AND irf.irf_status = 1
                    WHERE rect.rect_id NOT IN
                    (SELECT invrect_rect_id FROM invoice_reciepts WHERE invrect_rect_id > 0 AND invrect_inv_id > 0)
                    AND rect.rect_amount > 0 
                    AND rect.rect_type='Advance' 
                    AND rect.rect_deletestate=1
                    ORDER BY rect.rect_id ASC";
    $advanceStmt = $db->prepare($advanceQuery);
    $advanceStmt->execute();
    $advResult = $advanceStmt->fetchAll(PDO::FETCH_ASSOC);
    
    //$advResult = sqlStatement($advanceQuery);
    foreach ($advResult as $advRow) {
        if(!empty($advRow['age_npid'])) {
            $age_npid=$advRow['age_npid'];        
        } else {
            $age_npid = 0;        
        }
            
        $docName='';
        $doctor_id= $advRow['doctor_id'];
        //Get Doctor Details
        $docQuery="SELECT CONCAT_WS(' ', u.fname, u.lname) AS dname FROM users as u where u.id = $doctor_id";
        $doctorStmt = $db->prepare($docQuery);
        $doctorStmt->execute();
        $docQueryRes = $doctorStmt->fetchAll(PDO::FETCH_ASSOC);
                    
        //$docQueryRes = sqlStatement($docQuery);
        foreach ($docQueryRes as $docRow) {
            $docName = $docRow['dname'];
        }
        
        $logger->debug("age_pid =".$advRow['age_pid']);
        $logger->debug("age_npid =".$advRow['age_npid']);
        $logger->debug("age_pubpid =".$advRow['age_pubpid']);
        $logger->debug("age_patient_name =".$advRow['age_patient_name']);
        $logger->debug("age_clinic_id =".$advRow['age_clinic_id']);
        $logger->debug("age_clinic_npi =".$advRow['age_clinic_npi']);
        $logger->debug("age_clinic_name =".$advRow['age_clinic_name']);
        $logger->debug("age_doctor =".$docName);
        $logger->debug("age_inv_id = ''");
        $logger->debug("age_inv_number = ''");
        $logger->debug("age_inv_date = ''");
        $logger->debug("age_rect_id =". $advRow['age_rect_id']);
        $logger->debug("age_rect_number =". $advRow['age_rect_number']);
        $logger->debug("age_rect_date =". $advRow['age_rect_date']);
        $logger->debug("age_tr_name = ''");
        $logger->debug("age_tr_workstatus = ''");
        $logger->debug("age_due = ''");
        $logger->debug("age_advance =". $advRow['age_advance']);
        
        $insertAdvanceQuery = "insert into ageing_reports(age_pid, age_npid, age_pubpid, age_patient_name, age_clinic_id, age_clinic_npi, 
                        age_clinic_name, age_doctor, age_rect_id, age_rect_number, age_rect_date, age_advance, age_finance_status, age_finance_details, age_finance_recovery_date, age_finance_remarks, age_finance_mantis_id) 
                        values( 
                                '".$advRow['age_pid']."',
                                '".$age_npid."',
                                '".$advRow['age_pubpid']."',
                                '".$advRow['age_patient_name']."',
                                '".$advRow['age_clinic_id']."',
                                '".$advRow['age_clinic_npi']."',
                                '".$advRow['age_clinic_name']."',
                                '".$docName."',
                                '".$advRow['age_rect_id']."',
                                '".$advRow['age_rect_number']."',
                                '".$advRow['age_rect_date']."',
                                '".$advRow['age_advance']."',
                                '".$advRow['irf_finance_status']."',
                                '".addslashes($advRow['irf_details'])."',
                                '".$advRow['irf_finance_recovery_date']."',
                                '".addslashes($advRow['irf_finance_remarks'])."',
                                '".$advRow['irf_finance_mantis_id']."'
                                )";
            debugPDO($insertAdvanceQuery,array(),'Change Advance Ageing',1,$GLOBALS['enable_auditlog'],$paramsArray['username'],$pubpidFun);
            //sqlStatement($insertAdvanceQuery);
            $mainAdvanceQuery = $db->prepare($insertAdvanceQuery);
            $mainAdvanceQuery->execute();
    }

    /**************************************** Advance Section End ********************************************************/
    
    $logger->info("calculateAgeingReprtByPatientId end");
    if(empty($dbConn)) {
        $db = null;
    }
}
//end of function