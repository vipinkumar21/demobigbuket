<table border="0" align="center" cellspacing="10" cellpadding="5" width="100%" height="22">
    <tr bgcolor="#00ffff">
        <?php if (acl_check('admin', 'notification')) { ?>
            <td valign="middle" nowrap><a class=menu href="../sms_email_template/followup_reminder.php"
                                          onclick="top.restoreSession()"
                                          title="Follow Up Reminder Email/SMS Alert"><?php echo xl('Follow Up Reminder Email/SMS Alert', 'e'); ?></a>
            </td>      
            <td valign="middle" nowrap><a class=menu href="../sms_email_template/oneweek_reminder.php"
                                          onclick="top.restoreSession()"
                                          title="One Week Email/SMS Alert"><?php echo xl('One Week Email/SMS Alert', 'e'); ?></a>
            </td>
            <td valign="middle" nowrap><a class=menu href="../sms_email_template/sixmonths_reminder.php"
                                          onclick="top.restoreSession()"
                                          title="6 Months Email/SMS Alert"><?php echo xl('6 Months Email/SMS Alert', 'e'); ?></a>
            </td>
            <td valign="middle" nowrap><a class=menu href="../sms_email_template/twelvemonths_reminder.php"
                                          onclick="top.restoreSession()"
                                          title="12 Months Email/SMS Alert"><?php echo xl('12 Months Email/SMS Alert', 'e'); ?></a>
            </td>
            <td valign="middle" nowrap><a class=menu href="../sms_email_template/birthday_reminder.php"
                                          onclick="top.restoreSession()"
                                          title="Patient Birthday Email/SMS Alert"><?php echo xl('Patient Birthday Email/SMS Alert', 'e'); ?></a>
            </td>
    <tr bgcolor="#00ffff">
            <td valign="middle" nowrap><a class=menu href="../sms_email_template/appt_trtmnt_reminder.php"
                                          onclick="top.restoreSession()"
                                          title="App. Trt. Reminder"><?php echo xl('Reminder:Treatment in progress', 'e'); ?></a>
            </td>
    </tr>        
        <?php } ?>
    </tr>
</table>