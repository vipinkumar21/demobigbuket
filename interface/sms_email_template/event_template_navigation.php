<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%" height="22">
<tr bgcolor="#00ffff">
<?php if (acl_check('admin', 'notification')) { ?>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/event_email_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('New Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/event_sms_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('New SMS Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/event_email_update_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Update Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/event_sms_update_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Update SMS Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/event_email_delete_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Delete Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/event_sms_delete_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Delete SMS Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/pending_treatment_email_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Pending Treatment Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/pending_treatment_sms_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Pending Treatment SMS Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/rearrange_app_email_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Rearrange Appointment Alert','e');?></a>&nbsp;
</td>
<?php } ?>

<td width="20%">&nbsp;</td>
</tr>
</table>