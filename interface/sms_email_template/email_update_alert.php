<?php
//INCLUDES, DO ANY ACTIONS, THEN GET OUR DATA
include_once("../globals.php");
include_once("$srcdir/registry.inc");
include_once("$srcdir/sql.inc");
include_once("../../library/acl.inc");
include_once("batchcom.inc.php");

// gacl control
$thisauth = acl_check('admin', 'notification');

if (!$thisauth) {
  echo "<html>\n<body>\n";
  echo "<p>".xl('You are not authorized for this.','','','</p>')."\n";
  echo "</body>\n</html>\n";
  exit();
 }

 // default value
$next_app_date = date("Y-m-d");
$hour="12";
$min="15";
$provider_name="EMR Group";
$message="Welcome to EMR Group";
$type = "Update";
$email_sender = "EMR Group";
$email_subject = "Welcome to EMR Group";
// process form
if ($_POST['form_action']=='Save') 
{
    //validation uses the functions in notification.inc.php
    if ($_POST['ent_patient_subject']=="") $form_err.=xl('Empty value in "Patient Email Sender"','','<br>');
	if ($_POST['ent_patient_message']=="") $form_err.=xl('Empty value in "Patient Email Template"','','<br>');
    if ($_POST['ent_provider_subject']=="") $form_err.=xl('Empty value in "Provider Email Subject"','','<br>');
    if ($_POST['ent_provider_message']=="") $form_err.=xl('Empty value in "Provider Email Template"','','<br>');
    //process sql
    if (!$form_err) 
    {
        $next_app_time = $_POST[hour].":".$_POST['min'];
        $sql_text=" ( `ent_id` , `ent_tem_type` , `ent_patient_subject` , `ent_provider_subject` , `ent_provider_message` , `ent_patient_message` ) ";
        $sql_value=" ( '".$_POST['ent_id']."' , '".$_POST['type']."' , '".$_POST['ent_patient_subject']."' , '".$_POST['ent_provider_subject']."' , '".$_POST['ent_provider_message']."' , '".$_POST['ent_patient_message']."') ";
        $query = "REPLACE INTO `email_notification_template` $sql_text VALUES $sql_value";
        //echo $query;
        $id = sqlInsert($query);
        $sql_msg="ERROR!... in Update";
        if($id)    $sql_msg="Email Alert Notification Updated Successfully";
    } 
}

// fetch data from table
$sql="select * from email_notification_template where ent_tem_type='$type'";
$result = sqlQuery($sql);
if($result)
{
    $ent_id = $result['ent_id'];
    $patient_subject=$result['ent_patient_subject'];
    $provider_subject=$result['ent_provider_subject'];
    $patient_message=$result['ent_patient_message'];
	$provider_message=$result['ent_provider_message'];
}
//my_print_r($result);

// menu arrays (done this way so it's easier to validate input on validate selections)
$hour_array =array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','21','21','22','23');
$min_array = array('00','05','10','15','20','25','30','35','40','45','50','55');

//START OUT OUR PAGE....
?>
<html>
<head>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="batchcom.css" type="text/css">
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/calendar.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/custom_template/fullckeditor/ckeditor.js"></script>
</head>
<body class="body_top">
<span class="title"><?php include_once("template_navigation.php");?></span>
<span class="title"><?php xl('Update Email Alert Template','e')?></span>
<br><br>
<!-- for the popup date selector -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<FORM name="select_form" METHOD=POST ACTION="">
<input type="Hidden" name="type" value="<?php echo $type;?>">
<input type="Hidden" name="ent_id" value="<?php echo $ent_id;?>">
<div class="text">
    <div class="main_box">
        <?php
        if ($form_err) {
            echo ("The following errors occurred<br>$form_err<br><br>");
        }
        if ($sql_msg) {
            echo ("$sql_msg<br><br>");
        }
        ?>
        <?php xl('Patient Email Subject','e')?> :
        <INPUT TYPE="text" NAME="ent_patient_subject" size="100" value="<?php echo $patient_subject?>">
        <br>
        
        <?php xl('Patient Template','e')?> :        
        <br>
        <?php xl('Email Dynamic Text, Usable Tag: ***PATIENT_NAME***, ***APPDATETIME***, ***FACILITYNAME***, ***FACILITY_STREET***, ***FACILITY_CITY***, ***FACILITY_STATE***, ***FACILITY_COUNTRY***, ***FACILITY_ZIP***, ***FACILITY_PHONE***, ***FACILITY_WEBSITE***','e')?>
        <br>
        <textarea name="ent_patient_message" id="ent_patient_message" rows="10" cols="10"><?php echo $patient_message?></textarea>
		<script type="text/javascript">
			CKEDITOR.replace( 'ent_patient_message' );
		</script>
        <br>
		<?php xl('Provider Email Subject','e')?> :
        <INPUT TYPE="text" NAME="ent_provider_subject" size="100" value="<?php echo $provider_subject?>">
        <br>
		 <?php xl('Provider Template','e')?> :        
        <br>
        <?php xl('Email Dynamic Text, Usable Tag: ***PROVIDERNAME***, ***APPDATETIME***, ***PATIENT_NAME***, ***PATIENTID***, ***FACILITYNAME***, ***FACILITY_STREET***, ***FACILITY_CITY***, ***FACILITY_STATE***, ***FACILITY_COUNTRY***, ***FACILITY_ZIP***, ***FACILITY_PHONE***, ***FACILITY_WEBSITE***','e')?>
        <br>
        <textarea name="ent_provider_message" id="ent_provider_message" rows="10" cols="10"><?php echo $provider_message?></textarea>
		<script type="text/javascript">
			CKEDITOR.replace( 'ent_provider_message' );
		</script>
        <br><br>
        <INPUT TYPE="submit" name="form_action" value="Save">
    </div>
</div>
</FORM>
