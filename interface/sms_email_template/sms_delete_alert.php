<?php
//INCLUDES, DO ANY ACTIONS, THEN GET OUR DATA
include_once("../globals.php");
include_once("$srcdir/registry.inc");
include_once("$srcdir/sql.inc");
include_once("../../library/acl.inc");
include_once("batchcom.inc.php");

// gacl control
$thisauth = acl_check('admin', 'notification');

if (!$thisauth) {
  echo "<html>\n<body>\n";
  echo "<p>".xl('You are not authorized for this.','','','</p>')."\n";
  echo "</body>\n</html>\n";
  exit();
 }

 // default value
$next_app_date = date("Y-m-d");
$hour="12";
$min="15";
$provider_name="EMR Group";
$message="Welcome to EMR Group";
$type = "Delete";

// process form
if ($_POST['form_action']=='Save') 
{
    //validation uses the functions in notification.inc.php
    // validates and or
    if ($_POST['snt_patient_message']=="") $form_err.=xl('Empty value in "Patient Template"','','<br>');
    if ($_POST['snt_provider_message']=="") $form_err.=xl('Empty value in "Provider Template"','','<br>');	
    //process sql
    if (!$form_err) 
    {
        if($_POST['snt_patient_draft_status']){
			$patientSql = "UPDATE sms_notification_template SET `snt_patient_message` = '".$_POST['snt_patient_message']."', `snt_patient_message_draft` = '', `snt_patient_draft_status` ='".$_POST['snt_patient_draft_status']."' WHERE `snt_id` = ".$_POST['snt_id'];
		}else {
			$patientSql = "UPDATE sms_notification_template SET `snt_patient_message_draft` = '".$_POST['snt_patient_message']."', `snt_patient_draft_status` ='".$_POST['snt_patient_draft_status']."' WHERE `snt_id` = ".$_POST['snt_id'];
		}
		
		if($_POST['snt_provider_draft_status']){
			$providerSql = "UPDATE sms_notification_template SET `snt_provider_message` = '".$_POST['snt_provider_message']."', `snt_provider_message_draft` = '', `snt_provider_draft_status` ='".$_POST['snt_provider_draft_status']."' WHERE `snt_id` = ".$_POST['snt_id'];
		}else {
			$providerSql = "UPDATE sms_notification_template SET `snt_provider_message_draft` = '".$_POST['snt_provider_message']."', `snt_provider_draft_status` ='".$_POST['snt_provider_draft_status']."' WHERE `snt_id` = ".$_POST['snt_id'];;
		}
		if(sqlStatement( $patientSql ) && sqlStatement( $providerSql )){
			$sql_msg="SMS Alert Template Updated Successfully";
		}else {
			$sql_msg="ERROR!... in Update";
		}           
    } 
}

// fetch data from table
$sql="select * from sms_notification_template where snt_tem_type='$type'";
$result = sqlQuery($sql);
if($result)
{
    $snt_id = $result['snt_id'];
    $snt_provider_message=$result['snt_provider_message'];
	$snt_patient_message=$result['snt_patient_message'];
	$snt_provider_message_draft=$result['snt_provider_message_draft'];
	$snt_patient_message_draft=$result['snt_patient_message_draft'];
	$snt_provider_draft_status=$result['snt_provider_draft_status'];
	$snt_patient_draft_status=$result['snt_patient_draft_status'];
}
//my_print_r($result);
?>
<html>
<head>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="batchcom.css" type="text/css">

<script type="text/javascript" src="../../library/overlib_mini.js"></script>
<script type="text/javascript" src="../../library/calendar.js"></script>

<script type="text/javascript">  
function textchange(yesFieldId, noFieldId)  
{  
	document.getElementById(''+yesFieldId+'').checked = false;
	document.getElementById(''+noFieldId+'').checked = true;
}  
</script> 
</head>
<body class="body_top">
<span class="title"><?php include_once("template_navigation.php");?></span>
<span class="title"><?php xl('Delete SMS Alert Template','e')?></span>
<br><br>
<!-- for the popup date selector -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<FORM name="select_form" METHOD=POST ACTION="">
<input type="Hidden" name="type" value="<?php echo $type;?>">
<input type="Hidden" name="snt_id" value="<?php echo $snt_id;?>">
<div class="text">
    <div class="main_box">
        <?php
        if ($form_err) {
            echo ("The following errors occurred<br>$form_err<br><br>");
        }
        if ($sql_msg) {
            echo ("$sql_msg<br><br>");
        }
        ?>
        
        <?php xl('Patient Template','e')?> :
        <br>
        <?php xl('SMS Text, Usable Tag: ***PATIENT_NAME***, ***APPDATETIME***, ***FACILITY_STREET***,***FACILITY_CITY***, ***FACILITY_PHONE***','e')?>
        <br>
		<?php if($snt_patient_draft_status == 1){?>
			<TEXTAREA NAME="snt_patient_message" ROWS="8" COLS="35"  onchange ="textchange('snt_patient_draft_status_yes', 'snt_patient_draft_status_no')"><?php echo $snt_patient_message?></TEXTAREA>
		<?php }else {?>
			<TEXTAREA NAME="snt_patient_message" ROWS="8" COLS="35"><?php echo $snt_patient_message_draft?></TEXTAREA>
		<?php }?>
		<br>
		 <?php xl('Patient Template Verification Status','e')?> :
        <br>
        <input type="radio" name="snt_patient_draft_status" id="snt_patient_draft_status_yes" value="1" <?php if($snt_patient_draft_status == 1){echo 'checked=checked';}?>>Yes &nbsp;&nbsp; <input type="radio" name="snt_patient_draft_status" id="snt_patient_draft_status_no" value="0" <?php if($snt_patient_draft_status == 0){echo 'checked=checked';}?>>No
        <br><br>
		<?php xl('Provider Template','e')?> :
        <br>
        <?php xl('SMS Text, Usable Tag: ***PROVIDER_NAME***, ***FACILITY_NAME***, ***APPDATETIME***, ***PATIENTID***','e')?>
        <br>
		<?php if($snt_provider_draft_status == 1){?>
			<TEXTAREA NAME="snt_provider_message" ROWS="8" COLS="35" onchange ="textchange('snt_provider_draft_status_yes', 'snt_provider_draft_status_no')"><?php echo $snt_provider_message?></TEXTAREA>
		<?php } else {?>
			<TEXTAREA NAME="snt_provider_message" ROWS="8" COLS="35"><?php echo $snt_provider_message_draft?></TEXTAREA>
		<?php }?>
		<br>
		 <?php xl('Provider Template Verification Status','e')?> :
        <br>
        <input type="radio" name="snt_provider_draft_status" id="snt_provider_draft_status_yes" value="1" <?php if($snt_provider_draft_status == 1){echo 'checked=checked';}?>>Yes &nbsp;&nbsp; <input type="radio" name="snt_provider_draft_status"  id="snt_provider_draft_status_no" value="0" <?php if($snt_provider_draft_status == 0){echo 'checked=checked';}?>>No
        <br><br>
        <INPUT TYPE="submit" name="form_action" value="Save">
    </div>
</div>
</FORM>
