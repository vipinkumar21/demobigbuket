<?php
//INCLUDES, DO ANY ACTIONS, THEN GET OUR DATA
include_once("../globals.php");
include_once("$srcdir/registry.inc");
include_once("$srcdir/sql.inc");
include_once("../../library/acl.inc");
include_once("batchcom.inc.php");

// gacl control
$thisauth = acl_check('admin', 'notification');

if (!$thisauth) {
    echo "<html>\n<body>\n";
    echo "<p>" . xl('You are not authorized for this.', '', '', '</p>') . "\n";
    echo "</body>\n</html>\n";
    exit();
}

// default value
$type = "Report";

if(isset($_POST['form_action'])){
    $email_entid = $_POST['email_entid'];     
    $email_provider_subject = trim($_POST['ent_provider_subject']); 
    $email_provider_message = trim($_POST['ent_provider_message']);
    
    if ($email_provider_subject=="") { $form_err.=xl('Empty value in "Provider Subject"','','<br>'); }
    if ($email_provider_message=="") { $form_err.=xl('Empty value in "Provider Message"','','<br>'); }
    
    //// Store Email Template Data into table ////////
    if(!empty($email_entid) && $email_entid > 0){
        
        if (!$form_err) 
        {           
            $providerSql = "UPDATE email_notification_template SET `ent_provider_subject` = '".$email_provider_subject."', `ent_provider_message` ='".$email_provider_message."' WHERE `ent_id` = ".$email_entid;
            if(sqlStatement( $providerSql )){
                    $sql_msg="Report Email Template Updated Successfully";
            }else {
                    $sql_msg="ERROR!... in Update";
            }           
        } 
        
    }
    ///// End Code Store Email Template data into table ///////
    
}else {
        // fetch Email Template data from table
        $emailsql = "SELECT ent_id,ent_patient_subject,ent_provider_subject,ent_patient_message,ent_provider_message FROM email_notification_template WHERE ent_tem_type='$type'";
        $emailresult = sqlQuery($emailsql);
        if ($emailresult) {
            $email_entid = $emailresult['ent_id'];
            $email_patient_subject = $emailresult['ent_patient_subject'];
            $email_provider_subject = $emailresult['ent_provider_subject'];
            $email_patient_message = $emailresult['ent_patient_message'];
            $email_provider_message = $emailresult['ent_provider_message'];
        }
        
}

?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <link rel="stylesheet" href="batchcom.css" type="text/css">
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/calendar.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/custom_template/fullckeditor/ckeditor.js"></script>
    </head>
    <body class="body_top">       
        <span class="title"><?php xl('Reports Email Template', 'e') ?></span>       
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form name="select_form" method="post" action="">
            <input type="hidden" name="type" value="<?php echo $type; ?>">
            <input type="hidden" name="email_entid" value="<?php echo $email_entid; ?>">
            <div class="text">
                <div class="main_box">
                    <?php
                    if ($form_err) {
                        echo ("The following errors occurred<br>$form_err<br><br>");
                    }
                    if ($sql_msg) {
                        echo ("$sql_msg<br><br>");
                    }
                    ?>
                    <?php xl('Provider Email Subject', 'e') ?> :
                    <input type="text" name="ent_provider_subject" size="100" value="<?php echo $email_provider_subject ?>">
                    <br>
                    <?php xl('Provider Template', 'e') ?> :        
                    <br>
                    <?php xl('Email Dynamic Text, Usable Tag: ***PROVIDERNAME***, ***REPORTNAME***, ***REQUESTEDDATE***, ***SEARCHPARAM***, ***FACILITYNAME***, ***FACILITY_STREET***, ***FACILITY_CITY***, ***FACILITY_STATE***, ***FACILITY_COUNTRY***, ***FACILITY_ZIP***, ***FACILITY_PHONE***, ***FACILITY_WEBSITE***,***RECORDNOTFOUND***', 'e') ?>
                    <br>
                    <textarea name="ent_provider_message" id="ent_provider_message" rows="10" cols="10"><?php echo $email_provider_message; ?></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('ent_provider_message');
                    </script>
                    <br/><br/>
                     <input type="submit" name="form_action" value="Save">
                </div>
            </div> 
        </form>
    </body>
</html>
