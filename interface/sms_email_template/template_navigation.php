<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%" height="22">
<tr bgcolor="#00ffff">
<?php if (acl_check('admin', 'notification')) { ?>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/email_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('New Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/email_reminder.php"
 onclick="top.restoreSession()"
 title="SMS/Email Alert Settings"><?php echo xl('Email Reminder','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/sms_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('New SMS Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/sms_reminder.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('SMS Reminder','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/email_update_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Update Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/sms_update_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Update SMS Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/email_delete_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Delete Email Alert','e');?></a>&nbsp;
</td>
<td valign="middle" nowrap>
&nbsp;<a class=menu href="../sms_email_template/sms_delete_alert.php"
 onclick="top.restoreSession()"
 title="SMS Notification"><?php echo xl('Delete SMS Alert','e');?></a>&nbsp;
</td>
<?php } ?>

<td width="20%">&nbsp;</td>
</tr>
</table>