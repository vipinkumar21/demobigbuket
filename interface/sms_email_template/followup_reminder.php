<?php
//INCLUDES, DO ANY ACTIONS, THEN GET OUR DATA
include_once("../globals.php");
include_once("$srcdir/registry.inc");
include_once("$srcdir/sql.inc");
include_once("../../library/acl.inc");
include_once("batchcom.inc.php");

// gacl control
$thisauth = acl_check('admin', 'notification');

if (!$thisauth) {
    echo "<html>\n<body>\n";
    echo "<p>" . xl('You are not authorized for this.', '', '', '</p>') . "\n";
    echo "</body>\n</html>\n";
    exit();
}

// default value
$type = "FollowUp";

if(isset($_POST['form_action'])){
    $email_entid = $_POST['email_entid'];     
    $email_provider_subject = trim($_POST['ent_provider_subject']); 
    $email_provider_message = trim($_POST['ent_provider_message']);
    
    if ($email_provider_subject=="") { $form_err.=xl('Empty value in "Provider Subject"','','<br>'); }
    if ($email_provider_message=="") { $form_err.=xl('Empty value in "Provider Message"','','<br>'); }
    
    //// Store Email Template Data into table ////////
    if(!empty($email_entid) && $email_entid > 0){
        
        if (!$form_err) 
        {           
            $providerSql = "UPDATE email_notification_template SET `ent_provider_subject` = '".$email_provider_subject."', `ent_provider_message` ='".$email_provider_message."' WHERE `ent_id` = ".$email_entid;
            if(sqlStatement( $providerSql )){
                    $sql_msg="Follow Up Email Template Updated Successfully";
            }else {
                    $sql_msg="ERROR!... in Update";
            }           
        } 
        
    }
    ///// End Code Store Email Template data into table ///////
    
    
    $sms_sntid = $_POST['sms_sntid'];
    $sms_provider_message = $_POST['snt_provider_message']; 
    $sms_provider_draft_status = $_POST['snt_provider_draft_status']; 
    
    if ($sms_provider_message=="") { $form_err.=xl('Empty value in "Provider Message"','','<br>'); }    
    
    ////// Store SMS Template data into table ////////
    if(!empty($sms_sntid) && $sms_sntid > 0){
        
        if (!$form_err) 
        {
            if($sms_provider_draft_status){
                    $providerSql = "UPDATE sms_notification_template SET `snt_provider_message` = '".$sms_provider_message."', `snt_provider_message_draft` = '', `snt_provider_draft_status` ='".$sms_provider_draft_status."' WHERE `snt_id` = ".$sms_sntid;
            }else {
                    $providerSql = "UPDATE sms_notification_template SET `snt_provider_message_draft` = '".$sms_provider_message."', `snt_provider_draft_status` ='".$sms_provider_draft_status."' WHERE `snt_id` = ".$sms_sntid;
            }
            if(sqlStatement( $providerSql )){
                    $sql_msg="Follow Up SMS Template Updated Successfully";
            }else {
                    $sql_msg="ERROR!... in Update";
            }           
        } 
        
    }
    ///// End Store SMS Template data into table ////////
    
    
}else {
        // fetch Email Template data from table
        $emailsql = "SELECT ent_id,ent_patient_subject,ent_provider_subject,ent_patient_message,ent_provider_message FROM email_notification_template WHERE ent_tem_type='$type'";
        $emailresult = sqlQuery($emailsql);
        if ($emailresult) {
            $email_entid = $emailresult['ent_id'];
            $email_patient_subject = $emailresult['ent_patient_subject'];
            $email_provider_subject = $emailresult['ent_provider_subject'];
            $email_patient_message = $emailresult['ent_patient_message'];
            $email_provider_message = $emailresult['ent_provider_message'];
        }
        // fetch SMS Template data from table
        $smssql = "SELECT snt_id,snt_provider_message,snt_provider_message_draft,snt_provider_draft_status FROM sms_notification_template WHERE snt_tem_type='$type'";
        $smsresult = sqlQuery($smssql);
        if ($smsresult) {
            $sms_sntid = $smsresult['snt_id'];
            $sms_provider_message = $smsresult['snt_provider_message'];
            $sms_provider_message_draft = $smsresult['snt_provider_message_draft'];
            $sms_provider_draft_status = $smsresult['snt_provider_draft_status'];
        }
}

?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <link rel="stylesheet" href="batchcom.css" type="text/css">
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/calendar.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/custom_template/fullckeditor/ckeditor.js"></script>
    </head>
    <body class="body_top">
        <span class="title"><?php include_once("followup_template_navigation.php"); ?></span>
        <span class="title"><?php xl('Followup Email Template', 'e') ?></span>       
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form name="select_form" method="post" action="">
            <input type="hidden" name="type" value="<?php echo $type; ?>">
            <input type="hidden" name="email_entid" value="<?php echo $email_entid; ?>">           
            <input type="hidden" name="sms_sntid" value="<?php echo $sms_sntid; ?>">
            <div class="text">
                <div class="main_box">
                    <?php
                    if ($form_err) {
                        echo ("The following errors occurred<br>$form_err<br><br>");
                    }
                    if ($sql_msg) {
                        echo ("$sql_msg<br><br>");
                    }
                    ?>
                    <?php xl('Provider Email Subject', 'e') ?> :
                    <input type="text" name="ent_provider_subject" size="100" value="<?php echo $email_provider_subject ?>">
                    <br>
                    <?php xl('Provider Template', 'e') ?> :        
                    <br>
                    <?php xl('Email Dynamic Text, Usable Tag: ***PROVIDER_NAME***, ***TOTAL_REMINDER***, ***PATIENT_DETAILS***, ***FACILITY_NAME***, ***FACILITY_STREET***, ***FACILITY_CITY***, ***FACILITY_STATE***, ***FACILITY_COUNTRY***, ***FACILITY_ZIP***, ***FACILITY_PHONE***, ***FACILITY_WEBSITE***', 'e') ?>
                    <br>
                    <textarea name="ent_provider_message" id="ent_provider_message" rows="10" cols="10"><?php echo $email_provider_message; ?></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('ent_provider_message');
                    </script>                    
                </div>
            </div>       
            <span class="title"><?php xl('Followup SMS Template', 'e') ?></span>           
            <div class="text">
                <div class="main_box">                    
                    <?php xl('Provider Template', 'e') ?> :
                    <br>
                    <?php xl('SMS Text, Usable Tag: ***PROVIDER_NAME***, ***FACILITYNAME***, ***NUMBER_OF_REM***', 'e') ?>
                    <br>
                    <?php if ($sms_provider_draft_status == 1) { ?>
                        <textarea name="snt_provider_message" rows="8" cols="35" onchange ="textchange('snt_provider_draft_status_yes', 'snt_provider_draft_status_no')"><?php echo $sms_provider_message ?></TEXTAREA>
                    <?php } else { ?>
                        <textarea name="snt_provider_message" rows="8" cols="35"><?php echo $sms_provider_message_draft ?></TEXTAREA>
                    <?php } ?>
                        <br>
                    <?php xl('Provider Template Verification Status', 'e') ?> :
                <br>
                <input type="radio" name="snt_provider_draft_status" id="snt_provider_draft_status_yes" value="1" <?php echo ($sms_provider_draft_status == 1) ? ' checked=checked' : ''; ?>>Yes &nbsp;&nbsp; 
                <input type="radio" name="snt_provider_draft_status"  id="snt_provider_draft_status_no" value="0" <?php echo ($sms_provider_draft_status == 0) ? ' checked=checked' : ''; ?>>No
                <br><br>
                <input type="submit" name="form_action" value="Save">
            </div>
        </div>
        </form>
    </body>
</html>
