<?php
//INCLUDES, DO ANY ACTIONS, THEN GET OUR DATA
include_once("../globals.php");
include_once("$srcdir/registry.inc");
include_once("$srcdir/sql.inc");
include_once("../../library/acl.inc");
include_once("batchcom.inc.php");

// gacl control
$thisauth = acl_check('admin', 'notification');

if (!$thisauth) {
    echo "<html>\n<body>\n";
    echo "<p>" . xl('You are not authorized for this.', '', '', '</p>') . "\n";
    echo "</body>\n</html>\n";
    exit();
}

// default value
$type = "ApptByTrmntInprogressReminder";

if (isset($_POST['form_action'])) {
    $email_entid = $_POST['email_entid'];
    $email_patient_subject = trim($_POST['ent_patient_subject']);
    $email_patient_message = trim($_POST['ent_patient_message']);

    if ($email_patient_subject == "") {
        $form_err.=xl('Empty value in "Patient Subject"', '', '<br>');
    }
    if ($email_patient_message == "") {
        $form_err.=xl('Empty value in "Patient Message"', '', '<br>');
    }

    //// Store Email Template Data into table ////////
    if (!empty($email_entid) && $email_entid > 0) {

        if (!$form_err) {
            $providerSql = "UPDATE email_notification_template SET `ent_patient_subject` = '" . $email_patient_subject . "', `ent_patient_message` ='" . $email_patient_message . "' WHERE `ent_id` = " . $email_entid;
            if (sqlStatement($providerSql)) {
                $sql_msg = "One Week Email Template Updated Successfully";
            } else {
                $sql_msg = "ERROR!... in Update";
            }
        }
    }
    ///// End Code Store Email Template data into table ///////


    $sms_sntid = $_POST['sms_sntid'];
    $sms_patient_message = $_POST['snt_patient_message'];
    $sms_patient_draft_status = $_POST['snt_patient_draft_status'];

    if ($sms_patient_message == "") {
        $form_err.=xl('Empty value in "Patient Message"', '', '<br>');
    }

    ////// Store SMS Template data into table ////////
    if (!empty($sms_sntid) && $sms_sntid > 0) {

        if (!$form_err) {
            if ($sms_patient_draft_status) {
                $providerSql = "UPDATE sms_notification_template SET `snt_patient_message` = '" . $sms_patient_message . "', `snt_patient_message_draft` = '', `snt_patient_draft_status` ='" . $sms_patient_draft_status . "' WHERE `snt_id` = " . $sms_sntid;
            } else {
                $providerSql = "UPDATE sms_notification_template SET `snt_patient_message_draft` = '" . $sms_patient_message . "', `snt_patient_draft_status` ='" . $sms_patient_draft_status . "' WHERE `snt_id` = " . $sms_sntid;
            }
            if (sqlStatement($providerSql)) {
                $sql_msg = "Updated Successfully";
            } else {
                $sql_msg = "ERROR!... in Update";
            }
        }
    }
    ///// End Store SMS Template data into table ////////
} else {
    // fetch Email Template data from table
    $emailsql = "SELECT ent_id,ent_patient_subject,ent_provider_subject,ent_patient_message,ent_provider_message FROM email_notification_template WHERE ent_tem_type='$type'";
    $emailresult = sqlQuery($emailsql);
    if ($emailresult) {
        $email_entid = $emailresult['ent_id'];
        $email_patient_subject = $emailresult['ent_patient_subject'];
        $email_provider_subject = $emailresult['ent_provider_subject'];
        $email_patient_message = $emailresult['ent_patient_message'];
        $email_provider_message = $emailresult['ent_provider_message'];
    }
    // fetch SMS Template data from table
    /*$smssql = "SELECT snt_id,snt_patient_message,snt_patient_message_draft,snt_patient_draft_status FROM sms_notification_template WHERE snt_tem_type='$type'";
    $smsresult = sqlQuery($smssql);
    if ($smsresult) {
        $sms_sntid = $smsresult['snt_id'];
        $sms_patient_message = $smsresult['snt_patient_message'];
        $sms_patient_message_draft = $smsresult['snt_patient_message_draft'];
        $sms_patient_draft_status = $smsresult['snt_patient_draft_status'];
    }*/
}
?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <link rel="stylesheet" href="batchcom.css" type="text/css">
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/calendar.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/custom_template/fullckeditor/ckeditor.js"></script>
    </head>
    <body class="body_top">
        <span class="title"><?php include_once("followup_template_navigation.php"); ?></span>
        <span class="title"><?php xl('Treatment started and in progress with no patient appointment', 'e') ?></span>
        <br><br>       
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form name="select_form" method="post" action="">
            <input type="hidden" name="type" value="<?php echo $type; ?>">
            <input type="hidden" name="email_entid" value="<?php echo $email_entid; ?>">           
            <div class="text">
                <div class="main_box">
                    <?php
                    if ($form_err) {
                        echo ("The following errors occurred<br>$form_err<br><br>");
                    }
                    if ($sql_msg) {
                        echo ("$sql_msg<br><br>");
                    }
                    ?>
                    <?php xl('Email Subject', 'e') ?> :
                    <input type="text" name="ent_patient_subject" size="100"  value="<?php echo $email_patient_subject ?>">
                    <br>
                    <br>
                    <?php xl('Template: Email Dynamic Text, Usable Tag: ***FACILITY_PHONE***, ***MESSAGE_CONTENTS***', 'e') ?>
                    <br>
                    <textarea name="ent_patient_message" id="ent_patient_message" rows="10" cols="10"><?php echo $email_patient_message ?></textarea>
                    <script type="text/javascript">
                        CKEDITOR.replace('ent_patient_message');
                    </script>
                </div>
                <div class="text">
                    <div class="main_box">                    
                        
                        <input type="submit" name="form_action" value="Save">
                    </div>
                </div>
            </div>
         </form>
    </body>
</html>
