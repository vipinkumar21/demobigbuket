<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");

$alertmsg = '';
$res = sqlStatement("select * from collaterals where c_id={$_GET["id"]}");
for ($iter = 0;$row = sqlFetchArray($res);$iter++)
                $result[$iter] = $row;
$iter = $result[0];

///
if (isset($_POST["mode"])) {
  	echo '
<script type="text/javascript">
<!--
parent.$.fn.fancybox.close();
//-->
</script>

	';
}
///
?>

<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>
<script src="checkpwd_validation.js" type="text/javascript"></script>



<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
	if (document.forms[0].c_name.value.length>0 && document.forms[0].c_ct_id.value.length>0 && (document.getElementById('file_name').value.length >0 || document.getElementById('c_filename').value.length >0)) {
		top.restoreSession();		
		if(document.getElementById('file_name').value.length >0 && !validateFileExtension(document.getElementById('file_name').value,file_name)){
			document.getElementById('file_name').style.backgroundColor="red";
			alert("<?php xl('File Type Missing: Please upload PDF file','e');?>");
			document.getElementById('file_name').focus();
			return false;
		}
		document.forms[0].submit();
	} else {
		if (document.forms[0].c_name.value.length<=0)
		{
			document.forms[0].c_name.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please enter the Collateral Name','e');?>");
			document.forms[0].c_name.focus();
			return false;
		}
		if (document.forms[0].c_ct_id.value =="")
		{
			document.forms[0].c_ct_id.style.backgroundColor="red";
			alert("<?php echo xl('Required field missing: Please choose collateral type'); ?>");
			document.forms[0].c_ct_id.focus();
			return false;
		}
		if(trimAll(document.getElementById('file_name').value) == ""){
			document.getElementById('file_name').style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please browse collateral file','e');?>");
			document.getElementById('file_name').focus();
			return false;
		}	
		
	}
}
function validateFileExtension(fld,FieldName) {
   if(!/(\.pdf)$/i.test(fld)) 
	 {
	   return false;
	  }
	return true;
}

</script>

</head>
<body id="addcollateralbody" class="body_top">


<h3 class="emrh3"><?php xl('Edit Collateral','e'); ?></h3>
<div class="emrwrapper"> 
<form name='new_user' method='POST' enctype="multipart/form-data" target="_parent" action="list.php" onsubmit='return top.restoreSession()'>
	<table id="editcollateraltable" border='0' cellpadding='7' cellspacing='0' width="100%" class='emrtable nobrdr'>
		<tr>
			<td>
				<span class="text"><?php xl('Col Name','e'); ?>: </span>
			</td>
			<td style="border-right:1px solid #333 !important;">
				<input type='entry' name='c_name' style="width:120px;" value="<?php echo $iter['c_name'];?>">
				<span class="mandatory">&nbsp;*</span>
			</td>
			<td>
				<span class="text"><?php xl('Col Type','e'); ?>: </span>
			</td>
			<td>
				<select style="width:120px;" name="c_ct_id">
					<?php
					$ctres = sqlStatement("select ct_id, ct_name from collaterals_type group by ct_name");
					$result2 = array();
					for ($ctiter = 0;$row = sqlFetchArray($ctres);$ctiter++)
					$result2[$ctiter] = $row;
					foreach ($result2 as $ctiter) {?>
					<option value='<?php echo $ctiter["ct_id"];?>' <?php if ($iter['c_ct_id'] == $ctiter['ct_id']) echo "selected"; ?>><?php echo $ctiter["ct_name"];?></option>
					<?php 
					}
					?>
				</select>
				<span class="mandatory">&nbsp;*</span>
			</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #333 !important;">
				<span class="text"><?php xl('File','e'); ?>: </span>
			</td>
			<td style="border-right:1px solid #333 !important; border-top:1px solid #333 !important;">
				<input type='file' name='file_name' id='file_name' style="width:175px;">
				<span class="mandatory">&nbsp;*</span>
				<input type="hidden" name="c_filename" id="c_filename" value="<?php echo $iter['c_filename'];?>">
				<br>
				<span class="text"><?php echo $iter['c_filename'];?></span>
			</td>
			<td style="border-top:1px solid #333 !important;">
				<span class="text"><?php xl('Description','e'); ?>: </span>
			</td>
			<td style="border-top:1px solid #333 !important;">
				<textarea name="c_desc" style="width:120px;" cols="27" rows="4" wrap="auto"><?php echo $iter['c_desc'];?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan='4' style="border-top:1px solid #333 !important;">
				<div class="subsave" style="text-align:center;margin-top:20px;">

				<a class="btn btn-warning btn-sm" name='form_save' id='form_save' href='#' onclick="return submitform()">
				<span><?php xl('Save','e');?></span></a>

				<a class="btn btn-warning btn-sm" id='cancel' href='#'>
				<span class='css_button_span large_button_span'><?php xl('Cancel','e');?></span>
				</a>

				</div> 
			</td>
		</tr>
	</table>
	<input type='hidden' name='mode' value='update_collateral'>
	<INPUT TYPE="HIDDEN" NAME="id" VALUE="<?php echo $_GET["id"]; ?>">
</form>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });

});
</script>

</div> 
</body>
</html>
