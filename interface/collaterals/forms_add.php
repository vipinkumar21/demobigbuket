<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");

$alertmsg = '';

?>
<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script src="checkpwd_validation.js" type="text/javascript"></script>

<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
	if (document.forms[0].c_name.value.length>0 && document.forms[0].c_ct_id.value.length>0 && document.getElementById('file_name').value.length >0) {
		top.restoreSession();		
		if(!validateFileExtension(document.getElementById('file_name').value,file_name)){
			document.getElementById('file_name').style.backgroundColor="red";
			alert("<?php xl('File Type Missing: Please upload PDF file','e');?>");
			document.getElementById('file_name').focus();
			return false;
		}
		document.forms[0].submit();
	} else {
		if (document.forms[0].c_name.value.length<=0)
		{
			document.forms[0].c_name.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please enter the Collateral Name','e');?>");
			document.forms[0].c_name.focus();
			return false;
		}
		if (document.forms[0].c_ct_id.value =="")
		{
			document.forms[0].c_ct_id.style.backgroundColor="red";
			alert("<?php echo xl('Required field missing: Please choose collateral type'); ?>");
			document.forms[0].c_ct_id.focus();
			return false;
		}
		if(trimAll(document.getElementById('file_name').value) == ""){
			document.getElementById('file_name').style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please browse collateral file','e');?>");
			document.getElementById('file_name').focus();
			return false;
		}	
		
	}
}
function validateFileExtension(fld,FieldName) {
   if(!/(\.pdf)$/i.test(fld)) 
	 {
	   return false;
	  }
	return true;
}

</script>

</head>
<body class="body_top">


<h3 class="emrh3"><?php xl('Add Collateral','e'); ?></h3>
<div class="emrwrapper" > 
<form name='new_user' method='post' enctype="multipart/form-data" target="_parent" action="list.php" onsubmit='return top.restoreSession()'>
	<table border='0' cellpadding='11' cellspacing='0' width='100%' class='emrtable nobrdr'>
		<tr>
			<td>
				<span class="text"><?php xl('Col Name','e'); ?>: </span>
			</td>
			<td style="border-right:1px solid #333 !important;">
				<input type='entry' name='c_name' style="width:175px;" maxlength='20'>
				<span class="mandatory">&nbsp;*</span>
			</td>
			<td>
				<span class="text"><?php xl('Col Type','e'); ?>: </span>
			</td>
			<td>
				<select style="width:175px" name="c_ct_id">
					<?php
					$res = sqlStatement("select ct_id, ct_name from collaterals_type group by ct_name");
					$result2 = array();
					for ($iter = 0;$row = sqlFetchArray($res);$iter++)
					  $result2[$iter] = $row;
					foreach ($result2 as $iter) {
					  print "<option value='".$iter{"ct_id"}."'>" . $iter{"ct_name"} . "</option>\n";
					}
					?>
				</select>
				<span class="mandatory">&nbsp;*</span>
			</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #333 !important;">
				<span class="text"><?php xl('File','e'); ?>: </span>
			</td>
			<td style="border-right:1px solid #333 !important; border-top:1px solid #333 !important;">
				<input type='file' name='file_name' id='file_name' style="width:175px;">
				<span class="mandatory">&nbsp;*</span>
			</td>
			<td style="border-top:1px solid #333 !important;">
				<span class="text"><?php xl('Description','e'); ?>: </span>
			</td>
			<td style="border-top:1px solid #333 !important;">
				<textarea name="c_desc" style="width:175px;" cols="27" rows="4" wrap="auto"></textarea>
			</td>
	  	</tr>
		<tr>
			<td colspan="4" style="border-top:1px solid #333 !important;">
				<div class="subsave" style="text-align:center;margin:0px;"> 
					<a class="btn btn-warning btn-sm" name='form_save' id='form_save' href='#' onclick="return submitform()">
					<span><?php xl('Save','e');?></span></a>
					<a class="btn btn-warning btn-sm" id='cancel' href='#'>
					<span class='css_button_span large_button_span'><?php xl('Cancel','e');?></span>
					</a>
				</div> 
			</td>
		</tr>
	</table>
	<input type='hidden' name='mode' value='new_collateral'>
	<input type="hidden" name="newauthPass">
</form>

<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });

});
</script>
<table>

</table>
</div> <!-- emrwrapper closed --> 
</body>
</html>
