<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/auth.inc");
require_once("$srcdir/formdata.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");

$alertmsg = '';
$bg_msg = '';
$set_active_msg = 0;

/* To refresh and save variables in mail frame  - Arb */
if (isset($_POST["mode"])) {
    if ($_POST["mode"] == "update_collateral") {
        sqlStatement("delete from treatment_collaterals where tc_treatment_id = '" . $_POST["id"] . "'");
        $ctres = sqlStatement("select ct_id, ct_name from collaterals_type group by ct_name");
        while ($erow = sqlFetchArray($ctres)) {
            $collateralTypeName = $erow['ct_name'] . '_collaterals';
            for ($typeCount = 0; $typeCount < count($_POST[$collateralTypeName]); $typeCount++) {
                idSqlStatement("insert into treatment_collaterals set " .
                        "tc_treatment_id = '" . $_POST["id"] .
                        "', tc_c_id = '" . $_POST[$collateralTypeName][$typeCount] .
                        "'");
            }
        }
        header('Location: map_treatment.php');
        exit;
    }
}
if (isset($_GET["mode"])) {
    if ($_GET["mode"] == "delete") {
        $res = sqlStatement("select distinct tc_c_id from treatment_collaterals where tc_c_id = '" .
                $_GET["id"] . "'");
        $result = array();
        for ($iter = 0; $row = sqlFetchArray($res); $iter++)
            $result[$iter] = $row;
        if (count($result) > 0) {
            $alertmsg .= "You must remove maped treatments before " .
                    "removing them from this collateral. ";
        } else {
            $res = sqlStatement("select * from collaterals where c_id = '" . $_GET["id"] . "'");
            $row = sqlFetchArray($res);
            $uploadedDir = $_SERVER['DOCUMENT_ROOT'] . $web_root . "/sites/" . $_SESSION['site_id'] . "/collaterals/";
            if (!empty($row['c_filename']) && file_exists($uploadedDir . $row['c_filename'])) {
                unlink($uploadedDir . $row['c_filename']);
            }
            sqlStatement("delete from collaterals where c_id = '" . $_GET["id"] . "'");
        }
    }
}
?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <link type="text/css" href="../themes/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />

        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/commonScript.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
    </head>

    <body id="maptreatmentbody" class="body_top">

        <!-- Required for the popup date selectors -->
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <h3 class="emrh3"><?php xl('Map Collateral With Treatment', 'e'); ?></h3>
                <!--a href="forms_add.php" class="iframe-md-2 css_button"><span><?php xl('Map Collateral with treatment', 'e'); ?></span></a-->
        <form class='emrform topnopad' name='theform' method='post' action='map_treatment.php' id='theform'>
            <table style="margin-top:10px; margin-bottom: 10px;">
                <tr>
                    <td width="10%"><?php xl('Treatment', 'e'); ?>:</td>
                    <td width="20%"><input class='emrdate' type="text" name="SearchTreatment" value="<?php echo $_REQUEST['SearchTreatment']; ?>" placeholder="Search Treatment" style='width:100%; height:100%'></td>

                    <td width="10%"><?php xl('Collateral', 'e'); ?>:</td>
                    <td width="20%"><input class='emrdate' type="text" name="SearchCollateral" value="<?php echo $_REQUEST['SearchCollateral']; ?>" placeholder="Search Collateral" style='width:100%; height:100%'></td>
                    <td><a href='#' class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value", "true");
                            $("#theform").submit();'>
                            <span>
                                <?php xl('Submit', 'e'); ?>
                            </span>
                        </a>
                    </td>
                </tr>
            </table>
            <?php
            $treatment = '';
            $collateral = '';

            if (true || $_POST['form_refresh'] || $_GET['SearchTreatment'] || $_GET['SearchCollateral']) {
                if ($_POST['SearchTreatment'] || $_POST['SearchCollateral']) {
                    $treatment = $_POST['SearchTreatment'];
                    $collateral = $_POST['SearchCollateral'];
                } else {
                    $treatment = $_GET['SearchTreatment'];
                    $collateral = $_GET['SearchCollateral'];
                }

                $eres = "SELECT  tm.id,tm.trname, tg.title,tm.code,GROUP_CONCAT(col.c_name SEPARATOR ' , ') AS cname
                            FROM `treatment_master` AS tm 
                            LEFT JOIN `treatment_collaterals` AS tc ON tc.tc_treatment_id=tm.code
                            LEFT JOIN `treatment_groups` AS tg ON tg.id=tm.category_id 
                            LEFT JOIN `collaterals` AS col ON col.c_id=tc.tc_c_id ";

                if (!empty($treatment) && !empty($collateral)) {
                    $condition = " WHERE tm.trname LIKE '%" . $treatment . "%' AND col.c_name LIKE '%" . $collateral . "%' GROUP BY tm.code ";
                    $eres.=$condition;
                } else if (!empty($treatment) && empty($collateral)) {
                    $condition = " WHERE tm.trname LIKE '%" . $treatment . "%' GROUP BY tm.code";
                    $eres.=$condition;
                } else if (!empty($collateral) && empty($treatment)) {
                    $condition = " WHERE col.c_name LIKE '%" . $collateral . "%' GROUP BY tm.code ";
                    $eres.=$condition;
                } else {

                    $condition = "GROUP BY tm.code";
                    $eres.=$condition;
                    //echo $eres;exit();
                }
            }
            ?>                 
            <table  id="collateralmaptable" style="border-collapse: collapse !important;width:100%;" class="emrtable icontable">
                <thead>
                    <tr style="color:white!important;">
                        <td style="color:white!important;" width="35%">
                            <?php xl('Treatment Name', 'e'); ?>
                        </td>
                        <td style="color:white!important;" width="10%">
                            <?php xl('Category', 'e'); ?>
                        </td>
                        <td style="color:white!important;" width="10%">
                            <?php xl('Code', 'e'); ?>
                        </td>
                        <td style="color:white!important;" width="30%">
                            <?php xl('Mapped Collaterals', 'e'); ?>
                        </td>		
                        <td style="color:white!important;" width="10%" class="newtextcenter">
                            <?php xl('Action', 'e'); ?>
                        </td>
                    </tr>
                </thead>
                <?php ?>
                <tbody>
                    <?php
                    $num_rows = sqlNumRows(sqlStatement($eres)); // total no. of rows
                    $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                    //$per_page = 2;
                    $page = $_GET["page"];

                    if (!$_GET["page"]) {
                        $page = 1;
                    }
                    $prev_page = $page - 1;
                    $next_page = $page + 1;
                    $page_start = (($per_page * $page) - $per_page);
                    if ($num_rows <= $per_page) {
                        $num_pages = 1;
                    } else if (($num_rows % $per_page) == 0) {
                        $num_pages = ($num_rows / $per_page);
                    } else {
                        $num_pages = ($num_rows / $per_page) + 1;
                        $num_pages = (int) $num_pages;
                    }
                    $eres .= " LIMIT $page_start , $per_page";
                    $eres = sqlStatement($eres);
                    while ($erow = sqlFetchArray($eres)) {
                        ?>
                        <tr class='text' style='border-bottom: 1px dashed;'>
                            <td class='detail'>
                                <?php echo $erow['trname']; ?>
                            </td>
                            <td class='detail'>
                                <?php
                                echo $erow['title']
                                //  $categoryName = sqlStatement("select tg.title from treatment_groups as tg where tg.id=" . $erow['category_id']);
                                // while ($categoryName = sqlFetchArray($categoryName)) {
                                // echo $categoryName[title];
                                //}
                                ?>       
                            </td>
                            <td class='detail'>
                                <?php echo $erow['code']; ?>
                            </td>
                            <td class='detail'>
                                <?php
                                echo ucwords(strtolower($erow['cname']));
                                /* $mappedCollRes = sqlStatement("select c.c_name from treatment_collaterals as tc INNER JOIN collaterals as c ON c.c_id = tc.tc_c_id where tc_treatment_id=" . $erow['code']);
                                  while ($mappedCollRow = sqlFetchArray($mappedCollRes)) {
                                  echo $mappedCollRow[c_name] . '<br>';
                                  } */
                                ?>
                            </td>

                            <td class='detail newtextcenter' style='padding-right:0px;' >
                                <a title="edit" href='map_collaterals.php?id=<?php echo $erow['code']; ?>' class='iconanchor iframe-md-2' onclick='top.restoreSession()'><span class="glyphicon glyphicon-pencil"></span></a> <br>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php // Pagination Displaying Section ?>
            <table>
                <tbody>
                    <tr>
                        <td class="text">
                            <?php if ($num_rows > 0) { ?>
                                <span class='paging'>
                                    <span class="pagingInfo">Total Records: <?php echo $num_rows; ?></span> Page :
                                    <?php
                                    if ($prev_page) {
                                        echo " <span class='prev' style='margin:0 4px 0 5px'><a href='$_SERVER[SCRIPT_NAME]?SearchTreatment=$treatmentValue&page=$prev_page'><< Prev</a></span> ";
                                    }

                                    if ($num_rows > 0) {
                                        for ($i = 1; $i <= $num_pages; $i++) {
                                            if ($i != $page) {
                                                echo "<span><a href='$_SERVER[SCRIPT_NAME]?SearchTreatment=$treatmentValue&page=$i'>$i</a></span>";
                                            } else {
                                                echo "<span class='current'>$i</span>";
                                            }
                                        }
                                    }
                                    if ($page != $num_pages) {
                                        echo "<span class='next'><a href ='$_SERVER[SCRIPT_NAME]?SearchTreatment=$treatmentValue&page=$next_page'>Next >></a></span>";
                                    }
                                    ?>
                                </span>
                            <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- stuff for the popup calendar -->
        </form>
    </div> 
    <script language="JavaScript">
<?php
if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
}
?>
    </script>
    <br/>
</body>
</html>
