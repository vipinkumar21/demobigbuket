<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");

$alertmsg = '';
$res = sqlStatement("select * from treatment_collaterals where tc_treatment_id={$_GET["id"]}");
$result=array();
for ($iter = 0;$row = sqlFetchArray($res);$iter++)
                $result[$iter] = $row;
//$iter = $result[0];

///
if (isset($_POST["mode"])) {
  	echo '
<script type="text/javascript">
<!--
parent.$.fn.fancybox.close();
//-->
</script>

	';
}
///
function checkMappedColl($collID, $allMappedId){
	$returnFlag = 0;
	foreach($allMappedId as $mappedId){
		if($mappedId['tc_c_id'] == $collID){
			$returnFlag = 1;
		}
	}
	return $returnFlag;
}
?>

<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>
<script src="checkpwd_validation.js" type="text/javascript"></script>

<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
	if (document.forms[0].id.value.length>0) {
		top.restoreSession();		
		document.forms[0].submit();
	} else {
		if (document.forms[0].id.value.length<=0)
		{
			alert("<?php xl('Required field missing: Please select treatment after that map','e');?>");
			return false;
		}
		
		
	}
}
function validateFileExtension(fld,FieldName) {
   if(!/(\.pdf|\.doc|\.docs)$/i.test(fld)) 
	 {
	   return false;
	  }
	return true;
}

</script>

</head>
<body id="mapcollateralbody" class="body_top">


<h3 class="emrh3"><?php xl('Map Collaterals With Treatment','e'); ?></h3>
<div class="emrwrapper" >
<form name='new_user' method='POST' target="_parent" action="map_treatment.php" onsubmit='return top.restoreSession()'>
	<table border='0' cellpadding='8' cellspacing='0' class='emrtable nobrdr' width='100%'>
		<tr>
		<?php
			$ctres = sqlStatement("select ct_id, ct_name from collaterals_type group by ct_name");
			$result2 = array();
			for ($ctiter = 0;$row = sqlFetchArray($ctres);$ctiter++)
			  $result2[$ctiter] = $row;
			$count=1;
			foreach ($result2 as $ctiter) {
			?>

				<td <?php if($count/3 == 1){?>style="border-top:1px solid #333 !important;" <?php } ?>><span class="text"><?php xl(ucfirst($ctiter['ct_name']).' Collateral','e'); ?>: </span></td>
				 
				<td
					<?php if($count/3 == 1){?> style="border-top:1px solid #333 !important;" colspan="3" <?php } ?>
					<?php if($count/1 == 1){?> style="border-right:1px solid #333 !important;" <?php } ?>
				>

					<select name='<?php echo $ctiter['ct_name'];?>_collaterals[]' multiple>
						<?php
						
						$eres = sqlStatement("SELECT c.c_id, c.c_name FROM collaterals AS c WHERE c.c_ct_id = " .$ctiter['ct_id'] ." ORDER BY c.c_created_date DESC, c.c_id DESC");
						while ($erow = sqlFetchArray($eres)) { 
						?>
							<option value="<?php echo $erow['c_id'];?>" <?php if(checkMappedColl($erow['c_id'], $result)){ echo 'selected=selected'; }?>><?php echo $erow['c_name'];?></option>
						<?php
						}
						?>
					</select>
				</td>
		<?php
				if($count%2 == 0){
					echo "</tr><tr>";
				}
				$count++;
			}
		?>
		</tr>
		<tr>
			<td colspan='4' style="border-top:1px solid #333 !important;">
			<div class="subsave" style="text-align:center"> 

				<a class="btn btn-warning btn-sm" name='form_save' id='form_save' href='#' onclick="return submitform()">
					<span><?php xl('Save','e');?></span>
				</a>

				<a class="btn btn-warning btn-sm" id='cancel' href='#'>
					<span class='css_button_span large_button_span'><?php xl('Cancel','e');?></span>
				</a>

			</div> 
			</td>
		</tr>
	</table>
	<input type='hidden' name='mode' value='update_collateral'>
	<INPUT TYPE="HIDDEN" NAME="id" VALUE="<?php echo $_GET["id"]; ?>">
</form>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });

});
</script>



</div> 
</body>
</html>
