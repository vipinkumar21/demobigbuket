<?php 
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/auth.inc");
require_once("$srcdir/formdata.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");
require_once("$srcdir/S3.php");
$alertmsg = '';
$bg_msg = '';
$set_active_msg=0;

/* To refresh and save variables in mail frame  - Arb*/
if (isset($_POST["mode"])) {
	if ($_POST["mode"] == "new_collateral") {
	//print_r($_FILES);
	//die();
		$doit = true;
		$c_filename = '';
		$updir = $_SERVER['DOCUMENT_ROOT'].$web_root."/sites/" . $_SESSION['site_id'] . "/collaterals/"; 
	$max_size = 2000;         // sets maximum file size allowed (in KB)
	// file types allowed
	$allowtype = array('pdf', 'doc', 'docs');
	if (isset($_FILES['file_name'])) {
	  // check for errors
		if ($_FILES['file_name']['error'] > 0) {
			$doit = false;
		}
		else {
		// get the name, size (in kb) and type (the extension) of the file
			$fname = $_FILES['file_name']['name'];
			$fsize = $_FILES['file_name']['size'] / 1024;
			$ftype = end(explode('.', strtolower($fname)));
			$newFileName = time().'_'.$fname;
		//check to see if the keys have been set
			if(empty($GLOBALS['s3_access_key_id']) && empty($GLOBALS['s3_secret_access_key']) && empty($GLOBALS['collaterals_s3_full_url']) && empty($GLOBALS['collaterals_s3_bucket_name'])){
				$doit = false;
			}else {		
				$s3 = new S3($GLOBALS['s3_access_key_id'], $GLOBALS['s3_secret_access_key']);
			// checks if the file already exists
			/*if (file_exists($updir. $newFileName)) {
			  $doit = false;
			}
			else {*/
			  // if the file not exists, check its type (by extension) and size
				if (in_array($ftype, $allowtype)) {
				// check the size
					if ($fsize <= $max_size) {
						if($s3->putObject($s3->inputResource(fopen($_FILES['file_name']['tmp_name'], 'rb'), filesize($_FILES['file_name']['tmp_name'])), $GLOBALS['collaterals_s3_bucket_name'], $newFileName, S3::ACL_PUBLIC_READ, array(), array("Content-Type" => $_FILES['file_name']['type'])))
						{
						//return true as file was uploaded
							$c_filename = $newFileName;
						}
						else
						{
						//return false as there was a problem
							$doit = false;
						}
				  // uses  function to copy the file from temporary folder to $updir
				  /*if (!move_uploaded_file ($_FILES['file_name']['tmp_name'], $updir. $newFileName)) {
					$doit = false;
				  }
				  else {
					$c_filename = $newFileName;
					//echo $fname. ' ('. $fsize. ' kb) was successfully uploaded';
				}*/
			}
			else {
				$doit = false;
			}
		}
		else {
			$doit = false;
		}
			//}
	}
}
}else{
	$doit = false;
}

if ($doit == true) {

	$prov_id = idSqlStatement("insert into collaterals set " .
		"c_ct_id = '"         . trim(formData('c_ct_id'       )) .
		"', c_name = '"      . trim(formData('c_name'         )) .
		"', c_filename = '"         . $c_filename .
		"', file_url = '"         . $GLOBALS['collaterals_s3_full_url'].$c_filename .
		"', c_desc = '"         . trim(formData('c_desc'        )) .
		"', c_created_date = '" . date('Y-m-d') .
		"'");
} else {
	echo $alertmsg = 'There is some problem occurred in collateral save. Please try again';
	  //die();
}
header('Location: list.php');
exit;
}
if ($_POST["mode"] == "update_collateral") {
	if (isset($_FILES['file_name'])) {
		$doit = true;
		//$c_filename = '';
		$updir = $_SERVER['DOCUMENT_ROOT'].$web_root."/sites/" . $_SESSION['site_id'] . "/collaterals/"; 
		$max_size = 2000;// sets maximum file size allowed (in KB)
		  // check for errors
		$allowtype = array('pdf', 'doc', 'docs');
		if ($_FILES['file_name']['error'] > 0) {
			$doit = false;
		}
		else {
			// get the name, size (in kb) and type (the extension) of the file
			$fname = $_FILES['file_name']['name'];
			$fsize = $_FILES['file_name']['size'] / 1024;
			$ftype = end(explode('.', strtolower($fname)));
			$newFileName = time().'_'.$fname;
			// checks if the file already exists
			/*if (file_exists($updir. $newFileName)) {
			  $doit = false;
			}
			else {*/
			  // if the file not exists, check its type (by extension) and size
				if (in_array($ftype, $allowtype)) {
				// check the size
					if ($fsize <= $max_size) {
						if(empty($GLOBALS['s3_access_key_id']) && empty($GLOBALS['s3_secret_access_key']) && empty($GLOBALS['collaterals_s3_full_url']) && empty($GLOBALS['collaterals_s3_bucket_name'])){
							$doit = false;
						}else {
							$s3 = new S3($GLOBALS['s3_access_key_id'], $GLOBALS['s3_secret_access_key']);
						// uses  function to copy the file from temporary folder to S3 server.
							if($s3->putObject($s3->inputResource(fopen($_FILES['file_name']['tmp_name'], 'rb'), filesize($_FILES['file_name']['tmp_name'])), $GLOBALS['collaterals_s3_bucket_name'], $newFileName, S3::ACL_PUBLIC_READ, array(), array("Content-Type" => $_FILES['file_name']['type'])))
							{
							//return true as file was uploaded
								$c_filename = $newFileName;
								sqlStatement("update collaterals set c_filename='$newFileName', file_url = '". $GLOBALS['collaterals_s3_full_url'].$c_filename ."' where c_id={$_POST["id"]}");
							}
						}
					}

				}

			//}
			}
		}
		if ($_POST["c_name"]) {
		// $tqvar = addslashes(trim($_GET["username"]));
			$tqvar = trim(formData('c_name'));
			sqlStatement("update collaterals set c_name='$tqvar' where c_id={$_POST["id"]}");
		}
		if ($_POST["c_ct_id"]) {
			$tqvar = formData('c_ct_id');
			sqlStatement("update collaterals set c_ct_id='$tqvar' where c_id={$_POST["id"]}");
		}
		if ($_POST["c_desc"]) {
			$tqvar = formData('c_desc');
			sqlStatement("update collaterals set c_desc='$tqvar' where c_id={$_POST["id"]}");
		}
		header('Location: list.php');
		exit;
	}
}
if (isset($_GET["mode"])) {
	if ($_GET["mode"] == "delete") {
		$res = sqlStatement("select distinct tc_c_id from treatment_collaterals where tc_c_id = '" .
			$_GET["id"] . "'");
		$result = array(); 
		for ($iter = 0; $row = sqlFetchArray($res); $iter++)
			$result[$iter] = $row;    
		if (count($result) > 0) {
			$alertmsg .= "You must remove mapped treatments before " .
			"removing them from this collateral. ";
		} else {
			$res = sqlStatement("select `c_id`, `c_ct_id`, `c_name`, `c_filename`, `file_url`, `c_desc`, `c_created_date` from collaterals where c_id = '" .$_GET["id"] . "'");
			$row = sqlFetchArray($res);
		//$uploadedDir = $_SERVER['DOCUMENT_ROOT'].$web_root."/sites/" . $_SESSION['site_id'] . "/collaterals/"; 
			if(!empty($row['file_url'])){
			//unlink($uploadedDir.$row['c_filename']);
				if(!empty($GLOBALS['s3_access_key_id']) && !empty($GLOBALS['s3_secret_access_key']) && !empty($GLOBALS['collaterals_s3_full_url']) && !empty($GLOBALS['collaterals_s3_bucket_name'])){
					$s3 = new S3($GLOBALS['s3_access_key_id'], $GLOBALS['s3_secret_access_key']);
				// uses  function to copy the file from temporary folder to S3 server.				
					$s3->deleteObject($GLOBALS['collaterals_s3_bucket_name'], $row['file_url']);				
				}
			}
			sqlStatement("delete from collaterals where c_id = '" . $_GET["id"] . "'");
		}
	}
}
?>
<html>
<head>
	<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
	<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
	<link type="text/css" href="../themes/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

	<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/commonScript.js"></script>        
</head>

<body class="body_top">

	<!-- Required for the popup date selectors -->
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<!-- <div>
		<table>
			<tr >
				<td><b></b></td>
				<td>
				</td>

			</tr>
		</table>
	</div> -->
	<div id="report_results" class="panel panel-warning">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-3 boldtxt"><?php  xl('Collaterals','e'); ?></div>
				<div class="col-xs-9 text-right">
					<a href="forms_add.php" class="iframe-md btn btn-default btn-sm">
					<span><?php xl('Add Collateral','e'); ?></span></a>
				</div>
			</div>
		</div>

		<div class='emrform'> 
		<table class="emrtable" border='0' cellpadding='1' cellspacing='2' width='98%'>
			<thead>
				<tr>
					<th class="dehead" width="9%">
						<?php  xl('Col Name','e'); ?>
					</th>
					<th class="dehead" width="9%">
						<?php  xl('Col Type','e'); ?>
					</th>
					<th class='dehead' width="30%">
						<?php  xl('File Name','e'); ?>
					</th>
					<th class='dehead'>
						<?php  xl('Description','e'); ?>
					</th>
					<th class='dehead' width="10%">
						<?php  xl('Created Date','e'); ?>
					</th>
					<th class='dehead newtextcenter' width="15%">
						<?php  xl('Action','e'); ?>
					</th>
				</tr>
			</thead>
			<?php 
				$eres = sqlStatement("SELECT c.c_id,c.c_ct_id,c.c_name,c.c_filename,c.file_url,c.c_desc,c.c_created_date, ct.ct_id, ct.ct_name, ct.ct_desc " .
					"FROM collaterals AS c " .
					"JOIN collaterals_type AS ct ON " .
					"ct.ct_id = c.c_ct_id " .
					"ORDER BY c.c_created_date DESC, c.c_id DESC ");
			?>
			<tbody>
				<?php while ($erow = sqlFetchArray($eres)) { ?>
					<tr class='text' style='border-bottom: 1px dashed;'>
						<td class='detail'>
							<?php echo $erow['c_name']; ?>
						</td>
						<td class='detail'>
							<?php echo $erow['ct_name']; ?>
						</td>
						<td class='detail'>
							<?php echo $erow['c_filename']; ?>
						</td>
						<td class='detail'>
							<?php echo nl2br($erow['c_desc']); ?>
						</td>
						<td class='detail'>
							<?php echo $erow['c_created_date']; ?>
						</td>
						<td class='detail newtextcenter'>
							<a title="edit" href='forms_edit.php?id=<?php echo $erow['c_id']; ?>' class='iconanchor iframe-md' onclick='top.restoreSession()'><span class="glyphicon glyphicon-pencil"></span></a> 
							<?php 
							if(!empty($erow['file_url'])){
								?>
								<a title="download" href='<?php echo $erow['file_url'];?>' class='iconanchor' target="_blank" onclick='top.restoreSession()'><span class="glyphicon glyphicon glyphicon-arrow-down"></span></a> 
								<?php
							}else{
								?>				
								<a title="download" class='iconanchor' href='<?php echo 'http://'.$_SERVER['HTTP_HOST'].$web_root."/sites/" . $_SESSION['site_id'] . "/collaterals/".$erow['c_filename'];?>' target="_blank" onclick='top.restoreSession()'><span class="glyphicon glyphicon glyphicon-arrow-down"></span></a> 
								<?php 
							}
							?>
							<a title="delete"  class='iconanchor' onclick='return jConfirm("Are you sure to delete?","Confirm",function(result) { if(result){location.href="list.php?mode=delete&id=<?php echo $erow['c_id']; ?>";};});'><span class="glyphicon glyphicon-trash"></span>
						</td>
					</tr>
				<?php }?>
			</tbody>
		</table>
		</div>


	</div>
	<script language="JavaScript">
		<?php
		if ($alertmsg = trim($alertmsg)) {
			echo "jAlert('$alertmsg');\n";
		}
		?>
	</script>
	</body>

	</html>
