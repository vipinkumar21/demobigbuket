<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

 require_once("../../globals.php");
 require_once("$srcdir/patient.inc");
 require_once("$srcdir/formatting.inc.php");
  require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");
// Prepare a string for CSV export.
function qescape($str) {
  $str = str_replace('\\', '\\\\', $str);
  return str_replace('"', '\\"', $str);
}

?>
<html>
<head>
<?php html_header_show();?>
<title><?php xl('Patient List','e'); ?></title>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/textformat.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/jquery.searchabledropdown-1.0.8.min.js"></script>
<script language="JavaScript">
 var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
 
 function showClickedData(data, fieldId){
	$("#"+fieldId).val(data);
 }
 function allowPositiveNumber(e) {
	var charCode = (e.which) ? e.which : event.keyCode

	if (charCode > 31 && (charCode <= 46 || charCode > 57 )) {
		return false;
	}
	return true;
 
}
function showClickedData(data, fieldId){
	if(data.length > 0){
		$.ajax({
			url:"get_country_state.php?countryId="+data,
			success:function(result){
				$("#"+fieldId).html(result);
			}
		});
	}
 }
 
 function enableTextBox(fieldId){
	$("#"+fieldId).show();
 }
</script>
<script type="text/javascript">
function submitVersionFiveForm() {
	//alert(unanswered.length +'=>'+answered.length);
	if (document.forms['versionFive'].patient_file.value.length>0) {
		top.restoreSession();
		//alert('All fields data entered');
		//return false;
		document.forms['versionFive'].submit();
	} else {
		if (document.forms['versionFive'].patient_file.value.length<=0)
		{
			document.forms['versionFive'].patient_file.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please browse the patient file','e');?>");
			document.forms['versionFive'].patient_file.focus();
			return false;
		}		
	}
}
function submitVersionSixForm() {
	//alert(unanswered.length +'=>'+answered.length); form_facility
	if (document.forms['versionSix'].form_facility.value.length>0 && document.forms['versionSix'].patient_file.value.length>0) {
		top.restoreSession();
		//alert('All fields data entered');
		//return false;
		document.forms['versionSix'].submit();
	} else {
		if (document.forms['versionSix'].form_facility.value.length<=0)
		{
			document.forms['versionSix'].form_facility.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please choose the facility','e');?>");
			document.forms['versionSix'].form_facility.focus();
			return false;
		}
		if (document.forms['versionSix'].patient_file.value.length<=0)
		{
			document.forms['versionSix'].patient_file.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please browse the patient file','e');?>");
			document.forms['versionSix'].patient_file.focus();
			return false;
		}
		
	}
}
</script>
<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<style type="text/css">

/* specifically include & exclude from printing */
@media print {
    #report_parameters {
        visibility: hidden;
        display: none;
    }
    #report_parameters_daterange {
        visibility: visible;
        display: inline;
		margin-bottom: 10px;
    }
    #report_results table {
       margin-top: 0px;
    }
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
	#report_results {
		width: 100%;
	}
}

</style>

</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<span class='title'><?php xl('Import','e'); ?> - <?php xl('Patients Data','e'); ?></span>

<div id="report_parameters_daterange">
<?php echo date("d F Y", strtotime($form_from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($form_to_date)); ?>
</div>



<div id="report_parameters">
<table>
 <tr>
  <td width='60%'>
	<div style='float:left'>

	<table class='text'>
		<tr>
			<td class='label'>
			<?php xl('Version','e'); ?>:
			</td>
			<td>
				<select id="form_version" name="form_version">
					<option value="">-- Select Version --</option>
					<option value="5">Version 5</option>
					<option value="6">Version 6</option>
				</select>
			</td>			
		</tr>
	</table>

	</div>

  </td>
 </tr>
</table>
</div> <!-- end of parameters -->
<div id="importVersion5" style="background-color: #ECECEC; margin-top: 10px; display:none;">
<form name='versionFive' id='versionFive' method='post' enctype='multipart/form-data' action='import_version_five_patient_data.php'>
<input type='hidden' name='mode' id='mode' value='import_patient'/>
<table width="100%">
 <tr>
  <td width='60%'>
	<div style='float:left'>

	<table class='text'>
		<tr>
			<td class='label' width="25%">
			<?php xl('Patients','e'); ?>:
			</td>
			<td  width="75%">
				<input type="file" name="patient_file">
			</td>			
		</tr>
		<!--tr>
			<td class='label'>
			<?php xl('Appointments','e'); ?>:
			</td>
			<td>
				<input type="file" name="appointment_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Treatment','e'); ?>:
			</td>
			<td>
				<input type="file" name="treatment_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Income','e'); ?>:
			</td>
			<td>
				<input type="file" name="income_file">
			</td>			
		</tr>		
		<tr>
			<td class='label'>
			<?php xl('Bill','e'); ?>:
			</td>
			<td>
				<input type="file" name="bill_file">
			</td>			
		</tr-->
	</table>

	</div>

  </td>
  <td align='left' valign='middle' height="100%">
	<table style='border-left:1px solid; width:100%; height:100%' >
		<tr>
			<td>
				<div style='margin-left:15px;' id="mergePatientDataButton">
					<a href='#' class='css_button' onclick='return submitVersionFiveForm();'>
					<span>
						<?php xl('Upload&Import Patients','e'); ?>
					</span>
					</a>					
				</div>
			</td>
		</tr>
	</table>
  </td>
 </tr>
</table>
</form>
</div> <!-- end of parameters -->
<div id="importVersion6" style="background-color: #ECECEC; margin-top: 10px; display:none;">
<form name='versionSix' id='versionSix' method='post' enctype='multipart/form-data' action='import_version_six_patient_data.php'>
<input type='hidden' name='mode' id='mode' value='import_patient'/>

<table width="100%">
 <tr>
  <td width='60%'>
	<div style='float:left'>

	<table class='text'>
		<tr>
			<td class='label'>
			<?php xl('Facility','e'); ?>:
			</td>
			<td>
				<?php dropdown_facility(strip_escape_custom($facility), 'form_facility'); ?>
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Patients','e'); ?>:
			</td>
			<td>
				<input type="file" name="patient_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Appointments','e'); ?>:
			</td>
			<td>
				<input type="file" name="appointment_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Treatment','e'); ?>:
			</td>
			<td>
				<input type="file" name="treatment_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Invoice','e'); ?>:
			</td>
			<td>
				<input type="file" name="invoice_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Invoice Cancel','e'); ?>:
			</td>
			<td>
				<input type="file" name="invoice_cancel_file">
			</td>			
		</tr>
		<tr>
			<td class='label'>
			<?php xl('Payment','e'); ?>:
			</td>
			<td>
				<input type="file" name="payment_file">
			</td>			
		</tr>
	</table>

	</div>

  </td>
  <td align='left' valign='middle' height="100%">
	<table style='border-left:1px solid; width:100%; height:100%' >
		<tr>
			<td>
				<div style='margin-left:15px;' id="mergePatientDataButton">
					<a href='#' class='css_button' onclick='return submitVersionSixForm();'>
					<span>
						<?php xl('Upload&Import Patients','e'); ?>
					</span>
					</a>					
				</div>
			</td>
		</tr>
	</table>
  </td>
 </tr>
</table>
</form>
</div> <!-- end of parameters -->
<div id="report_results">

</div> <!-- end of results -->

</body>
</html>
<script>
	$( document ).ready(function() {
		$("#form_version").change(function(){
			var form_version = $("#form_version").val();
			if(form_version == 5){
				$("#importVersion5").show();
				$("#importVersion6").hide();
			}else if(form_version == 6){
				$("#importVersion6").show();
				$("#importVersion5").hide();
			}else {
				$("#importVersion5").hide();
				$("#importVersion6").hide();
			}
		});
	});
</script>