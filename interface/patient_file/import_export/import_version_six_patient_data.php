<?php
set_time_limit(0);
 // Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
 //
 // This program is import practo version-6 patient and related data.
require_once("../../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
require("$srcdir/spreadsheet_reader/php-excel-reader/excel_reader2.php");
require("$srcdir/spreadsheet_reader/SpreadsheetReader.php");
/*echo '<pre>';
print_r($_POST);
print_r($_FILES);
echo '</pre>';*/

$updir = $_SERVER['DOCUMENT_ROOT'].$web_root."/sites/" . $_SESSION['site_id'] . "/imported_files/"; 
if(isset($_POST['mode']) && $_POST['mode']=='import_patient' && isset($_POST['form_facility']) && !empty($_POST['form_facility'])){
	$facilityID = $_POST['form_facility'];
	$patientFileInfo = fileUpload($_FILES['patient_file'], $updir);
	/*echo '<pre>';
	print_r($patientFileInfo);
	echo '</pre>';
	die();*/
	if($patientFileInfo['status'] == true && !empty($patientFileInfo['url']) && file_exists($patientFileInfo['url'])) {
		$Filepath = $patientFileInfo['url'];
		$fileDatas = readUploadedFileData($Filepath);
		if(is_array($fileDatas) && !empty($fileDatas)){
			if(array_key_exists('column', $fileDatas)){
				if(count($fileDatas['column']) >= 16){
					addPatientData($fileDatas, $facilityID);
				}
			}
		}
		/*echo '<pre>';
		print_r($fileDatas);
		echo '</pre>';*/
	}
	$appointmentFileInfo = fileUpload($_FILES['appointment_file'], $updir);
	if($appointmentFileInfo['status'] == true && !empty($appointmentFileInfo['url']) && file_exists($appointmentFileInfo['url'])) {
		$appFilepath = $appointmentFileInfo['url'];
		$appfileDatas = readUploadedFileData($appFilepath);
		//print_r($appfileDatas);
		if(is_array($appfileDatas) && !empty($appfileDatas)){
			if(array_key_exists('column', $appfileDatas)){
				if(count($appfileDatas['column']) >= 8){
					//echo 'Appointment Data insertion In Progress.';
					addAppData($appfileDatas, $facilityID);
				}
			}
		}
	}
	
	$treatmentFileInfo = fileUpload($_FILES['treatment_file'], $updir);
	if($treatmentFileInfo['status'] == true && !empty($treatmentFileInfo['url']) && file_exists($treatmentFileInfo['url'])) {
		$trpFilepath = $treatmentFileInfo['url'];
		$trpfileDatas = readUploadedFileData($trpFilepath);
		//print_r($trpfileDatas);
		if(is_array($trpfileDatas) && !empty($trpfileDatas)){
			if(array_key_exists('column', $trpfileDatas)){
				if(count($trpfileDatas['column']) >= 8){
					//echo 'Heloooooooooooooooo<br>';
					addTrpData($trpfileDatas, $facilityID);
				}
			}
		}
	}
	//die();
	$invoiceFileInfo = fileUpload($_FILES['invoice_file'], $updir);
	if($invoiceFileInfo['status'] == true && !empty($invoiceFileInfo['url']) && file_exists($invoiceFileInfo['url'])) {
		$invoiceFilepath = $invoiceFileInfo['url'];
		$invoicefileDatas = readUploadedFileData($invoiceFilepath);
		if(is_array($invoicefileDatas) && !empty($invoicefileDatas)){
			if(array_key_exists('column', $invoicefileDatas)){
				if(count($invoicefileDatas['column']) >= 11){
					//echo 'Heloooooooooooooooo<br>';
					addInvoiceData($invoicefileDatas, $facilityID, 1);
				}
			}
		}
	}
	$cInFileInfo = fileUpload($_FILES['invoice_cancel_file'], $updir);
	if($cInFileInfo['status'] == true && !empty($cInFileInfo['url']) && file_exists($cInFileInfo['url'])) {
		$cInFilepath = $cInFileInfo['url'];
		$cInfileDatas = readUploadedFileData($cInFilepath);
		if(is_array($cInfileDatas) && !empty($cInfileDatas)){
			if(array_key_exists('column', $cInfileDatas)){
				if(count($cInfileDatas['column']) >= 11){
					//echo 'Heloooooooooooooooo<br>';
					addInvoiceData($cInfileDatas, $facilityID, 0);
				}
			}
		}
	}
	$paymentFileInfo = fileUpload($_FILES['payment_file'], $updir);
	if($paymentFileInfo['status'] == true && !empty($paymentFileInfo['url']) && file_exists($paymentFileInfo['url'])) {
		$paymentFilepath = $paymentFileInfo['url'];
		$paymentfileDatas = readUploadedFileData($paymentFilepath);
		if(is_array($paymentfileDatas) && !empty($paymentfileDatas)){
			if(array_key_exists('column', $paymentfileDatas)){
				if(count($paymentfileDatas['column']) >= 10){
					//echo 'Heloooooooooooooooo<br>';
					addPaymentData($paymentfileDatas, $facilityID);
				}
			}
		}		
	}
	?>
	<script>
		window.location.href='import.php';
	</script>
	<?php
}
function readUploadedFileData($Filepath){
	//echo $Filepath;
	$returnFileData = array();
	$columnNames=array();
	$rowData = array();
	$Spreadsheet = new SpreadsheetReader($Filepath);
	$Sheets = $Spreadsheet -> Sheets();
	foreach ($Sheets as $Index => $Name){
		$Spreadsheet -> ChangeSheet($Index);
		foreach ($Spreadsheet as $Key => $Row){
			if($Key == 0){
				if($Row){
					$columnNames=$Row;
				}else{
					$columnNames=$Row;
				}
			}else {
				if($Row){
					$rowData[] = $Row;
				}else{
					$rowData[] = $Row;
				}
			}
			
		}
	}
	$returnFileData['column'] = $columnNames;
	$returnFileData['data'] = $rowData;
	return $returnFileData;
}
function addPatientData($allData, $facilityID){
	$columns = $allData['column'];
	$rowDatas = $allData['data'];
	$finalDataRows = array();
	for($count = 0; $count < count($rowDatas); $count++){
		$pubpid = $rowDatas[$count][0];
		$fname = $rowDatas[$count][1];
		$phone_cell  = str_replace('\'+91', '', $rowDatas[$count][2]);
		if(!empty($rowDatas[$count][2])) {
			$hpaa_alowsms  = 'Yes';
		}else {
			$hpaa_alowsms  = 'No';
		}
		$phone_contact  = $rowDatas[$count][3];
		$email  = $rowDatas[$count][4];
		if(!empty($rowDatas[$count][4])) {
			$hpaa_alowemail  = 'Yes';
		}else {
			$hpaa_alowemail  = 'No';
		}
		$sex  = '';
		if($rowDatas[$count][6] == 'MALE'){
			$sex  = 'Male';
		}else if($rowDatas[$count][6] == 'FEMALE'){
			$sex  = 'Female';
		}
		$street  = $rowDatas[$count][7];
		$state  = $rowDatas[$count][8];
		$city  = $rowDatas[$count][9];
		$postal_code  = $rowDatas[$count][10];
		$country_code  = $rowDatas[$count][11];
		$DOB  = '';
		//echo "DOB Sheet Data =>".$rowDatas[$count][12].'<br>';
		if(!empty($rowDatas[$count][12])){
			//$dobDataArr = explode('/', $rowDatas[$count][12]);
			//$DOB  = $dobDataArr[2].'-'.$dobDataArr[0].'-'.$dobDataArr[1];
			$DOB  = date("Y-m-d", strtotime($rowDatas[$count][12]));
		}
		//echo "DOB =>".$DOB.'<br>';
		$ptntnts  = $rowDatas[$count][14];
		if($rowDatas[$count][15] != 'NONE') {
			$ptntnts  .= $rowDatas[$count][15];
		}
		$group  = $rowDatas[$count][16];
		if(checkPatientRecord($pubpid)){
			$pid = 1;
			sqlStatement("lock tables patient_data read");
			$result = sqlQuery("select max(pid)+1 as pid from patient_data");
			sqlStatement("unlock tables");
			if ($result['pid'] > 1) {
				$pid = $result['pid'];
			}			
			$patientId = sqlInsert("INSERT INTO patient_data ( " .
					"`pubpid`, `pid`, `fname`, `phone_cell`, `hipaa_allowsms`, `phone_contact`, `email`, `hipaa_allowemail`, `sex`, `street`, `state`, `city`, `postal_code`, `country_code`, `DOB`, `ptntnts`, `group` ".
					") VALUES ( " .
					"'" . add_escape_custom($pubpid) . "', " .
					"'" . add_escape_custom($pid) . "', " .
					"'" . add_escape_custom($fname)			. "', " .
					"'" . add_escape_custom($phone_cell)			. "', " .
					"'" . add_escape_custom($hpaa_alowsms)       . "', " .
					"'" . add_escape_custom($phone_contact) . "', " .
					"'" . add_escape_custom($email)			. "', " .
					"'" . add_escape_custom($hpaa_alowemail)			. "', " .
					"'" . add_escape_custom($sex)       . "', " .
					"'" . add_escape_custom($street) . "', " .
					"'" . add_escape_custom($state)			. "', " .
					"'" . add_escape_custom($city)			. "', " .
					"'" . add_escape_custom($postal_code)       . "', " .
					"'" . add_escape_custom($country_code) . "', " .
					"'" . add_escape_custom($DOB)			. "', " .
					"'" . add_escape_custom($ptntnts)			. "', " .
					"'" . add_escape_custom($group)       . "' " .
					")");
			addPatientMedicalHistory($patientId);
			$issue = sqlInsert("INSERT INTO patient_facilities ( " .
					"pf_pid, pf_fid ".
					") VALUES ( " .
					"'" . add_escape_custom($patientId) . "', " .
					"'" . add_escape_custom($facilityID)			. "' " .				
					")");
		}
	}
}
function addPatientMedicalHistory($pID){
	$checkTypeQuery = "SELECT * from checkup_type";
	$checkTypeRes = sqlStatement($checkTypeQuery);
	while ($checkTypeRow = sqlFetchArray($checkTypeRes)) {
		$checkConfigQuery = "SELECT * from checkup_config WHERE cc_ct_id=".$checkTypeRow['ct_id'];
		$checkConfigRes = sqlStatement($checkConfigQuery);
		while ($checkConfigRow = sqlFetchArray($checkConfigRes)) {
			$issue = sqlInsert("INSERT INTO checkup_data ( " .
				"cd_pid, cd_cc_id, cd_ct_id, cd_radio ".
				") VALUES ( " .
				"'" . add_escape_custom($pID) . "', " .
				"'" . add_escape_custom($checkTypeRow['ct_id'])			. "', " .
				"'" . add_escape_custom($checkConfigRow['cc_id'])			. "', " .
				"'" . add_escape_custom($checkConfigRow['cc_radio3'])       . "' " .
				")");
		}
	}
}
function checkPatientRecord($pubpid){
	$checkQuery = "SELECT pubpid from patient_data WHERE pubpid ='".$pubpid."'";
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		return false;
	}else {
		return true;
	}
}
function getExPatientId($pubpid){
	$checkQuery = "SELECT id from patient_data WHERE pubpid ='".$pubpid."'";
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		$res = sqlFetchArray($checkRes);
		return $res['id'];
	}
}
function addAppData($appfileDatas, $facilityID){
	/*echo '<pre>';
	print_r($appfileDatas);
	echo '</pre>';*/
	$columns = $appfileDatas['column'];
	$rowDatas = $appfileDatas['data'];
	$appItemsData = array();
	/*echo '<pre>';
	print_r($rowDatas);
	echo '</pre>';*/
	for($count = 0; $count < count($rowDatas); $count++){
		/*echo '<pre>';
		print_r($rowDatas[$count]);
		echo '</pre>';*/
		$appDateTime = $rowDatas[$count][0];
		$appDate = date("Y-m-d", strtotime($appDateTime));
		$appTime = date("H:i:s", strtotime($appDateTime));
		$appEndDate = date("Y-m-d", strtotime($appDateTime)+1800);
		$appEndTime = date("H:i:s", strtotime($appDateTime)+1800);
		$patientId = 0;
		if(!empty($rowDatas[$count][1])){
			if(!checkPatientRecord($rowDatas[$count][1])){
				$patientId = getExPatientId($rowDatas[$count][1]);
			}
		}
		$hometext  = $rowDatas[$count][6];
		$proID  = 0;
		if(!empty($rowDatas[$count][7])){
			$doctorId = getOperatorDetail($rowDatas[$count][7]);
			if(!empty($doctorId)) {
				$proID  = $doctorId;
			}
		}
		$deleted = 0;
		if($rowDatas[$count][8] == 'Cancelled'){
			$deleted = 1;
		}
		if($proID == 0){
			$proID  = getClinicHeadId($facilityID);
		}
		if($proID == 0){
			$proID  = 1;
		}
		$operatory = 1;
		$duration = 1800;
		$recurrspecs = array("event_repeat_freq" => "",
			"event_repeat_freq_type" => "",
			"event_repeat_on_num" => "1",
			"event_repeat_on_day" => "0",
			"event_repeat_on_freq" => "0",
			"exdate" => ""
		);
		$recurrspec = serialize($recurrspecs);

		$locationspecs = array("event_location" => "",
			"event_street1" => "",
			"event_street2" => "",
			"event_city" => "",
			"event_state" => "",
			"event_postal" => ""
		);
		$locationspec = serialize($locationspecs);
		if($patientId != 0) {
			$appItemsData[] = add_escape_custom($patientId).",'' ,'".
				add_escape_custom($hometext)."','".
				date('Y-m-d H:i:s')."','".
				add_escape_custom($appDate)."','".
				add_escape_custom($appTime)."','".
				add_escape_custom($appEndTime)."','-','0',".
				add_escape_custom($proID).",".
				add_escape_custom($facilityID).",".
				add_escape_custom($duration).",1,1,1,'".
				add_escape_custom($recurrspec)."','".
				add_escape_custom($locationspec)."',".
				add_escape_custom($deleted).",".
				add_escape_custom($operatory);
		}
	}
	/*echo '<pre>';
	print_r($appItemsData);
	echo '</pre>';*/
	insertAppData($appItemsData);
}
function insertAppData($appItemsData){
	if(!empty($appItemsData)) {
		$result = sqlStatement("INSERT INTO openemr_postcalendar_events (pc_pid, pc_title, pc_hometext , pc_time, pc_eventDate, pc_startTime, pc_endTime, pc_apptstatus, pc_catid, pc_aid, pc_facility, pc_duration , pc_informant, pc_eventstatus, pc_sharing, pc_recurrspec, pc_location, pc_delete, pc_operatory) VALUES (".implode('),(', $appItemsData).")");
		return $result;
	}else {
		return 0;
	}
}
function addTrpData($trpfileDatas, $facilityID){
	/*echo '<pre>';
	 print_r($trpfileDatas);
	echo '</pre>';*/
	$columns = $trpfileDatas['column'];
	$rowDatas = $trpfileDatas['data'];
	/*echo '<pre>';
	print_r($rowDatas);
	echo '</pre>';*/
	//$wdItemsData = array();
	for($count = 0; $count < count($rowDatas); $count++){
		$trpDate = date("Y-m-d H:i:s", strtotime($rowDatas[$count][0]));
		$patientId = 0;
		if(!empty($rowDatas[$count][1])){
			if(!checkPatientRecord($rowDatas[$count][1])){
				$patientId = getExPatientId($rowDatas[$count][1]);
			}
		}
		$trpName  = trim($rowDatas[$count][3]);
		$trpToothNum  = $rowDatas[$count][4];
		$units = 1;		
		$trpNotes  = $rowDatas[$count][5];
		$trpCost  = $rowDatas[$count][6];
		$trpFee = $trpCost;
		$trpDiscount  = $rowDatas[$count][7];
		$trpDiscountType  = '%';
		if($rowDatas[$count][8] == 'NUMBER'){
			$trpDiscountType  = 'Amt';
			$trpFee = $trpCost - $trpDiscount;
		}else {
			$trpFee = $trpCost - ($trpCost * ($trpDiscount/100));
		}
		$proID  = 1;
		if(!empty($rowDatas[$count][7])){
			$doctorId = getOperatorDetail($rowDatas[$count][7]);
			if(!empty($doctorId)) {
				$proID  = $doctorId;
			}
		}
		//echo $patientId;
		if($patientId != 0) {
			//echo "Hello";
			$trpDetail = getTreatmentDetail($trpName);
			$proID = getDoctorId($facilityID, $trpDate, $patientId);
			//print_r($trpDetail);
			if(!empty($trpDetail)) {
				if(!empty($trpToothNum)){
					$allToothArr = explode("|", $trpToothNum);
					$units = count($allToothArr);
				}else {
					if($trpCost > $trpDetail['pr_price']){
						$units = ceil($trpCost/$trpDetail['pr_price']);
					}
				}				
				$strQuery = "INSERT INTO billing (
							`provider_id`,
							`pid`, 
							`toothno`, 
							`code_type`, 
							`code`, 
							`code_text`, 
							`units`, 
							`fee`, 
							`discount`, 
							`discount_type`, 
							`treatmenttype`, 
							`remarks`, 
							`plangroup`, 
							`date`, 
							`work_status`, 
							`clinic_id`,
                                                        `bill_type`,
                                                        `unit_price`) VALUES (
								".add_escape_custom($proID).",
								".add_escape_custom($patientId).",
								'".add_escape_custom($trpToothNum)."',
								'".add_escape_custom($trpDetail['code_type'])."',
								'".add_escape_custom($trpDetail['code'])."',
								'".add_escape_custom($trpName)."',".$units.",
								".add_escape_custom($trpFee).",".
								add_escape_custom($trpDiscount).",'".
								add_escape_custom($trpDiscountType)."','".
								add_escape_custom('Urgent')."','".
								add_escape_custom($trpNotes)."',1,'".
								$trpDate."',0,".
								add_escape_custom($facilityID).",
                                                                '0',
                                                                ".add_escape_custom($trpDetail['pr_price'])."
							)";			
				$treatmentId = sqlInsert($strQuery);
								
			}
		}
	}
	/*echo '<pre>';
	print_r($trpItemsData);
	echo '</pre>';*/
	//insertWorkDoneData($wdItemsData);
}
function insertWorkDoneData($trpItemsData){
	if(!empty($trpItemsData)) {
		$result = sqlStatement("INSERT INTO work_log (`wl_tr_id`,`wl_created_date`,`wl_created_by`) VALUES (".implode('),(', $trpItemsData).")");
		//insert into `openemr_clove_product`.`work_log`(`wl_id`,`wl_tr_id`,`wl_remark`,`wl_created_by`,`wl_created_date`,`wl_is_modified`,`wl_is_deleted`) values ( NULL,'108','No Remarks','1',NULL,'0','0')
		return $result;
	}else {
		return 0;
	}
}
function addInvoiceData($invoicefileDatas, $facilityID, $deleted){
	$columns = $invoicefileDatas['column'];
	$rowDatas = $invoicefileDatas['data'];
	$repArr = array('th','st','rd','nd',',');
	$invoiceItems = array();
	$wdItemsData = array();
	$invoiceTrp = array();
	for($count = 0; $count < count($rowDatas); $count++){
		$invDate = date("Y-m-d H:i:s", strtotime(str_replace($repArr,'',$rowDatas[$count][0])));
		$invNumber = $rowDatas[$count][1];
		$patientId = 0;
		if(!empty($rowDatas[$count][2])){
			if(!checkPatientRecord($rowDatas[$count][2])){
				$patientId = getExPatientId($rowDatas[$count][2]);
			}
		}
		$proID = getDoctorId($facilityID, $invDate, $patientId);
		$trpName  = $rowDatas[$count][6];
		$invFullCost  = $rowDatas[$count][7];
		$invFullDiscounted  = $rowDatas[$count][8];
		$invFullInvoiced  = $rowDatas[$count][10];
		$invFullPaid  = $rowDatas[$count][11];
		if($patientId != 0) {			
			$trpId = getTreatmentPlanId($patientId, $trpName, $invDate, $facilityID, implode(',',$invoiceTrp), $invFullInvoiced);
			$invoiceTrp[] = $trpId;
			if(!empty($trpId) && $invFullCost != 0){
				$invId = checkInvoiceId($patientId, $invNumber);
				if($invId == 0){
					$strQuery = "INSERT INTO `invoice` (
						`inv_number`,
						`inv_pid`,
						`inv_clinic_id`,
						`inv_created_by` ,
						`inv_deletestate` ,
						`inv_created_date`
					) 
					VALUES (
						'" . add_escape_custom($invNumber) . "',
						'" . add_escape_custom($patientId) . "',
						'" . add_escape_custom($facilityID) . "',
						'" . add_escape_custom($proID) . "',
						'" . add_escape_custom($deleted) . "',
						'" . add_escape_custom($invDate) . "'
					)";
					$invId = sqlInsert($strQuery);
					
				}
				$invItemStatus = 0;
				if($deleted == 0){
					$invItemStatus = 1;
				}
				if($invFullInvoiced == $invFullPaid) {
					updateTrpStatus($trpId, $patientId, 2);
				}else {
					updateTrpStatus($trpId, $patientId, 1);
				}
				//$trpId = getTreatmentPlanId($patientId, $trpName);
				$invoiceItems[] = $invId.",".$trpId.",".$invItemStatus;	
				$wdItemsData[] = add_escape_custom($trpId).",'".add_escape_custom($invDate)."',".add_escape_custom($proID);
			}		
		}
	}
	insertInvoiceItems($invoiceItems);
	insertWorkDoneData($wdItemsData);
}
function insertInvoiceItems($invoiceItemsData){
	if(!empty($invoiceItemsData)) {
		$result = sqlStatement("INSERT INTO invoice_items (invit_inv_id, invit_tp_id,invit_deleted) VALUES (".implode('),(', $invoiceItemsData).")");
		return $result;
	}else {
		return 0;
	}
}
function updateTrpStatus($trpId, $patientId, $trpStatus){
	$result = sqlStatement("UPDATE billing SET work_status = ".$trpStatus." WHERE id = ".$trpId." AND pid = ".$patientId);
	return $result;
	
}
function addPaymentData($paymentfileDatas, $facilityID){
	$columns = $paymentfileDatas['column'];
	$rowDatas = $paymentfileDatas['data'];
	$repArr = array('th','st','rd','nd',',');
	$invRectItems = array();
	for($count = 0; $count < count($rowDatas); $count++){
		$rectDate = date("Y-m-d H:i:s", strtotime(str_replace($repArr,'',$rowDatas[$count][0])));
		$rectNumber = $rowDatas[$count][1];
		$patientId = 0;
		if(!empty($rowDatas[$count][2])){
			if(!checkPatientRecord($rowDatas[$count][2])){
				$patientId = getExPatientId($rowDatas[$count][2]);
			}
		}
		$proID  = getClinicHeadId($facilityID);
		if($proID == 0){
			$proID  = 1;
		}
		$amountPaid  = $rowDatas[$count][6];
		$rectType = 'Advance';
		if($rowDatas[$count][7] == 'No'){
			$rectType = 'Payment';
		}
		$rect_mode = $rowDatas[$count][8];
		$invNumber = $rowDatas[$count][9];
		$rect_remarks = $rowDatas[$count][10];
		if($patientId != 0) {
			$strQuery = "INSERT INTO `reciept` (
				`rect_number`,
				`rect_amount`,
				`rect_pid`,
				`rect_mode`,
				`rect_type`,
				`rect_remarks`,
				`rect_clinic_id`,
				`rect_created_by` ,
				`rect_deletestate` ,
				`rect_created_date`
			) 
			VALUES (
				'" . add_escape_custom($rectNumber) . "',
				'" . add_escape_custom($amountPaid) . "',
				'" . add_escape_custom($patientId) . "',
				'" . add_escape_custom($rect_mode) . "',
				'" . add_escape_custom($rectType) . "',
				'" . add_escape_custom($rect_remarks) . "',
				'" . add_escape_custom($facilityID) . "',
				'" . add_escape_custom($proID) . "',
				'1',
				'" . add_escape_custom($rectDate) . "'
			)";
			$rectId = sqlInsert($strQuery);
			$invId = checkInvoiceId($patientId, $invNumber);
			$invRectItems[] = $invId.",".$rectId;			
		}
	}
	insertInvRectItems($invRectItems);
}
function insertInvRectItems($invoiceItemsData){
	if(!empty($invoiceItemsData)) {
		$result = sqlStatement("INSERT INTO invoice_reciepts (invrect_inv_id, invrect_rect_id) VALUES (".implode('),(', $invoiceItemsData).")");
		return $result;
	}else {
		return 0;
	}
}
function getTreatmentPlanId($patientId, $trName, $invDate, $facilityID, $notTrpId, $trFee){
	$checkQuery = "SELECT id FROM billing WHERE pid='".$patientId."' AND clinic_id = ".$facilityID." AND DATE(date) <= '".$invDate."' AND (code_text='".add_escape_custom($trName)."' OR code_text='".$trName."') AND fee = '".$trFee."'";
	if(!empty($notTrpId)) {
		$checkQuery .=" AND id NOT IN (".$notTrpId.")";
	}
        $checkQuery .=" ORDER BY `date` DESC";
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		$res = sqlFetchArray($checkRes);
		return $res['id'];
	}else {
		return 0;
	}
}
function checkInvoiceId($patientId, $invNumber){
	$checkQuery = "SELECT inv_id FROM invoice WHERE inv_pid='".$patientId."' AND inv_number='".$invNumber."'";
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		$res = sqlFetchArray($checkRes);
		return $res['inv_id'];
	}else {
		return 0;
	}
}
function getOperatorDetail($name){
	//SELECT id, CONCAT_WS(' ', fname, lname) AS user_name FROM users HAVING user_name='Dev Acc'
	$checkQuery = "SELECT id, CONCAT_WS(' ', fname, lname) AS user_name FROM users HAVING user_name='".$name."'";
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		$res = sqlFetchArray($checkRes);
		return $res['id'];
	}
}
function getTreatmentDetail($trpName){	
	$eres = sqlStatement("SELECT c.id, c.code_type, c.code, c.code_text, c.modifier, c.superbill, ct.ct_key as code_type_name, p.pr_price
	FROM codes AS c
	LEFT OUTER JOIN `code_types` as ct ON c.code_type = ct.ct_id 
	INNER JOIN prices AS p ON p.pr_id = c.id
	WHERE ct.ct_external = '0'
	AND ct.ct_active = '1' 
	AND c.code_text = '".$trpName."' 
	AND p.pr_level = 'standard' 
	ORDER BY c.id DESC limit 0, 1");
	$checkNumRows = sqlNumRows($eres);
	if($checkNumRows){
		$res = sqlFetchArray($eres);
		return $res;
	}else{
		return array();
	}
}
function getDoctorId($facilityID, $opeDate, $pid){
	$checkQuery = "SELECT pc_aid FROM openemr_postcalendar_events WHERE pc_pid = ".$pid." AND pc_eventDate ='".$opeDate."' AND pc_facility=".$facilityID;
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		$res = sqlFetchArray($checkRes);
		return $res['pc_aid'];
	}else{
		$proID = getClinicHeadId($facilityID);
		if($proID == 0){
			$proID  = 1;
		}
		return $proID;
	}
}
function getClinicHeadId($facilityID){
	$checkQuery = "SELECT u.id FROM users AS u INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id 
WHERE u.facility_id = ".$facilityID." AND gag.id=12";
	$checkRes = sqlStatement($checkQuery);
	$checkNumRows = sqlNumRows($checkRes);
	if($checkNumRows){
		$res = sqlFetchArray($checkRes);
		return $res['id'];
	}else{
		return 0;
	}
}
function fileUpload($fileData, $updir){
	$doit = true;
	$returnData = array(); 
	$c_filename = '';	
	$max_size = 2000;         // sets maximum file size allowed (in KB)
	// file types allowed
	$allowtype = array('xlsx', 'xls', 'csv');
    if (isset($fileData)) {
	  // check for errors
	  if ($fileData['error'] > 0) {
		$doit = false;
		$returnData['status'] = $doit;
		$returnData['url'] = '';
	  }
	  else {
		// get the name, size (in kb) and type (the extension) of the file
		$fname = $fileData['name'];
		$fsize = $fileData['size'] / 1024;
		$ftype = end(explode('.', strtolower($fname)));
		$newFileName = time().'_'.$fname;
		// checks if the file already exists
		if (file_exists($updir. $newFileName)) {
			$doit = false;
			$returnData['status'] = $doit;
			$returnData['url'] = '';
		}
		else {
		  // if the file not exists, check its type (by extension) and size
		  if (in_array($ftype, $allowtype)) {
			// check the size
			if ($fsize <= $max_size) {
			  // uses  function to copy the file from temporary folder to $updir
			  if (!move_uploaded_file ($fileData['tmp_name'], $updir. $newFileName)) {
				$doit = false;
				$returnData['status'] = $doit;
				$returnData['url'] = '';
			  }
			  else {
				$c_filename = $updir.$newFileName;
				$returnData['status'] = $doit;
				$returnData['url'] = $c_filename;
				//echo $fname. ' ('. $fsize. ' kb) was successfully uploaded';
			  }
			}
			else {
				$doit = false;
				$returnData['status'] = $doit;
				$returnData['url'] = '';
			}
		  }
		  else {
			$doit = false;
			$returnData['status'] = $doit;
			$returnData['url'] = '';
		  }
		}
	  }
	}else{
		$doit = false;
		$returnData['status'] = $doit;
		$returnData['url'] = '';
	}
	return $returnData;
}
?>