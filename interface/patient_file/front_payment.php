<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$fake_register_globals=false;
$sanitize_all_escapes=true;

require_once("../globals.php");
require_once("$srcdir/acl.inc");
require_once("$srcdir/patient.inc");
require_once("$srcdir/billing.inc");
require_once("$srcdir/forms.inc");
require_once("$srcdir/sl_eob.inc.php");
require_once("$srcdir/invoice_summary.inc.php");
require_once("../../custom/code_types.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/encounter_events.inc.php");
$pid = $_REQUEST['hidden_patient_code'] > 0 ? $_REQUEST['hidden_patient_code'] : $pid;

$INTEGRATED_AR = $GLOBALS['oer_config']['ws_accounting']['enabled'] === 2;
?>
<html>
<head>
<?php html_header_show();?>
<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<?php
// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = oeFormatMoney($amount);
    return $amount;
  }
  return '';
}

function rawbucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    return $amount;
  }
  return '';
}

// Display a row of data for an encounter.
//
$var_index=0;
$var_row_index =0;
function echoLine($iname, $date, $charges, $ptpaid, $inspaid, $duept,$encounter=0,$copay=0,$patcopay=0) {
  global $var_index;
  global $var_row_index;
  
  //echo "ptpaid => ".$ptpaid;
  //$balance = bucks($charges - $ptpaid - $inspaid - $copay*-1);
  //$balance = (round($duept,2) != 0) ? 0 : $balance;//if balance is due from patient, then insurance balance is displayed as zero
  $balance = $duept-$patcopay;
  $encounter = $encounter ? $encounter : ''; 
	if($duept > 0) {
		$var_row_index++;
		$var_index++;
		echo " <tr id='tr_".attr($var_index)."'";
		/*if($var_row_index%2 == 0){
			echo " bgcolor='#F0F0F0'";
		}*/
		echo " >\n";
		echo "  <td class='detail'>" . text(oeFormatShortDate($date)) . "</td>\n";
		echo "  <td class='detail' id='".attr($date)."'>" . $encounter . "</td>\n";
		echo "  <td class='detail' align='center' id='td_charges_$var_index' >" . htmlspecialchars(bucks($charges), ENT_QUOTES) . "</td>\n";
		//echo "  <td class='detail' align='center' id='td_inspaid_$var_index' >" . htmlspecialchars(bucks($inspaid*-1), ENT_QUOTES) . "</td>\n";
		echo "  <td class='detail' align='center' id='td_ptpaid_$var_index' >" . htmlspecialchars(bucks(round($charges-($duept+$patcopay),2)*1), ENT_QUOTES) . "</td>\n";
		echo "  <td class='detail' align='center' id='td_patient_copay_$var_index' >" . htmlspecialchars(bucks($patcopay), ENT_QUOTES) . "</td>\n";
		//echo "  <td class='detail' align='center' id='td_copay_$var_index' >" . htmlspecialchars(bucks($copay), ENT_QUOTES) . "</td>\n";
		//echo "  <td class='detail' align='center' id='balance_$var_index'>" . htmlspecialchars(bucks($balance), ENT_QUOTES) . "</td>\n";
		echo "  <td class='detail' align='center' id='duept_$var_index'>" . htmlspecialchars(bucks(round($duept,2)*1), ENT_QUOTES) . "</td>\n";
		echo "  <td class='detail' align='right'><input type='hidden' value='".($duept)."' id='pbalance_".attr($var_index)."' name='form_balance[]'><input type='text' name='".attr($iname)."'  id='paying_".attr($var_index)."' " .
		" value='" .  '' . "' onchange='coloring();calctotal(); disableother(this.id);'  autocomplete='off' " .
		"onkeyup='calctotal()'  onkeypress='return allowPositiveNumber(event);'  style='width:50px'/></td>\n";
		echo " </tr>\n";
		echo "<tr><td colspan='7'><hr></td></tr>";
		
	}
}

// Post a payment to the payments table.
//
function frontPayment($patient_id, $encounter, $method, $source, $amount1, $amount2) {
  global $timestamp;
  global $currentTime;
  $tmprow = sqlQuery("SELECT date FROM form_encounter WHERE " .
    "encounter=? and pid=?",
		array($encounter,$patient_id));
	//the manipulation is done to insert the amount paid into payments table in correct order to show in front receipts report,
	//if the payment is for today's encounter it will be shown in the report under today field and otherwise shown as previous
  $tmprowArray=explode(' ',$tmprow['date']);
  if(date('Y-m-d')==$tmprowArray[0])
   {
    if($amount1==0)
	 {
	  $amount1=$amount2;
	  $amount2=0;
	 }
   }
  else
   {
    if($amount2==0)
	 {
	  $amount2=$amount1;
	  $amount1=0;
	 }
   }
  $payid = sqlInsert("INSERT INTO payments ( " .
    "pid, encounter, dtime, user, method, source, amount1, amount2 " .
    ") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)", array($patient_id,$encounter,$currentTime,$_SESSION['authUser'],$method,$source,$amount1,$amount2) );
  return $payid;
}

// We use this to put dashes, colons, etc. back into a timestamp.
//
function decorateString($fmt, $str) {
  $res = '';
  while ($fmt) {
    $fc = substr($fmt, 0, 1);
    $fmt = substr($fmt, 1);
    if ($fc == '.') {
      $res .= substr($str, 0, 1);
      $str = substr($str, 1);
    } else {
      $res .= $fc;
    }
  }
  return $res;
}

// Compute taxes from a tax rate string and a possibly taxable amount.
//
function calcTaxes($row, $amount) {
  $total = 0;
  if (empty($row['taxrates'])) return $total;
  $arates = explode(':', $row['taxrates']);
  if (empty($arates)) return $total;
  foreach ($arates as $value) {
    if (empty($value)) continue;
    $trow = sqlQuery("SELECT option_value FROM list_options WHERE " .
      "list_id = 'taxrate' AND option_id = ? LIMIT 1", array($value) );
    if (empty($trow['option_value'])) {
      echo "<!-- Missing tax rate '".text($value)."'! -->\n";
      continue;
    }
    $tax = sprintf("%01.2f", $amount * $trow['option_value']);
    // echo "<!-- Rate = '$value', amount = '$amount', tax = '$tax' -->\n";
    $total += $tax;
  }
  return $total;
}

$now = time();
$today = date('Y-m-d', $now);
$timestamp = date('Y-m-d H:i:s', $now);
$mysqlCurrentTime = sqlQuery("SELECT NOW() as currenttime");
$currentTime = $mysqlCurrentTime['currenttime'];
if (!$INTEGRATED_AR) slInitialize();

// $patdata = getPatientData($pid, 'fname,lname,pubpid');

$patdata = sqlQuery("SELECT " .
  "p.fname, p.mname, p.lname, p.pubpid,p.pid, i.copay " .
  "FROM patient_data AS p " .
  "LEFT OUTER JOIN insurance_data AS i ON " .
  "i.pid = p.pid AND i.type = 'primary' " .
  "WHERE p.pid = ? ORDER BY i.date DESC LIMIT 1", array($pid) );

$alertmsg = ''; // anything here pops up in an alert box

// If the Save button was clicked...
if ($_POST['form_save']) {
  $form_pid = $_POST['form_pid'];
  $form_method = trim($_POST['form_method']);
  $form_source = trim($_POST['form_source']);
  $patdata = getPatientData($form_pid, 'fname,mname,lname,pubpid');
  $NameNew=$patdata['fname'] . " " .$patdata['lname']. " " .$patdata['mname'];
  $mysqlCurrentTime = sqlQuery("SELECT NOW() as currenttime");
  $currentTime = $mysqlCurrentTime['currenttime'];
//echo '<pre>';
//print_r($_POST);
//echo '</pre>';
//die();
	/*if($_REQUEST['radio_type_of_payment']=='pre_payment')
	 {
		  $payment_id = idSqlStatement("insert into ar_session set "    .
			"payer_id = ?"       .
			", patient_id = ?"   .
			", user_id = ?"     . 
			", closed = ?"      .
			", reference = ?"   . 
			", check_date =  now() , deposit_date = now() "	.
			",  pay_total = ?"    . 
			", payment_type = 'patient'" .
			", description = ?"   .
			", adjustment_code = 'pre_payment'" .
			", post_to_date = now() " .
			", payment_method = ?",
			array(0,$form_pid,$_SESSION['authUserID'],0,$form_source,$_REQUEST['form_prepayment'],$NameNew,$form_method));
	
		 frontPayment($form_pid, 0, $form_method, $form_source, $_REQUEST['form_prepayment'], 0);//insertion to 'payments' table.
	 }*/
  
  if ($_POST['form_upay']) {
    foreach ($_POST['form_upay'] as $enc => $payment) {
      if ($amount = 0 + $payment) {
	       $zero_enc=$enc;
	       if($_REQUEST['radio_type_of_payment']=='invoice_balance')
		    { 
			 ;
		    }
		   else
		    { 
			 if (!$enc) 
			  {
					$enc = calendar_arrived($form_pid);
			  }
		    }
//----------------------------------------------------------------------------------------------------
			//Fetching the existing code and modifier
			$ResultSearchNew = sqlStatement("SELECT * FROM billing LEFT JOIN code_types ON billing.code_type=code_types.ct_key ".
				"WHERE code_types.ct_fee=1 AND billing.activity!=0 AND billing.pid =? AND encounter=? ORDER BY billing.code,billing.modifier",
				array($form_pid,$enc));
			 if($RowSearch = sqlFetchArray($ResultSearchNew))
			  {
                                $Codetype=$RowSearch['code_type'];
				$Code=$RowSearch['code'];
				$Modifier=$RowSearch['modifier'];
			  }
			 else
			  {
                                $Codetype='';
				$Code='';
				$Modifier='';
			  }
//----------------------------------------------------------------------------------------------------
			if($_REQUEST['radio_type_of_payment']=='copay')//copay saving to ar_session and ar_activity tables
			 {
				$session_id=idSqlStatement("INSERT INTO ar_session (payer_id,user_id,reference,check_date,deposit_date,pay_total,".
				 " global_amount,payment_type,description,patient_id,payment_method,adjustment_code,post_to_date) ".
				 " VALUES ('0',?,?,'".$currentTime."','".$currentTime."',?,'','patient','COPAY',?,?,'patient_payment','".$currentTime."')",
				 array($_SESSION['authId'],$form_source,$amount,$form_pid,$form_method));
				 
				  $insrt_id=idSqlStatement("INSERT INTO ar_activity (pid,encounter,code_type,code,modifier,payer_type,post_time,post_user,session_id,pay_amount,account_code)".
				   " VALUES (?,?,?,?,?,0,'".$currentTime."',?,?,?,'PCP')",
					 array($form_pid,$enc,$Codetype,$Code,$Modifier,$_SESSION['authId'],$session_id,$amount));
				   
				 frontPayment($form_pid, $enc, $form_method, $form_source, $amount, 0);//insertion to 'payments' table.
			 }
			if($_REQUEST['radio_type_of_payment']=='invoice_balance' ||  $_REQUEST['radio_type_of_payment']=='pre_payment' || $_REQUEST['radio_type_of_payment']=='cash')
			 {				//Payment by patient after insurance paid, cash patients similar to do not bill insurance in feesheet.
				  if($_REQUEST['radio_type_of_payment']=='cash')
				   {
				    sqlStatement("update form_encounter set last_level_closed=? where encounter=? and pid=? ",
							array(4,$enc,$form_pid));
				    sqlStatement("update billing set billed=? where encounter=? and pid=?",
							array(1,$enc,$form_pid));
				   }
				  $adjustment_code='patient_payment';
				  $payment_id = idSqlStatement("insert into ar_session set "    .
					"payer_id = ?"       .
					", patient_id = ?"   .
					", user_id = ?"     .
					", closed = ?"      .
					", reference = ?"   .
					", check_date =  '".$currentTime."' , deposit_date = '".$currentTime."' "	.
					",  pay_total = ?"    .
					", payment_type = 'patient'" .
					", description = ?"   .
					", adjustment_code = ?" .
					", post_to_date = now() " .
					", payment_method = ?",
					array(0,$form_pid,$_SESSION['authUserID'],0,$form_source,$amount,$NameNew,$adjustment_code,$form_method));

	//--------------------------------------------------------------------------------------------------------------------

        			frontPayment($form_pid, $enc, $form_method, $form_source, 0, $amount);//insertion to 'payments' table.

	//--------------------------------------------------------------------------------------------------------------------

					$resMoneyGot = sqlStatement("SELECT sum(pay_amount) as PatientPay FROM ar_activity where pid =? and ".
						"encounter =? and payer_type=0 and account_code='PCP'",
						array($form_pid,$enc));//new fees screen copay gives account_code='PCP'
					$rowMoneyGot = sqlFetchArray($resMoneyGot);
					$Copay=$rowMoneyGot['PatientPay'];
					
	//--------------------------------------------------------------------------------------------------------------------

					//Looping the existing code and modifier
					$ResultSearchNew = sqlStatement("SELECT * FROM billing LEFT JOIN code_types ON billing.code_type=code_types.ct_key WHERE code_types.ct_fee=1 ".
						"AND billing.activity!=0 AND billing.pid =? AND encounter=? ORDER BY billing.code,billing.modifier",
					  array($form_pid,$enc));
					 while($RowSearch = sqlFetchArray($ResultSearchNew))
					  {
                        $Codetype=$RowSearch['code_type'];
						$Code=$RowSearch['code'];
						$Modifier =$RowSearch['modifier'];
						$Fee =$RowSearch['fee'];
						
						$resMoneyGot = sqlStatement("SELECT sum(pay_amount) as MoneyGot FROM ar_activity where pid =? ".
							"and code_type=? and code=? and modifier=? and encounter =? and !(payer_type=0 and account_code='PCP')",
						array($form_pid,$Codetype,$Code,$Modifier,$enc));
						//new fees screen copay gives account_code='PCP'
						$rowMoneyGot = sqlFetchArray($resMoneyGot);
						$MoneyGot=$rowMoneyGot['MoneyGot'];

						$resMoneyAdjusted = sqlStatement("SELECT sum(adj_amount) as MoneyAdjusted FROM ar_activity where ".
						  "pid =? and code_type=? and code=? and modifier=? and encounter =?",
						  array($form_pid,$Codetype,$Code,$Modifier,$enc));
						$rowMoneyAdjusted = sqlFetchArray($resMoneyAdjusted);
						$MoneyAdjusted=$rowMoneyAdjusted['MoneyAdjusted'];
						
						$Remainder=$Fee-$Copay-$MoneyGot-$MoneyAdjusted;
						$Copay=0;
						if(round($Remainder,2)!=0 && $amount!=0) 
						 {
						  if($amount-$Remainder >= 0)
						   {
								$insert_value=$Remainder;
								$amount=$amount-$Remainder;
						   }
						  else
						   {
								$insert_value=$amount;
								$amount=0;
						   }
						  sqlStatement("insert into ar_activity set "    .
							"pid = ?"       .
							", encounter = ?"     .
                            ", code_type = ?"      .
							", code = ?"      .
							", modifier = ?"      .
							", payer_type = ?"   .
							", post_time = '".$currentTime."' " .
							", post_user = ?" .
							", session_id = ?"    .
							", pay_amount = ?" .
							", adj_amount = ?"    .
							", account_code = 'PP'",
							array($form_pid,$enc,$Codetype,$Code,$Modifier,0,$_SESSION['authUserID'],$payment_id,$insert_value,0));
						 }//if
					  }//while
					 if($amount!=0)//if any excess is there.
					  {
						  sqlStatement("insert into ar_activity set "    .
							"pid = ?"       .
							", encounter = ?"     .
                                                        ", code_type = ?"      .
							", code = ?"      .
							", modifier = ?"      .
							", payer_type = ?"   .
							", post_time = '".$currentTime."' " .
							", post_user = ?" .
							", session_id = ?"    .
							", pay_amount = ?" .
							", adj_amount = ?"    .
							", account_code = 'PP'",
							array($form_pid,$enc,$Codetype,$Code,$Modifier,0,$_SESSION['authUserID'],$payment_id,$amount,0));
					  }

	//--------------------------------------------------------------------------------------------------------------------
			   }//invoice_balance
			}//if ($amount = 0 + $payment) 
		}//foreach
	 }//if ($_POST['form_upay'])
  }//if ($_POST['form_save'])

if ($_POST['form_save'] || $_REQUEST['receipt']) {
  if ($_REQUEST['receipt']) {
    $form_pid = $_GET['patient'];
	$form_encounter = $_GET['encounter'];
    $timestamp = decorateString('....-..-.. ..:..:..', $_GET['time']);
	// Re-fetch payment info.
	  /*echo "SELECT " .
		"SUM(amount1) AS amount1, " .
		"SUM(amount2) AS amount2, " .
		"MAX(method) AS method, " .
		"MAX(source) AS source, " .
		"MAX(dtime) AS dtime, " .
		// "MAX(user) AS user " .
		"MAX(user) AS user, " .
		"MAX(encounter) as encounter ".
		"FROM payments WHERE " .
		"pid = $form_pid AND encounter=$form_encounter AND dtime = $timestamp";*/
	  $payrow = sqlQuery("SELECT id, " .
		"SUM(amount1) AS amount1, " .
		"SUM(amount2) AS amount2, " .
		"MAX(method) AS method, " .
		"MAX(source) AS source, " .
		"MAX(dtime) AS dtime, " .
		// "MAX(user) AS user " .
		"MAX(user) AS user, " .
		"MAX(encounter) as encounter ".
		"FROM payments WHERE " .
		"pid = ? AND encounter = ? AND dtime = ?", array($form_pid, $form_encounter,$timestamp) );
  }else {
	  // Re-fetch payment info.
	  /*echo "SELECT " .
		"SUM(amount1) AS amount1, " .
		"SUM(amount2) AS amount2, " .
		"MAX(method) AS method, " .
		"MAX(source) AS source, " .
		"MAX(dtime) AS dtime, " .
		// "MAX(user) AS user " .
		"MAX(user) AS user, " .
		"MAX(encounter) as encounter ".
		"FROM payments WHERE " .
		"pid = $form_pid AND dtime = $timestamp";*/
	  $timestamp = $currentTime;
	  $payrow = sqlQuery("SELECT id, " .
		"SUM(amount1) AS amount1, " .
		"SUM(amount2) AS amount2, " .
		"MAX(method) AS method, " .
		"MAX(source) AS source, " .
		"MAX(dtime) AS dtime, " .
		// "MAX(user) AS user " .
		"MAX(user) AS user, " .
		"MAX(encounter) as encounter ".
		"FROM payments WHERE " .
		"pid = ? AND dtime = ?", array($form_pid,$timestamp) );
  }

  // Get details for what we guess is the primary facility.
  $frow = sqlQuery("SELECT * FROM facility " .
    "ORDER BY billing_location DESC, accepts_assignment DESC, id LIMIT 1");
 	
  // Get the patient's name and chart number.
  $patdata = getPatientData($form_pid, 'fname,mname,lname,pubpid,title,language,DOB,street,postal_code,city,state,country_code,drivers_license,phone_home,phone_biz,phone_contact,phone_cell,sex,email');

  
	/*echo '<pre>';
	print_r($payrow);
	echo '</pre>';*/
  // Create key for deleting, just in case.
	$ref_id = ($_REQUEST['radio_type_of_payment']=='copay') ? $session_id : $payment_id ;
  $payment_key = $form_pid . '.' . preg_replace('/[^0-9]/', '', $timestamp).'.'.$ref_id;

  // get facility from encounter
  $tmprow = sqlQuery("
    SELECT facility_id
    FROM form_encounter
    WHERE encounter = ?", array($payrow['encounter']) );
  $frow = sqlQuery("SELECT * FROM facility " .
    " WHERE id = ?", array($tmprow['facility_id']) );
 //print_r($frow);
  // Now proceed with printing the receipt.
?>

<title><?php echo xlt('Receipt for Payment'); ?></title>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script language="JavaScript">

<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

 // Process click on Print button.
 function printme() {
  var divstyle = document.getElementById('hideonprint').style;
  divstyle.display = 'none';
  window.print();
  // divstyle.display = 'block';
 }
 // Process click on Delete button.
 function deleteme() {
  dlgopen('deleter.php?payment=<?php echo $payment_key ?>', '_blank', 500, 450);
  return false;
 }
 // Called by the deleteme.php window on a successful delete.
 function imdeleted() {
  window.close();
 }

 // Called to switch to the specified encounter having the specified DOS.
 // This also closes the popup window.
 function toencounter(enc, datestr, topframe) {
  topframe.restoreSession();
<?php if ($GLOBALS['concurrent_layout']) { ?>
  // Hard-coding of RBot for this purpose is awkward, but since this is a
  // pop-up and our openemr is left_nav, we have no good clue as to whether
  // the top frame is more appropriate.
  topframe.left_nav.forceDual();
  topframe.left_nav.setEncounter(datestr, enc, '');
  topframe.left_nav.setRadio('RBot', 'enc');
  topframe.left_nav.loadFrame('enc2', 'RBot', 'patient_file/encounter/encounter_top.php?set_encounter=' + enc);
<?php } else { ?>
  topframe.Title.location.href = 'encounter/encounter_title.php?set_encounter='   + enc;
  topframe.Main.location.href  = 'encounter/patient_encounter.php?set_encounter=' + enc;
<?php } ?>
  window.close();
 }

</script>
</head>
<body bgcolor='#ffffff'>
<!--<center>

<p><h2><?php echo xlt('Receipt for Payment'); ?></h2>
<hr>
<table border='0' cellspacing='8'>
	<tr>
		<td width='50%' align="left">
			<?php echo text($frow['street']) ?>
			<br><?php echo text($frow['name']) ?>
			<br><?php echo text($frow['city'] . ', ' . $frow['state']) . ' ' .text($frow['postal_code']) ?>
		</td>
		<td width='50%' align="right">
			<?php echo xlt('Phone'); ?>:<?php echo htmlentities($frow['phone']) ?>
			<br><?php echo xlt('Website'); ?>:<?php echo htmlentities($frow['website']) ?>
			<br><?php echo xlt('Email'); ?>:<?php echo htmlentities($frow['email']) ?>
		</td>
	</tr>
</table>-->
<table border='0' cellspacing='8'>
	<tr>
		<td width='50%'>
		<div style="float:left;padding-right:5px;"><img style=" width:100px; height:50px;" src="http://clovedental.in/images/clove-logo.png" /></div>
			<div style="float:left;"><?php echo text($frow['street']) ?>
			<br><?php echo text($frow['name']) ?>
			<br><?php echo text($frow['city'] . ', ' . $frow['state']) . ' ' .text($frow['postal_code']) ?>
			<?php echo xlt('Phone'); ?>:<?php echo htmlentities($frow['phone']) ?>
			<br><?php echo xlt('Website'); ?>:<?php echo htmlentities($frow['website']) ?>
			<br><?php echo xlt('Email'); ?>:<?php echo htmlentities($frow['email']) ?>
			</div>
		</td>
	</tr>
</table>
<hr>
<table border="0" width="100%">
<tr>
<td width="70%">Customer Name:	<?php echo text($patdata['fname']) . " " . text($patdata['mname']) . " " .text($patdata['lname']);?></td>
<td width="30%">Customer ID: <?php echo text($patdata['pubpid']);?></td>
</tr>
</table>
<hr>
<table border="0" width="100%">
<tr>
<td>Received Rs. <?php $totalPayed = $payrow['amount1']+$payrow['amount2'];echo text(number_format($totalPayed,2)) ?> on <?php echo text(date("F j, Y \A\T\ g:i A", strtotime($payrow['dtime']))) ?><date e.g. July 29, 2013> towards payment of following invoices.</td>
</tr>
<tr>
<td>Mode of payment: <?php echo generate_display_field(array('data_type'=>'1','list_id'=>'payment_method'),$payrow['method']); ?></td>
</tr>
</table>
<hr>
<table border="0" width="100%">
<tr>
<?php
$encrow = sqlQuery("SELECT invoice_refno FROM form_encounter WHERE " .
"pid = ? AND encounter = ? LIMIT 1", array($form_pid,$payrow['encounter']) );
$invoice_refno = $encrow['invoice_refno'];
?>
<td width="70%">Bill Number: <?php if ($invoice_refno) echo text($invoice_refno);?>
<br>
<?php echo xlt('Receipt Number'); ?>: <?php echo text($payrow['id']) ?>
</td>
<td width="30%">Bill Date: 	<?php echo text(oeFormatSDFT(strtotime($payrow['dtime']))) ?></td>
</tr>
</table>
<table width="100%" style="border-collapse:collapse;" border="1">
<tr><th align="left" style="background:#999;">Service Rendered/Product Name</th>
<th align="left" style="background:#999;">Discount</th>
<th align="left" style="background:#999;">Cost (INR)</th>
</tr>
<?php
//Looping the existing code and modifier
	$ResultSearchNew = sqlStatement("SELECT * FROM billing LEFT JOIN code_types ON billing.code_type=code_types.ct_key WHERE code_types.ct_fee=1 ".
		"AND billing.activity!=0 AND billing.pid =? AND encounter=? ORDER BY billing.code,billing.modifier",
	  array($form_pid,$payrow['encounter']));
	$totoalCast = 0;
	while($RowSearch = sqlFetchArray($ResultSearchNew)){
		//print_r($RowSearch);
		$totoalCast += sprintf('%01.2f', $RowSearch['fee']);
		?>
		<tr>
			<td width='60%' align='left'><?php echo text($RowSearch['code_text']);?></td>
			<td width='20%' align='left'><?php if($RowSearch['discount_type'] == 'amt') { echo 'INR ';} echo text($RowSearch['discount']); if($RowSearch['discount_type'] == 'per') { echo ' %';}?></td>
			<td width='20%'><?php echo text(number_format($RowSearch['fee'],2));?></td>
		<tr>
	<?php
	}
	
	// Product sales
	$inres = sqlStatement("SELECT s.sale_id, s.sale_date, s.fee, " .
	  "s.quantity, s.drug_id, d.name " .
	  "FROM drug_sales AS s LEFT JOIN drugs AS d ON d.drug_id = s.drug_id " .
	  // "WHERE s.pid = '$patient_id' AND s.encounter = '$encounter' AND s.fee != 0 " .
	  "WHERE s.pid = ? AND s.encounter = ? " .
	  "ORDER BY s.sale_id", array($form_pid,$payrow['encounter']) );
	while ($inrow = sqlFetchArray($inres)) {
		
	  $totoalCast += sprintf('%01.2f', $inrow['fee']);
	  //receiptDetailLine($inrow['sale_date'], $inrow['name'], $inrow['fee'], $inrow['quantity']);
		?>
		<tr>
			<td width='60%' align='left'><?php echo text($inrow['name']);?></td>
			<td width='20%' align='left'><?php if($inrow['discount_type'] == 'amt') { echo 'INR ';} echo text($inrow['discount']); if($inrow['discount_type'] == 'per') { echo ' %';}?></td>
			<td width='20%'><?php echo text(number_format($inrow['fee'],2));?></td>
		<tr>
	<?php
		
	}
	?>
	<tr>

		<td width="80%" align="right" colspan="2">
			<div style="padding:5px 20px;">Sub – Total</div>
			<div style="padding:5px 20px;">Discount</div>
			<div style="padding:5px 20px;"><b>Total Cost</b></div>
			<div style="padding:5px 20px;">Amount Received Earlier</div>
			<div style="padding:5px 20px;">Amount Received Today</div>
			<div style="padding:5px 20px;">Balance Amount</div>
		</td>
		<td width="20%" >
			<div style="border-bottom:#999 1px solid;padding:5px;"><?php echo text(number_format($totoalCast,2));?></div>
			<?php
			// Adjustments.
			$discounts = 0.00;
			$inres = sqlStatement("SELECT " .
			  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
			  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
			  "FROM ar_activity AS a " .
			  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
			  "a.pid = ? AND a.encounter = ? AND " .
			  "a.adj_amount != 0 " .
			  "ORDER BY s.check_date, a.sequence_no", array($form_pid,$payrow['encounter']) );
			while ($inrow = sqlFetchArray($inres)) {
			  $discounts += sprintf('%01.2f', $inrow['adj_amount']);
			  //$payer = empty($inrow['payer_type']) ? 'Pt' : ('Ins' . $inrow['payer_type']);
			  //receiptDetailLine($svcdate, $payer . ' ' . $inrow['memo'], 0 - $inrow['adj_amount'], 1);
			}
			/*echo "SELECT " .
			  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
			  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
			  "FROM ar_activity AS a " .
			  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
			  "a.pid = $form_pid  AND a.encounter = '".$payrow['encounter']."' AND " .
			  "a.pay_amount != 0  AND a.post_time <= '".date('Y-m-d H:i:s',strtotime($timestamp))."'".
			  " ORDER BY s.check_date, a.sequence_no";*/
			$inres = sqlStatement("SELECT " .
			  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
			  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
			  "FROM ar_activity AS a " .
			  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
			  "a.pid = ?  AND a.encounter = ? AND " .
			  "a.pay_amount != 0 AND a.post_time <= ?" .
			  "ORDER BY s.check_date, a.sequence_no", array($form_pid, $payrow['encounter'], $timestamp) );
			  $charges = 0;
			while ($inrow = sqlFetchArray($inres)) {
			  $payer = empty($inrow['payer_type']) ? 'Pt' : ('Ins' . $inrow['payer_type']);
			  $charges += sprintf('%01.2f', $inrow['pay_amount']);							  
			}
			?>
			<div style="border-bottom:#999 1px solid;padding:5px;"><?php echo text(number_format($discounts,2)); ?></div>
			<div style="border-bottom:#999 1px solid;padding:5px;"><?php $total = $totoalCast - $discounts; echo text(number_format($total,2)); ?></div>
			<div style="border-bottom:#999 1px solid;padding:5px;"><?php if($charges > 0) {echo text(number_format(($charges-$totalPayed),2));}else { echo text(number_format('0.00',2));} ?></div>
			<div style="border-bottom:#999 1px solid;padding:5px;"><?php echo text(number_format($totalPayed,2)) ?></div>
			<div style="padding:5px;"><?php $balanceAmt = $total - $charges; if($balanceAmt > 0){ echo text(number_format($balanceAmt,2));}else{ echo text(number_format('0.00',2));}?></div>
		</td>
	</tr>
</table>

<table border="0" width="100%">
	<tr>
		<td width="70%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
	</tr>
	<tr>
		<td width="70%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
	</tr>
	<tr>
		<td width="70%">&nbsp;</td>
		<td width="30%"><hr><div style="text-align:center;">Authorized Signatory<div></td>
	</tr>
</table>
<!--<table border='0' cellspacing='8' width="900px">
	<tr>
		<td width='50%'><strong>Invoice</strong></td>
		<td width='50%' align='right'>
		<?php echo xlt('Date'); ?> : <?php echo text(oeFormatSDFT(strtotime($payrow['dtime']))) ?>
		<?php
			// Get invoice reference number.
			$encrow = sqlQuery("SELECT invoice_refno FROM form_encounter WHERE " .
			"pid = ? AND encounter = ? LIMIT 1", array($form_pid,$payrow['encounter']) );
			$invoice_refno = $encrow['invoice_refno'];
			if ($invoice_refno) echo "<br> " . xlt("Invoice Number") . ": " . text($invoice_refno);
		?>
		<br>
		<?php echo xlt('Receipt #'); ?> : <?php echo text($payrow['id']) ?>
		</td>
	</tr>
	<tr>
		<td colspan='2' align='left'>
			<table border='0' cellspacing='8' width="100%">
				<tr  bgcolor="#d6cece">
					<th width='30%' align='left'>Treatment</th>
					<th width='20%'>Unit Cost INR</th>
					<th width='10%'>Qty</th>
					<th width='40%' align='right'>Total Cost INR</th>
				<tr>
				<?php
				//Looping the existing code and modifier
					$ResultSearchNew = sqlStatement("SELECT * FROM billing LEFT JOIN code_types ON billing.code_type=code_types.ct_key WHERE code_types.ct_fee=1 ".
						"AND billing.activity!=0 AND billing.pid =? AND encounter=? ORDER BY billing.code,billing.modifier",
					  array($form_pid,$payrow['encounter']));
					$totoalCast = 0;
					while($RowSearch = sqlFetchArray($ResultSearchNew)){
						//print_r($RowSearch);
						$totoalCast += sprintf('%01.2f', $RowSearch['fee']);
						?>
						<tr>
							<td width='30%' align='left'><?php echo text($RowSearch['code_text']);?></td>
							<td width='20%' align='center'><?php echo text(number_format(($RowSearch['fee']/$RowSearch['units']),2));?></td>
							<td width='10%' align='center'><?php echo text($RowSearch['units']);?></td>
							<td width='40%' align='right'><?php echo text(number_format($RowSearch['fee'],2));?></td>
						<tr>
					<?php
					}
					
					// Product sales
					$inres = sqlStatement("SELECT s.sale_id, s.sale_date, s.fee, " .
					  "s.quantity, s.drug_id, d.name " .
					  "FROM drug_sales AS s LEFT JOIN drugs AS d ON d.drug_id = s.drug_id " .
					  // "WHERE s.pid = '$patient_id' AND s.encounter = '$encounter' AND s.fee != 0 " .
					  "WHERE s.pid = ? AND s.encounter = ? " .
					  "ORDER BY s.sale_id", array($form_pid,$payrow['encounter']) );
					while ($inrow = sqlFetchArray($inres)) {
						
					  $totoalCast += sprintf('%01.2f', $inrow['fee']);
					  //receiptDetailLine($inrow['sale_date'], $inrow['name'], $inrow['fee'], $inrow['quantity']);
						?>
						<tr>
							<td width='30%' align='left'><?php echo text($inrow['name']);?></td>
							<td width='20%' align='center'><?php echo text(number_format($inrow['fee']/$inrow['quantity'],2));?></td>
							<td width='10%' align='center'><?php echo text($RowSearch['units']);?></td>
							<td width='40%' align='right'><?php echo text(number_format($inrow['fee'],2));?></td>
						<tr>
					<?php
						
					}
					// Adjustments.
					$inres = sqlStatement("SELECT " .
					  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
					  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
					  "FROM ar_activity AS a " .
					  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
					  "a.pid = ? AND a.encounter = ? AND " .
					  "a.adj_amount != 0 " .
					  "ORDER BY s.check_date, a.sequence_no", array($form_pid,$payrow['encounter']) );
					while ($inrow = sqlFetchArray($inres)) {
					  $totoalCast -= sprintf('%01.2f', $inrow['adj_amount']);
					  $payer = empty($inrow['payer_type']) ? 'Pt' : ('Ins' . $inrow['payer_type']);
					  //receiptDetailLine($svcdate, $payer . ' ' . $inrow['memo'], 0 - $inrow['adj_amount'], 1);
						?>
						<tr>
							<td width='30%' align='left'><?php echo text($payer . ' ' . $inrow['memo']);?></td>
							<td width='20%' align='center'><?php echo text(number_format((0 - $inrow['adj_amount']),2));?></td>
							<td width='10%' align='center'><?php echo text($RowSearch['units']);?></td>
							<td width='40%' align='right'><?php echo text(number_format((0 - $inrow['adj_amount']),2));?></td>
						<tr>
					<?php
					}
				?>
				<tr>
					<td width='30%'>&nbsp;</td>
					<td width='20%'>&nbsp;</td>
					<td width='10%'>&nbsp;</td>
					<td width='40%' align='right'><hr><?php echo xlt('Total Cost'); ?> : <?php echo text(number_format($totoalCast,2));?> INR<hr></td>
				<tr>
				<tr>
					<td colspan='3' style="padding-top:40px;" valign="top">
						<table border='0' cellspacing='8'>
							<tr  bgcolor="#d6cece">
								<th width='40%' align='left'>Date</th>
								<th width='20%'>Mode Of Payment</th>
								<th width='20%'>Received By</th>
								<th width='30%'>Amount Paid INR</th>
							<tr>
							<?php
							// Get co-pays.	
							//echo 'Total Cast => '.$totoalCast;
							$inres = sqlStatement("SELECT fee, code_text, date FROM billing WHERE " .
							  "pid = ? AND encounter = ?  AND " .
							  "code_type = 'COPAY' AND activity = 1 AND fee != 0 " .
							  "ORDER BY id", array($form_pid,$payrow['encounter']) );
							while ($inrow = sqlFetchArray($inres)) {
							  $totoalCast += sprintf('%01.2f', $inrow['fee']);							  
							}
							//echo 'Total Cast => '.$totoalCast;
							/*echo "SELECT " .
							  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
							  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
							  "FROM ar_activity AS a " .
							  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
							  "a.pid = $form_pid   AND a.encounter = ".$payrow['encounter']." AND " .
							  "a.pay_amount != 0 " .
							  "ORDER BY s.check_date, a.sequence_no";*/
							$inres = sqlStatement("SELECT " .
							  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
							  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
							  "FROM ar_activity AS a " .
							  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
							  "a.pid = ?  AND a.encounter = ? AND " .
							  "a.pay_amount != 0 " .
							  "ORDER BY s.check_date, a.sequence_no", array($form_pid, $payrow['encounter']) );
							  $charges = 0;
							while ($inrow = sqlFetchArray($inres)) {
							  $payer = empty($inrow['payer_type']) ? 'Pt' : ('Ins' . $inrow['payer_type']);
							  $charges += sprintf('%01.2f', $inrow['pay_amount']);							  
							}
							/*//echo 'Total Cast => '.$totoalCast;
							$ResultSearchNew = sqlStatement("SELECT * FROM billing LEFT JOIN code_types ON billing.code_type=code_types.ct_key WHERE code_types.ct_fee=1 ".
								"AND billing.activity!=0 AND billing.pid =? AND encounter=? ORDER BY billing.code,billing.modifier",
							  array($form_pid,$payrow['encounter']));
							$totoalCast = 0;
							while($RowSearch = sqlFetchArray($ResultSearchNew)){
								//print_r($RowSearch);
								$totoalCast += sprintf('%01.2f', $RowSearch['fee']);
							}
							//echo 'Total Cast => '.$totoalCast;
							$inres = sqlStatement("SELECT s.sale_id, s.sale_date, s.fee, " .
							  "s.quantity, s.drug_id, d.name " .
							  "FROM drug_sales AS s LEFT JOIN drugs AS d ON d.drug_id = s.drug_id " .
							  // "WHERE s.pid = '$patient_id' AND s.encounter = '$encounter' AND s.fee != 0 " .
							  "WHERE s.pid = ? AND s.encounter = ? " .
							  "ORDER BY s.sale_id", array($form_pid,$payrow['encounter']) );
							while ($inrow = sqlFetchArray($inres)) {
								$totoalCast += sprintf('%01.2f', $inrow['fee']);
							}
							//echo 'Total Cast => '.$totoalCast;
							$inres = sqlStatement("SELECT " .
							  "a.code_type, a.code, a.modifier, a.memo, a.payer_type, a.adj_amount, a.pay_amount, " .
							  "s.payer_id, s.reference, s.check_date, s.deposit_date " .
							  "FROM ar_activity AS a " .
							  "LEFT JOIN ar_session AS s ON s.session_id = a.session_id WHERE " .
							  "a.pid = ? AND a.encounter = ? AND " .
							  "a.adj_amount != 0 " .
							  "ORDER BY s.check_date, a.sequence_no", array($form_pid,$payrow['encounter']) );
							while ($inrow = sqlFetchArray($inres)) {
								/*echo '<pre>';
								print_r($inrow);
								echo '</pre>';
								$totoalCast -= sprintf('%01.2f', $inrow['adj_amount']);
							}*/
							//echo 'Total Cast => '.$totoalCast;
							?>
							<tr>
								<td width='40%' align='left'><?php echo text(oeFormatSDFT(strtotime($payrow['dtime']))) ?></td>
								<td width='20%' align='center'><?php echo generate_display_field(array('data_type'=>'1','list_id'=>'payment_method'),$payrow['method']); ?></td>
								<td width='10%' align='center'><?php echo text($payrow['user']) ?></td>
								<td width='30%' style="text-align:center;"><?php $totalPayed = $payrow['amount1']+$payrow['amount2'];echo text(number_format($totalPayed,2)) ?></td>
							<tr>
						</table>
					</td>					
					<td width='40%' align='right' valign="top">
						<?php echo xlt('Grand Total'); ?> : <?php echo text(number_format($totoalCast,2));?> INR
						<br><?php echo xlt('Past Amount Received'); ?> : <?php echo text(number_format(($charges-$totalPayed),2)) ?> INR
						<div style="padding-top:85px;"><hr><?php echo xlt('Today Amount Received'); ?> : <?php echo text(number_format($totalPayed,2)) ?> INR
						<hr><?php echo xlt('Total Balance Amount'); ?> : <?php $balanceAmt = $totoalCast - $charges; if($balanceAmt > 0){ echo text(number_format($balanceAmt,2));}else{ echo text(number_format('0.00',2));}?> INR </div>
					</td>
				<tr>
			</table>
		</td>		
	</tr>	
</table>-->
<div id='hideonprint'>
<p>
<input type='button' value='<?php echo xla('Print'); ?>' onclick='printme()' />

<?php
  $todaysenc = todaysEncounterIf($pid);
  if ($todaysenc && $todaysenc != $encounter) {
    echo "&nbsp;<input type='button' " .
      "value='" . xla('Open Today`s Visit') . "' " .
      "onclick='toencounter($todaysenc,\"$today\",opener.top)' />\n";
  }
?>

<?php if (acl_check('admin', 'super')) { ?>
&nbsp;
<input type='button' value='<?php xl('Delete','e'); ?>' style='color:red' onclick='deleteme()' />
<?php } ?>

</div>
<!--</center>-->
</body>

<?php
  //
  // End of receipt printing logic.
  //
} else {
  //
  // Here we display the form for data entry.
  //
?>
<title><?php echo xlt('Record Payment'); ?></title>

<style type="text/css">
 body    { font-family:sans-serif; font-size:10pt; font-weight:normal }
 .dehead { color:#000000; font-family:sans-serif; font-size:10pt; font-weight:bold }
 .detail { color:#000000; font-family:sans-serif; font-size:10pt; font-weight:normal }
#ajax_div_patient {
	position: absolute;
	z-index:10;
	background-color: #FBFDD0;
	border: 1px solid #ccc;
	padding: 10px;
}
</style>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">

<!-- supporting javascript code -->
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.js"></script>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>



<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="../../library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>
<script type="text/javascript" src="../../library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="../../library/js/jquery.easydrag.handler.beta2.js"></script> 
<script language='JavaScript'>
 var mypcc = '1';
</script>
<?php include_once("{$GLOBALS['srcdir']}/ajax/payment_ajax_jav.inc.php"); ?>
<script language="javascript" type="text/javascript">
document.onclick=HideTheAjaxDivs;
</script>

<script type="text/javascript" src="../../library/topdialog.js"></script>

<script language="JavaScript">
<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

function calctotal() {
 var f = document.forms[0];
 var total = 0;
 for (var i = 0; i < f.elements.length; ++i) {
  var elem = f.elements[i];
  var ename = elem.name;
  if (ename.indexOf('form_upay[') == 0 || ename.indexOf('form_bpay[') == 0) {
   if (elem.value.length > 0) total += Number(elem.value);
  }
 }
 f.form_paytotal.value = Number(total).toFixed(2);
 return true;
}
function disableother(id){

	var n=id.split("_"); 
	for (var i = 1; ; ++i) 
    {
		if(document.getElementById('paying_'+i)){
			var pendingCharges =  document.getElementById('paying_'+n[1]).value;
			//alert();
			if('paying_'+n[1] != 'paying_'+i && pendingCharges != ''){
				document.getElementById('paying_'+i).disabled = true;
			}else {
				document.getElementById('paying_'+i).disabled = false;
			}
		}else {
			break;
		}
	}

}


function coloring()
 {
   for (var i = 1; ; ++i) 
    {
	  if(document.getElementById('paying_'+i))
	   {
	    paying=document.getElementById('paying_'+i).value*1;
		patient_balance=document.getElementById('duept_'+i).innerHTML*1;
		//balance=document.getElementById('balance_'+i).innerHTML*1;
		if(patient_balance>0 && paying>0)
		 {
			if(paying>patient_balance)
			 {
			  document.getElementById('paying_'+i).style.background='#FF0000';
			 }
			else if(paying<patient_balance)
			 {
			  document.getElementById('paying_'+i).style.background='#99CC00';
			 }
			else if(paying==patient_balance)
			 {
			  document.getElementById('paying_'+i).style.background='#ffffff';
			 }
		 }
		else
		 {
		  document.getElementById('paying_'+i).style.background='#ffffff';
		 }
	   }
	  else
	   {
	    break;
	   }
	}
 }
function CheckVisible(MakeBlank)
 {//Displays and hides the check number text box.
   if(document.getElementById('form_method').options[document.getElementById('form_method').selectedIndex].value=='check_payment' ||
   	  document.getElementById('form_method').options[document.getElementById('form_method').selectedIndex].value=='bank_draft'  )
   {
	document.getElementById('check_number').disabled=false;
   }
   else
   {
	document.getElementById('check_number').disabled=true;
   }
 }
function validate()
 {
  var f = document.forms[0];
  ok=-1;
  top.restoreSession();
  issue='no';
   if(((document.getElementById('form_method').options[document.getElementById('form_method').selectedIndex].value=='check_payment' ||
   	  document.getElementById('form_method').options[document.getElementById('form_method').selectedIndex].value=='bank_draft') &&
	   document.getElementById('check_number').value=='' ))
   {
    alert("<?php echo addslashes( xl('Please Fill the Check/Ref Number')) ?>");
	document.getElementById('check_number').focus();
	return false;
   }

  if(document.getElementById('radio_type_of_payment_self1').checked==false && document.getElementById('radio_type_of_payment_self2').checked==false   && document.getElementById('radio_type_of_payment1').checked==false && document.getElementById('radio_type_of_payment2').checked==false  && document.getElementById('radio_type_of_payment5').checked==false  && document.getElementById('radio_type_of_payment4').checked==false)
   {
	  alert("<?php echo addslashes( xl('Please Select Type Of Payment.')) ?>");
	  return false;
   }
  if(document.getElementById('radio_type_of_payment_self1').checked==true || document.getElementById('radio_type_of_payment_self2').checked==true || document.getElementById('radio_type_of_payment1').checked==true  || document.getElementById('radio_type_of_payment4').checked==true || document.getElementById('radio_type_of_payment5').checked==true)
   {
		 var countPending = 0;
		 for (var i = 0; i < f.elements.length; ++i) 
		 {
			  var elem = f.elements[i];
			  var ename = elem.name;
			  if(ename == 'form_paytotal' && elem.value*1 <= 0) {
				alert('Please enter ammount');
				ok=1;
				return false;
			  }
			  if (ename.indexOf('form_upay[0') == 0) //Today is this text box.
			  {
			   if(elem.value*1>0)
				{//A warning message, if the amount is posted with out encounter.
				 if(confirm("<?php echo addslashes( xl('Are you sure to post for today?')) ?>"))
				  {
				   ok=1;
				  }
				 else
				  {
				   elem.focus();
				   return false;
				  }
				}
			   break;
			  }else if (ename.indexOf('form_upay[') == 0){
				//form_balance 
				//var aryInp=document.forms.remainingBillPayment.elements('form_balance[]');
				var aryInp=document.getElementsByName('form_balance[]');
				//alert(aryInp);
				var pendingCharges =  parseFloat(aryInp[countPending].value,10);
				var paidMoney = parseFloat(elem.value, 10);
				//var pendingChargesParsed = pendingCharges.toFixed(2);
				//var paidMoneyParsed = paidMoney.toFixed(2);
				//alert('Pending Charge '+pendingCharges);
				//alert('Paid Money '+paidMoney);
				//alert('Pending Charge '+typeof pendingCharges);
				//alert('Paid Money '+typeof paidMoney);
				//return false;
				if(paidMoney*1>0) {
					if(paidMoney > pendingCharges) {
						alert('Payment should be less than or equal to patient balance');
						elem.focus();
						return false;
					}
					
				}
				countPending++;
				
			  }
		  }
   }

  /*if(document.getElementById('radio_type_of_payment1').checked==true)//CO-PAY 
   {
	 var total = 0;
	 for (var i = 0; i < f.elements.length; ++i) 
	 {
	  var elem = f.elements[i];
	  var ename = elem.name;
	  if (ename.indexOf('form_upay[') == 0) //Today is this text box.
	  {
	   if(f.form_paytotal.value*1!=elem.value*1)//Total CO-PAY is not posted against today
	    {//A warning message, if the amount is posted against an old encounter.
		 if(confirm("<?php echo addslashes( xl('You are posting against an old encounter?')) ?>"))
		  {
		   ok=1;
		  }
		 else
		  {
		   elem.focus();
		   return false;
		  }
		}
	   break;
	  }
	}
   }//Co Pay
 else if(document.getElementById('radio_type_of_payment2').checked==true)//Invoice Balance
  {
   if(document.getElementById('Today').innerHTML=='')
    {
	 for (var i = 0; i < f.elements.length; ++i) 
	  {
	   var elem = f.elements[i];
	   var ename = elem.name;
	   if (ename.indexOf('form_upay[') == 0) 
		{
		 if (elem.value*1 > 0)
		  {
			  alert("<?php echo addslashes( xl('Invoice Balance cannot be posted. No Encounter is created.')) ?>");
			  return false;
		 }
		 break;
	   }
	  }
	}
  }*/
 if(ok==-1)
  {
	 if(confirm("<?php echo addslashes( xl('Would you like to save?')) ?>"))
	  {
	   return true;
	  }
	 else
	  {
	   return false;
	  }
  }
}
function allowPositiveNumber(e) {
	var charCode = (e.which) ? e.which : event.keyCode

	if (charCode > 31 && (charCode < 46 || charCode > 57 )) {
		return false;
	}
	return true;
 
}
function cursor_pointer()
 {//Point the cursor to the latest encounter(Today)
	 var f = document.forms[0];
	 var total = 0;
	 for (var i = 0; i < f.elements.length; ++i) 
	 {
	  var elem = f.elements[i];
	  var ename = elem.name;
	  if (ename.indexOf('form_upay[') == 0) 
	  {
	   elem.focus();
	   break;
	  }
	}
 }
 //=====================================================
function make_it_hide_enc_pay()
 {
  	document.getElementById('td_head_insurance_payment').style.display="none";
  	document.getElementById('td_head_patient_co_pay').style.display="none";
  	document.getElementById('td_head_co_pay').style.display="none";
  	document.getElementById('td_head_insurance_balance').style.display="none";
  for (var i = 1; ; ++i) 
  {
   	var td_inspaid_elem = document.getElementById('td_inspaid_'+i)
		var td_patient_copay_elem = document.getElementById('td_patient_copay_'+i)
   	var td_copay_elem = document.getElementById('td_copay_'+i)
   	var balance_elem = document.getElementById('balance_'+i)
   if (td_inspaid_elem) 
   {
    td_inspaid_elem.style.display="none";
		td_patient_copay_elem.style.display="none";
    td_copay_elem.style.display="none";
    balance_elem.style.display="none";
   }
  else
   {
    break;
   }
  }
  document.getElementById('td_total_4').style.display="none";
  document.getElementById('td_total_7').style.display="none";
	document.getElementById('td_total_8').style.display="none";
  document.getElementById('td_total_6').style.display="none";
 
  document.getElementById('table_display').width="420px";
 }

 //=====================================================
function make_visible()
 {
  document.getElementById('td_head_rep_doc').style.display="";
  document.getElementById('td_head_description').style.display="";
  document.getElementById('td_head_total_charge').style.display="none";
  document.getElementById('td_head_insurance_payment').style.display="none";
  document.getElementById('td_head_patient_payment').style.display="none";
	document.getElementById('td_head_patient_co_pay').style.display="none";
  document.getElementById('td_head_co_pay').style.display="none";
  document.getElementById('td_head_insurance_balance').style.display="none";
  document.getElementById('td_head_patient_balance').style.display="none";
  for (var i = 1; ; ++i) 
  {
   var td_charges_elem = document.getElementById('td_charges_'+i)
   var td_inspaid_elem = document.getElementById('td_inspaid_'+i)
   var td_ptpaid_elem = document.getElementById('td_ptpaid_'+i)
	 var td_patient_copay_elem = document.getElementById('td_patient_copay_'+i)
   var td_copay_elem = document.getElementById('td_copay_'+i)
   var balance_elem = document.getElementById('balance_'+i)
   var duept_elem = document.getElementById('duept_'+i)
   if (td_charges_elem) 
   {
    td_charges_elem.style.display="none";
    td_inspaid_elem.style.display="none";
    td_ptpaid_elem.style.display="none";
		td_patient_copay_elem.style.display="none";
    td_copay_elem.style.display="none";
    balance_elem.style.display="none";
    duept_elem.style.display="none";
   }
  else
   {
    break;
   }
  }
  document.getElementById('td_total_7').style.display="";
	document.getElementById('td_total_8').style.display="";
  document.getElementById('td_total_1').style.display="none";
  document.getElementById('td_total_2').style.display="none";
  document.getElementById('td_total_3').style.display="none";
  document.getElementById('td_total_4').style.display="none";
  document.getElementById('td_total_5').style.display="none";
  document.getElementById('td_total_6').style.display="none";
 
  document.getElementById('table_display').width="505px";
 }
function make_it_hide()
 {
  document.getElementById('td_head_rep_doc').style.display="none";
  document.getElementById('td_head_description').style.display="none";
  document.getElementById('td_head_total_charge').style.display="";
  document.getElementById('td_head_insurance_payment').style.display="";
  document.getElementById('td_head_patient_payment').style.display="";
  document.getElementById('td_head_patient_co_pay').style.display="";
	document.getElementById('td_head_co_pay').style.display="";
  document.getElementById('td_head_insurance_balance').style.display="";
  document.getElementById('td_head_patient_balance').style.display="";
  for (var i = 1; ; ++i) 
  {
   var td_charges_elem = document.getElementById('td_charges_'+i)
   var td_inspaid_elem = document.getElementById('td_inspaid_'+i)
   var td_ptpaid_elem = document.getElementById('td_ptpaid_'+i)
	 var td_patient_copay_elem = document.getElementById('td_patient_copay_'+i)
   var td_copay_elem = document.getElementById('td_copay_'+i)
   var balance_elem = document.getElementById('balance_'+i)
   var duept_elem = document.getElementById('duept_'+i)
   if (td_charges_elem) 
   {
    td_charges_elem.style.display="";
    td_inspaid_elem.style.display="";
    td_ptpaid_elem.style.display="";
		td_patient_copay_elem.style.display="";
    td_copay_elem.style.display="";
    balance_elem.style.display="";
    duept_elem.style.display="";
   }
  else
   {
    break;
   }
  }
  document.getElementById('td_total_1').style.display="";
  document.getElementById('td_total_2').style.display="";
  document.getElementById('td_total_3').style.display="";
  document.getElementById('td_total_4').style.display="";
  document.getElementById('td_total_5').style.display="";
  document.getElementById('td_total_6').style.display="";
	document.getElementById('td_total_7').style.display="";
  document.getElementById('td_total_8').style.display="";
 
  document.getElementById('table_display').width="635px";
 }
function make_visible_radio()
 {
  document.getElementById('tr_radio1').style.display="";
  document.getElementById('tr_radio2').style.display="none";
 }
function make_hide_radio()
 {
  document.getElementById('tr_radio1').style.display="none";
  document.getElementById('tr_radio2').style.display="";
 }
function make_visible_row()
 {
  document.getElementById('table_display').style.display="";
  document.getElementById('table_display_prepayment').style.display="none";
 }
function make_hide_row()
 {
  document.getElementById('table_display').style.display="none";
  document.getElementById('table_display_prepayment').style.display="";
 }
function make_self()
 {
  make_visible_row();
  make_it_hide();
  make_it_hide_enc_pay();
  document.getElementById('radio_type_of_payment_self1').checked=true;
  cursor_pointer();
 }
function make_insurance()
 {
  make_visible_row();
  make_it_hide();
  cursor_pointer();
  document.getElementById('radio_type_of_payment1').checked=true;
 }
 //make_self();
</script>

</head>

<body class="body_top" onunload='imclosing()' onLoad="cursor_pointer();">
<center>

<form method='post' id='remainingBillPayment' action='front_payment.php<?php if ($payid) echo "?payid=$payid"; ?>' onsubmit='return validate();'>
<input type='hidden' name='form_pid' value='<?php echo attr($pid) ?>' />


<table border='0' cellspacing='0' cellpadding="0">

 <tr height="10">
 	<td colspan="3">&nbsp;</td>
 </tr>

 <tr>
  <td colspan='3' align='center' class='text' >
   <b><?php echo htmlspecialchars(xl('Accept Payment for'), ENT_QUOTES); ?>&nbsp;:&nbsp;&nbsp;<?php echo htmlspecialchars($patdata['fname'], ENT_QUOTES) . " " .
    htmlspecialchars($patdata['lname'], ENT_QUOTES) . " " .htmlspecialchars($patdata['mname'], ENT_QUOTES). " (" . htmlspecialchars($patdata['pid'], ENT_QUOTES) . ")" ?></b>
	<?php $NameNew=$patdata['fname'] . " " .$patdata['lname']. " " .$patdata['mname'];?>
  </td>
 </tr>

 <tr height="15"><td colspan='3'></td></tr>


 <tr>
  <td class='text' >
   <?php echo xlt('Payment Method'); ?>:
  </td>
  <td colspan='2' >
  <select name="form_method" id="form_method"  class="text" onChange='CheckVisible("yes")'>
  <?php
  $query1112 = "SELECT * FROM list_options where list_id=?  ORDER BY seq, title ";
  $bres1112 = sqlStatement($query1112,array('payment_method'));
  while ($brow1112 = sqlFetchArray($bres1112)) 
   {
  	if($brow1112['option_id']=='electronic' || $brow1112['option_id']=='bank_draft' || $brow1112['option_id']=='authorize_net')
	continue;
	$selected = '';
	if($brow1112['option_id'] == 'cash'){
		$selected = 'selected = "selected"';
	}
	echo "<option value='".htmlspecialchars($brow1112['option_id'], ENT_QUOTES)."' ".$selected.">".htmlspecialchars(xl_list_label($brow1112['title']), ENT_QUOTES)."</option>";
   }
  ?>
  </select>
  </td>
 </tr>
 
 <tr height="5"><td colspan='3'></td></tr>

 <tr>
  <td class='text' >
   <?php echo xla('Check/Ref Number'); ?>:
  </td>
  <td colspan='2' ><div id="ajax_div_patient" style="display:none;"></div>
   <input type='text'  id="check_number" name='form_source' style="width:120px" disabled="disabled" value='<?php echo htmlspecialchars($payrow['source'], ENT_QUOTES); ?>'>
  </td>
 </tr>
 <tr height="5"><td colspan='3'></td></tr>

 <tr>
  <td class='text' valign="middle" >
   <?php echo htmlspecialchars(xl('Patient Coverage'), ENT_QUOTES); ?>:
  </td>
  <td class='text' colspan="2" ><input type="radio" name="radio_type_of_coverage" id="radio_type_of_coverage1" value="self"  checked="checked" /><?php echo htmlspecialchars(xl('Self'), ENT_QUOTES); ?><!--<input type="radio" name="radio_type_of_coverage" id="radio_type_of_coverag2" value="insurance"  checked="checked" onClick="make_hide_radio();make_insurance();"/><?php echo htmlspecialchars(xl('Insurance'), ENT_QUOTES); ?>-->  </td>
 </tr>

 <tr height="5"><td colspan='3'></td></tr>

 <tr id="tr_radio1"><!-- For radio Insurance -->
  <td class='text' valign="top"  >
   <?php echo htmlspecialchars(xl('Payment against'), ENT_QUOTES); ?>:
  </td>
  <td class='text' colspan="2" ><input type="radio" name="radio_type_of_payment" id="radio_type_of_payment_self1" value="cash"  checked="checked" onClick="make_visible_row();cursor_pointer();"/><?php echo htmlspecialchars(xl('Invoice Balance'), ENT_QUOTES); ?><input type="radio" name="radio_type_of_payment" id="radio_type_of_payment_self2" value="pre_payment" onClick="make_visible_row();cursor_pointer();"/><?php echo htmlspecialchars(xl('Advance Pay'), ENT_QUOTES); ?></td>
 </tr>
 <!--<tr id="tr_radio2"> For radio self 
  <td class='text' valign="top" >
   <?php echo htmlspecialchars(xl('Payment against'), ENT_QUOTES); ?>:
  </td>
  <td class='text' colspan="2" ><input type="radio" name="radio_type_of_payment" id="radio_type_of_payment1" value="copay" checked="checked" onClick="make_visible_row();cursor_pointer();"/><?php echo htmlspecialchars(xl('Co Pay'), ENT_QUOTES); ?><input type="radio" name="radio_type_of_payment" id="radio_type_of_payment2" value="invoice_balance"  onClick="make_visible_row();"/><?php echo htmlspecialchars(xl('Invoice Balance'), ENT_QUOTES); ?><br/><input type="radio" name="radio_type_of_payment" id="radio_type_of_payment4" value="pre_payment" onClick="make_hide_row();"/><?php echo htmlspecialchars(xl('Pre Pay'), ENT_QUOTES); ?></td>
 </tr>-->

 <tr height="15"><td colspan='3'></td></tr>

</table>
<table width="200" border="0" cellspacing="0" cellpadding="0" id="table_display_prepayment" style="display:none">
  <tr>
    <td class='detail'><?php echo htmlspecialchars(xl('Pre Payment'), ENT_QUOTES); ?></td>
    <td><input type='text' name='form_prepayment' style='width:100px' /></td>
  </tr>
</table>

<table border='0' id="table_display" cellpadding='0' cellspacing='0' width='70%'>
 <tr bgcolor="#cccccc" id="tr_head">
  <td class="dehead" width="70">
   <?php echo htmlspecialchars( xl('DOS'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" width="65">
   <?php echo htmlspecialchars( xl('Treatment'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="80" id="td_head_total_charge" >
   <?php echo htmlspecialchars( xl('Total Charge'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="70" id="td_head_rep_doc" style='display:none'>
   <?php echo htmlspecialchars( xl('Report/ Form'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="200" id="td_head_description" style='display:none'>
   <?php echo htmlspecialchars( xl('Description'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="80" id="td_head_insurance_payment" style='display:none'>
   <?php echo htmlspecialchars( xl('Insurance Payment'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="80" id="td_head_patient_payment" >
   <?php echo htmlspecialchars( xl('Patient Payment'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="55" id="td_head_patient_co_pay" >
   <?php echo htmlspecialchars( xl('Discount'), ENT_QUOTES) ?>
  </td>
	<td class="dehead" align="center" width="55" id="td_head_co_pay" style='display:none' >
   <?php echo htmlspecialchars( xl('Required Co Pay'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="80" id="td_head_insurance_balance" style='display:none' >
   <?php echo htmlspecialchars( xl('Insurance Balance'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="80" id="td_head_patient_balance" >
   <?php echo htmlspecialchars( xl('Patient Balance'), ENT_QUOTES) ?>
  </td>
  <td class="dehead" align="center" width="50">
   <?php echo htmlspecialchars( xl('Paying'), ENT_QUOTES) ?>
  </td>
 </tr>

<?php
  $encs = array();

  // Get the unbilled service charges and payments by encounter for this patient.
  //
  /*echo $queryecho  = "SELECT fe.encounter, b.code_type, b.code, b.modifier, b.fee, " .
    "LEFT(fe.date, 10) AS encdate ,fe.last_level_closed " .
    "FROM  form_encounter AS fe left join billing AS b  on " .
    "b.pid = $pid AND b.activity = 1  AND " .//AND b.billed = 0
    "b.code_type != 'TAX' AND b.fee != 0 " .
    "AND fe.pid = b.pid AND fe.encounter = b.encounter " .
	"where fe.pid = $pid " .
    "ORDER BY b.encounter";*/
  $query = "SELECT fe.encounter, b.code_type, b.code, b.modifier, b.fee, " .
    "LEFT(fe.date, 10) AS encdate ,fe.last_level_closed " .
    "FROM  form_encounter AS fe left join billing AS b  on " .
    "b.pid = ? AND b.activity = 1  AND " .//AND b.billed = 0
    "b.code_type != 'TAX' AND b.fee != 0 " .
    "AND fe.pid = b.pid AND fe.encounter = b.encounter " .
	"where fe.pid = ? " .
    "ORDER BY b.encounter";
  $bres = sqlStatement($query,array($pid,$pid));
  //
  while ($brow = sqlFetchArray($bres)) {
    $key = 0 - $brow['encounter'];
    if (empty($encs[$key])) {
      $encs[$key] = array(
        'encounter' => $brow['encounter'],
        'date' => $brow['encdate'],
        'last_level_closed' => $brow['last_level_closed'],
        'charges' => 0,
        'payments' => 0);
    }
    if ($brow['code_type'] === 'COPAY') {
      //$encs[$key]['payments'] -= $brow['fee'];
    } else {
      $encs[$key]['charges']  += $brow['fee'];
      // Add taxes.
      $sql_array=array();
      $query = "SELECT taxrates FROM codes WHERE " .
        "code_type = ? AND " .
        "code = ? AND ";
      array_push($sql_array,$code_types[$brow['code_type']]['id'],$brow['code']);
      if ($brow['modifier']) {
        $query .= "modifier = ?";
        array_push($sql_array,$brow['modifier']);
      } else {
        $query .= "(modifier IS NULL OR modifier = '')";
      }
      $query .= " LIMIT 1";
      $trow = sqlQuery($query,$sql_array);
      $encs[$key]['charges'] += calcTaxes($trow, $brow['fee']);
    }
  }

  // Do the same for unbilled product sales.
  //
 $query = "SELECT fe.encounter, s.drug_id, s.fee, " .
    "LEFT(fe.date, 10) AS encdate,fe.last_level_closed " .
    "FROM form_encounter AS fe left join drug_sales AS s " .
    "on s.pid = ? AND s.fee != 0 " .//AND s.billed = 0 
    "AND fe.pid = s.pid AND fe.encounter = s.encounter " .
	"where fe.pid = ? " .
    "ORDER BY s.encounter";

  $dres = sqlStatement($query,array($pid,$pid));
  //
  while ($drow = sqlFetchArray($dres)) {
    $key = 0 - $drow['encounter'];
    if (empty($encs[$key])) {
      $encs[$key] = array(
        'encounter' => $drow['encounter'],
        'date' => $drow['encdate'],
        'last_level_closed' => $drow['last_level_closed'],
        'charges' => 0,
        'payments' => 0);
    }
    $encs[$key]['charges'] += $drow['fee'];
    // Add taxes.
    $trow = sqlQuery("SELECT taxrates FROM drug_templates WHERE drug_id = ? " .
      "ORDER BY selector LIMIT 1", array($drow['drug_id']) );
    $encs[$key]['charges'] += calcTaxes($trow, $drow['fee']);
  }
/*echo '<pre>';
print_r($encs);
echo '</pre>';*/	
  ksort($encs, SORT_NUMERIC);
  $gottoday = false;
  //Bringing on top the Today always
  foreach ($encs as $key => $value) {
    $dispdate = $value['date'];
    if (strcmp($dispdate, $today) == 0 && !$gottoday) {
      $gottoday = true;
	  break;
    }
   }

  // If no billing was entered yet for today, then generate a line for
  // entering today's co-pay.
  //
  //if (! $gottoday) {
   // echoLine("form_upay[0]", date("Y-m-d"), 0, 0, 0, 0 /*$duept*/);//No encounter yet defined.
  //}

  $gottoday = false;
  
  foreach ($encs as $key => $value) {
    $enc = $value['encounter'];
    $dispdate = $value['date'];
    if (strcmp($dispdate, $today) == 0 && !$gottoday) {
      $dispdate = date("Y-m-d");
      $gottoday = true;
    }
 //------------------------------------------------------------------------------------
    $inscopay = getCopay($pid, $dispdate);
	$patcopay = getPatientCopay($pid, $enc);
//Insurance Payment
//-----------------
	$drow = sqlQuery("SELECT  SUM(pay_amount) AS payments, " .
	  "SUM(adj_amount) AS adjustments  FROM ar_activity WHERE " .
      "pid = ? and encounter = ? and " .
      "payer_type != 0 and account_code!='PCP' ",
			array($pid,$enc));
	/*echo '<pre>';
	echo 'Insurance Payment';
	print_r($drow);
	echo '</pre>';*/
	$dpayment=$drow['payments'];
	$dadjustment=$drow['adjustments'];
//Patient Payment
//---------------
	$drow = sqlQuery("SELECT  SUM(pay_amount) AS payments, " .
	  "SUM(adj_amount) AS adjustments  FROM ar_activity WHERE " .
      "pid = ? and encounter = ? and " .
      "payer_type = 0 and account_code!='PCP' ",
			array($pid,$enc));
	/*echo '<pre>';
	echo 'Patient Payment';
	print_r($drow);
	echo '</pre>';*/
	$dpayment_pat=$drow['payments'];
	$dadjustment=$drow['adjustments'];

 //------------------------------------------------------------------------------------
 //NumberOfInsurance
 	$ResultNumberOfInsurance = sqlStatement("SELECT COUNT( DISTINCT TYPE ) NumberOfInsurance FROM insurance_data
			where pid = ? and provider>0 ",array($pid));
	$RowNumberOfInsurance = sqlFetchArray($ResultNumberOfInsurance);
	$NumberOfInsurance=$RowNumberOfInsurance['NumberOfInsurance']*1;
 //------------------------------------------------------------------------------------
	$duept=0;
	if((($NumberOfInsurance==0 || $value['last_level_closed']==4 || $NumberOfInsurance== $value['last_level_closed'])))
	 {//Patient balance
	  $brow = sqlQuery("SELECT SUM(fee) AS amount FROM billing WHERE " .
	  "pid = ? and encounter = ? AND activity = 1",array($pid,$enc));
	 /* echo '<pre>';
		echo 'Billing Fee';
		print_r($brow);
	  echo '</pre>';*/
	  $srow = sqlQuery("SELECT SUM(fee) AS amount FROM drug_sales WHERE " .
	  "pid = ? and encounter = ? ",array($pid,$enc));
	  /*echo '<pre>';
		echo 'Drugs Fee';
		print_r($srow);
	  echo '</pre>';*/
	  $drow = sqlQuery("SELECT SUM(pay_amount) AS payments, " .
	  "SUM(adj_amount) AS adjustments FROM ar_activity WHERE " .
	  "pid = ? and encounter = ? ",array($pid,$enc));
	  /*echo '<pre>';
	echo 'Patient Payment';
	print_r($drow);
	echo '</pre>';*/
	 $duept= $brow['amount'] + $srow['amount'] - $drow['payments'] - $drow['adjustments'];
	 }
	$enCodeData = '';
	//  if(!empty($encounter)) {
		$enCodeQuery = "SELECT * FROM billing  WHERE billing.activity!=0 AND billing.pid =$pid AND encounter=$enc ORDER BY billing.code,billing.modifier";		
		$enCoderes = sqlStatement($enCodeQuery);
		while ($drow = sqlFetchArray($enCoderes)) {
			if(!empty($enCodeData)) {
				$enCodeData .= '<br>'.$drow['code_text'];
			}else {
				$enCodeData = $drow['code_text'];
			}
		}
	//  }
    echoLine("form_upay[$enc]", $dispdate, $value['charges'],
      $dpayment_pat, $dpayment, $duept,$enCodeData,$inscopay,$dadjustment);
  }


  // Now list previously billed visits.

  if ($INTEGRATED_AR) {

 } // end $INTEGRATED_AR
  else {
    $query = "SELECT ar.id, ar.invnumber, ar.amount, ar.paid, " .
      "ar.intnotes, ar.notes, ar.shipvia, " .
      "(SELECT SUM(invoice.sellprice * invoice.qty) FROM invoice WHERE " .
      "invoice.trans_id = ar.id AND invoice.sellprice > 0) AS charges, " .
      "(SELECT SUM(invoice.sellprice * invoice.qty) FROM invoice WHERE " .
      "invoice.trans_id = ar.id AND invoice.sellprice < 0) AS adjustments, " .
      "(SELECT SUM(acc_trans.amount) FROM acc_trans WHERE " .
      "acc_trans.trans_id = ar.id AND acc_trans.chart_id = ? " .
      "AND acc_trans.source NOT LIKE 'Ins%') AS ptpayments " .
      "FROM ar WHERE ar.invnumber LIKE ? AND " .
      "ar.amount != ar.paid " .
      "ORDER BY ar.invnumber";
    $ires = SLQuery($query, array($chart_id_cash,$pid."%") );
    if ($sl_err) die($sl_err);
    $num_invoices = SLRowCount($ires);

    for ($ix = 0; $ix < $num_invoices; ++$ix) {
      $irow = SLGetRow($ires, $ix);

      // Get encounter ID and date of service.
      list($patient_id, $enc) = explode(".", $irow['invnumber']);
      $tmp = sqlQuery("SELECT LEFT(date, 10) AS encdate FROM form_encounter " .
        "WHERE encounter = ?", array($enc) );
      $svcdate = $tmp['encdate'];

      // Compute $duncount as in sl_eob_search.php to determine if
      // this invoice is at patient responsibility.
      $duncount = substr_count(strtolower($irow['intnotes']), "statement sent");
      if (! $duncount) {
        $insgot = strtolower($irow['notes']);
        $inseobs = strtolower($irow['shipvia']);
        foreach (array('ins1', 'ins2', 'ins3') as $value) {
          if (strpos($insgot, $value) !== false &&
              strpos($inseobs, $value) === false)
            --$duncount;
        }
      }

      $inspaid = $irow['paid'] + $irow['ptpayments'] - $irow['adjustments'];
      $balance = $irow['amount'] - $irow['paid'];
      $duept  = ($duncount < 0) ? 0 : $balance;

      echoLine("form_bpay[$enc]", $svcdate, $irow['charges'],
        0 - $irow['ptpayments'], $inspaid, $duept);
    }
  } // end not $INTEGRATED_AR

  // Continue with display of the data entry form.
?>

 <tr bgcolor="#cccccc">
  <td class="dehead" id='td_total_1'></td>
  <td class="dehead" id='td_total_2'></td>
  <td class="dehead" id='td_total_3'></td>
  <td class="dehead" id='td_total_4'></td>
  <td class="dehead" id='td_total_5'></td>
  <!--<td class="dehead" id='td_total_6'></td>
	<td class="dehead" id='td_total_7'></td>
  <td class="dehead" id='td_total_8'></td>-->
  <td class="dehead" align="right">
   <?php echo htmlspecialchars( xl('Total'), ENT_QUOTES);?>
  </td>
  <td class="dehead" align="right">
   <input type='text' name='form_paytotal'  value=''
    style='color:#00aa00;width:50px' readonly  />
  </td>
 </tr>

</table>

<p>
<input type='submit' name='form_save' value='<?php echo htmlspecialchars( xl('Generate Invoice'), ENT_QUOTES);?>' /> &nbsp;
<input type='button' value='<?php echo xla('Cancel'); ?>' onclick='window.close()' />

<input type="hidden" name="hidden_patient_code" id="hidden_patient_code" value="<?php echo attr($pid);?>"/>
<input type='hidden' name='ajax_mode' id='ajax_mode' value='' />
<input type='hidden' name='mode' id='mode' value='' />
</form>
<script language="JavaScript">
 calctotal();
</script>
</center>
</body>

<?php
}
if (!$INTEGRATED_AR) SLClose();
?>
</html>
