<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

 require_once("../../globals.php");
 require_once("$srcdir/patient.inc");
 require_once("$srcdir/formatting.inc.php");
  require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");
// Prepare a string for CSV export.
function qescape($str) {
  $str = str_replace('\\', '\\\\', $str);
  return str_replace('"', '\\"', $str);
}

?>
<html>
<head>
<?php html_header_show();?>
<title><?php xl('Patient List','e'); ?></title>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/textformat.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/jquery.searchabledropdown-1.0.8.min.js"></script>
<script language="JavaScript">
 var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
 
 function showClickedData(data, fieldId){
	$("#"+fieldId).val(data);
 }
 function allowPositiveNumber(e) {
	var charCode = (e.which) ? e.which : event.keyCode

	if (charCode > 31 && (charCode <= 46 || charCode > 57 )) {
		return false;
	}
	return true;
 
}
function getRelatedState(data, fieldId){
	if(data.length > 0){
		$.ajax({
			url:"get_country_state.php?countryId="+data,
			success:function(result){
				$("#"+fieldId).html(result);
			}
		});
	}
 }
 
 function enableTextBox(fieldId){
	$("#"+fieldId).show();
 }
</script>
<script type="text/javascript">
function submitform() {
	//alert(unanswered.length +'=>'+answered.length);
	if (document.forms[0].patient_name.value.length>0 && document.forms[0].age.value.length>0 && document.forms[0].next_to_kin.value.length>0 && document.forms[0].next_to_kin_contact.value.length>0 && validatePhoneCell(document.forms[0].next_to_kin_contact.value)) {
		//top.restoreSession();
		//alert('All fields data entered');
		//return false;
		document.forms[0].submit();
	} else {
		if (document.forms[0].patient_name.value.length<=0)
		{
			document.forms[0].patient_name.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please enter the patient name','e');?>");
			document.forms[0].patient_name.focus();
			return false;
		}
		if (document.forms[0].age.value.length<=0)
		{
			document.forms[0].age.style.backgroundColor="red";
			alert("<?php echo xl('Required field missing: Please enter age.'); ?>");
			document.forms[0].age.focus();
			return false;
		}
		if (document.forms[0].next_to_kin.value.length<=0)
		{
			document.forms[0].next_to_kin.style.backgroundColor="red";
			alert("<?php echo xl('Required field missing: Please enter alternate contact name.'); ?>");
			document.forms[0].next_to_kin.focus();
			return false;
		}
		/*if (document.forms[0].contact_relationship.value.length<=0)
		{
			document.forms[0].contact_relationship.style.backgroundColor="red";
			alert("<?php //echo xl('Required field missing: Please enter next to kin relation ship.'); ?>");
			document.forms[0].contact_relationship.focus();
			return false;
		}
                */
		if (document.forms[0].next_to_kin_contact.value.length<=0)
		{
			document.forms[0].next_to_kin_contact.style.backgroundColor="red";
			alert("<?php echo xl('Required field missing: Please enter alternate contact no..'); ?>");
			document.forms[0].next_to_kin_contact.focus();
			return false;
		}else if(!validatePhoneCell(document.forms[0].next_to_kin_contact.value)) {
			document.forms[0].next_to_kin_contact.style.backgroundColor="red";
			alert("<?php echo xl('Entered alternate contact no. not valid.'); ?>");
			document.forms[0].next_to_kin_contact.focus();
			return false;	
		}
	}
}

</script>
<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<style type="text/css">
    .imgloader {
    left:40%;top:40%;
    width: 90px;
    height: 90px;
    background: url("../../../images/ajax-loader.gif") center no-repeat;
    position:absolute;
    display: none;
}
.ui-widget-overlay {
    height: 100%;display: none;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    background: none repeat scroll 0 0 #000000 !important;
    opacity: 0.75 !important;
    z-index: 1000;
}
/* specifically include & exclude from printing */
@media print {
    #report_parameters {
        visibility: hidden;
        display: none;
    }
    #report_parameters_daterange {
        visibility: visible;
        display: inline;
		margin-bottom: 10px;
    }
    #report_results table {
       margin-top: 0px;
    }
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
	#report_results {
		width: 100%;
	}
}

</style>

</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<h3 class='emrh3'><?php xl('Merge','e'); ?> - <?php xl('Patients','e'); ?></h3>

<div id="report_parameters_daterange" >
<?php echo date("d F Y", strtotime($form_from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($form_to_date)); ?>
</div>
<?php if(!empty($_SESSION['msg'])) {?>
<div style="text-align:center"><?php echo $_SESSION['msg']; $_SESSION['msg']='';?></div>
<?php }?>
<form name='theform' id='theform' method='post' action='merge_patient_data.php'>

<div id="report_parameters" class="emrform">

<input type='hidden' name='mode' id='mode' value='merge_patient'/>

<table>
 <tr>
  <td width='90%'>
	<div >

	<table class='text'>
		<tr>
			<td style='width:130px;float:left'>
			<?php xl('Patient 1','e'); ?>:
			</td>
			<td style='width:200px;margin-right:20px;float:left'>
				<input class="emrdate" type='text' id="form_patient1" name="form_patient1">
             </td>
			<td style='width:130px;float:left'>
			<?php xl('Patient 2','e'); ?>:
			</td>
			<td style='width:200px;margin-right:20px;float:left'>
				<input class="emrdate" type='text' id="form_patient2" name="form_patient2">
             </td>
		</tr>
	</table>

	</div>

  </td>
  <td align='right' valign='middle' height="100%">
	<table style='text-align:right;width:100%; height:100%' >
		<tr>
			<td>
				<div style='margin-left:15px; display:none;' id="mergePatientDataButton">
					<a href='#' class='btn btn-warning btn-sm' onclick='return submitform();'>
					<span>
						<?php xl('Merge Patients','e'); ?>
					</span>
					</a>					
				</div>
                            
                            <div class='btn btn-warning btn-sm' style='margin-left:15px; display:none;' id="mergePatientDataButton1">
					<a href='#' class='btn btn-warning btn-sm' onclick='submitform1();'>
					<span>
						<?php xl('Show Both Patients','e'); ?>
					</span>
					</a>					
				</div>
			</td>
		</tr>
	</table>
  </td>
 </tr>
</table>
</div> <!-- end of parameters -->
  <!-- START: loader -->
    <div class="imgloader"></div>
    <div class="ui-widget-overlay"></div>
<div id="report_results">

</div> <!-- end of results -->

</form>
</body>
</html>
<script>
	$( document ).ready(function() {
		$("#form_patient1").searchable();
		$("#form_patient2").searchable();
		$("#form_patient1").change(function(){
			var form_patient1 = $("#form_patient1").val();
			var form_patient2 = $("#form_patient2").val();
			$.ajax({
				url:"get_patient_status.php?selected="+form_patient1+"&notinlist="+'form_patient1',
				beforeSend:function () {
								
								$('.imgloader').show();
								$(".ui-widget-overlay").show();
							},
                                 success:function(result){
					$("#form_patient2").html(result);
                                        $('.imgloader').hide();
                                        $(".ui-widget-overlay").hide();
				}
			});
			if(form_patient1.length > 0 && form_patient2.length > 0){
				$.ajax({
					url:"get_both_patient_data.php?form_patient1="+form_patient1+"&form_patient2="+form_patient2,
					success:function(result){
						$("#report_results").html(result);
					}
				});
			}
		});
		$("#form_patient2").change(function(){
			var form_patient1 = $("#form_patient1").val();
			var form_patient2 = $("#form_patient2").val();
			$.ajax({
				url:"get_patient_status.php?selected="+form_patient2+"&notinlist="+'form_patient2',
                                beforeSend:function () {
								
								$('.imgloader').show();
								$(".ui-widget-overlay").show();
							},
				success:function(result){
					//$("#form_patient1").html(result);
                                        $("#report_results").html(result);
                                        $('.imgloader').hide();
					$(".ui-widget-overlay").hide();
				}
			});
                            
			if(form_patient1.length > 0 && form_patient2.length > 0){
				$.ajax({
					url:"get_both_patient_data.php?form_patient1="+form_patient1+"&form_patient2="+form_patient2,
					success:function(result){
						$("#report_results").html(result);
						$("#mergePatientDataButton").show();
					}
				});
			}
		});
	});
</script>