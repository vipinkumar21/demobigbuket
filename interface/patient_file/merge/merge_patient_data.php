<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

require_once("../../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
/*echo '<pre>';
print_r($_POST);
echo '</pre>';
die();*/
if(isset($_POST['mode']) && $_POST['mode']=='merge_patient' && isset($_POST['patient_id']) && !empty($_POST['patient_id'])){
	
        //----------------------get patient Id according to pubpid-----------start-------------------
        $patid1 = "SELECT id FROM patient_data WHERE pubpid = '".$_POST['form_patient1']."'";
        $patId1res = sqlStatement($patid1);
        $patId1row = sqlFetchArray($patId1res);

        $patid2 = "SELECT id FROM patient_data WHERE pubpid = '".$_POST['form_patient2']."'";
        $patId2res = sqlStatement($patid2);
        $patId2row = sqlFetchArray($patId2res);
        //----------------------get patient Id according to pubpid--------end----------------------
        
        $patientID = $_POST['patient_id'];
	$firstPatientID = $patId1row['id'];
	$scondPatientID = $patId2row['id'];
	$removePatientID = '';
         
 //       echo "first - $firstPatientID , 2nd - $scondPatientID , merge-  $patientID";
   //     exit;
	if($patientID == $firstPatientID){
		$removePatientID = $scondPatientID;
	} else if($patientID == $scondPatientID) {
		$removePatientID = $firstPatientID;
	}
	if($patientID == $firstPatientID || $patientID == $scondPatientID){
		sqlStatement("update patient_data set
			`fname` = '".$_POST['patient_name']."',
			`mname` = '',
			`lname` = '',
			`pubpid` = '".$_POST['patient_pubpid']."',
			`sex` = '".$_POST['sex']."',
			`DOB` = '".$_POST['DOB']."',
			`age` = '".$_POST['age']."',
			`country_code` = '".$_POST['country_code']."',
			`email` = '".$_POST['patient_email']."',
			`phone_home` = '".$_POST['phone_home']."',
			`phone_cell` = '".$_POST['phone_cell']."',
			`occupation` = '".$_POST['occupation']."',
			`street` = '".$_POST['street']."',
			`state` = '".$_POST['state']."',
			`city` = '".$_POST['city']."',
			`postal_code` = '".$_POST['postal_code']."',
			`next_to_kin` 		= '".$_POST['next_to_kin']."',
			`contact_relationship` 	= '".$_POST['contact_relationship']."',
			`next_to_kin_contact` 		= '".$_POST['next_to_kin_contact']."',
			`ref_doctor` 			= '".$_POST['ref_doctor']."',
			`ref_doctor_contact` 	= '".$_POST['ref_doctor_contact']."',
			`group`					= '".$_POST['group']."',
			`ptntnts` 				= '".$_POST['ptntnts']."'    
		where `id`='" . $patientID . "'" );
		
		sqlStatement("DELETE FROM checkup_data where cd_pid=".$firstPatientID);
		sqlStatement("DELETE FROM checkup_data where cd_pid=".$scondPatientID);
		$checkTypeQuery = "SELECT * from checkup_type";
		$checkTypeRes = sqlStatement($checkTypeQuery);
		while ($checkTypeRow = sqlFetchArray($checkTypeRes)) {
			$checkConfigQuery = "SELECT * from checkup_config WHERE cc_ct_id=".$checkTypeRow['ct_id'];
			$checkConfigRes = sqlStatement($checkConfigQuery);
			while ($checkConfigRow = sqlFetchArray($checkConfigRes)) {
				if(1 == $checkTypeRow['ct_id']){
					$issue = sqlInsert("INSERT INTO checkup_data ( " .
						"cd_pid, cd_cc_id, cd_ct_id, cd_radio ".
						") VALUES ( " .
						"'" . add_escape_custom($patientID) . "', " .
						"'" . add_escape_custom($checkTypeRow['ct_id'])			. "', " .
						"'" . add_escape_custom($checkConfigRow['cc_id'])			. "', " .
						"'" . add_escape_custom($_POST['medicalHistory_'.$checkConfigRow['cc_id']])       . "' " .
						")");
				}else {
					$issue = sqlInsert("INSERT INTO checkup_data ( " .
						"cd_pid, cd_cc_id, cd_ct_id, cd_radio, cd_text1 ".
						") VALUES ( " .
						"'" . add_escape_custom($patientID) . "', " .
						"'" . add_escape_custom($checkTypeRow['ct_id'])			. "', " .
						"'" . add_escape_custom($checkConfigRow['cc_id'])			. "', " .
						"'" . add_escape_custom($_POST['medicalHistory_'.$checkConfigRow['cc_id']])       . "', " .
						"'" . add_escape_custom($_POST['medicalHistoryData_'.$checkConfigRow['cc_id']])       . "' " .
						")");
				}
			}
		}
		sqlStatement("update chiefcomplaints set `cc_pid` = '".$patientID."' where cc_pid='" . $removePatientID . "'" );
		sqlStatement("update oral_exams set `oe_pid` = '".$patientID."' where oe_pid='" . $removePatientID . "'" );
		sqlStatement("update diagnosistics set `d_pid` = '".$patientID."' where d_pid='" . $removePatientID . "'" );
		sqlStatement("update billing set `pid` = '".$patientID."' where pid='" . $removePatientID . "'" );
		sqlStatement("update patient_prescriptions set `pp_pid` = '".$patientID."' where pp_pid='" . $removePatientID . "'" );
		sqlStatement("update openemr_postcalendar_events set `pc_pid` = '".$patientID."' where pc_pid='" . $removePatientID . "'" );
		sqlStatement("update documents set `foreign_id` = '".$patientID."' where foreign_id='" . $removePatientID . "'" );
		sqlStatement("update patient_facilities set `pf_pid` = '".$patientID."' where pf_pid='" . $removePatientID . "'" );
		sqlStatement("update invoice set `inv_pid` = '".$patientID."' where inv_pid='" . $removePatientID . "'" );
		sqlStatement("update patient_facilities set `pf_pid` = '".$patientID."' where pf_pid='" . $removePatientID . "'" );
		$rmPatientAmQuery = "SELECT * FROM patient_amount WHERE pa_pid = '".$removePatientID."'";
		$rmPatientAmRes = sqlStatement($rmPatientAmQuery);
		$rmPatientAmRow = sqlFetchArray($rmPatientAmRes);
		if(!empty($rmPatientAmRow)) {
			sqlStatement("update patient_amount set `pa_dues` = pa_dues+".$rmPatientAmRow['pa_dues'].", `pa_advance` = pa_advance+".$rmPatientAmRow['pa_advance']." where pa_pid='" . $patientID . "'" );
			sqlStatement("DELETE FROM patient_amount WHERE pa_pid = '".$removePatientID."'" );
		}
		sqlStatement("update reciept set `rect_pid` = '".$patientID."' where rect_pid='" . $removePatientID . "'" );
		//Start subscribers Merge
                $rpRetSubsId = findSubsDetailsForMerge($removePatientID);
                $cpRetSubsId = findSubsDetailsForMerge($patientID);
                sqlStatement("DELETE FROM subscribers WHERE subs_pid = '".$removePatientID."'");
                sqlStatement("UPDATE subscribers SET `subs_p_pubid` = '".$_POST['patient_pubpid']."',`subs_name` = '".$_POST['patient_name']."', `subs_gender` = '".$_POST['sex']."', `subs_email` = '".$_POST['patient_email']."', `subs_dob` = '".$_POST['DOB']."', `subs_mobile` = '".$_POST['phone_cell']."', `subs_other_phone` = '".$_POST['phone_cell']."' WHERE subs_pid='" . $patientID . "'");
                sqlStatement("update `subscriber_calls` set `scalls_subs_id` = '".$cpRetSubsId."' where `scalls_subs_id` = '" . $rpRetSubsId . "'" );
                sqlStatement("update `subscriber_documents` set `subsdoc_subscribers_id` = '".$cpRetSubsId."' where `subsdoc_subscribers_id` = '" . $rpRetSubsId . "'" );
                //End subscribers Merge
                sqlStatement("DELETE FROM patient_data WHERE id = '".$removePatientID."'" );
		$_SESSION['msg'] = 'Patients data merged successfully.';
                ?>
		<script>
			window.location.href='merge.php';
		</script>
		<?php
	}else {
		$_SESSION['msg'] = 'Patient data not merged. Please try again.';
		?>
		<script>
			window.location.href='merge.php';
		</script>
		<?php
	}
}
/*
 * @author: Saurabh.kumar
 * @desc: return subscriber id(PK) accordingly PatientId
 */
function findSubsDetailsForMerge($subsPid){
    $query = "SELECT `id` from `subscribers` WHERE subs_pid=".$subsPid;
        $queryRes = sqlStatement($query);
        $resSet = sqlFetchArray($queryRes);
        return $resSet['id'];
}

?>