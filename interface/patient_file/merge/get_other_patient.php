<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

require_once("../../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
$selected = '';
if(!empty($_GET['selected'])){
	$selected = $_GET['selected'];
}
if(!empty($_GET['notinlist'])){
	$query = "SELECT id, CONCAT_WS(' ', fname, lname, pubpid) AS name FROM patient_data WHERE id != '".$_GET['notinlist']."' ORDER BY name";
}else{
	$query = "SELECT id, CONCAT_WS(' ', fname, lname, pubpid) AS name FROM patient_data ORDER BY name";
}
$fres = sqlStatement($query);
$returnData ="<option value=''>-- Select Patient --</option>\n";
while ($frow = sqlFetchArray($fres)) {
	$facility_id = $frow['id'];
	$option_value = htmlspecialchars($facility_id, ENT_QUOTES);
	$option_content = htmlspecialchars($frow['name'], ENT_NOQUOTES);
	$option_selected_attr = '';
	if ($selected == $facility_id) {
      $option_selected_attr = ' selected="selected"';
      $have_selected = true;
    }
	$returnData.= "    <option value=\"$option_value\" $option_selected_attr>$option_content</option>\n";
}
echo $returnData;
?>