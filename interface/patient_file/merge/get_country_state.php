<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

require_once("../../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
$selected = '';
$countySel = $_GET['countryId'];
$stateQuery = "SELECT state_id, name FROM state WHERE country_id=".$countySel." ORDER BY name";
$stateRes = sqlStatement($stateQuery);
$returnData ="<option value='pleaseSelect'>-- Select State --</option>\n";
while ($stateRow = sqlFetchArray($stateRes)) {
	$state_id = $stateRow['state_id'];
	$option_value = htmlspecialchars($state_id, ENT_QUOTES);
	$option_content = htmlspecialchars($stateRow['name'], ENT_NOQUOTES);
	$option_selected_attr = '';
	if ($selected == $state_id) {
      $option_selected_attr = ' selected="selected"';
      $have_selected = true;
    }
	$returnData.= "    <option value=\"$option_value\" $option_selected_attr>$option_content</option>\n";
}
echo $returnData;
?>