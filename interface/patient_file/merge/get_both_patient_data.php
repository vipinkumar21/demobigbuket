<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

require_once("../../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
$selected = '';
if(!empty($_GET['selected'])){
	$selected = $_GET['selected'];
}

//----------------------get patient Id according to pubpid-----------start-------------------
$patid1 = "SELECT id FROM patient_data WHERE pubpid = '".$_GET['form_patient1']."'";
$patId1res = sqlStatement($patid1);
$patId1row = sqlFetchArray($patId1res);

$patid2 = "SELECT id FROM patient_data WHERE pubpid = '".$_GET['form_patient2']."'";
$patId2res = sqlStatement($patid2);
$patId2row = sqlFetchArray($patId2res);
//----------------------get patient Id according to pubpid--------end----------------------
if($patId1row['id'] == $patId2row['id']){    
echo "Both Patient Id must be unique!"; 
}
else if($patId1row['id'] and $patId2row['id'] and $patId1row['id'] !== $patId2row['id']){
$patient1query = "SELECT * FROM patient_data WHERE id = '".$patId1row['id']."'";
$patient1res = sqlStatement($patient1query);
$patient1row = sqlFetchArray($patient1res);
$patient2query = "SELECT * FROM patient_data WHERE id = '".$patId2row['id']."'";
$patient2res = sqlStatement($patient2query);
$patient2row = sqlFetchArray($patient2res);
$returnData ="<table style='float:left;'>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Choose Patient For Update:</td>\n";
$returnData .="<td>";
$returnData .="<input type='radio' name='patient_id' value='".$patient1row['id']."' checked='checked'>".$patient1row['fname']." ".$patient1row['lname']."<br>\n";
$returnData .="<input type='radio' name='patient_id' value='".$patient2row['id']."'>".$patient2row['fname']." ".$patient2row['lname']."\n";
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="</table>";


/* 1 Personal Informaton */ 


$returnData .="<h3 class='emrh3'>Personal Information</h3>\n";
$returnData .="<table>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Name:</td>\n";
$returnData .="<td class='input'>";
$returnData .="<input type='text' name='patient_name' value='".$patient1row['fname']." ".$patient1row['lname']."' size='40' id='patient_name'>\n<br>";
$returnData .="<input type='radio' name='patient_name_radio' value='".$patient1row['fname']." ".$patient1row['lname']."' checked='checked' onclick='showClickedData(this.value, \"patient_name\");'>".$patient1row['fname']." ".$patient1row['lname']."<br>\n";
$returnData .="<input type='radio' name='patient_name_radio' value='".$patient2row['fname']." ".$patient2row['lname']."' onclick='showClickedData(this.value, \"patient_name\");'>".$patient2row['fname']." ".$patient2row['lname']."\n";
$returnData .="</td>\n";
$returnData .="<td class='label'>ID:</td>\n";
$returnData .="<td class='input'>";
$returnData .="<input type='radio' name='patient_pubpid' value='".$patient1row['pubpid']."' checked='checked'>".$patient1row['pubpid']."<br>\n";
$returnData .="<input type='radio' name='patient_pubpid' value='".$patient2row['pubpid']."'>".$patient2row['pubpid']."\n";
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Email:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['email']) && !empty($patient2row['email'])) || (!empty($patient1row['email']) && empty($patient2row['email']))){
	$returnData .="<input type='text' name='patient_email' value='".$patient1row['email']."' size='40' id='patient_email'>\n<br>";
}else if(empty($patient1row['email']) && !empty($patient2row['email'])) {
	$returnData .="<input type='text' name='patient_email' value='".$patient2row['email']."' size='40' id='patient_email'>\n<br>";
}else {
	$returnData .="<input type='text' name='patient_email' value='' size='40' id='patient_email'>\n<br>";
}
if(!empty($patient1row['email']) && !empty($patient2row['email'])){
	$returnData .="<input type='radio' name='patient_email_radio' value='".$patient1row['email']."' checked='checked' onclick='showClickedData(this.value, \"patient_email\");'>".$patient1row['email']."<br>\n";
	$returnData .="<input type='radio' name='patient_email_radio' value='".$patient2row['email']."' onclick='showClickedData(this.value, \"patient_email\");'>".$patient2row['email']."\n";
}
$returnData .="</td>\n";
$returnData .="<td class='label'>Gender:</td>\n";
$returnData .="<td class='input'>";
$maleChecked = '';
$femaleChecked = '';
if($patient1row['sex'] == 'Male'){
	$maleChecked = "checked=checked";
}
if($patient1row['sex'] == 'Female'){
	$femaleChecked = "checked=checked";
}
$returnData .="<input type='radio' name='sex' value='Male' ".$maleChecked.">Male &nbsp; <input type='radio' name='sex' value='Female' ".$femaleChecked.">Female\n<br>";
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Date Of Birth:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['DOB']) && !empty($patient2row['DOB'])) || (!empty($patient1row['DOB']) && empty($patient2row['DOB']))){
	$returnData .="<input type='text' name='DOB' value='".$patient1row['DOB']."' size='40' id='DOB'>\n<br>";
}else if(empty($patient1row['DOB']) && !empty($patient2row['DOB'])) {
	$returnData .="<input type='text' name='DOB' value='".$patient2row['DOB']."' size='40' id='DOB'>\n<br>";
}else {
	$returnData .="<input type='text' name='DOB' value='' size='40' id='DOB'>\n<br>";
}
if(!empty($patient1row['DOB']) && !empty($patient2row['DOB'])){
	$returnData .="<input type='radio' name='DOB_radio' value='".$patient1row['DOB']."' checked='checked' onclick='showClickedData(this.value, \"DOB\");'>".$patient1row['DOB']."<br>\n";
	$returnData .="<input type='radio' name='DOB_radio' value='".$patient2row['DOB']."' onclick='showClickedData(this.value, \"DOB\");'>".$patient2row['DOB']."\n";
}
$returnData .="</td>\n";
$returnData .="<td class='label'>Age:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['age']) && !empty($patient2row['age'])) || (!empty($patient1row['age']) && empty($patient2row['age']))){
	$returnData .="<input type='text' name='age' value='".$patient1row['age']."' size='40' id='age' onkeypress='return allowPositiveNumber(event);'>\n<br>";
}else if(empty($patient1row['age']) && !empty($patient2row['age'])) {
	$returnData .="<input type='text' name='age' value='".$patient2row['age']."' size='40' id='age' onkeypress='return allowPositiveNumber(event);'>\n<br>";
}else {
	$returnData .="<input type='text' name='age' value='' size='40' id='age' onkeypress='return allowPositiveNumber(event);'>\n<br>";
}
if(!empty($patient1row['age']) && !empty($patient2row['age'])){
	$returnData .="<input type='radio' name='age_radio' value='".$patient1row['age']."' checked='checked' onclick='showClickedData(this.value, \"age\");'>".$patient1row['age']."<br>\n";
	$returnData .="<input type='radio' name='age_radio' value='".$patient2row['age']."' onclick='showClickedData(this.value, \"age\");'>".$patient2row['age']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Mobile:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['phone_cell']) && !empty($patient2row['phone_cell'])) || (!empty($patient1row['phone_cell']) && empty($patient2row['phone_cell']))){
	$returnData .="<input type='text' name='phone_cell' value='".$patient1row['phone_cell']."' size='40' id='phone_cell'>\n<br>";
}else if(empty($patient1row['phone_cell']) && !empty($patient2row['phone_cell'])) {
	$returnData .="<input type='text' name='phone_cell' value='".$patient2row['phone_cell']."' size='40' id='phone_cell'>\n<br>";
}else {
	$returnData .="<input type='text' name='phone_cell' value='' size='40' id='phone_cell'>\n<br>";
}
if(!empty($patient1row['phone_cell']) && !empty($patient2row['phone_cell'])){
	$returnData .="<input type='radio' name='phone_cell_radio' value='".$patient1row['phone_cell']."' checked='checked' onclick='showClickedData(this.value, \"phone_cell\");'>".$patient1row['phone_cell']."<br>\n";
	$returnData .="<input type='radio' name='phone_cell_radio' value='".$patient2row['phone_cell']."' onclick='showClickedData(this.value, \"phone_cell\");'>".$patient2row['phone_cell']."\n";
}
$returnData .="</td>\n";
$returnData .="<td class='label'>Landline:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['phone_home']) && !empty($patient2row['phone_home'])) || (!empty($patient1row['phone_home']) && empty($patient2row['phone_home']))){
	$returnData .="<input type='text' name='phone_home' value='".$patient1row['phone_home']."' size='40' id='phone_home'>\n<br>";
}else if(empty($patient1row['phone_home']) && !empty($patient2row['phone_home'])) {
	$returnData .="<input type='text' name='phone_home' value='".$patient2row['phone_home']."' size='40' id='phone_home'>\n<br>";
}else {
	$returnData .="<input type='text' name='phone_home' value='' size='40' id='phone_home'>\n<br>";
}
if(!empty($patient1row['phone_home']) && !empty($patient2row['phone_home'])){
	$returnData .="<input type='radio' name='phone_home_radio' value='".$patient1row['phone_home']."' checked='checked' onclick='showClickedData(this.value, \"phone_home\");'>".$patient1row['phone_home']."<br>\n";
	$returnData .="<input type='radio' name='phone_home_radio' value='".$patient2row['phone_home']."' onclick='showClickedData(this.value, \"phone_home\");'>".$patient2row['phone_home']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Country:</td>\n";
$returnData .="<td class='input'>";
if($patient1row['country_code'] == 'pleaseSelect'){
	$patient1row['country_code'] ='';
}
if($patient2row['country_code'] == 'pleaseSelect'){
	$patient2row['country_code'] ='';
}
$countySel = '';
if(!empty($patient1row['country_code'])) {
	$countySel = $patient1row['country_code'];
}else {
	$countySel = $patient2row['country_code'];
}
$returnData .="<select id='country_code' width='260px'  name='country_code' onchange='getRelatedState(this.value, \"state\")'>";
$countryQuery = "SELECT countries_id, countries_name FROM geo_country_reference ORDER BY countries_name";
$countryRes = sqlStatement($countryQuery);
$returnData .="<option value='pleaseSelect'>-- Select Country --</option>\n";
while ($countryRow = sqlFetchArray($countryRes)) {
	$countries_id = $countryRow['countries_id'];
	$option_value = htmlspecialchars($countries_id, ENT_QUOTES);
	$option_content = htmlspecialchars($countryRow['countries_name'], ENT_NOQUOTES);
	$option_selected_attr = '';
	if ($countySel == $countries_id) {
      $option_selected_attr = ' selected="selected"';
      $have_selected = true;
    }
	$returnData.= "    <option value=\"$option_value\" $option_selected_attr>$option_content</option>\n";
}
$returnData .="</select>";
$returnData .="</td>\n";
$returnData .="<td class='label'>State:</td>\n";
$returnData .="<td class='input'>";
if($patient1row['state'] == 'pleaseSelect'){
	$patient1row['state'] ='';
}
if($patient2row['state'] == 'pleaseSelect'){
	$patient2row['state'] ='';
}
$stateSel = '';
if(!empty($patient1row['state'])) {
	$stateSel = $patient1row['state'];
}else {
	$stateSel = $patient2row['state'];
}
$returnData .="<select id='state' name='state'>";
$stateQuery = "SELECT state_id, name FROM state WHERE country_id='".$countySel."' ORDER BY name";
$stateRes = sqlStatement($stateQuery);
$returnData .="<option value='pleaseSelect'>-- Select State --</option>\n";
while ($stateRow = sqlFetchArray($stateRes)) {
	$state_id = $stateRow['state_id'];
	$option_value = htmlspecialchars($state_id, ENT_QUOTES);
	$option_content = htmlspecialchars($stateRow['name'], ENT_NOQUOTES);
	$option_selected_attr = '';
	if ($stateSel == $state_id) {
      $option_selected_attr = ' selected="selected"';
      $have_selected = true;
    }
	$returnData.= "    <option value=\"$option_value\" $option_selected_attr>$option_content</option>\n";
}
$returnData .="</select>";
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>City:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['city']) && !empty($patient2row['city'])) || (!empty($patient1row['city']) && empty($patient2row['city']))){
	$returnData .="<input type='text' name='city' value='".$patient1row['city']."' size='40' id='city'>\n<br>";
}else if(empty($patient1row['city']) && !empty($patient2row['city'])) {
	$returnData .="<input type='text' name='city' value='".$patient2row['city']."' size='40' id='city'>\n<br>";
}else {
	$returnData .="<input type='text' name='city' value='' size='40' id='city'>\n<br>";
}
if(!empty($patient1row['city']) && !empty($patient2row['city'])){
	$returnData .="<input type='radio' name='city_radio' value='".$patient1row['city']."' checked='checked' onclick='showClickedData(this.value, \"city\");'>".$patient1row['city']."<br>\n";
	$returnData .="<input type='radio' name='city_radio' value='".$patient2row['city']."' onclick='showClickedData(this.value, \"city\");'>".$patient2row['city']."\n";
}
$returnData .="</td>\n";
$returnData .="<td class='label'>Zip:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['postal_code']) && !empty($patient2row['postal_code'])) || (!empty($patient1row['postal_code']) && empty($patient2row['postal_code']))){
	$returnData .="<input type='text' name='postal_code' value='".$patient1row['postal_code']."' size='40' id='postal_code'>\n<br>";
}else if(empty($patient1row['postal_code']) && !empty($patient2row['postal_code'])) {
	$returnData .="<input type='text' name='postal_code' value='".$patient2row['postal_code']."' size='40' id='postal_code'>\n<br>";
}else {
	$returnData .="<input type='text' name='postal_code' value='' size='40' id='postal_code'>\n<br>";
}
if(!empty($patient1row['postal_code']) && !empty($patient2row['postal_code'])){
	$returnData .="<input type='radio' name='postal_code_radio' value='".$patient1row['postal_code']."' checked='checked' onclick='showClickedData(this.value, \"postal_code\");'>".$patient1row['postal_code']."<br>\n";
	$returnData .="<input type='radio' name='postal_code_radio' value='".$patient2row['postal_code']."' onclick='showClickedData(this.value, \"postal_code\");'>".$patient2row['postal_code']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Address:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['street']) && !empty($patient2row['street'])) || (!empty($patient1row['street']) && empty($patient2row['street']))){
	$returnData .="<textarea name='street'size='40' id='street'>".$patient1row['street']."</textarea>\n<br>";
}else if(empty($patient1row['street']) && !empty($patient2row['street'])) {
	$returnData .="<textarea name='street'size='40' id='street'>".$patient2row['street']."</textarea>\n<br>";
}else {
	$returnData .="<textarea name='street'size='40' id='street'></textarea>\n<br>";
}
if(!empty($patient1row['street']) && !empty($patient2row['street'])){
	$returnData .="<input type='radio' name='street_radio' value='".$patient1row['street']."' checked='checked' onclick='showClickedData(this.value, \"street\");'>".$patient1row['street']."<br>\n";
	$returnData .="<input type='radio' name='street_radio' value='".$patient2row['street']."' onclick='showClickedData(this.value, \"street\");'>".$patient2row['street']."\n";
}
$returnData .="</td>\n";
$returnData .="<td class='label'>Occupation:</td>\n";
$returnData .="<td class='input'>";
if((!empty($patient1row['occupation']) && !empty($patient2row['occupation'])) || (!empty($patient1row['occupation']) && empty($patient2row['occupation']))){
	$returnData .="<input type='text' name='occupation' value='".$patient1row['occupation']."' size='40' id='occupation'>\n<br>";
}else if(empty($patient1row['occupation']) && !empty($patient2row['occupation'])) {
	$returnData .="<input type='text' name='occupation' value='".$patient2row['occupation']."' size='40' id='occupation'>\n<br>";
}else {
	$returnData .="<input type='text' name='occupation' value='' size='40' id='occupation'>\n<br>";
}
if(!empty($patient1row['occupation']) && !empty($patient2row['occupation'])){
	$returnData .="<input type='radio' name='occupation_radio' value='".$patient1row['occupation']."' checked='checked' onclick='showClickedData(this.value, \"occupation\");'>".$patient1row['occupation']."<br>\n";
	$returnData .="<input type='radio' name='occupation_radio' value='".$patient2row['occupation']."' onclick='showClickedData(this.value, \"occupation\");'>".$patient2row['occupation']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";

$returnData .="<tr>\n";

$returnData .="<td class='label'>Alternate Contact Name :</td>\n";

$returnData .="<td class='input'>";
if((!empty($patient1row['next_to_kin']) && !empty($patient2row['next_to_kin'])) || (!empty($patient1row['next_to_kin']) && empty($patient2row['next_to_kin']))){
	$returnData .="<input type='text' name='next_to_kin' value='".$patient1row['next_to_kin']."' size='40' id='next_to_kin'>\n<br>";
}else if(empty($patient1row['next_to_kin']) && !empty($patient2row['next_to_kin'])) {
	$returnData .="<input type='text' name='next_to_kin' value='".$patient2row['next_to_kin']."' size='40' id='next_to_kin'>\n<br>";
}else {
	$returnData .="<input type='text' name='next_to_kin' value='' size='40' id='next_to_kin'>\n<br>";
}
if(!empty($patient1row['next_to_kin']) && !empty($patient2row['next_to_kin'])){
	$returnData .="<input type='radio' name='next_to_kin_radio' value='".$patient1row['next_to_kin']."' checked='checked' onclick='showClickedData(this.value, \"next_to_kin\");'>".$patient1row['next_to_kin']."<br>\n";
	$returnData .="<input type='radio' name='next_to_kin_radio' value='".$patient2row['next_to_kin']."' onclick='showClickedData(this.value, \"next_to_kin\");'>".$patient2row['next_to_kin']."\n";
}
$returnData .="</td>\n";


$returnData .="<td class='label'>Alternate Contact no.:</td>\n";

$returnData .="<td class='input'>";
if((!empty($patient1row['next_to_kin_contact']) && !empty($patient2row['next_to_kin_contact'])) || (!empty($patient1row['next_to_kin_contact']) && empty($patient2row['next_to_kin_contact']))){
	$returnData .="<input type='text' name='next_to_kin_contact' value='".$patient1row['next_to_kin_contact']."' size='40' id='next_to_kin_contact'>\n<br>";
}else if(empty($patient1row['next_to_kin_contact']) && !empty($patient2row['next_to_kin_contact'])) {
	$returnData .="<input type='text' name='next_to_kin_contact' value='".$patient2row['next_to_kin_contact']."' size='40' id='next_to_kin_contact'>\n<br>";
}else {
	$returnData .="<input type='text' name='next_to_kin_contact' value='' size='40' id='next_to_kin_contact'>\n<br>";
}
if(!empty($patient1row['next_to_kin_contact']) && !empty($patient2row['next_to_kin_contact'])){
	$returnData .="<input type='radio' name='next_to_kin_contact_radio' value='".$patient1row['next_to_kin_contact']."' checked='checked' onclick='showClickedData(this.value, \"next_to_kin_contact\");'>".$patient1row['next_to_kin_contact']."<br>\n";
	$returnData .="<input type='radio' name='next_to_kin_contact_radio' value='".$patient2row['next_to_kin_contact']."' onclick='showClickedData(this.value, \"next_to_kin_contact\");'>".$patient2row['next_to_kin_contact']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";

$returnData .="</table>";

/* 2 In case of Emergency */ 

/*
$returnData .="<h3 class='emrh3'>In Case Of Emergency</h3>\n";

$returnData .="<table>\n";



				// Start 1st Row  
$returnData .="<tr>\n";

$returnData .="<td class='label'>Alternate Contact Name :</td>\n";

$returnData .="<td class='input'>";
if((!empty($patient1row['next_to_kin']) && !empty($patient2row['next_to_kin'])) || (!empty($patient1row['next_to_kin']) && empty($patient2row['next_to_kin']))){
	$returnData .="<input type='text' name='next_to_kin' value='".$patient1row['next_to_kin']."' size='40' id='next_to_kin'>\n<br>";
}else if(empty($patient1row['next_to_kin']) && !empty($patient2row['next_to_kin'])) {
	$returnData .="<input type='text' name='next_to_kin' value='".$patient2row['next_to_kin']."' size='40' id='next_to_kin'>\n<br>";
}else {
	$returnData .="<input type='text' name='next_to_kin' value='' size='40' id='next_to_kin'>\n<br>";
}
if(!empty($patient1row['next_to_kin']) && !empty($patient2row['next_to_kin'])){
	$returnData .="<input type='radio' name='next_to_kin_radio' value='".$patient1row['next_to_kin']."' checked='checked' onclick='showClickedData(this.value, \"next_to_kin\");'>".$patient1row['next_to_kin']."<br>\n";
	$returnData .="<input type='radio' name='next_to_kin_radio' value='".$patient2row['next_to_kin']."' onclick='showClickedData(this.value, \"next_to_kin\");'>".$patient2row['next_to_kin']."\n";
}
$returnData .="</td>\n";


$returnData .="<td class='label'>Alternate Contact no.:</td>\n";

$returnData .="<td class='input'>";
if((!empty($patient1row['next_to_kin_contact']) && !empty($patient2row['next_to_kin_contact'])) || (!empty($patient1row['next_to_kin_contact']) && empty($patient2row['next_to_kin_contact']))){
	$returnData .="<input type='text' name='next_to_kin_contact' value='".$patient1row['next_to_kin_contact']."' size='40' id='next_to_kin_contact'>\n<br>";
}else if(empty($patient1row['next_to_kin_contact']) && !empty($patient2row['next_to_kin_contact'])) {
	$returnData .="<input type='text' name='next_to_kin_contact' value='".$patient2row['next_to_kin_contact']."' size='40' id='next_to_kin_contact'>\n<br>";
}else {
	$returnData .="<input type='text' name='next_to_kin_contact' value='' size='40' id='next_to_kin_contact'>\n<br>";
}
if(!empty($patient1row['next_to_kin_contact']) && !empty($patient2row['next_to_kin_contact'])){
	$returnData .="<input type='radio' name='next_to_kin_contact_radio' value='".$patient1row['next_to_kin_contact']."' checked='checked' onclick='showClickedData(this.value, \"next_to_kin_contact\");'>".$patient1row['next_to_kin_contact']."<br>\n";
	$returnData .="<input type='radio' name='next_to_kin_contact_radio' value='".$patient2row['next_to_kin_contact']."' onclick='showClickedData(this.value, \"next_to_kin_contact\");'>".$patient2row['next_to_kin_contact']."\n";
}
$returnData .="</td>\n";




/* 
$returnData .="<td class='label'>Relationship:</td>\n";
$returnData .="<td>";
if((!empty($patient1row['contact_relationship']) && !empty($patient2row['contact_relationship'])) || (!empty($patient1row['contact_relationship']) && empty($patient2row['contact_relationship']))){
	$returnData .="<input type='text' name='contact_relationship' value='".$patient1row['contact_relationship']."' size='40' id='contact_relationship'>\n<br>";
}else if(empty($patient1row['contact_relationship']) && !empty($patient2row['contact_relationship'])) {
	$returnData .="<input type='text' name='contact_relationship' value='".$patient2row['contact_relationship']."' size='40' id='contact_relationship'>\n<br>";
}else {
	$returnData .="<input type='text' name='contact_relationship' value='' size='40' id='contact_relationship'>\n<br>";
}
if(!empty($patient1row['contact_relationship']) && !empty($patient2row['contact_relationship'])){
	$returnData .="<input type='radio' name='contact_relationship_radio' value='".$patient1row['contact_relationship']."' checked='checked' onclick='showClickedData(this.value, \"contact_relationship\");'>".$patient1row['contact_relationship']."<br>\n";
	$returnData .="<input type='radio' name='contact_relationship_radio' value='".$patient2row['contact_relationship']."' onclick='showClickedData(this.value, \"contact_relationship\");'>".$patient2row['contact_relationship']."\n";
}
$returnData .="</td>\n"; */



/*
// Added Physican data 
$returnData .="</tr>\n";
				// End 1st Row  



				// Start 2nd Row  
$returnData .="<tr>\n";


// Adding physican data  
$returnData .="<td class='label'>Physician's Name:</td>\n";

$returnData .="<td class='input'>";
if((!empty($patient1row['ref_doctor']) && !empty($patient2row['ref_doctor'])) || (!empty($patient1row['ref_doctor']) && empty($patient2row['ref_doctor']))){
	$returnData .="<input type='text' name='ref_doctor' value='".$patient1row['ref_doctor']."' size='40' id='ref_doctor'>\n<br>";
}else if(empty($patient1row['ref_doctor']) && !empty($patient2row['ref_doctor'])) {
	$returnData .="<input type='text' name='ref_doctor' value='".$patient2row['ref_doctor']."' size='40' id='ref_doctor'>\n<br>";
}else {
	$returnData .="<input type='text' name='ref_doctor' value='' size='40' id='ref_doctor'>\n<br>";
}
if(!empty($patient1row['ref_doctor']) && !empty($patient2row['ref_doctor'])){
	$returnData .="<input type='radio' name='ref_doctor_radio' value='".$patient1row['ref_doctor']."' checked='checked' onclick='showClickedData(this.value, \"ref_doctor\");'>".$patient1row['ref_doctor']."<br>\n";
	$returnData .="<input type='radio' name='ref_doctor_radio' value='".$patient2row['ref_doctor']."' onclick='showClickedData(this.value, \"ref_doctor\");'>".$patient2row['ref_doctor']."\n";
}
$returnData .="</td>\n";

$returnData .="<td class='label'>Physician's Contact:</td>\n";

$returnData .="<td class='input'>";
if((!empty($patient1row['ref_doctor_contact']) && !empty($patient2row['ref_doctor_contact'])) || (!empty($patient1row['ref_doctor_contact']) && empty($patient2row['ref_doctor_contact']))){
	$returnData .="<input type='text' name='ref_doctor_contact' value='".$patient1row['ref_doctor_contact']."' size='40' id='ref_doctor_contact'>\n<br>";
}else if(empty($patient1row['ref_doctor_contact']) && !empty($patient2row['ref_doctor_contact'])) {
	$returnData .="<input type='text' name='ref_doctor_contact' value='".$patient2row['ref_doctor_contact']."' size='40' id='ref_doctor_contact'>\n<br>";
}else {
	$returnData .="<input type='text' name='ref_doctor_contact' value='' size='40' id='ref_doctor_contact'>\n<br>";
}
if(!empty($patient1row['ref_doctor_contact']) && !empty($patient2row['ref_doctor_contact'])){
	$returnData .="<input type='radio' name='ref_doctor_contact_radio' value='".$patient1row['ref_doctor_contact']."' checked='checked' onclick='showClickedData(this.value, \"ref_doctor_contact\");'>".$patient1row['ref_doctor_contact']."<br>\n";
	$returnData .="<input type='radio' name='ref_doctor_contact_radio' value='".$patient2row['ref_doctor_contact']."' onclick='showClickedData(this.value, \"ref_doctor_contact\");'>".$patient2row['ref_doctor_contact']."\n";
}
$returnData .="</td>\n";

$returnData .="<td class='label'>&nbsp;</td>\n";

$returnData .="<td>&nbsp;";
$returnData .="</td>\n";

/* Picking up physican name */ /*
$returnData .="<td class='label'>Physician's Name:</td>\n";
$returnData .="<td>";
if((!empty($patient1row['ref_doctor']) && !empty($patient2row['ref_doctor'])) || (!empty($patient1row['ref_doctor']) && empty($patient2row['ref_doctor']))){
	$returnData .="<input type='text' name='ref_doctor' value='".$patient1row['ref_doctor']."' size='40' id='ref_doctor'>\n<br>";
}else if(empty($patient1row['ref_doctor']) && !empty($patient2row['ref_doctor'])) {
	$returnData .="<input type='text' name='ref_doctor' value='".$patient2row['ref_doctor']."' size='40' id='ref_doctor'>\n<br>";
}else {
	$returnData .="<input type='text' name='ref_doctor' value='' size='40' id='ref_doctor'>\n<br>";
}
if(!empty($patient1row['ref_doctor']) && !empty($patient2row['ref_doctor'])){
	$returnData .="<input type='radio' name='ref_doctor_radio' value='".$patient1row['ref_doctor']."' checked='checked' onclick='showClickedData(this.value, \"ref_doctor\");'>".$patient1row['ref_doctor']."<br>\n";
	$returnData .="<input type='radio' name='ref_doctor_radio' value='".$patient2row['ref_doctor']."' onclick='showClickedData(this.value, \"ref_doctor\");'>".$patient2row['ref_doctor']."\n";
}
$returnData .="</td>\n"; */

/*
// Picked up Physican name 
$returnData .="</tr>\n";
			// End 2nd Row 



			// Start 3rd Row 
$returnData .="<tr>\n";


$returnData .="</tr>\n";
 			// End 3rd Row 
$returnData .="</table>";
*/

/* 3 GROUP */ 

$returnData .="<h3 class='emrh3'>Group</h3>\n";
$returnData .="<table>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Group Name:</td>\n";
$returnData .="<td style='width:852px'>";
if((!empty($patient1row['group']) && !empty($patient2row['group'])) || (!empty($patient1row['group']) && empty($patient2row['group']))){
	$returnData .="<input type='text' name='group' value='".$patient1row['group']."' size='40' id='group'>\n<br>";
}else if(empty($patient1row['group']) && !empty($patient2row['group'])) {
	$returnData .="<input type='text' name='group' value='".$patient2row['group']."' size='40' id='group'>\n<br>";
}else {
	$returnData .="<input type='text' name='group' value='' size='40' id='group'>\n<br>";
}
if(!empty($patient1row['group']) && !empty($patient2row['group'])){
	$returnData .="<input type='radio' name='group_radio' value='".$patient1row['group']."' checked='checked' onclick='showClickedData(this.value, \"group\");'>".$patient1row['group']."<br>\n";
	$returnData .="<input type='radio' name='group_radio' value='".$patient2row['group']."' onclick='showClickedData(this.value, \"group\");'>".$patient2row['group']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="</table>";
$checkTypeQuery = "SELECT * from checkup_type";
$checkTypeRes = sqlStatement($checkTypeQuery);
while ($checkTypeRow = sqlFetchArray($checkTypeRes)) {

/* 4 Medial Problems */ 


	$returnData .="<h3 class='emrh3'>".$checkTypeRow['ct_type']."</h3>\n";
	$returnData .="<table>\n";
	$returnData .="<tr>\n";
	$checkConfigQuery = "SELECT * from checkup_config WHERE cc_ct_id=".$checkTypeRow['ct_id']." AND cc_status=1 ORDER BY cc_sequence";
	$checkConfigRes = sqlStatement($checkConfigQuery);
	$count=0;
	while ($checkConfigRow = sqlFetchArray($checkConfigRes)) {
		$returnData .="<td class='label'>".$checkConfigRow['cc_desc'].":</td>\n";
		$returnData .="<td>";
		$checkupDataQry = "SELECT * from checkup_data WHERE cd_pid=".$patient1row['id']." AND cd_cc_id=".$checkTypeRow['ct_id']." AND cd_ct_id=".$checkConfigRow['cc_id'];
		$checkupDataRes = sqlStatement($checkupDataQry);
		$checkupDataRow = sqlFetchArray($checkupDataRes);
		if(empty($checkupDataRow)){
			$checkupDataQry = "SELECT * from checkup_data WHERE cd_pid=".$patient2row['id']." AND cd_cc_id=".$checkTypeRow['ct_id']." AND cd_ct_id=".$checkConfigRow['cc_id'];
			$checkupDataRes = sqlStatement($checkupDataQry);
			$checkupDataRow = sqlFetchArray($checkupDataRes);
		}
		$checkedData ="";
		if(!empty($checkupDataRow)){
			$checkedData = $checkupDataRow['cd_radio'];
		}
		if(!empty($checkConfigRow['cc_radio1'])){
			$returnData .="<input type='radio' name='medicalHistory_".$checkConfigRow['cc_id']."' value='".$checkConfigRow['cc_radio1']."'";
			if($checkedData == $checkConfigRow['cc_radio1']){
				$returnData .=" checked=checked";
			}
			if($checkTypeRow['ct_id'] == 2 || $checkTypeRow['ct_id'] == 3 || $checkTypeRow['ct_id'] == 4){
				$returnData .=" onclick='enableTextBox(\"medicalHistoryDataDiv_".$checkTypeRow['ct_id']."\")'";
			}
			$returnData .=">".$checkConfigRow['cc_radio1']." &nbsp;";
		}
		if(!empty($checkConfigRow['cc_radio2'])){
			$returnData .="<input type='radio' name='medicalHistory_".$checkConfigRow['cc_id']."' value='".$checkConfigRow['cc_radio2']."'";
			if($checkedData == $checkConfigRow['cc_radio2']){
				$returnData .=" checked=checked";
			}
			$returnData .=">".$checkConfigRow['cc_radio2']." &nbsp;";
		} 
		if(!empty($checkConfigRow['cc_radio3'])){
			$returnData .="<input type='radio' name='medicalHistory_".$checkConfigRow['cc_id']."' value='".$checkConfigRow['cc_radio3']."'";
			if($checkedData == $checkConfigRow['cc_radio3']){
				$returnData .=" checked=checked";
			}
			$returnData .=">".$checkConfigRow['cc_radio3']." &nbsp;";
		}
		if(!empty($checkConfigRow['cc_radio4'])){
			$returnData .="<input type='radio' name='medicalHistory_".$checkConfigRow['cc_id']."' value='".$checkConfigRow['cc_radio4']."'";
			if($checkedData == $checkConfigRow['cc_radio4']){
				$returnData .=" checked=checked";
			}
			$returnData .=">".$checkConfigRow['cc_radio4']." &nbsp;";
		}
		if(!empty($checkConfigRow['cc_radio5'])){
			$returnData .="<input type='radio' name='medicalHistory_".$checkConfigRow['cc_id']."' value='".$checkConfigRow['cc_radio5']."'";
			if($checkedData == $checkConfigRow['cc_radio5']){
				$returnData .=" checked=checked";
			}
			$returnData .=">".$checkConfigRow['cc_radio5']." &nbsp;";
		}
		$returnData .="\n<br>";
		if($checkTypeRow['ct_id'] == 2 || $checkTypeRow['ct_id'] == 3 || $checkTypeRow['ct_id'] == 4){
			$displayDivStyle = '';
			if($checkedData == $checkConfigRow['cc_radio1']){
				$displayDivStyle="";
			}else {
				$displayDivStyle = "display:none;";
			}
			$returnData .="<div id='medicalHistoryDataDiv_".$checkTypeRow['ct_id']."' style='".$displayDivStyle."'><input type='text' name='medicalHistoryData_".$checkConfigRow['cc_id']."' value='".$checkupDataRow['cd_text1']."' size='40' id='medicalHistoryData_".$checkConfigRow['cc_id']."'></div>\n<br>";
		}
		$returnData .="</td>\n";
		$count++;
		if($count%2 == 0){
			$returnData .="</tr>\n";
			$returnData .="<tr>\n";
		}
	}
	$returnData .="</tr>\n";
	$returnData .="</table>";
}




/* 8 Patient Notes */ 



$returnData .="<h3 class='emrh3'>Patient Notes</h3>\n";
$returnData .="<table>\n";
$returnData .="<tr>\n";
$returnData .="<td class='label'>Notes:</td>\n";
$returnData .="<td>";
if((!empty($patient1row['ptntnts']) && !empty($patient2row['ptntnts'])) || (!empty($patient1row['ptntnts']) && empty($patient2row['ptntnts']))){
	$returnData .="<textarea name='ptntnts' id='ptntnts'>".$patient1row['ptntnts']."</textarea>\n<br>";
}else if(empty($patient1row['ptntnts']) && !empty($patient2row['ptntnts'])) {
	$returnData .="<textarea name='ptntnts' id='ptntnts'>".$patient2row['ptntnts']."</textarea>\n<br>";
}else {
	$returnData .="<textarea name='ptntnts' id='ptntnts'></textarea>\n<br>";
}
if(!empty($patient1row['ptntnts']) && !empty($patient2row['ptntnts'])){
	$returnData .="<input type='radio' name='ptntnts_radio' value='".$patient1row['ptntnts']."' checked='checked' onclick='showClickedData(this.value, \"ptntnts\");'>".$patient1row['ptntnts']."<br>\n";
	$returnData .="<input type='radio' name='ptntnts_radio' value='".$patient2row['ptntnts']."' onclick='showClickedData(this.value, \"ptntnts\");'>".$patient2row['ptntnts']."\n";
}
$returnData .="</td>\n";
$returnData .="</tr>\n";
$returnData .="</table>";
echo $returnData;
} else {
echo "Both Patient Id must be valid!";    
}
?>