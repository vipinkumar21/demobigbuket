<?php
require_once("../../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
?>
<script src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js" type="text/javascript"></script>
<link href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
<?php
if (!empty($_GET['selected'])) {
    $selected = $_GET['selected'];
    $notinlist = $_GET['notinlist'];
    $query = "SELECT id FROM patient_data WHERE pubpid = '" . $selected . "'";
    $datQuery = mysql_query($query);
    $prow = sqlFetchArray($datQuery);
    if ($prow) {
        ?>
        <script type="text/javascript">
            <?php echo $notinlist; ?>.style.backgroundColor = "green";
        </script>
        <?php
        //echo "Patiennt found";
    } else {
        ?>
        <script type="text/javascript">
            <?php echo $notinlist; ?>.style.backgroundColor = "red";
            jAlert("<?php xl('Wrong Patient Id Entered with field ', 'e'); ?>", 'Alert');
            <?php echo $notinlist; ?>.focus();
        </script>
        <?php
    }
} else {
    ?>
    <script type="text/javascript">
        <?php echo $notinlist; ?>.style.backgroundColor = "red";
        jAlert("<?php xl('Field can not be empty!', 'e'); ?>", 'Alert');
        <?php echo $notinlist; ?>.focus();
    </script>
    <?php
}
?>