<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/classes/POSRef.class.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");

$alertmsg = '';

/* For Inserting New Treatment Group */
if (isset($_POST["mode"]) && $_POST["mode"] == "Add") {
    $proDiaNameExist = checkProDiagnosticsExist(add_escape_custom(trim(formData('provisonalName'))));

    if ($proDiaNameExist) {
        $alertmsg = "Provisional Diagnostics already exist. Please try again.";
        $success = 0;
    } else {
        $insert_id = sqlInsert("INSERT INTO provisional_diagnostics SET " .
                "title = '" . add_escape_custom(trim(formData('provisonalName'))) . "', " .
                "sequence = '" . add_escape_custom(trim(formData('provisonalSeq'))) . "', " .
                "active = '" . add_escape_custom(trim(formData('active'))) . "', " .
                "created = '" . date('Y-m-d') . "'");
        //For copy selected group treatments and price in new created treatment group
        //mapping_treatment_diagnostic
        if ($insert_id && formData('selectedTreatmentIds')) {

            $treatmentIds = trim(formData('selectedTreatmentIds'));
            $treatment = explode(",", $treatmentIds);
            for ($counter = 0; $counter < count($treatment); $counter++) {
                sqlInsert("INSERT INTO mapping_treatment_diagnostic (trm_id, pro_id) VALUES ('" . $treatment[$counter] . "','" . $insert_id . "')");
            }
        }
        $alertmsg = "Provisional Diagnostics added successfully.";
        $success = 1;
    }
}

/* For Editing existing Treatment Group */
if (isset($_POST["mode"]) && $_POST["mode"] == "Edit") {
    $proDiaNameExist = checkProDiagnosticsIfExist(add_escape_custom(trim(formData('provisonalName'))), formData('proDiagId'));

    if ($proDiaNameExist > 2) {
        $alertmsg = "Provisional Diagnostics already exist. Please try again.";
        $success = 0;
    } else {

        sqlStatement("UPDATE provisional_diagnostics SET " .
                "title = '" . add_escape_custom(trim(formData('provisonalName'))) . "', " .
                "sequence = '" . add_escape_custom(trim(formData('provisonalSeq'))) . "', " .
                "modified = '" . date('Y-m-d') . "' 
        where id ='" . add_escape_custom(trim(formData('proDiagId'))) . "'");

        if (formData('selectedTreatmentIds')) {
            $treatmentIds = trim(formData('selectedTreatmentIds'));
            $treatment = explode(",", $treatmentIds);
            sqlInsert("DELETE FROM mapping_treatment_diagnostic WHERE pro_id = '" . add_escape_custom(trim(formData('proDiagId'))) . "'");
            for ($counter = 0; $counter < count($treatment); $counter++) {
                sqlInsert("INSERT INTO mapping_treatment_diagnostic (trm_id, pro_id) VALUES ('" . $treatment[$counter] . "','" . add_escape_custom(trim(formData('proDiagId'))) . "')");
            }
        }
        $alertmsg = "Provisional Diagnostics edited successfully.";
        $success = 1;
    }
}

function checkProDiagnosticsExist($provisonalName) {
    $sql = "SELECT title FROM provisional_diagnostics WHERE title = '" . $provisonalName . "'";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    return sqlNumRows($eres);
}

function checkProDiagnosticsIfExist($provisonalName, $proId) {
    $sql = "SELECT title FROM provisional_diagnostics WHERE title = '" . $provisonalName . "' AND id = '" . $proId . "'";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    return sqlNumRows($eres);
}

function checkAlredayExist($proDiagnoId, $treatmentId) {
    if (!empty($proDiagnoId)) {
        $sql = "SELECT id FROM mapping_treatment_diagnostic WHERE pro_id = '" . $proDiagnoId . "' AND trm_id = '" . $treatmentId . "'";
        $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
        $linkedid = array();
        if (sqlNumRows($eres)) {
            while ($row = sqlFetchArray($eres)) {
                $linkedid[] = $row['id'];
            }
        }
        return $linkedid;
    } else {
        return false;
    }
}

if (isset($_GET['proDiagId'])) {
    $sql = "SELECT id,title,sequence,active FROM provisional_diagnostics WHERE id = '" . $_GET['proDiagId'] . "'";
    $proDiagnosticArra = sqlQuery($sql, null, $GLOBALS['adodb']['dbreadonly']);
    $_REQUEST['proDiagId'] = $proDiagnosticArra['id'];
    $sqltrmt = "SELECT trm_id,pro_id FROM mapping_treatment_diagnostic WHERE pro_id = '" . $_GET['proDiagId'] . "'";
    $erestrmt = sqlStatement($sqltrmt, null, $GLOBALS['adodb']['dbreadonly']);
    $linkedTreatment = '';
    while ($row = sqlFetchArray($erestrmt)) {
        $linkedTreatment .= $row['trm_id'] . ',';
    }
    $ftrmIds = rtrim($linkedTreatment, ',');
}
?>
<html>
    <head>
        <script LANGUAGE="JavaScript">

            //Select an item by "copying" it from one select box to another
            function select_item(src_form_element, dst_form_element) {
                //alert('Src: ' + src_form_element);
                //alert('Dst: ' + dst_form_element);
                found_dup = false;
                var treatmentId = document.proDiagnostic.elements['selectedTreatmentIds'].value;
                //Copy it over to the dst element
                for (i = 0; i < src_form_element.options.length; i++) {
                    if (src_form_element.options[i].selected) {
                        //Check to see if duplicate entries exist.
                        for (n = 0; n < dst_form_element.options.length; n++) {
                            if (src_form_element.options[i].id == dst_form_element.options[n].id) {
                                found_dup = true;
                            }
                        }

                        //Only add if its not a duplicate entry.
                        if (!found_dup) {
                            //Grab the current selected value from the parent

                            src_id = src_form_element.options[i].id;
                            src_text = src_form_element.options[i].text;
                            options_length = dst_form_element.options.length;
                            if (treatmentId != "") {
                                treatmentId = treatmentId + ',' + src_id;
                            } else {
                                treatmentId = src_id;
                            }

                            dst_form_element.options[options_length] = new Option(src_text, src_id);
                            dst_form_element.options[options_length].setAttribute("id", src_id);
                            //dst_form_element.options[options_length].setAttribute("selected",'selected');
                            //dst_form_element.options[options_length].selected = true;
                        }
                    }
                    found_dup = false;
                }
                document.proDiagnostic.elements['selectedTreatmentIds'].value = treatmentId;
            }

            //Used for moving items to and from the selected combo box.
            function deselect_item(form_element) {
                //alert('Src: ' + src_form_element);
                //alert('Dst: ' + dst_form_element);
                var treatmentId = '';
                //Copy it over to the dst element
                for (i = 0; i < form_element.options.length; i++) {
                    if (form_element.options[i].selected) {
                        form_element.options[i] = null;
                        i = i - 1;
                    } else {

                        if (treatmentId != "") {
                            treatmentId = treatmentId + ',' + form_element.options[i].id;
                        } else {
                            treatmentId = form_element.options[i].id;
                        }
                    }

                }
                document.proDiagnostic.elements['selectedTreatmentIds'].value = treatmentId;
            }
        </script>

        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css"> 
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script> 
        <script type="text/javascript">


            /// todo, move this to a common library
            function trimAll(sString)
            {
                while (sString.substring(0, 1) == ' ')
                {
                    sString = sString.substring(1, sString.length);
                }
                while (sString.substring(sString.length - 1, sString.length) == ' ')
                {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }
            function submitform() {
                if (document.forms[0].provisonalName.value.length > 0 && document.forms[0].provisonalSeq.value.length > 0) {
                    document.forms[0].submit();
                    //alert('Form able to submit');
                } else {
                    if (document.forms[0].provisonalName.value.length <= 0) {
                        document.forms[0].provisonalName.style.backgroundColor = "red";
                        document.forms[0].provisonalName.focus();
                    } else if (document.forms[0].provisonalSeq.value.length <= 0) {
                        document.forms[0].provisonalSeq.style.backgroundColor = "red";
                        document.forms[0].provisonalSeq.focus();
                    }
                }
            }



            $(document).ready(function () {



                // fancy box
                enable_modals();

                tabbify();

                // special size for
                $(".large_modal").fancybox({
                    'overlayOpacity': 0.0,
                    'showCloseButton': true,
                    'frameHeight': 600,
                    'frameWidth': 1000
                });

                // special size for
                $(".medium_modal").fancybox({
                    'overlayOpacity': 0.0,
                    'showCloseButton': true,
                    'frameHeight': 260,
                    'frameWidth': 510
                });

            });






        </script>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">

    </head>
    <body class="body_top" >
        <table>
            <tr><td>
                    <span class="title"><?php xl('Add Provisional Diagnostics', 'e'); ?></span>&nbsp;&nbsp;&nbsp;</td>
                <td colspan=5 align=center style="padding-left:2px;">
                    <a onclick="submitform();" class="css_button large_button" name='form_save' id='form_save' href='#'>
                        <span class='css_button_span large_button_span'><?php xl('Save', 'e'); ?></span> </a>
                    <a class="css_button large_button" id='cancel' href='trProDiagnosticsList.php' >
                        <span class='css_button_span large_button_span'><?php xl('Cancel', 'e'); ?></span>
                    </a>
                </td></tr>
        </table>

        <br>

        <form name='proDiagnostic' method='post' action="proDiagnosticAdd.php">
            <input type="hidden" name="proDiagId" id="proDiagId" value="<?php echo $_REQUEST['proDiagId']; ?>">
            <input type="hidden" name="mode" id="mode" value="<?php echo (!empty($_REQUEST['proDiagId']) ? 'Edit' : 'Add'); ?>">

            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><span class="text"><?php xl('Name', 'e'); ?>:</span></td>
                    <td><input type=entry name="provisonalName" size=50 maxlength="50" value="<?php echo $proDiagnosticArra['title']; ?>"><span class="mandatory">&nbsp;*</span></td>
                </tr>
                <tr>
                    <td><span class="text"><?php xl('Seq', 'e'); ?>: </span></td>
                    <td><input type=entry size=20 name="provisonalSeq" value="<?php echo $proDiagnosticArra['sequence']; ?>" maxlength="3" onkeypress="return allowPositiveNumber(event);"><span class="mandatory">&nbsp;*</span></td>
                </tr>
                <tr>
                    <td><span class="text"><?php xl('Active', 'e'); ?>:&nbsp;*</span></td>
                    <td><span class='text'><?php echo (!empty($proDiagnosticArra['active']) ? 'Yes' : 'No') ?></span> <input type=hidden name=active value="1"></td>            
                </tr>
                <tr>
                    <td colspan="2" >&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" ><span class="title"><?php xl('Map With Treatment', 'e'); ?></span></td>                    
                </tr>
                <tr>
                    <td colspan="2" >&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2"><span class="text"><?php xl('InActive', 'e'); ?></span></td>
                                <td>&nbsp;</td>
                                <td colspan="2"><span class="text"><?php xl('Active', 'e'); ?></span></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <select name="treatment[]" tabindex="0" size="10" width="200" multiple style="height: 250px; width: 300px; overflow: auto;">
<?php
$query = "SELECT id, trname FROM treatment_master WHERE active = '1' ORDER BY id asc";
$sql = sqlStatement($query);
while ($frow = sqlFetchArray($sql)) {
    echo '<option vaule="' . $frow['id'] . '" id="' . $frow['id'] . '">' . $frow['trname'] . '</option>';
}
?>                      
                                    </select>
                                </td>
                                <td valign="middle">
                                    <br /><input type="button" class="select" name="select" value="&nbsp;&gt;&gt;&nbsp;" onClick="select_item(document.proDiagnostic.elements['treatment[]'], document.proDiagnostic.elements['selected_treatment[]'])">
                                    <br /><input type="button" class="deselect" name="deselect" value="&nbsp;&lt;&lt;&nbsp;" onClick="deselect_item(document.proDiagnostic.elements['selected_treatment[]'])">
                                </td>
                                <td colspan="2" >
                                    <select name="selected_treatment[]" tabindex="0" size="10" width="200" multiple style="height: 250px; width: 300px; overflow: auto;">
<?php
if (!empty($ftrmIds)) {
    $query = "SELECT id, trname FROM treatment_master WHERE active = '1' AND id IN ($ftrmIds) ORDER BY id asc";
    $sql = sqlStatement($query);
    while ($frow = sqlFetchArray($sql)) {
        echo '<option vaule="' . $frow['id'] . '" id="' . $frow['id'] . '">' . $frow['trname'] . '</option>';
    }
}
?>   
                                    </select>
                                    <input type="hidden" name="selectedTreatmentIds" id="selectedTreatmentIds" value="<?php echo $ftrmIds; ?>">
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
            </table>
        </form>

        <script language="JavaScript">
<?php
if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
    if($success){
        echo 'location.href="trProDiagnosticsList.php"';
    }
}
?>
        </script>

    </body>
</html>
