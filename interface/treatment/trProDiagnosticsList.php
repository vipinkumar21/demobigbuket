<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");

$alertmsg = '';

/* For Activate Treatment Group */
if (isset($_GET['mode']) && $_GET['mode'] == 'activate' && isset($_GET['proDiagId'])) {
    $result = sqlStatement("update provisional_diagnostics SET active = '1' where id='" . add_escape_custom(trim($_GET['proDiagId'])) . "'");
    $alertmsg = "Provisional Diagnostics activated successfully.";
}
/* For Deactivate Treatment Group */
if (isset($_GET['mode']) && $_GET['mode'] == 'deactivate' && isset($_GET['proDiagId'])) {

    $linkedTreatment = getLinkedTreatments(add_escape_custom(trim($_GET['proDiagId'])));
    if (empty($linkedTreatment)) {
        $result = sqlStatement("update provisional_diagnostics SET active = '0' where id='" . add_escape_custom(trim($_GET['proDiagId'])) . "'");
        $alertmsg = "Provisional Diagnostics deactivated successfully.";
    } else {
        $alertmsg = "Selected Provisional Diagnostics linked with Treatments";
    }
}
/* For Delete Treatment Group */
if (isset($_GET['mode']) && $_GET['mode'] == 'delete' && isset($_GET['proDiagId'])) {

    $linkedTreatment = getLinkedTreatments(add_escape_custom(trim($_GET['proDiagId'])));
    if (empty($linkedTreatment)) {
        //For delete Provisional Diagnostics 
        $updateResult = sqlStatement("DELETE FROM provisional_diagnostics where id='" . add_escape_custom(trim($_GET['proDiagId'])) . "'");
        $alertmsg = "Provisional Diagnostics deleted successfully.";
    } else {
        $alertmsg = "Selected Provisional Diagnostics linked with Treatments.";
    }
}

function getLinkedTreatments($proDiagnoId) {
    if (!empty($proDiagnoId)) {
        $sql = "SELECT trm_id,pro_id FROM mapping_treatment_diagnostic WHERE pro_id = '" . $proDiagnoId . "'";
        $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
        $linkedTreatment = array();
        if (sqlNumRows($eres)) {
            while ($row = sqlFetchArray($eres)) {
                $linkedTreatment[] = $row['trm_id'];
            }
        }
        return $linkedTreatment;
    } else {
        return false;
    }
}
?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>


        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
    </head>

    <body class="body_top">

        <div>
            <div>
                <table><tr><td>
                            <b><?php xl('Provisional Diagnostics ', 'e'); ?></b>&nbsp;</td><td>
                            <a href="proDiagnosticAdd.php" class="iframe css_button"><span><?php xl('Add', 'e'); ?></span></a>
                        </td></tr>
                </table>
            </div>
            <div class="tabContainer" style="width:100%;">
                <div>
                    <table cellpadding="1" cellspacing="0" class="showborder" style="width:100%;">
                        <tr class="showborder_head" height="22">
                            <th style="border-style:1px solid #000" width="160px"><?php xl('Name', 'e'); ?></th>
                            <th style="border-style:1px solid #000"><?php xl('Status', 'e'); ?></th>
                            <th style="border-style:1px solid #000"><?php xl('Action', 'e'); ?></th>
                        </tr>
<?php
$fres = 0;
$query = "SELECT id, title, sequence, active, created, modified FROM provisional_diagnostics order by sequence asc";
$sql_num = sqlStatement($query); // total no. of rows
$num_rows = sqlNumRows($sql_num);

$per_page = $GLOBALS['encounter_page_size'];  // Pagination variables processing
$page = $_GET["page"];

if (!$_GET["page"]) {
    $page = 1;
}

$prev_page = $page - 1;
$next_page = $page + 1;

$page_start = (($per_page * $page) - $per_page);
if ($num_rows <= $per_page) {
    $num_pages = 1;
} else if (($num_rows % $per_page) == 0) {
    $num_pages = ($num_rows / $per_page);
} else {
    $num_pages = ($num_rows / $per_page) + 1;
    $num_pages = (int) $num_pages;
}

$query .= " LIMIT $page_start , $per_page "; // limit as per pagination
$fres = sqlStatement($query);
if ($fres) {
    $result2 = array();
    for ($iter3 = 0; $frow = sqlFetchArray($fres); $iter3++)
        $result2[$iter3] = $frow;
    foreach ($result2 as $iter3) {
        $groupStatus = "Active";
        if ($iter3['active'] == 0) {
            $groupStatus = "Deactive";
        }
        ?>
                                <tr height="22">
                                    <td valign="top" class="text"><b><span><?php echo htmlspecialchars($iter3{title}); ?></a></b>&nbsp;</td>       
                                    <td><?php echo htmlspecialchars($groupStatus); ?>&nbsp;</td>
                                    <td><a href='proDiagnosticAdd.php?proDiagId=<?php echo $iter3{id}; ?>' class='iframe medium_modal' onclick='top.restoreSession()'>Edit</a>&nbsp;
                                <?php
                                if ($iter3['active'] == 0) {
                                    ?>
                                            <a href='trProDiagnosticsList.php?proDiagId=<?php echo $iter3{id}; ?>&mode=activate'>Activate</a>&nbsp;
                                    <?php
                                } else {
                                    ?>
                                            <a href='trProDiagnosticsList.php?proDiagId=<?php echo $iter3{id}; ?>&mode=deactivate'>Deactivate</a>&nbsp;
                                    <?php
                                }
                                ?> 
                                        <a href='trProDiagnosticsList.php?proDiagId=<?php echo $iter3{id}; ?>&mode=delete'>Delete</a>&nbsp;    
                                    </td>

                                </tr>
                                        <?php
                                    }
                                }
                                if (count($result2) <= 0) {
                                    ?>
                            <tr height="25">
                                <td colspan="3"  style="text-align:center;font-weight:bold;"> <?php echo xl("Currently there are no Provisional Diagnostics."); ?></td>
                            </tr>
                                <?php }
                                ?>
                    </table>
<?php // Pagination Displaying Section ?>
                    <br>
                    <table><tbody><tr><td class="text" >
                                    Total <?php echo $num_rows; ?> Records : Page :
                        <?php
                        if ($prev_page) {
                            echo " <a href='$_SERVER[SCRIPT_NAME]?page=$prev_page'><< Back</a> ";
                        }

                        for ($i = 1; $i <= $num_pages; $i++) {
                            if ($i != $page) {
                                echo "[ <a href='$_SERVER[SCRIPT_NAME]?page=$i'>$i</a> ]";
                            } else {
                                echo "<b> $i </b>";
                            }
                        }
                        if ($page != $num_pages) {
                            echo " <a href ='$_SERVER[SCRIPT_NAME]?page=$next_page'>Next>></a> ";
                        }
                        ?>

                                </td></tr></tbody></table>
                </div>
            </div>
        </div>
        <script language="JavaScript">
                                    <?php
                                    if ($alertmsg = trim($alertmsg)) {
                                        echo "alert('$alertmsg');\n";
                                    }
                                    ?>
        </script>

    </body>
</html>
