<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/classes/POSRef.class.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");

$alertmsg = '';
?>
<html>
<head>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
<?php
// Old Browser comp trigger on js

if (isset($_POST["mode"]) && $_POST["mode"] == "facility") {
  	echo '
<script type="text/javascript">
<!--
parent.$.fn.fancybox.close();
//-->
</script>

	';
}
function getTreatmentGroupOption($selectedGroupID  = NULL){
    $sql = "SELECT ct_id, ct_label, ct_key FROM code_types WHERE ct_active=1";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    $returnData = '<select id="form_tr_group" name="form_tr_group"><option value="">-- All Treatment Group --</option>';
    while ($erow = sqlFetchArray($eres)) {
        if($selectedGroupID == $erow['ct_id']){
            $returnData .= '<option value="'.$erow['ct_id'].'" selected = "selected">'.$erow['ct_key'].'</option>';
        }else {
           $returnData .= '<option value="'.$erow['ct_id'].'">'.$erow['ct_key'].'</option>'; 
        }
    }
    $returnData .= '</select>';    
    return $returnData;
}        
?>
<script type="text/javascript">
/// todo, move this to a common library
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}
function submitform() {	
    if (document.forms[0].name.value.length>0 && document.forms[0].id.value.length>0 && document.forms[0].label.value.length>0 && document.forms[0].seq.value.length>0 && document.forms[0].modlength.value.length>0 && document.forms[0].fees.value==1 && document.forms[0].relation.value==1 && document.forms[0].diagnosis.value==1) {
        top.restoreSession();
        document.forms[0].submit();
        //alert('Form able to submit');
    } else {
        if(document.forms[0].name.value.length<=0){
                document.forms[0].name.style.backgroundColor="red";
                document.forms[0].name.focus();
        }else if(document.forms[0].id.value.length<=0){
                document.forms[0].id.style.backgroundColor="red";
                document.forms[0].id.focus();	
        }else if(document.forms[0].label.value.length<=0){
                document.forms[0].label.style.backgroundColor="red";
                document.forms[0].label.focus();	
        }else if(document.forms[0].seq.value.length<=0){
                document.forms[0].seq.style.backgroundColor="red";
                document.forms[0].seq.focus();	
        }else if(document.forms[0].modlength.value.length<=0){
                document.forms[0].modlength.style.backgroundColor="red";
                document.forms[0].modlength.focus();	
        }else if(document.forms[0].fees.checked === false){
                document.forms[0].fees.style.backgroundColor="red";
                document.forms[0].fees.focus();	
        }else if(document.forms[0].relation.checked === false){
                document.forms[0].relation.style.backgroundColor="red";
                document.forms[0].relation.focus();	
        }else if(document.forms[0].diagnosis.checked === false){
                document.forms[0].diagnosis.style.backgroundColor="red";
                document.forms[0].diagnosis.focus();	
        }
    }
}

function toggle( target, div ) {

    $mode = $(target).find(".indicator").text();
    if ( $mode == "collapse" ) {
        $(target).find(".indicator").text( "expand" );
        $(div).hide();
    } else {
        $(target).find(".indicator").text( "collapse" );
        $(div).show();
    }

}

$(document).ready(function(){

    $("#dem_view").click( function() {
        toggle( $(this), "#DEM" );
    });

    // fancy box
    enable_modals();

    tabbify();

    // special size for
	$(".large_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 600,
		'frameWidth' : 1000
	});

    // special size for
	$(".medium_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 260,
		'frameWidth' : 510
	});

});

$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });
});

function displayAlert()
{
	if(document.getElementById('primary_business_entity').checked==false)
	alert("<?php echo addslashes(xl('Primary Business Entity tax id is used as account id for NewCrop ePrescription. Changing the facility will affect the working in NewCrop.'));?>");
	else if(document.getElementById('primary_business_entity').checked==true)
	alert("<?php echo addslashes(xl('Once the Primary Business Facility is set, it should not be changed. Changing the facility will affect the working in NewCrop ePrescription.'));?>");
}
function allowPositiveNumber(e) {
	var charCode = (e.which) ? e.which : event.keyCode

	if (charCode > 31 && (charCode <= 46 || charCode > 57 )) {
		return false;
	}
	return true;
 
}


</script>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">

</head>
<body class="body_top">
<!-- <table>
<tr><td>
    <span class="title"><?php xl('Add Treatment Group','e'); ?></span>&nbsp;&nbsp;&nbsp;</td>
    <td colspan=5 align=center style="padding-left:2px;">
        <a onclick="submitform();" class="" name='form_save' id='form_save' href='#'>
            <span class='btn btn-warning btn-sm'><?php xl('Save','e');?></span>
        </a>

        <a class="" id='cancel' href='#' >
            <span class='btn btn-warning btn-sm'><?php xl('Cancel','e');?></span>
        </a>

</td></tr>
</table> --> 


    <h3 class="emrh3"><?php xl('Add Treatment Group','e'); ?></h3>
    









<form id="treatmentgroupnewform" name='facility' method='post' action="trGroupList.php" target='_parent' >





    <input type=hidden name=mode value="group">
    <table border=0 cellpadding=0 cellspacing=0>

        <tr style="margin-bottom:6px;">
            <td width ="15%">
                <span class="text"><?php xl('Name','e'); ?>: </span>
                <span class="mandatory">&nbsp;*</span>
            </td>
            <td width="32.5%">
                <input class="emrdate" type=entry name=name size=15 maxlength="15" value="">
            </td>
            <td width="5%">&nbsp;</td>
            <td width="15%"><!--span class="text"><?php //xl('Copy','e'); ?>:&nbsp;*</span></td>
                <td><?php //echo getTreatmentGroupOption($selectedGroupID  = NULL); ?></td-->
                <span class="text"><?php xl('Code','e'); ?>: </span>
                <span class="mandatory">&nbsp;*</span>
            </td>
            <td width="32.5%">
                <input class="emrdate" type=entry name=id size=15 value="" maxlength="3" onkeypress="return allowPositiveNumber(event);">
            </td>
        </tr>

         <tr>
             <td>
                <span class="text"><?php xl('Label','e'); ?>: </span><span class="mandatory">&nbsp;*</span>
             </td>
             <td>
                <input class="emrdate" type=entry name=label size=15 value="">
             </td>
            <td width=20>&nbsp;</td>
            <td>
                <span class="text"><?php xl('Seq','e'); ?>: </span><span class="mandatory">&nbsp;*</span>
            </td>
            <td>
                <input class="emrdate" type=entry size=15 name=seq value="" maxlength="3" onkeypress="return allowPositiveNumber(event);">
            </td>
        </tr>

        <tr>
            <!--<td><span class="text"><?php //xl('ModLength','e'); ?>: </span><span class="mandatory">&nbsp;*</span></td><td><input type=entry name=modlength size=15 value=""  maxlength="2" onkeypress="return allowPositiveNumber(event);"></td>
            <td>&nbsp;</td>
            -->
            <input type=hidden name=modlength size=15 value="12"  maxlength="2">
            <!--<td><span class="text"><?php //xl('Justify','e'); ?>: </span></td><td><input type=entry size=15 name=justify value=""></td>-->
            <input type=hidden size=15 name=justify value="">
            <input type=hidden size=15 name=mask value="">
            <input type='hidden' name='claims' value = '1'>
            <input type='hidden' name='fees' value = '1'>
            <input type='hidden' name='relation' value = '1'>
            <input type='hidden' name='hide' value = '1'>
            <input type='hidden' name='procedure' value = '1'>
            <input type='hidden' name='diagnosis' value = '1'>
            <input type=hidden name=external value="0">
            <input type=hidden name=active value="1">
            <input type=hidden name=description size=15 maxlength="254" value="">
        </tr>
        <tr>
            <td></td><td><input type='checkbox' name='copyTrt' value = '1'><span class='text'><?php xl('Copy From Master','e'); ?></span></td>
            <td>&nbsp;</td>
        </tr>
       






        
        <!--
        <tr>
        <td><span class="text"><?php //xl('Mask','e'); ?>: </span></td><td><input type=entry size=15 name=mask value=""></td>
        <td>&nbsp;</td>
        <td><span class='text'><?php //xl('Claims','e'); ?>: </span></td><td><input type='checkbox' name='claims' value = '1'></td>
        </tr>
        -->
        <!--<tr>
          <td><span class='text'><?php //xl('Fees','e'); ?>: </span><span class="mandatory">&nbsp;*</span></td> <td><input type='checkbox' checked="checked" name='fees' value = '1'></td>
          <td>&nbsp;</td>
          <td><span class='text'><?php //xl('Relations','e'); ?>: </span><span class="mandatory">&nbsp;*</span></td> <td><input type='checkbox' checked="checked" name='relation' value = '1'></td>
        </tr>
        -->
        <!--<tr>
          <td><span class='text'><?php //echo xl('Hide', 'e'); ?>: </span></td> <td><input type='checkbox' name='hide' value = '1'></td>
          <td>&nbsp;</td>
          <td><span class='text'><?php //xl('Procedure','e'); ?>: </span></td> <td><input type='checkbox' name='procedure' value = '1'></td>
        </tr>
        -->
        <!--<tr>
          <td><span class='text'><?php //echo xl('Diagnosis', 'e'); ?>: </span><span class="mandatory">&nbsp;*</span></td> <td><input type='checkbox' checked="checked" name='diagnosis' value = '1'></td>
          <td>&nbsp;</td>
          <td><span class="text"><?php //xl('External','e'); ?>:&nbsp;*</span></td>
            <td><span class='text'>No</span> <input type=hidden name=external value="0"></td>
        </tr>
        -->
	<!--<tr>
            <td><span class="text"><?php //xl('Active','e'); ?>:&nbsp;*</span></td><td><span class='text'>Yes</span> <input type=hidden name=active value="1"></td>
			<td>&nbsp;</td>
          	<td><span class="text"><?php //xl('Description','e'); ?>: </span></td><td><input type=entry name=description size=15 maxlength="254" value=""></td></td>
        </tr>
        -->
        <tr height="25" style="valign:bottom;">
        <td></td><td><font class="mandatory">*</font><span class="text"> <?php echo xl('Required','e'); ?></span></td><td>&nbsp;</td>
        <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
    </table>

    <div style="margin-left:175px ;margin-top:20px;"> 
        <a onclick="submitform();" class="" name='form_save' id='form_save' href='#'>
            <span class='btn btn-warning btn-sm'><?php xl('Save','e');?></span>
        </a>

        <a class="" id='cancel' href='#' >
            <span class='btn btn-warning btn-sm'><?php xl('Cancel','e');?></span>
        </a>
    </div>


</form>

<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
</script>

</body>
</html>
