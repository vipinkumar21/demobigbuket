<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");

$alertmsg = '';
//echo "<pre>add";print_r($_POST);
/*For Inserting New Treatment Group*/
if (isset($_POST["mode"]) && $_POST["mode"] == "group") { 
    $codeKeyExist = checkCodeKeyExist(add_escape_custom(trim(formData('name'))));
    $codeIdExist = checkCodeIdExist(add_escape_custom(trim(formData('id'))));
    
    if($codeKeyExist || $codeIdExist) {
        $alertmsg = "Treatment group name OR code already exist. Please try again.";
    }else { 
        $insert_id=sqlInsert("INSERT INTO code_types SET " .
        "ct_key = '"         . add_escape_custom(trim(formData('name'))) . "', " .
        "ct_id = '"        . add_escape_custom(trim(formData('id'))) . "', " .
        "ct_seq = '"        . add_escape_custom(trim(formData('seq'))) . "', " .
        "ct_mod = '"          . add_escape_custom(trim(formData('modlength'))) . "', " .
        "ct_just = '"       . add_escape_custom(trim(formData('justify'))) . "', " .
        "ct_mask = '"         . add_escape_custom(trim(formData('mask'))) . "', " .
        "ct_fee = '"        . add_escape_custom(trim(formData('fees'))) . "', " .
        "ct_rel = '"  . add_escape_custom(trim(formData('relation'))) . "', " .
        "ct_nofs = '" . add_escape_custom(trim(formData('hide'))) . "', " .
        "ct_diag = '"  . add_escape_custom(trim(formData('diagnosis'))) . "', " .
        "ct_active = '"      . add_escape_custom(trim(formData('active'))) . "', " .
        "ct_label = '"      	 . add_escape_custom(trim(formData('label'))) . "', " .
        "ct_external = '"  . add_escape_custom(trim(formData('external'))) . "', " .
        "ct_claim = '"  . add_escape_custom(trim(formData('claims'))) . "', " .
        "ct_proc = '"  . add_escape_custom(trim(formData('procedure'))) . "', " .
        "ct_description = '"  . add_escape_custom(trim(formData('description' ))) . "'");  
        if(formData('copyTrt')==1){
            //For copy selected group treatments and price in new created treatment group
            $codeKeyExist = checkCodeKeyExist(add_escape_custom(trim(formData('name'))));
            if($codeKeyExist){      
                $insertedGroupTrIds=sqlInsert("INSERT INTO codes SELECT '', `trname` AS `code_text`,  `trshort_text` AS `code_text_short`,  `code`,  '".formData('id')."',  '',  NULL,  NULL,  `title` AS `superbill`,  '',  '',  0,  tm.active AS `active`,  1,  1,  `multisettings_status` AS `multisettingstate`,  `multisettings_no` AS `multisettingno`, `unit_multiply`, `quadrant_status`, `default_aplicable`, `tm`.`consumable_status`, `tm`.`sequence`  FROM treatment_master AS tm INNER JOIN treatment_groups AS tg ON tg.id = tm.category_id WHERE tm.active = 1");
                $insert_seed_id=sqlInsert("INSERT INTO prices (pr_id, pr_selector, pr_level, pr_price) SELECT c.id AS pr_id, ''  AS  pr_selector, 'standard'  AS pr_level, tm.trprice AS pr_price FROM treatment_master AS tm INNER JOIN codes AS c ON c.code = tm.code WHERE code_type = '".formData('id')."'");

            }
        }
        $alertmsg = "Treatment group added successfully.";
    }
}

/*For Editing existing Treatment Group*/
if (isset($_POST["mode"]) && $_POST["mode"] == "editgroup")
{
    sqlStatement("update code_types code_types SET " .
    "ct_key = '"         . add_escape_custom(trim(formData('name'))) . "', " .
    "ct_id = '"        . add_escape_custom(trim(formData('id'))) . "', " .
    "ct_seq = '"        . add_escape_custom(trim(formData('seq'))) . "', " .
    "ct_mod = '"          . add_escape_custom(trim(formData('modlength'))) . "', " .
    "ct_just = '"       . add_escape_custom(trim(formData('justify'))) . "', " .
    "ct_mask = '"         . add_escape_custom(trim(formData('mask'))) . "', " .
    "ct_fee = '"        . add_escape_custom(trim(formData('fees'))) . "', " .
    "ct_rel = '"  . add_escape_custom(trim(formData('relation'))) . "', " .
    "ct_nofs = '" . add_escape_custom(trim(formData('hide'))) . "', " .
    "ct_diag = '"  . add_escape_custom(trim(formData('diagnosis'))) . "', " .
    "ct_active = '"      . add_escape_custom(trim(formData('active'))) . "', " .
    "ct_label = '"      	 . add_escape_custom(trim(formData('label'))) . "', " .
    "ct_external = '"  . add_escape_custom(trim(formData('external'))) . "', " .
    "ct_claim = '"  . add_escape_custom(trim(formData('claims'))) . "', " .
    "ct_proc = '"  . add_escape_custom(trim(formData('procedure'))) . "', " .
    "ct_description = '"  . add_escape_custom(trim(formData('description' ))) . "'    
    where ct_id='" . add_escape_custom(trim(formData('ct_id'))) . "'" );
    $alertmsg = "Treatment group edited successfully.";
}
/*For Activate Treatment Group*/
if(isset($_GET['mode']) && $_GET['mode'] == 'activate' && isset($_GET['trgCode'])){    
    $result = sqlStatement("update code_types SET ct_active = '1' where ct_id='" . add_escape_custom(trim($_GET['trgCode'])) . "'");
    $alertmsg = "Treatment group activated successfully.";
}
/*For Deactivate Treatment Group*/
if(isset($_GET['mode']) && $_GET['mode'] == 'deactivate' && isset($_GET['trgCode'])){ 
    $linkedClinics = getLinkedClinics(add_escape_custom(trim($_GET['trgCode'])));
    if(empty($linkedClinics)){
        $result = sqlStatement("update code_types SET ct_active = '0' where ct_id='" . add_escape_custom(trim($_GET['trgCode'])) . "'");
        $alertmsg = "Treatment group deactivated successfully.";
    }else {
        $alertmsg = "Selected treatment group linked with these clinics:".  implode(',', $linkedClinics).". Please unlink treatment group from clinics then try again for deactivate.";
    }
}
/*For Delete Treatment Group*/
if(isset($_GET['mode']) && $_GET['mode'] == 'delete' && isset($_GET['trgCode'])){
    $linkedClinics = getLinkedClinics(add_escape_custom(trim($_GET['trgCode'])));
    if(empty($linkedClinics)){ 
        //For delete treatments and price from selected group treatment
        $deleteResult = sqlStatement("DELETE pr, co FROM prices AS pr RIGHT JOIN codes AS co ON co.id = pr.pr_id WHERE co.code_type='" . add_escape_custom(trim($_GET['trgCode']))."'");
        //For delete selected group treatment
        $updateResult = sqlStatement("DELETE FROM code_types where ct_id='" . add_escape_custom(trim($_GET['trgCode'])) . "'");
        $alertmsg = "Treatment group deleted successfully.";
    }else {
        $alertmsg = "Selected treatment group linked with these clinics:".  implode(',', $linkedClinics).". Please unlink treatment group from clinics then try again for delete.";
    }
}
/*For get linked clinic list to Treatment Group*/
function getLinkedClinics($trGroupCode){
    $sql = "SELECT id, name FROM facility WHERE treatment_group = '".$trGroupCode."'";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    $linkedClinics = array();
    if(sqlNumRows($eres)){
        while($row = sqlFetchArray($eres)){
            $linkedClinics[] = $row['name'];
        }
    }
    return $linkedClinics;
}
/*For get number of treatments in Treatment Group*/
function teratmentsInGroup($trGroupCode){
    $sql = "SELECT id FROM codes WHERE code_type = '".$trGroupCode."'";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    return sqlNumRows($eres);
}

/*For get number of treatments in Treatment Group*/
function checkCodeKeyExist($codeKey){
    $sql = "SELECT ct_key FROM code_types WHERE ct_key = '".$codeKey."'";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    return sqlNumRows($eres);
}
/*For get number of treatments in Treatment Group*/
function checkCodeIdExist($codeId){
    $sql = "SELECT ct_id FROM code_types WHERE ct_id = '".$codeId."'";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    return sqlNumRows($eres);
}
?>
<html>
<head>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script type="text/javascript">


$(document).ready(function(){

    // fancy box
    enable_modals();

    // special size for
	$(".addfac_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 610,
		'frameWidth' : 650
	});

    // special size for
	$(".medium_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 610,
		'frameWidth' : 650
	});

});

</script>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
</head>

<body id="trgroupaddbody" class="body_top">

<div>
    
	<!-- <table><tr><td>
        <b><?php xl('Treatment Group','e'); ?></b>&nbsp;</td><td>
                    <a href="trGroupAdd.php" class="btn btn-warning btn-sm"><span><?php xl('Add','e');?></span></a>
		 </td></tr>
	</table> -->

    
        <h3 class="emrh3" style="position:relative;padding:15px;">
            <?php xl('Treatment Group','e'); ?>

         <a href="trGroupAdd.php" class="iframe btn btn-default btn-sm " style="position: absolute; right: 20px; top: 9px;"><span>
            <?php xl('Add','e');?></span>
        </a>
        </h3>
       
         
    


   
    <div class="tabContainer" style="width:100%;background:white;">



        <div>
<table id="trgrouptable" cellpadding="1" cellspacing="0" class="emrtable" style="width:100%;margin-top:12px;">
    <thead>
    	<tr class="showborder_head" height="22" style="border:1px solid #000;">
    		<th style="" width="20%"><?php xl('Name','e'); ?></th>
    		<th style="" width="10%"><?php xl('Code','e'); ?></th>
            <th style="" width="20%"><?php xl('Description','e'); ?></th>
            <th style="" width="10%"><?php xl('Treatments#','e'); ?></th>
    		<th style="" width="20%"><?php xl('Status','e'); ?></th>
            <th style="padding-left:0px;" width="15%" class="newtextcenter"><?php xl('Action','e'); ?></th>
        </tr>
    </thead>
     <?php
        $fres = 0;
        $query = "SELECT ct_id, ct_seq, ct_mask, ct_fee, ct_rel, ct_nofs, ct_diag, ct_external, ct_proc, ct_label, ct_key, ct_active, ct_description FROM code_types order by ct_key desc";
        $sql_num = sqlStatement($query) ;	// total no. of rows
        $num_rows = sqlNumRows($sql_num);
        
        $per_page = $GLOBALS['encounter_page_size'];		// Pagination variables processing
        $page = $_GET["page"];
        
        if(!$_GET["page"])
        {
        	$page=1;
        }
        
        $prev_page = $page-1;
        $next_page = $page+1;
        
        $page_start = (($per_page*$page)-$per_page);
        if($num_rows<=$per_page)
        {
        	$num_pages =1;
        }
        else if(($num_rows % $per_page)==0)
        {
        	$num_pages =($num_rows/$per_page) ;
        }
        else
        {
        	$num_pages =($num_rows/$per_page)+1;
        	$num_pages = (int)$num_pages;
        }
        
        $query .= " LIMIT $page_start , $per_page ";	// limit as per pagination
        $fres = sqlStatement($query);
        if ($fres) {
          $result2 = array();
          for ($iter3 = 0;$frow = sqlFetchArray($fres);$iter3++)
            $result2[$iter3] = $frow;
          foreach($result2 as $iter3) {
			$groupStatus="Active";
                        if($iter3['ct_active'] == 0){
                           $groupStatus="Deactive"; 
                        }
			
          
    ?>
    <tr height="22">
       <td valign="top" class=""><b><span><?php echo htmlspecialchars($iter3{ct_key});?></a></b>&nbsp;</td>
       <td valign="top" class=""><?php echo htmlspecialchars($iter3{ct_id}); ?>&nbsp;</td>
       <td valign="top" class=""><?php echo htmlspecialchars($iter3{ct_description}); ?>&nbsp;</td>
       <td valign="top" class=""><?php echo htmlspecialchars(teratmentsInGroup($iter3{ct_id})); ?>&nbsp;</td>
       <td><?php echo htmlspecialchars($groupStatus);?>&nbsp;</td>
       <td class="newtextcenter" >
            <a title="edit" class="iframe iconanchor" href='trGroupEdit.php?trgCode=<?php echo $iter3{ct_id};?>'  onclick='top.restoreSession()'><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
           <?php
           if($iter3['ct_active'] == 0){
           ?>
           <a title="activate" class="iconanchor"  href='trGroupList.php?trgCode=<?php echo $iter3{ct_id};?>&mode=activate'><span class=" glyphicon glyphicon-ok-circle"></span></a>&nbsp;
           <?php
           }else {
           ?>
           <a title="deactivate" class="iconanchor"  href='trGroupList.php?trgCode=<?php echo $iter3{ct_id};?>&mode=deactivate'><span class=" newdeactivate"></span></a>&nbsp;
           <?php    
           }
           ?>
           <a title="delete" class="iconanchor" href='trGroupList.php?trgCode=<?php echo $iter3{ct_id};?>&mode=delete'><span class=" glyphicon glyphicon-trash"></span></a>&nbsp;       
       </td>
       
    </tr>
<?php
  }
}
 if (count($result2)<=0)
  {?>
  <tr height="25">
		<td colspan="3"  style="text-align:center;font-weight:bold;"> <?php echo xl( "Currently there are no facilities." ); ?></td>
	</tr>
  <?php }
?>
	</table>
<?php // Pagination Displaying Section?>
<table style="margin-top:15px;margin-left:15px;">
    <tbody>
        <tr>
            <td class="text">
                <?php if($num_rows > 0) {?>
                <span class='paging'>
                <span class="pagingInfo">Total Records: <?php echo $num_rows;?></span> Page :
                <?php
                    if($prev_page) {
                        echo " <span class='prev' style='margin:0 4px 0 5px'><a href='$_SERVER[SCRIPT_NAME]?page=$prev_page&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'><< Prev</a></span> ";
                    }

                    if($num_rows > 0){
                        for($i=1; $i<=$num_pages; $i++){
                            if($i != $page)
                            {
                                echo "<span><a href='$_SERVER[SCRIPT_NAME]?page=$i&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'>$i</a></span>";
                            }
                            else
                            {
                                echo "<span class='current'>$i</span>";
                            }
                        }
                    }
                    if($page!=$num_pages){
                        echo "<span class='next'><a href ='$_SERVER[SCRIPT_NAME]?page=$next_page&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'>Next >></a></span>";
                    } 
                ?>
                </span>
                <?php } ?>
            </td>
        </tr>
    </tbody>
</table>
<!-- stuff for the popup calendar -->
        </div>
    </div>
</div>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "jAlert('$alertmsg');\n";
  }
?>
</script>

</body>
</html>
