<?php
include_once("../globals.php");
?>

<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel='stylesheet' href="../../css/bootstrap.css" type="text/css">
<style type="text/css">
      .hidden {
        display:none;
      }
      .visible{
        display:block;
      }
      .topnav a{color:#fff;}
</style>
<script type="text/javascript" language="javascript">
function toencounter(rawdata) {
//This is called in the on change event of the Encounter list.
//It opens the corresponding pages.
	document.getElementById('EncounterHistory').selectedIndex=0;
	if(rawdata=='')
	 {
		 return false;
	 }
	else if(rawdata=='New Encounter')
	 {
	 	top.window.parent.left_nav.loadFrame2('nen1','RBot','forms/newpatient/new.php?autoloaded=1&calenc=')
		return true;
	 }
	else if(rawdata=='Past Encounter List')
	 {
	 	top.window.parent.left_nav.loadFrame2('pel1','RBot','patient_file/history/encounters.php')
		return true;
	 }
    var parts = rawdata.split("~");
    var enc = parts[0];
    var datestr = parts[1];
    var f = top.window.parent.left_nav.document.forms[0];
	frame = 'RBot';
    if (!f.cb_bot.checked) frame = 'RTop'; else if (!f.cb_top.checked) frame = 'RBot';

    top.restoreSession();
<?php if ($GLOBALS['concurrent_layout']) { ?>
    parent.left_nav.setEncounter(datestr, enc, frame);
    parent.left_nav.setRadio(frame, 'enc');
    top.frames[frame].location.href  = '../patient_file/encounter/encounter_top.php?set_encounter=' + enc;
<?php } else { ?>
    top.Title.location.href = '../patient_file/encounter/encounter_title.php?set_encounter='   + enc;
    top.Main.location.href  = '../patient_file/encounter/patient_encounter.php?set_encounter=' + enc;
<?php } ?>
}
function showhideMenu() {
	var m = parent.document.getElementById("fsbody");
	var targetWidth = '0,*';
	if (m.cols == targetWidth) {
		m.cols = '<?php echo $GLOBALS['gbl_nav_area_width'] ?>,*';
		document.getElementById("showMenuLink").innerHTML = '<span class="glyphicon glyphicon-align-justify"></span>';
	} else {
		m.cols = targetWidth;
		document.getElementById("showMenuLink").innerHTML = '<span class="glyphicon glyphicon-align-justify"></span>';
	}
}
</script>
</head>
<body class="body_title">

<?php
$res = sqlQuery("select * from users where username='".$_SESSION{"authUser"}."'");
?>
    <div class="container-fluid topnav">
        <table cellspacing="0" cellpadding="0" width="100%" class="table" height="100%" style="padding-bottom: 20px;">
<tr>
<td align="left">
<?php if ($GLOBALS['concurrent_layout']) { ?>
    
    <div class="row" style="margin-left: -23px;">
        <div class="col-xs-2 " style="margin-top:25px; padding: 0;"> <b> <a class="btn btn-sm btn-warning"  href="main_title.php" id='showMenuLink' onclick='javascript:showhideMenu();return false;'><span class="glyphicon glyphicon-align-justify"></span></a></B></div>
        <div class="col-xs-1" style="margin-left: -33px"><a href="main_screen.php?auth=login&site=default" target="_parent"><img src="../pic/clove-sm-logo.png" alt="logo" ></a></div>
</div>
	
<?php } else { ?>
&nbsp;
<?php } ?>
</td>
<td style="margin:3px 0px 3px 0px;vertical-align:middle;">
        <div style='margin-left:10px; float:left; display:none' id="current_patient_block">
            <span class='text'><?php xl('Patient','e'); ?>:&nbsp;</span><span class='title_bar_top' id="current_patient"><b><?php xl('None','e'); ?></b></span>
        </div>
</td>
<td style="margin:3px 0px 3px 0px;vertical-align:middle;" align="left">
	<table cellspacing="0" cellpadding="1" ><tr><td>
		<div style='margin-left:5px; float:left; display:none' id="past_encounter_block">
			<span class='title_bar_top' id="past_encounter"><b><?php echo htmlspecialchars( xl('None'), ENT_QUOTES) ?></b></span>
		</div></td></tr>
	<tr><td valign="baseline" align="center">	
        <div style='display:none' class='text' id="current_encounter_block" >
            <span class='text'><?php xl('Selected Encounter','e'); ?>:&nbsp;</span><span class='title_bar_top' id="current_encounter"><b><?php xl('None','e'); ?></b></span>
        </div></td></tr></table>
</td>

<td align="right">
	<table cellspacing="0" cellpadding="3"><tr>
		<td align="right" class="text" style="vertical-align:text-bottom;color:#fff;">
                    <span class="text title_bar_top" title="<?php echo htmlspecialchars( xl('Authorization group') .': '.$_SESSION['authGroup'], ENT_QUOTES); ?>"><?php echo htmlspecialchars($res{"fname"}.' '.$res{"lname"},ENT_NOQUOTES); ?></span>
                    &nbsp;|&nbsp;
                    <a href='main_title.php' class="btn btn-sm btn-warning" onclick="javascript:parent.left_nav.goHome();return false;" ><i class="glyphicon glyphicon-home"></i></a>
		&nbsp;&nbsp;
		<td align="right" style="vertical-align:top;"><a href="<?php echo $cakePhpFullUrl.'users/logout';?>" target="_top" class="btn btn-warning btn-sm" style='float:right;' id="logout_link" onclick="top.restoreSession()" >
			<span class='glyphicon glyphicon-off'></span></a></td>
	</tr><tr>
            <td colspan='2' valign="baseline" align='right' ><B>
			</td>
    	</tr></table>
</td>
</tr>
</table>
    </div>
<script type="text/javascript" language="javascript">
parent.loadedFrameCount += 1;
</script>

</body>
</html>
