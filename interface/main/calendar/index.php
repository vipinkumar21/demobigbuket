<?php
// include base api
//$ignoreAuth = true;
include_once("../../globals.php");
include_once("$srcdir/calendar.inc");
include_once("$srcdir/patient.inc");
include "includes/pnre.inc.php";
include 'includes/pnAPI.php';
$disallowed = $_SESSION['navigationAccess'];
//print_r($disallowed);
?>
<html>
    <head>

        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <link rel=stylesheet href="../../themes/bootstrap.css" type="text/css">
        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="../../../js/bootstrap.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container-fluid" id='wrap'>
            <div class="row home_box">
        <div class="col-md-6 mb-2">
                    <div class="panel panel-gray box-heght">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-book huge"></i>
                                </div>
                                <div class="col-xs-9 text-right">

                                    <div>Inventory</div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php if (acl_check('inventory', 'inv_mas_access')){?>
                                <li role="presentation" class="active"><a href="#Masters" aria-controls="home" role="tab" data-toggle="tab">Masters</a></li>
                                <?php }?>
                                <li role="presentation"><a href="#Documents" aria-controls="profile" role="tab" data-toggle="tab">Documents</a></li>
                                <li role="presentation"><a href="#Reports" aria-controls="messages" role="tab" data-toggle="tab">Reports</a></li>
                                <?php if (acl_check('inventory', 'inv_mas_access')){?>
                                <li role="presentation"><a href="#ImportMaster" aria-controls="messages" role="tab" data-toggle="tab">Import Master CSV</a></li>
                                <?php }
                                if (acl_check('inventory', 'inv_mas_access')){?>
                                <li role="presentation"><a href="#ImportDoc" aria-controls="messages" role="tab" data-toggle="tab">Import Doc CSV</a></li>
                                <?php 
                                }
                                ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <?php if (acl_check('inventory', 'inv_mas_access')){?>
                                <div role="tabpanel" class="tab-pane active" id="Masters">
                                    <ul class="list-group">
                                        <?php if (acl_check('inventory', 'im_cat_list')){?> <li class="list-group-item"><i class='glyphicon glyphicon-chevron-right space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/category.php">Category</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'im_scat_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-chevron-right space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/subcategory.php">Sub Category</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'im_uom_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-chevron-right space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/uom.php">UOM</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'im_item_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-chevron-right space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/items.php">Item</a></li><?php }?>
                                    </ul>
                                </div>
                                <?php }?>
                                <?php if (acl_check('inventory', 'inv_forms_access')){?>
                                <div role="tabpanel" class="tab-pane <?php if (!acl_check('inventory', 'inv_mas_access')){?>active<?php } ?>" id="Documents">
                                    <ul class="list-group">
                                        <?php if (acl_check('inventory', 'invf_stock_list') || acl_check('inventory', 'invf_adj_list')){ ?>
                                        <li class="list-group-item">
                                            <?php if (acl_check('inventory', 'invf_stock_list')){ ?><div class="pull-left leftmenu"> <i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/stocks.php">Stock</a></div><?php }?>
                                            <?php if (acl_check('inventory', 'invf_adj_list')){ ?><div class="text-left"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/adjustmentList.php">Adjustments</a></div><?php }else{?><div class="text-left">&nbsp;</div><?php }?>
                                        </li>
                                        <?php }?>
                                        <?php if (acl_check('inventory', 'invf_reqin_list') || acl_check('inventory', 'invf_reqout_list')){ ?>
                                        <li class="list-group-item">
                                            <?php if (acl_check('inventory', 'invf_reqin_list')){ ?><div class="pull-left leftmenu">  <i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/requisitionToClinic.php">Request In</a></div><?php }?>
                                            <?php if (acl_check('inventory', 'invf_reqout_list')){ ?><div class="text-left"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/requisitionFromClinic.php">Request Out</a></div><?php }?>
                                        </li>
                                        <?php }?>
                                        <?php if (acl_check('inventory', 'invf_tran_in_list') || acl_check('inventory', 'invf_tran_out_list')){ ?>
                                        <li class="list-group-item">
                                            <?php if (acl_check('inventory', 'invf_tran_in_list')){ ?><div class="pull-left leftmenu">  <i class='glyphicon glyphicon-list-alt  space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/transferToClinic.php">Transfer In</a></div><?php }?>
                                            <?php if (acl_check('inventory', 'invf_tran_out_list')){ ?><div class="text-left"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/transferFromClinic.php">Transfer Out</a></div><?php }?>
                                        </li>
                                        <?php }?>
                                        <?php if (acl_check('inventory', 'invf_cons_list') || acl_check('inventory', 'invf_item_issue_list')){ ?>
                                        <li class="list-group-item">
                                            <?php if (acl_check('inventory', 'invf_cons_list')){ ?><div class="pull-left leftmenu"> <i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/consumption.php">Consumption</a></div><?php }?>
                                            <?php if (acl_check('inventory', 'invf_item_issue_list')){ ?><div class="text-left"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/issue_return.php">Internal Issues</a></div><?php }?>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                                <?php }?>
                                <?php if (acl_check('inventory', 'inv_adm_rep_access')){?>
                                <div role="tabpanel" class="tab-pane" id="Reports">
                                    <ul class="list-group">
                                        <?php if (acl_check('inventory', 'iar_stock_leadger')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt  space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/stock_leadger_report.php">Stock Ledger</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'iar_consum')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/consumption_report.php">Consumption</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'iar_batch_exp')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/batch_expiry_report.php">Batch Expiry</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'iar_adj') || acl_check('inventory', 'iar_stock_blance')){ ?>
                                        <li class="list-group-item">
                                            <?php if (acl_check('inventory', 'iar_adj')){ ?><div class="pull-left leftmenu"> <i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/adjustment_report.php">Adjustments</a></div><?php }?>
                                            <?php if (acl_check('inventory', 'iar_stock_blance')){ ?><div class="text-left"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/stock_balance_report.php">Stock Balance</a></div><?php }?>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                                <?php } else if (acl_check('inventory', 'inv_cln_rep_access')){
                                ?>
                                <div role="tabpanel" class="tab-pane" id="Reports">
                                    <ul class="list-group">
                                        <?php if (acl_check('inventory', 'icr_stock_leadger')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt  space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/stock_leadger_report_clinic.php">Stock Ledger</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'icr_consum')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/consumption_report_clinic.php">Consumption</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'icr_batch_exp')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/batch_expiry_report_clinic.php">Batch Expiry</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'icr_adj') || acl_check('inventory', 'icr_stock_balance')){ ?>
                                        <li class="list-group-item">
                                            <?php if (acl_check('inventory', 'icr_adj')){ ?><div class="pull-left leftmenu"> <i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/adjustment_report_clinic.php">Adjustments</a></div><?php }?>
                                            <?php if (acl_check('inventory', 'icr_stock_balance')){ ?><div class="text-left"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/stock_balance_report_clinic.php">Stock Balance</a></div><?php }?>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                                <?php } 
                                    if (acl_check('inventory', 'inv_mas_access')){ // Upload CSV for Inventory
                                ?>
                                <div role="tabpanel" class="tab-pane" id="ImportMaster">
                                    <ul class="list-group">
                                        <?php if (acl_check('inventory', 'im_cat_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt  space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/upload_category.php">Category</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'im_cat_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt  space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/upload_subcategory.php">Sub Category</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'im_cat_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/upload_uom.php">UOM</a></li><?php }?>
                                        <?php if (acl_check('inventory', 'im_cat_list')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt  space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/upload_item.php">Item</a></li><?php }?>
                                    </ul>
                                </div>                                
                                <?php }  
                                    if (acl_check('inventory', 'inv_mas_access') && acl_check('inventory', 'im_cat_list')){ // Upload CSV for Inventory
                                ?>
                                <div role="tabpanel" class="tab-pane" id="ImportDoc">
                                    <ul class="list-group">
                                        <li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/inventory/upload_stocks.php">Stock</a></li>

                                    </ul>
                                </div>
                                <?php }?>
                            </div>

                        </div>

                    </div>
                </div>
                <?php if (!$disallowed['adm']) { ?>
                <div class="col-md-6 mb-2">
                    <div class="panel panel-gray box-heght">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-stats huge"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Reports</div>
                                </div>
                            </div>
                        </div>
                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl;?>ReportContacts">Contact</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl ; ?>ReportInvoices">Invoice</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon glyphicon-usd space'></i><a href="<?php echo $cakePhpFullUrl ; ?>ReportPayments">Payment</a></div>
                            </li>
                            <li class="list-group-item">                   
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl ; ?>InvoiceQualityReports">Invoice Quality</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl ; ?>ReportWorkDone">Work Done</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl ; ?>ReportAppointments">Appointment</a></div>
                            </li>
                            <li class="list-group-item">                              
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/reports/daily_util_report.php">Daily Utilization</a></div>
                                <div class="pull-left leftmenu" ><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/reports/monthly_util_report.php">Monthly Utilization</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl ; ?>ReportTreatment">Treatments</a></div>
                            </li>
                            <li class="list-group-item">
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/reports/appt_slots_report.php">Appt Slots</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/reports/appt_shows_report.php">Shows/NoShow</a></div>
                                <div class="pull-left leftmenu"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $cakePhpFullUrl ; ?>AgeingReports/report">Ageing</a></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php
                }
                ?>
                <?php if (acl_check('Feedback', 'fdbk_access')) { ?>
                <div class="col-md-4 mb-2">
                    <div class="panel panel-gray box-heght">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-pencil huge"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Feedback</div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item"><i class='glyphicon glyphicon-plus space'></i><a href="<?php echo $full_url ; ?>/interface/feedback/feedback_add.php">Add Feedback</a></li>
                            <li class="list-group-item"><i class='glyphicon glyphicon-search space'></i><a href="<?php echo $full_url ; ?>/interface/feedback/feedback_report.php">Search Feedback</a></li>
                            <li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/feedback/feedback_referral_report.php">Refferal Report</a></li>
                            <li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/feedback/patient-feedback/fcrDashBoard.php">Clinic Report</a></li>
                            
                        </ul>

                        <!-- List group -->

                    </div>
                </div>
                <?php }?>
                <div class="col-md-4 mb-2">
                    <div class="panel panel-gray box-heght">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-briefcase huge"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Miscellaneous</div>
                                </div>
                            </div>
                        </div>

                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item"><i class='glyphicon glyphicon-log-out space'></i><a href="<?php echo $full_url ; ?>/interface/usergroup/user_info.php">Password</a></li>

                        </ul>
                    </div>
                </div>
                <?php if (acl_check('acct', 'audit_report')){ ?>
                <div class="col-md-4 mb-2">
                    <div class="panel panel-gray box-heght">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-dashboard huge"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>AC/Audit</div>
                                </div>
                            </div>
                        </div>

                        <ul class="list-group">
                            <?php if (acl_check('acct', 'audit_invoice')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/ac_audit/dashboard.php">Invoice</a></li><?php }?>
                            <?php if (acl_check('acct', 'audit_reciept')){ ?><li class="list-group-item"><i class='glyphicon glyphicon-list-alt space'></i><a href="<?php echo $full_url ; ?>/interface/ac_audit/reciept_dashboard.php">Receipts</a></li><?php }?>

                        </ul>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
    </body>
</html>