<?php
 $fake_register_globals=false;
 $sanitize_all_escapes=true;

 require_once("../../globals.php");
 require_once("$srcdir/patient.inc");
 require_once("$srcdir/forms.inc");
 require_once("$srcdir/calendar.inc");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/encounter_events.inc.php");
 require_once("$srcdir/acl.inc");
 require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");
require_once ($GLOBALS['srcdir'] . "/maviq_phone_api.php");
require_once($GLOBALS['srcdir'] . "/event_notification.php");
// Get the providers list.
$facilList = $_REQUEST['facilityId'];
if ($GLOBALS['restrict_user_facility']) {
	$ures = sqlStatement("SELECT id, username, fname, lname FROM users as us, users_facility as uf WHERE " .
	"authorized != 0 AND active = 1 and ((us.facility_id = $facilList) OR (uf.facility_id = $facilList AND uf.table_id = us.id)) GROUP BY us.id ORDER BY lname, fname");
}else {
	$ures = sqlStatement("SELECT id, username, fname, lname FROM users WHERE " .
	"authorized != 0 AND active = 1 and facility_id = $facilList ORDER BY lname, fname");
 }
 $returnData = ''; 
// =======================================
// multi providers 
// =======================================
if  ($GLOBALS['select_multi_providers']) {

    //  there are two posible situations: edit and new record

    // this is executed only on edit ($eid)
    if ($eid) {
        if ( $multiple_value ) {
            // find all the providers around multiple key
            $qall = sqlStatement ("SELECT pc_aid AS providers FROM openemr_postcalendar_events WHERE pc_multiple = ?", array($multiple_value) );
            while ($r = sqlFetchArray($qall)) {
                $providers_array[] = $r['providers'];
            }
        } else {
           $qall = sqlStatement ("SELECT pc_aid AS providers FROM openemr_postcalendar_events WHERE pc_eid = ?", array($eid) );
            $providers_array = sqlFetchArray($qall);
        }
    }
    
    // build the selection tool
    $returnData.= "<select name='form_provider[]' style='width:100%' multiple='multiple' size='5' >";
    
    while ($urow = sqlFetchArray($ures)) {
        $returnData.= "<option value='" . attr($urow['id']) . "'";
    
        if ($userid) {
            if ( in_array($urow['id'], $providers_array) || ($urow['id'] == $userid) ) $returnData.= " selected";
        }
    
        $returnData.= ">" . text($urow['lname']);
        if ($urow['fname']) $returnData.= ", " . text($urow['fname']);
        $returnData.= "</option>\n";
    }
    
    $returnData.= '</select>';

// =======================================
// single provider 
// =======================================
} else {

    if ($eid) {
        // get provider from existing event
       $qprov = sqlStatement ("SELECT pc_aid FROM openemr_postcalendar_events WHERE pc_eid = ?", array($eid) );
        $provider = sqlFetchArray($qprov);
        $defaultProvider = $provider['pc_aid'];
    }
    else {
      // this is a new event so smartly choose a default provider 
    /*****************************************************************
      if ($userid) {
        // Provider already given to us as a GET parameter.
        $defaultProvider = $userid;
      }
        else {
        // default to the currently logged-in user
        $defaultProvider = $_SESSION['authUserID'];
        // or, if we have chosen a provider in the calendar, default to them
        // choose the first one if multiple have been selected
        if (count($_SESSION['pc_username']) >= 1) {
          // get the numeric ID of the first provider in the array
          $pc_username = $_SESSION['pc_username'];
          $firstProvider = sqlFetchArray(sqlStatement("select id from users where username='".$pc_username[0]."'"));
          $defaultProvider = $firstProvider['id'];
        }
      }
    }

    echo "<select name='form_provider' style='width:100%' />";
    while ($urow = sqlFetchArray($ures)) {
        echo "    <option value='" . $urow['id'] . "'";
        if ($urow['id'] == $defaultProvider) echo " selected";
        echo ">" . $urow['lname'];
        if ($urow['fname']) echo ", " . $urow['fname'];
        echo "</option>\n";
    }
    echo "</select>";
    *****************************************************************/
      // default to the currently logged-in user
      $defaultProvider = $_SESSION['authUserID'];
      // or, if we have chosen a provider in the calendar, default to them
      // choose the first one if multiple have been selected
      if (count($_SESSION['pc_username']) >= 1) {
        // get the numeric ID of the first provider in the array
        $pc_username = $_SESSION['pc_username'];
        $firstProvider = sqlFetchArray(sqlStatement("select id from users where username=?", array($pc_username[0]) ));
        $defaultProvider = $firstProvider['id'];
      }
      // if we clicked on a provider's schedule to add the event, use THAT.
      if ($userid) $defaultProvider = $userid;
    }
    $returnData.= "<select name='form_provider' style='width:100%' >";
    while ($urow = sqlFetchArray($ures)) {
      $returnData.= "<option value='" . attr($urow['id']) . "'";
      if ($urow['id'] == $defaultProvider) $returnData.= " selected";
      $returnData.= ">" . text($urow['lname']);
      if ($urow['fname']) $returnData.= ", " . text($urow['fname']);
      $returnData.= "</option>";
    }
    $returnData.= "</select>";
    /****************************************************************/
}
echo $returnData;
?>