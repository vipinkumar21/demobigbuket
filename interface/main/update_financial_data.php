<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.

 require_once("../globals.php");
 require_once("$srcdir/patient.inc");
 require_once("$srcdir/formatting.inc.php");
  require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");
// Prepare a string for CSV export.
function qescape($str) {
  $str = str_replace('\\', '\\\\', $str);
  return str_replace('"', '\\"', $str);
}

?>
<html>
<head>
<?php html_header_show();?>
<title><?php xl('Update Financial Year','e'); ?></title>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/overlib_mini.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/textformat.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<style type="text/css">

/* specifically include & exclude from printing */
@media print {
    #report_parameters {
        visibility: hidden;
        display: none;
    }
    #report_parameters_daterange {
        visibility: visible;
        display: inline;
		margin-bottom: 10px;
    }
    #report_results table {
       margin-top: 0px;
    }
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
	#report_results {
		width: 100%;
	}
}

</style>

</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<span class='title'><?php xl('Update','e'); ?> - <?php xl('Financial Year','e'); ?></span>
<div id="report_parameters">
<table>
 <tr>  
  <td align='left' valign='middle' height="100%">
	<?php 
		$currentTime = time();
		if(4 == date("n", $currentTime) && 1 == date("j", $currentTime) && 0 <= date("G", $currentTime) && 8 >= date("G", $currentTime)){
			//SELECT CONCAT_WS('/', CONCAT_WS('-', DATE_FORMAT(CURRENT_DATE(),'%Y'), DATE_FORMAT(CURRENT_DATE(),'%y')+1), SUBSTRING_INDEX(mask_invoice_number_pre, '/', -1)) AS inv, CONCAT_WS('/', CONCAT_WS('-', DATE_FORMAT(CURRENT_DATE(),'%Y'), DATE_FORMAT(CURRENT_DATE(),'%y')+1), SUBSTRING_INDEX(mask_reciept_number_pre, '/', -1)) AS rec FROM facility
			sqlStatement("UPDATE facility SET `mask_invoice_number_pre` = CONCAT_WS('/', CONCAT_WS('-', DATE_FORMAT(CURRENT_DATE(),'%Y'), DATE_FORMAT(CURRENT_DATE(),'%y')+1), SUBSTRING_INDEX(mask_invoice_number_pre, '/', -1)), mask_reciept_number_pre = CONCAT_WS('/', CONCAT_WS('-', DATE_FORMAT(CURRENT_DATE(),'%Y'), DATE_FORMAT(CURRENT_DATE(),'%y')+1), SUBSTRING_INDEX(mask_reciept_number_pre, '/', -1)), virtual_mask_reciept_number_pre = CONCAT_WS('/', CONCAT_WS('-', DATE_FORMAT(CURRENT_DATE(),'%Y'), DATE_FORMAT(CURRENT_DATE(),'%y')+1), SUBSTRING_INDEX(virtual_mask_reciept_number_pre, '/', -1))");
			sqlStatement("UPDATE facility_ids SET fi_inseq=1, fi_rseq=1");
			echo 'Financial data updated successfuly.';
		}else {
			echo 'You cannot update your records. It can be updated on 1st Apr between 0 AM to 8 AM.';
		}
	?>
  </td>
 </tr>
</table>
</div> <!-- end of parameters -->
</body>
</html>