<?php

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
// Get the providers list.
$facilList = $_REQUEST['facilityId'];
$providerId =$_REQUEST['providerId'];

if ($GLOBALS['restrict_user_facility']) {
    if($facilList=='0'){
        $fac=userFacilitys();
    }else{
       $fac=$facilList;   
    }
    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
    $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
    $sql .= " WHERE ";
    $sql .= "uf.facility_id IN (" . $fac . ") ";
    /*if (!empty($facilList)) {
        $sql .= "uf.facility_id = $facilList ";
    } else {
        $fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = " . $userid . " AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
        //$fRes = sqlStatement($fsql);
        $fRes = $pdoobject->custom_query($fsql);
        $facilityIds = '';
        //while ($fRow = sqlFetchArray($fRes)) {
        foreach($fRes as $fRow) {    
            if (!empty($facilityIds)) {
                $facilityIds .= ', ' . $fRow['facility_id'];
            } else {
                $facilityIds = $fRow['facility_id'];
            }
        }
        $sql .= "uf.facility_id IN (" . $facilityIds . ") ";
    }*/
    $sql .= "AND gag.id IN (12, 18, 22, 23, 24) AND u.active='1' ";
    $sql .= " GROUP BY u.id DESC";
    $ures = $pdoobject->custom_query($sql);
} else {
    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename ";
    $sql .= "FROM users AS u  ";
    $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
    $sql .= "WHERE gag.id IN (12, 18, 22, 23, 24) AND u.authorized !=0 GROUP BY u.id DESC";
}
$returnData = '';
// default to the currently logged-in user
$defaultProvider = $_SESSION['authUserID'];
$selected_attr = '';
$option_value = '';
$option_content = '';
$alreadySelected = 0;
$allproviderContent = "All Doctors";
$returnData.="<select name=\"form_provider\" id=\"form_provider\" class=\"form-control input-sm\">";
    if (empty($providerId)) {
        $returnData.=  "<option value='0' selected='selected'>$allproviderContent</option>\n";
        $alreadySelected = 1;
    } else {
        $returnData.=  "<option value='0'>$allproviderContent</option>\n";
    } 
     
    foreach($ures as $row){  
    $provider_id = $row['id'];
        $selected_attr = '';
        if ($provider_id==$providerId) {
            $selected_attr = (!$alreadySelected) ? ' selected="selected"' : '';
            $option_value = htmlspecialchars($provider_id, ENT_QUOTES);
            $alreadySelected=1;
        }  elseif ($provider_id ==$defaultProvider) {
            $selected_attr = (!$alreadySelected) ? ' selected="selected"' : '';
            $option_value = htmlspecialchars($defaultProvider, ENT_QUOTES);
        }else{
            $option_value = htmlspecialchars($provider_id, ENT_QUOTES); 
        }
        $option_content = htmlspecialchars($row['completename'], ENT_NOQUOTES);
       
        $returnData.= "<option value=\"$option_value\" $selected_attr>" . $option_content . "</option>\n";
        
    
}
$returnData.="</select>";
echo $returnData;
