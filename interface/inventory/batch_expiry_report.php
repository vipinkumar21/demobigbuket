<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

$thisauth = $invgacl->acl_check('inventory', 'iar_batch_exp', 'users', $_SESSION['authUser']);
$thisAuthDoctor = $invgacl->acl_check('inventory', 'icr_batch_exp', 'users', $_SESSION['authUser']);
if (!$thisauth && !$thisAuthDoctor) {
    die(xlt('Not authorized'));
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$show_available_times = false;
$ORDERHASH = array(
    'invist_id' => 'invist_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
    'invist_expiry' => 'invist_expiry DESC',
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invist_expiry';
$orderby = $ORDERHASH[$form_orderby];
if(isset($_REQUEST['form_facility'])){
     $_SESSION['cid']=$_REQUEST['form_facility'];
}else{
   $_REQUEST['form_facility']= $_SESSION['cid']; 
}
$form_facility = isset($_REQUEST['form_facility']) ? $_REQUEST['form_facility'] : $_SESSION['Auth']['User']['facility_id'];
$form_date = $_REQUEST['form_expiry_date'];
$form_to_date = $_REQUEST['to_expiry_date'];
$provider = $_REQUEST['form_provider'];
$batch = $_REQUEST['form_batch'];
$item = $_REQUEST['form_item'];
$catid = $_REQUEST['catid'];
$sellable = $_REQUEST['form_sellable'];
$searchParam = '';
$fileName = "batch_expiry_" . date("Ymd_his") . ".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvBatchExpiry') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Batch/Expiry Report', 'e'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        </head>
        <body>
            <!-- page -->
            <div id="page" data-role="page" class="ui-content">
                <!-- header -->
                <?php include_once("oi_header.php"); ?>
                <!-- header -->
                <!-- contentArea -->
                <div id="wrapper" data-role="content" role="main">
                    <!-- wrapper -->
                    <div class='themeWrapper' id='rightpanel'>  
                        <div class='containerWrap'>
                            <!-- pageheading -->
                            <div class='col-sm-12 borbottm'>
                                    <?php include_once("inv_links.html"); ?>
                                <h1><?php xl('Batch/Expiry Report', 'e'); ?></h1>
                                <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?></div>
                            </div>
                            <!-- pageheading -->
                            <!-- mdleCont -->
                            <form method='post' name='theform' id='theform' action='batch_expiry_report.php' class="botnomrg">
                                <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                                <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                                <!-- leftPart -->
                                <div class="filterWrapper">
                                    <!-- Facility --> 
                                    <div class="ui-block">
                                        <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('form_facility', '', 'form_facility', $form_facility, $_SESSION['authId'], $userFacilityRestrict, $pdoobject);
                                        ?>
                                    </div>
                                    <!-- Facility --> 
                                    <!-- Expiry --> 
                                    <div class="ui-block form_to_date_bx">
                                        <input type='text' placeholder='From Date' name='form_expiry_date_in' id="form_expiry_date_in" value='' />
                                        <input type='hidden' name='form_expiry_date' id='form_expiry_date' value='<?php echo ($form_date != '') ? $form_date : '' ?>' />
                                    </div>
                                    <!-- Expiry --> 
                                    <!-- to --> 
                                    <div class="ui-block form_to_date_bx">
                                        <input type='text' placeholder='To Date' name='to_expiry_date_in' id="to_expiry_date_in" value='' />
                                        <input type='hidden' name='to_expiry_date' id='to_expiry_date' value='<?php echo ($form_to_date != '') ? $form_to_date : '' ?>' />
                                    </div>
                                    <!-- to --> 
                                    <!-- Saleable -->
                                    <div class="ui-block ui-chkbx">
                                            <label for="form_sellable" class="interButt">
                                                <?php xl('Saleable', 'e'); ?>
                                            </label>
                                            <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_REQUEST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
                                        </div> 
                                    <!-- Saleable -->
                                    <div class='ui-block wdth25'>                                                                      
                                        <a class="pull-right btn_bx" id='reset_form1' href="batch_expiry_report.php?form_facility=<?php echo $_SESSION['reset_cid']; ?>">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-retweet icon"></span>
                                            </span>
                                            <b class="btn-text">Reset</b>
                                        </a>
                                        <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "csvBatchExpiry");
                                                    $("#theform").submit();'>
                                            <span class="new-btnWrap btn">
                                                <span class="csv_icon"></span>
                                            </span>
                                            <b class="btn-text"><?php xl('Export', 'e'); ?></b>
                                        </a>

                                        <a id='advanceOptions' class="pull-right" href="javascript:void(0)">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-zoom-in icon"></span>
                                            </span>
                                            <b class="btn-text">Advance</b>
                                        </a>

                                        <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "viewBatchExpiry");
                                                    $("#theform").submit();'>
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-search icon5"></span>
                                            </span>
                                            <b class="btn-text">Search</b>
                                        </a>
                                    </div>
                                    <div class='<?php if (empty($batch) && empty($catid) && empty($item)) {
                                            echo "dnone";
                                        } ?>' id='advanceOptionsToggle'>
                                        <!-- Category --> 
                                        <div class="ui-block">
                                            <select class="form-control input-sm" name='catid' id='catid'>
                                                <?php
                                                if (empty($row['inv_im_catId'])) {
                                                    ?>
                                                    <option value='0' selected="selected">All Category</option>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                $cres = $pdoobject->fetch_multi_row("invcategories", array("id as invcat_id", "name as invcat_name"), array('is_active' => '1', 'deleted' => '0'));
                                                foreach ($cres as $crow) {
                                                    ?>
                                                    <option value='<?php echo $crow['invcat_id'] ?>' <?php
                                                    if ($_REQUEST['catid'] == $crow['invcat_id']) {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?>><?php echo $crow['invcat_name'] ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </div>
                                        <!-- Categorys --> 
                                        <!-- Item --> 
                                        <div class="ui-block">
                                            <input type='text' placeholder='Item' name='form_item' id="form_item" value='<?php echo ($item != '') ? $item : ''; ?>' title=''>
                                        </div>
                                        <!-- Item --> 
                                        <!-- Batch --> 
                                        <div class="ui-block">
                                            <input type='text' placeholder='Batch' name='form_batch' id="form_batch" value='<?php echo ($batch != '') ? $batch : ''; ?>' title=''>
                                        </div>
                                        <!-- Batch --> 
                                        
                                    </div>
                                </div>
                                <!-- leftPart -->
                                <!-- table -->
                            <?php
                            } // end not form_csvexport
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_POST['form_csvexport'] || empty($form_facility) || $_REQUEST['form_facility']) {
                                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvBatchExpiry') {
                                    ob_clean();
                                    // CSV headers:
                                    echo '"' . xl('Clinic Name') . '",';
                                    echo '"' . xl('Item Name') . '",';
                                    echo '"' . xl('Batch#') . '",';
                                    echo '"' . xl('Expiry Date') . '",';
                                    echo '"' . xl('Price') . '",';
                                    echo '"' . xl('Quantity') . '"' . "\n";
                                } else {
                                    ?>
                                    <div id='' class='tableWrp pb-2'>
                                        <div class="dataTables_wrapper no-footer">
                                            <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                    <!-- added for better print-ability -->
                                                <?php
                                                }
                                                $message = "";
                                                $rang = getDateRange($form_date, $form_to_date, $form_facility);
                                                $where = " AND DATE(invis.invist_expiry) >= '" . $rang["FromDate"] . "' AND DATE(invis.invist_expiry) <= '" . $rang["ToDate"] . "'";
                                                $searchParam .= ' Expiry Date From = ' . $rang["FromDate"] . ' | Expiry Date To = ' . $rang["ToDate"] . ' | ';

                                                $facilityIdList = $form_facility == 0 ? userFacilitys() : $form_facility;

                                                $where .= " AND invis.invist_clinic_id IN (" . $facilityIdList . ")";
                                                $searchParam .= $form_facility == 0 ? "Facility = All |" : "Facility = " . $facilityIdList . " | ";
                                                $where .=!empty($provider) ? "  AND invistr.invistr_createdby = '" . $provider . "'" : "";
                                                $searchParam .=!empty($provider) ? ' Provider = ' . $provider . ' | ' : "Provider = All | ";
                                                $where .=!empty($catid) ? " AND FIND_IN_SET(" . $catid . ",im.inv_im_catId)" : "";
                                                $searchParam .=!empty($catid) ? ' Category = ' . $catid . ' | ' : "Category = All | ";
                                                $where .=!empty($batch) ? " AND invis.invist_batch = '" . $batch . "'" : "";
                                                $searchParam .=!empty($batch) ? ' Batch# = ' . $batch . ' | ' : "";
                                                $where .=!empty($sellable) ? " AND im.inv_im_sale = '" . $sellable . "'" : "";
                                                $searchParam .=!empty($sellable) ? ' Saleable Items = Yes | ' : "Saleable Items = No | ";
                                                $where .=!empty($item) ? " AND im.inv_im_name LIKE '" . $item . "%'" : "";
                                                $searchParam .=!empty($item) ? ' Item = ' . $item . ' | ' : "";

                                                $dateMysqlFormat = getDateDisplayFormat(1);

                                                $query = "SELECT invis.invist_id, invis.invist_itemid,invis.invist_clinic_id As clinicId, invis.invist_batch,
                                                            CASE 
                                                                WHEN (invis.invist_expiry = '0000-00-00') THEN ''
                                                                ELSE DATE_FORMAT(invis.invist_expiry , '%d-%m-%y') 
                                                            END 
                                                                AS formatexpirydate,
                                                                invis.invist_price, invis.invist_quantity, im.inv_im_name, f.name AS clinicname,
                                                                 CASE 
                                                                    WHEN (invis.invist_expiry = '0000-00-00') THEN ''
                                                                    ELSE DATE_FORMAT(invis.invist_created_date , '%d-%m-%y') 
                                                                END 
                                                                    AS formatcreateddate
                                                            FROM inv_item_stock AS invis
                                                            INNER JOIN facility AS f ON f.id = invis.invist_clinic_id
                                                            INNER JOIN inv_item_master AS im ON im.inv_im_id = invis.invist_itemid
                                                            WHERE invis.invist_isdeleted = '0' AND im.inv_im_deleted = '0' " . $where . " ORDER BY $orderby";
                                                $daysRangeNotAllowed = $form_facility == 0 ? $form_facility_all_time_range_valid : $form_facility_time_range_valid;
                                                $daysRangeNotAllowedMessage = $form_facility == 0 ? $form_facility_all_time_range_valid_errorMsg : $form_facility_time_range_valid_errorMsg;
                                                $cronScheduleMessage = $form_facility == 0 ? $form_facility_all_time_range_errorMsg : $form_facility_time_range_errorMsg;
                                                $pdoobjectr=new database("write");
                                                if ($rang['Days'] > $daysRangeNotAllowed) {

                                                    $message = $daysRangeNotAllowedMessage;
                                                } else {
                                                    if ($rang['Cron']) {
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $rang["FromDate"], "toDate" => $rang["ToDate"], "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "batch" => $batch, "facility" => $form_facility))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from " . $rang["FromDate"] . " to " . $rang["ToDate"]; // changable   
                                                        $getFacility = getFacilityName($form_facility);
                                                        $subject = "Batch Expiry Report" . "_" . $getFacility . "_" . date('Y-m-d H:i:s');
                                                        $sellebleCondition = ($sellable==1) ? "Yes" : "No";
                                                        $filters = "From Date =" . $rang["FromDate"] . " ,To Date =" . $rang["ToDate"] . " ,Item Text =" . $item . " ,Saleable =" . $sellebleCondition ." ,Category  =". getCategoryName($catid)." ,Batch=".$batch. " ,Facility =" . $getFacility;
                                                        $sellebleHeader = ($sellable == 1) ? ",Price," : ",";
                                                        $header = "Clinic Name,Item Name,Batch #, Expiry Date $sellebleHeader Quantity";
                                                        $includeArr = array('clinicname', 'inv_im_name', 'invist_batch', 'formatexpirydate');
                                                        if ($sellable) {
                                                            array_push($includeArr, "invist_price", "invist_quantity");
                                                        } else {
                                                            array_push($includeArr,"invist_quantity");
                                                        }
                                                        $report_data = json_encode(array(
                                                            'header' => $header,
                                                            'include' => $includeArr,
                                                            'name' => 'Batch Expiry Report', 
                                                            'subject' => $subject, 
                                                            'facility' => $getFacility, 
                                                            'filters' => $filters, 
                                                            'query' => $query)
                                                        );
                                                        $msgForReportLog = scheduleReports("batch_expiry_report", $rcsl_requested_filter, $rcsl_report_description, $report_data, $pdoobjectr);
                                                        $message = empty($msgForReportLog) ? $cronScheduleMessage : $msgForReportLog;
                                                  
                                                        } else {

                                                        $message = "";
                                                    }
                                                }

                                                if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvBatchExpiry') {
                                                    $event = "Report Batch/Expiry Export";
                                                } else {
                                                    $event = "Report Batch/Expiry View";
                                                }
                                                 
                                                if ($message) {
                                                    $auditid = debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2, $pdoobjectr); // Cron Schedule Report Auditing Section
                                                } else {
                                                    $auditid = debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 0, $pdoobjectr); // Report Auditing Section
                                                    $num_rows = $pdoobject->custom_query($query, NULL, 1);
                                                    $per_page = $GLOBALS['encounter_page_size'];
                                                    $page = $_GET["page"];
                                                    $curr_URL = $_SERVER['SCRIPT_NAME'] . '?form_refresh=viewBatchExpiry&' . 'form_facility=' . $facility . '&form_expiry_date=' . $form_date . '&to_expiry_date=' . $form_to_date . '&form_item=' . $item . '&form_batch=' . $batch . '&catid=' . $catid . '&form_sellable=' . $sellable . '&';
                                                    $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                                    $page = ($page == 0 ? 1 : $page);
                                                    $page_start = ($page - 1) * $per_page;

                                                    if ($_POST['form_csvexport'] != 'csvConsumption') {
                                                        $query.=" LIMIT $page_start , $per_page";
                                                    }
                                                    $res = $pdoobject->custom_query($query);
                                                    $lastid = "";
                                                   
                                                    ?>
                                                    <?php if ($_POST['form_csvexport'] != 'csvBatchExpiry' && $num_rows > 0) { ?>
                                                        <!-- pagination --> 
                                                            <?php echo $pa_data; ?> 
                                                        <!-- pagination -->
                                                    <?php } ?>

                                                    <?php
                                                    if (!empty($res)) {
                                                        $prevClinic = '';
                                                        $currClinic = '';
                                                        $currItem = ''; 
                                                        $prevItem = '';
                                                        $srCount = 1;
                                                        foreach ($res as $row) {
                                                            $currClinic = $row['clinicId'];
                                                            $currItem = $row['invist_itemid'];
                                                            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvBatchExpiry') {
                                                                echo '"'.qescape($row['clinicname']).'",';
                                                                echo '"'.qescape($row['inv_im_name']).'",';
                                                                echo '"'.qescape($row['invist_batch']).'",';
                                                                echo '"'.qescape($row['formatexpirydate']).'",';
                                                                echo '"'.qescape($row['invist_price'] == '0.00' ? '' : $row['invist_price']).'",';
                                                                echo '"'.qescape($row['invist_quantity']).'"'."\n";
                                                              } else { 
                                                                  if($srCount==1) {
                                                                ?>
                                                                <thead>
                                                                    <tr>
                                                                        <?php if (empty($form_facility)) { ?>
                                                                            <th width='12%'><?php xl('Clinic Name', 'e'); ?></th><?php } ?>
                                                                        <th><?php xl('Item Name', 'e'); ?></th>
                                                                        <th><?php xl('Batch#', 'e'); ?></th>
                                                                        <th width='10%'><?php xl('Expiry Date', 'e'); ?></th>
                                                                        <?php if (isset($_REQUEST['form_sellable']) && $_REQUEST['form_sellable'] != '') { ?>
                                                                            <th width='10%'><?php xl('Price', 'e'); ?></th>
                                                                        <?php } ?>
                                                                        <th width='10%'><?php xl('Quantity', 'e'); ?></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                              <?php }
                                                              if ($currClinic != $prevClinic && empty($form_facility)) { ?>
                                                                <tr bgcolor='<?php echo $bgcolor ?>'>
                                                                    <td><?php echo $row['clinicname']; ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                    <td></td> 
                                                                <?php } ?>
                                                                    <td></td>
                                                                </tr>
                                                                <tr bgcolor='<?php echo $bgcolor ?>'>
                                                                    <td></td>
                                                                    <?php if ($currItem != $prevItem && $currClinic != $prevClinic ) { ?>
                                                                        <td><?php echo $row['inv_im_name']; ?></td>
                                                                                        <?php } else if($currItem == $prevItem && $currClinic != $prevClinic) { ?>
                                                                        <td><?php echo $row['inv_im_name']; ?></td>
                                                                    <?php } else 
                                                                    {?>
                                                                        <td></td>
                                                                    <?php }?>
                                                                    <td><?php echo $row['invist_batch']; ?></td>
                                                                    <td><?php echo $row['formatexpirydate']; ?></td>
                                                                    <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                        <td><?php echo $row['invist_price'] == '0.00' ? '' : $row['invist_price']; ?></td>

                                                                <?php } ?>
                                                                    <td><?php echo $row['invist_quantity']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <tr bgcolor='<?php echo $bgcolor ?>'>
                                                                   <?php if (empty($form_facility)) { ?>
                                                                                    <td></td><?php } ?>
                                                                    <?php if ($currItem != $prevItem) { ?>
                                                                        <td><?php echo $row['inv_im_name']; ?></td>
                                                                    <?php } else { ?>
                                                                        <td></td>
                                                                    <?php } ?>
                                                                    <td><?php echo $row['invist_batch']; ?></td>
                                                                    <td><?php echo $row['formatexpirydate']; ?></td>
                                                                    <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                        <td><?php echo $row['invist_price'] == '0.00' ? '' : $row['invist_price']; ?></td>

                                                                    <?php } ?>
                                                                    <td><?php echo $row['invist_quantity']; ?></td>
                                                                   </tr>

                                                                <?php
                                                            }
                                                        }
                                                        $prevClinic = $row['clinicId'];
                                                        $prevItem = $row['invist_itemid'];
                                                        $srCount++;
                                                    }
                                                }
                                                debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog'], NULL, $pdoobjectr); /// Update Report Auditing Section
                                            }
                                            // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
                                            if ($_POST['form_csvexport'] != 'csvBatchExpiry') {
                                                            ?>

                                                            <?php
                                                            if (!$srCount && empty($message)) {
                                                                ?>
                                                               <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                                <?php
                                                            }
                                                        }
                                           if ($message) {
                                                ?>

                                               <tr>
                                                    <td colspan="8" class='newtextcenter' style="font-size:10pt;"> <?php echo ((!empty($message)) ? $message : 'No Results Found'); ?></td>
                                                </tr>
                                                <!-- </table> -->
                                                <?php
                                            }
                                            if ($_REQUEST['form_csvexport'] != 'csvBatchExpiry') {
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- pagination --> 
                                            <?php echo $pa_data; ?> 
                                        <!-- pagination -->
                                    </div>
                                    <!-- table -->
                                </form>
                                <!-- mdleCont -->
                            </div>
                        </div>
                        <!-- wrapper -->
                    </div>
                    <!-- contentArea -->
                </div>
                <!-- page -->
            </body>
        </html>
        <!-- end of search results --> <?php
    }
} else {
    ?> 
<?php } ?> 
<?php if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvBatchExpiry') { ?>
    <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
    <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
    <input type='hidden' name='form_refresh' id='form_refresh' value='' />
<?php } ?>