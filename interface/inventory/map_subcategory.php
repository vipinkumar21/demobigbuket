<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");

if (!acl_check('inventory', 'im_cat_map')) die(xlt('Not authorized'));

$alertmsg = '';
$result=getActiveMappedSubCat($_GET["id"]);
//print_r($result);
///
if (isset($_POST["mode"])) {
  	echo '
<script type="text/javascript">
<!--
parent.$.fn.fancybox.close();
//-->
</script>

	';
}
///

?>

<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
<link rel="stylesheet" href="../themes/bootstrap.css" type="text/css">
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>
<script src="checkpwd_validation.js" type="text/javascript"></script>

<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
	if (document.forms[0].id.value.length>0) {
		top.restoreSession();		
		document.forms[0].submit();
	} else {
		if (document.forms[0].id.value.length<=0)
		{
			alert("<?php xl('Required field missing: Please select category after that map','e');?>");
			return false;
		}
		
		
	}
}
function validateFileExtension(fld,FieldName) {
   if(!/(\.pdf|\.doc|\.docs)$/i.test(fld)) 
	 {
	   return false;
	  }
	return true;
}

</script>

</head>
<body class="body_top">
	<h3 class="emrh3"><?php xl('Map Sub Category With Category','e'); ?></h3>
	<form name='new_user' method='POST' target="_parent" action="category.php" onsubmit='return top.restoreSession()'>
		<input type='hidden' name='mode' value='update_mapped_scat'>
		<INPUT TYPE="HIDDEN" NAME="id" VALUE="<?php echo $_GET["id"]; ?>">
		<span class="bold"></span>
		<table border='0' cellpadding='0' cellspacing='0' width='100%' class='emrtable' align='center'>
		<tr>
				<td style="width:150px;"><span class="text"><?php xl('Sub Category','e'); ?>: </span></td>
				<td  style="width:220px;">
					<select name='subCategory[]' multiple>
						<?php
						
						$eres = sqlStatement("SELECT invsubcat_id, invsubcat_name FROM inv_subcategory WHERE invsubcat_deleted = '0' ORDER BY invsubcat_id DESC");
						while ($erow = sqlFetchArray($eres)) { 
						?>
							<option value="<?php echo $erow['invsubcat_id'];?>" <?php if(checkMappedSubCat($erow['invsubcat_id'], $result)){ echo 'selected=selected'; }?>><?php echo $erow['invsubcat_name'];?></option>
						<?php
						}
						?>
					</select>
				</td>
		</tr>
		  <tr height="25"><td colspan="4" align='center'>

		<a class="btn btn-warning btn-sm" name='form_save' id='form_save' href='#' onclick="return submitform()">
			<span><?php xl('Save','e');?></span>
		</a>
		<a class="btn btn-warning btn-sm" id='cancel' href='#'>
			<span class='css_button_span large_button_span'><?php xl('Cancel','e');?></span>
		</a>

		  </td></tr>


		</table>
	</form>


<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });

});
</script>
</body>
</html>
