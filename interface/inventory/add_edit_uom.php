<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $uom_id = $_REQUEST['uomid'];
 $info_msg = "";
 $tmpl_line_no = 0;

 if (!acl_check('inventory', 'im_uom_add') || !acl_check('inventory', 'im_uom_edit')) die(xlt('Not authorized'));
 

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
<?php html_header_show(); ?>
<title><?php echo $cat_id ? xlt("Edit") : xlt("Add New"); echo ' ' . xlt('UOM'); ?></title>
<link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>
<link rel='stylesheet' href="../themes/bootstrap.css" type="text/css">
<style>
td { font-size:10pt; }
</style>
<script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../../library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="../../library/topdialog.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/commonScript.js"></script>
<script type="text/javascript" src="../../library/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script language="JavaScript">

<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

// This is for callback by the find-code popup.
// Appends to or erases the current list of related codes.
function set_related(codetype, code, selector, codedesc) {
 var f = document.forms[0];
 var s = f.form_related_code.value;
 if (code) {
  if (s.length > 0) s += ';';
  s += codetype + ':' + code;
 } else {
  s = '';
 }
 f.form_related_code.value = s;
}
</script>
<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
  // return false;
	if (document.forms[0].form_name.value.length > 0) {
		return true;
	} else {
		/*if (document.forms[0].form_name.value.length <= 0)
		{*/

			document.forms[0].form_name.style.backgroundColor="red";
      //alertPopUp ('Alert', "<?php xl('Required field missing: Please enter the UOM Name.','e');?>");
      //$('#myAlert').modal();
      jAlert ('Required field missing: Please enter the UOM Name.', 'Alert');
			document.forms[0].form_name.focus();
			return false;
		/*}*/			
	}
}
</script>
</head>

<body class="body_top">
  <?php include_once("../../library/popUp.html"); ?>

<?php
// If we are saving, then save and close the window.
// First check for duplicates.
//
if ($_POST['form_save']) {
  $crow = sqlQuery("SELECT COUNT(invuom_id) AS count FROM inv_uom WHERE " .
    "invuom_name = '"  . escapedff('form_name')  . "' AND invuom_deleted = '0' AND " .    
    "invuom_id != ?", array($uom_id));
  if ($crow['count']) {
    $alertmsg = addslashes(xl('Cannot add this uom because it already exists!'));
  }
}

if (($_POST['form_save'] || $_POST['form_delete']) && !$alertmsg) {
  $new_drug = false;
  if ($uom_id) {
   if ($_POST['form_save']) { // updating an existing drug
    sqlStatement("UPDATE inv_uom SET " .
     "invuom_name = '"           . escapedff('form_name')          . "', " .
     "invuom_desc = '"     . escapedff('form_desc')    . "', " .
     "invuom_status = '"       . (empty($_POST['form_active']) ? 0 : 1) . "', " .     
     "invuom_createdby = "          . $_SESSION['authId'] . " " .
     "WHERE invuom_id = ?", array($uom_id));    
   }
   else { // deleting
    if (acl_check('admin', 'super')) {
		if(checkUomStatus($uom_id)){
     		sqlStatement("update inv_uom set invuom_deleted='1' WHERE invuom_id = ?", array($uom_id)); 
     	}    
    }
   }
  }
  else if ($_POST['form_save']) { // saving a new drug
   $new_drug = true;
   $uom_id = sqlInsert("INSERT INTO inv_uom ( " .
    "invuom_name, invuom_desc, invuom_status, invuom_createdby " .    
    ") VALUES ( " .
    "'" . escapedff('form_name')          . "', " .
    "'" . escapedff('form_desc')    . "', " .
    "'" . (empty($_POST['form_active']) ? 0 : 1) . "', " . $_SESSION['authId'] .
    ")");
  } 

  // Close this window and redisplay the updated list of drugs.
  //
  echo "<script language='JavaScript'>\n";
  if ($info_msg) echo " alert('$info_msg');\n";
  echo "parent.location.reload();\n";  
  /*echo " if (opener.refreshme) opener.refreshme();\n";  
  echo " window.close();\n";  */
  echo "</script></body></html>\n";
  exit();
}

if ($uom_id) {
  $row = sqlQuery("SELECT invuom_id, invuom_name, invuom_desc, invuom_status, invuom_createdby FROM inv_uom WHERE invuom_id = ?", array($uom_id));
}else {
  $row = array(
    'invuom_name' => '',
    'invuom_desc' => '',
    'invuom_status' => '1'    
  );
}
?>

<form method='post' name='theform' onsubmit="return submitform();" action='add_edit_uom.php?uomid=<?php echo $uom_id; ?>'>

<table border='0' width='100%' class='emrtable'>

 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Name'); ?>:</b></td>
  <td>
   <input type='text' size='40' name='form_name' maxlength='80' value='<?php echo attr($row['invuom_name']) ?>' style='width:75%' />
  </td>
 </tr>
<tr>
  <td valign='top' nowrap><b><?php echo xlt('Description'); ?>:</b></td>
  <td>
   <textarea name='form_desc' rows="5" style='width:75%'><?php echo attr($row['invuom_desc']);?></textarea>
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Active'); ?>:</b></td>
  <td>
   <input type='checkbox' name='form_active' value='1'<?php if ($row['invuom_status']) echo ' checked'; ?> />
  </td>
 </tr>  
<tr>
<td colspan='2' align='center'>
<input type='submit' class="btn btn-warning btn-sm" name='form_save' onclick="" value='<?php echo xla('Save'); ?>' />
&nbsp;
<input id="cancel" class="btn btn-warning btn-sm" type='button' value='<?php echo xla('Cancel'); ?>'  />
</td>
</tr>
</table>
</form>

<script language="JavaScript">
<?php
 if ($alertmsg) {
  // echo "alert('" . htmlentities($alertmsg) . "');\n";
  echo "alertPopUp('Alert', '" . htmlentities($alertmsg) . "'); $('#myAlert').modal();\n";
 }
?>
</script>

</body>
</html>
