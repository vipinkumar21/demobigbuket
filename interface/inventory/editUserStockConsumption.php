<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $transId = $_REQUEST['transId'];
 $info_msg = "";
 $tmpl_line_no = 0;
 
 if (!acl_check('inventory', 'invf_cons_edit')) die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
	<title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
	<script language="JavaScript">
		<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
		// This is for callback by the find-code popup.
		// Appends to or erases the current list of related codes.
		function set_related(codetype, code, selector, codedesc) {
		 var f = document.forms[0];
		 var s = f.form_related_code.value;
		 if (code) {
		  if (s.length > 0) s += ';';
		  s += codetype + ':' + code;
		 } else {
		  s = '';
		 }
		 f.form_related_code.value = s;
		}


		function trimAll(sString)
		{
			while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}
			while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		} 

		function isNumber(evt) {
		    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;
		}

		function submitform() {
			if (document.forms[0].facility.value>0 && document.forms[0].invistr_id.value>0 && parseInt(document.forms[0].form_old_qty.value)>0 && document.forms[0].form_qty.value.length>0) {
				var itemStockValue = document.forms[0].itemid.value;
				var itemStockArr = itemStockValue.split('#');
				if(parseInt(document.forms[0].form_old_qty.value) > parseInt(document.forms[0].form_qty.value)){
					return true;
				}else{
					if((parseInt(document.forms[0].form_total_qty.value)+parseInt(document.forms[0].form_old_qty.value)) >= parseInt(document.forms[0].form_qty.value)) {
						return true;
					}else {
						document.forms[0].form_qty.style.backgroundColor="red";
						alert("<?php xl('Required field missing: Entered item quantity not avialable in stock.','e');?>");
						document.forms[0].form_qty.focus();
						return false;
					}
				}
			} else {
				if (document.forms[0].facility.value<=0)
				{
					document.forms[0].facility.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Facility is should not be blank.','e');?>");
					document.forms[0].facility.focus();
					return false;
				}
				if (document.forms[0].invistr_id.value<=0)
				{
					document.forms[0].invistr_id.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Consumption id should not be blank.','e');?>");
					document.forms[0].invistr_id.focus();
					return false;
				}			
				if (parseInt(document.forms[0].form_old_qty.value)<=0)
				{
					document.forms[0].form_old_qty.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Consumption added quantity should not be blank.','e');?>");
					document.forms[0].form_old_qty.focus();
					return false;
				}
				if (document.forms[0].form_qty.value.length<=0)
				{
					document.forms[0].form_qty.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Please enter the Item quantity.','e');?>");
					document.forms[0].form_qty.focus();
					return false;
				}				
			}
		}
	</script>
</head>
<body>
<?php
	// If we are saving, then save and close the window.
	// First check for duplicates.
	//
	if ($_POST['form_save']) {	
	   //$new_drug = true;
	   //print_r($_POST);
	   //die();
		$itemStockValue = $_POST['itemid'];
		//$itemStockArr = explode('#', $itemStockValue);
		$itemStockDetails = getUserStockDetail($_POST['issue_id']);
		//print_r($itemStockDetails);
		if($itemStockDetails['invir_quantity']-$itemStockDetails['invir_return_qty'] >= $_POST['form_qty']){		
			//sqlStatement("update inv_issue_return set invir_cons_qty=".escapedff('form_qty')." where invir_id = '" . escapedff('issue_id') . "'");
			$issueData=array(
					'invir_id' => escapedff('issue_id'),				
					'invir_cons_qty' => $itemStockDetails['invir_cons_qty']+$_POST['form_cqty']
			);
			$issueId = insertUpdateIssueReturn($issueData);
			//sqlStatement("update inv_item_stock_transaction set invistr_quantity=".escapedff('form_qty').", invistr_comment='".escapedff('form_desc')."' where invistr_id = '" . escapedff('invistr_id') . "' AND invistr_clinic_id = ".escapedff('facility'));
			$tranData=array(
					'invistr_id' => escapedff('invistr_id'),
					'invistr_quantity' => escapedff('form_qty'),				
					'invistr_comment' => escapedff('form_desc'),
					'invistr_createdby' => $_SESSION['authId'],
					'invistr_created_date' => date("Y-m-d H:i:s")
			);
			$transId = updateStockTransaction($tranData);
			sqlStatement("update inv_user_issue_consumption set iuic_quantity=".escapedff('form_qty')." where iuic_tranid = '" . escapedff('invistr_id') . "' AND iuic_issueid = ".escapedff('issue_id'));
		  // Close this window and redisplay the updated list of drugs.
		  //  
		  echo "<script language='JavaScript'>\n";
		  if ($info_msg) echo " alert('$info_msg');\n";
		  echo " if (opener.refreshme) opener.refreshme();\n";  
		  echo " window.close();\n";  
		  echo "</script></body></html>\n";
		  exit();
		}else {
			$alertmsg='Entered quantity not available in selected item stock. Please try again.';
		}
	}
	if ($transId) {
		$row = sqlQuery("SELECT ist.invistr_id, ist.invistr_itemid, ist.invistr_batch, ist.invistr_expiry, ist.invistr_price, ist.invistr_quantity, ist.invistr_clinic_id, ist.invistr_isdeleted, ist.invistr_createdby, ist.invistr_comment, ist.invistr_created_date, im.inv_im_name " .
			"FROM inv_item_stock_transaction AS ist  
			INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invistr_itemid 
			WHERE ist.invistr_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invistr_tran_type = 8 AND ist.invistr_createdby = ? AND ist.invistr_id = ?", array($_SESSION['authId'], $transId));
		$stockDetails = getUserIssueStockDetail($transId);
	}
	if(empty($row)){
	?>
		<script language="JavaScript">
			alert('You should not able to access this page due to wrong consumption.');
			window.close();
		</script>
	<?php
	}
?>
<div class="infopop"><?php  xl('Edit Consumption','e'); ?></div>
<div id='popUpformWrap'>
	<form class='nomrgn' method='post' name='theform' action='editUserStockConsumption.php?transId=<?php echo $transId; ?>'>
		<input type='hidden' name='form_old_qty' value='<?php echo attr($row['invistr_quantity']) ?>' />
		<input type='hidden' name='facility' value='<?php echo attr($row['invistr_clinic_id']) ?>' />
		<input type='hidden' name='invistr_id' value='<?php echo attr($row['invistr_id']) ?>' />
		<input type='hidden' name='itemid' value='<?php echo attr($stockDetails['invir_id']) ?>' />
		<input type='hidden' name='issue_id' value='<?php echo attr($stockDetails['invir_id']) ?>' />
		<input type='hidden' name='form_stock_id' value='<?php echo attr($stockDetails['invir_stock_id']) ?>' />
		<input type='hidden' name='form_total_qty' value='<?php echo attr($stockDetails['invir_quantity']-($stockDetails['invir_return_qty']+$stockDetails['invir_cons_qty'])) ?>' />
		<!-- Row 1 -->
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Item'); ?></label>
					<input type='text' value='<?php echo attr($row['inv_im_name']).'#'.attr($row['invistr_batch']).'#'.attr($row['invistr_expiry']); ?>' disabled />
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Quantity'); ?></label>
					<input type='text' size='10' name='form_qty' maxlength='7' onkeypress="return isNumber(event)" value='<?php echo attr($row['invistr_quantity']) ?>' />
				</div>
			</div>
		</div>
		<!-- Row 1 -->
		<!-- Row 3 -->
		<div class="row">
			<div class="col-sm-6 txtarea">
				<div class="form-group">
					<label><?php echo xlt('Notes'); ?></label>
					<textarea name='form_desc' rows="5" cols="34"><?php echo attr($row['invistr_comment'])?></textarea>
				</div>
			</div>
		</div>
		<!-- Row 3 -->
		<a id="subform" href="javascript:void(0)" class="save-btn"></a>
		<input type='submit' name='form_save' id='form_save' value='<?php echo xla('Save'); ?>' />
	</form>
</div>
<script language="JavaScript">
<?php if ($alertmsg) {
	echo "alert('" . htmlentities($alertmsg) . "');\n";
} ?>
</script>
</body>
</html>