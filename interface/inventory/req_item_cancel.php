<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//require_once("mag_update_status.php");
require_once("oneivoryintg.php");

$alertmsg = '';
$reqid = $_REQUEST['reqid'];
$facility = $_REQUEST['facility'];
if (isset($_REQUEST['magorderid']))
    $magorderid = $_REQUEST['magorderid'];
else {
    $row = "SELECT isr_magento_orderid as magorderid FROM inv_stock_requisition WHERE isr_id = $reqid";
    $mageId = $pdoobject->custom_query($row);
    $magorderid = $mageId[0]['magorderid'];
}
$info_msg = "";
$tmpl_line_no = 0;
$usergd = getUserGroup('value');
if (!$invgacl->acl_check('inventory', 'invf_reqout_del','users',$_SESSION['authUser']))
    die(xlt('Not authorized'));

if (empty($reqid)) {
    echo "<script language='JavaScript'>\n";
    echo " alert(You have not selected requisition.);\n";
    echo " if (opener.refreshme) opener.refreshme();\n";
    echo " window.close();\n";
    echo "</script></body></html>\n";
    exit();
}

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
    <?php html_header_show(); ?>
    <title><?php echo $reqid ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock Requisition'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <?php
// If we are saving, then save and close the window.
// First check for duplicates.
    if ($_POST['form_save'] == 'Remove') {
        
        $reqStatus = 3;
        if($_REQUEST['cancel_note']){
           $message=' By '.$_SESSION['Auth']['User']['nickname'].', '.$_REQUEST['cancel_note'] ; 
       
        $deleteSql = $pdoobject->update("inv_stock_requisition", array("isr_isapproved" => $reqStatus, "isr_status" => $reqStatus,"message"=>$message), array("isr_id" => $_POST["reqid"]));
        //mag_order_cancel($_POST[magorderid]);
        if($GLOBALS['order_cancel_api']){
        //if ($_REQUEST['facility'] == '35') {
                 $type=getFacilityType($_REQUEST['facility']);
            if($magorderid){
            oneivoryintg::save(array('module_name' => 'sales_order_cancel', 'endpoint' => 'sales_order.cancel', 'action_id' => $_POST['reqid'], 'request' => json_encode($magorderid)));
            }
        //}
        }

        if ($deleteSql) {
            $alertmsg = "<div class='alert alert-success alert-dismissable'>Requisition has been Cancelled!</div>";
            //echo " alert('Requisition has been successfully cancelled');\n";  
        } else {
            $alertmsg = "<div class='alert alert-danger alert-dismissable'>There is an error during cancel the requisition!</div>";
            //echo " alert('There is an error during cancel the requisition.');\n";  
        }
        }else{
          $alertmsg = "<div class='alert alert-danger alert-dismissable'>Notes is required field!</div>";  
        }
        // Close this window and redisplay the updated list of drugs.
        $_SESSION['INV_MESSAGE'] = $alertmsg;
        // if ($info_msg)
        // echo " alert('$info_msg');\n";
        
        echo "<script language='JavaScript'>\n";
        echo "parent.location.reload();\n";
        echo "</script></body></html>\n";
        exit();
    }
    ?>
    <body class="ui-bg-wht">
        <?php
        $sql = "SELECT istreq.isr_id, istreq.isr_number, istreq.isr_from_clinic, istreq.isr_to_clinic, istreq.isr_isapproved, istreq.isr_status, istreq.isr_isdeleted, isr_createdby, istreq.isr_created_date, frf.name AS fromFacility, tof.name AS toFacility " .
                "FROM inv_stock_requisition AS istreq 
	INNER JOIN facility AS frf ON frf.id = istreq.isr_from_clinic
	INNER JOIN facility AS tof ON tof.id = istreq.isr_to_clinic " .
                "WHERE istreq.isr_id = ?  ORDER BY istreq.isr_id DESC";
        $res = $pdoobject->custom_query($sql, array($reqid));
        //print_r($res);
        $row = $res[0];
        ?>
        <div class="infopop"><?php xl('Cancel Requisition', 'e'); ?></div>
        <div class="popupTableWrp mt-0">
            <!-- <form method='post' id='theform' name='theform' > -->
                <table class="popupTable ui-table" cellspacing='0'>
                    <thead>
                        <tr>
                            <th><?php echo xlt('From Facility'); ?></th>
                            <th><?php echo xlt('To Facility'); ?></th>
                            <th><?php echo xlt('Requisition#'); ?></th>
                            <!-- <th><?php //echo xlt('Appr. Status'); ?></th> -->
                            <th><?php echo xlt('Req. Status'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>	 
                                <span><?php echo $row['fromFacility']; ?></span>
                            </td>
                            <td>
                                <span><?php echo $row['toFacility']; ?></span>   	
                            </td>
                            <td>
                                <span><?php echo $row['isr_number']; ?></span>
                            </td>
                            <!-- <td>
                                <?php
                                if ($row['isr_isapproved'] == 2) {
                                    echo 'Rejected';
                                } else if ($row['isr_isapproved'] == 1) {
                                    echo 'Approved';
                                } else {
                                    echo 'Waiting';
                                }
                                ?>
                            </td> -->
                            <td>
                                <?php
                                if ($row['isr_status'] == 2) {
                                    echo 'Completed';
                                } else if ($row['isr_status'] == 1) {
                                    echo 'Partially Completed';
                                } else {
                                    echo 'In Progress';
                                }
                                ?>	
                            </td>
                        </tr>
                    </tbody> 
                </table>
            <!-- </form> -->
        </div>
        <br />
         <form method='post' id='reqCancel' name='requisitionListForm' action='req_item_cancel.php?reqid=<?php echo $reqid; ?>&facility=<?php echo $facility; ?>&magorderid=<?php echo $magorderid; ?>'>
            <div class="popupTableWrp mt-0">
                <input type="hidden" name="reqid" value="<?php echo $reqid; ?>">
                <input type="hidden" name="facility" value="<?php echo $facility; ?>">
                <input type="hidden" name="tofacility" value="<?php echo $row['isr_from_clinic']; ?>">
                <input type="hidden" name="magorderid" value="<?php echo $magorderid ?>">

                <div id="requisitionListContainer">
                    <table id='requisitionList' class="popupTable ui-table" cellspacing='0'>
                        <thead>
                            <tr>
                                <th width='30%'>Item</th>
                                <th>Item Code</th>
                                <th>Current Qty</th>
                                <th>Req. Qty</th>
                                <th>Issued Qty</th>		
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $reqSql = "SELECT irit.iri_itemid, irit.iri_reqid, irit.iri_quantity, irit.iri_isdeleted, im.inv_im_name, im.inv_im_code FROM inv_requisition_item AS irit 
                                    INNER JOIN inv_item_master AS im ON im.inv_im_id = irit.iri_itemid 
                                    WHERE irit.iri_isdeleted = '0' AND irit.iri_reqid = ?";
                            $reqResult = $pdoobject->custom_query($reqSql, array($reqid));
                            if ($pdoobject->custom_query($reqSql, array($reqid), 1)) {
                                foreach ($reqResult AS $itemrow) {
                                    ?>
                                <input type="hidden" name="reqItems[]" value="<?php echo $itemrow['iri_itemid']; ?>">
                                <tr>				
                                    <td>
                                        <?php echo $itemrow['inv_im_name']; ?>&nbsp;					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['inv_im_code']; ?>&nbsp;				
                                    </td>
                                    <td>
                                        &nbsp;			
                                    </td>
                                    <td>
                                        &nbsp;<?php echo $itemrow['iri_quantity']; ?>			
                                    </td>
                                    <td>
                                        &nbsp;
                                        <?php
                                        $issuedQtySql = "SELECT SUM(iist.iii_quantity) AS issuedqty FROM inv_issue_notes AS iisn INNER JOIN inv_issue_item AS iist ON iist.iii_issueid = iisn.iin_id
            		                      WHERE iisn.iin_reqid = ? AND iist.iii_itemid=? ";
                                        $issuedQtyResult = $pdoobject->custom_query($issuedQtySql, array($reqid, $itemrow['iri_itemid']));
                                        $issuedQty = (!empty($issuedQtySql[0]['issuedqty'])) ? $issuedQtySql[0]['issuedqty'] : 0;
                                        echo $issuedQty;
                                        ?>
                                        <input type="hidden" id="requiredQty_<?php echo $itemrow['iri_itemid']; ?>" name="requiredQty_<?php echo $itemrow['iri_itemid']; ?>" value="<?php echo $itemrow['iri_quantity'] - $issuedQty; ?>">
                                    </td>
                                </tr>
                                <?php
                                $getClinicStockSql = "SELECT ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry,"
                                        . "ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted,"
                                        . "ist.invist_createdby, im.inv_im_name "
                                        . "FROM inv_item_stock AS ist "
                                        . "INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid "
                                        . "WHERE ist.invist_isdeleted = '0' "
                                        . "AND im.inv_im_deleted = '0' "
                                        . "AND ist.invist_quantity > 0 "
                                        . "AND ist.invist_itemid= ? "
                                        . "AND ist.invist_clinic_id=? "
                                        . "ORDER BY ist.invist_expiry ASC";
                                $getClinicStockResult = $pdoobject->custom_query($getClinicStockSql, array($itemrow['iri_itemid'], $facility));
                                if ($pdoobject->custom_query($getClinicStockSql, array($itemrow['iri_itemid'], $facility), 1)) {
                                    foreach ($getClinicStockResult as $reqItemStock) {
                                        ?>
                                        <tr>				
                                            <td>
                                                &nbsp;												
                                            </td>
                                            <td>
                                                <?php echo $reqItemStock['invist_batch'] . ' ' . ($reqItemStock['invist_expiry']) == '0000-00-00' ? '' : $reqItemStock['invist_expiry'] . ' ' . ($reqItemStock['invist_price']) == 0.00 ? '' : $reqItemStock['invist_price']; ?>&nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                                <?php echo $reqItemStock['invist_quantity']; ?>
                                                <input type="hidden" id="stockQtyAv_<?php echo $reqItemStock['invist_id']; ?>" name="stockQtyAv_<?php echo $reqItemStock['invist_id']; ?>" value="<?php echo $reqItemStock['invist_quantity']; ?>">
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                                <?php
                                                $stockQtySql = "SELECT SUM(iist.iii_quantity) AS issuedqty FROM inv_issue_notes AS iisn INNER JOIN inv_issue_item AS iist ON iist.iii_issueid = iisn.iin_id 
                                                                WHERE iisn.iin_reqid = ? AND iist.iii_itemid=? AND iist.iii_stock_id=?";
                                                $stockQtyResult = $pdoobject->custom_query($stockQtySql, array($reqid, $reqItemStock['invist_itemid'], $reqItemStock['invist_id']));
                                                $stockQty = (!empty($issuedQtySql[0]['issuedqty'])) ? $issuedQtySql[0]['issuedqty'] : 0;
                                                echo $stockQty;
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } //else {
                                    ?>
                                    <!--<tr>				
                                        <td colspan="6" align="center" style="text-align:center;">
                                            No Stock Available													
                                        </td>

                                    </tr>-->
                                    <?php
                                //}
                            }
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="cancelnotes">
                <div class="form-group">
                    <label>Notes:<sup class='required'>*</sup></label>
                    <textarea name='cancel_note' rows="5" cols="34" maxlenght="768"></textarea><span class="pull-right counter-text">Max. 768 character(s)</span>
                </div>
            </div>
            <a id="subform" href="javascript:void(0)" class="save-btn"></a>
            <input type='submit' name='form_save' id='form_save' class="btn btn-warning btn-sm dnone" value='<?php echo xla('Remove'); ?>' />
        </form>
        <script language="JavaScript">
<?php
if ($alertmsg) {
    echo "alert('" . htmlentities($alertmsg) . "');\n";
}
?>
        </script>

    </body>
</html>