<?php
// Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("oneivoryintg.php");

$alertmsg = '';
$adjid = $_REQUEST['adjid'];
$facility = $_REQUEST['facility'];
$info_msg = "";
$tmpl_line_no = 0;

if (!acl_check('inventory', 'invf_adj_add'))
    die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php
            echo $item_id ? xlt("Edit") : xlt("Add");
            echo ' ' . xlt('Stock');
            ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body class="body_top">
        <?php
        // If we are saving, then save and close the window.
        // First check for duplicates.
        //
            if ($_POST['form_save']) {

            $itemStockValue = $_POST['itemid'];
            $itemStockArr = explode('#', $itemStockValue);
            $stockId = $itemStockArr[0];
            $itemStockSql = "SELECT im.oneivory_id, ist.invist_id, ist.invist_itemid, ist.invist_batch, 
                            ist.invist_expiry, ist.invist_price, ist.invist_quantity, 
                            ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name 
                            FROM inv_item_stock AS ist 
                            INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid 
                            WHERE ist.invist_isdeleted = '0' 
                            AND im.inv_im_deleted = '0'
                            AND ist.invist_id =?";
            $itemStockRes = $pdoobject->custom_query($itemStockSql, array($stockId));
            $itemStockDetails = $itemStockRes[0];
            if ($_POST['adjType'] == 'Out') {
                if ($itemStockDetails['invist_quantity'] >= $_POST['form_qty']) {
                    try {
                        $pdoobject->begin_transaction();
                        $removeStockQuantity = "update inv_item_stock set invist_quantity=invist_quantity-" . $_POST['form_qty'] . " where invist_id = ?";
                        $removeStockQuantityRes = $pdoobject->custom_query($itemStockSql, array($stockId));
                        $adjData = array(
                            'ina_id' => '',
                            'ina_item_id' => $itemStockArr[1],
                            'ina_stock_id' => $itemStockArr[0],
                            'ina_quantity' => escapedff('form_qty'),
                            'ina_type' => escapedff('adjType'),
                            'ina_facility_id' => escapedff('facility'),
                            'ina_addedby' => $_SESSION['authId'],
                            'ina_added_date' => date("Y-m-d H:i:s"),
                            'notes' => escapedff('form_desc')
                        );
                        $adjId = $pdoobject->insert("inv_adjustments", $adjData);
                        $tranData = array(
                            'invistr_itemid' => $itemStockDetails['invist_itemid'],
                            'invistr_batch' => $itemStockDetails['invist_batch'],
                            'invistr_expiry' => $itemStockDetails['invist_expiry'],
                            'invistr_price' => $itemStockDetails['invist_price'],
                            'invistr_quantity' => escapedff('form_qty'),
                            'invistr_before_qty' => $itemStockDetails['invist_quantity'],
                            'invistr_after_qty' => $itemStockDetails['invist_quantity'] - escapedff('form_qty'),
                            'invistr_clinic_id' => escapedff('facility'),
                            'invistr_tran_type' => 5,
                            'invistr_comment' => escapedff('form_desc'),
                            'invistr_createdby' => $_SESSION['authId'],
                            'invistr_created_date' => date("Y-m-d H:i:s"),
                            'invistr_action' => 'inventory/add_edit_adjustment',
                            'invistr_action_id' => $adjId
                        );

                        $transId = $pdoobject->insert("inv_item_stock_transaction", $tranData);
                        $after_qty = $pdoobject->fetch_multi_row("inv_item_stock_transaction", array("invistr_after_qty"), array("invistr_id" => $transId));
                        $stockData = array(
                            'invist_quantity' => $after_qty[0]['invistr_after_qty']
                        );
                        $where = array('invist_id' => $stockId);
                        $update_stock = $pdoobject->update("inv_item_stock", $stockData, $where);
                        $adjRelData = array(
                            'iatr_adj_id' => $adjId,
                            'iatr_trans_id' => $transId
                        );
                        $adjRelId = $pdoobject->insert('inv_adj_trans_rel', $adjRelData);
                        $pdoobject->commit();
                        $message = "<div class='alert alert-success alert-dismissable'>Adjustment quantity removed successfully!</div>";
                        if ($GLOBALS['adjustment_api']) {

                            //if ($_REQUEST['facility'] == '35') {
                                 $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                                oneivoryintg::save(array('module_name' => 'catalog/adjustment', 'endpoint' => 'apilogin_apiupdatestock.update', 'action_id' => $adjRelId, 'request' => json_encode(array($data = array('product_id' => $itemStockDetails['oneivory_id'], 'qty' => $_REQUEST['form_qty'], 'is_in_stock' => '1', 'adjustment' => $_POST['adjType'])))));
            }
                            //}
                        }else{
                           if(getItemDetail($_REQUEST['itemid']) && $GLOBALS['stock_api_exp']){
                                    $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                                oneivoryintg::save(array('module_name' => 'catalog/adjustment', 'endpoint' => 'apilogin_apiupdatestock.update', 'action_id' => $adjRelId, 'request' => json_encode(array($data = array('product_id' => $itemStockDetails['oneivory_id'], 'qty' => $_REQUEST['form_qty'], 'is_in_stock' => '1', 'adjustment' => $_POST['adjType'])))));
            }
                           }  
                        }
                    } catch (PDOException $e) {
                        $pdoobject->rollback();
                        $message = "<div class='alert alert-danger alert-dismissable'>Error in saving data!</div>";
                    }
                    $_SESSION['INV_MESSAGE'] = $message;

                    // if ($message)
                    //     echo " alert('$message');\n";
                    echo "<script language='JavaScript'>\n";
                    echo "parent.location.reload();\n";
                    echo "</script></body></html>\n";
                    exit();
                } else {
                    //echo "<div class='alertMsg'>Entered quantity not available in selected item stock. Please try again.</div>";
                    $alertmsg = '<div class="alert alert-warning alert-dismissable">Entered quantity not available in selected item stock. Please try again!</div>';
                    $_SESSION['INV_MESSAGE'] = $alertmsg;
                }
            } else {
                try {
                    $pdoobject->begin_transaction();
                    $removeStockQuantity = "update inv_item_stock set invist_quantity=invist_quantity+" . $_POST['form_qty'] . " where invist_id = ?";
                    //echo $removeStockQuantity;exit;
                    $removeStockQuantityRes = $pdoobject->custom_query($itemStockSql, array($stockId));
                    $adjData = array(
                        'ina_id' => '',
                        'ina_item_id' => $itemStockArr[1],
                        'ina_stock_id' => $itemStockArr[0],
                        'ina_quantity' => escapedff('form_qty'),
                        'ina_type' => escapedff('adjType'),
                        'ina_facility_id' => escapedff('facility'),
                        'ina_addedby' => $_SESSION['authId'],
                        'ina_added_date' => date("Y-m-d H:i:s"),
                        'notes' => escapedff('form_desc')
                    );
                    $adjId = $pdoobject->insert("inv_adjustments", $adjData);
                    $tranData = array(
                        'invistr_itemid' => $itemStockDetails['invist_itemid'],
                        'invistr_batch' => $itemStockDetails['invist_batch'],
                        'invistr_expiry' => $itemStockDetails['invist_expiry'],
                        'invistr_price' => $itemStockDetails['invist_price'],
                        'invistr_quantity' => escapedff('form_qty'),
                        'invistr_before_qty' => $itemStockDetails['invist_quantity'],
                        'invistr_after_qty' => $itemStockDetails['invist_quantity'] + escapedff('form_qty'),
                        'invistr_clinic_id' => escapedff('facility'),
                        'invistr_tran_type' => 5,
                        'invistr_comment' => escapedff('form_desc'),
                        'invistr_createdby' => $_SESSION['authId'],
                        'invistr_created_date' => date("Y-m-d H:i:s"),
                        'invistr_action' => 'inventory/add_edit_adjustment',
                        'invistr_action_id' => $adjId
                    );
                    $transId = $pdoobject->insert("inv_item_stock_transaction", $tranData);
                    $after_qty = $pdoobject->fetch_multi_row("inv_item_stock_transaction", array("invistr_after_qty"), array("invistr_id" => $transId));
                    $stockData = array(
                        'invist_quantity' => $after_qty[0]['invistr_after_qty']
                    );
                    $where = array('invist_id' => $stockId);
                    $update_stock = $pdoobject->update("inv_item_stock", $stockData, $where);
                    $adjRelData = array(
                        'iatr_adj_id' => $adjId,
                        'iatr_trans_id' => $transId
                    );
                    $adjRelId = $pdoobject->insert('inv_adj_trans_rel', $adjRelData);
                    $pdoobject->commit();
                    $message = "<div class='alert alert-success alert-dismissable'>Adjustment quantity added successfully!</div>";
                    if ($GLOBALS['adjustment_api']) {

                        //if ($_REQUEST['facility'] == '35') {
     $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                            oneivoryintg::save(array('module_name' =>
                                'catalog/adjustment', 'endpoint' => 'apilogin_apiupdatestock.update', 'action_id' => $adjRelId, 'request' => json_encode(array($data = array('product_id' => $itemStockDetails['oneivory_id'], 'qty' => $_REQUEST['form_qty'], 'is_in_stock' => '1', 'adjustment' => $_POST['adjType'])))));
            }
                        //}
                    }else{
                       if(getItemDetail($_REQUEST['itemid']) && $GLOBALS['stock_api_exp']){
                                $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                           oneivoryintg::save(array('module_name' =>
                                'catalog/adjustment', 'endpoint' => 'apilogin_apiupdatestock.update', 'action_id' => $adjRelId, 'request' => json_encode(array($data = array('product_id' => $itemStockDetails['oneivory_id'], 'qty' => $_REQUEST['form_qty'], 'is_in_stock' => '1', 'adjustment' => $_POST['adjType'])))));
            }
                       } 
                    }
                } catch (PDOException $e) {
                    $pdoobject->rollback();
                    $message = "<div class='alert alert-danger alert-dismissable'>Error in saving data!</div>";
                }

                $_SESSION['INV_MESSAGE'] = $message;
                // if ($message)
                //     echo " alert('$message');\n";
                echo "<script language='JavaScript'>\n";
                echo " window.close();\n";
                echo "parent.location.reload()\n";
                echo " if (opener.refreshme) opener.refreshme();\n";
                echo " window.close();\n";
                echo "</script></body></html>\n";
                exit();
            }
        }
        ?>
        <div class="infopop"><?php xl('Add Adjustment', 'e'); ?></div>
        <!-- tableData -->
        <div id='popUpformWrap'>
            <form method='post' name='theform' id='addAdjustmentForm' action='add_edit_adj.php?adjid=<?php echo $adjid; ?>&facility=<?php echo $facility; ?>' >
                <!-- Row 1 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>      <?php echo xlt('Facility'); ?>:<sup class="required">*</sup></label>
                            <?php
                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                            $userId = $_SESSION['authId'];
                            InvUsersFacilityDropdown('facility', '', 'facility', $facility, $userId, $userFacilityRestrict, $pdoobject,0);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group posrel">
                            <label><?php echo xlt('Item'); ?>:<sup class="required">*</sup></label>
                            <input type="text" class="formEle" id="itemField" name="itemField" value='' />
                            <img id="loading" style="display:none" src="../../images/loader.gif" />
                            <input type="hidden" class="formEle" id="itemId" name="itemid">
                            <input type="hidden" value="0" id="form_sale" name="form_sale">
                            <input type="hidden" value="0" id="form_expiry" name="form_expiry">
                        </div>
                    </div>
                </div>
                <!-- Row 1 -->

                <!-- Row 2 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Adjustment Type:<sup class="required">*</sup></label>
                            <input type="hidden" value="adj" id="form_page" name="form_page">
                            <select name='adjType' id="adjType" class='formEle'>  		
                                <option value='0' selected="selected">Select Type</option>
                                <option value='In'>Add</option>
                                <option value='Out'>Remove</option>
                            </select>  
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Quantity:<sup class="required">*</sup></label>
                            <input type='number' size='10' name='form_qty' id='form_qty' maxlength='7' onkeypress="return isNumber(event);" value='<?php echo attr($row['invist_quantity']) ?>' class='formEle' />
                            <input type='hidden' id='itemQty' name='itemQty' />
                        </div>
                    </div>
                </div>
                <!-- Row 2 -->

                <!-- Row 3 -->
                <div class="row">
                    <div class="col-sm-6 txtarea">
                        <div class="form-group">
                            <label>Notes:<sup class="required">*</sup></label>
                            <textarea name='form_desc' rows="5" cols="34" maxlenght="768"></textarea><span class="pull-right counter-text">Max. 768 character(s)</span>
                        </div>
                    </div>
                </div>
                <!-- Row 3 -->
                <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                <input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' value='<?php echo xla('Save'); ?>' />
            </form>
        </div>
        <!-- tableData -->
        <div id="placeholder">
            <img src='<?php echo $inventry_url; ?>emr/images/oi-wm-logo.jpg' class='autohgt' alt='' />
        </div>
    </body>
</html>