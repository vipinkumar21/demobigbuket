<?php
// Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");

$alertmsg = '';
$stockid = $_REQUEST['stockid'];
$info_msg = "";
$tmpl_line_no = 0;
if (!$invgacl->acl_check('admin','drugs','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body class="ui-bg-wht">
        <?php
        // If we are saving, then save and close the window.
        // First check for duplicates.
        //
	if ($_POST['form_save']) {
            $itemStockValue = $_POST['itemid'];
            $itemStockArr = explode('#', $itemStockValue);
            $stockId =$itemStockArr[0];
            
            try {
                $pdoobject->begin_transaction();
                $itemStockSql ="SELECT im.oneivory_id, ist.invist_id, ist.invist_itemid, ist.invist_batch, 
                                ist.invist_expiry, ist.invist_price, ist.invist_quantity, 
                                ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name 
                                FROM inv_item_stock AS ist 
                                INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid 
                                WHERE ist.invist_isdeleted = '0' 
                                AND im.inv_im_deleted = '0'
                                AND ist.invist_quantity > 0
                                AND ist.invist_id =?";
            
                $itemStockRes =$pdoobject-> custom_query($itemStockSql,array($stockId));
                $itemStockDetails = $itemStockRes[0];
                if ($itemStockDetails['invist_quantity'] >= $_POST['form_qty']) {
                    $beforeQty = $itemStockDetails['invist_quantity'];	   
                    $issueData = array(
                        'invir_id' => '',
                        'invir_item_id' => $itemStockArr[1],
                        'invir_stock_id' => $itemStockArr[0],
                        'invir_quantity' => escapedff('form_qty'),
                        'invir_issuee_id' => escapedff('issueeid'),
                        'invir_issuer_id' => $_SESSION['authId'],
                        'invir_facility_id' => escapedff('facility'),
                        'invir_issue_date' => date("Y-m-d H:i:s")
                    );
                    $issueId = $pdoobject->insert("inv_issue_return", $issueData);
                    $tranData = array(
                        'invistr_itemid' => $itemStockDetails['invist_itemid'],
                        'invistr_batch' => $itemStockDetails['invist_batch'],
                        'invistr_expiry' => $itemStockDetails['invist_expiry'],
                        'invistr_price' => $itemStockDetails['invist_price'],
                        'invistr_quantity' => escapedff('form_qty'),
                        'invistr_before_qty' => $beforeQty,
                        'invistr_after_qty' => $beforeQty - escapedff('form_qty'),
                        'invistr_clinic_id' => escapedff('facility'),
                        'invistr_tran_type' => 6,
                        'invistr_comment' => '',
                        'invistr_createdby' => $_SESSION['authId'],
                        'invistr_created_date' => date("Y-m-d H:i:s"),
                        'invistr_action' => 'inventory/add_edit_issue',
                        'invistr_action_id' => $issueId
                    );
                    $tresult = $pdoobject->insert("inv_item_stock_transaction", $tranData);
                    $after_qty = $pdoobject->fetch_multi_row("inv_item_stock_transaction", array("invistr_after_qty"), array("invistr_id" => $tresult));
                    $stockData = array(
                        'invist_quantity' => $after_qty[0]['invistr_after_qty']
                    );
                    $where = array('invist_id' => $stockId);
                    $update_stock = $pdoobject->update("inv_item_stock", $stockData, $where);
                    $providerSql = "SELECT CONCAT_WS(' ',fname,lname) AS name FROM users WHERE id ='".escapedff('issueeid')."'";
                    $name =$pdoobject->custom_query($providerSql,null,'','fetch');
                    $pdoobject->commit();
                    $message = "<div class='alert alert-success alert-dismissable'>Item issued to Dr. ".  ucwords($name['name']) ."!</div>";
                } else {
                    $message = '<div class="alert alert-warning alert-dismissable">Entered quantity not available in selected item stock. Please try again!</div>';
                    $_SESSION['INV_MESSAGE'] = $message;
                }
            } catch (PDOException $e) {
                $pdoobject->rollback();
                $message = "<div class='alert alert-danger alert-dismissable'>Error in saving data!</div>";
            }

            $_SESSION['INV_MESSAGE'] = $message;
            // if ($message)
            //     echo " alert('$message');\n";
            echo "<script language='JavaScript'>\n";
            echo "parent.location.reload();\n";
            echo "</script></body></html>\n";
            exit();
        }
        ?>
        <div class="infopop"><?php xl('Add Issue Item', 'e'); ?></div>
        <!-- tableData -->
        <div id='popUpformWrap'>
            <form method='post' id='addEditIssueForm' name='theform' action='add_edit_issue.php?stockid=<?php echo $stockid; ?>'>
                <!-- Row 1 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><?php echo xlt('Facility'); ?>:<sup class="required">*</sup>
                            </label>
                            <?php
                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                            $userId = $_SESSION['authId'];
                            InvUsersFacilityDropdown('facility', '', 'facility', $facility, $userId, $userFacilityRestrict, $pdoobject,0);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group posrel">
                            <label>Item:<sup class="required">*</sup></label>
                            <input type="text" class="formEle" id="itemField" name="itemField" readonly>
                            <img id="loading" style="display:none" src="../../images/loader.gif" />
                            <input type="hidden" class="formEle" id="itemId" name="itemid">
                            <input type="hidden" value="0" id="form_sale" name="form_sale">
                            <input type="hidden" value="0" id="form_expiry" name="form_expiry">
                        </div>
                    </div>
                </div>
                <!-- Row 1 -->

                <!-- Row 2 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><?php echo xlt('Doctor'); ?>:<sup class="required">*</sup></label>
                            <select name='issueeid' id="issueeidField" class='formEle'>
                                <option value='0' selected="selected">Select Doctor</option>
                                <?php
                                $sql = "Select id FROM users WHERE id=$userId AND active='1' ";
                                $defaultFac = $pdoobject->custom_query($sql,null,'','fetch');
                                getFacilityProvider($userId, $defaultFac['id'], $userFacilityRestrict)
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Quantity:<sup class="required">*</sup></label>
                            <input type='number' size='10' name='form_qty' maxlength='7' onkeypress="return isNumber(event);" value='<?php echo attr($row['invist_quantity']) ?>'  class='formEle'/>
                            <input type='hidden' id='itemQty' name='itemQty' value='' />
                        </div>
                    </div>
                </div>
                <!-- Row 2 -->
                <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                <input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' value='<?php echo xla('Save'); ?>' />
            </form>
        </div>
        <!-- tableData -->
        <div id="placeholder">
            <img src='<?php echo $inventry_url; ?>emr/images/oi-wm-logo.jpg' class='autohgt' alt='' />
        </div>
    </body>
</html>