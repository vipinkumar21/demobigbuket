<?php
 // Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 $sanitize_all_escapes  = true;
 $fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
?>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
  <link rel='shortcut icon' href='<?php echo $inventry_url; ?>emr/interface/img/favicon-oneivory.png' type='image/x-icon'/ >
  <title><?php echo xlt('Marketplace - Oneivory'); ?></title>
  <link rel="stylesheet" href="<?php echo $inventry_url; ?>emr/interface/themes/marketplace.css" type="text/css" />
</head>
<body>
  <!-- START: loader -->
  <div id="customLoading"><img src="<?php echo $inventry_url; ?>emr/interface/img/oi_loader.gif" /></div>
  <div class="ui-widget-overlay"></div>
  <!-- END: loader -->

  <!-- header -->
  <div class='floatingheader transition transition-hidden'>
    <?php include_once("im_header.html"); ?>
    <div id='collapse-bar'></div>
  </div>
  <!-- header -->

  <!-- contentArea -->
  <div id="page">
    
    <div class='dnone'>
      <a href="javascript:void(0)" class='dnone' id="getSession">Marketplace</a>      
      <?php $magUrl = $GLOBALS['apiurl']; ?>
      
      <form action="<?php echo $magUrl;?>apilogin" method="POST" target="marketplace" name="marketplace" id="marketplace">
        <input type="hidden" name="SID" id="SID" value="<?php echo $magtoken;?>" />
      </form>
    </div>
    
    <div class="mplc-load dnone">Something went wrong!! Please try again.</div>

    <iframe name='marketplace' id='marketplace_frame' class='dnone' frameborder='0' width='100%' height='100%'></iframe>
  </div>
  <!-- contentArea -->

  <!-- script -->
  <script type="text/javascript" src="<?php echo $inventry_url; ?>webapp/js/jquery/jquery-1.11.1.min.js"></script>
  <!-- global vairables declaration -->
  <script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/contains.js"></script>
  <!-- page script -->
  <script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/marketplace.js"></script>
  <!-- floating header -->
  <script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/transition-plug.js"></script>
</body>
</html>