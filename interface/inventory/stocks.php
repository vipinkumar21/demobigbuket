<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
 // This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("./lib/inv.users.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
//echo "<pre>";
//print_r($_SESSION);
if (!$invgacl->acl_check('inventory','invf_stock_list','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));    
if (isset($_GET["mode"])) {
    if ($_GET["mode"] == "delete") {
        if (checkUomStatus($_GET["itemid"])) {
            sqlStatement("update inv_item_master set inv_im_deleted='1' where inv_im_id = '" . $_GET["itemid"] . "'");
            $_SESSION['alertmsg'] = '<div class="alert alert-success alert-dismissable">Item deleted successfully!</div>';
            header('Location: items.php');
            exit;
        } else {
            $_SESSION['alertmsg'] = '<div class="alert alert-danger alert-dismissable">You should not able delete selected item!</div>';
            header('Location: items.php');
            exit;
        }
    }
    if ($_GET["mode"] == "activate") {
        sqlStatement("update inv_item_master set inv_im_status='1' where inv_im_id = '" . $_GET["itemid"] . "'");
        $_SESSION['alertmsg'] = '<div class="alert alert-success alert-dismissable">Item activated successfully!</div>';
        header('Location: items.php');
        exit;
    }
    if ($_GET["mode"] == "deactivate") {
        sqlStatement("update inv_item_master set inv_im_status='0' where inv_im_id = '" . $_GET["itemid"] . "'");
        $_SESSION['alertmsg'] = '<div class="alert alert-success alert-dismissable">Item deactivated successfully!</div>';
        header('Location: items.php');
        exit;
    }
}
// For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
    'inv_im_id' => 'inv_im_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? urldecode($_REQUEST['form_orderby']) : 'inv_im_id';
$orderby = $ORDERHASH[$form_orderby];

if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
     
}else{
   $_REQUEST['facility']= $_SESSION['cid']; 
}
//echo ".............".$_SESSION['reset_cid'];
$facility = isset($_REQUEST['facility'])? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$form_item = urldecode($_REQUEST['form_item']);
//$to_date = urldecode($_REQUEST['form_to_date']);

$userFacilityRestrict =$GLOBALS['restrict_user_facility'];
//echo $facility;
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Item Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body class="stocksPage">
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->

        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <h1><?php xl('Item Stock', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='stocks.php' name='theform' id='theform' class="botnomrg">
                            <!-- leftPart -->
                            <div class="filterWrapper">
                                <!-- first column starts --> 
                                <div class="ui-block">
                                    <?php
                                    usersFacilityDropdown('facility','' ,'facility',$facility,$_SESSION['authId'], $userFacilityRestrict,$pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends --> 
                                <!-- second column starts -->
                                <div class="ui-block">
                                    <input type='text' placeholder='item' name='form_item' id="form_item" size='10' value='<?php echo urldecode($_REQUEST['form_item']) ?>' title='' />
                                </div>
                                <!-- second column ends --> 
                                <!-- third column starts --> 
<!--                                <div class='ui-block form_to_date_bx'>
                                    <input type='text' placeholder='Till Date' name='form_to_date_stock' id="form_to_date_stock" size='10' readonly='readonly' />
                                    <input type='hidden' name='form_to_date' id="dateFormat" size='10' value='<?php //echo $to_date ?>' />
                                </div>-->
                                <!-- third column ends --> 
                                <!-- fourth column starts -->
                                <div class="ui-block ui-chkbx">
                                    <label for="zero_quantity_items" class="interButt">
                                        <?php xl('Zero quantity items', 'e'); ?>
                                    </label>
                                    <input type='checkbox' class="custom" name='zero_quantity_items' id="zero_quantity_items" value='1' <?php echo (isset($_REQUEST['zero_quantity_items']) && urldecode($_REQUEST['zero_quantity_items']) == 1) ? 'checked' : ''; ?> />
                                </div> 
                                <!-- fourth column ends -->
                                <!-- fifth column starts -->
                                <div class='ui-block wdth20'>                                                                        
                                    <a class="pull-right btn_bx" id='reset_form1' href="stocks.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true");
                                            $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- fifth column ends -->
                            </div>
                            <!-- leftPart -->
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <?php if (!empty($_SESSION['alertmsg'])) { ?>
                                    <tr>
                                        <td colspan="2"><?php echo $_SESSION['alertmsg'];
                                $_SESSION['alertmsg'] = "dsfsdfsdgdfgdfgdfgdfg";
                                    ?></td>		
                                    </tr>
                            <?php } ?>
                            </table>
                            <?php
                            
                           if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_item'] || $_REQUEST[form_to_date] || empty($facility)) {
                               
                                 $sql = "SELECT invist.`invist_clinic_id`,f.`name`,invist.`invist_itemid`,im.`inv_im_name`,
                                        invist.`invist_batch`,invist.`invist_expiry`,invist.`invist_quantity`,invist.`invist_price` 
                                        FROM `inv_item_stock` AS invist 
                                        INNER JOIN `inv_item_master` AS im ON invist_itemid = im.inv_im_id
                                        INNER JOIN facility  AS f ON f.id = invist.`invist_clinic_id` WHERE invist.`invist_isdeleted`='0' ";
                                if (!empty($facility)) {
                                    $sql .= " AND invist.`invist_clinic_id` = '$facility' ";
                                }
                                else
                                {
                                    $sql .= " AND invist.`invist_clinic_id` IN(".getLoggedUserAssignedClinics().")";
                                }
//                                if (!empty($to_date)) { // If only 'To' date exists
//                                    $sql .= " AND invist.`invist_created_date` <= '$to_date 23:59:59' ";
//                                }
                                if (!empty($form_item)) {  // if cat/subcat/item name/item code exists
                                    $sql .= " AND (";
                                    $sql .= " im.inv_im_code LIKE '%" . $form_item . "%' OR im.inv_im_name LIKE '%" . $form_item . "%')";
                                }
                                if ($_REQUEST['zero_quantity_items'] == 1) {
                                    $sql .= " AND invist.`invist_quantity` = 0 ";
                            } else {
                                $sql .= " AND invist.`invist_quantity` > 0 ";
                            }
                                $sql .= " ORDER BY f.id , im.inv_im_name,invist_batch";
                                //echo $sql;
                                $num_rows = $pdoobject->custom_query($sql,NULL,1);
                                $per_page = $GLOBALS['encounter_page_size'];
                                $page = $_GET["page"];
                                $curr_URL = $_SERVER['SCRIPT_NAME'] . '?' . 'form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&facility=' . $facility . '&form_item=' . urldecode($_REQUEST['form_item']) . '&form_to_date=' . urldecode($_REQUEST['form_to_date']) . '&';
                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                ?>
                                <div id='' class='tableWrp pb-2'>
                                    <!-- pagination -->
                                        <?php
                                        if ($num_rows > 0) {
                                            echo $pa_data;
                                        }
                                        ?>
                                    <!-- pagination -->
                                    <div class="dataTables_wrapper no-footer">
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                            <?php if ($num_rows > 0) {?>
                                            <thead>
                                                <tr role='row'>
                                                    <?php if(empty($facility)) {?>
                                                    <th width='15%'>   
                                                    <?php echo xlt('Clinic Name'); ?>
                                                    </th><?php }?>
                                                    <th>   
                                                    <?php echo xlt('Item Name'); ?>
                                                    </th>
                                                    <th width='15%'>
                                                        <?php echo xlt('Batch#'); ?>
                                                    </th>
                                                    <th width='10%'>
                                                        <?php echo xlt('Expiry Date'); ?>
                                                    </th>
                                                    <th width='10%'>
                                                        <?php echo xlt('Sale Price'); ?>
                                                    </th>
                                                    <th width='10%'>
                                                         <?php echo xlt('Quantity'); ?>
                                                    </th>
                                                </tr>
                                            </thead>								
                                            <?php }
                                            $page = ($page == 0 ? 1 : $page);
                                            $page_start = ($page - 1) * $per_page;
                                            $sql .= " LIMIT $page_start , $per_page";
                                           
                                            $res = $pdoobject->custom_query($sql);
                                            if ($pdoobject->custom_query($sql,NULL,1)) {
                                                $prevClinic = '';
                                                $currClinic = '';
                                                $currItem = '';
                                                $prevItem = '';
                                                $currBatch = '';
                                                $prevBatch = '';
                                                $srCount=0;
                                                foreach ($res As $row) {
                                                    $srCount=1;
                                                    $currClinic = $row['invist_clinic_id'];
                                                    $currItem = $row['invist_itemid'];
                                                    $expiry = invDateFormat($row['invist_expiry']);
                                                    if (empty($facility)) {
                                                        if($currClinic!=$prevClinic){
                                                    ?>
                                            <tr>
                                            <td><?php echo $row['name']; ?> </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            </tr>
                                                        <?php }?>
                                            <tr>
                                            <td>&nbsp;</td>
                                           <?php 
                                                                    
                                                                if($currClinic!=$prevClinic){
                                                                    ?>
                                                                    <td><?php echo $row['inv_im_name']; ?></td>
                                                                     
                                                                <?php }else{
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }}?> 
                                            <td><?php echo $row['invist_batch']; ?></td>
                                            <td><?php echo $expiry; ?></td>
                                            <td><?php echo $row['invist_price'] == '0.00' ? '' : $row['invist_price']; ?></td>
                                            <td><?php echo $row['invist_quantity']; ?></td>
                                            </tr>
                                            <?php    }  else {?>
                                            <tr>
                                         
                                            <?php 
                                                                    
                                                               
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }//}?>
                                            <td><?php echo $row['invist_batch']; ?></td>
                                            <td><?php echo $expiry; ?></td>
                                            <td><?php echo $row['invist_price'] == '0.00' ? '' : $row['invist_price']; ?></td>
                                            <td><?php echo $row['invist_quantity']; ?></td>
                                            </tr>
        
                                          <?php  }
                                            $prevClinic = $row['invist_clinic_id'];
                                            $prevItem = $row['invist_itemid'];
                                            $srCount++;
                                                }
                                            }else {
                                            ?>
                                                <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                <?php
                                            } // end If
                                            ?>
                                        </table>
                                    </div>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) {
                                            echo $pa_data;
                                        } ?>
                                    <!-- pagination -->
                                </div>
                                <?php } else { ?>
                            
                                <div class='text leftmrg6 dnone'>
                                    
                                <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                                </div>
                                    <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            <!-- leftPart -->
                        </form>
                        <!-- mdleCont -->
                    </div>                    
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
        <!-- page -->
    </body>
</html>
