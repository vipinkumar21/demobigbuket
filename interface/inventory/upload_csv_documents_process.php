<?php

/**
  * @author Brajraj Agrawal <brajraj.agarwal@instantsys.com>
  * @Created : 31-May-2016
  * @Description : This file is used to process the stock insertion and updation through a CSV file based on a perticular facility.
  */
set_time_limit(0);

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require("$srcdir/spreadsheet_reader/php-excel-reader/excel_reader2.php");
require("$srcdir/spreadsheet_reader/SpreadsheetReader.php");

$updir = $_SERVER['DOCUMENT_ROOT'] . $web_root . "/sites/" . $_SESSION['site_id'] . "/inventory/"; // uploading directory
//die();
// For Uploading Stock CSV
if (isset($_POST['mode']) && $_POST['mode'] == 'import_stock') {
    $_SESSION['msg'] = '';
    $itemFileInfo = fileUpload($_FILES['stock_file'], $updir);
    $facilityId = $_POST['facility'];
    if (empty($facilityId)) {
        header("Location: upload_stocks.php?error=noFacility");
        exit();
    } else {
        if (!empty($itemFileInfo['url']) && $itemFileInfo['status'] == true && file_exists($itemFileInfo['url'])) {
            $Filepath = $itemFileInfo['url'];
            $fileDatas = readUploadedFileData($Filepath);
            if (is_array($fileDatas) && !empty($fileDatas)) { // Correct CSV file upload
                if (array_key_exists('column', $fileDatas)) {
                    if (count($fileDatas['column']) == 6) { // checking correct columns in CSV
                        $addErr = addStock($fileDatas, $facilityId);
                        if ($addErr) {
                            $str = "<table class='text' border='1'>
                                    <tr>
                                            <th>Item</th><th>Code</th><th>Batch</th><th>Expiry</th><th>Price</th><th>Quantity</th><th>Failure Reason</th>
                                    </tr>";
                            foreach ($addErr AS $v) {
                                $str .="<tr>";
                                foreach ($v AS $k1 => $v1) {

                                    $str .= "<td>" . $v1 . "</td>";
                                }
                                $str .="</tr>";
                            }
                            $_SESSION['msg'] = "Following Items have Unsucessful Transactions...<br/>";
                            $_SESSION['msg'] .= "<br/>" . $str;
                        } else {
                            $_SESSION['msg'] = "Stock Insertion Successful..";
                        }
                    } else {
                        $_SESSION['msg'] = 'Invalid CSV Format..';
                    }
                    unlink($itemFileInfo['url']); // delete file from folder after processing
                }
            }
        } else { // Wrong file upload
            $_SESSION['msg'] = 'Incorrect File Upload..';
        }
    }
    header('Location: upload_stocks.php');
}

// Read columns and data of uploaded CSV file
function readUploadedFileData($Filepath) {

    $returnFileData = array();
    $columnNames = array();
    $rowData = array();
    $Spreadsheet = new SpreadsheetReader($Filepath);
    $Sheets = $Spreadsheet->Sheets();
    foreach ($Sheets as $Index => $Name) {
        $Spreadsheet->ChangeSheet($Index);
        
        //print_r($Spreadsheet);
        //die();
        foreach ($Spreadsheet as $Key => $Row) {
             
            if ($Key == 0) {
                if ($Row) {
                    $columnNames = $Row;
                } else {
                    $columnNames = $Row;
                }
            } else {
                if ($Row) {
                    $rowData[] = $Row;
                } else {
                    $rowData[] = $Row;
                }
            }
        }
    }
    $returnFileData['column'] = $columnNames;
    $returnFileData['data'] = $rowData;
    return $returnFileData;
}

// Checking Upload file status
function fileUpload($fileData, $updir) {
    $doit = true;
    $returnData = array();
    $c_filename = '';
    $max_size = 2000;         // sets maximum file size allowed (in KB)
    // file types allowed
    $allowtype = array('csv');
    if (isset($fileData)) {
        // check for errors
        if ($fileData['error'] > 0) {
            $doit = false;
            $returnData['status'] = $doit;
            $returnData['url'] = '';
        } else {
            // get the name, size (in kb) and type (the extension) of the file
            $fname = $fileData['name'];
            $fsize = $fileData['size'] / 1024;
            $ftype = end(explode('.', strtolower($fname)));
            $newFileName = time() . '_' . $fname;
            // checks if the file already exists
            if (file_exists($updir . $newFileName)) {
                $doit = false;
                $returnData['status'] = $doit;
                $returnData['url'] = '';
            } else {
                // if the file not exists, check its type (by extension) and size
                if (in_array($ftype, $allowtype)) {
                    // check the size
                    if ($fsize <= $max_size) {
                        // uses  function to copy the file from temporary folder to $updir
                        if (!move_uploaded_file($fileData['tmp_name'], $updir . $newFileName)) {
                            $doit = false;
                            $returnData['status'] = $doit;
                            $returnData['url'] = '';
                        } else {
                            $c_filename = $updir . $newFileName;
                            $returnData['status'] = $doit;
                            $returnData['url'] = $c_filename;
                        }
                    } else {
                        $doit = false;
                        $returnData['status'] = $doit;
                        $returnData['url'] = '';
                    }
                } else {
                    $doit = false;
                    $returnData['status'] = $doit;
                    $returnData['url'] = '';
                }
            }
        }
    } else {
        $doit = false;
        $returnData['status'] = $doit;
        $returnData['url'] = '';
    }
    return $returnData;
}

// Add Item to DB from CSV
function addStock($allData, $facilityId) {
    $rowDatas = $allData['data'];
    $addErr = array();
    for ($count = 0; $count < count($rowDatas); $count++) {
        $flag = 0;
        $item = trim($rowDatas[$count][0]);
        $code = trim($rowDatas[$count][1]);
        $batch = trim($rowDatas[$count][2]);
        $expiry = empty(!$rowDatas[$count][3]) ? date("Y-m-d", strtotime($rowDatas[$count][3])) : ''; //'2014-04-25'
        $price = $rowDatas[$count][4]; // 100.00
        $qty = empty($rowDatas[$count][5]) ? 0 : $rowDatas[$count][5];
        $createdBy = $_SESSION['authId'];
        $date = date('Y-m-d H:i:s');
        $reason = '';
        if ($qty > 0) { // Itemns having qty zero will not upload
            try {
                $GLOBALS['pdoobject']->begin_transaction();
                $sqlItemMaster = "SELECT inv_im_id,inv_im_name,inv_im_code,inv_im_sale,inv_im_isExpiry "
                        . "FROM inv_item_master WHERE inv_im_status = '1' AND inv_im_deleted ='0' AND inv_im_code ='$code'";
                $master_result = $GLOBALS['pdoobject']->custom_query($sqlItemMaster, null, '', 'fetch');
                if (!empty($master_result)) {
                    $id = $master_result['inv_im_id']; // store item's id
                    $sale = $master_result['inv_im_sale']; // Is Item saleable or not
                    $expire = $master_result['inv_im_isExpiry']; // Is Item hv expiry or not
                    $flag = 1; // for process of insertion in transaction table
                } else {
                    $flag = 0; // to display failed rows of CSV
                    $reason = "Product Code is Incorrect";
                }
                if ($flag == 1) { // if item and code already exists
                    
                     $queryString = array('invist_itemid' => $id, 'invist_clinic_id' => $facilityId);
                    if (!empty($batch)) {
                        $queryString['invist_batch'] = $batch;
                    } elseif ($expiry != '') {
                        $queryString['invist_expiry'] = $expiry;
                    } else {
                        $queryString['invist_batch'] = "";
                        $queryString['invist_expiry'] = "0000-00-00";
                    }
                    
                    
                    /*$queryString = array('invist_clinic_id' => $facilityId, 'invist_isdeleted' => 0, 'invist_itemid' => $id);
                    if (!empty($batch)) {
                        $queryString['invist_batch'] = $batch;
                    }*/
                    //print_r($queryString);
                    $stock_result = $GLOBALS['pdoobject']->fetch_multi_row('inv_item_stock', array('invist_id', 'invist_quantity', 'invist_expiry'), $queryString, 'FOR UPDATE');
                    //echo "<pre>";
                    //print_r($stock_result);die();
                    if ($stock_result) {
                        $befor_qty = $stock_result['0']['invist_quantity'];
                        $after_qty = $befor_qty + $qty;
                        $stockid = $stock_result['0']['invist_id'];
                        /*if ($expire=='1' && $expiry != $stock_result['0']['invist_expiry']) {
                            $flag = 0;
                            $reason = "Expiry date doesn't matched for this batch";
                        } else {*/
                            $insertCondition = checkInsertCondition($sale, $expire, $expiry, $price);
                            $flag = $insertCondition['flag'];
                            $reason = $insertCondition['reason'];
                        //}
                        if ($flag != 0) {
                            $GLOBALS['pdoobject']->update('inv_item_stock', array('invist_quantity' => $after_qty, 'invist_expiry'=>$insertCondition['expiry'],'invist_price'=>$insertCondition['price']), array('invist_id' => $stockid));
                            $transArr = transactionArr($id, $batch, $insertCondition['expiry'], $insertCondition['price'], $qty, $befor_qty, $after_qty, $facilityId, 5, $createdBy, $date);
                            $GLOBALS['pdoobject']->insert("inv_item_stock_transaction", $transArr);
                        }
                    } else {
                        $insertCondition = checkInsertCondition($sale, $expire, $expiry, $price);
                        $flag = $insertCondition['flag'];
                        if ($flag != 0) {
                            $stockData = array(
                                'invist_itemid' => $id,
                                'invist_batch' => $batch,
                                'invist_expiry' => $insertCondition['expiry'],
                                'invist_price' => $insertCondition['price'],
                                'invist_quantity' => $qty,
                                'invist_clinic_id' => $facilityId,
                                'invist_createdby' => $_SESSION['authId'],
                                'invist_created_date' => $date
                            );
                            $GLOBALS['pdoobject']->insert("inv_item_stock", $stockData);
                            $transArr = transactionArr($id, $batch, $insertCondition['expiry'], $insertCondition['price'], $qty, 0, $qty, $facilityId, 1, $createdBy, $date);
                            $GLOBALS['pdoobject']->insert("inv_item_stock_transaction", $transArr);
                        } else {
                            $reason = $insertCondition['reason'];
                        }
                    }
                }
                $GLOBALS['pdoobject']->commit();
            } catch (Exception $ex) {
                $GLOBALS['pdoobject']->rollback();
            }
        } else {
            $reason = "Quanity can not be 0 or blank";
        }
        // if item,code not exists ,then store CSV row into temp array to display failed rows to user
        if ($flag == 0) {
            $addErr[$count]['item'] = $item;
            $addErr[$count]['code'] = $code;
            $addErr[$count]['batch'] = $batch;
            $addErr[$count]['expiry'] = $expiry;
            $addErr[$count]['price'] = $price;
            $addErr[$count]['qty'] = $qty;
            $addErr[$count]['reason'] = $reason;
        }
    }
    return $addErr;
}

function checkInsertCondition($sale, $expire, $expiry, $price) {
    $flag = 1;
    if ($sale == '1') {
        if ($price == '') {
            $flag = 0; // No entry in DB if price not exists for saleable item
            $reason = "Price can not be blank for salable items";
        } else {
            $price = $price;
        }
    } else {
        $price = '0.00';
    }
    if ($expire == '1') {
        if ($expiry == '') {
            $flag = 0; // No entry in DB if expiry not exists for expire item
            $reason = "Expiry can not be blank for expire items";
        } else {
            $expiry = $expiry;
        }
    } else {
        $expiry='0000-00-00';
//        if ($expiry == '') {
//            $expiry = '';
//        } else {
//            $expiry = $expiry;
//        }
    }
    $returnData = array('price' => $price, 'expiry' => $expiry, 'flag' => $flag, 'reason' => $reason);
    return $returnData;
}

function transactionArr($itemId, $batch, $expiry, $price, $qty, $beforeQty, $afterQty, $facilityId, $transType, $createdBy, $createdDate) {
    $tranData = array(
        'invistr_itemid' => $itemId,
        'invistr_batch' => $batch,
        'invistr_expiry' => (!empty($expiry)) ? $expiry : '0000-00-00',
        'invistr_price' => (!empty($price)) ? $price : '0.00',
        'invistr_quantity' => $qty,
        'invistr_before_qty' => $beforeQty,
        'invistr_after_qty' => $afterQty,
        'invistr_clinic_id' => $facilityId,
        'invistr_tran_type' => $transType,
        'invistr_createdby' => $createdBy,
        'invistr_created_date' => $createdDate
    );
    return $tranData;
}
?>