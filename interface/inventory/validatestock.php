<?php

//$sanitize_all_escapes = true;
//$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");

$queryString=array('invist_itemid'=>$_REQUEST['itemid'],'invist_clinic_id'=>$_REQUEST['facility']);
if($_REQUEST['form_batch']!=''){
    $queryString['invist_batch']=$_REQUEST['form_batch'];
    $message="Stock for this item having same batch already exist. Do you want to add stock to same batch, if yes please click on checkbox and do save again.";
}elseif($_REQUEST['form_expiryDate']!=''){
    $queryString['invist_expiry']=$_REQUEST['form_expiryDate'];
    $message="Stock for this item having same expiry date alredy exist, Do you want to add stock, if yes please click on checkbox and do save again.";
}else{
    $queryString['invist_batch']=$_REQUEST['form_batch'];
    $queryString['invist_expiry']=$_REQUEST['form_expiryDate'] == "" ? "0000-00-00" : $_REQUEST['form_expiryDate'];
    $message="Stock for this item without batch already exist. Do you want to add stock, if yes please click on checkbox and do save again.";
}
//echo print_r($queryString);die();
//$valdate=validateStock($queryString,$pdoobject);
$valdate=$pdoobject->fetch_multi_row("inv_item_stock",array("invist_quantity","invist_id"),$queryString); 

//print_r($valdate);

if($valdate){
    $response=array('status'=>'1','message'=>$message,'data'=>$valdate);
}else{
   $response=array('status'=>'0','message'=>'No stock for this item exist'); 
}
echo json_encode($response);


