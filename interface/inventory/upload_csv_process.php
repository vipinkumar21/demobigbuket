<?php
/**
*
* Upload Category CSV : (Swati Jain)
*
**/

set_time_limit(0);
 
require_once("../globals.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require("$srcdir/spreadsheet_reader/php-excel-reader/excel_reader2.php");
require("$srcdir/spreadsheet_reader/SpreadsheetReader.php");

$updir = $_SERVER['DOCUMENT_ROOT'].$web_root."/sites/" . $_SESSION['site_id'] . "/inventory/"; // uploading directory

// For Uploading Category CSV
if(isset($_POST['mode']) && $_POST['mode']=='import_category'){
	$_SESSION['msg'] = '';
	$categoryFileInfo = fileUpload($_FILES['category_file'], $updir);
	
	 if(!empty($categoryFileInfo['url']) && $categoryFileInfo['status'] == true && file_exists($categoryFileInfo['url'])) {
		$Filepath = $categoryFileInfo['url'];
		$fileDatas = readUploadedFileData($Filepath);
	
		if(is_array($fileDatas) && !empty($fileDatas)){ // Correct CSV file upload
			if(array_key_exists('column', $fileDatas)){
				if(count($fileDatas['column']) == 2){ // checking correct columns in CSV
					addCategory($fileDatas, $facilityID);
					$_SESSION['msg'] = "Category Insertion Successful..";					
				}else{
					$_SESSION['msg'] = 'Invalid CSV Format..';					
				}
				unlink($categoryFileInfo['url']);	// delete file from folder after processing
			}
		}
	 }else{	// Wrong file upload
	 	$_SESSION['msg'] = 'Incorrect File Upload..';
	}
	
	?>
	<script>
		window.location.href='upload_category.php';	// redirect to upload csv page
	</script>
	<?php
}

// For Uploading Category CSV
if(isset($_POST['mode']) && $_POST['mode']=='import_subcategory'){
	$_SESSION['msg'] = '';
	$subCategoryFileInfo = fileUpload($_FILES['subCategory_file'], $updir);

	if(!empty($subCategoryFileInfo['url']) && $subCategoryFileInfo['status'] == true && file_exists($subCategoryFileInfo['url'])) {
		$Filepath = $subCategoryFileInfo['url'];
		$fileDatas = readUploadedFileData($Filepath);

		if(is_array($fileDatas) && !empty($fileDatas)){ // Correct CSV file upload
			if(array_key_exists('column', $fileDatas)){
				if(count($fileDatas['column']) == 2){ // checking correct columns in CSV
					addSubCategory($fileDatas, $facilityID);
					$_SESSION['msg'] = "Sub-Category Insertion Successful..";					
				}else{
					$_SESSION['msg'] = 'Invalid CSV Format..';					
				}
				unlink($subCategoryFileInfo['url']);	// delete file from folder after processing
			}
		}
	}else{	// Wrong file upload
		$_SESSION['msg'] = 'Incorrect File Upload..';
	}

	?>
	<script>
		window.location.href='upload_subcategory.php';	// redirect to upload csv page
	</script>
	<?php
}

// For Uploading UOM CSV
if(isset($_POST['mode']) && $_POST['mode']=='import_uom'){
	$_SESSION['msg'] = '';
	$uomFileInfo = fileUpload($_FILES['uom_file'], $updir);

	if(!empty($uomFileInfo['url']) && $uomFileInfo['status'] == true && file_exists($uomFileInfo['url'])) {
		$Filepath = $uomFileInfo['url'];
		$fileDatas = readUploadedFileData($Filepath);

		if(is_array($fileDatas) && !empty($fileDatas)){ // Correct CSV file upload
			if(array_key_exists('column', $fileDatas)){
				if(count($fileDatas['column']) == 2){ // checking correct columns in CSV
					addUom($fileDatas, $facilityID);
					$_SESSION['msg'] = "Unit Of Measurement Insertion Successful..";
				}else{
					$_SESSION['msg'] = 'Invalid CSV Format..';
				}
				unlink($uomFileInfo['url']);	// delete file from folder after processing
			}
		}
	}else{	// Wrong file upload
		$_SESSION['msg'] = 'Incorrect File Upload..';
	}

	?>
	<script>
		window.location.href='upload_uom.php';	// redirect to upload csv page
	</script>
	<?php
}

// For Uploading Item CSV
if(isset($_POST['mode']) && $_POST['mode']=='import_item'){
	$_SESSION['msg'] = '';
	$itemFileInfo = fileUpload($_FILES['item_file'], $updir);

	if(!empty($itemFileInfo['url']) && $itemFileInfo['status'] == true && file_exists($itemFileInfo['url'])) {
		$Filepath = $itemFileInfo['url'];
		$fileDatas = readUploadedFileData($Filepath);

		if(is_array($fileDatas) && !empty($fileDatas)){ // Correct CSV file upload
			if(array_key_exists('column', $fileDatas)){
				if(count($fileDatas['column']) == 8){ // checking correct columns in CSV
					addItem($fileDatas, $facilityID);
					$_SESSION['msg'] = "Item Insertion Successful..";
				}else{
					$_SESSION['msg'] = 'Invalid CSV Format..';
				}
				unlink($itemFileInfo['url']);	// delete file from folder after processing
			}
		}
	}else{	// Wrong file upload
		$_SESSION['msg'] = 'Incorrect File Upload..';
	}

	?>
	<script>
		window.location.href='upload_item.php';	// redirect to upload csv page
	</script>
	<?php
}

// Read columns and data of uploaded CSV file
function readUploadedFileData($Filepath){
	
	$returnFileData = array();
	$columnNames=array();
	$rowData = array();
	$Spreadsheet = new SpreadsheetReader($Filepath);
	$Sheets = $Spreadsheet -> Sheets();
	foreach ($Sheets as $Index => $Name){
		$Spreadsheet -> ChangeSheet($Index);
		foreach ($Spreadsheet as $Key => $Row){
			if($Key == 0){
				if($Row){
					$columnNames=$Row;
				}else{
					$columnNames=$Row;
				}
			}else {
				if($Row){
					$rowData[] = $Row;
				}else{
					$rowData[] = $Row;
				}
			}
			
		}
	}
	$returnFileData['column'] = $columnNames;
	$returnFileData['data'] = $rowData;
	return $returnFileData;
}

// Checking Upload file status
function fileUpload($fileData, $updir){
	$doit = true;
	$returnData = array();
	$c_filename = '';
	$max_size = 2000;         // sets maximum file size allowed (in KB)
	// file types allowed
	$allowtype = array('csv');
	if (isset($fileData)) {
		// check for errors
		if ($fileData['error'] > 0) {
			$doit = false;
			$returnData['status'] = $doit;
			$returnData['url'] = '';
		}
		else {
			// get the name, size (in kb) and type (the extension) of the file
			$fname = $fileData['name'];
			$fsize = $fileData['size'] / 1024;
			$ftype = end(explode('.', strtolower($fname)));
			$newFileName = time().'_'.$fname;
			// checks if the file already exists
			if (file_exists($updir. $newFileName)) {
				$doit = false;
				$returnData['status'] = $doit;
				$returnData['url'] = '';
			}
			else {
		  // if the file not exists, check its type (by extension) and size
		  if (in_array($ftype, $allowtype)) {
		  	// check the size
		  	if ($fsize <= $max_size) {
		  		// uses  function to copy the file from temporary folder to $updir
		  		if (!move_uploaded_file ($fileData['tmp_name'], $updir. $newFileName)) {
		  			$doit = false;
		  			$returnData['status'] = $doit;
		  			$returnData['url'] = '';
		  		}
		  		else {
		  			$c_filename = $updir.$newFileName;
		  			$returnData['status'] = $doit;
		  			$returnData['url'] = $c_filename;
		  		}
		  	}
		  	else {
		  		$doit = false;
		  		$returnData['status'] = $doit;
		  		$returnData['url'] = '';
		  	}
		  }
		  else {
		  	$doit = false;
		  	$returnData['status'] = $doit;
		  	$returnData['url'] = '';
		  }
			}
		}
	}else{
		$doit = false;
		$returnData['status'] = $doit;
		$returnData['url'] = '';
	}
	return $returnData;
}

// Add Categories to DB from CSV
function addCategory($allData, $facilityID){
	$columns = $allData['column'];
	$rowDatas = $allData['data'];

	$finalDataRows = array();
	for($count = 0; $count < count($rowDatas); $count++){

		$name = $rowDatas[$count][0];	// Table columns data
		$desc = $rowDatas[$count][1];
		$status = 1;
		$deleted = 0;
		$createdBy = 1;

		sqlInsert("INSERT INTO inv_category ( " .
				"`invcat_id`, `invcat_name`, `invcat_desc`, `invcat_status`, `invcat_deleted`, `invcat_createdby`".
				") VALUES ( " .
				"'" . '' . "', " .
				"'" . add_escape_custom($name) . "', " .
				"'" . add_escape_custom($desc) . "', " .
				"'" . add_escape_custom($status)			. "', " .
				"'" . add_escape_custom($deleted)			. "', " .
				"'" . add_escape_custom($createdBy)       . "' " .
				")");
	}
}

// Add Sub-Categories to DB from CSV
function addSubCategory($allData, $facilityID){
	$columns = $allData['column'];
	$rowDatas = $allData['data'];

	$finalDataRows = array();
	for($count = 0; $count < count($rowDatas); $count++){

		$name = $rowDatas[$count][0];	// Table columns data
		$desc = $rowDatas[$count][1];
		$status = 1;
		$deleted = 0;
		$createdBy = 1;

		sqlInsert("INSERT INTO inv_subcategory ( " .
				"`invsubcat_id`, `invsubcat_name`, `invsubcat_desc`, `invsubcat_status`, `invsubcat_deleted`, `invsubcat_createdby`".
				") VALUES ( " .
				"'" . '' . "', " .
				"'" . add_escape_custom($name) . "', " .
				"'" . add_escape_custom($desc) . "', " .
				"'" . add_escape_custom($status)			. "', " .
				"'" . add_escape_custom($deleted)			. "', " .
				"'" . add_escape_custom($createdBy)       . "' " .
				")");
	}
}

// Add UOM to DB from CSV
function addUom($allData, $facilityID){
	$columns = $allData['column'];
	$rowDatas = $allData['data'];

	$finalDataRows = array();
	for($count = 0; $count < count($rowDatas); $count++){

		$name = $rowDatas[$count][0];	// Table columns data
		$desc = $rowDatas[$count][1];
		$status = 1;
		$deleted = 0;
		$createdBy = 1;

		sqlInsert("INSERT INTO inv_uom ( " .
				"`invuom_id`, `invuom_name`, `invuom_desc`, `invuom_status`, `invuom_deleted`, `invuom_createdby`".
				") VALUES ( " .
				"'" . '' . "', " .
				"'" . add_escape_custom($name) . "', " .
				"'" . add_escape_custom($desc) . "', " .
				"'" . add_escape_custom($status)			. "', " .
				"'" . add_escape_custom($deleted)			. "', " .
				"'" . add_escape_custom($createdBy)       . "' " .
				")");
	}
}

// Add Item to DB from CSV
function addItem($allData, $facilityID){
	$columns = $allData['column'];
	$rowDatas = $allData['data'];
		
	$finalDataRows = array();
	for($count = 0; $count < count($rowDatas); $count++){
		$flag = 0;
		
		// getting existing categories,subcategories & uom
		$cat_result = sqlStatement("SELECT invcat_id,invcat_name FROM invcategories WHERE is_active = '1' AND deleted ='0' ");
		//$subcat_result = sqlStatement("SELECT invsubcat_id,invsubcat_name FROM inv_subcategory WHERE invsubcat_status = '1' AND invsubcat_deleted ='0' ");
		$uom_result = sqlStatement("SELECT invuom_id,invuom_name FROM inv_uom WHERE invuom_status = '1' AND invuom_deleted ='0' ");
		
		$item = $rowDatas[$count][0];	// Table columns data
		$code = $rowDatas[$count][1];
		$cat = $rowDatas[$count][2];
		$subcat = $rowDatas[$count][3];
		$desc = $rowDatas[$count][4];
		$uom = $rowDatas[$count][5];
		$status = 1;
		$expiry = $rowDatas[$count][7]; // 0:no exp date ; 1:exp date
		$sale = $rowDatas[$count][6]; // 0:non saleable ; 1:saleable
		$deleted = 0;
		$createdBy = 1;
			
		while ($row = sqlFetchArray($cat_result)) {		// if cat exists then put its id in DB else put 0
			if(strtolower($row['invcat_name']) == strtolower($cat)){
				$flag = 1;
				$cat = $row['invcat_id'];
			}
		}
		if($flag==0){
			$cat = 0;
		}
		
		$flag = 0; 
		while ($row = sqlFetchArray($subcat_result)) {		// if subcat exists then put its id in DB else put 0
			if(strtolower($row['invsubcat_name']) == strtolower($subcat)){
				$flag = 1;
				$subcat = $row['invsubcat_id'];
			}
		}
		if($flag==0){
			$subcat = 0;
		}
		
		$flag = 0;
		while ($row = sqlFetchArray($uom_result)) {			// if uom exists then put its id in DB else put 0
			if(strtolower($row['invuom_name']) == strtolower($uom)){
				$flag = 1;
				$uom = $row['invuom_id'];
			}
		}
		if($flag==0){
			$uom = 0;
		}
		// insert csv data to table
		sqlInsert("INSERT INTO inv_item_master ( " .
				"`inv_im_id`, `inv_im_name`, `inv_im_code`, `inv_im_catId`, `inv_im_subcatId`, `inv_im_desc`,`inv_im_isExpiry`,`inv_im_status`,`inv_im_sale`,`inv_im_uomId`,`inv_im_deleted`,`inv_im_createdby`".
				") VALUES ( " .
				"'" . '' . "', " .
				"'" . add_escape_custom($item) . "', " .
				"'" . add_escape_custom($code) . "', " .
				"'" . add_escape_custom($cat)	. "', " .
				"'" . add_escape_custom($subcat)	. "', " .
				"'" . add_escape_custom($desc)    . "', " .
				"'" . add_escape_custom($expiry)  . "', " .
				"'" . add_escape_custom($status)   . "', " .
				"'" . add_escape_custom($sale)    . "', " .
				"'" . add_escape_custom($uom)     . "', " .
				"'" . add_escape_custom($deleted)     . "', " .
				"'" . add_escape_custom($createdBy)   . "' " .
					
				")");
		
		/********************************* This is added by Prashant (11/06/2014) *********************************************/
		//$res = sqlStatement("SELECT icsm_id FROM inv_cat_subcat_map WHERE icsm_catid = ? AND icsm_scatid = ?", array($cat, $subcat));
		if(!sqlNumRows($res)){
			// insert csv data to table
			sqlInsert("INSERT INTO inv_cat_subcat_map ( " .
					"`icsm_catid`, `icsm_scatid`, `icsm_deleted`".
					") VALUES ( " .
					"'" . add_escape_custom($cat) . "', " .
					"'" . add_escape_custom($subcat) . "', " .
					"'0' " .
					")");
		}
		
		
		
	}
}


?>