<?php
$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 $facility = $_REQUEST['facilityId'];
 if ($facility) {
 	$res = sqlStatement("SELECT ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name " .
		"FROM inv_item_stock AS ist  
		 INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid 
		  " .
                "WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invist_quantity > 0 AND ist.invist_clinic_id = '".$facility."'");
 	if(sqlNumRows($res)){
 		$optionString = "<option value='' selected='selected'>Select Item</option>";
 		while ($row = sqlFetchArray($res)) { 			
 			$optionString .= "<option value='".$row['invist_id']."#".$row['invist_itemid']."#".$row['invist_quantity']."'>".$row['inv_im_name']." ".$row['invist_batch']." ".$row['invist_expiry']."</option>"; 			
 		}
 		echo $optionString;
 	}else{
 		echo "<option value='' selected='selected'>Select Item</option>";
 	}
 }else {
 	echo "<option value='' selected='selected'>Select Item</option>";
 }