<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $adjid = $_REQUEST['adjid'];
 $facility = $_REQUEST['facility'];
 $info_msg = "";
 $tmpl_line_no = 0;

 if (!acl_check('inventory', 'invf_adj_edit')) die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
	<?php html_header_show(); ?>
	<title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
    <?php include_once("themestyle.php"); ?>
    <?php include_once("scriptcommon.php"); ?>
	<script language="JavaScript">
		<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
		// This is for callback by the find-code popup.
		// Appends to or erases the current list of related codes.
		function set_related(codetype, code, selector, codedesc) {
			var f = document.forms[0];
			var s = f.form_related_code.value;
			if (code) {
			if (s.length > 0) s += ';';
			s += codetype + ':' + code;
			} else {
			s = '';
			}
			f.form_related_code.value = s;
		}

		function trimAll(sString) {
			while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}
			while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
			return sString;
		} 

		function isNumber(evt) {
		    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;
		}

		function submitform() {
			//alert(document.forms[0].form_qty.value.length);
			if (document.forms[0].facility.value>0 && document.forms[0].itemid.value.length>0 && document.forms[0].adjType.value.length>0 && document.forms[0].form_qty.value.length>0 && parseInt(document.forms[0].form_qty.value) > 0) {
				var itemStockValue = document.forms[0].itemid.value;
				var itemStockArr = itemStockValue.split('#');		
				if(document.forms[0].adjType.value == 'In'){
					var difQty = 0;
					if(parseInt(document.forms[0].form_qty.value) > parseInt(document.forms[0].old_qty.value)){
						return true;
					}else {
						difQty = parseInt(document.forms[0].old_qty.value)-parseInt(document.forms[0].form_qty.value);
						if(itemStockArr[2] >= parseInt(difQty)) {
							return true;
						}else {
							document.forms[0].form_qty.style.backgroundColor="red";
							alert("<?php xl('You can not change Item quantity adjustment more than in stock.','e');?>");
							document.forms[0].form_qty.focus();
							return false;
						}
					}
				}else {
					var difQty = 0;
					if(parseInt(document.forms[0].form_qty.value) > parseInt(document.forms[0].old_qty.value)){
						return true;
						difQty = parseInt(document.forms[0].form_qty.value)-parseInt(document.forms[0].old_qty.value);
						if(itemStockArr[2] >= parseInt(difQty)) {
							return true;
						}else {
							document.forms[0].form_qty.style.backgroundColor="red";
							alert("<?php xl('You can not change Item quantity adjustment more than in stock.','e');?>");
							document.forms[0].form_qty.focus();
							return false;
						}
					}else {
						return true;
					}
				}
				
			} else {
				if (document.forms[0].facility.value<=0)
				{
					document.forms[0].facility.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Please choose the facility.','e');?>");
					document.forms[0].facility.focus();
					return false;
				}				
				if (document.forms[0].itemid.value.length<=0)
				{
					document.forms[0].itemid.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Please choose the item.','e');?>");
					document.forms[0].itemid.focus();
					return false;
				}
				if (document.forms[0].adjType.value.length<=0)
				{
					document.forms[0].adjType.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Please choose the adjustment type.','e');?>");
					document.forms[0].adjType.focus();
					return false;
				}
				if (document.forms[0].form_qty.value.length<=0 || parseInt(document.forms[0].form_qty.value) <= 0)
				{
					document.forms[0].form_qty.style.backgroundColor="red";
					alert("<?php xl('Required field missing: Please enter the Item quantity.','e');?>");
					document.forms[0].form_qty.focus();
					return false;
				}				
			}
		}
	</script>
</head>
<body>
<?php
// If we are saving, then save and close the window.
// First check for duplicates.
//
if ($_POST['form_save']) {	
   //$new_drug = true;
   //print_r($_POST);
	$itemStockValue = $_POST['itemid'];
	$itemStockArr = explode('#', $itemStockValue);
	$itemStockDetails = getItemStockDetail($itemStockArr[0]);
	//print_r($itemStockDetails);
	//die();
	if($_POST['adjType'] == 'Out'){
		if($_POST['form_qty'] > $_POST['old_qty']){			// if input new qty is more than previous one
			if($itemStockDetails['invist_quantity'] >= ($_POST['form_qty']-$_POST['old_qty'])){	
				//sqlStatement("update inv_item_stock set invist_quantity=invist_quantity-".($_POST['form_qty']-$_POST['old_qty'])." where invist_id = '" . $itemStockArr[0] . "'");				 
				removeStockQuantity($_POST['form_qty']-$_POST['old_qty'], $itemStockArr[0]);
				$adjData=array(
						'ina_id' => escapedff('ina_id'),
						//'ina_item_id' => $itemStockArr[1],
						//'ina_stock_id' => $itemStockArr[0],
						'ina_quantity' => escapedff('form_qty'),
						//'ina_type' => escapedff('adjType'),
						//'ina_facility_id' => escapedff('facility'),
						//'ina_addedby' => $_SESSION['authId'],
						//'ina_added_date' => date("Y-m-d H:i:s")
				);
				$adjId = updateAdjustment($adjData);
				//$adjId = insertUpdateAdjustment($adjData);
				$diff_qty = escapedff('form_qty')-escapedff('old_qty');
				$tranData=array(
						'invistr_id' => '',
						'invistr_itemid' => $itemStockDetails['invist_itemid'],
						'invistr_batch' => $itemStockDetails['invist_batch'],
						'invistr_expiry' => $itemStockDetails['invist_expiry'],
						'invistr_price' => $itemStockDetails['invist_price'],
						'invistr_quantity' => $diff_qty,
						'invistr_before_qty' => $itemStockDetails['invist_quantity'],
						'invistr_after_qty' => $itemStockDetails['invist_quantity']-$diff_qty,
						'invistr_clinic_id' => escapedff('facility'),
						'invistr_tran_type' => 5,
						'invistr_comment' => escapedff('form_desc'),
						'invistr_createdby' => $_SESSION['authId'],
						'invistr_created_date' => date("Y-m-d H:i:s")
				);
				$transId = insertStockTransaction($tranData);				
				// Close this window and redisplay the updated list of drugs.				
				echo "<script language='JavaScript'>\n";
				if ($info_msg) echo " alert('$info_msg');\n";
				echo " parent.location.reload();\n";
				echo "</script></body></html>\n";
				exit();
			}else {
				$alertmsg='Entered quantity not available in selected item stock. Please try again.';
			}
		}else {	// if old qty > new qty 
			//sqlStatement("update inv_item_stock set invist_quantity=invist_quantity+".($_POST['old_qty']-$_POST['form_qty'])." where invist_id = '" . $itemStockArr[0] . "'");
			addStockQuantity($_POST['old_qty']-$_POST['form_qty'], $itemStockArr[0]);
			$adjData=array(
					'ina_id' => escapedff('ina_id'),
					//'ina_item_id' => $itemStockArr[1],
					//'ina_stock_id' => $itemStockArr[0],
					'ina_quantity' => escapedff('form_qty'),
					//'ina_type' => escapedff('adjType'),
					//'ina_facility_id' => escapedff('facility'),
					//'ina_addedby' => $_SESSION['authId'],
					//'ina_added_date' => date("Y-m-d H:i:s")
			);
			$adjId = updateAdjustment($adjData);
			//$adjId = insertUpdateAdjustment($adjData);
			$diff_qty = escapedff('old_qty')-escapedff('form_qty');
			$tranData=array(
					'invistr_id' => '',
					'invistr_itemid' => $itemStockDetails['invist_itemid'],
					'invistr_batch' => $itemStockDetails['invist_batch'],
					'invistr_expiry' => $itemStockDetails['invist_expiry'],
					'invistr_price' => $itemStockDetails['invist_price'],
					'invistr_quantity' => $diff_qty,
					'invistr_before_qty' => $itemStockDetails['invist_quantity'],
					'invistr_after_qty' => $itemStockDetails['invist_quantity']+$diff_qty,
					'invistr_clinic_id' => escapedff('facility'),
					'invistr_tran_type' => 16,
					'invistr_comment' => escapedff('form_desc'),
					'invistr_createdby' => $_SESSION['authId'],
					'invistr_created_date' => date("Y-m-d H:i:s")
			);
			$transId = insertStockTransaction($tranData);			
			// Close this window and redisplay the updated list of drugs.
			//
			echo "<script language='JavaScript'>\n";
				if ($info_msg) echo " alert('$info_msg');\n";
				echo " parent.location.reload();\n";
				echo "</script></body></html>\n";
				exit();
		}
		
	}else {	// "In" case
		if($_POST['form_qty'] > $_POST['old_qty']){	// Input qty > old qty
			//sqlStatement("update inv_item_stock set invist_quantity=invist_quantity+".($_POST['form_qty']-$_POST['old_qty'])." where invist_id = '" . $itemStockArr[0] . "'");
			addStockQuantity($_POST['form_qty']-$_POST['old_qty'], $itemStockArr[0]);
			$adjData=array(
					'ina_id' => escapedff('ina_id'),
					//'ina_item_id' => $itemStockArr[1],
					//'ina_stock_id' => $itemStockArr[0],
					'ina_quantity' => escapedff('form_qty'),
					//'ina_type' => escapedff('adjType'),
					//'ina_facility_id' => escapedff('facility'),
					//'ina_addedby' => $_SESSION['authId'],
					//'ina_added_date' => date("Y-m-d H:i:s")
			);
			$diff_qty = escapedff('form_qty')-escapedff('old_qty');
			$adjId = updateAdjustment($adjData);
			$tranData=array(
					'invistr_id' => '',
					'invistr_itemid' => $itemStockDetails['invist_itemid'],
					'invistr_batch' => $itemStockDetails['invist_batch'],
					'invistr_expiry' => $itemStockDetails['invist_expiry'],
					'invistr_price' => $itemStockDetails['invist_price'],
					'invistr_quantity' => $diff_qty,
					'invistr_before_qty' => $itemStockDetails['invist_quantity'],
					'invistr_after_qty' => $itemStockDetails['invist_quantity']+$diff_qty,
					'invistr_clinic_id' => escapedff('facility'),
					'invistr_tran_type' => 5,
					'invistr_comment' => escapedff('form_desc'),
					'invistr_createdby' => $_SESSION['authId'],
					'invistr_created_date' => date("Y-m-d H:i:s")
			);
			$transId = insertStockTransaction($tranData);			
			// Close this window and redisplay the updated list of drugs.
			//
			echo "<script language='JavaScript'>\n";
			if ($info_msg) echo " alert('$info_msg');\n";
			echo " parent.location.reload();\n";
			echo "</script></body></html>\n";
			exit();
 		}else {		// old qty > new qty	
			if($itemStockDetails['invist_quantity'] >= ($_POST['old_qty']-$_POST['form_qty'])){
				//sqlStatement("update inv_item_stock set invist_quantity=invist_quantity-".($_POST['old_qty']-$_POST['form_qty'])." where invist_id = '" . $itemStockArr[0] . "'");
				removeStockQuantity($_POST['old_qty']-$_POST['form_qty'], $itemStockArr[0]);
				$adjData=array(
						'ina_id' => escapedff('ina_id'),
						//'ina_item_id' => $itemStockArr[1],
						//'ina_stock_id' => $itemStockArr[0],
						'ina_quantity' => escapedff('form_qty'),
						//'ina_type' => escapedff('adjType'),
						//'ina_facility_id' => escapedff('facility'),
						//'ina_addedby' => $_SESSION['authId'],
						//'ina_added_date' => date("Y-m-d H:i:s")
				);
				$adjId = updateAdjustment($adjData);
				$diff_qty = escapedff('old_qty')-escapedff('form_qty');
				$tranData=array(
						'invistr_id' => '',
						'invistr_itemid' => $itemStockDetails['invist_itemid'],
						'invistr_batch' => $itemStockDetails['invist_batch'],
						'invistr_expiry' => $itemStockDetails['invist_expiry'],
						'invistr_price' => $itemStockDetails['invist_price'],
						'invistr_quantity' => $diff_qty,
						'invistr_before_qty' => $itemStockDetails['invist_quantity'],
						'invistr_after_qty' => $itemStockDetails['invist_quantity']-$diff_qty,
						'invistr_clinic_id' => escapedff('facility'),
						'invistr_tran_type' => 16,
						'invistr_comment' => escapedff('form_desc'),
						'invistr_createdby' => $_SESSION['authId'],
						'invistr_created_date' => date("Y-m-d H:i:s")
				);
				$transId = insertStockTransaction($tranData);				
				// Close this window and redisplay the updated list of drugs.
				//
				echo "<script language='JavaScript'>\n";
						if ($info_msg) echo " alert('$info_msg');\n";
						echo " parent.location.reload();\n";
				echo "</script></body></html>\n";
				exit();
			}else {
				$alertmsg='Entered quantity not available in selected item stock. Please try again.';
			}
		}		
	}
}
if ($adjid && $facility) {
	$row = sqlQuery("SELECT adj.ina_id, adj.ina_item_id, adj.ina_stock_id, adj.ina_quantity, adj.ina_type, adj.ina_facility_id, adjrel.iatr_id, tran.invistr_id, tran.invistr_before_qty, tran.invistr_after_qty, tran.invistr_comment 
FROM inv_adjustments AS adj 
INNER JOIN inv_adj_trans_rel AS adjrel ON adjrel.iatr_adj_id = adj.ina_id 
INNER JOIN inv_item_stock_transaction AS tran ON tran.invistr_id = adjrel.iatr_trans_id 
WHERE adj.ina_id= ? AND adj.ina_facility_id= ?", array($adjid,$facility));
}else {
	$row = array(
			'ina_id' => 0,
			'ina_item_id' => 0,
			'ina_stock_id' => 0,
			'ina_quantity' => 0,
			'ina_type' => '',
			'ina_facility_id' => 0,
			'iatr_id'=> 0,
			'invistr_id'=> 0,
			'invistr_before_qty'=> 0,
			'invistr_after_qty'=> 0,
			'invistr_comment' => ''
	);
}
?>
<div class="infopop"><?php  xl('Edit Adjustment','e'); ?></div>
<!-- tableData -->
<div id='popUpformWrap'>
	<form method='post' name='theform' id='editadjustment' action='edit_adj.php?adjid=<?php echo $adjid; ?>&facility=<?php echo $facility; ?>'>
		<input name='ina_id' id='ina_id' value='<?php echo $row['ina_id'];?>' type="hidden">
		<input name='iatr_id' id='iatr_id' value='<?php echo $row['iatr_id'];?>' type="hidden">
		<input name='invistr_id' id='invistr_id' value='<?php echo $row['invistr_id'];?>' type="hidden">
		<input name='invistr_before_qty' id='invistr_before_qty' value='<?php echo $row['invistr_before_qty'];?>' type="hidden">
		<input name='invistr_after_qty' id='invistr_after_qty' value='<?php echo $row['invistr_after_qty'];?>' type="hidden">
		<input name='old_qty' id='old_qty' value='<?php echo $row['ina_quantity'];?>' type="hidden">

		<!-- Row 1 -->
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Facility'); ?></label>
					<select name="facility" id="facility">
				      <?php 
				      if(empty($row['ina_facility_id'])){
				      ?>
				      <option value='0' selected="selected">Select Facility</option>
				      <?php
				      }
					  $countUserFacilities = 0;
					  if (!$GLOBALS['restrict_user_facility']) {
						$qsql = sqlStatement("
							select id, name, color
							from facility
							where service_location != 0
						");
					  } else {
						  $qsql = sqlStatement("
							  select uf.facility_id as id, f.name, f.color
							  from users_facility uf
							  left join facility f on (uf.facility_id = f.id)
							  where uf.tablename='users' 
							  and uf.table_id = ? 
							", array($_SESSION['authId']) );
					  }      
				      while ($facrow = sqlFetchArray($qsql)) {       
						$selected = ( $facrow['id'] == $row['ina_facility_id'] ) ? 'selected="selected"' : 'disabled="disabled"' ;
				        echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";       
						$countUserFacilities++;
				        /************************************************************/
				      }      
				      ?>
      				</select>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Item'); ?></label>
					<select name='itemid' id="itemField">
				  		<?php if(!empty($row['ina_facility_id'])){
							if(empty($row['ina_item_id'])){	?>
				  				<option value='' selected="selected">Select Item</option>  		
					  		<?php } 
					  		$imcres = sqlStatement("SELECT ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name, cat.invcat_name, scat.invsubcat_name " .
							"FROM inv_item_stock AS ist  
							INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid 
							LEFT JOIN inv_subcategory AS scat ON scat.invsubcat_id = im.inv_im_subcatId 
							INNER JOIN inv_category AS cat ON cat.invcat_id = im.inv_im_catId " .
							"WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invist_quantity > 0 AND ist.invist_clinic_id = '".$row['ina_facility_id']."'");
					  		if(sqlNumRows($imcres)){
					  			while ($imcrow = sqlFetchArray($imcres)) { ?>
									<option value='<?php echo $imcrow['invist_id']."#".$imcrow['invist_itemid']."#".$imcrow['invist_quantity'];?>' <?php if($row['ina_stock_id']==$imcrow['invist_id']){ echo 'selected="selected"';}else {echo 'disabled="disabled"';} ?>> 
										<?php echo $imcrow['inv_im_name']." ".$imcrow['invist_batch']." ".$imcrow['invist_expiry'];?>
									</option>
								<?php }
							}	  		
				  		} else { ?>
							<option value='0' selected="selected">Select Item</option>
						<?php } ?>
				  	</select>  	 
				</div>
			</div>
		</div>
		<!-- Row 1 -->

		<!-- Row 2 -->
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Adjustment Type'); ?></label>
					<select name='adjType' id="adjType">  		
				  		<option value='' <?php if(''==$row['ina_type']){ echo 'selected="selected"';}else {echo 'disabled="disabled"';}?>>Select Type</option>
				  		<option value='In' <?php if('In'==$row['ina_type']){ echo 'selected="selected"';}else {echo 'disabled="disabled"';}?>>Add</option>
				  		<option value='Out' <?php if('Out'==$row['ina_type']){ echo 'selected="selected"';}else {echo 'disabled="disabled"';}?>>Remove</option>
				  	</select>  
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Quantity'); ?></label>
					<input type='text' size='10' name='form_qty' maxlength='7' onkeypress="return isNumber(event)" value='<?php echo attr($row['ina_quantity']) ?>' />
				</div>
			</div>
		</div>
		<!-- Row 2 -->
		<!-- Row 3 -->
		<div class="row">
			<div class="col-sm-6 txtarea">
				<div class="form-group">
					<label><?php echo xlt('Notes'); ?></label>
					<textarea name='form_desc' class="" rows="5" cols="34"><?php echo attr($row['invistr_comment']) ?></textarea>
				</div>
			</div>
		</div>
		<!-- Row 3 -->
		<a id="subform" href="javascript:void(0)" class="save-btn"></a>
		<input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' value='<?php echo xla('Save'); ?>' />
	</form>
</div>
<!-- tableData -->
<script language="JavaScript">
	$("#form_save").click(function(event) {
	    parent.location.reload();
	});
	<?php
		if ($alertmsg) {
			echo "alert('" . htmlentities($alertmsg) . "');\n";
		}
	?>

	$(document).ready(function() {
		$("#facility").change(function(){
			var facilityId = $("#facility").val();			
			$.ajax({
				type: 'GET',
				url:"get_clinic_item.php?facilityId="+facilityId,
				success:function(data){
					$("#itemField").html(data);					
				}
			});				
		});		
	});
</script>
</body>
</html>