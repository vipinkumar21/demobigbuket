<?php
 

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $item_id = $_REQUEST['itemid'];
 $info_msg = "";
 $tmpl_line_no = 0;


// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}

?>
<html>
<head>
<?php html_header_show(); ?>
<title><?php echo $item_id ? xlt("View") : ''; echo ' ' . xlt('Item'); ?></title>
<link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>
<link rel='stylesheet' href="../themes/bootstrap.css" type="text/css">
<style>
td { font-size:10pt; }
</style>

</head>

<body class="body_top">
<?php

if ($item_id) {
  $row = sqlQuery("SELECT inv_item_images.url, im.inv_im_id, im.inv_im_name, im.inv_im_code, im.inv_im_catId, im.inv_im_desc, im.inv_im_isExpiry, im.inv_im_status, im.inv_im_sale, im.inv_im_uomId, im.inv_im_deleted, im.inv_im_createdby FROM inv_item_master as im 
                   LEFT JOIN inv_item_images  ON inv_item_images.inv_im_id = im.inv_im_id
                   WHERE im.inv_im_id = ?", array($item_id));
  
}else {
  $row = array(
    'inv_im_catId' => 0,
	'inv_im_name' => '',
	'inv_im_code' => '',
    'inv_im_desc' => '',
    'inv_im_status' => '1',
	'inv_im_sale' => '1',
	'inv_im_isExpiry' => '1',
	'inv_im_uomId' => 0    
  );
}
function getSlect($cat,$id){
    
    foreach($cat as $catid){
      if($catid==$id){
          return 1;
      }  
    }
    
    return 0;
    
}
?>
<table border='0' width='100%' class='emrtable'>   
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Category'); ?>:</b></td>
  <td>
    <?php 
        if($row[inv_im_catId]) {    
        $query= "SELECT  GROUP_CONCAT(`name`  SEPARATOR ', ') as `name` FROM `invcategories` 
        JOIN `invcategories_invitemmasters`  ON  `invcategories` .`id`= `invcategories_invitemmasters`.`invcategory_id`  
        WHERE 1 AND inv_im_id IN ($row[inv_im_catId])";        
        $cres = sqlStatement($query);
        if(sqlNumRows($cres)){
                while ($crow = sqlFetchArray($cres)) {
                        ?>
                               <?php echo $crow['name'].'<br >'; ?>
                        <?php
                }
        }
        }
    ?>  
  </td>
 </tr>
 
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Name'); ?>:</b></td>
  <td>
   <?php echo attr($row['inv_im_name']) ?><br /> <br />
   <img  src="<?php echo $row['url']; ?> ?>" >
   
      
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Code'); ?>:</b></td>
  <td>
   <?php echo attr($row['inv_im_code']) ?>
  </td>
 </tr>
<tr>
  <td valign='top' nowrap><b><?php echo xlt('Description'); ?>:</b></td>
  <td>
   <?php echo attr($row['inv_im_desc']);?>
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Unit Of Measurement'); ?>:</b></td>
  <td>
<?php 
$uomSql = "SELECT invuom_id, invuom_name FROM inv_uom WHERE invuom_status='1' AND invuom_deleted='0' and invuom_id = ".$row['inv_im_uomId'];
$uomrow = sqlQuery($uomSql);

if(!empty($uomrow['invuom_name'])){

          echo $uomrow['invuom_name'];
       
}
?></td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Has Expiry'); ?>:</b></td>
  <td>
   <?php if ($row['inv_im_isExpiry']) echo ' Yes'; else echo 'No'; ?>
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('For Sale'); ?>:</b></td>
  <td>
   <?php if ($row['inv_im_sale']) echo ' Yes'; else echo 'No'; ?>
  </td>
 </tr>  
</table>
</body>
</html>