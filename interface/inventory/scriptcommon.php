<?php
$oi_theme = isset($GLOBALS['inventory_oneivory_theme']) ? $GLOBALS['inventory_oneivory_theme'] : 0;
?>

<!-- commonscripts -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<!-- jquery lib_new -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>webapp/js/jquery/jquery-1.11.1.min.js"></script>
<!-- jqueryui -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>web/js/jquery-ui.min.js"></script>
<!-- global vairables declaration -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/contains.js"></script>

<?php if ($oi_theme == 0) { ?>	
    <!-- required for: prmtheme || "0" -->	
    <script src="<?php echo $inventry_url; ?>web/js/jquery.mobile-1.3.2.min.js"></script>
    <!-- required for: prmtheme || "0" -->	
<?php } ?>

<!-- fancyBox js -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/jquery.fancybox.js"></script>
<!-- validationJs -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>web/js/jquery.validate.min.1.11.1.js"></script>
<!-- used for inventory module -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/inventory-script.js"></script>
<!-- textarea character counter -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>web/js/textCounter.js"></script>
<!-- commonscripts -->

<?php if ($oi_theme == 1) { ?>	
<!-- floatingheader -->
<script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/transition-plug.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#collapse-bar").on('click', function () {
		var flhdr = $(".floatingheader");
		if (flhdr.hasClass("transition-hidden")) {
			flhdr.upTransition();
		} else {
			flhdr.downTransition();
			setTimeout(function(){
				$(".floatingheader").upTransition();
			}, 5000);
		}
	});

	$(document).on("mousemove", function(event) {
		var flhdr = $(".floatingheader");
	    if(event.pageY <= 4){
	    	event.stopPropagation();
			flhdr.downTransition();
			setTimeout(function(){
				$(".floatingheader").upTransition();
			}, 5000);
	    }
	});
});
</script>
<?php } ?>