<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//require_once("mag_update_status.php");
require_once("oneivoryintg.php");

$alertmsg = '';
$reqid = $_REQUEST['reqid'];
$facility = $_REQUEST['facility'];
$magorderid = $_REQUEST['magorderid'];
$info_msg = "";
$tmpl_line_no = 0;

if (!acl_check('inventory', 'invf_reqin_issue_add'))
    die(xlt('Not authorized'));

if (empty($reqid)) {
    echo "<script language='JavaScript'>\n";
    echo " alert(You have not selected requisition.);\n";
    echo " if (opener.refreshme) opener.refreshme();\n";
    echo " window.close();\n";
    echo "</script></body></html>\n";
    exit();
}

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo $reqid ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock Requisition'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            function checkInputData(fieldData) {
                var fieldName = fieldData.name;
                var fieldNameArr = fieldName.split("_");
                var itemId = fieldNameArr[1];
                var stockId = fieldNameArr[2];
                var enteredQty = fieldData.value;
                if (enteredQty == '') {
                    enteredQty = 0;
                }
                var avialableStockQty = document.getElementById("stockQtyAv_" + stockId).value;
                var requiredQty = document.getElementById("requiredQty_" + itemId).value;
                var itemStocksCount = document.getElementsByName("itemstockid_" + itemId + "[]");

                for (var i = 0; i < itemStocksCount.length; i++) {
                    var otherStockId = itemStocksCount[i].value;
                    if (parseInt(otherStockId) != parseInt(stockId)) {
                        var otherStockEnteredQty = document.getElementById("isqty_" + itemId + "_" + otherStockId).value;
                        requiredQty = parseInt(requiredQty) - parseInt(otherStockEnteredQty);
                    }
                }
                
                if (parseInt(enteredQty) > parseInt(avialableStockQty)) {
                    $(fieldData).parent('td').find('#lowaq').css('display', 'block');
                    $(fieldData).parent('td').find('#mrq').css('display', 'none');
                    fieldData.value = 0;
                    fieldData.focus();
                } else {
                    $(fieldData).parent('td').find('#lowaq').css('display', 'none');                    
                }

                if (parseInt(enteredQty) > parseInt(requiredQty)) {
                    $(fieldData).parent('td').find('#lowaq').css('display', 'none');
                    $(fieldData).parent('td').find('#mrq').css('display', 'block');
                    fieldData.value = requiredQty;
                    for (var i = 0; i < itemStocksCount.length; i++) {
                        var otherStockId = itemStocksCount[i].value;
                    }
                    fieldData.focus();
                } else {
                    $(fieldData).parent('td').find('#mrq').css('display', 'none');
                }
            };            
        </script>
    </head>
    <?php
    // If we are saving, then save and close the window.
    // First check for duplicates.
    //
	if ($_POST['form_save']) {
        //echo "<pre>";
        //print_r($_POST);
        
        //die();
        $saveIssue = 0;
        $reqItemIds = $_POST['reqItems'];
        foreach ($reqItemIds as $reqItemId) {
            $itemIdArray = explode('-', $reqItemId);
            $itemId = $itemIdArray['0'];
            $avStockIds = $_POST['itemstockid_' . $itemId];
            foreach ($avStockIds as $avStockId) {
                $enteredQty = $_POST['isqty_' . $itemId . '_' . $avStockId];
                $reqQty = $_POST['isqtyr_' . $itemId . '_' . $avStockId];
                if (!empty($enteredQty) && preg_match("/^[0-9]+$/",$enteredQty && $enteredQty<=$reqQty)) {
                    $saveIssue = 1;
                }
            }
        }
        
        if ($saveIssue) {
            try {
        $pdoobject->begin_transaction();
            $issueData = array(
                'iin_reqid' => $_POST['reqid'],
                'iin_number' => getIssueNumber($_POST['facility']),
                'iin_from_clinic' => $_POST['facility'],
                'iin_to_clinic' => $_POST['tofacility'],
                'iin_createdby' => $_SESSION['authId'],
                'iin_date' => date("Y-m-d H:i:s"),
                'issue_note' => $_POST['issue_note']
            );
            $issueid = $pdoobject->insert("inv_issue_notes", $issueData);
            $stockIssueItemData = array();
            $stockIssueItemTranData = array();
            $reqStatus = 0;
            $reqAllRequiredQty = 0;
            $allItemIssuedQty = 0;
            $i = 0;
            foreach ($reqItemIds as $reqItemId) {

                //$itemId = $reqItemId;
                $itemIdArray = explode('-', $reqItemId);
                $itemId = $itemIdArray['0'];
                $oneivory_order_item_id = $itemIdArray['1'];
                $oneivoryItem[$oneivory_order_item_id] = 0;
                $avStockIds = $_POST['itemstockid_' . $itemId];
                $reqAllRequiredQty += $_POST['requiredQty_' . $itemId];
                //$itemIssuedQty = 0;
                foreach ($avStockIds as $avStockId) {
                    $enteredQty = $_POST['isqty_' . $itemId . '_' . $avStockId];
                    if (!empty($enteredQty)) {
                        $itemStockDetails = getItemStockDetail($avStockId,1," FOR UPDATE ");
                        //print_r($itemStockDetails);
                        
                        //die();
                        if ($itemStockDetails['invist_quantity'] >= $enteredQty) {
                            $oneivoryItem[$oneivory_order_item_id] = $oneivoryItem[$oneivory_order_item_id] + $enteredQty;
                            $allItemIssuedQty+=$enteredQty;
                            $reqStatus = 1;
                            $itemData = array(
                                'iii_id' => '',
                                'iii_issueid' => $issueid,
                                'iii_itemid' => $itemId,
                                'oneivory_order_item_id' => $oneivory_order_item_id,
                                'iii_stock_id' => $avStockId,
                                'iii_quantity' => $enteredQty,
                                'iii_isdeleted' => '0'
                            );
                            
                           $stockInsertId = $pdoobject->insert("inv_issue_item", $itemData);
                           $stockIssueItemData[] = $itemData;
                            $tranData = array(
                                'invistr_itemid' => $itemId,
                                'oneivory_order_item_id' => $oneivory_order_item_id,
                                'invistr_batch' => $itemStockDetails['invist_batch'],
                                'invistr_expiry' => $itemStockDetails['invist_expiry'],
                                'invistr_price' => $itemStockDetails['invist_price'],
                                'invistr_quantity' => $enteredQty,
                                'invistr_before_qty' => $itemStockDetails['invist_quantity'],
                                'invistr_after_qty' => $itemStockDetails['invist_quantity'] - $enteredQty,
                                'invistr_clinic_id' => $_POST['facility'],
                                'invistr_tran_type' => 14,
                                'invistr_comment' => '',
                                'invistr_createdby' => $_SESSION['authId'],
                                'invistr_created_date' => date("Y-m-d H:i:s"),
                                'invistr_action' => 'inventory/req_item_issue',
                                'invistr_action_id' => $stockInsertId
                            );
                            $stockInsertTransId = $pdoobject->insert("inv_item_stock_transaction", $tranData);
                            $stockIssueItemTranData[] = $tranData;
                            $updateSql = "update inv_item_stock set invist_quantity=invist_quantity-" . $itemData['iii_quantity'] . " where invist_id ='" . $itemData['iii_stock_id'] . "'";
                            $pdoobject->custom_query_execute($updateSql);
                            
                        }
                    }
                }
                
                
                $oneivoryItemList = $oneivoryItem;
                
            }
            
            
            if ( $magorderid != "") {
                  $comment= ($_POST['issue_note']) ? $_POST['issue_note'] : 'Issue Note';
                  $includeComment= isset($_POST['is_customer_notified']) ? '1' : '0';
                  $commentShip="shipment done";
                  
                  if($GLOBALS['order_invoice_api']){
                       $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                  oneivoryintg::save(array('module_name'=>'sales_order_invoice','endpoint'=>'sales_order_invoice.create','action_id'=> $issueid,'request'=> json_encode(array('orderIncrementId'=>$magorderid,$oneivoryItemList,$comment,'1',$includeComment))));
            }
                  }
                  if($GLOBALS['order_shipment_api']){
                       $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                  oneivoryintg::save(array('module_name'=>'sales_order_shipment','endpoint'=>'order_shipment.create','action_id'=> $issueid,'request'=> json_encode(array('orderIncrementId'=>$magorderid,$oneivoryItemList,$commentShip,'1','0'))));
            }
                  }
            }
            
            
            //}
            if (!empty($allItemIssuedQty)) {
                if ($reqAllRequiredQty == $allItemIssuedQty) {
                    $reqStatus = 2;
                    $pdoobject->custom_query_execute("update inv_stock_requisition set isr_status='" . $reqStatus . "' where isr_id = '" . $_POST['reqid'] . "'");
                    $info_msg = "<div class='alert alert-success alert-dismissable'>Requested Quantity has been issued successfully!</div>";
                } else {
                    $reqStatus = 1;
                    $pdoobject->custom_query_execute("update inv_stock_requisition set isr_status='" . $reqStatus . "' where isr_id = '" . $_POST['reqid'] . "'");
                    $info_msg = "<div class='alert alert-success alert-dismissable'>Requested Quantity has been issued successfully!</div>";
                }
            }
        
        $pdoobject->commit();
    } catch (Exception $e) {
        $pdoobject->rollback();
        $error = $e->getMessage();
        $info_msg ="<div class='alert alert-danger alert-dismissable'>Error in saving Data!</div>";   
    }
        }else{
         $info_msg = "<div class='alert alert-danger alert-dismissable'>Please enter valid data!</div>";   
        }
    
    // Close this window and redisplay the updated list of drugs.
        if ($info_msg)
        $_SESSION['INV_MESSAGE'] = $info_msg;
        // if ($info_msg)
        //     echo " alert('$info_msg');\n";
        echo "<script language='JavaScript'>\n";
        echo "parent.location.reload();\n";
        echo "</script></body></html>\n";
        exit();
    }
    ?>
    <body>
    <div class="showInvIssueError" id='showInvIssueError'></div>
        <?php
        $row = sqlQuery("SELECT istreq.isr_id, istreq.isr_number, istreq.isr_from_clinic, istreq.isr_to_clinic, istreq.isr_isapproved, istreq.isr_status, istreq.isr_isdeleted, isr_createdby, istreq.isr_created_date, frf.name AS fromFacility, tof.name AS toFacility " .
                "FROM inv_stock_requisition AS istreq 
            	INNER JOIN facility AS frf ON frf.id = istreq.isr_from_clinic
            	INNER JOIN facility AS tof ON tof.id = istreq.isr_to_clinic " .
                "WHERE istreq.isr_id = ? AND istreq.isr_to_clinic = ? ORDER BY istreq.isr_id DESC", array($reqid, $facility));
        ?>
        <!-- page -->
        <div class="infopop"><?php xl('Issue Against Requisiton', 'e'); ?></div>
        <div class="popupTableWrp mt-0">
            <!-- <form method='post' id='theform' name='theform' > -->
                <table class="popupTable ui-table" cellspacing='0'>
                    <thead>
                        <tr>
                            <th width='25%'><?php echo xlt('From Facility'); ?></th>
                            <th width='25%'><?php echo xlt('To Facility'); ?></th>
                            <th width='25%'><?php echo xlt('Requisition#'); ?></th>
                            <th width='25%'><?php echo xlt('Req. Status'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span><?php echo $row['fromFacility']; ?></span></td>
                            <td><span><?php echo $row['toFacility']; ?></span></td>
                            <td><span><?php echo $row['isr_number']; ?></span></td>
                            <!-- <td>
                                <?php
                                if ($row['isr_isapproved'] == 2) {
                                    echo 'Rejected';
                                } else if ($row['isr_isapproved'] == 1) {
                                    echo 'Approved';
                                } else {
                                    echo 'Waiting';
                                }
                                ?>
                            </td> -->
                            <td><?php
                                if ($row['isr_status'] == 2) {
                                    echo 'Completed';
                                } else if ($row['isr_status'] == 1) {
                                    echo 'Partially Completed';
                                } else {
                                    echo 'In Progress';
                                }
                                ?></td>
                        </tr>
                    </tbody>  
                </table>
            <!-- </form> -->
        </div>
        <br />
        <form method='post' id='requisitionListForm' name='requisitionListForm' action='req_item_issue.php?reqid=<?php echo $reqid; ?>&facility=<?php echo $facility; ?>&magorderid=<?php echo $magorderid; ?>'>
            <div class="popupTableWrp mt-0">
                <input type="hidden" name="reqid" value="<?php echo $reqid; ?>">
                <input type="hidden" name="facility" value="<?php echo $facility; ?>">
                <input type="hidden" name="tofacility" value="<?php echo $row['isr_from_clinic']; ?>">
                <input type="hidden" name="magorderid" value="<?php echo $magorderid; ?>">

                <div id="requisitionListContainer">
                    <table id='requisitionList' cellspacing='0' class="popupTable ui-table">
                        <thead>
                            <tr>
                                <th width="20%">Item</th>
                                <th width="20%">Item Code</th>
                                <th width="15%">Current Qty.</th>
                                <th width="15%">Req. Qty.</th>
                                <th width="15%">Issued Qty.</th>
                                <th width="15%">Issue Qty.</th> 				
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT irit.iri_itemid, oneivory_order_item_id, irit.iri_reqid, irit.iri_quantity, irit.iri_isdeleted, im.inv_im_name, im.inv_im_code FROM inv_requisition_item AS irit 
                                            INNER JOIN inv_item_master AS im ON im.inv_im_id = irit.iri_itemid  
                                            WHERE irit.iri_isdeleted = '0' AND irit.iri_reqid = ?";
                            $res = $pdoobject->custom_query($query, array($reqid));
                          // print_r($res);die;
                            if ($pdoobject->custom_query($query, array($reqid),1)) {
                                foreach($res as $itemrow) {
                                    ?>
                                 
                                <tr>				
                                    <td>
                                        <input type="hidden" name="reqItems[]" value="<?php echo $itemrow['iri_itemid'] . '-' . $itemrow['oneivory_order_item_id']; ?>">
                                        <?php echo $itemrow['inv_im_name']; ?>&nbsp;					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['inv_im_code']; ?>&nbsp;				
                                    </td>
                                    <td>
                                        &nbsp;			
                                    </td>
                                    <td>
                                        &nbsp;<?php echo $itemrow['iri_quantity']; ?>			
                                    </td>
                                    <td>
                                        &nbsp;
                                        <?php
                                        $issuedQty = getStockIssueQty($reqid, $itemrow['iri_itemid'], 0);
                                        echo $issuedQty;
                                        $rqty=$itemrow['iri_quantity'] - $issuedQty;
                                        ?>
                                        <input type="hidden" id="requiredQty_<?php echo $itemrow['iri_itemid']; ?>" name="requiredQty_<?php echo $itemrow['iri_itemid']; ?>" value="<?php echo $itemrow['iri_quantity'] - $issuedQty; ?>">
                                    </td>
                                    <td>
                                        &nbsp;					
                                    </td>				
                                </tr>
                                <?php
                                $reqItemStocks = getClinicItemStock($itemrow['iri_itemid'], $facility);
                                if (!empty($reqItemStocks)) {
                                    foreach ($reqItemStocks as $reqItemStock) {
                                        ?>
                                        <tr>				
                                            <td>
                                                &nbsp;												
                                            </td>
                                            <td>
                                                <?php 
                                                $exp=$reqItemStock['invist_expiry']=='0000-00-00' ? '' : $reqItemStock['invist_expiry'];
                                                $pre= $reqItemStock['invist_price'] == '0.00' ? '' : $reqItemStock['invist_price'];
                                                echo $reqItemStock['invist_batch'] . ' ' . $exp . ' ' .$pre; ?>&nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                                <?php echo $reqItemStock['invist_quantity']; ?>
                                                <input type="hidden" id="stockQtyAv_<?php echo $reqItemStock['invist_id']; ?>" name="stockQtyAv_<?php echo $reqItemStock['invist_id']; ?>" value="<?php echo $reqItemStock['invist_quantity']; ?>">
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                                <?php echo getStockIssueQty($reqid, $reqItemStock['invist_itemid'], $reqItemStock['invist_id']); ?>
                                            </td>
                                            <td>
                                                &nbsp;
                                                <input type="hidden" id="isqtyr_<?php echo $reqItemStock['invist_itemid']; ?>_<?php echo $reqItemStock['invist_id']; ?>" name="isqtyr_<?php echo $itemrow['iri_itemid']; ?>_<?php echo $reqItemStock['invist_id']; ?>" value="<?php echo $itemrow['iri_quantity'] - $issuedQty; ?>" >
                                                <?php 
                                                $idlist=$idlist.$sp."isqty_".$reqItemStock['invist_itemid']."_".$reqItemStock['invist_id'];
                                                $sp=",";
                                                ?>
                                                <input type="number" class='issueitem' id="isqty_<?php echo $reqItemStock['invist_itemid']; ?>_<?php echo $reqItemStock['invist_id']; ?>" name="isqty_<?php echo $reqItemStock['invist_itemid']; ?>_<?php echo $reqItemStock['invist_id']; ?>" onkeypress="return isNumber(event);" onkeyup="checkInputData(this);" value="0" <?php if(empty($rqty)){ echo 'disabled="disabled"';}?>/>
                                                <input type="hidden" name="itemstockid_<?php echo $reqItemStock['invist_itemid']; ?>[]" value="<?php echo $reqItemStock['invist_id']; ?>">
                                                <span class='error reqissue' id='lowaq'>Requested Quantity is not available!</span>
                                                <span class='error reqissue' id='mrq'>You cannot issue Quantity more than requested!</span>
                                            </td>				
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>				
                                        <td colspan="6" align="center" style="text-align:center;">
                                            No Stock Available													
                                        </td>

                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                <input type='submit' id="form_save" class="pull-right cross-btn dnone" name='form_save' value='<?php echo xla('Save'); ?>' />
            </div>
            <?php if($GLOBALS['order_invoice_api']){ ?>
            <div class="cancelnotes">
                <div class="col-sm-6 txtareanote">
                    <div class="form-group">
                        <label>Notes:</label>
                        <textarea name="issue_note" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset"></textarea>
                    </div>
                </div>
                
                <div class="col-sm-6 checkbxNotified">
                    <div class="form-group ui-block">
                        <label for="is_customer_notified" class="interButt">
                            <?php xl('Notify by Email', 'e'); ?>
                        </label>
                        <input type="checkbox" class="custom" name="is_customer_notified" id="is_customer_notified" value="1">
                        <!-- <input type='checkbox' name='zero_quantity_items' id="zero_quantity_items" value='1' <?php echo (isset($_POST['zero_quantity_items']) && $_POST['zero_quantity_items'] == 1) ? 'checked' : ''; ?> /> -->
                        
                    </div>
                </div>
            </div>
             <?php } ?>
            <input type="hidden" name="validate_ids" id="validate_ids" value="<?php echo $idlist;?>" />
        </form>
        <!-- page -->
        <script language="JavaScript">
            <?php
            if ($alertmsg) {
                echo "alert('" . htmlentities($alertmsg) . "');\n";
            }
            ?>
        </script>
    </body>
</html>