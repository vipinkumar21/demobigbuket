<?php
 // Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 $sanitize_all_escapes  = true;
 $fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 // Check authorization.
 $thisauth = acl_check('inventory', 'im_cat_list');
 if (!$thisauth) die(xlt('Not authorized'));
 /* To refresh and save variables in mail frame  - Arb*/
 if (isset($_POST["mode"])) {
 	if ($_POST["mode"] == "update_mapped_scat") {
 		//sqlStatement("delete from treatment_collaterals where tc_treatment_id = '" . $_POST["id"] . "'");
 		$result=getAllMappedSubCat($_POST["id"]);
 		for($typeCount = 0; $typeCount < count($_POST['subCategory']); $typeCount++){
 			if(checkMappedSubCat($_POST['subCategory'][$typeCount], $result)){
 				idSqlStatement("update inv_cat_subcat_map set " .
 						"icsm_catid = '". $_POST["id"] .
 						"', icsm_scatid = '". $_POST['subCategory'][$typeCount] .
 						"', icsm_deleted = '0' WHERE icsm_catid=".$_POST["id"]." AND icsm_scatid=".$_POST['subCategory'][$typeCount]);
 			}else{
	 			idSqlStatement("insert into inv_cat_subcat_map set " .
	 				"icsm_catid = '". $_POST["id"] .
	 				"', icsm_scatid = '". $_POST['subCategory'][$typeCount] .
	 				"'");
 			}
 		}
 		deleteMappedSubCat($_POST['subCategory'], $_POST["id"]); 		
 		header('Location: category.php');
 		exit;
 	}
 }
 if (isset($_GET["mode"])) {
 	if ($_GET["mode"] == "delete") { 		
 		if(checkCatStatus($_GET["catid"])){
 			sqlStatement("update inv_category set invcat_deleted='1' where invcat_id = '" . $_GET["catid"] . "'");
 			$_SESSION['alertmsg'] = 'Category deleted successfully.';
 			header('Location: category.php');
 			exit;
 		}else {
 			$_SESSION['alertmsg'] = 'You should not able delete selected category.';
 			header('Location: category.php');
 			exit;
 		}
 		
 	}
 	if ($_GET["mode"] == "activate") { 		
 		sqlStatement("update inv_category set invcat_status='1' where invcat_id = '" . $_GET["catid"] . "'"); 		
 		$_SESSION['alertmsg'] = '<span style="color:green;font-weight:bold;">Category activated successfully. </span>';
 		header('Location: category.php');
 		exit;
 	}
 	if ($_GET["mode"] == "deactivate") {
            if(checkCatItem($_GET["catid"])){
 		 sqlStatement("update inv_category set invcat_status='0' where invcat_id = '" . $_GET["catid"] . "'");
 		 $_SESSION['alertmsg'] = '<span style="color:green;font-weight:bold;">Category deactivated successfully.</span>';
 		 header('Location: category.php');
 		 exit;
            } else {
                 $_SESSION['alertmsg'] = '<span style="color:red;font-weight:bold;"> Category can\'t be deactivate there are item associate with it </span>';
                 header('Location: category.php');
                 exit; 
            }
 	}
 }
 // For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
	'invcat_id' => 'cat.id DESC',
	'invcat_name' => 'cat.name',
	'invcat_status'  => 'cat.is_active',  
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invcat_id';
$orderby = $ORDERHASH[$form_orderby];



 // get drugs
 /*echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " . 
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby";*/
 $res = "SELECT cat.id as invcat_id, cat.name as invcat_name, cat.description as invcat_desc, cat.is_active as invcat_status, cat.deleted as invcat_deleted, cat.createdby as invcat_createdby " .
  "FROM invcategories AS cat " . 
  //"WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby";
 $num_rows = sqlNumRows(sqlStatement($res));	// total no. of rows
 $per_page = $GLOBALS['encounter_page_size'];			// Pagination variables processing
 //$per_page = 2;
 $page = $_GET["page"];
 
 if(!$_GET["page"])
 {
 	$page=1;
 }
 
 $prev_page = $page-1;
 $next_page = $page+1;
 
 $page_start = (($per_page*$page)-$per_page);
 if($num_rows<=$per_page)
 {
 	$num_pages =1;
 }
 else if(($num_rows % $per_page)==0)
 {
 	$num_pages =($num_rows/$per_page) ;
 }
 else
 {
 	$num_pages =($num_rows/$per_page)+1;
 	$num_pages = (int)$num_pages;
 }
 $res .= " LIMIT $page_start , $per_page";
 $res = sqlStatement($res);
?>
<html>

<head>
<?php html_header_show();?>

<link rel="stylesheet" href='<?php  echo $css_header ?>' type='text/css'>
<title><?php echo xlt('Inventory Category Master'); ?></title>
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<style>
tr.head   { font-size:10pt; background-color:#cccccc; text-align:center; }
tr.detail { font-size:10pt; }

</style>

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.easydrag.handler.beta2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script language="JavaScript">

// callback from add_edit_drug.php or add_edit_drug_inventory.php:
function refreshme() {
 location.reload();
}

// Process click on drug title.
function dodclick(id) {
 dlgopen('add_edit_category.php?cat=' + id, '_blank', 400, 275);
}

// Process click on drug QOO or lot.
function doiclick(id, lot) {
 dlgopen('add_edit_lot.php?drug=' + id + '&lot=' + lot, '_blank', 600, 475);
}

// Process click on a column header for sorting.
function dosort(orderby) {
 var f = document.forms[0];
 f.form_orderby.value = orderby;
 top.restoreSession();
 f.submit();
 return false;
}
function confirmation(link){
	jConfirm('Are you sure to delete?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}
function confirmation2(link){
	jConfirm('Are you sure to Activate?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}
function confirmation3(link){
	jConfirm('Are you sure to Deactivate?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}


</script>
<script type="text/javascript">

$(document).ready(function(){

    // fancy box
    enable_modals();

    tabbify();

    // special size for
	$(".iframe_medium").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 450,
		'frameWidth' : 660
	});
	
	$(function(){
		// add drag and drop functionality to fancybox
		$("#fancy_outer").easydrag();
	});
});

</script>
</head>

<body class="body_top">
    <div class="panel panel-warning">
     <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3 boldtxt">
                                      <?php  xl('Category','e'); ?>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                  <?php if(acl_check('inventory', 'im_cat_add')){?><a class="iframe btn btn-default btn-sm" href="add_edit_category.php?cat=0"><?php xl('Add Category','e'); ?></a><?php }?>
                                    </div>
                                </div>
                            </div>
<div>
       <table border='0' cellpadding='1' cellspacing='2' width='100%'>
	
	  <?php 
	  	if(!empty($_SESSION['alertmsg'])){
		?>
		<tr >
		<td colspan="2"><?php echo $_SESSION['alertmsg']; $_SESSION['alertmsg']="";?></td>		
	  </tr>
		<?php 
		}
	  ?>
	</table>
    </div>
<div class="panel-body">
	
<form id='report_results' method='post' action='category.php'>

<table border='0' cellpadding='1' cellspacing='0' width='100%' class='table table-bordered'>
<?php if($num_rows > 0){?>	
<thead>
		<tr>
			<!--<th width="2%">
				<?php echo xlt('S.N.'); ?>
			</th>-->
			<th width="23%">
				<a href="#" onclick="return dosort('invcat_name')"
				<?php if ($form_orderby == "invcat_name") echo " style=\"color:#00cc00\""; ?>>
				<?php echo xlt('Name'); ?> </a>
			</th>
			<th width="">
				<?php echo xlt('Description'); ?>
			</th>
			<th width="12%">
				<a href="#" onclick="return dosort('invcat_status')"
				<?php if ($form_orderby == "ndc") echo " style=\"color:#00cc00\""; ?>>
				<?php echo xlt('Status'); ?> </a>
			</th>  
			<th width="15%" class="newtextc">
				<?php echo xlt('Action'); ?>
			</th>  
		</tr>
	</thead>
<?php } ?>
	<tbody>
<?php 
 $lastid = "";
 $encount = 0;
 if(sqlNumRows($res)){
	 while ($row = sqlFetchArray($res)) {  
	   ++$encount;
	   $bgcolor = "#" . (($encount & 1) ?  "f7d9ca" : "fdede5");
	   $lastid = $row['invcat_id'];
	   if($row['invcat_deleted'] == 1){
			echo " <tr class='detail strikeThrough' bgcolor='$bgcolor'>\n";
		}else {
	   		echo " <tr class='detail' bgcolor='$bgcolor'>\n";
	   }
	   //echo "  <td>" .$encount. "</td>\n";
	   echo "  <td>" .	    
	    text($row['invcat_name']) . "</td>\n";
	   echo "  <td>" . text($row['invcat_desc']) . "</td>\n";
	   echo "  <td>" . ($row['invcat_status'] ? xlt('Active') : xlt('Not Active')) . "</td>\n";
	   echo "  <td class='newtextc newvm'>";
 	   if($row['invcat_deleted'] == 0){
 	   		if(acl_check('inventory', 'im_cat_edit')){ echo "<a href='add_edit_category.php?cat=".attr($lastid)."' class='iconanchor iframe' title='Edit' ><span class='glyphicon glyphicon-pencil'></span></a> &nbsp; ";}
			if(acl_check('inventory', 'im_cat_del')){ echo "<a class='iconanchor' href='#' onclick='return confirmation(\"category.php?mode=delete&catid=".attr($lastid)."\"); top.restoreSession()' title='Delete'><span class=' glyphicon glyphicon-trash'></span></a> &nbsp; "; }
 	   		if($row['invcat_status'] == 0){
 	   			//if(acl_check('inventory', 'im_cat_edit')){ echo "<a class='iconanchor' href='category.php?mode=activate&catid=".attr($lastid)."' onclick='return confirm(\"Are you sure to activate?\"); top.restoreSession()' title='Activate'><span class=' glyphicon glyphicon-ok'></span></a>";}
 	   		if(acl_check('inventory', 'im_cat_edit')){ echo "<a class='iconanchor' href='#' onclick='return confirmation2(\"category.php?mode=activate&catid=".attr($lastid)."\"); top.restoreSession()' title='Activate'><span class=' newactivate'></span></a>";}
 	   		
 	   		}else {
 	   			//if(acl_check('inventory', 'im_cat_edit')){ echo "<a class='iconanchor' href='category.php?mode=deactivate&catid=".attr($lastid)."' onclick='return confirm(\"Are you sure to deactivate?\"); top.restoreSession()' title='Deactivate'><span class=' glyphicon glyphicon glyphicon-remove'></span></a>";}
 	   			if(acl_check('inventory', 'im_cat_edit')){ echo "<a class='iconanchor' href='#' onclick='return confirmation3(\"category.php?mode=deactivate&catid=".attr($lastid)."\"); top.restoreSession()' title='Dectivate'><span class=' newdeactivate'></span></a>";}
 	   			if(acl_check('inventory', 'im_cat_map')){ echo " &nbsp; <a class='iconanchor iframe_medium' href='map_subcategory.php?id=".attr($lastid)."' onclick='top.restoreSession()' title='Sub Category'><span class=' glyphicon glyphicon-align-left'></span></a>";}
 	   		}
 	   		
 	   }
		echo "</td>\n";	     
	  	echo " </tr>\n";
	 } // end while
 }else{
	?>
	<tr>
		<td colspan="5" style="text-align:center; font-size:10pt;">No Category Found</td>
	</tr>
	<?php 
 } // end If
?>
</tbody>
</table>
<input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />

</form>
<?php // Pagination Displaying Section?>
<table>
	<tbody>
		<tr>
			<td class="text">
				<?php if($num_rows > 0) {?>
				<span class='paging'>
				<span class="pagingInfo">Total Records: <?php echo $num_rows;?></span> Page :
				<?php
					if($prev_page) {
						echo " <span class='prev' style='margin:0 4px 0 5px'><a href='$_SERVER[SCRIPT_NAME]?page=$prev_page&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'><< Prev</a></span> ";
					}

					if($num_rows > 0){
						for($i=1; $i<=$num_pages; $i++){
							if($i != $page)
							{
								echo "<span><a href='$_SERVER[SCRIPT_NAME]?page=$i&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'>$i</a></span>";
							}
							else
							{
								echo "<span class='current'>$i</span>";
							}
						}
					}
					if($page!=$num_pages){
						echo "<span class='next'><a href ='$_SERVER[SCRIPT_NAME]?page=$next_page&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'>Next >></a></span>";
					} 
				?>
				</span>
				<?php } ?>
			</td>
		</tr>
	</tbody>
</table>
<!-- stuff for the popup calendar -->
</div>    </div>
</body>
</html>
