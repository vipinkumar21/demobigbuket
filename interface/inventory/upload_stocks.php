<?php
 /**
  * @author Brajraj Agrawal <brajraj.agarwal@instantsys.com>
  * @Created : 31-May-2016
  * @Description : This file is used to process the stock insertion and updation through a CSV file based on a perticular facility.
  */
require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");


if($_GET['error'])
{
    echo "<script>alert('Please select the facility.');"
    . "window.location.href='upload_stocks.php';</script>";
}
if ($_POST['form_csv_sample'] && $_POST['form_csv_sample']=='sample') {	// Sample CSV Format
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Disposition: attachment; filename=stock.csv");
	header("Content-Description: File Transfer");
	echo xl('Item Name') . ',';
	echo xl('Code') . ',';
	echo xl('Batch') . ',';
	echo xl('Expiry') . ',';
	echo xl('Price') . ',';	
	echo '"' . xl('Qty') . '"' . "\n";
	echo '"Absorbent Paper Points No.15-Diadent	",';
	echo '"DC00001",';
	echo '"10913",';
	echo '"2017-08-31",';
	echo '"189.50",';	
	echo '"15",'."\n";

	echo '"NA-Barbed Broaches No. 6-Dentsply",';
	echo '"DC00019",';
	echo '"10453",';
	echo '"2017-02-15",';
	echo '"259.25",';	
	echo '"45",'."\n";
	exit;
}
?>
<html>

<head>

<?php html_header_show();?>

<title><?php xl('Stock CSV Upload','e'); ?></title>

<!-- CSS Libraries --> 
<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

<!-- Custom CSS --> 
<style type="text/css">
    /* specifically include & exclude from printing */
    @media print {
        #report_parameters {
            visibility: hidden;
            display: none;
        }
        #report_parameters_daterange {
            visibility: visible;
            display: inline;
            margin-bottom: 10px;
        }
        #report_results table {
           margin-top: 0px;
        }
    }

    /* specifically exclude some from the screen */
    @media screen {
        #report_parameters_daterange {
            visibility: hidden;
            display: none;
        }
        #report_results {
            width: 100%;
        }
    }
</style>

<!-- JS Libraries --> 
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>

<!-- Custom JS --> 
<script type="text/javascript">

        function facilityChange(){
            if(document.forms['stockInsert'].facility.value!=0){
                document.getElementById('error1').style.display = 'none';   
            }
        }

        function uploadChange(){
            var res = (document.forms['stockInsert'].stock_file.value).split(".");
            if(res[res.length - 1]=="csv"){
                 document.getElementById('error2').style.display = 'none'; 
            }
        }

       function submitStockCsv() {

            if(document.forms['stockInsert'].facility.value==0){
                document.getElementById('error1').style.display = 'block';   
                document.forms['stockInsert'].facility.focus();
                return false;
            }
            else{
                document.getElementById('error1').style.display = 'none';   
            }
            if (document.forms['stockInsert'].stock_file.value.length<=0){
                document.getElementById('error2').style.display = 'block';   
                document.forms['stockInsert'].stock_file.focus();
                return false;
             }

            var res = (document.forms['stockInsert'].stock_file.value).split(".");
            if(res[res.length - 1]!="csv"){
                return false;
            }


            document.forms['stockInsert'].submit();
            return true;

        }

       
    // });


</script>

</head>

<body class="body_top importStock">

<div class="panel panel-warning">

    <div class="panel-heading boldtxt">
        <?php xl('Import','e'); ?> - <?php xl('Stock CSV','e'); ?>
    </div>

    <div class="panel-body">

        <div class="row">

            <form name='stockInsert' id='stockInsert' method='post' enctype='multipart/form-data' action='upload_csv_documents_process.php' onsubmit="return submitStockCsv()">
                
                <input type='hidden' name='mode' id='mode' value='import_stock'/>

                <div class="row">

                    <!-- first column --> 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class='newfontnormal'> <?php xl('Facility', 'e'); ?>:</label>
                            <?php
                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                            usersFacilityDropdown('facility', 'form-control input-sm', 'facility', $facility, $_SESSION['authId'], $userFacilityRestrict, $pdoobject,1,'onchange="facilityChange()"');
                            ?>
                            <p class="newError" id="error1" style="display:none;">Please select a facality</p>
                        </div>
                    </div>

                    <!-- second column --> 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label newfontnormal"><?php xl('Stock', 'e'); ?>: </label>
                            <input id="fileUploader" type="file" name="stock_file" style="margin-top:6px;" onchange = "uploadChange()">
                            <p class="newError" id="error2" style="display:none;">Please select a valid file</p>
                        </div>
                    </div>

                    <!-- third column --> 
                    <div class="col-sm-4"  id="mergePatientDataButton">
                        <div class="form-group">
                            <label class="control-label newfontnormal">Upload:</label>
                            <button class="btn btn-warning btn-sm" style="display:block;" type="submit">Upload Stock</button> 
                            <!-- <input type="submit" value="Upload Stock"> -->
                        </div> 
                    </div>

                </div>

            </form>

        </div>

        <!-- end of parameters -->
        <form name='stockcsv' id='stockcsv' method='post' enctype='multipart/form-data' action='upload_stocks.php'>
            <input type='hidden' name='form_csv_sample' id='form_csv_sample' value=''/>
            <div class="row">   
                <div  class="col-xs-12"><b>Note:</b> Please first select Facility and click Submit.Then browse CSV and upload it.</div>
                <div class="col-xs-12">
                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csv_sample").attr("value","sample");$("#stockcsv").submit();'>
                        <span><?php xl('Sample CSV', 'e'); ?></span>
                    </a>	
                </div>
            </div>
        </form>

        <div class="serverMessage"> 
            <?php
                if (!strpos($_SERVER['HTTP_REFERER'], "upload_csv_documents_process")) { 
                    // Reset CSV uploaded msg display
                    echo $_SESSION['msg'];
                    $_SESSION['msg'] = '';
                }
            ?>
        </div> 
        
    </div>


</div>

</body> 

</html> 