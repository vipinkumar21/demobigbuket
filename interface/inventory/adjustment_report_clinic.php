<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Fetch the Provide based on facility (Clinic ID)
function restrict_user_facility($facilList) {
	$sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	$sql .= " WHERE ";
	if (isset($facilList) && $facilList !== '') {
		$sql .= "uf.facility_id = $facilList ";
	} else {
		$fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = " . $userid . " AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
		$fRes = sqlStatement($fsql, null, $GLOBALS['adodb']['dbreport']);
		$facilityIds = '';
		while ($fRow = sqlFetchArray($fRes)) {
			if (!empty($facilityIds)) {
				$facilityIds .= ', ' . $fRow['facility_id'];
			} else {
				$facilityIds = $fRow['facility_id'];
			}
		}
		$sql .= "uf.facility_id IN (" . $facilityIds . ") ";
	}
	$sql .= "AND gag.id IN (12, 13, 18) ";
	return $sql .= " GROUP BY u.id DESC";
}

function non_restrict_user_facility($facilList) {
	$sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename ";
	$sql .= "FROM users AS u  ";
	$sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
	return $sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";
}

// Check authorization.
$thisauth = acl_check('inventory', 'icr_adj');
//if (!$thisauth) die(xlt('Not authorized'));

// Prepare a string for CSV export.
function qescape($str) {
	$str = str_replace('\\', '\\\\', $str);
	return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$form_facility = $_POST['form_facility'];
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
$provider = $_POST['form_provider'];
$item = $_POST['form_item'];
$sellable = $_POST['form_sellable'];
$catid = $_POST['catid'];
$subcatid = $_POST['subcatid'];

$searchParam = '';

$show_available_times = false;
$ORDERHASH = array(
	'invistr_id' => 'invistr_id DESC',
	'inv_im_name' => 'inv_im_name',
	'inv_im_status' => 'inv_im_status',
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invistr_id';
$orderby = $ORDERHASH[$form_orderby];

// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvAdjustment') {
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Disposition: attachment; filename=adjustment_report.csv");
	header("Content-Description: File Transfer");
} else {
	?>
	<html>
		<head>
			<?php html_header_show(); ?>

			<link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
			<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
			<title><?php xl('Stock Leadger', 'e'); ?></title>
			<script type="text/javascript" src="../../library/overlib_mini.js"></script>
			<script type="text/javascript" src="../../library/textformat.js"></script>
			<script type="text/javascript" src="../../library/dialog.js"></script>
			<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

			<script type="text/javascript">
				var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

				function dosort(orderby) {
					var f = document.forms[0];
					f.form_orderby.value = orderby;
					f.submit();
					return false;
				}

				function oldEvt(eventid) {
					dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
				}

				function refreshme() {
					document.forms[0].submit();
				}


				$(document).ready(function () {
					var facilityID = $("#form_facility").val(); 
					if(facilityID > 0 ){
					var providerID = '<?php echo $_REQUEST['form_provider'] ?>' ;
					$.ajax({
							url: "get_facility_provider.php?facilityId=" + facilityID,
							success: function (result) {
								$("#form_provider").html(result);
								 $('#form_provider').val(providerID);
							}
						});

					}
					//Get Provider based on Facility or Clinic
					$("#form_facility").change(function () {
						var facilityID = $("#form_facility").val();
						$.ajax({
							url: "get_facility_provider.php?facilityId=" + facilityID,
							success: function (result) {
								$("#form_provider").html(result);
							}
						});
					});

		 //Get Sub-Category based on Category
		 var catid = $("#catid").val(); 
                    if(catid > 0 ){
                       var subCat = '<?php echo $_REQUEST['subcatid'] ?>';                  
                        $.ajax({
                                type: 'GET',
                                url: "get_item_subcat.php?catid=" + catid +"&subCat=" + subCat,
                                success: function (data) {
                                    $("#itemSubCatField").html(data); 
                                    $('#itemSubCatField').val(subCat);
                                }
                            });

                    }  
                    //Get Sub-Category based on Category
                    $("#catid").change(function () {
                        var catid = $("#catid").val();
                        var subCat = '<?php echo $_REQUEST['subcatid'] ?>';
                        $.ajax({
                            type: 'GET',
                            url: "get_item_subcat.php?catid=" + catid +"&subCat=" + subCat,
                            success: function (data) {
                                $("#itemSubCatField").html(data);
                                $('#itemSubCatField').val(subCat);
                            }
                        });
                    });
				});
			</script>

			<style type="text/css">
				/* specifically include & exclude from printing */
				@media print {
					#report_parameters {
						visibility: hidden;
						display: none;
					}
					#report_parameters_daterange {
						visibility: visible;
						display: inline;
					}
					#report_results table {
						margin-top: 0px;
					}
				}

				/* specifically exclude some from the screen */
				@media screen {
					#report_parameters_daterange {
						visibility: hidden;
						display: none;
					}
				}
			</style>
		</head>

		<body class="body_top">
		<div class="panel panel-warning">
			<!-- Required for the popup date selectors -->
			<div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
			<div class="panel-heading">
				<div class="row">
                    <div class="col-xs-3 boldtxt"><?php xl('Report', 'e'); ?> - <?php xl('Adjustment', 'e'); ?></div>
                    <div class="col-xs-9 text-right"></div>
                </div>
            </div>
			<div class="panel-body">
			<div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
			</div>

			<form method='post' name='theform' id='theform' action='adjustment_report_clinic.php'>
				<div id="report_parameters" class='searchReport'>
					<input type='hidden' name='form_refresh' id='form_refresh' value=''/>
					<input type='hidden' name='form_csvexport' id='form_csvexport' value='viewAdjustment'/>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<?php xl('Facility', 'e'); ?>:<br>
								<?php //dropdown_facility(strip_escape_custom($facility), 'form_facility'); ?>
				
								<select name="form_facility" id="form_facility" class="form-control input-sm" >	
									<?php 
								  if(empty($row['invist_clinic_id'])){
								  ?>
								  <option value='0' selected="selected">Select Facility</option>
								  <?php
								  }
								  $countUserFacilities = 0;
								  if (!$GLOBALS['restrict_user_facility']) {
									$qsql = sqlStatement("
										select id, name, color
										from facility
										where service_location != 0
									");
								  } else {
									  $qsql = sqlStatement("
										  select uf.facility_id as id, f.name, f.color
										  from users_facility uf
										  left join facility f on (uf.facility_id = f.id)
										  where uf.tablename='users' 
										  and uf.table_id = ? 
										", array($_SESSION['authId']) );
								  }      
								  while ($facrow = sqlFetchArray($qsql)) {       
									$selected = ( $facrow['id'] == $form_facility ) ? 'selected="selected"' : '' ;
									echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";       
									$countUserFacilities++;
									/************************************************************/
								  }      
								  ?>      
								  </select>
							</div>
						</div>
						<div class="col-md-3">
							<?php xl('Provider', 'e'); ?>:
							<?php
								if (empty($facility)) {
									$query = "SELECT id, lname, fname FROM users WHERE " . "authorized = 1 $provider_facility_filter ORDER BY lname, fname";
									$ures = sqlStatement($query);
									echo "   <select name='form_provider' id='form_provider' class='form-control input-sm'>\n";
									echo "    <option value=''>-- " . xl('All') . " --\n";
									while ($urow = sqlFetchArray($ures)) {
										$provid = $urow['id'];
										echo "    <option value='$provid'";
										if ($provid == $provider)
											echo " selected";
										echo ">" . $urow['fname'] . " " . $urow['lname'] . "\n";
									}
									echo "   </select>\n";
								} else {

									if ($GLOBALS['restrict_user_facility']) {
										$sql = restrict_user_facility($form_facility);
										$ures = sqlStatement($sql, null, $GLOBALS['adodb']['dbreport']);
									} else {
										$sql = non_restrict_user_facility($form_facility);
										$ures = sqlStatement($sql, null, $GLOBALS['adodb']['dbreport']);
									}

									$returnData = '';
									// default to the currently logged-in user
									$defaultProvider = $_SESSION['authUserID'];
									$returnData.= "<option value='' selected='selected'>Select Provider</option>";
									while ($urow = sqlFetchArray($ures)) {
										$returnData.= "<option value='" . attr($urow['id']) . "'";
										if ($urow['id'] == $provider)
											$returnData.= " selected";
										$returnData.= ">" . text($urow['completename']);
										$returnData.= "</option>";
									}
									echo "   <select name='form_provider' id='form_provider'>\n";
									echo $returnData;
									echo "   </select>\n";
								}
								?>
						</div>
						<div class="col-md-3">
							<?php xl('Category', 'e'); ?>:
							<select name='catid' id='catid' class="form-control input-sm">
								<?php
								if (empty($row['inv_im_catId'])) {
									?>
									<option value='0' selected="selected">All Category</option>
									<?php
								}
								?>
								<?php
								$catSql = "SELECT id as invcat_id, name as invcat_name FROM invcategories WHERE is_active='1' AND deleted='0'";
								$cres = sqlStatement($catSql, null, $GLOBALS['adodb']['dbreport']);
								if (sqlNumRows($cres)) {
									while ($crow = sqlFetchArray($cres)) {
										?>
										<option value='<?php echo $crow['invcat_id'] ?>' <?php
										if ($_POST['catid'] == $crow['invcat_id']) {
											echo 'selected="selected"';
										}
										?>><?php echo $crow['invcat_name'] ?></option>
												<?php
											}
										}
										?>
							</select>
						</div>
						<div class="col-md-3">
							<?php xl('Sub-Category', 'e'); ?>:
							<select name='subcatid' id="itemSubCatField" class="form-control input-sm">
									<?php
									if (!empty($row['inv_im_catId'])) {
										if (empty($row['inv_im_subcatId'])) {
											?>
											<option value='0' selected="selected">All Sub Category</option>  		
											<?php
										}
										$scatSql = "SELECT id as invsubcat_id, name as invsubcat_name FROM invcategories WHERE is_active='1' AND deleted='0' AND id = " . $row['inv_im_catId'];
										$scres = sqlStatement($scatSql, null, $GLOBALS['adodb']['dbreport']);
										if (sqlNumRows($scres)) {
											while ($scrow = sqlFetchArray($scres)) {
												?>
												<option value='<?php echo $scrow['invsubcat_id'] ?>' <?php
														if ($_POST['subcatid'] == $scrow['invsubcat_id']) {
															echo 'selected="selected"';
														}
														?>><?php echo $scres['invsubcat_name'] ?></option>
														<?php
													}
												}
											} else { ?>
												<option value='0' selected="selected">All Sub Category</option>
											<?php } ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<?php xl('Item', 'e'); ?>:
								<input type='text' name='form_item' class="form-control input-sm" id="form_item" size='20' value='<?php echo ($item !='')?$item:'' ;?>' title=''>
							</div>
						</div>
						<div class="col-md-3">
							<?php xl('From', 'e'); ?>:<br>
							<input type='text' name='form_from_date' id="form_from_date"
									   size='10' value='<?php echo $form_date ?>'
									   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
									   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
									   align='absbottom' width='24' height='22' id='img_from_date'
									   border='0' alt='[?]' style='cursor: pointer'
									   title='<?php xl('Click here to choose a date', 'e'); ?>'>
						</div>
						<div class="col-md-3">
							<?php xl('To', 'e'); ?>:<br>
							<input type='text' name='form_to_date' id="form_to_date"
									   size='10' value='<?php echo $form_to_date ?>'
									   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
									   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
									   align='absbottom' width='24' height='22' id='img_to_date'
									   border='0' alt='[?]' style='cursor: pointer'
									   title='<?php xl('Click here to choose a date', 'e'); ?>'>
						</div>
						<?php if (0) { ?>	
							<div class="col-md-3" style="margin-top: 20px;">
								<?php xl('Price', 'e'); ?>:
								<input type='checkbox' name='form_price' id="form_price" value='1' <?php echo (isset($_POST['form_price']) && $_POST['form_price'] == 1) ? 'checked' : ''; ?> >
							</div>
						<?php } ?>	
						<div class="col-md-3" style="margin-top: 20px;">
							<?php xl('Saleable', 'e'); ?>:
							<input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_POST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewAdjustment"); $("#theform").submit();'><span> <?php xl('Submit', 'e'); ?> </span> </a> 
							<?php if (1) { ?>
								<a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvAdjustment"); $("#theform").submit();'><span><?php xl('Export to CSV', 'e'); ?></span></a>
							<?php } ?>
						</div>
					</div>
				</div>
				<br>
							
				<!-- end of search parameters --> <?php
			} // end not form_csvexport
			if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
				if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvAdjustment') {
					// CSV headers:
					echo '"' . xl('S. No.') . '",';
					echo '"' . xl('Name') . '",';
					echo '"' . xl('Batch Number') . '",';
					echo '"' . xl('Expiry') . '",';

					if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
						echo '"' . xl('Price') . '",';
					}
					echo '"' . xl('Quantity') . '",';
					echo '"' . xl('Running Qty') . '",';
					echo '"' . xl('Clinic Name') . '",';
					echo '"' . xl('Transaction Date') . '",';
					echo '"' . xl('Created By') . '"' . "\n";
				} else {
					?>
					<div id="report_results">
						<table class="table table-bordered table-striped">
							<tr class='head'>
							<thead>
							<th><?php xl('S.No.', 'e'); ?></th>
							<th><?php xl('Name', 'e'); ?></th>
							<th><?php xl('Batch Number', 'e'); ?></th>
							<th><?php xl('Expiry', 'e'); ?></th>
		<?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
								<th><?php xl('Price', 'e'); ?></th>
		<?php } ?>
							<th><?php xl('Quantity', 'e'); ?></th>
							<th><?php xl('Running Qty', 'e'); ?></th>
							<th><?php xl('Clinic Name', 'e'); ?></th>
							<th><?php xl('Transaction Date', 'e'); ?></th>
							<th><?php xl('Created By', 'e'); ?></th>

							</thead>
							<tbody>
								<!-- added for better print-ability -->
								<?php
							}

							$dateMysqlFormat = getDateDisplayFormat(1);

							if ($form_facility) {
								//for individual facility
								if ($form_date) {
									//if from date selected
									if ($form_to_date) {
										// if $form_date && $form_to_date
										$toDate = date_create(date($form_to_date));
										$fromDate = date_create($form_date);
										$diff = date_diff($fromDate, $toDate);
										$days_between_from_to = $diff->days;


										if ($days_between_from_to <= $form_facility_time_range) {

											if ($where) {
												$where .= " AND ";
											}
											$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";

											if ($where) {
												$where .= " AND ";
											}
											$where .= " tran.invistr_clinic_id = '$form_facility'";
										} else {
											if($days_between_from_to<=$form_facility_time_range_valid){
											##########Cron Request####################section only for all FACILITY#########################
											// following value will be change according to report
											$rcsl_name_type = "adjustment_report"; // changable
											$rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "facility"=>$form_facility))); // changable
											$rcsl_report_description = "Adjustment report from $form_date to $form_to_date"; // changable
											//allFacilityReports() defined with globals.php
											$msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
												if ($msgForReportLog) {
													$msgForReportLog = $msgForReportLog;
												} else {
													$msgForReportLog = "$form_facility_time_range_errorMsg";
												}
											##########Cron Request####################section only for all FACILITY#########################
											}
											else{
											$msgForReportLog= $form_facility_time_range_valid_errorMsg;  
											}
										}
									} else {
										//only from date: no TO date
										$form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
										$toDate = date_create(date($form_to_date));
										$fromDate = date_create($form_date);
										$diff = date_diff($fromDate, $toDate);
										$days_between_from_to = $diff->days;

										if ($days_between_from_to <= $form_facility_time_range) {
											if ($where) {
												$where .= " AND ";
											}
											$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
											if ($where) {
												$where .= " AND ";
											}
											$where .= " tran.invistr_clinic_id = '$form_facility'";
										} else {
											if($days_between_from_to<=$form_facility_time_range_valid){
											##########Cron Request####################section only for all FACILITY#########################
											// following value will be change according to report
											$rcsl_name_type = "adjustment_report"; // changable
											$rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "facility"=>$form_facility))); // changable
											$rcsl_report_description = "Adjustment report from $form_date to $form_to_date"; // changable
											//allFacilityReports() defined with globals.php
											$msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
												if ($msgForReportLog) {
													$msgForReportLog = $msgForReportLog;
												} else {
													$msgForReportLog = "$form_facility_time_range_errorMsg";
												}
											##########Cron Request####################section only for all FACILITY#########################
											}
											else{
											$msgForReportLog= $form_facility_time_range_valid_errorMsg;  
											}
										}
									}
								} else {
									// if from date not selected
									if ($form_to_date) {
										$form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
										$toDate = date_create(date($form_to_date));
										$fromDate = date_create($form_date);
										$diff = date_diff($fromDate, $toDate);
										$days_between_from_to = $diff->days;

										if ($days_between_from_to <= $form_facility_time_range) {
											if ($where) {
												$where .= " AND ";
											}
											$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
											if ($where) {
												$where .= " AND ";
											}
											$where .= " tran.invistr_clinic_id = '$form_facility'";
										} else {
											if($days_between_from_to<=$form_facility_time_range_valid){
											##########Cron Request####################section only for all FACILITY#########################
											// following value will be change according to report
											$rcsl_name_type = "adjustment_report"; // changable
											$rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "facility"=>$form_facility))); // changable
											$rcsl_report_description = "Adjustment report from $form_date to $form_to_date"; // changable
											//allFacilityReports() defined with globals.php
											$msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
												if ($msgForReportLog) {
													$msgForReportLog = $msgForReportLog;
												} else {
													$msgForReportLog = "$form_facility_time_range_errorMsg";
												}
											##########Cron Request####################section only for all FACILITY#########################
											}
											else{
											$msgForReportLog= $form_facility_time_range_valid_errorMsg;  
											}
										}
									} else {
										if ($where) {
											$where .= " AND ";
										}
										$form_to_date = date("Y-m-d");
										$form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
										$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
										if ($where) {
											$where .= " AND ";
										}
										$where .= " tran.invistr_clinic_id = '$form_facility'";
									}
								}
								
								 $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param
								
							} else {
								//for all facility
								$facilityIdList = userFacilitys();
								if ($where) {
									$where .= " AND ";
								}
								$where .= " tran.invistr_clinic_id IN (".$facilityIdList.")";
								if ($form_date) {
									//if from date selected
									if ($form_to_date) {
										// if $form_date && $form_to_date
										$toDate = date_create(date($form_to_date));
										$fromDate = date_create($form_date);
										$diff = date_diff($fromDate, $toDate);
										$days_between_from_to = $diff->days;

										if ($days_between_from_to <= $form_facility_all_time_range) {

											if ($where) {
												$where .= " AND ";
											}
											$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
										} else {
											if($days_between_from_to<=$form_facility_all_time_range_valid){
											##########Cron Request####################section only for all FACILITY#########################
											// following value will be change according to report
											$rcsl_name_type = "adjustment_report"; // changable
											$rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "facility"=>$facilityIdList))); // changable
											$rcsl_report_description = "Adjustment report from $form_date to $form_to_date"; // changable
											//allFacilityReports() defined with globals.php
											$msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
												if ($msgForReportLog) {
													$msgForReportLog = $msgForReportLog;
												} else {
													$msgForReportLog = "$form_facility_all_time_range_errorMsg";
												}
											##########Cron Request####################section only for all FACILITY#########################
											}
											else{
											$msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
											}
										}
									} else {
										//only from date: no TO date
										$form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
										$toDate = date_create(date($form_to_date));
										$fromDate = date_create($form_date);
										$diff = date_diff($fromDate, $toDate);
										$days_between_from_to = $diff->days;

										if ($days_between_from_to <= $form_facility_all_time_range) {
											if ($where) {
												$where .= " AND ";
											}
											$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
										} else {
											if($days_between_from_to<=$form_facility_all_time_range_valid){
											##########Cron Request####################section only for all FACILITY#########################
											// following value will be change according to report
											$rcsl_name_type = "adjustment_report"; // changable
											$rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "facility"=>$facilityIdList))); // changable
											$rcsl_report_description = "Adjustment report from $form_date to $form_to_date"; // changable
											//allFacilityReports() defined with globals.php
											$msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
												if ($msgForReportLog) {
													$msgForReportLog = $msgForReportLog;
												} else {
													$msgForReportLog = "$form_facility_all_time_range_errorMsg";
												}
											##########Cron Request####################section only for all FACILITY#########################
											}
											else{
											$msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
											}
										}
									}
								} else {
									// if from date not selected
									if ($form_to_date) {
										$form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
										$toDate = date_create(date($form_to_date));
										$fromDate = date_create($form_date);
										$diff = date_diff($fromDate, $toDate);
										$days_between_from_to = $diff->days;

										if ($days_between_from_to <= $form_facility_all_time_range) {
											if ($where) {
												$where .= " AND ";
											}
											$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
										} else {
											if($days_between_from_to<=$form_facility_all_time_range_valid){
											##########Cron Request####################section only for all FACILITY#########################
											// following value will be change according to report
											$rcsl_name_type = "adjustment_report"; // changable
											$rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "facility"=>$facilityIdList))); // changable
											$rcsl_report_description = "Adjustment report from $form_date to $form_to_date"; // changable
											//allFacilityReports() defined with globals.php
											$msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
												if ($msgForReportLog) {
													$msgForReportLog = $msgForReportLog;
												} else {
													$msgForReportLog = "$form_facility_all_time_range_errorMsg";
												}
											##########Cron Request####################section only for all FACILITY#########################
											}
											else{
											$msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
											}
										}
									} else {
										if ($where) {
											$where .= " AND ";
										}
										$form_to_date = date("Y-m-d");
										$form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
										$where .= " DATE(tran.invistr_created_date) >= '$form_date' AND DATE(tran.invistr_created_date) <= '$form_to_date'";
									}
								}
								
								$searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param
							}


							if (isset($provider) && $provider != '') {
								if ($where) {
									$where .= " AND ";
								}

								$where .= "  tran.invistr_createdby = '" . $provider . "'";
								$searchParam .= ' Provider = '. $provider.' | ';
							} else {
								$searchParam .= ' Provider = All | ';
							}

							if (isset($item) && $item != '') {
								if ($where) {
									$where .= " AND ";
								}
								$where .= "  im.inv_im_name LIKE '" . $item . "%'";
								$searchParam .= ' Item = '. $item.' | ';
							}
							if (isset($catid) && $catid != 0) {
								if ($where) {
									$where .= " AND ";
								}
								$where .= "  im.inv_im_catId = '" . $catid . "'";
								$searchParam .= ' Category = '. $catid.' | ';
							} else {
								$searchParam .= ' Category = All | ';
							}
							
							if (isset($subcatid) && $subcatid != 0) {
								if ($where) {
									$where .= " AND ";
								}
								$where .= "  im.inv_im_subcatId = '" . $subcatid . "'";
								$searchParam .= ' Sub-Category = '. $subcatid .' | ';
							} else {
								$searchParam .= ' Sub-Category = All | ';
							}

							if (isset($sellable) && $sellable != 0) {
								if ($where) {
									$where .= " AND ";
								}
								$where .= "  im.inv_im_sale = '" . $sellable . "'";
								$searchParam .= ' Saleable Items = Yes | ';
							} else {
								 $searchParam .= ' Saleable Items = No | ';
							}

							if (!$where) {
								$where = "1 = 1";
							}

							$query = "SELECT adj.ina_id, adj.ina_item_id, adj.ina_stock_id, adj.ina_quantity, adj.ina_type, adj.ina_facility_id, adjrel.iatr_id, tran.invistr_id, tran.invistr_before_qty, tran.invistr_after_qty, tran.invistr_comment,
DATE_FORMAT(tran.invistr_expiry, '%d-%m-%y') AS expiryformateddate, DATE_FORMAT(tran.invistr_created_date , '%d-%m-%y') AS batchformateddate, u.fname, u.lname, u.nickname, im.inv_im_name, tran.invistr_batch, tran.invistr_price, tran.invistr_quantity, f.name AS clinicname  
FROM inv_item_stock_transaction AS tran  
INNER JOIN inv_adj_trans_rel AS adjrel ON tran.invistr_id = adjrel.iatr_trans_id 
INNER JOIN inv_adjustments AS adj ON adjrel.iatr_adj_id = adj.ina_id 
INNER JOIN facility AS f ON f.id = tran.invistr_clinic_id
INNER JOIN inv_item_master AS im ON im.inv_im_id = tran.invistr_itemid
LEFT JOIN inv_transaction_type AS invtrt ON tran.invistr_tran_type = invtrt.invtrt_id
LEFT JOIN `users` AS u ON u.id = im.inv_im_createdby 
WHERE " . $where . " AND tran.invistr_isdeleted = '0' AND tran.invistr_tran_type = 5  ORDER BY $orderby";

							if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvAdjustment') {
								$event = "Report Adjustment Export";
							} else {
								$event = "Report Adjustment View";
							}
							

							if ($msgForReportLog) {
								debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
							} else {
								$auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
								$res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);

								$lastid = "";
								$srCount = 1;
								if (sqlNumRows($res)) {
									while ($row = sqlFetchArray($res)) {
										if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvAdjustment') {
											echo '"' . qescape($srCount) . '",';
											echo '"' . qescape($row['inv_im_name']) . '",';
											echo '"' . qescape($row['invistr_batch']) . '",';
											echo '"' . qescape($row['expiryformateddate']) . '",';
											if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
												echo '"' . qescape($row['invistr_price']) . '",';
											}
											echo '"' . qescape($row['invistr_quantity']) . '",';
											echo '"' . qescape($row['invistr_after_qty']) . '",';
											echo '"' . qescape($row['clinicname']) . '",';
											echo '"' . qescape($row['batchformateddate']) . '",';
											echo '"' . qescape($row['nickname']) . '"' . "\n";
										} else {
											?>
											<tr bgcolor='<?php echo $bgcolor ?>'>
												<td class="detail">&nbsp;<?php echo $srCount; ?></td>
												<td class="detail"><?php echo $row['inv_im_name']; ?></td>
												<td class="detail"><?php echo $row['invistr_batch']; ?></td>
												<td class="detail">&nbsp;<?php echo $row['expiryformateddate']; ?></td>
											<?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
													<td class="detail">&nbsp;<?php echo $row['invistr_price'];
						;
												?></td>
											<?php } ?>
												<td class="detail">&nbsp;<?php echo $row['invistr_quantity']; ?></td>
												<td class="detail">&nbsp;<?php echo $row['invistr_after_qty']; ?></td>
												<td class="detail">&nbsp;<?php echo $row['clinicname']; ?></td>
												<td class="detail">&nbsp;<?php echo $row['batchformateddate']; ?></td>
												<td class="detail">&nbsp;<?php echo $row['nickname']; ?></td>
											</tr>
					<?php
				}
				$srCount++;
			}
		}
								debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']);
								// assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
							}
	if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvAdjustment') {
		?>

					<?php
					$srCountFlag = $srCount - 1;
					if (!$srCountFlag) {
						?>
									<tr>
										<td colspan='9'><?php xl('No Record Found', 'e'); ?></td>
									</tr>
		<?php } //else { ?>
									<!--<tr>
										<td colspan='9'><?php //echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
									</tr>-->
		<?php //} ?>
							</tbody>
						</table>
					</div>
					<!-- end of search results --> <?php }
} else {
	?>
				<div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
				</div>

				<!-- stuff for the popup calendar -->
				<style type="text/css">
					@import url(../../library/dynarch_calendar.css);
				</style>
				<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
	<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
				<script type="text/javascript"
				src="../../library/dynarch_calendar_setup.js"></script>
				<script type="text/javascript">
														Calendar.setup({
															inputField: "form_from_date"
															, ifFormat: "%Y-%m-%d"
															, button: "img_from_date"
															, onUpdate: function () {
																var toDate = document.getElementById('form_to_date').value,
																		fromDate = document.getElementById('form_from_date').value;

																if (toDate) {
																	var toDate = new Date(toDate),
																			fromDate = new Date(fromDate);

																	if (fromDate > toDate) {
																		alert('From date cannot be later than To date.');
																		document.getElementById('form_from_date').value = '';
																	}
																}
															}
														});
														Calendar.setup({
															inputField: "form_to_date"
															, ifFormat: "%Y-%m-%d"
															, button: "img_to_date"
															, onUpdate: function () {
																var toDate = document.getElementById('form_to_date').value,
																		fromDate = document.getElementById('form_from_date').value;

																if (fromDate) {
																	var toDate = new Date(toDate),
																			fromDate = new Date(fromDate);

																	if (fromDate > toDate) {
																		alert('From date cannot be later than To date.');
																		document.getElementById('form_to_date').value = '';
																	}
																}
															}
														});
				</script>

<?php } ?> 
<?php
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvAdjustment') {
	?>
				<input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
				<input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
				<input type='hidden' name='form_refresh' id='form_refresh' value='' /></form>

			<script type="text/javascript">
	<?php
	if ($alertmsg) {
		echo " alert('$alertmsg');\n";
	}
	?>
			</script>
		</div>
		</div>
		</body>

		<!-- stuff for the popup calendar -->
		<style type="text/css">
			@import url(../../library/dynarch_calendar.css);
		</style>
		<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
	<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
		<script type="text/javascript"
		src="../../library/dynarch_calendar_setup.js"></script>
		<script type="text/javascript">
				Calendar.setup({
					inputField: "form_from_date"
					, ifFormat: "%Y-%m-%d"
					, button: "img_from_date"
					, onUpdate: function () {
						var toDate = document.getElementById('form_to_date').value,
								fromDate = document.getElementById('form_from_date').value;

						if (toDate) {
							var toDate = new Date(toDate),
									fromDate = new Date(fromDate);

							if (fromDate > toDate) {
								alert('From date cannot be later than To date.');
								document.getElementById('form_from_date').value = '';
							}
						}
					}
				});
				Calendar.setup({
					inputField: "form_to_date"
					, ifFormat: "%Y-%m-%d"
					, button: "img_to_date"
					, onUpdate: function () {
						var toDate = document.getElementById('form_to_date').value,
								fromDate = document.getElementById('form_from_date').value;

						if (fromDate) {
							var toDate = new Date(toDate),
									fromDate = new Date(fromDate);

							if (fromDate > toDate) {
								alert('From date cannot be later than To date.');
								document.getElementById('form_to_date').value = '';
							}
						}
					}
				});
		</script>

	</html>
	<?php
}
?>