<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("./lib/database.php");
 require_once("./lib/inv.gacl.class.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $transId = $_REQUEST['transId'];
 $info_msg = "";
 $tmpl_line_no = 0;
 
 if (!$invgacl->acl_check('inventory', 'invf_cons_edit','users',$_SESSION['authUser'])) die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
	<title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
    <?php include_once("themestyle.php"); ?>
    <?php include_once("scriptcommon.php"); ?>
	<script language="JavaScript">
		<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
		function submitform() {
			if (document.forms[0].facility.value>0 && document.forms[0].invistr_id.value>0 && parseInt(document.forms[0].form_old_qty.value)>0 && document.forms[0].form_qty.value.length>0) {
				var itemStockValue = document.forms[0].itemid.value;
				var itemStockArr = itemStockValue.split('#');
				if(parseInt(document.forms[0].form_old_qty.value) > parseInt(document.forms[0].form_qty.value)){
					return true;
				}else{
					if((parseInt(itemStockArr[2])+parseInt(document.forms[0].form_old_qty.value)) >= parseInt(document.forms[0].form_qty.value)) {
						return true;
					}else {
						document.forms[0].form_qty.style.backgroundColor="red";
						alert('Required field missing: Entered item quantity not avialable in stock.', 'Alert');
						document.forms[0].form_qty.focus();
						return false;
					}
				}
			} else {
				if (document.forms[0].facility.value<=0)
				{
					document.forms[0].facility.style.backgroundColor="red";
					alert('Required field missing: Facility is should not be blank.', 'Alert');
					document.forms[0].facility.focus();
					return false;
				}
				if (document.forms[0].invistr_id.value<=0)
				{
					document.forms[0].invistr_id.style.backgroundColor="red";
					alert('Required field missing: Consumption id should not be blank.', 'Alert');
					document.forms[0].invistr_id.focus();
					return false;
				}			
				if (parseInt(document.forms[0].form_old_qty.value)<=0)
				{
					document.forms[0].form_old_qty.style.backgroundColor="red";
					alert('Required field missing: Consumption added quantity should not be blank.', 'Alert');
					document.forms[0].form_old_qty.focus();
					return false;
				}
				if (document.forms[0].form_qty.value.length<=0)
				{
					document.forms[0].form_qty.style.backgroundColor="red";
					alert('Required field missing: Please enter the Item quantity.', 'Alert');
					document.forms[0].form_qty.focus();
					return false;
				}				
			}
		}
	</script>
</head>
<body>
<?php
	// If we are saving, then save and close the window.
	// First check for duplicates.
	//
	if ($_POST['form_save']) {
		$itemStockValue = $_POST['itemid'];
		$itemStockArr = explode('#', $itemStockValue);
		$itemStockDetails = getItemStockDetail($itemStockArr[0]);
		//print_r($itemStockDetails);
		if($itemStockDetails['invist_quantity']+$_POST['form_old_qty'] >= $_POST['form_qty']){
			$stockTransDetails = getStockTransDetail($transId,$pdoobject);
			if($_POST['form_old_qty'] >= $_POST['form_qty']) {			
				addStockQuantity(escapedff('form_old_qty')-escapedff('form_qty'), $itemStockArr[0]);
				$editedQty = escapedff('form_old_qty')-escapedff('form_qty');
	                        if($editedQty > 0){
	                            $tranData=array(
	                                            'invistr_itemid' => $itemStockDetails['invist_itemid'],
	                                            'invistr_batch' => $itemStockDetails['invist_batch'],
	                                            'invistr_expiry' => $itemStockDetails['invist_expiry'],
	                                            'invistr_price' => $itemStockDetails['invist_price'],
	                                            'invistr_quantity' => $editedQty,
	                                            'invistr_before_qty' => $itemStockDetails['invist_quantity'],
	                                            'invistr_after_qty' => $itemStockDetails['invist_quantity']+$editedQty,
	                                            'invistr_clinic_id' => escapedff('facility'),
	                                            'invistr_tran_type' => 17,
	                                            'invistr_comment' => escapedff('form_desc'),
	                                            'invistr_createdby' => $_SESSION['authId'],
	                                            'invistr_created_date' => date("Y-m-d H:i:s")
	                            );
	                            $stockTranid = insertStockTransaction($tranData);
	                        }
			}else {			
				removeStockQuantity(escapedff('form_qty')-escapedff('form_old_qty'), $itemStockArr[0]);
				$editedQty = escapedff('form_qty')-escapedff('form_old_qty');
				$tranData=array(
						'invistr_itemid' => $itemStockDetails['invist_itemid'],
						'invistr_batch' => $itemStockDetails['invist_batch'],
						'invistr_expiry' => $itemStockDetails['invist_expiry'],
						'invistr_price' => $itemStockDetails['invist_price'],
						'invistr_quantity' => $editedQty,
						'invistr_before_qty' => $itemStockDetails['invist_quantity'],
						'invistr_after_qty' => $itemStockDetails['invist_quantity']-$editedQty,
						'invistr_clinic_id' => escapedff('facility'),
						'invistr_tran_type' => 12,
						'invistr_comment' => escapedff('form_desc'),
						'invistr_createdby' => $_SESSION['authId'],
						'invistr_created_date' => date("Y-m-d H:i:s")
				);
				$stockTranid = insertStockTransaction($tranData);
			}
			/*$beforeQty = $stockTransDetails['invistr_after_qty']+escapedff('form_old_qty');
			$tranData=array(
					'invistr_id' => escapedff('invistr_id'),
					'invistr_quantity' => escapedff('form_qty'),
					'invistr_before_qty' => $beforeQty,
					'invistr_after_qty' => $beforeQty-escapedff('form_qty'),
					'invistr_comment' => escapedff('form_desc'),
					'invistr_createdby' => $_SESSION['authId'],
					'invistr_created_date' => date("Y-m-d H:i:s")
			);*/
			//$transId = updateStockTransaction($tranData);
			//sqlStatement("update inv_item_stock_transaction set invistr_quantity=".escapedff('form_qty')." where invistr_id = '" . escapedff('invistr_id') . "' AND invistr_clinic_id = ".escapedff('facility'));		
		  // Close this window and redisplay the updated list of drugs.
		  //  
		  echo "<script language='JavaScript'>\n";
		  if ($info_msg) echo " alert('$info_msg');\n";
		  echo " parent.location.reload();\n";  
		  echo "</script></body></html>\n";
		  exit();
		}else {
			$alertmsg='Entered quantity not available in selected item stock. Please try again.';
		}
	}
	if ($transId) {	
		$row = sqlQuery("SELECT ist.invistr_id, ist.invistr_itemid, ist.invistr_batch, ist.invistr_expiry, ist.invistr_price, ist.invistr_quantity, ist.invistr_clinic_id, ist.invistr_isdeleted, ist.invistr_createdby, ist.invistr_comment, ist.invistr_created_date, im.inv_im_name " .
			"FROM inv_item_stock_transaction AS ist  
			INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invistr_itemid 
			WHERE ist.invistr_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invistr_tran_type = 12 AND ist.invistr_createdby = ? AND ist.invistr_id = ?", array($_SESSION['authId'], $transId));
		$stockDetails = getClincItemStockDetail($row['invistr_itemid'], $row['invistr_batch'], $row['invistr_expiry'], $row['invistr_price'], $row['invistr_clinic_id']);
                $stockDetailsList =$stockDetails[0];
                
        }
	if(empty($row)){
	?>
		<script language="JavaScript">
		alert('You should not able to access this page due to wrong consumption.');
		</script>
	<?php
	}
?>
<div class="infopop"><?php  xl('Edit Consumption','e'); ?></div>
<div id='popUpformWrap'>
	<form class='nomrgn' method='post' name='theform' action='edit_consumption.php?transId=<?php echo $transId; ?>'>
		<input type='hidden' name='form_old_qty' value='<?php echo attr($row['invistr_quantity']) ?>' />
		<input type='hidden' name='facility' value='<?php echo attr($row['invistr_clinic_id']) ?>' />
		<input type='hidden' name='invistr_id' value='<?php echo attr($row['invistr_id']) ?>' />
		<input type='hidden' name='itemid' value='<?php echo $stockDetailsList['invist_id']."#".$stockDetailsList['invist_itemid']."#".$stockDetails['invist_quantity']; ?>' />
		<!-- Row 1 -->
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Item'); ?></label>
					<input type='text' value='<?php echo attr($row['inv_im_name']).'#'.attr($row['invistr_batch']).'#'.attr($row['invistr_expiry']); ?>' disabled />
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo xlt('Quantity'); ?></label>
					<input type='number' size='10' name='form_qty' id='form_qty' maxlength='7' onkeypress="return isNumber(event);" value='<?php echo attr($row['invistr_quantity']) ?>' class='formEle' />
				</div>
			</div>
		</div>
		<!-- Row 1 -->
		<!-- Row 3 -->
		<div class="row">
			<div class="col-sm-6 txtarea">
				<div class="form-group">
					<label><?php echo xlt('Notes'); ?></label>
					<textarea name='form_desc' rows="5" cols="34"><?php echo attr($row['invistr_comment'])?></textarea>
				</div>
			</div>
		</div>
		<!-- Row 3 -->
		<a id="subform" href="javascript:void(0)" class="save-btn"></a>
		<input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' onclick="return submitform();" value='<?php echo xla('Save'); ?>' />
	</form>
</div>
	<script language="JavaScript">
	<?php if ($alertmsg) {
		echo "alert('" . htmlentities($alertmsg) . "');\n";
	} ?>
	</script>
</body>
</html>