<?php
//require_once("../globals.php");
function mag_update_status($mag_order_id, $status, $comment = "", $notify = false) {
    $magUrl = $GLOBALS['mag']['magUrl'];
    $magApiUser = $GLOBALS['mag']['magApiUser'];
    $magApiKey = $GLOBALS['mag']['magApiKey'];
    try {
        $client = new SoapClient($magUrl . 'api/soap/?wsdl');

        // If somestuff requires api authentification,
        // then get a session token
        $session = $client->login($magApiUser, $magApiKey);

        $result = $client->call($session, 'sales_order.addComment', array('orderIncrementId' => $mag_order_id, 'status' => $status, 'comment'=>$comment, 'notify'=>$notify));
    return $result;
    } catch (SoapFault $fault) {
        print_r($fault);    //("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
    }
}
function 
sendMail($faceltyId,$templet,$ffaceltyId,$reqId,$orderId,$sendRole=null){
	//echo $sendRole;die();
    
        $mail = new PHPMailer();
        $globalsData=$GLOBALS;
        
        $sender_name = $globalsData['patient_reminder_sender_name'];
        $email_address = $globalsData['patient_reminder_sender_email'];
        $mail->Mailer = "smtp";
        $mail->SMTPAuth = true;
        $mail->IsHTML(true);
        $mail->Host = $globalsData['SMTP_HOST'];
        $mail->Username = $globalsData['SMTP_USER'];
        $mail->Password = $globalsData['SMTP_PASS'];
        $mail->Port = $globalsData['SMTP_PORT'];
        $mail->FromName = $sender_name;  // required
        $mail->Sender = $email_address;    // required
        $mail->From = $email_address;    // required
        		
        $mail->AddReplyTo($email_address, $sender_name);  // required
        //$mail->SMTPDebug =true;
        
    $query = "select name from facility where id='" . $ffaceltyId . "'";
    $fdata = sqlFetchArray(sqlStatement($query));
    $faciltyName=$fdata['name'];
    
    //$query = "select ent_id,ent_tem_type,ent_patient_subject,ent_provider_subject,ent_provider_message,ent_patient_message from email_notification_template where ent_tem_type='" . $templet . "'";
    
    $query = "select subject,message from email_templates where name='" . $templet . "'";
    
    $tempData = sqlFetchArray(sqlStatement($query));
   
    $temp=$tempData['message'];
    $subject=$tempData['subject'];
    
    $mail->Subject = $subject;
    if($faceltyId!=0){
     $query="SELECT `User`.`id`, `User`.`username`, `User`.`fname`, 
    `User`.`mname`, `User`.`lname`, `User`.`nickname`, `User`.`email`, 
    `Group`.`id`, `Group`.`name`, `Group`.`value` FROM `users` AS 
    `User` LEFT JOIN `gacl_aro_groups` AS `Group` ON(`User`.`group_id` 
    = `Group`.`id`) LEFT JOIN `facility` AS `DefaultFacility` ON ( 
    `User`.`facility_id` = `DefaultFacility`.`id`) WHERE 
    `User`.`active`='1' AND `User`.`facility_id` = '".$faceltyId."'";
    if(!empty($sendRole)){
	  $query.=" AND `Group`.`value`='".$sendRole."'";	
	}
    $query.=" ORDER BY `User`.`username` ASC";
    
    $rs = sqlStatement($query);
    while($getData = mysql_fetch_array($rs)){
       $tempRep="";
       if($templet=='RequisitionAlert'){
		   if($getData["value"]=="clin"){
			   //echo $getData["email"];
               $tempRep=str_replace("***USERNAME***",$getData['nickname'],$temp);
               $tempRep=str_replace("***FACILITYNAME***",$faciltyName,$tempRep);
               $tempRep=str_replace("***TRANSACTION***",$reqId,$tempRep);
               $tempRep=str_replace("***ORDERID***",$orderId,$tempRep); 
               $mail->AddAddress($getData["email"]);   // required		
               $mail->Body = $tempRep;
               $mail->Send();
               $mail->ClearAddresses();
               
           }
		   
	   }elseif($templet=='WarehouseRequisitionAlert'){
           if($getData["value"]=="invuser" || $getData["value"]=="invadmin"|| $getData["value"]=="clin"){
               $tempRep=str_replace("***USERNAME***",$getData['nickname'],$temp);
               $tempRep=str_replace("***FACILITYNAME***",$faciltyName,$tempRep);
               $tempRep=str_replace("***TRANSACTION***",$reqId,$tempRep);
               $tempRep=str_replace("***ORDERID***",$orderId,$tempRep); 
               $mail->AddAddress($getData["email"]);   // required		
               $mail->Body = $tempRep;
               $mail->Send();
               $mail->ClearAddresses();
               
           }
       }elseif($templet=='WarehouseRequisitionAccept'){
           
           if($getData["value"]=="invuser" || $getData["value"]=="invadmin"){
               $tempRep=str_replace("***USERNAME***",$getData['nickname'],$temp);
               $tempRep=str_replace("***FACILITYNAME***",$faciltyName,$tempRep);
               $tempRep=str_replace("***TRANSACTION***",$reqId,$tempRep);
               $tempRep=str_replace("***ORDERID***",$orderId,$tempRep); 
               $mail->AddAddress($getData["email"]);   // required		
               $mail->Body = $tempRep;
               $mail->Send();
               $mail->ClearAddresses();
               
           }
           
       }elseif($templet=='WarehouseRequisitionReject'){
           
           if($getData["value"]=="invuser" || $getData["value"]=="invadmin"){
               $tempRep=str_replace("***USERNAME***",$getData['nickname'],$temp);
               $tempRep=str_replace("***FACILITYNAME***",$faciltyName,$tempRep);
               $tempRep=str_replace("***TRANSACTION***",$reqId,$tempRep);
               $tempRep=str_replace("***ORDERID***",$orderId,$tempRep); 
               $mail->AddAddress($getData["email"]);   // required		
               $mail->Body = $tempRep;
               $mail->Send();
               $mail->ClearAddresses();
               
           }
           
       }
    }}else{
      
    echo $query = "SELECT `User`.`id`, `User`.`username`, 
    `User`.`fname`, `User`.`mname`, `User`.`lname`, `User`.`nickname`, 
    `User`.`email` FROM `users` AS `User` JOIN `inv_stock_requisition` AS `Requis` ON( `User`.`id` = `Requis`.`isr_createdby`) WHERE `Requis`.`isr_id` = '".$reqId."' ORDER BY `User`.`username` ASC ";
    $fdata = sqlFetchArray(sqlStatement($query));
    if(!empty($fdata)){
        if($templet=='UserRequisitionAccept'){
               $tempRep=str_replace("***USERNAME***",$fdata['nickname'],$temp);
               $tempRep=str_replace("***FACILITYNAME***",$faciltyName,$tempRep);
               $tempRep=str_replace("***TRANSACTION***",$reqId,$tempRep);
               $tempRep=str_replace("***ORDERID***",$orderId,$tempRep); 
               $mail->AddAddress($fdata["email"]);   // required		
               $mail->Body = $tempRep;
               $mail->Send();
               $mail->ClearAddresses();
        }elseif ($templet=='UserRequisitionReject') {
              $tempRep=str_replace("***USERNAME***",$fdata['nickname'],$temp);
               $tempRep=str_replace("***FACILITYNAME***",$faciltyName,$tempRep);
               $tempRep=str_replace("***TRANSACTION***",$reqId,$tempRep);
               $tempRep=str_replace("***ORDERID***",$orderId,$tempRep); 
               $mail->AddAddress($fdata["email"]);   // required		
               $mail->Body = $tempRep;
               $mail->Send();
               $mail->ClearAddresses(); 
        }
    }
    
    }
  //die();  
}

function mag_order_cancel($mag_order_id) {
    $magUrl = $GLOBALS['mag']['magUrl'];
    $magApiUser = $GLOBALS['mag']['magApiUser'];
    $magApiKey = $GLOBALS['mag']['magApiKey'];
    try {
        $client = new SoapClient($magUrl . 'api/soap/?wsdl');

        // If somestuff requires api authentification,
        // then get a session token
        $session = $client->login($magApiUser, $magApiKey);

        $result = $client->call($session, 'sales_order.cancel', $mag_order_id);
    return $result;
    } catch (SoapFault $fault) {
        print_r($fault);    //("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
    }
}
?>
