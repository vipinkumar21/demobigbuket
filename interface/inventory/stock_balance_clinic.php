<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.

require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");

// Prepare a string for CSV export.
function qescape($str) {
  $str = str_replace('\\', '\\\\', $str);
  return str_replace('"', '\\"', $str);
}
$alertmsg = ''; // not used yet but maybe later
//$patient = $_REQUEST['patient'];
$from_date = fixDate($_POST['form_from_date'], '');
$to_date   = fixDate($_POST['form_to_date'], '');
if (empty($to_date) && !empty($from_date)) $to_date = date('Y-12-31');
if (empty($from_date) && !empty($to_date)) $from_date = date('Y-01-01');


$show_available_times = false;

$ORDERHASH = array(
		'invistr_id' => 'invistr_id',
		'im.inv_im_name' => 'im.inv_im_name',
		'invistr.invistr_batch'=>'invistr.invistr_batch',
		'invistr.invistr_created_date'  => 'invistr.invistr_created_date'
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'im.inv_im_name';
$orderby = $ORDERHASH[$form_orderby].' DESC';

$form_orderby1 = $ORDERHASH[$_REQUEST['form_orderby1']] ? $_REQUEST['form_orderby1'] : 'invistr.invistr_batch';
$orderby .= ', '.$ORDERHASH[$form_orderby1].' DESC';


$form_orderby2 = $ORDERHASH[$_REQUEST['form_orderby2']] ? $_REQUEST['form_orderby2'] : 'invistr.invistr_created_date';
$orderby .= ', '.$ORDERHASH[$form_orderby2].' DESC';

if(isset($_POST['form_facility']) && $_POST['form_facility']!=''){
	$facility  = $_POST['form_facility'];
	$where = " AND invistr.invistr_clinic_id = '".$facility."'" ;
}
if(isset($_POST['form_provider']) && $_POST['form_provider']!=''){
	$provider  = $_POST['form_provider'];
	$where .= " AND invistr.invistr_createdby = '" .$provider."'" ;
}
if(isset($_POST['form_transaction_type']) && $_POST['form_transaction_type']!=''){
	$transactionId  = $_POST['form_transaction_type'];
	$where .= " AND invtrt.invtrt_id = '" .$transactionId."'" ;
}
if (!empty($from_date)) {
	$where .= " AND invistr.invistr_created_date >= '$from_date 00:00:00' AND "."invistr.invistr_created_date <= '$to_date 23:59:59' ";
}
/*
$facility_filter = '';
if ( $facility_id ) {
	$event_facility_filter = " AND e.pc_facility = '$facility_id'";
	$provider_facility_filter = " AND u.facility_id = '$facility_id'";
	$facility_filter = $event_facility_filter . $provider_facility_filter;
}
*/
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport']=='csvStockBalance') {
  header("Pragma: public");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Content-Type: application/force-download");
  header("Content-Disposition: attachment; filename=stock balance clinic.csv");
  header("Content-Description: File Transfer");
}
else {
?>

<html>

<head>
<?php html_header_show();?>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">

<title><?php xl('Stock Leadger','e'); ?></title>

<script type="text/javascript" src="../../library/overlib_mini.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

<script type="text/javascript">
var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

function dosort(orderby) {
    var f = document.forms[0];
    f.form_orderby.value = orderby;
    f.submit();
    return false;
}

function oldEvt(eventid) {
    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
}

function refreshme() {
    document.forms[0].submit();
}

 
$(document).ready(function(){
     var facilityID = $("#form_facility").val();
                    if(facilityID != '' ){
                    var providerID =$('#form_provider').val();
                    $.ajax({
                            url: "get_facility_provider.php?facilityId=" + facilityID,
                            success: function (result) {
                                $("#form_provider").html(result);
                                 $('#form_provider').val(providerID);
                            }
                        });

                    }
	$("#form_facility").change(function(){
		var facilityID = $("#form_facility").val();
		$.ajax({
			url:"get_facility_provider.php?facilityId="+facilityID,
			success:function(result){
				$("#form_provider").html(result);
			}
		});
	});
});
</script>

<style type="text/css">
/* specifically include & exclude from printing */
@media print {
	#report_parameters {
		visibility: hidden;
		display: none;
	}
	#report_parameters_daterange {
		visibility: visible;
		display: inline;
	}
	#report_results table {
		margin-top: 0px;
	}
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
}
</style>
</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
<span class='title'><?php xl('Report','e'); ?> - <?php xl('Stock Ledger','e'); ?></span>
<div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($form_to_date)); ?>
</div>

<form method='post' name='theform' id='theform' action='stock_balance_clinic.php'>
<div id="report_parameters">
<input type='hidden' name='form_refresh' id='form_refresh' value=''/>
<input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
<table>
	<tr>
		<td width='650px'>
		<div style='float: left'>
		<table class='text'>
			<tr>
				<td class='label'><?php xl('Facility','e'); ?>:</td>
				<td><?php //dropdown_facility(strip_escape_custom($facility), 'form_facility'); ?>
				
				<select name="form_facility" id="form_facility" >
				      <?php 
				      if(empty($row['invist_clinic_id'])){
				      ?>
				      <option value='0' selected="selected">Select Facility</option>
				      <?php
				      }
					  $countUserFacilities = 0;
					  if (!$GLOBALS['restrict_user_facility']) {
						$qsql = sqlStatement("
							select id, name, color
							from facility
							where service_location != 0
						");
					  } else {
						  $qsql = sqlStatement("
							  select uf.facility_id as id, f.name, f.color
							  from users_facility uf
							  left join facility f on (uf.facility_id = f.id)
							  where uf.tablename='users' 
							  and uf.table_id = ? 
							", array($_SESSION['authId']) );
					  }      
				      while ($facrow = sqlFetchArray($qsql)) {       
						$selected = ( $facrow['id'] == $facility ) ? 'selected="selected"' : '' ;
				        echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";       
						$countUserFacilities++;
				        /************************************************************/
				      }      
				      ?>      
				      </select>
				
				</td>
				
				
				<?php /*
				<td class='label'><?php xl('Provider','e'); ?>:</td>
				<td><?php
				$query = "SELECT id, lname, fname FROM users WHERE "."authorized = 1 $provider_facility_filter ORDER BY lname, fname"; 
				$ures = sqlStatement($query);
				echo "   <select name='form_provider' id='form_provider'>\n";
				echo "    <option value=''>-- " . xl('All') . " --\n";
				while ($urow = sqlFetchArray($ures)) {
					$provid = $urow['id'];
					echo "    <option value='$provid'";
					if ($provid == $_POST['form_provider']) echo " selected";
					echo ">" . $urow['lname'] . ", " . $urow['fname'] . "\n";
				}
				echo "   </select>\n";
				?></td>
				
				
				
				<td class='label'><?php xl('Transaction Type','e'); ?>:</td>
				<td><?php
				$query = "SELECT invtrt_id, invtrt_name FROM inv_transaction_type ORDER BY invtrt_name ";
				$transactionType = sqlStatement($query);
				echo "   <select name='form_transaction_type' id='form_transaction_type'>\n";
				echo "    <option value=''>-- " . xl('All') . " --\n";
				while ($trow = sqlFetchArray($transactionType)) {
					$trid = $trow['invtrt_id'];
					echo "    <option value='$trid'";
					if ($trid == $_POST['form_transaction_type']) echo " selected";
					echo ">" . $trow['invtrt_name'] . "\n";
				}
				echo "   </select>\n";
				?></td>
				*/ ?>
			</tr>
			<tr>
				<td class='label'><?php xl('From','e'); ?>:</td>
				<td><input type='text' name='form_from_date' id="form_from_date"
					size='10' value='<?php echo $form_from_date ?>'
					onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)'
					title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
					align='absbottom' width='24' height='22' id='img_from_date'
					border='0' alt='[?]' style='cursor: pointer'
					title='<?php xl('Click here to choose a date','e'); ?>'></td>
				<td class='label'><?php xl('To','e'); ?>:</td>
				<td><input type='text' name='form_to_date' id="form_to_date"
					size='10' value='<?php echo $form_to_date ?>'
					onkeyup='datekeyup(this,mypcc)' onblur='dateblur(this,mypcc)'
					title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
					align='absbottom' width='24' height='22' id='img_to_date'
					border='0' alt='[?]' style='cursor: pointer'
					title='<?php xl('Click here to choose a date','e'); ?>'></td>
			</tr>
		</table>

		</div>

		</td>
		<td align='left' valign='middle' height="100%">
		<table style='border-left: 1px solid; width: 100%; height: 100%'>
			<tr>
				<td>
				<div style='margin-left: 15px'>
                                <a href='#' class='css_button' onclick='$("#form_csvexport").attr("value","viewStockBalance"); $("#theform").submit();'>
				<span> <?php xl('Submit','e'); ?> </span> </a> 
				
				<?php if(1) { ?>
				<a href='#' class='css_button' onclick='$("#form_csvexport").attr("value","csvStockBalance"); $("#theform").submit();'>
					<span>
						<?php xl('Export to CSV','e'); ?>
					</span>
					</a>
				<?php } ?>	
                <?php /*if ($_POST['form_refresh'] || $_POST['form_orderby'] ) { ?>
					<a href='#' class='css_button' onclick='window.print()'> 
                    <span> <?php xl('Print','e'); ?> </span> </a> 
                    <a href='#' class='css_button' onclick='window.open("../patient_file/printed_fee_sheet.php?fill=2","_blank")'> 
                    <span> <?php xl('Superbills','e'); ?> </span> </a> 
                <?php }*/ ?>
                
                </div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</div>
<!-- end of search parameters --> <?php
} // end not form_csvexport
if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
if ($_POST['form_csvexport'] && $_POST['form_csvexport']=='csvStockBalance') {
    // CSV headers:
    echo '"' . xl('S. No.') . '",';
    echo '"' . xl('Clinic Name') . '",';
    echo '"' . xl('Item Name') . '",';
    echo '"' . xl('Item Code') . '",';
    echo '"' . xl('Batch') . '",';
    echo '"' . xl('Expiry') . '",';
    if(0){
	    echo '"' . xl('Price') . '",';
    }
    echo '"' . xl('Quantity') . '",';
    echo '"' . xl('Date') . '"' . "\n";
} else {
	?>
<div id="report_results">
<table>
<tr class='head'>
	<thead>
		<th><?php xl('S.N.','e'); ?></th>
		<th><?php xl('Clinic Name','e'); ?></th>
		<th><?php xl('Item Name','e'); ?></th>
		<th><?php xl('Item Code','e'); ?></th>
		<th><?php xl('Batch','e'); ?></th>
		<th><?php xl('Expiry','e'); ?></th>
		<?php if(0) { ?>
		<th><?php xl('Price','e'); ?></th>
		<?php } ?>
        <th><?php xl('Quantity','e');  ?></th>
        <th><?php xl('Date','e'); ?></th>
      </thead>
	<tbody>
		<!-- added for better print-ability -->
	<?php
}
//Get Stock Ledgel Detail 
$stockLedgerData = getStockLedgelDetail($where);

$srCount=1;
if ($_POST['form_csvexport'] && $_POST['form_csvexport']=='csvStockBalance') {
	foreach($stockLedgerData as $key => $val){
		//CSV EXPORT - First Nested
		echo '"' . qescape($srCount) . '",';
		echo '"' . qescape($val['clinic_name']) . '",';
		echo '"",';
		echo '"",';
		echo '"",';
		echo '"",';
		echo '"",';
		//echo '"",';
		echo '""' . "\n";
	
		foreach($val['clinicWiseDetail'] as $subKey => $subVal){ 
			//CSV EXPORT - Second Nested
			echo '"",';
			echo '"",';
			echo '"' . qescape($subVal['item_name']) . '",';
			echo '"",';
			echo '"",';
			echo '"",';
			echo '"",';
			//echo '"",';
			echo '""' . "\n";
			
			//Third
			foreach($subVal['itemWiseDetail'] as $secondNestedKey => $secondNestedVal){ 
					//CSV EXPORT - Second Nested
					echo '"",';
					echo '"",';
					echo '"",';
					echo '"' . qescape($secondNestedVal['item_code']) . '",';
					echo '"' . qescape($secondNestedVal['item_stock_batch']) . '",';
					echo '"' . qescape($secondNestedVal['item_stock_expiry']) . '",';
					echo '"' . qescape($secondNestedVal['item_stock_qty']) . '",';
					echo '"' . qescape($secondNestedVal['item_transaction_date']) . '"' . "\n";
			} 
			//Third
		}
		$srCount++;
	}
} else {

	
	foreach($stockLedgerData as $key => $val){ ?>
		<tr bgcolor='<?php echo $row['invtrt_color'] ?>'>
			<td class="detail">&nbsp;<?php echo $srCount; ?></td>
			<td class="detail"><?php echo $val['clinic_name']; ?></td>
			<td class="detail"><?php echo $val['item_name']; ?></td>
			<td class="detail"><?php //echo $val['item_code']; ?></td>
			<td class="detail"><?php //echo $val['item_stock_batch']; ?></td>
			<td class="detail"><?php //echo $val['item_stock_expiry']; ?>	</td>
			<td class="detail">&nbsp;<?php //echo $val['invistr_quantity']; ?></td>
			<td class="detail"><?php //echo '0'; ?></td>
		</tr>
		<?php 
		$itemNameRepet = '';
		$countItemBasedRunningQty = 0 ;
		foreach($val['clinicWiseDetail'] as $subKey => $subVal){ ?>
			<tr bgcolor='<?php echo $row['invtrt_color'] ?>'>
				<td class="detail">&nbsp;<?php echo ''; ?></td>
				<td class="detail"><?php //echo $subVal['item_name']; ?></td>
				<td class="detail"><?php echo $subVal['item_name']; ?></td>
				<td class="detail"><?php echo $subVal['item_code']; ?>	</td>
				<td class="detail"><?php //echo $subVal['item_stock_batch']; ?></td>
				<td class="detail"><?php //echo $subVal['item_stock_expiry']; ?>	</td>
				<td class="detail">&nbsp;<?php //echo $subVal['item_stock_qty']; ?></td>
				<td class="detail">&nbsp;<?php //echo $subVal['item_transaction_date']; ?></td>
			</tr>
				<?php 
				
				//Third
				//$itemNameRepet = '';
				//$countItemBasedRunningQty = 0 ;
				foreach($subVal['itemWiseDetail'] as $secondNestedKey => $secondNestedVal){ ?>
					<tr bgcolor='<?php echo $secondNestedVal['item_transaction_color_code'] ?>'>
						<td class="detail">&nbsp;<?php echo ''; ?></td>
						<td class="detail"><?php //echo $subVal['item_name']; ?></td>
						<td class="detail"><?php //echo $subVal['item_name']; ?></td>
						<td class="detail"><?php echo $secondNestedVal['item_code']; ?>	</td>
						<td class="detail"><?php echo $secondNestedVal['item_stock_batch']; ?></td>
						<td class="detail"><?php echo $secondNestedVal['item_stock_expiry']; ?>	</td>
						<td class="detail">&nbsp;<?php echo $secondNestedVal['item_stock_qty']; ?></td>
						<td class="detail">&nbsp;<?php echo $secondNestedVal['item_transaction_date']; ?></td>
					</tr>
				<?php //Third
				} 
				
				?>		
		<?php }
		
		$srCount++;
	}

}
	// assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
    //$_SESSION['pidList'] = $pid_list;
	if ($_POST['form_csvexport'] && $_POST['form_csvexport']!='csvStockBalance') {
	?>
	
	<?php if(0) { ?>
	<tr class="report_totals">
	<td colspan='10'><?php xl('Total Number of Stock Ledger Report','e'); ?> : <?php echo --$srCount; ?></td>
 	</tr>
 	<?php } ?>
 	
 	
 	<?php 
	
	$srCountFlag = $srCount-1;
	if(!$srCountFlag){ ?>
	<tr>
	<td colspan='8'><?php xl('No Record Found','e'); ?></td>
 	</tr>
 	<?php } ?>
 	
 	
	</tbody>
</table>
</div>
<!-- end of search results --> <?php } } else { ?>
<div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e' ); ?>
</div>

<!-- stuff for the popup calendar -->
<style type="text/css">
    @import url(../../library/dynarch_calendar.css);
</style>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript"
	src="../../library/dynarch_calendar_setup.js"></script>
<script type="text/javascript">
 Calendar.setup({inputField:"form_from_date", ifFormat:"%Y-%m-%d", button:"img_from_date"});
 Calendar.setup({inputField:"form_to_date", ifFormat:"%Y-%m-%d", button:"img_to_date"});
</script>

	<?php } ?> 
	<?php
	if ($_POST['form_csvexport'] && $_POST['form_csvexport']!='csvStockBalance') {
	?>
	<input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
	<input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
	<input type='hidden' name='form_refresh' id='form_refresh' value='' /></form>
<script type="text/javascript">
<?php
if ($alertmsg) { echo " alert('$alertmsg');\n"; }
?>
</script>

</body>
<?php
}
?>

<?php
	if ($_POST['form_csvexport'] && $_POST['form_csvexport']!='csvStockBalance') {
	?>
<!-- stuff for the popup calendar -->
<style type="text/css">
    @import url(../../library/dynarch_calendar.css);
</style>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript"
	src="../../library/dynarch_calendar_setup.js"></script>
<script type="text/javascript">
 Calendar.setup({inputField:"form_from_date", ifFormat:"%Y-%m-%d", button:"img_from_date"});
 Calendar.setup({inputField:"form_to_date", ifFormat:"%Y-%m-%d", button:"img_to_date"});
</script>

</html>
<?php
}
?>
