<?php
 // Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.
 
 $sanitize_all_escapes  = true;
 $fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 // Check authorization.
 $thisauth = acl_check('inventory', 'im_uom_list');
 if (!$thisauth) die(xlt('Not authorized'));

 if (isset($_GET["mode"])) {
 	if ($_GET["mode"] == "delete") { 		
 		if(checkUomStatus($_GET["uomid"])){
 			/*echo "update inv_uom set invuom_deleted='1' where invuom_id = '" . $_GET["uomid"] . "'";
 			die();*/
 			sqlStatement("update inv_uom set invuom_deleted='1' where invuom_id = '" . $_GET["uomid"] . "'");
 			$_SESSION['alertmsg'] = 'Unit Of Measurement deleted successfully.';
 			header('Location: uom.php');
 			exit;
 		}else {
 			$_SESSION['alertmsg'] = 'You should not able delete selected Unit Of Measurement.';
 			header('Location: uom.php');
 			exit;
 		}
 		
 	}
 	if ($_GET["mode"] == "activate") { 		
 		sqlStatement("update inv_uom set invuom_status='1' where invuom_id = '" . $_GET["uomid"] . "'"); 		
 		$_SESSION['alertmsg'] = 'Unit Of Measurement activated successfully.';
 		header('Location: uom.php');
 		exit;
 	}
 	if ($_GET["mode"] == "deactivate") { 		
 		sqlStatement("update inv_uom set invuom_status='0' where invuom_id = '" . $_GET["uomid"] . "'");
 		$_SESSION['alertmsg'] = 'Unit Of Measurement deactivated successfully.';
 		header('Location: uom.php');
 		exit;
 	}
 }
 // For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
	'invuom_id' => 'invuom_id DESC',
	'invuom_name' => 'invuom_name',
	'invuom_status'  => 'invuom_status',  
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invuom_id';
$orderby = $ORDERHASH[$form_orderby];



 // get drugs
 /*echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " . 
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby";*/
 $res = "SELECT invuom_id, invuom_name, invuom_desc, invuom_status, invuom_createdby, invuom_deleted " .
  "FROM inv_uom " . 
  //"WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby";
 $num_rows = sqlNumRows(sqlStatement($res));	// total no. of rows
 $per_page = $GLOBALS['encounter_page_size'];			// Pagination variables processing
 //$per_page = 2;
 $page = $_GET["page"];
 
 if(!$_GET["page"])
 {
 	$page=1;
 }
 
 $prev_page = $page-1;
 $next_page = $page+1;
 
 $page_start = (($per_page*$page)-$per_page);
 if($num_rows<=$per_page)
 {
 	$num_pages =1;
 }
 else if(($num_rows % $per_page)==0)
 {
 	$num_pages =($num_rows/$per_page) ;
 }
 else
 {
 	$num_pages =($num_rows/$per_page)+1;
 	$num_pages = (int)$num_pages;
 }
 $res .= " LIMIT $page_start , $per_page";
 $res = sqlStatement($res);
 
 
 
 
?>
<html>

<head>
<?php html_header_show();?>

<link rel="stylesheet" href='<?php  echo $css_header ?>' type='text/css'>
<title><?php echo xlt('Inventory UOM Master'); ?></title>

<style>
tr.head   { font-size:11pt; background-color:#cccccc; text-align:center; }
tr.detail { font-size:10pt; }
</style>
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<link rel="stylesheet" type="text/css" href="../../library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />

<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>

<script language="JavaScript">

// callback from add_edit_drug.php or add_edit_drug_inventory.php:
function refreshme() {
 location.reload();
}

// Process click on drug title.
function dodclick(id) {
 dlgopen('add_edit_uom.php?uomid=' + id, '_blank', 400, 275);
}
// Process click on a column header for sorting.
function dosort(orderby) {
 var f = document.forms[0];
 f.form_orderby.value = orderby;
 top.restoreSession();
 f.submit();
 return false;
}

$(document).ready(function(){
    // fancy box
    enable_modals();
});

function confirmation(link){
	jConfirm('Are you sure to delete?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}

function confirmation2(link){
	jConfirm('Are you sure to Activate?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}

function confirmation3(link){
	jConfirm('Are you sure to Deactivate?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}

</script>

</head>

<body class="body_top">
       <div class="panel panel-warning">
     <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3 boldtxt">
                              <?php  xl('Unit Of Measurement','e'); ?>
                                    </div>
                                    <div class="col-xs-9 text-right">
                            <?php if(acl_check('inventory', 'im_uom_add')){?><a href="add_edit_uom.php?uomid=0" class="iframe btn btn-default btn-sm" ><span><?php xl('Add UOM','e'); ?></span></a><?php }?>
                                    </div>
                                </div>
                            </div>

       <table border='0' cellpadding='1' cellspacing='2' width='100%'>
	 
	  <?php 
	  	if(!empty($_SESSION['alertmsg'])){
		?>
		<tr >
		<td colspan="2"><?php echo $_SESSION['alertmsg']; $_SESSION['alertmsg']="";?></td>		
	  </tr>
		<?php 
		}
	  ?>
	</table>
 
<div class="panel-body">
<form id="report_results" method='post' action='uom.php'>

<table border='0' class="table table-bordered" cellpadding='1' cellspacing='0' width='100%'>
<?php if($num_rows > 0){?>
	<thead>
		<tr>
			<!-- <th width="2%">
				<?php echo xlt('S.N.'); ?>
			</th>-->
			<th width="23%">
				<a href="#" onclick="return dosort('invuom_name')"
				<?php if ($form_orderby == "invuom_name") echo " style=\"color:#00cc00\""; ?>>
				<?php echo xlt('Name'); ?> </a>
			</th>
			<th width="47%">
				<?php echo xlt('Description'); ?>
			</th>
			<th width="12%">
				<a href="#" onclick="return dosort('invuom_status')"
				<?php if ($form_orderby == "invuom_status") echo " style=\"color:#00cc00\""; ?>>
				<?php echo xlt('Status'); ?> </a>
			</th>  
			<th width="15%" class="newtextc">
				<?php echo xlt('Action'); ?>
			</th>  
		</tr>
	</thead>
	<?php } ?>
<?php 
 $lastid = "";
 $encount = 0;
 if(sqlNumRows($res)){
	 while ($row = sqlFetchArray($res)) {  
	   ++$encount;
	   $bgcolor = "#" . (($encount & 1) ?  "f7d9ca" : "fdede5");
	   $lastid = $row['invuom_id'];
	   if($row['invuom_deleted'] == '1'){
			echo " <tr class='detail strikeThrough' bgcolor='$bgcolor'>\n";
		}else {
	   		echo " <tr class='detail' bgcolor='$bgcolor'>\n";
	   }
	   //echo "  <td>" .$encount. "</td>\n";
	   echo "  <td>" .	    
	    text($row['invuom_name']) . "</td>\n";
	   echo "  <td>" . text($row['invuom_desc']) . "</td>\n";
	   echo "  <td>" . ($row['invuom_status'] ? xlt('Active') : xlt('Not Active')) . "</td>\n";
	   echo "  <td class='newtextc newvm'>";
 	   if($row['invuom_deleted'] == '0'){
 	   		if(acl_check('inventory', 'im_uom_edit')){ echo "<a class='iframe iconanchor' href='add_edit_uom.php?uomid=".attr($lastid)."' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a> &nbsp;";}
 	   		//if(acl_check('inventory', 'im_uom_del')){ echo "<a class='iconanchor' href='uom.php?mode=delete&uomid=".attr($lastid)."' onclick='return confirm(\"Are you sure to delete?\"); top.restoreSession()' title='Delete'><span class=' glyphicon glyphicon-trash'></span></a> &nbsp; ";}
 	   		if(acl_check('inventory', 'im_uom_del')){ echo "<a class='iconanchor' href='#' onclick='return confirmation(\"uom.php?mode=delete&uomid=".attr($lastid)."\"); top.restoreSession()' title='Delete'><span class=' glyphicon glyphicon-trash'></span></a> &nbsp; ";}
 	   		
 	   		if($row['invuom_status'] == 0){
 	   			//if(acl_check('inventory', 'im_uom_edit')){ echo "<a class='iconanchor' href='uom.php?mode=activate&uomid=".attr($lastid)."' onclick='return confirm(\"Are you sure to activate?\"); top.restoreSession()' title='Activate'><span class=' glyphicon glyphicon-ok'></span></a>";}
 	   		if(acl_check('inventory', 'im_uom_edit')){ echo "<a class='iconanchor' href='#' onclick='return confirmation2(\"uom.php?mode=activate&uomid=".attr($lastid)."\"); top.restoreSession()' title='Activate'><span class=' newactivate'></span></a>";}
 	   		
 	   		}else {
 	   			//if(acl_check('inventory', 'im_uom_edit')){echo "<a class='iconanchor' href='uom.php?mode=deactivate&uomid=".attr($lastid)."' onclick='return confirm(\"Are you sure to deactivate?\"); top.restoreSession()' title='Deactivate'><span class=' glyphicon glyphicon-remove'></span></a>";}
 	   		if(acl_check('inventory', 'im_uom_edit')){echo "<a class='iconanchor' href='#' onclick='return confirmation3(\"uom.php?mode=deactivate&uomid=".attr($lastid)."\"); top.restoreSession()' title='Dectivate'><span class=' newdeactivate'></span></a>";}
 	   		
 	   		} 	   		
 	   }
 	   
		echo "</td>\n";	     
	  	echo " </tr>\n";
	 } // end while
 }else{
	?>
	<tr>
		<td colspan="5" style="text-align:center; font-size:10pt;">No Unit Of Measurement Found</td>
	</tr>
	<?php 
 } // end If
?>
</table>
<input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />

</form>
<?php // Pagination Displaying Section?>
<table>
	<tbody>
		<tr>
			<td class="text">
				<?php if($num_rows > 0) {?>
				<span class='paging'>
				<span class="pagingInfo">Total Records: <?php echo $num_rows;?></span> Page :
				<?php
					if($prev_page) {
						echo " <span class='prev' style='margin:0 4px 0 5px'><a href='$_SERVER[SCRIPT_NAME]?page=$prev_page&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'><< Prev</a></span> ";
					}

					if($num_rows > 0){
						for($i=1; $i<=$num_pages; $i++){
							if($i != $page)
							{
								echo "<span><a href='$_SERVER[SCRIPT_NAME]?page=$i&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'>$i</a></span>";
							}
							else
							{
								echo "<span class='current'>$i</span>";
							}
						}
					}
					if($page!=$num_pages){
						echo "<span class='next'><a href ='$_SERVER[SCRIPT_NAME]?page=$next_page&facility=$_REQUEST[facility]&form_item=$_REQUEST[form_item]&form_to_date=$_REQUEST[form_to_date]'>Next >></a></span>";
					} 
				?>
				</span>
				<?php } ?>
			</td>
		</tr>
	</tbody>
</table>
<!-- stuff for the popup calendar -->
       </div></div>
</body>
</html>
