<?php
// Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("oneivoryintg.php");


//echo "</pre>";
$alertmsg = '';
$stockid = $_REQUEST['stockid'];
$info_msg = "";
$tmpl_line_no = 0;
if (!$invgacl->acl_check('inventory','invf_stock_add','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));
        
// Format dollars for display.
//
	function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
	function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
    <?php include_once("themestyle.php"); ?>
    <?php include_once("scriptcommon.php"); ?>
    <script language="JavaScript">
        <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>     
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
    </head>
    <body>
        <?php
        //echo "=======>>>".$GLOBALS['stock_api'];
        // If we are saving, then save and close the window.
        // First check for duplicates.
        //
	
	if ($_POST['form_save']) {
            
            if ($_REQUEST['itemid'] && $_REQUEST['facility'] && $_REQUEST['form_qty'] && is_numeric($_REQUEST['form_qty'])) {
                
                $beforeQty = 0;
                $sresult = 0;
                //$message="Stock added to your facility successfully!";
                $queryString = array('invist_itemid' => $_REQUEST['itemid'], 'invist_clinic_id' => $_REQUEST['facility']);
                if ($_REQUEST['form_batch'] != '') {
                    $queryString['invist_batch'] = $_REQUEST['form_batch'];
                } elseif ($_REQUEST['form_expiryDate'] != '') {
                    $queryString['invist_expiry'] = $_REQUEST['form_expiryDate'];
                } else {
                    $queryString['invist_batch'] = $_REQUEST['form_batch'];
                    $queryString['invist_expiry'] = $_REQUEST['form_expiryDate'] == "" ? "0000-00-00" : $_REQUEST['form_expiryDate'];
                }
                //print_r($queryString);
                //die();
                try {
                    $pdoobject->begin_transaction();
                    $valdate = $pdoobject->fetch_multi_row("inv_item_stock", array("invist_quantity", "invist_id"), $queryString, " FOR UPDATE ");

                    if ($valdate) {
                        $beforeQty = $valdate['0']['invist_quantity'];
                        $stockid = $valdate['0']['invist_id'];
                        

                        $stockData = array(
                            'invist_quantity' => $beforeQty + escapedff('form_qty'),
                            'invist_expiry' => (isset($_POST['form_expiryDate']) && !empty($_POST['form_expiryDate']) ? escapedff('form_expiryDate') : '0000-00-00'),
                            'invist_price' => (!empty($_POST['form_price']) ? escapedff('form_price') : '0.00')
                        );
                        $where = array('invist_id' => $stockid);
                        $pdoobject->update("inv_item_stock", $stockData, $where);
                        $sresult= $stockid;
                    } else {
                        $stockData = array(
                            'invist_itemid' => $_POST['itemid'],
                            'invist_batch' => $_POST['form_batch'],
                            'invist_expiry' => (isset($_POST['form_expiryDate']) && !empty($_POST['form_expiryDate']) ? escapedff('form_expiryDate') : '0000-00-00'),
                            'invist_price' => (!empty($_POST['form_price']) ? escapedff('form_price') : '0.00'),
                            'invist_quantity' => $_POST['form_qty'],
                            'invist_clinic_id' => $_REQUEST['facility'],
                            'invist_createdby' => $_SESSION['authId'],
                            'invist_created_date' => date("Y-m-d H:i:s")
                        );
                        $sresult = $pdoobject->insert("inv_item_stock", $stockData);
                        $message = (!empty($sresult)) ? "<div class='alert alert-success alert-dismissable'>Stock added to your facility successfully!</div>" : "<div class='alert alert-danger alert-dismissable'>Error while adding stock!</div>";
                        $stockid=$sresult;
                        
                    }
                    $tranData = array(
                        'invistr_itemid' => $_POST['itemid'],
                        'invistr_batch' => $_POST['form_batch'],
                        'invistr_expiry' => (isset($_POST['form_expiryDate']) && !empty($_POST['form_expiryDate']) ? escapedff('form_expiryDate') : '0000-00-00'),
                        'invistr_price' => (!empty($_POST['form_price']) ? escapedff('form_price') : '0.00'),
                        'invistr_quantity' => $_POST['form_qty'],
                        'invistr_before_qty' => $beforeQty,
                        'invistr_after_qty' => $beforeQty + $_POST['form_qty'],
                        'invistr_clinic_id' => $_POST['facility'],
                        'invistr_tran_type' => 1,
                        'invistr_createdby' => $_SESSION['authId'],
                        'invistr_created_date' => date("Y-m-d H:i:s"),
                        'invistr_action'=> 'inventory/add_edit_stock',
                        'invistr_action_id'=> $sresult
                    );
                    $tresult = $pdoobject->insert("inv_item_stock_transaction", $tranData);
                    $pdoobject->commit();
               
                    if ($GLOBALS['stock_api']) {
                        //if ($_REQUEST['facility'] == '35') {
                                 $type=getFacilityType($_REQUEST['facility']);
                        if($type==2){
                            oneivoryintg::save(array('module_name' => 'catalog/stock', 'endpoint' => 'apilogin_apiupdatestock.update', 'action_id' => $stockid, 'request' => json_encode(array($data = array('product_id' => $_REQUEST['OneivoryId'], 'qty' => $_REQUEST['form_qty'], 'is_in_stock' => '1', 'adjustment' => 'In')))));
                        }
                        //}
                    }else{
                        if(getItemDetail($_REQUEST['itemid']) && $GLOBALS['stock_api_exp']){
                                 $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                            oneivoryintg::save(array('module_name' => 'catalog/stock', 'endpoint' => 'apilogin_apiupdatestock.update', 'action_id' => $stockid, 'request' => json_encode(array($data = array('product_id' => $_REQUEST['OneivoryId'], 'qty' => $_REQUEST['form_qty'], 'is_in_stock' => '1', 'adjustment' => 'In')))));
            }
                        }
                    }
           
                } catch (PDOException $e) {
                    $pdoobject->rollback();
                    echo $error = $e->getMessage();
                    $message = "<div class='alert alert-danger alert-dismissable'>Error in saving data!</div>";
                }
            } else {
                $message = "<div class='alert alert-danger alert-dismissable'>Stock adding is not successfull!</div>";
            }

            $_SESSION['INV_MESSAGE'] = $message;
            echo "<script language='javascript'>\n";
            echo "parent.location.reload();\n";
            echo "</script></body></html>\n";
            exit();
        }

        /*
          if ($stockid) {
          $row = sqlQuery("SELECT `invist_id`, `invist_itemid`, `invist_batch`, `invist_expiry`, `invist_price`, `invist_quantity`, `invist_clinic_id`, invist_deleted, invist_createdby, inv_im_catId, inv_im_subcatId, inv_im_isExpiry, inv_im_sale FROM inv_item_stock AS st INNER JOIN inv_item_master AS im ON im.inv_im_id = st.invist_itemid  WHERE invist_id = ?", array($stockid));
          }else {
          $row = array(
          'inv_im_catId' => 0,
          'inv_im_subcatId' => 0,
          `invist_itemid` => 0,
          `invist_batch` => '',
          `invist_expiry` => '',
          `invist_price` => 0.00,
          `invist_quantity` => '',
          `invist_clinic_id` => 0,
          'inv_im_isExpiry' => 0,
          'inv_im_sale' => 0,
          'invist_clinic_id' => 0
          );
          } */
        ?>
        <div class="infopop"><?php xl('Add Item Stock', 'e'); ?></div>
        <!-- tableData -->
        <div id='popUpformWrap'>
            <form class='nomrgn' id='dataSubmit' method='post' name='theform' action='add_edit_stock.php?stockid=<?php echo $stockid; ?>'>
                <!-- Row 1 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="date-wrp ui-input-text" for="facility">Facility:<sup class="required">*</sup></label>
                            <?php
                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                            InvUsersFacilityDropdown('facility', '', 'facility', $facility, $_SESSION['authId'],$userFacilityRestrict,$pdoobject, 0);
                            ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group posrel">
                            <label class="date-wrp" for="itemStock">Item:<sup class="required">*</sup></label>
                            <input type="text" class="formEle" id="itemStock" name="itemStock" readonly>
                            <img id="loading" style="display:none" src="../../images/loader.gif" />
                            <input type="hidden" class="formEle" id="itemId" name="itemid">
                            <input type="hidden" value="0" id="form_sale" name="form_sale">
                            <input type="hidden" value="0" id="form_expiry" name="form_expiry">
                            <input type="hidden" value="0" id="OneivoryId" name="OneivoryId">
                            <input type="hidden" value="0" id="StockeditID" name="StockeditID">                           
                            
                        </div>
                    </div>
                </div>
                <!-- Row 1 -->

                <!-- Row 2 -->
                <!-- <div class="row">
                        <div class="col-sm-6">
                                <div class="form-group">
                                <label class="date-wrp" for="catName">Category:</label>
                                <input type="text" class="formEle" disabled name="catName" id="catName" readonly>
                                <input type="hidden" class="formEle" name="catId" id="catId" >
                                <span></span>
                                </div>
                        </div>
        
                        <div class="col-sm-6">
                                <div class="form-group">
                                <label class="date-wrp" for="subCatName">Sub Category:</label>
                                <input type="text" class="formEle" disabled name="subCatName" id="subCatName" readonly>
                                <input type="hidden" class="formEle" name="itemSubCatField" id="itemSubCatField" >
                                <span></span>
                                </div>
                        </div>
                </div> -->
                <!-- Row 2 -->

                <!-- Row 4 -->
                <div class="rowhalf" style="display:block">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="date-wrp" for="form_batch">Batch#:</label>
                            <input type='text' size='30' name='form_batch' id='form_batch' maxlength='30' value='<?php echo attr($row['invist_batch']) ?>' class='formEle' />
                        </div>
                    </div>
                </div>
                <!-- Row 4 -->

                <!-- Row 5 -->
                <div class="rowhalf">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="date-wrp" for="form_qty">Quantity:<sup class="required">*</sup></label>
                            <input type='number' size='10' name='form_qty' id='form_qty' maxlength='7' onkeypress="return isNumber(event);" value='<?php echo attr($row['invist_quantity']) ?>' class='formEle' />
                        </div>
                    </div>
                </div>
                <!-- Row 5 -->

                <!-- Row 3 -->
                <div class="rowhalf" id="expiryrow" style="display: none;">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="date-wrp" for="form_expiryDate">Expiry:<sup class="required">*</sup></label> 
                            <input type='text' placeholder='Expiry Date' name='form_exDate' id="form_exDate" size='10' title='' />
                            <input type='hidden' name='form_expiryDate' id="form_expiryDate" size='10' value='<?php echo attr($row['invist_expiry']) ?>' />
                            <script language="JavaScript">
                                //  var today = new Date();
                                //  todayDate = today.getDate();
                                //  todayMonth = today.getMonth();
                                //  todayYear = today.getFullYear();

                                //  dateStr = '';
                                //  todayDate = (todayDate.toString()).length > 1 ? todayDate : '0' + todayDate;
                                //  //Setting Current month
                                //  todayMonth = parseInt(todayMonth)+1;
                                //  todayMonth = (todayMonth.toString()).length > 1 ? todayMonth : '0' + todayMonth;

                                //  //console.log(todayYear);
                                //  //console.log(todayMonth);
                                //  //console.log(todayDate);

                                //  dateStr = todayYear.toString() + todayMonth.toString() + todayDate.toString();
                                //  //console.log(dateStr);
                                //  dateStr = parseInt(dateStr);

                                //  // Calendar.setup({
                                //  // trigger    : "img_expiration",
                                //  // inputField : "form_expiryDate",
                                //  // dateFormat: "%Y-%m-%d",
                                //  // min:dateStr,
                                //  // onSelect   : function() { this.hide() },
                                //  // align: 'Bl'
                                //  // });
                                //  <//?php
                                //  if ($alertmsg) {
                                //  echo "alert('" . htmlentities($alertmsg) . "');\n";
                                //  }
                                //  ?>
                            </script>
                        </div>
                    </div>
                </div>
                <!-- Row 3 -->

                <!-- Row 6 -->
                <div class="rowhalf" id="salerow" style="display:none">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="date-wrp" for="form_price">Sale Price:<sup class="required">*</sup></label>
                            <input type='text' name='form_price' id='form_price' maxlength='10' value='<?php echo attr($row['invist_price']) ?>' class='formEle' />
                        </div>
                    </div>
                </div>
                <!-- Row 6 -->
                <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                <input type='submit' name='form_save' id='form_save' class="btn btn-warning btn-sm dnone" value='<?php echo xla('Save'); ?>' />
            </form>
            <div class='stockmessage'>
                <div id="placeholdermessage"></div>
                <div class='stockchkbox'>
                    <label for="checkconfirm" class="interButt"></label>
                    <input type='checkbox' class="custom" name='checkconfirm' id="checkconfirm" />
                </div>
            </div>
            </div>
        <!-- tableData -->
        <div id="placeholder">
            <img src='<?php echo $inventry_url; ?>emr/images/oi-wm-logo.jpg' class='autohgt' alt='' />
        </div>
    </body>
</html>
