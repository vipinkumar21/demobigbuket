<?php
 /**
*
* Upload Category CSV : (Swati Jain)
*
**/

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 
 if(!strpos($_SERVER['HTTP_REFERER'],"upload_csv_process")){	// Reset CSV uploaded msg display
 	$_SESSION['msg'] = '';
 }
 if ($_POST['form_csv_sample'] && $_POST['form_csv_sample']=='sample') {	// Sample CSV Format
 	header("Pragma: public");
 	header("Expires: 0");
 	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 	header("Content-Type: application/force-download");
 	header("Content-Disposition: attachment; filename=category.csv");
 	header("Content-Description: File Transfer");
 		echo xl('Category Name') . ',';
 		echo '"' . xl('Description') . '"' . "\n";
 		echo '"Lab Consumables",';
 		echo '"Lab Consumables Description",'."\n";
 		echo '"Housekeeping	",';
 		echo '"Housekeeping Description",'."\n";
 		exit;
 }
?>
<html>
<head>
<?php html_header_show();?>
<title><?php xl('Category CSV Upload','e'); ?></title>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script> 
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script type="text/javascript">
// On Category CSV upload
function submitCategoryCsv() {	
	
	if (document.forms['categoryInsert'].category_file.value.length>0) {
		top.restoreSession();
		document.forms['categoryInsert'].submit();
	} else {
		if (document.forms['categoryInsert'].category_file.value.length<=0)
		{
			document.forms['categoryInsert'].category_file.style.backgroundColor="red";
			//alert("<?php xl('Required field missing: Please browse the Category file','e');?>");
			 jAlert ('Required field missing: Please browse the Category file', 'Alert');
			document.forms['categoryInsert'].category_file.focus();
			return false;
		}		
	}
}
</script>

<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
 <link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<style type="text/css">

/* specifically include & exclude from printing */
@media print {
    #report_parameters {
        visibility: hidden;
        display: none;
    }
    #report_parameters_daterange {
        visibility: visible;
        display: inline;
		margin-bottom: 10px;
    }
    #report_results table {
       margin-top: 0px;
    }
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
	#report_results {
		width: 100%;
	}
}

</style>

</head>
<body class="body_top">
<div class="panel panel-warning">
    <div class="panel-heading">  <?php xl('Import','e'); ?> - <?php xl('Category CSV','e'); ?></div>

 <div class="panel-body">
<form name='categoryInsert' id='categoryInsert' method='post' enctype='multipart/form-data' action='upload_csv_process.php'>
<input type='hidden' name='mode' id='mode' value='import_category'/>

<table width="100%">
 <tr>
  <td width='60%'>
      <div class="row">
            <div class="form_left col-xs-4 topnopad botnopad">

            	<div class="form-group botnomrg"> 
	            	<?php xl('Category','e'); ?>:
	            	<input type="file" name="category_file" style="display:inline-block;">
            	</div> 
            </div>

            <div class="form_left col-xs-4 topnopad botnopad" id="mergePatientDataButton">
            	<div class="form-group botnomrg"> 
					<a href='#' class='btn btn-warning btn-sm' onclick='return submitCategoryCsv();'>
						<span><?php xl('Upload Category','e'); ?> </span>
					</a>
				</div> 
			</div>
	</div>
  </td>
   </tr>
 </table>

</form>
 <!-- end of parameters -->
<form name='categorycsv' id='categorycsv' method='post' enctype='multipart/form-data' action='upload_category.php'>
<input type='hidden' name='form_csv_sample' id='form_csv_sample' value=''/>
<div class="row">
    <div class="form_left col-xs-4 topnopad botnopad">Please Find Sample CSV Format</div>
    <div class="form_left col-xs-4 topnopad botnopad">
        <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csv_sample").attr("value","sample");$("#categorycsv").submit();'>
<span><?php xl('Sample CSV','e'); ?></span>
</a>
    </div>
    <div col-xs-2><b><?php echo $_SESSION['msg'];?></b></div>
</div>

</form>
</div>