<?php
	// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
	//
	// This program is for PRM software.
	$sanitize_all_escapes  = true;
	$fake_register_globals = false;

	require_once("../globals.php");
        require_once("./lib/database.php");
	require_once("$srcdir/acl.inc");
	require_once("drugs.inc.php");
	require_once("$srcdir/options.inc.php");
	require_once("$srcdir/formdata.inc.php");
	require_once("$srcdir/htmlspecialchars.inc.php");
	//$datePhpFormat = getDateDisplayFormat(0);
	$alertmsg = '';
	$issueid = $_REQUEST['issueid'];
	$facility = $_REQUEST['facility'];
	$info_msg = "";
	$tmpl_line_no = 0;

	if (!acl_check('inventory', 'invf_reqout_view')) die(xlt('Not authorized'));

	if(empty($issueid)){
		echo "<script language='JavaScript'>\n";
		echo " alert(You have not selected issue.);\n";
		echo " if (opener.refreshme) opener.refreshme();\n";
		echo " window.close();\n";
		echo "</script></body></html>\n";
		exit();
	}
	// Format dollars for display.
	//
	function bucks($amount) {
	if ($amount) {
		$amount = sprintf("%.2f", $amount);
		if ($amount != 0.00) return $amount;
	}
		return '';
	}
	// Translation for form fields used in SQL queries.
	//
	function escapedff($name) {
		return add_escape_custom(trim($_POST[$name]));
	}
	function numericff($name) {
		$field = trim($_POST[$name]) + 0;
		return add_escape_custom($field);
	}
?>
<html>
<head>
	<?php html_header_show(); ?>
	<title><?php echo $reqid ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock Requisition'); ?></title>
    <?php include_once("themestyle.php"); ?>
    <?php include_once("scriptcommon.php"); ?>
	<script language="JavaScript">
		<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
	</script>
</head>
<body class="body_top">
<?php
	$sql = "SELECT iisn.iin_id, iisn.iin_reqid, istreq.isr_number, iisn.iin_from_clinic, iisn.iin_to_clinic, iisn.iin_number, iisn.iin_status, iisn.iin_isdeleted, iisn.iin_createdby, iisn.iin_date, frf.name AS fromFacility, tof.name AS toFacility " .
		"FROM inv_issue_notes AS iisn INNER JOIN inv_stock_requisition AS istreq ON istreq.isr_id = iisn.iin_reqid
		INNER JOIN facility AS frf ON frf.id = iisn.iin_from_clinic
		INNER JOIN facility AS tof ON tof.id = iisn.iin_to_clinic " .
		"WHERE iisn.iin_isdeleted = '0' AND iisn.iin_id = ? AND iisn.iin_from_clinic = ?";
        $res = $pdoobject->custom_query($sql, array($issueid, $facility));
        $row =$res[0];
?>
<!-- page -->
<div data-role="page" class="ui-page ui-body-c ui-page-active">
	<div data-role="content" class="ui-content" role="main">
		<div class="infopop"><?php  xl('View in Requisition','e'); ?></div>
		<div class="popupTableWrp mt-0">
			<form method='post' id='theform' name='theform' >
				<table cellpadding='0' cellspacing='0' border='0' class='popupTable ui-table' width='100%'>
					<thead>
						<tr>
							<th width="20%"><?php echo xlt('From Facility'); ?></th>
							<th width="20%"><?php echo xlt('To Facility'); ?></th>
							<th width="20%"><?php echo xlt('Issue'); ?></th>
							<th width="20%"><?php echo xlt('Requisition'); ?></th>
							<th width="20%"><?php echo xlt('Issue Status'); ?></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><span><?php echo $row['toFacility'];?></span> </td>
							<td><span><?php echo $row['fromFacility'];?></span></td>
							<td><span><?php echo $row['iin_number'];?></span></td>
							<td><span><?php echo $row['isr_number'];?></span></td>
							<td><?php if($row['iin_status'] == 1){echo 'Completed';} else {echo 'In Progress';} ?></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<br />
		<div class="popupTableWrp mt-0">
			<form method='post' id='requisitionListForm' name='requisitionListForm' action='add_edit_requisition.php?reqid=<?php echo $reqid; ?>'>
				<div id="requisitionListContainer">
					<table id='requisitionList' cellspacing='0' class="popupTable ui-table">
						<thead>
							<tr>
								<!-- <th width='12%'>Category</th>
								<th width='12%'>Sub-Category</th> -->
								<th width='40%'>Item Name</th>
								<th width='12%'>Item Code</th>
								<th width='12%'>Batch</th>
								<th width='12%'>Expiry</th>
								<th width='12%'>Price</th>
								<th width='12%'>Quantity</th>					
							</tr>
						</thead>
						<tbody>
						<?php 
						
						$sql2 = "SELECT irit.iii_itemid, irit.iii_issueid, irit.iii_quantity, irit.iii_isdeleted, im.inv_im_name, im.inv_im_code, iist.invist_batch, iist.invist_expiry, iist.invist_price FROM inv_issue_item AS irit 
                                                                    INNER JOIN inv_item_stock AS iist ON iist.invist_id = irit.iii_stock_id 
                                                                    INNER JOIN inv_item_master AS im ON im.inv_im_id = irit.iii_itemid 
                                                                      
                                                                    WHERE irit.iii_issueid = ?";
                                                $res2 = $pdoobject->custom_query($sql2, array($issueid));
						if($pdoobject->custom_query($sql2, array($issueid),1)){
							foreach($res2 as $itemrow) {
						?>
							<tr <?php if($itemrow['iii_isdeleted'] == 1){echo 'class="strikeThrough"';}?>>				
							<!-- <td>
								<?php// echo $itemrow['invcat_name'];?>					
							</td>
							<td>
								<?php //if(!empty($itemrow['invsubcat_name'])){ echo $itemrow['invsubcat_name'];}else{echo 'NA';}?>					
							</td> -->
							<td>
								<?php echo $itemrow['inv_im_name'];?>					
							</td>
							<td>
								<?php echo $itemrow['inv_im_code'];?>					
							</td>
							<td>
								<?php echo $itemrow['invist_batch'];?>					
							</td>
							<td>
								<?php echo invDateFormat($itemrow['invist_expiry']);?>					
							</td>
							<td>
								<?php echo $itemrow['invist_price'];?>					
							</td>
							<td>
								<?php echo $itemrow['iii_quantity'];?>					
							</td>
							
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- page -->
<script language="JavaScript">
	<?php
		if ($alertmsg) {
			echo "alert('" . htmlentities($alertmsg) . "');\n";
		}
	?>
</script>
</body>
</html>