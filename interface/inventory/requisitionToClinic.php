<?php

// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("./lib/inv.users.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
$thisauth = $invgacl->acl_check('inventory', 'invf_reqin_list','users',$_SESSION['authUser']);
if (!$thisauth)
    die(xlt('Not authorized'));
// For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
    'invtran_id' => 'invtran_id DESC'
);

$usergd = getUserGroup('value');

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[urldecode($_REQUEST['form_orderby'])] ? urldecode($_REQUEST['form_orderby']) : 'invtran_id';
$orderby = $ORDERHASH[$form_orderby];
//$facility  = $_POST['facility'];

if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
}else{
   $_REQUEST['facility']= $_SESSION['cid'];
}

$facility = isset($_REQUEST['facility'])? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$from_date = urldecode($_REQUEST['form_from_date']);
$to_date = urldecode($_REQUEST['form_to_date']);
$transaction_id = urldecode($_REQUEST['form_transaction_id']);
$requistion_status = urldecode($_REQUEST['form_requistion_status']);

// get drugs
/* echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM invcategories AS cat " .
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby"; */
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Shipment'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body>
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <h1><?php xl('Shipment', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='requisitionToClinic.php'  name='theform' id='theform' class="botnomrg">
                            <!-- formPart -->
                            <div class="filterWrapper">
                                <!-- first column starts -->
                                <div class="ui-block">
                                    <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('facility', '', 'facility', $facility, $_SESSION['authId'], $userFacilityRestrict,$pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends -->
                                <!-- fourth column starts -->
                                <div class="ui-block">
                                    <input type='text' placeholder='Requisition Id' name='form_transaction_id' id="form_transaction_id" value='<?php echo $transaction_id ?>' title='' />
                                </div>
                                <!-- fourth column ends -->
                                <!-- fifth column starts -->
                                <div class="ui-block">
                                    <select class='form-control input-sm' name="form_requistion_status" id="form_requistion_status" >
                                        <option value="" <?php echo  $selected = ( $requistion_status == "" ) ? 'selected="selected"' : '';?>>All Status</option>
                                        <option value="3" <?php echo $selected = ( $requistion_status == "3" ) ? 'selected="selected"' : '';?>>Cancelled</option>
                                        <option value="2" <?php echo $selected = ( $requistion_status == "2" ) ? 'selected="selected"' : '';?>>Completed</option>
                                        <option value="0" <?php echo $selected = ( $requistion_status == "0" ) ? 'selected="selected"' : '';?>>In Progress</option>
                                        <option value="1" <?php echo $selected = ( $requistion_status == "1" ) ? 'selected="selected"' : '';?>>Partially Completed</option>
                                    </select>
                                </div>
                                <!-- fifth column ends -->
                                <!-- second column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                </div>
                                <!-- second column ends -->
                                <!-- third column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                </div>
                                <!-- third column ends -->
                                <!-- sixth column starts -->
                                <div class="ui-block wdth15">
                                   
                                    <a class="pull-right btn_bx" id='reset_form1' href="requisitionToClinic.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true"); $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- sixth column ends -->
                            </div>
                            <!-- formPart -->
                            <!-- tableData -->
                            <?php
                            //echo "----".$facility;
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_from_date'] || $_REQUEST['form_to_date'] || $_REQUEST['form_requistion_status'] || $_REQUEST['form_transaction_id'] || empty($facility)) {

                                $res = "SELECT istreq.isr_id, istreq.isr_magento_orderid, istreq.isr_number, istreq.isr_from_clinic,(SELECT nickname FROM users,inv_issue_notes WHERE inv_issue_notes.`iin_reqid`= istreq.`isr_id` AND inv_issue_notes.`iin_createdby` = users.`id` ORDER BY inv_issue_notes.iin_id DESC
LIMIT 1) AS name,  "
                                        . "istreq.isr_to_clinic, istreq.isr_isapproved, istreq.isr_status, istreq.isr_isdeleted, "
                                        . "isr_createdby, istreq.isr_created_date, frf.name AS fromFacility, tof.name AS toFacility " .
                                        "FROM inv_stock_requisition AS istreq
                                        INNER JOIN facility AS frf ON frf.id = istreq.isr_from_clinic  INNER JOIN facility AS tof ON tof.id = istreq.isr_to_clinic " .
                                        "WHERE istreq.isr_isapproved IN(1,3) AND istreq.isr_isdeleted = '0' ";



                                if (!empty($facility)) { // if facility exists

                                    $res .= " AND istreq.isr_to_clinic = '" . $facility . "'";
                                } else {

                                    $res .= " AND istreq.isr_to_clinic IN(" . getLoggedUserAssignedClinics() . ")";
                                }
                                if(empty($to_date) && !empty($from_date))
                                {

                                    $res .= " AND istreq.isr_created_date >= '$from_date  00:00:00' ";
                                }
                                if (!empty($from_date) && !empty($to_date)) { // If from dates exists

                                    $res .= " AND istreq.isr_created_date BETWEEN '$from_date 00:00:00' AND '$to_date  23:59:59'";
                                }
                                if (!empty($to_date) && empty($from_date)) { // If to dates exists

                                    $res .= " AND istreq.isr_created_date <= '$to_date  23:59:59' ";
                                }
                                if ($requistion_status == "0" || !empty($requistion_status)) { // If to dates exists

                                    $res .= " AND istreq.isr_status = '" . $requistion_status . "'";
                                }
                                if (!empty($transaction_id)) { // If to dates exists

                                    $res .= " AND istreq.isr_number like'%" . $transaction_id . "%'";
                                }
                                $res .= " ORDER BY istreq.isr_id DESC";
                                //echo $res;
                               // $sql_num = sqlStatement($res);
                                $num_rows = $pdoobject->custom_query($res,null,1); // total no. of rows
                                $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                $page = $_GET["page"];
                                $page = ($page == 0 ? 1 : $page);
                                $page_start = ($page - 1) * $per_page;
                                $curr_URL = $_SERVER['SCRIPT_NAME'] . '?' . 'form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&facility=' . $facility . '&form_from_date=' . urldecode($_REQUEST[form_from_date]) . '&form_to_date=' . urldecode($_REQUEST[form_to_date]) . '&form_transaction_id=' . urldecode($_REQUEST['form_transaction_id']) .  '&form_requistion_status=' . urldecode($_REQUEST['form_requistion_status']) .'&';
                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                $res .= " LIMIT $page_start , $per_page";
                                $result = $pdoobject->custom_query($res);
                                ?>
                                <div id='' class='tableWrp pb-2'>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                    <div class='dataTables_wrapper no-footer'>
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                            <?php if ($num_rows > 0) { ?>
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <?php echo xlt('Req. No'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo xlt('From Facility'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo xlt('To Facility'); ?>

                                                        </th>
                                                        <!-- <th>
                                                            <?php //echo xlt('Approval Status'); ?>
                                                        </th> -->
                                                        <th>
                                                            <?php echo xlt('Req. Status'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo xlt('Req. Date'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo xlt('Last Issue Date'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo xlt('Last Issue By'); ?>
                                                        </th>
                                                        <th width="15%">
                                                            <?php echo xlt('Action'); ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                            <?php } ?>
                                            <?php
                                            $lastid = "";
                                            $encount = 0;

                                            if (!empty($result)) {
                                                foreach ($result as $key => $row) {
                                                    ++$encount;
                                                    $bgcolor = "#" . (($encount & 1) ? "f7d4be" : "f1f1f1");
                                                    $lastid = $row['isr_id'];
                                                    $magorderid=$row['isr_magento_orderid'];
                                                    $rrdate = getReqReDate($lastid);

                                                    if ($row['isr_isdeleted'] == 1) {
                                                        echo "<tr class='detail strikeThrough'>\n";
                                                    } else {
                                                        echo "<tr class='detail' role='row'>\n";
                                                    }
                                                    //echo "  <td>" .$encount. "</td>\n";
                                                    echo "  <td>" . text($row['isr_number']) . "</td>\n";
                                                    echo "  <td>" . text($row['fromFacility']) . "</td>\n";
                                                    echo "  <td>" . text($row['toFacility']) . "</td>\n";
                                                    // echo "  <td>";
                                                    //     if ($row['isr_isapproved'] == 2) {
                                                    //         echo 'Rejected';
                                                    //     } else if ($row['isr_isapproved'] == 1) {
                                                    //         echo 'Approved';
                                                    //     } else if ($row['isr_isapproved'] == 3) {
                                                    //         echo 'Cancelled';
                                                    //     } else {
                                                    //         echo 'Waiting';
                                                    //     }
                                                    // echo "</td>\n";
                                                    echo "  <td>";

                                                    if ($row['isr_status'] == 2) {
                                                        echo 'Completed';
                                                    } else if ($row['isr_status'] == 1) {
                                                        echo 'Partially Completed';
                                                    } else if ($row['isr_status'] == 3) {
                                                        echo 'Cancelled';
                                                    } else {
                                                        echo 'In Progress';
                                                    }

                                                    echo "</td>\n";
                                                    echo "  <td>" . invDateFormat($row['isr_created_date']) . "</td>\n";
                                                    echo "  <td>";

                                                    if ($rrdate) {
                                                        echo invDateFormat($rrdate);
                                                    } else{
                                                        echo "---";
                                                    }
                                                    echo "</td>\n";
                                                    echo "  <td>".$row['name']."</td>\n";
                                                    echo "  <td>";

                                                    if ($invgacl->acl_check('inventory', 'invf_reqin_view','users',$_SESSION['authUser'])) {
                                                        echo "<div class='listbtn viewTableDataList' data-pageUrl='view_requisition_in.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_to_clinic']) . "'><span class='dashboardViewIcon'></span>View</div> &nbsp; ";
                                                    }

 if($invfac->validateFacilityId($row['isr_to_clinic'])){
                                                    if ($row['isr_status'] != 2 && $row['isr_status'] != 3) {
                                                        if ($invgacl->acl_check('inventory', 'invf_reqin_issue_add','users',$_SESSION['authUser'])) {
                                                            echo "<div class='listbtn addEditTableData' data-pageUrl='req_item_issue.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_to_clinic']). "&magorderid=" . attr($magorderid) . "'><span class='dashboardEditIcon'></span>Issue</div> &nbsp; ";
                                                        }
                                                    }
                                                    if (($row['isr_status'] == 1 || $row['isr_status'] == 2) && $row['isr_status'] != 3) {
                                                        if ($invgacl->acl_check('inventory', 'invf_issueout_list','users',$_SESSION['authUser'])) {
                                                            echo "<a class='listbtn' data-ajax='false' href='issueNotesFromClinic.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_to_clinic']) . "' title='Issued'><span class='dashboardIssuedIcon'></span>Issued</a> &nbsp; ";
                                                        }
                                                    }
                                                    if (($row['isr_status'] != 2 && $row['isr_status'] != 1 && $row['isr_status'] != 3) && $usergd['0'] == 'admin') {
                                                        echo "<div style='width:38px;' class='listbtn addEditTableData' data-pageUrl='req_item_cancel.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_to_clinic']). "&magorderid=" . attr($magorderid) . "'><span class='dashboardDeleteIcon'></span>Cancel</div> &nbsp; ";
                                                    }
                                                }
                                                    echo "</td>\n";
                                                    echo " </tr>\n";
                                                } // end while
                                            } else {
                                                ?>

                                                <div style="display: block;" id="dailynorecord">No Records Found!</div>

                                                    <?php
                                                } // end If
                                                ?>
                                        </table>
                                    </div>
                                    <!-- pagination -->
                                        <?php if ($num_rows > 0) { ?>
                                            <?php echo $pa_data; ?>
                                        <?php } ?>
                                    <!-- pagination -->
                                </div>
                                <?php } else { ?>
                                <div class='text leftmrg6 dnone'><?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                                </div>
                                <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            <!-- tableData -->
                        </form>
                        <!-- mdleCont -->
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
        <!-- page -->
    </body>
</html>