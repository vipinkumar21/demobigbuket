<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.

require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");


// Fetch the Provide based on facility (Clinic ID)
function restrict_user_facility($facilList) {
    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
    $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
    $sql .= " WHERE ";
    if (isset($facilList) && $facilList !== '') {
        $sql .= "uf.facility_id = $facilList ";
    } else {
        $fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = " . $userid . " AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
        $fRes = sqlStatement($fsql,null, $GLOBALS['adodb']['dbreport']);
        $facilityIds = '';
        while ($fRow = sqlFetchArray($fRes)) {
            if (!empty($facilityIds)) {
                $facilityIds .= ', ' . $fRow['facility_id'];
            } else {
                $facilityIds = $fRow['facility_id'];
            }
        }
        $sql .= "uf.facility_id IN (" . $facilityIds . ") ";
    }
    $sql .= "AND gag.id IN (12, 13, 18) ";
    return $sql .= " GROUP BY u.id DESC";
    //echo $sql;
    //$ures = sqlStatement($sql);
}

function non_restrict_user_facility($facilList) {
    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename ";
    $sql .= "FROM users AS u  ";
    $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
    return $sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";
    
}

// Check authorization.
$thisauth = acl_check('inventory', 'icr_stock_leadger');
if (!$thisauth)
    die(xlt('Not authorized'));

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$searchParam = '';

$form_facility = $_POST['form_facility'];
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
$provider = $_POST['form_provider'];
$transactionId = $_POST['form_transaction_type'];
$sellable = $_POST['form_sellable'];
$zero_quantity_items = $_POST['zero_quantity_items'];


$show_available_times = false;



// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockLedger') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=stock_leadger.csv");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>

        <head>
            <?php html_header_show(); ?>

            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">

            <title><?php xl('Stock Leadger', 'e'); ?></title>

            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
            <link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

            <script type="text/javascript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

                function dosort(orderby) {
                    var f = document.forms[0];
                    f.form_orderby.value = orderby;
                    f.submit();
                    return false;
                }

                function oldEvt(eventid) {
                    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
                }

                function refreshme() {
                    document.forms[0].submit();
                }


                $(document).ready(function () {
                    var facilityID = $("#form_facility").val();
                    if(facilityID != '' ){
                    var providerID =$('#form_provider').val();
                    $.ajax({
                            url: "get_facility_provider.php?facilityId=" + facilityID,
                            success: function (result) {
                                $("#form_provider").html(result);
                                 $('#form_provider').val(providerID);
                            }
                        });

                    } 
                    
                    $("#form_facility").change(function () {
                        var facilityID = $("#form_facility").val();
                        $.ajax({
                            url: "get_facility_provider.php?facilityId=" + facilityID,
                            success: function (result) {
                                $("#form_provider").html(result);
                            }
                        });
                    });
                });
                function exportCSV() {
                    //this.document.frm.act.value="show_home";
                    document.forms[0].form_csvexport.value = "csvStockLedger";
                    //this.document.frm.action="index.php"
                    //this.document.frm.submit();
                }
                function viewStockLedger() {
                    //this.document.frm.act.value="show_home";
                    document.forms[0].form_csvexport.value = "viewStockLedger";
                    //this.document.frm.action="index.php"
                    //this.document.frm.submit();
                }
            </script>

            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">
<div class="panel panel-warning">
   
            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
             <div class="panel-heading"><?php xl('Report', 'e'); ?> - <?php xl('Stock Ledger', 'e'); ?></div>
              <div class="panel-body">
            <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form method='post' name='theform' id='theform' action='stock_leadger_report_clinic.php'>
                <div id="report_parameters">
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value='viewStockLegder'/>
                    <input type="hidden" name="default_facility" value="<?php echo $form_facility ?>" />
                    <input type="hidden" name="default_provider" value="<?php echo $provider ?>" />
                   <div class='searchReport'>
                                    <div class='row'>
                                          <div class="col-xs-6 col-sm-4">
                                              <div class="form-group">
                                             <?php xl('Facility', 'e'); ?>:
                                             <select name="form_facility" id="form_facility" class="form-control input-sm" >
                                          <?php 
                                          if(empty($row['invist_clinic_id'])){
                                              ?>
                                              <option value='0' selected="selected">Select Facility</option>
                                              <?php
                                          }
                                          $countUserFacilities = 0;
                                          if (!$GLOBALS['restrict_user_facility']) {
                                              $qsql = sqlStatement("
                                                 select id, name, color
                                                 from facility
                                                 where service_location != 0
                                                 ");
                                          } else {
                                            $qsql = sqlStatement("
                                               select uf.facility_id as id, f.name, f.color
                                               from users_facility uf
                                               left join facility f on (uf.facility_id = f.id)
                                               where uf.tablename='users' 
                                               and uf.table_id = ? 
                                               ", array($_SESSION['authId']) );
                                        }      
                                        while ($facrow = sqlFetchArray($qsql)) {       
                                          $selected = ( $facrow['id'] == $form_facility ) ? 'selected="selected"' : '' ;
                                          echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";       
                                          $countUserFacilities++;
                                          /************************************************************/
                                      }      
                                      ?>      
                                </select>
                                           <?php //dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                                          </div>
                                          </div>
                                        <div class="col-xs-6 col-sm-4">
                                                 <?php xl('Provider', 'e'); ?>:
                                        <?php
                                                if (empty($facility)) {
                                                    $query = "SELECT id, lname, fname FROM users WHERE " . "authorized = 1 $provider_facility_filter ORDER BY lname, fname";
                                                    $ures = sqlStatement($query,null, $GLOBALS['adodb']['dbreport']);
                                                    echo "   <select class='form-control input-sm' name='form_provider' id='form_provider'>\n";
                                                    echo "    <option value=''>-- " . xl('All') . " --</option>";
                                                    while ($urow = sqlFetchArray($ures)) {
                                                        $provid = $urow['id'];
                                                        echo "    <option value='$provid'";
                                                        if ($provid == $provider)
                                                            echo " selected";
                                                        echo ">" . $urow['fname'] . " " . $urow['lname'] . "</option>";
                                                    }
                                                    echo "   </select>\n";
                                                } else {

                                                    if ($GLOBALS['restrict_user_facility']) {
                                                        $sql = restrict_user_facility($facility);
                                                        $ures = sqlStatement($sql,null, $GLOBALS['adodb']['dbreport']);
                                                    } else {
                                                        $sql = non_restrict_user_facility($facility);
                                                        $ures = sqlStatement($sql,null, $GLOBALS['adodb']['dbreport']);
                                                    }

                                                    $returnData = '';
                                                    // default to the currently logged-in user
                                                    $defaultProvider = $_SESSION['authUserID'];
                                                    $returnData.= "<option value='' selected='selected'>Select Provider</option>";
                                                    while ($urow = sqlFetchArray($ures)) {
                                                        $returnData.= "<option value='" . attr($urow['id']) . "'";
                                                        if ($urow['id'] == $provider)
                                                            $returnData.= " selected";
                                                        $returnData.= ">" . text($urow['completename']);
                                                        //$returnData.= ">" . text($urow['fname']);
                                                        //if ($urow['lname']) $returnData.= " " . text($urow['lname']);
                                                        $returnData.= "</option>";
                                                    }
                                                    echo "   <select name='form_provider' id='form_provider'>\n";
                                                    echo $returnData;
                                                    echo "   </select>\n";
                                                }
                                                ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-4">
                                            <div class="form-group">
                                            <?php xl('Transaction Type', 'e'); ?>:
                                        <?php
                                                $query = "SELECT invtrt_id, invtrt_name FROM inv_transaction_type ORDER BY invtrt_name ";
                                                $transactionType = sqlStatement($query,null, $GLOBALS['adodb']['dbreport']);
                                                echo "   <select class='form-control input-sm' name='form_transaction_type' id='form_transaction_type'>\n";
                                                echo "    <option value=''>-- " . xl('All') . " --\n";
                                                while ($trow = sqlFetchArray($transactionType)) {
                                                    $trid = $trow['invtrt_id'];
                                                    echo "    <option value='$trid'";
                                                    if ($trid == $_POST['form_transaction_type'])
                                                        echo " selected";
                                                    echo ">" . $trow['invtrt_name'] . "\n";
                                                }
                                                echo "   </select>\n";
                                                ?>
                                        </div>
                                            </div>
                                       
                                        <div class="col-xs-6 col-sm-4">
                                            <?php xl('From', 'e'); ?>:<br>
                                        <input type='text'  name='form_from_date' id="form_from_date"
                                                       size='10' value='<?php echo $form_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                       title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                       align='absbottom' width='24' height='22' id='img_from_date'
                                                       border='0' alt='[?]' style='cursor: pointer'
                                                       title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                        </div>
                                        <div class="col-xs-6 col-sm-4">
                                            <?php xl('To', 'e'); ?>:<br>
                                        <input type='text'  name='form_to_date' id="form_to_date"
                                                       size='10' value='<?php echo $form_to_date ?>'
                                                       onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                       title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                       align='absbottom' width='24' height='22' id='img_to_date'
                                                       border='0' alt='[?]' style='cursor: pointer'
                                                       title='<?php xl('Click here to choose a date', 'e'); ?>'>
                                        </div>
                                        <div class="col-xs-2"><?php xl('Saleable Items', 'e'); ?>:
                                        <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_POST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
                                        </div>
                                        <div class="col-xs-3"><?php xl('Zero quantity items', 'e'); ?>:
                                        <input type='checkbox'  name='zero_quantity_items' id="zero_quantity_items" value='1' <?php echo (isset($_POST['zero_quantity_items']) && $_POST['zero_quantity_items'] == 1) ? 'checked' : ''; ?> >
                                        </div>
                                  
                                    <div class='col-xs-12' style="margin-top: 20px;">
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewStockLedger");
                                                        $("#theform").submit();'>
                                                    <span> <?php xl('Submit', 'e'); ?> </span> </a> 

                                                <?php if (1) { ?>
                                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvStockLedger");
                                                            $("#theform").submit();'>
                                                        <span>
                                                            <?php xl('Export to CSV', 'e'); ?>
                                                        </span>
                                                    </a>
                                                <?php } ?>	
                                                <ul id="legendGroup">
                                                    <li style="background: #C5E3ED;">Adjustment</li>
                                                    <li style="background: #FFEE9E;">Consumption</li>
                                                    <li style="background: #FFD3DB;">Purchase</li>
                                                    <li style="background: #C7F975;">Received</li>
                                                    <li style="background: #C5E3ED;">Return</li>
                                                    <li style="background: #C7F975;">Revised</li>
                                                    <li style="background: #fff;">Sales</li>
                                                    <li style="background: #FFC0CB;">Stock Out</li>
                                                    <li style="background: #FCFCAB;">Transfer</li>
                                                    <li style="background: #DDF9AE;">Transfer Received</li>
                                                    <li style="background: #F8E7C9;">User Assign</li>
                                                    <li style="background: #CCC7C7;">User Consumption</li>
                                                    <li style="background: #FFFFE0;">User Return</li>
                                                </ul>
                                            </div>
                                     </div>
                         </div>

                </div>
                <!-- end of search parameters --> <?php
            } // end not form_csvexport
             if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvStockLedger') {
                $event = "Report Stock Ledger Export";
            } else {
                $event = "Report Stock Ledger View";
            }
                    
                    
            if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockLedger') {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Clinic Name') . '",';
                    echo '"' . xl('Item Name') . '",';
                    echo '"' . xl('Item Code') . '",';
                    echo '"' . xl('Batch Number') . '",';
                    echo '"' . xl('Expiry Date') . '",';
                    if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                        echo '"' . xl('Price') . '",';
                    }
                    echo '"' . xl('Quantity') . '",';
                    echo '"' . xl('Running Qty(Item Based)') . '",';
                    echo '"' . xl('Transaction Type') . '",';
                    echo '"' . xl('Transcation Date') . '",';
                    echo '"' . xl('Received User') . '"' . "\n";
                } else {
                   
                    ?>
                    <!-- <br> -->
                    <div id="report_results">
                        <table class="table table-bordered">
                            <tr class='head'>
                            <thead>
                            <th><?php xl('S.N.', 'e'); ?></th>
                            <th><?php xl('Clinic Name', 'e'); ?></th>
                            <th><?php xl('Item Name', 'e'); ?></th>
                            <th><?php xl('Item Code', 'e'); ?></th>
                            <th><?php xl('Batch Number', 'e'); ?></th>
                            <th><?php xl('Expiry Date', 'e'); ?></th>
                            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                <th><?php xl('Price', 'e'); ?></th>
                            <?php } ?>
                            <th><?php xl('Quantity', 'e'); ?></th>
                            <th><?php xl('Running Qty (Item Based)', 'e'); ?></th>
                            <th><?php xl('Transaction Type', 'e'); ?></th>
                            <th><?php xl('Transcation Date', 'e'); ?></th>
                            <th><?php xl('Received User', 'e'); ?></th>
                            </thead>
                            <tbody>
                                <!-- added for better print-ability -->
                                <?php
                            }
                            //Get Stock Ledgel Detail 
                            $dateMysqlFormat = getDateDisplayFormat(1);

                            if ($form_facility) {
                                //for individual facility
                                if ($form_date) {
                                    //if from date selected
                                    if ($form_to_date) {
                                        // if $form_date && $form_to_date
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;


                                        if ($days_between_from_to <= $form_facility_time_range) {

                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";

                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " invistr.invistr_clinic_id = '$form_facility'";
                                        } else {
                                            if($days_between_from_to<=$form_facility_time_range_valid){
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "stock_leadger_report"; // changable
                                            $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "transaction" => $transactionId, "saleable" => $sellable, "facility"=>$form_facility))); // changable
                                            $rcsl_report_description = "Stock Leadger report from $form_date to $form_to_date"; // changable
                                            //allFacilityReports() defined with globals.php
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            }
                                            else{
                                            $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                            }
                                        }
                                    } else {
                                        //only from date: no TO date
                                        $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " invistr.invistr_clinic_id = '$form_facility'";
                                        } else {
                                            if($days_between_from_to<=$form_facility_time_range_valid){
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "stock_leadger_report"; // changable
                                            $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "transaction" => $transactionId, "saleable" => $sellable, "facility"=>$form_facility))); // changable
                                            $rcsl_report_description = "Stock Leadger report from $form_date to $form_to_date"; // changable
                                            //allFacilityReports() defined with globals.php
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            }
                                            else{
                                            $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                            }
                                        }
                                    }
                                } else {
                                    // if from date not selected
                                    if ($form_to_date) {
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " invistr.invistr_clinic_id = '$form_facility'";
                                        } else {
                                            if($days_between_from_to<=$form_facility_time_range_valid){
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "stock_leadger_report"; // changable
                                            $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "transaction" => $transactionId, "saleable" => $sellable, "facility"=>$form_facility))); // changable
                                            $rcsl_report_description = "Stock Leadger report from $form_date to $form_to_date"; // changable
                                            //allFacilityReports() defined with globals.php
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            }
                                            else{
                                            $msgForReportLog= $form_facility_time_range_valid_errorMsg;  
                                            }
                                        }
                                    } else {
                                        if ($where) {
                                            $where .= " AND ";
                                        }
                                        $form_to_date = date("Y-m-d");
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                        $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                        if ($where) {
                                            $where .= " AND ";
                                        }
                                        $where .= " invistr.invistr_clinic_id = '$form_facility'";
                                    }
                                }
                                
                                  $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param  
                                
                            } else {
                                //for all facility
                                if ($form_date) {
                                    //if from date selected
                                    if ($form_to_date) {
                                        // if $form_date && $form_to_date
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_all_time_range) {

                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                        } else {
                                            if($days_between_from_to<=$form_facility_all_time_range_valid){
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "stock_leadger_report"; // changable
                                            $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "transaction" => $transactionId, "saleable" => $sellable))); // changable
                                            $rcsl_report_description = "Stock Leadger report from $form_date to $form_to_date"; // changable
                                            //allFacilityReports() defined with globals.php
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            }
                                            else{
                                            $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                            }
                                        }
                                    } else {
                                        //only from date: no TO date
                                        $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_all_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                        } else {
                                            if($days_between_from_to<=$form_facility_all_time_range_valid){
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "stock_leadger_report"; // changable
                                            $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "transaction" => $transactionId, "saleable" => $sellable))); // changable
                                            $rcsl_report_description = "Stock Leadger report from $form_date to $form_to_date"; // changable
                                            //allFacilityReports() defined with globals.php
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            }
                                            else{
                                            $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                            }
                                        }
                                    }
                                } else {
                                    // if from date not selected
                                    if ($form_to_date) {
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                        $toDate = date_create(date($form_to_date));
                                        $fromDate = date_create($form_date);
                                        $diff = date_diff($fromDate, $toDate);
                                        $days_between_from_to = $diff->days;

                                        if ($days_between_from_to <= $form_facility_all_time_range) {
                                            if ($where) {
                                                $where .= " AND ";
                                            }
                                            $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                        } else {
                                            if($days_between_from_to<=$form_facility_all_time_range_valid){
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "stock_leadger_report"; // changable
                                            $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $provider, "transaction" => $transactionId, "saleable" => $sellable))); // changable
                                            $rcsl_report_description = "Stock Leadger report from $form_date to $form_to_date"; // changable
                                            //allFacilityReports() defined with globals.php
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                if ($msgForReportLog) {
                                                    $msgForReportLog = $msgForReportLog;
                                                } else {
                                                    $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            }
                                            else{
                                            $msgForReportLog= $form_facility_all_time_range_valid_errorMsg;  
                                            }
                                        }
                                    } else {
                                        if ($where) {
                                            $where .= " AND ";
                                        }
                                        $form_to_date = date("Y-m-d");
                                        $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                        $where .= " DATE(invistr.invistr_created_date) >= '$form_date' AND DATE(invistr.invistr_created_date) <= '$form_to_date'";
                                    }
                                }
                                
                                $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param
                                
                            }

                            if (isset($form_provider) && $form_provider != '') {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $where .= "  invistr.invistr_createdby = '" . $provider . "'";
                                
                                $searchParam .= ' Provider = '. $provider.' | ';
                            } else {
                                $searchParam .= ' Provider = All | ';
                            }
                            if (isset($transactionId) && $transactionId != '') {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $where .= "  invtrt.invtrt_id = '" . $transactionId . "'";
                                
                                $searchParam .= ' Transaction Type = '. $transactionId.' | ';
                            }else{
                                $searchParam .= ' Transaction Type = All | ';
                            }

                            if (isset($sellable) && $sellable != 0) {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $where .= "  im.inv_im_sale = '" . $sellable . "'";
                                 $searchParam .= ' Saleable Items = Yes | ';
                            } else {
                                 $searchParam .= ' Saleable Items = No | ';
                            }
                            
                            
                        if(isset($zero_quantity_items) && $zero_quantity_items == 1){
                                $zeroItem = 1 ;
                        }else{
                                $zeroItem = 0 ;
                        }
                            
                            if (!empty($msgForReportLog)) {
                               debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                            } else {
                                $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                            }
                            
                           
                           
                            $stockLedgerData = getStockLedgelDetail($where, $dateMysqlFormat,0,$msgForReportLog , $zeroItem);
                            
                           if(count($stockLedgerData) > 0 && !empty($msgForReportLog)){ debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); } /// Update Report Auditing Section
                            
                            $srCount = 1;
                            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockLedger') {
                                if(is_array($stockLedgerData)){
                                        foreach ($stockLedgerData as $key => $val) {
                                        //CSV EXPORT - First Nested
                                        echo '"' . qescape($srCount) . '",';
                                        echo '"' . qescape($val['clinic_name']) . '",';
                                        echo '"",';
                                        echo '"",';
                                        echo '"",';
                                        echo '"",';
                                        if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                            echo '"",';
                                        }
                                        echo '"",';
                                        echo '"",';
                                        echo '"",';
                                        echo '"",';
                                        //echo '"",';
                                        echo '""' . "\n";

                                        foreach ($val['clinicWiseDetail'] as $subKey => $subVal) {
                                            //CSV EXPORT - Second Nested
                                            echo '"",';
                                            echo '"",';
                                            echo '"' . qescape($subVal['item_name']) . '",';
                                            echo '"' . qescape($subVal['item_code']) . '",';
                                            echo '"' . qescape($subVal['item_stock_batch']) . '",';
                                            echo '"' . qescape($subVal['item_stock_expiry']) . '",';

                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                                echo '"' . qescape($subVal['item_stock_price']) . '",';
                                            }
                                            echo '"",';
                                            echo '"' . qescape($subVal['opening_stock_balance_running_qty']) . '",';
                                            echo '"' . qescape($subVal['item_transaction_type']) . '",';
                                            echo '"",';
                                            //echo '"",';
                                            echo '""' . "\n";

                                            //Third
                                            foreach ($subVal['itemWiseDetail'] as $secondNestedKey => $secondNestedVal) {
                                                //CSV EXPORT - Second Nested
                                                echo '"",';
                                                echo '"",';
                                                echo '"",';
                                                echo '"",';
                                                echo '"",';
                                                echo '"",';
                                                if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                                    echo '"",';
                                                }
                                                echo '"' . qescape($secondNestedVal['item_stock_qty']) . '",';
                                                echo '"' . qescape($secondNestedVal['item_stock_running_qty']) . '",';
                                                echo '"' . qescape($secondNestedVal['item_transaction_type']) . '",';
                                                echo '"' . qescape($secondNestedVal['item_transaction_date']) . '",';
                                                echo '"' . qescape($secondNestedVal['item_received_by']) . '"' . "\n";
                                            }
                                            //Third
                                        }
                                        $srCount++;
                                    }
                                }
                                
                            } else {

                                
                                if(!is_array($stockLedgerData)){ }else{
                                foreach ($stockLedgerData as $key => $val) {
                                    ?>
                                    <tr bgcolor='<?php echo $row['invtrt_color'] ?>'>
                                        <td class="detail">&nbsp;<?php echo $srCount; ?></td>
                                        <td class="detail"><?php echo $val['clinic_name']; ?></td>
                                        <td class="detail"><?php echo $val['item_name']; ?></td>
                                        <td class="detail"></td>
                                        <td class="detail"></td>
                                        <td class="detail"></td>
            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                            <td class="detail"><?php echo ''; ?></td>
                                    <?php } ?>
                                        <td class="detail">&nbsp;</td>
                                        <td class="detail"></td>
                                        <td class="detail">&nbsp;</td>
                                        <td class="detail">&nbsp;</td>
                                        <td class="detail">&nbsp;</td>
                                    </tr>
            <?php
            $itemNameRepet = '';
            $countItemBasedRunningQty = 0;
            foreach ($val['clinicWiseDetail'] as $subKey => $subVal) {
                ?>
                                        <tr bgcolor='<?php echo $row['invtrt_color'] ?>'>
                                            <td class="detail">&nbsp;<?php echo ''; ?></td>
                                            <td class="detail"></td>
                                            <td class="detail"><?php echo $subVal['item_name']; ?></td>
                                            <td class="detail"><?php echo $subVal['item_code']; ?>	</td>
                                            <td class="detail"><?php echo $subVal['item_stock_batch']; ?></td>
                                            <td class="detail"><?php echo $subVal['item_stock_expiry']; ?>	</td>
                <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                <td class="detail"><?php echo ' ' . $subVal['item_stock_price']; ?></td>
                                        <?php } ?>
                                            <td class="detail">&nbsp;</td>
                                            <td class="detail"><?php echo $subVal['opening_stock_balance_running_qty']; ?></td>
                                            <td class="detail">&nbsp;<?php echo $subVal['item_transaction_type']; ?></td>
                                            <td class="detail">&nbsp;</td>
                                            <td class="detail">&nbsp;</td>
                                        </tr>
                <?php
                //Third               
                foreach ($subVal['itemWiseDetail'] as $secondNestedKey => $secondNestedVal) {
                    ?>

                                            <tr bgcolor='<?php echo $secondNestedVal['item_transaction_color_code'] ?>'>
                                                <td class="detail">&nbsp;<?php echo ''; ?></td>
                                                <td class="detail"></td>
                                                <td class="detail"></td>
                                                <td class="detail"></td>
                                                <td class="detail"></td>
                                                <td class="detail"></td>
                    <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                    <td class="detail"></td>
                                            <?php } ?>
                                                <td class="detail">&nbsp;<?php echo $secondNestedVal['item_stock_qty']; ?></td>
                                                <td class="detail"><?php echo $secondNestedVal['item_stock_running_qty']; ?></td> 
                                                <td class="detail">&nbsp;<?php echo $secondNestedVal['item_transaction_type']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $secondNestedVal['item_transaction_date']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $secondNestedVal['item_received_by']; ?></td>
                                            </tr>
                                            <?php
                                            //Third
                                        }
                                        ?>		
                                        <?php
                                    }

                                    $srCount++;
                                }
                               }
                            }


                            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvStockLedger') {
                                $srCountFlag = $srCount - 1;
                                if (!$srCountFlag) {
                                    ?>
                                    <tr>
                                        <td colspan='11' class='newtextcenter'><?php echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
                                    </tr>
                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- end of search results --> 
        <?php
    }
} else {
    ?>
                <div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>

                <!-- stuff for the popup calendar -->
                <style type="text/css">
                    @import url(../../library/dynarch_calendar.css);
                </style>
                <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
                <script type="text/javascript"
                src="../../library/dynarch_calendar_setup.js"></script>
                <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField: "form_from_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_from_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (toDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_from_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                                                    Calendar.setup({
                                                        inputField: "form_to_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_to_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('form_to_date').value,
                                                                    fromDate = document.getElementById('form_from_date').value;

                                                            if (fromDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_to_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                </script>

<?php } ?> 
<?php
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvStockLedger') {
    ?>

                <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
                </script>
        </body>

        <!-- stuff for the popup calendar -->
        <style type="text/css">
            @import url(../../library/dynarch_calendar.css);
        </style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript"
        src="../../library/dynarch_calendar_setup.js"></script>
        <script type="text/javascript">
                    Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
                    Calendar.setup({inputField: "form_to_date", ifFormat: "%Y-%m-%d", button: "img_to_date"});
        </script>
              </div>
</div>
        </body>
    </html>
    <?php
}
?>
