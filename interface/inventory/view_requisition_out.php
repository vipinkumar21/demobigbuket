<?php
	// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
	//
	// This program is for PRM software.
	$sanitize_all_escapes  = true;
	$fake_register_globals = false;

	 require_once("../globals.php");
	 require_once("$srcdir/acl.inc");
	 require_once("drugs.inc.php");
	 require_once("$srcdir/options.inc.php");
	 require_once("$srcdir/formdata.inc.php");
	 require_once("$srcdir/htmlspecialchars.inc.php");
         $datePhpFormat = getDateDisplayFormat(0);
	 $alertmsg = '';
	 $reqid = $_REQUEST['reqid'];
	 $facility = $_REQUEST['facility'];
	 $info_msg = "";
	 $tmpl_line_no = 0;

	 if (!acl_check('inventory', 'invf_reqout_view')) die(xlt('Not authorized'));

	 if(empty($reqid)){
	 	echo "<script language='JavaScript'>\n";
	 	echo " alert(You have not selected requisition.);\n";
	 	echo " if (opener.refreshme) opener.refreshme();\n";
	 	echo " window.close();\n";
	 	echo "</script></body></html>\n";
	 	exit();
	 }
	// Format dollars for display.
	//
	function bucks($amount) {
	  if ($amount) {
	    $amount = sprintf("%.2f", $amount);
	    if ($amount != 0.00) return $amount;
	  }
	  return '';
	}
	// Translation for form fields used in SQL queries.
	//
	function escapedff($name) {
	  return add_escape_custom(trim($_POST[$name]));
	}
	function numericff($name) {
	  $field = trim($_POST[$name]) + 0;
	  return add_escape_custom($field);
	}
?>
<html>
<head>
	<?php html_header_show(); ?>
	<title><?php xlt("View"); echo ' ' . xlt('Stock Requisition'); ?></title>
    <?php include_once("themestyle.php"); ?>
    <?php include_once("scriptcommon.php"); ?>
	<script language="JavaScript">
		<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
	</script>
</head>
<body class="body_top">
<?php
	$row = sqlQuery("SELECT istreq.isr_id, istreq.isr_number,istreq.message, istreq.isr_from_clinic, istreq.isr_to_clinic, istreq.isr_isapproved, istreq.isr_status, istreq.isr_isdeleted, isr_createdby, istreq.isr_created_date, frf.name AS fromFacility, tof.name AS toFacility ,users.nickname as name , istreq.isr_created_date as reqdate, (SELECT iin_received_date FROM inv_issue_notes WHERE inv_issue_notes.`iin_reqid`= istreq.`isr_id` AND inv_issue_notes.`iin_status` = '1' ORDER BY inv_issue_notes.iin_id DESC
LIMIT 1) AS lrd " .
	"FROM inv_stock_requisition AS istreq 
        INNER JOIN users AS users ON users.id = istreq.isr_createdby
	INNER JOIN facility AS frf ON frf.id = istreq.isr_from_clinic
	INNER JOIN facility AS tof ON tof.id = istreq.isr_to_clinic " .
	"WHERE istreq.isr_id = ? AND istreq.isr_from_clinic = ? ORDER BY istreq.isr_id DESC", array($reqid, $facility));
        
?>
<div class="infopop"><?php  xl('View Requisition','e'); ?></div>
<div class="popupTableWrp mt-0">
<table id='' cellpadding='0' cellspacing='0' border='0' class='popupTable ui-table' width='100%'>
	<thead>
		<tr>
			<th width='10%'><?php echo xlt('Req#'); ?></th>
			<th width='15%'><?php echo xlt('From Facility'); ?></th>
			<th width='15%'><?php echo xlt('To Facility'); ?></th>
			<!-- <th width='15%'><?php //echo xlt('App. Status'); ?>:</th> -->
			<th width='15%'><?php echo xlt('Req. By'); ?></th>
			<th width='15%'><?php echo xlt('Req. Status'); ?></th>
			<th width='10%'><?php echo xlt('Req. Date'); ?></th>
			<th width='15%'><?php echo xlt('Last Rcpt Date'); ?></th>
			<!-- <th width='15%'><?php //echo xlt('Reason'); ?></th> -->
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $row['isr_number'];?></td>
			<td><?php echo $row['fromFacility'];?></td>
			<td><?php echo $row['toFacility'];?></td>
			<td><?php echo $row['name'];?></td>
			<!-- <td><?php //if($row['isr_isapproved'] == 2){ echo 'Rejected';}else if($row['isr_isapproved'] == 1){echo 'Approved';} else if($row['isr_isapproved'] == 3){echo 'Cancelled';}  else {echo 'Waiting';}?></td> -->
			<td><?php if($row['isr_status'] == 2){ echo 'Completed';}else if($row['isr_status'] == 1) {echo 'Partially Completed';} else if($row['isr_status'] == 3){echo 'Cancelled';} else {echo 'In Progress';} ?></td>
			<td><?php 
                                    $rsdate= !empty($row['reqdate']) ? text(date($datePhpFormat, strtotime($row['reqdate']))) : '&nbsp;'; 
                                    echo $rsdate;
                                                ?></td>
			<td><?php 
                                 $lrd= !empty($row['lrd']) ? text(date($datePhpFormat, strtotime($row['lrd']))) : '&nbsp;'; 
                                 echo $lrd;
                             ?></td>
			<!-- <td>
		     
			</td> -->
		</tr>
	</tbody>
 </table>
</div>

<br />

<div class="popupTableWrp mt-0" id="requisitionListContainer">
	<table id='requisitionList' cellpadding='0' cellspacing='0' border='0' class='popupTable ui-table' width='100%'>
		<thead>
			<tr>
				<th width='55%'>Item Name</th>
				<th width='25%'>Item Code</th>
				<th width='15%'>Quantity</th>					
			</tr>
		</thead>
		<tbody>
		<?php 
		
		$res = sqlStatement("SELECT irit.iri_itemid, irit.iri_reqid, irit.iri_quantity, irit.iri_isdeleted, im.inv_im_name, im.inv_im_code
		FROM inv_requisition_item AS irit INNER JOIN inv_item_master AS im ON im.inv_im_id = irit.iri_itemid 
                      
		WHERE irit.iri_reqid = ?", array($reqid));
		if(sqlNumRows($res)){
			while ($itemrow = sqlFetchArray($res)) {
		?>
			<tr <?php if($itemrow['iri_isdeleted'] == 1){echo 'class="strikeThrough"';}?>>				
			<!-- <td>
				<?php //echo $itemrow['invcat_name'];?>					
			</td>
			<td>
				<?php //if(!empty($itemrow['invsubcat_name'])){ echo $itemrow['invsubcat_name'];}else{echo 'NA';}?>						
			</td> -->
			<td>
				<?php echo $itemrow['inv_im_name'];?>					
			</td>
			<td>
				<?php echo $itemrow['inv_im_code'];?>					
			</td>
			<td>
				<?php echo $itemrow['iri_quantity'];?>					
			</td>
			
		</tr>
		<?php 
			}
		}
		?>
		</tbody>
                
	</table>
    
</div>
<?php if(!empty($row['message'])){ ?>
<div class="popupTableWrp mt-0">
	<table id='requisitionList' cellspacing='0' class="popupTable ui-table">
		<thead>
			<tr>
				<th width='25%'>Canceled</th>
				<th width='75%'>Cancelation Notes</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<?php $msg=explode(",",$row['message']);?>
				<td><?php echo $msg['0'];?></td>
				<td><?php echo $msg['1'];?></td>
			</tr>
		</tbody>
	</table>
</div>
<?php }?>
<script language="JavaScript">
<?php
 if ($alertmsg) {
  echo "alert('" . htmlentities($alertmsg) . "');\n";
 }
?></script>
</body>
</html>