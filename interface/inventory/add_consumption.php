<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("./lib/database.php");
 require_once("./lib/inv.gacl.class.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $stockid = $_REQUEST['stockid'];
 $info_msg = "";
 $tmpl_line_no = 0;

 if (!acl_check('inventory', 'invf_cons_add')) die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
	<?php html_header_show(); ?>
	<title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
    <?php include_once("themestyle.php"); ?>
    <?php include_once("scriptcommon.php"); ?>
	<script language="JavaScript">
		<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
	</script>
</head>
<body class="ui-bg-wht">
<?php
	// If we are saving, then save and close the window.
	// First check for duplicates.
	//
	if ($_POST['form_save']) {
		$itemStockValue = $_POST['itemid'];
		$itemStockArr = explode('#', $itemStockValue);
		$itemStockDetails = getItemStockDetail($itemStockArr[0]);
		if($itemStockDetails['invist_quantity'] >= $_POST['form_qty']){
		   	removeStockQuantity(escapedff('form_qty'), $itemStockArr[0]);		
			$tranData=array(
					'invistr_itemid' => $itemStockDetails['invist_itemid'],
					'invistr_batch' => $itemStockDetails['invist_batch'],
					'invistr_expiry' => $itemStockDetails['invist_expiry'],
					'invistr_price' => $itemStockDetails['invist_price'],
					'invistr_quantity' => escapedff('form_qty'),
					'invistr_before_qty' => $itemStockDetails['invist_quantity'],
					'invistr_after_qty' => $itemStockDetails['invist_quantity']-escapedff('form_qty'),
					'invistr_clinic_id' => escapedff('facility'),
					'invistr_tran_type' => 12,
					'invistr_comment' => escapedff('form_desc'),
					'invistr_createdby' => $_SESSION['authId'],
					'invistr_created_date' => date("Y-m-d H:i:s"),
                                        'invistr_action' => 'inventory/add_consumption',
					'invistr_action_id' =>  $itemStockArr[0]
			);
			$stockTranid = insertStockTransaction($tranData);
                        $info_msg = (!empty($stockTranid)) ? "<div class='alert alert-success alert-dismissable'>Consumption for item saved successfully!</div>" : "<div class='alert alert-danger alert-dismissable'>Error in saving consumption!</div>";
		  // Close this window and redisplay the updated list of drugs.
		  //  
			$_SESSION['INV_MESSAGE'] = $info_msg;
		  //if ($info_msg) echo " alert('$info_msg');\n";
	        echo "<script language='javascript'>\n";
	        echo "$.fancybox.close();\n";
	        echo "parent.location.reload();\n";
	        echo "</script></body></html>\n";
		  exit();
		}
	}
       
?>
<div class="infopop"><?php  xl('Add Item Consumtion','e'); ?></div>
<!-- tableData -->
<div id='popUpformWrap'>
<form class='nomrgn' id='addconsumption' method='post' name='theform' action='add_consumption.php'>
	<!-- Row 1 -->
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="date-wrp" for="facility">Facility:<sup class="required">*</sup></label>
				<?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        $userId =$_SESSION['authId'];
                                        InvUsersFacilityDropdown('facility', '', 'facility', $facility,$userId , $userFacilityRestrict,$pdoobject,0);
                                        ?>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group posrel">
				<label class="date-wrp" for="itemStock">Item:<sup class="required">*</sup></label>
				<input type="text" class="formEle" id="itemField" name="itemField" readonly>
                 <img id="loading" style="display:none" src="../../images/loader.gif" />
				<input type="hidden" class="formEle" id="itemId" name="itemid">
				<input type="hidden" value="0" id="form_sale" name="form_sale">
				<input type="hidden" value="0" id="form_expiry" name="form_expiry">
			</div>
		</div>
	</div>
	<!-- Row 1 -->

	<!-- Row 2 -->
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label><?php echo xlt('Quantity'); ?>:<sup class="required">*</sup></label>
				<input type='number' size='10' name='form_qty' maxlength='7' onkeypress="return isNumber(event);" value='<?php echo attr($row['invist_quantity']) ?>' class='formEle' />
				<input type='hidden' id='itemQty' name='itemQty' value='' />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 txtarea">
			<div class="form-group">
				<label class="date-wrp">Notes:</label>
				<textarea name='form_desc' rows="5" cols="34"></textarea><span class="pull-right counter-text">Max. 768 character(s)</span>
			</div>
		</div>
	</div>
	<!-- Row 2 -->
	<a id="subform" href="javascript:void(0)" class="save-btn"></a>
	<input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' value='save' />
</form>
</div>
<!-- tableData -->
<div id="placeholder">
	<img src='<?php echo $inventry_url; ?>emr/images/oi-wm-logo.jpg' class='autohgt' alt='' />
</div>
</body>
</html>