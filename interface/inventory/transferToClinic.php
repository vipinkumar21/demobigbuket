<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("./lib/inv.users.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//$datePhpFormat = getDateDisplayFormat(0);

// Check authorization.
//$thisauth = acl_check('inventory', 'invf_tran_in_list');
//if (!$thisauth)
//    die(xlt('Not authorized'));
// For each sorting option, specify the ORDER BY argument.
if (!$invgacl->acl_check('inventory','invf_tran_in_list','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));

$ORDERHASH = array(
    'invtran_id' => 'invtran_id DESC'
);
//echo "<pre>";
//print_r($_POST); exit;
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[urldecode($_REQUEST['form_orderby'])] ? urldecode($_REQUEST['form_orderby']) : 'invtran_id';
$orderby = $ORDERHASH[$form_orderby];
if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
}else{
   $_REQUEST['facility']= $_SESSION['cid'];
}
$facility = isset($_REQUEST['facility'])? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$toFacility=$_REQUEST['tofacilityid'];
$form_item = urldecode($_REQUEST['form_item']);
$from_date = urldecode($_REQUEST['form_from_date']);
$to_date = urldecode($_REQUEST['form_to_date']);

// get drugs
/* echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " .
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby"; */
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Transfer Receipt'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body>
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->

            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <h1><?php xl('Transfer Receipt', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='transferToClinic.php'  name='theform' id='theform' class="botnomrg">
                            <!-- formPart -->
                            <div class="filterWrapper">
                                <!-- first column starts -->
                                <div class="ui-block">
                                    <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('facility', '', 'facility', $facility, $_SESSION['authId'], $userFacilityRestrict, $pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends -->
                                <!-- first column starts -->
                                <div class="ui-block">
                                    <select name='tofacilityid' id="tofacilityidField" class='formEle'>
                                <option value='0' selected="selected">All Facilities</option>
                                <?php

                                $qsql = $pdoobject->custom_query("SELECT id, name FROM facility ORDER BY name ASC ", null,'','fetchAll');
                                foreach ($qsql as $facrow) {
                                    $selected = ( $facrow['id'] == $toFacility ) ? 'selected="selected"' : '';
                                    echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";
                                }
                                ?>
                            </select>
                                </div>
                                <!-- first column ends -->
                                <!-- fifth column starts -->
                                <div class="ui-block">
                                    <input type='text' name='form_item' placeholder='Item' id="form_item" value='<?php echo $form_item ?>' title='' />
                                </div>
                                <!-- fifth column ends -->
                                <!-- second column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                </div>
                                <!-- second column ends -->
                                <!-- third column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                </div>
                                <!-- third column ends -->
                                <!-- fourth column starts -->
                                <div class="ui-block wdth15">
                                  <a class="pull-right btn_bx" id='reset_form1' href="transferToClinic.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true");
                                            $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- fourth column ends -->
                            </div>
                            <!-- formPart -->

                            <!-- tableData -->
                            <!-- <table border='0' cellpadding='1' cellspacing='2' width='100%'>
                                <?php
                                //if (!empty($_SESSION['alertmsg'])) {
                                    ?>
                                    <tr >
                                        <td colspan="2">
                                            <?php
                                            //echo $_SESSION['alertmsg']; $_SESSION['alertmsg'] = "";
                                            ?></td>
                                    </tr>
                                    <?php
                                //}
                                ?>
                            </table> -->
                            <?php
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_from_date'] || $_REQUEST['form_to_date'] || $_REQUEST['form_item'] || empty($facility)) {
                                $res = "SELECT tran.invtran_id, tran.invtran_item_id, tran.invtran_stock_id, tran.invtran_transfer_quantity, tran.invtran_accept_quantity, tran.invtran_transfer_user_id, tran.invtran_accept_user_id, tran.invtran_from_facility_id, tran.invtran_to_facility_id, tran.invtran_transfer_date, tran.invtran_accept_date, tran.invtran_transfer_notes, tran.invtran_accept_notes, ist.invist_id, im.inv_im_name, frf.name AS fromFacility, tof.name AS toFacility, tran.oneivory_order_id " .
                                        "FROM inv_transfer AS tran
                                        INNER JOIN facility AS frf ON frf.id = tran.invtran_from_facility_id
                                        INNER JOIN facility AS tof ON tof.id = tran.invtran_to_facility_id
                                        INNER JOIN inv_item_stock AS ist ON ist.invist_id = tran.invtran_stock_id
                                        INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid " .
                                        "WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' ";

                                if (!empty($facility)) { // if facility exists
                                    $res .= " AND tran.invtran_to_facility_id = '" . $facility . "'";
                                } else {
                                    $res .= " AND tran.invtran_to_facility_id IN(" . getLoggedUserAssignedClinics() . ")";
                                }
                                if (!empty($toFacility)) { // if facility exists
                                    $res .= " AND tran.invtran_from_facility_id = '" . $toFacility . "'";
                                }
                                if (!empty($from_date) && empty($to_date)) { // If from dates exists
                                    $res .= " AND tran.invtran_transfer_date >= '$from_date'";
                                }
                                if (!empty($to_date) && empty($from_date)) { // If to dates exists
                                    $res .= " AND " . "tran.invtran_transfer_date <= '$to_date'";
                                }
                                if (!empty($from_date) && !empty($to_date)) {
                                    $res .= " AND tran.invtran_transfer_date BETWEEN '$from_date' AND '$to_date' ";
                                }
                                if (!empty($form_item)) { // If to dates exists
                                    $res .= " AND im.inv_im_name like'%" . $form_item . "%'";
                                }
                                $res .= " ORDER BY $orderby";
                                //$sql_num = sqlStatement($res);
                                //$num_rows = sqlNumRows($sql_num); // total no. of rows
                                $num_rows = $pdoobject->custom_query($res,NULL,1);
                                $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                $page = $_GET["page"];
                                $page = ($page == 0 ? 1 : $page);
                                $page_start = ($page - 1) * $per_page;
                                $curr_URL = $_SERVER['SCRIPT_NAME'] . '?form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&facility=' . $facility . '&form_item=' . urldecode($_REQUEST['form_item']) . '&form_to_date=' . urldecode($_REQUEST['form_to_date']) . '&form_from_date=' . urldecode($_REQUEST['form_from_date']) . '&';
                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                $res .= " LIMIT $page_start , $per_page";
                                //echo $res;
                                //$res = sqlStatement($res);
                                $res = $pdoobject->custom_query($res);
                                ?>
                                <div id="" class='tableWrp pb-2'>
                                    <!-- pagination -->
                                    <?php
                                    if ($num_rows > 0) {
                                        echo $pa_data;
                                    }
                                    ?>
                                    <!-- pagination -->
                                    <div class='dataTables_wrapper no-footer'>
                                            <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                <?php if ($num_rows > 0) { ?>
                                                    <thead>
                                                        <tr>
                                                            <th width="14%">
                                                                <?php echo xlt('Item Name'); ?>
                                                            </th>
                                                            <th width="9%">
                                                                <?php echo xlt('Requisition No.'); ?>
                                                            </th>
                                                            <th width="10%">
                                                                <?php echo xlt('To Facility'); ?>
                                                            </th>
                                                            <th width="10%">
                                                                <?php echo xlt('From Facility'); ?>
                                                            </th>
                                                            <th width="7%">
                                                                <?php echo xlt('Trans Qty.'); ?>
                                                            </th>
                                                            <th width="8%">
                                                                <?php echo xlt('Accept Qty.'); ?>
                                                            </th>
                                                            <th width="10%">
                                                                <?php echo xlt('Trans Notes'); ?>
                                                            </th>
                                                            <th width="10%">
                                                                <?php echo xlt('Acpt. Notes'); ?>
                                                            </th>
                                                            <th width="8%">
                                                                <?php echo xlt('Trans. Date'); ?>
                                                            </th>
                                                            <th width="8%">
                                                                <?php echo xlt('Acpt. Date'); ?>
                                                            </th>
                                                            <th width="6%">
                                                                <?php echo xlt('Action'); ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                }
                                                $lastid = "";
                                                $encount = 0;
                                                if ($num_rows) {
                                                    foreach($res as $row){
                                                    //while ($row = sqlFetchArray($res)) {
                                                        ++$encount;
                                                        $bgcolor = "#" . (($encount & 1) ? "f7d4be" : "f1f1f1");
                                                        $lastid = $row['invtran_id'];
                                                        if ($row['invcat_deleted'] == 1) {
                                                            echo "<tr class='detail strikeThrough' bgcolor='$bgcolor'>\n";
                                                        } else {
                                                            echo "<tr class='detail' role='row'>\n";
                                                        }
                                                        //echo "  <td>" .$encount. "</td>\n";
                                                        echo "  <td>" .
                                                        text($row['inv_im_name']) . "</td>\n";
                                                        echo "  <td>" . getRequisitionNo($row['oneivory_order_id']) . "</td>\n";
                                                        echo "  <td>" . text($row['toFacility']) . "</td>\n";
                                                        echo "  <td>" . text($row['fromFacility']) . "</td>\n";
                                                        echo "  <td>" . text($row['invtran_transfer_quantity']) . "</td>\n";
                                                        echo "  <td>" . text($row['invtran_accept_quantity']) . "</td>\n";
                                                        echo "  <td>" . text($row['invtran_transfer_notes']) . "</td>\n";
                                                        echo "  <td>" . text($row['invtran_accept_notes']) . "</td>\n";
                                                        echo "  <td>" . invDateFormat($row['invtran_transfer_date']) . "</td>\n";
                                                        echo "  <td>";
                                                        if (!empty($row['invtran_accept_date'])) {
                                                            echo invDateFormat($row['invtran_accept_date']);
                                                        }
                                                        echo "</td>\n";
                                                        echo "  <td>";
                                                        if (empty($row['invtran_accept_date']) && empty($row['invtran_accept_quantity'])) {

                                                            if($invfac->validateFacilityId($row['invtran_to_facility_id'])){
                                                            //echo "========";
                                                            if ($invgacl->acl_check('inventory','invf_tran_in_accept','users', $_SESSION['authUser'])) {
                                                                echo "<div style='width:38px' data-pageURL='accept_transfer.php?tranid=" . attr($lastid) . "' class='addEditDataSml listbtn'><span class='dashboardRecieptIcon1'></span>Receipt</div> &nbsp; ";
                                                            }
                                                            //echo "<a href='javascript:void(0);' onclick='doReturnClick(".attr($lastid).", ".attr($facility).")'>Return</a> &nbsp; ";
                                                            }
                                                        }
                                                        echo "</td>\n";
                                                        echo " </tr>\n";
                                                    } // end while
                                                } else {
                                                    ?>
                                                    <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    <!-- pagination -->
                                    <?php
                                    if ($num_rows > 0) {
                                        echo $pa_data;
                                    }
                                    ?>
                                    <!-- pagination -->
                                </div>
                                <?php } else { ?>
                                    <div class='text leftmrg6 dnone'>
                                        <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                                    </div>
                                <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            <!-- tableData -->
                        </form>
                        <!-- mdleCont -->
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
    </body>
</html>
