<?php

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
$item = $_REQUEST['item'];
$clinicId = $_REQUEST['clinic'];
$adjustment = $_REQUEST['adjustment'];
$arrayItem = array();
$imSql = "SELECT oneivory_id, inv_item_images.url as s_image_url, its.invist_id AS `StockId`, its.invist_batch AS `BatchNo`, its.invist_expiry AS `StockExpiry`, "
        . "its.invist_price AS `StockPrice`, its.invist_quantity AS `StockQuantity`, im.inv_im_id AS `ItemId`, im.inv_im_name AS `ItemName`, im.inv_im_catId AS `CatId` "
        . ", concat_ws('#', im.inv_im_code, its.invist_batch, its.invist_expiry, its.invist_price) as stockKey "
        . "FROM inv_item_stock AS its "
        . "INNER JOIN inv_item_master AS im ON im.inv_im_id = its.invist_itemid "
        . "LEFT JOIN inv_item_images  ON inv_item_images.inv_im_id = im.inv_im_id "
        . "WHERE im.inv_im_status='1' AND im.inv_im_deleted='0' AND its.invist_clinic_id = '" . $clinicId . "' "
        . "AND its.invist_isdeleted = '0' and inv_item_images.deleted='0' and inv_item_images.is_active='0' and inv_item_images.type='1' ";
if (empty($adjustment)) { 
    $imSql .= "  AND its.invist_quantity  > 0";
}
$imSql .= " AND im.inv_im_name LIKE '%" . trim($item) . "%' GROUP BY stockKey ORDER BY im.inv_im_name ASC LIMIT 0 , 10";
//$imres = sqlStatement($imSql);
$imres =$pdoobject->custom_query($imSql);

$i=0;
foreach ($imres as $imcrow) {
    $arrayItem[$i] = $imcrow;
    $arrayItem[$i]['categories']= getCategoryList($imcrow['CatId']);
    $i++;
}

echo json_encode($arrayItem);
