<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("mag_update_status.php");
require_once("$srcdir/classes/class.phpmailer.php");


$alertmsg = '';
$issueid = $_REQUEST['issueid'];
$facility = $_REQUEST['facility'];
$info_msg = "";
$tmpl_line_no = 0;

$reqnumber = $_REQUEST['reqnumber'];
$magorderid = $_REQUEST['magorderid'];

if (!$invgacl->acl_check('inventory', 'invf_issuein_recpt', 'users', $_SESSION['authUser']))
    die(xlt('Not authorized'));

if (empty($issueid)) {
    echo "<script language='JavaScript'>\n";
    echo " alert(You have not selected issue.);\n";
    echo "parent.location.reload();\n";
    echo " window.close();\n";
    echo "</script></body></html>\n";
    exit();
}

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt("Accept"); echo ' ' . xlt('Issued Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
            function checkInputData(fieldData) {
                var fieldName = fieldData.name;
                var fieldNameArr = fieldName.split("_");
                var fieldStr = fieldNameArr[0];
                var stockId = fieldNameArr[1];
                var enteredQty = fieldData.value;
                if (enteredQty == '') {
                    enteredQty = 0;
                }
                var issuedStockQty = document.getElementById("issuedqty_" + stockId).value;
                var returnQty = document.getElementById("returnqty_" + stockId).value;
                if (returnQty == '') {
                    returnQty = 0;
                }
                var totalEnteredQty = parseInt(enteredQty) + parseInt(returnQty);
                if (parseInt(enteredQty) > parseInt(issuedStockQty)) {
                    fieldData.style.backgroundColor = "red";
                    alert('Accept quantity should not be greater than issued quantity.');
                    fieldData.value = parseInt(issuedStockQty) - parseInt(returnQty);
                    fieldData.focus();
                } else if (parseInt(totalEnteredQty) > parseInt(issuedStockQty)) {
                    fieldData.style.backgroundColor = "red";
                    alert('Sum of return quantity and accept quantity should not be greater than issued quantity.');
                    document.getElementById("returnqty_" + stockId).value = parseInt(issuedStockQty) - parseInt(enteredQty);
                    fieldData.focus();
                }
            }

            function checkReturnInputData(fieldData) {
                var fieldName = fieldData.name;

                var fieldNameArr = fieldName.split("_");
                var fieldStr = fieldNameArr[0];
                var stockId = fieldNameArr[1];
                var enteredQty = fieldData.value;
                //alert(enteredQty);
                if (enteredQty == '') {
                    enteredQty = 0;
                }
                var issuedStockQty = document.getElementById("issuedqty_" + stockId).value;


                if (fieldStr == 'returnqty') {

                    if (parseInt(enteredQty) <= parseInt(issuedStockQty)) {

                        document.getElementById("acceptqty_" + stockId).value = parseInt(issuedStockQty) - parseInt(enteredQty);
                        //var oneivoryitemid=document.getElementById("oneivoryrtn_"+stockId).value.split("_");
                        //document.getElementById("oneivoryrtn_"+stockId).value = oneivoryitemid[0]+'_'+document.getElementById("returnqty_"+stockId).value;
                    } else {
                        fieldData.value = 0;
                        document.getElementById("acceptqty_" + stockId).value = issuedStockQty;
                        //var oneivoryitemid=document.getElementById("oneivoryrtn_"+stockId).value.split("_");
                        //document.getElementById("oneivoryrtn_"+stockId).value = oneivoryitemid[0]+'_'+document.getElementById("returnqty_"+stockId).value;
                        fieldData.focus();
                        fieldData.select();
                        alert('return quantity should not be greater than issued quantity');
                    }
                } else {

                    if (parseInt(enteredQty) <= parseInt(issuedStockQty)) {

                        document.getElementById("returnqty_" + stockId).value = parseInt(issuedStockQty) - parseInt(enteredQty);
                        //var oneivoryitemid=document.getElementById("oneivoryrtn_"+stockId).value.split("_");
                        //document.getElementById("oneivoryrtn_"+stockId).value = oneivoryitemid[0]+'_'+document.getElementById("returnqty_"+stockId).value;
                    } else {
                        //fieldData.value = 0;
                        document.getElementById("acceptqty_" + stockId).value = issuedStockQty;
                        document.getElementById("returnqty_" + stockId).value = 0;
                        //var oneivoryitemid=document.getElementById("oneivoryrtn_"+stockId).value.split("_");
                        //document.getElementById("oneivoryrtn_"+stockId).value = oneivoryitemid[0]+'_'+document.getElementById("returnqty_"+stockId).value;
                        fieldData.focus();
                        fieldData.select();
                        alert('accept quantity should not be greater than issued quantity');
                    }
                }


            }


            function isInt(x) {
                var y = parseInt(x);
                if (isNaN(y))
                    return false;
                return x == y && x.toString() == y.toString();
            }
            function submitform() {
                var issuedStocksCount = document.getElementsByName("issuedStocks[]");
                var dataSubmitFlag = 0
                for (var i = 0; i < issuedStocksCount.length; i++) {
                    var stockId = issuedStocksCount[i].value;
                    var issuedStockQty = document.getElementById("issuedqty_" + stockId).value;
                    if (!isInt(issuedStockQty)) {
                        issuedStockQty = 0;
                    }
                    var acceptQty = document.getElementById("acceptqty_" + stockId).value;
                    if (!isInt(acceptQty)) {
                        acceptQty = 0;
                    }
                    var returnQty = document.getElementById("returnqty_" + stockId).value;
                    if (!isInt(returnQty)) {
                        returnQty = 0;
                    }
                    var totalEnteredQty = parseInt(returnQty) + parseInt(acceptQty);
                    if (parseInt(totalEnteredQty) < parseInt(issuedStockQty)) {
                        document.getElementById("acceptqty_" + stockId).style.backgroundColor = "red";
                        document.getElementById("returnqty_" + stockId).style.backgroundColor = "red";
                        alert('Sum of return quantity and accept quantity should not be less than issued quantity.');
                        document.getElementById("acceptqty_" + stockId).focus();
                        return false;
                    } else if (parseInt(totalEnteredQty) > parseInt(issuedStockQty)) {
                        document.getElementById("acceptqty_" + stockId).style.backgroundColor = "red";
                        document.getElementById("returnqty_" + stockId).style.backgroundColor = "red";
                        alert('Sum of return quantity and accept quantity should not be greater than issued quantity.');
                        document.getElementById("acceptqty_" + stockId).focus();
                        return false;
                    }
                }
                parent.$.fn.fancybox.close();
                parent.location.reload();
            }
        </script>
    </head>
    <?php
    // If we are saving, then save and close the window.
    // First check for duplicates.
    //
	if ($_POST['form_save']) {
             $totalissue=0;
             $totalaccept=0;
             $totalreject=0;
            
            foreach($_POST['issuedStocks'] as $issueid){
                $totalissue=$totalissue+$_POST['issuedqty_'.$issueid];
                $totalaccept=$totalaccept+$_POST['acceptqty_'.$issueid];
                $totalreject=$totalreject+$_POST['returnqty_'.$issueid];
            }
            //$totalreject=100;
            //echo $totalissue.'<br>'.$totalaccept.'<br>'.$totalreject;
            
        if($totalissue==($totalaccept+$totalreject)){
        $saveIssue = 0;
        $issuedStocks = $_POST['issuedStocks'];
        $issuedStockItems = $_POST['issuedItems'];
        $issueItemIds = $_POST['issueItemId'];
        $magorderid = $_POST['magorderid'];
        $issuedStockData = array();
        $issuedStockTranData = array();
        try {
            $pdoobject->begin_transaction();
            for ($countStockItems = 0; $countStockItems < count($issuedStocks); $countStockItems++) {
                
                $issuedItemId = $issuedStockItems[$countStockItems];
                $issuedStockId = $issuedStocks[$countStockItems];
                
                $itemStockDetails = getItemStockDetail($issuedStockId,'1');
               
                $issueItemId = $issueItemIds[$countStockItems];
                foreach ($_POST['oneivoryrtn'] as $oneivoryrtn) {
                    $itemId = explode('_', $oneivoryrtn);

                    if ($itemId['0'] == $issuedStockId) {
                        $oneivory_order_item_id = $itemId['1'];
                    }
                }
                
                if (!empty($_POST['acceptqty_' . $issuedStockId])) {
                    $stockData = array(
                        'invist_itemid' => $itemStockDetails['invist_itemid'],
                        'invist_batch' => $itemStockDetails['invist_batch'],
                        'invist_expiry' => $itemStockDetails['invist_expiry'],
                        'invist_price' => $itemStockDetails['invist_price'],
                        'invist_quantity' => intval(escapedff('acceptqty_' . $issuedStockId)),
                        'invist_clinic_id' => escapedff('facility'),
                        'invist_createdby' => $_SESSION['authId'],
                        'invist_created_date' => date("Y-m-d H:i:s")
                    );
                    
                    $beforeStockDetails = getClincItemStockDetail($itemStockDetails['invist_itemid'], $itemStockDetails['invist_batch'], $itemStockDetails['invist_expiry'], $itemStockDetails['invist_price'], escapedff('facility'));
                    $stockData = insertUpdateItemStock($stockData);
                    $stockid = $stockData['invist_id'];
                    $beforeQty = 0;
                    if (!empty($beforeStockDetails)) {
                        $beforeQty = $beforeStockDetails[0]['invist_quantity'];
                    }
                    $tranData = array(
                        'invistr_itemid' => $issuedItemId,
                        'invistr_batch' => $itemStockDetails['invist_batch'],
                        'invistr_expiry' => $itemStockDetails['invist_expiry'],
                        'invistr_price' => $itemStockDetails['invist_price'],
                        'invistr_quantity' => intval(escapedff('acceptqty_' . $issuedStockId)),
                        'invistr_before_qty' => $beforeQty,
                        'invistr_after_qty' => $beforeQty + intval(escapedff('acceptqty_' . $issuedStockId)),
                        'invistr_clinic_id' => $_POST['facility'],
                        'invistr_tran_type' => 15,
                        'invistr_comment' => escapedff('form_desc_' . $issuedStockId),
                        'invistr_createdby' => $_SESSION['authId'],
                        'invistr_created_date' => date("Y-m-d H:i:s"),
                        'invistr_action' => 'inventory/accept_issued_items',
                        'invistr_action_id' => $stockid
                    );
                    $transId = $pdoobject->insert("inv_item_stock_transaction", $tranData);
                    $pdoobject->custom_query_execute("update inv_item_stock set invist_quantity='" . $tranData['invistr_after_qty'] . "' where invist_id = '" . $stockid . "'");
                }
                if (!empty($_POST['returnqty_' . $issuedStockId])) {
                    $tranferData = array(
                        'invtran_item_id' => $issuedItemId,
                        'invtran_stock_id' => $issuedStockId,
                        'invtran_transfer_quantity' => intval(escapedff('returnqty_' . $issuedStockId)),
                        'invtran_transfer_user_id' => $_SESSION['authId'],
                        'invtran_from_facility_id' => escapedff('facility'),
                        'invtran_to_facility_id' => escapedff('tofacility'),
                        'invtran_transfer_notes' => escapedff('form_desc_' . $issuedStockId),
                        'invtran_transfer_date' => date("Y-m-d"),
                        'oneivory_order_id' => $magorderid,
                        'oneivory_order_item_id' => $oneivory_order_item_id
                    );
                    $stockTransid = $pdoobject->insert("inv_transfer", $tranferData);
                    $returnItemStockDetails = getItemStockDetail($itemStockDetails['invist_id']);
                    $returnBeforeQty = $beforeQty + intval(escapedff('acceptqty_' . $issuedStockId));
                    $tranData = array(
                        'invistr_itemid' => $issuedItemId,
                        'invistr_batch' => $returnItemStockDetails['invist_batch'],
                        'invistr_expiry' => $returnItemStockDetails['invist_expiry'],
                        'invistr_price' => $returnItemStockDetails['invist_price'],
                        'invistr_quantity' => intval(escapedff('returnqty_' . $issuedStockId)),
                        'invistr_before_qty' => $returnBeforeQty,
                        'invistr_after_qty' => $returnBeforeQty,
                        'invistr_clinic_id' => $_POST['facility'],
                        'invistr_tran_type' => 18,
                        'invistr_comment' => escapedff('form_desc_' . $issuedStockId),
                        'invistr_createdby' => $_SESSION['authId'],
                        'invistr_created_date' => date("Y-m-d H:i:s"),
                        'invistr_action' => 'inventory/accept_issued_items',
                        'invistr_action_id' => $stockTransid
                    );
                    $transId = insertStockTransaction($tranData);
                }
                $updateIssueArr = array(
                    'iii_accept_quantity' => intval(escapedff('acceptqty_' . $issuedStockId)),
                    'iii_return_quantity' => intval(escapedff('returnqty_' . $issuedStockId)),
                    'comment' => escapedff('form_desc_' . $issuedStockId),
                );
                $updateIssueWhere = array('iii_id' => $issueItemIds[$countStockItems]);
                $updateIssueId = $pdoobject->update('inv_issue_item', $updateIssueArr, $updateIssueWhere);
            }
            $updateIssueNotesArr = array('iin_receivedby' => $_SESSION['authId'],
                'iin_received_date' => date("Y-m-d H:i:s"),
                'iin_status' => '1');

            $updateIssueId = $pdoobject->update('inv_issue_notes', $updateIssueNotesArr, array('iin_id' => escapedff('issueid')));
            $reqData = getReqItemQtyIssuedQty(escapedff('reqid'));
            if ($reqData['returnedqty']) {

                $endpoint = $GLOBALS['EMAIL_METHOD'] ? $GLOBALS['EMAIL_METHOD'] : 'SMTP';
                $query = "select subject,message from email_templates where name='ReturnAlert'";
                $tempData = $pdoobject->custom_query($query);
                $temp = $tempData[0]['message'];
                $subject = $tempData[0]['subject'];
                $query2 = "SELECT `User`.`nickname`, `User`.`fname`, `User`.`lname`, `User`.`email` FROM `users` AS `User` LEFT JOIN `gacl_aro_groups` AS `Group` ON (`User`.`group_id` = `Group`.`id`) LEFT JOIN `facility` AS `DefaultFacility` ON (`User`.`facility_id` = `DefaultFacility`.`id`)  WHERE `Group`.`value` IN ('invadmin', 'invuser') AND `User`.`authorized` = '1'";
                $res = $pdoobject->custom_query($query2);
                if ($pdoobject->custom_query($query2, null, 1)) {
                    foreach ($res As $user) {

                        $tempRep = "";
                        $tempSub = "";
                        $tempRep = str_replace("***USERNAME***", $user['nickname'], $temp);
                        $tempRep = str_replace("***FACILITYNAME***", $_REQUEST['tofacilityname'], $tempRep);  
                        $tempRep = str_replace("***TRANSACTION***", $_REQUEST['reqnumber'], $tempRep);
                        $tempRep = str_replace("***ORDERID***", $_REQUEST['magorderid'], $tempRep);
                        $tempSub = str_replace("***USERNAME***", $user['nickname'], $subject);
                        $tempSub = str_replace("***FACILITYNAME***", $_REQUEST['tofacilityname'], $tempSub);
                        $tempSub = str_replace("***TRANSACTION***", $_REQUEST['reqnumber'], $tempSub);
                        $tempSub = str_replace("***ORDERID***", $_REQUEST['magorderid'], $tempSub);
                        $pdoobject->custom_query_execute("INSERT INTO `emaillogs` SET `module_name`='return',`module_transaction_id`='" . escapedff('issueid') . "',`endpoint`='" . $endpoint . "',`subject`='" . $tempSub . "',`from`='" . json_encode(array('noreply@clovedental.in' => 'oneivory')) . "',`to`='" . $user['email'] . "',`cc`='" . json_encode(array()) . "',`bcc`='" . json_encode(array()) . "',`request`='" . $tempRep . "',`attachment_path`='" . json_encode(array()) . "',`status`='0',`created_by`='" . $_SESSION['authId'] . "',`created`='" . date("Y-m-d H:i:s") . "',`modified`='" . date("Y-m-d H:i:s") . "'");
                    }
                }
            }
            $pdoobject->commit();
            $info_msg = "<div class='alert alert-success alert-dismissable'>Requisition receipt saved successfully!</div>";
        } catch (Exception $ex) {
            $pdoobject->rollback();
            $info_msg ="<div class='alert alert-danger alert-dismissable'>Error in saving Data!</div>";
        }
        }else{
           $info_msg ="<div class='alert alert-danger alert-dismissable'>Error in saving Data!</div>"; 
        }

        if ($info_msg){
            $_SESSION['INV_MESSAGE'] = $info_msg;
        }

        // if ($info_msg)
        //     echo " alert('$info_msg');\n";

        echo "<script language='JavaScript'>\n";
        echo "parent.location.reload();\n";
        echo " window.close();\n";
        echo "</script></body></html>\n";
        exit();
    }
    ?>
    <body class="body_top">
        <?php
        $listSql = "SELECT iisn.iin_id, iisn.iin_reqid, istreq.isr_number,istreq.isr_magento_orderid, iisn.iin_from_clinic, iisn.iin_to_clinic, iisn.iin_number, iisn.iin_status, iisn.iin_isdeleted, iisn.iin_createdby, iisn.iin_date, frf.name AS fromFacility, tof.name AS toFacility " .
                "FROM inv_issue_notes AS iisn INNER JOIN inv_stock_requisition AS istreq ON istreq.isr_id = iisn.iin_reqid
		INNER JOIN facility AS frf ON frf.id = iisn.iin_from_clinic
		INNER JOIN facility AS tof ON tof.id = iisn.iin_to_clinic " .
                "WHERE iisn.iin_isdeleted = '0' AND iisn.iin_id = ? AND iisn.iin_to_clinic = ?";
        $row = $pdoobject->custom_query($listSql, array($issueid, $facility));
        $row = $row[0];
        ?>
        <div class="infopop"><?php xl('Accept/Return Item', 'e'); ?></div>
        <div class="popupTableWrp mt-0">
            <!-- <form method='post' id='theform' name='theform' > -->
                <table class="popupTable ui-table" cellspacing="0">
                    <thead>
                        <tr>
                            <th width='25%'><?php echo xlt('From Facility'); ?></th>
                            <th width='30%'><?php echo xlt('To Facility'); ?></th>
                            <th width='15%'><?php echo xlt('Issue#'); ?></th>
                            <th width='15%'><?php echo xlt('Requisition#'); ?></th>
                            <th width='15%'><?php echo xlt('Issue Status'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $row['toFacility']; ?></td>
                            <td><?php echo $row['fromFacility']; ?></td>
                            <td><?php echo $row['iin_number']; ?></td>
                            <td><?php echo $row['isr_number']; ?></td>
                            <td><?php
                                if ($row['iin_status'] == 1) {
                                    echo 'Completed';
                                } else {
                                    echo 'In Progress';
                                }
                                ?></td>
                        </tr>
                    </tbody>
                </table>
            <!-- </form> -->
        </div>
        <br />
        <div class="popupTableWrp mt-0">
            <form method='post' id='acceptReceiptForm' name='requisitionListForm' action='accept_issued_items.php?issueid=<?php echo $issueid; ?>&facility=<?php echo $facility; ?>'>
                <input type="hidden" name="issueid" value="<?php echo $issueid; ?>">
                <input type="hidden" name="reqid" value="<?php echo $row['iin_reqid']; ?>">
                <input type="hidden" name="facility" value="<?php echo $facility; ?>">
                <input type="hidden" name="tofacility" value="<?php echo $row['iin_from_clinic']; ?>">
                <input type="hidden" name="reqnumber" value="<?php echo $row['isr_number']; ?>">
                <input type="hidden" name="magorderid" value="<?php echo $row['isr_magento_orderid']; ?>">
                <input type="hidden" name="tofacilityname" value="<?php echo $row['toFacility']; ?>">
                <div id="requisitionListContainer">
                    <table class="popupTable ui-table" cellspacing="0">
                        <thead> 
                            <tr>					
                                <th width='15%'>Item</th>
                                <th width='15%'>Item Code</th>
                                <th width='10%'>Batch</th>
                                <th width='10%'>Expiry</th>
                                <th width='10%'>Price</th>
                                <th width='15%'>Quantity</th>
                                <th width='13%'>Accept Qty</th>
                                <th width='12%'>Return Qty</th>					
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT irit.iii_id, irit.iii_itemid, irit.oneivory_order_item_id, irit.iii_issueid, irit.iii_stock_id, irit.iii_quantity, irit.iii_isdeleted, im.inv_im_name, im.inv_im_code,  iist.invist_batch, iist.invist_expiry, iist.invist_price 
                                    FROM inv_issue_item AS irit INNER JOIN inv_item_stock AS iist ON iist.invist_id = irit.iii_stock_id 
                                    INNER JOIN inv_item_master AS im ON im.inv_im_id = irit.iii_itemid 
                                    WHERE irit.iii_issueid = ?";
                            $res = $pdoobject->custom_query($sql, array($issueid));
                            if ($pdoobject->custom_query($sql, array($issueid), 1)) {
                                foreach ($res as $itemrow) {
                                    ?>
                                <input type="hidden" name="issueItemId[]" value="<?php echo $itemrow['iii_id']; ?>">
                                <input type="hidden" name="issuedItems[]" value="<?php echo $itemrow['iii_itemid']; ?>">
                                <input type="hidden" name="issuedStocks[]" value="<?php echo $itemrow['iii_stock_id']; ?>">
                                <input type="hidden" id="issuedqty_<?php echo $itemrow['iii_stock_id']; ?>" name="issuedqty_<?php echo $itemrow['iii_stock_id']; ?>" value="<?php echo $itemrow['iii_quantity']; ?>">
                                <tr <?php
                                if ($itemrow['iii_isdeleted'] == 1) {
                                    echo 'class="strikeThrough"';
                                }
                                ?>>				

                                    <td>
                                        <?php echo $itemrow['inv_im_name']; ?>					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['inv_im_code']; ?>					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['invist_batch']; ?>					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['invist_expiry']=='0000-00-00' ? '' : $itemrow['invist_expiry']; ?>					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['invist_price']=='0.00' ? '' : $itemrow['invist_price']; ?>					
                                    </td>
                                    <td>
                                        <?php echo $itemrow['iii_quantity']; ?>					
                                    </td>
                                    <td><input type="number" size='10' maxlength='7' id="acceptqty_<?php echo $itemrow['iii_stock_id']; ?>" name="acceptqty_<?php echo $itemrow['iii_stock_id']; ?>" onkeypress="return isNumber(event);" onkeyup="checkReturnInputData(this);" value="<?php echo $itemrow['iii_quantity']; ?>">													
                                    </td>
                                    <td><input type="number" class='returnqtyfield' size='10' maxlength='7' id="returnqty_<?php echo $itemrow['iii_stock_id']; ?>" name="returnqty_<?php echo $itemrow['iii_stock_id']; ?>" onkeypress="return isNumber(event);" onkeyup="checkReturnInputData(this);" value="0">				
                                        <input type="hidden" size='10' id="oneivoryrtn_<?php echo $itemrow['iii_stock_id']; ?>" name="oneivoryrtn[]"  value="<?php echo $itemrow['iii_stock_id'] . '_' . $itemrow['oneivory_order_item_id']; ?>">								
                                    </td>
                                </tr>
                                <tr>
                                    <td><b><?php echo xlt('Comment'); ?>:</b></td>
                                    <td colspan="7"><textarea name='form_desc_<?php echo $itemrow['iii_stock_id']; ?>' class='reqReceiptNotes' rows="2" style="width:100%"></textarea></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                    <input type='submit' name='form_save' id='form_save' class="btn btn-warning btn-sm" value='dsfsdfsdfsdfsd' />
                </div>
            </form>
        </div>
        <script language="JavaScript">
<?php
if ($alertmsg) {
    echo "alert('" . htmlentities($alertmsg) . "');\n";
}
?></script>

    </body>
</html>