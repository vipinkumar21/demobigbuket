<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
//$thisauth = acl_check('inventory', 'invf_issuein_list');
if (!$invgacl->acl_check('inventory', 'invf_issuein_list','users', $_SESSION['authUser']))
    die(xlt('Not authorized'));
// For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
    'invtran_id' => 'invtran_id DESC'
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invtran_id';
$orderby = $ORDERHASH[$form_orderby];
//$facility  = $_POST['facility'];
$facility = $_REQUEST['facility'];
$reqid = $_REQUEST['reqid'];
if(empty($_SESSION['reqid'])){
    $_SESSION['reqid']=$reqid;
}
$reqnumber = $_REQUEST['reqnumber'];
if(empty($_SESSION['reqnumber'])){
    $_SESSION['reqnumber']=$reqnumber;
}
$magorderid = $_REQUEST['magorderid'];
$view_all= $_REQUEST['view_all'];

// get drugs
/* echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " .
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby"; */
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Issue Notes In'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        
    </head>
    <body>
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <div class="mrgnSpc mrgnSpc_oi floatRight">
                                <?php if($invgacl->acl_check('inventory', 'invf_stock_add','users',$_SESSION['authUser'])){?>
                                    <a href="requisitionFromClinic.php?facility=<?php echo attr($facility);?>" data-ajax="false" class="primary-button btn-right ui-link">
                                        <span class="icon-container">
                                            <span class="icon dummyIcon rqs"></span>
                                        </span>
                                        <b class="btn-text"><?php xl('Requisitions','e'); ?></b>
                                    </a>
                                <?php }?>
                            </div>
                            <h1><?php xl('Receipts', 'e'); ?></h1>
                        </div>
                         <div class="filterWrapper">
                             <form method='GET' action='issueNotesToClinic.php'  name='theform' id='theform'>
                                <!-- fourth column starts -->
                                <div class="ui-block">
                                    <input type='text' placeholder='Requisition Id' name='reqnumber' id="reqnumber" value='<?php echo $reqnumber ?>' title='' />
                                    <input type='hidden' name='facility' id="facility1" value='<?php echo $facility ?>' title='' />
                                    <input type='hidden' placeholder='' name='magorderid' id="magorderid" value='<?php echo $magorderid ?>' title='' />
                                </div>
                                 <div class="ui-block ui-chkbx">
                                        <label for="view_all" class="interButt"><?php xl('View All Receipts', 'e'); ?></label>
                                        <input type='checkbox' name='view_all' id="view_all" value='1' <?php echo (isset($view_all) && $view_all == 1) ? 'checked' : ''; ?> >
                                    </div> 
                                
                                <!-- third column ends -->
                                <!-- sixth column starts -->
                                <div class="ui-block wdth15">
                                    <a class="pull-right btn_bx" id='reset_form1' href="issueNotesToClinic.php?reqid=<?php echo $_SESSION['reqid']; ?>&facility=<?php echo $facility;?>&reqnumber=<?php echo $_SESSION['reqnumber'];?>&magorderid=<?php echo $magorderid;?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true"); $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                </form> 
                                <!-- sixth column ends -->
                            </div>
                        <!-- pageheading -->
                        <div>
                            <!-- Datatable -->
                           
                                <?php
                                if ($_REQUEST['facility'] || $_REQUEST['reqid']) {
                                    $res = "SELECT iisn.iin_id, iisn.iin_reqid, istreq.isr_number, iisn.iin_from_clinic, iisn.iin_to_clinic, iisn.iin_number, iisn.iin_status, iisn.iin_isdeleted, iisn.iin_createdby, iisn.iin_date, frf.name AS fromFacility, tof.name AS toFacility, iisn.iin_received_date as receipt  " .
                                            "FROM inv_issue_notes AS iisn INNER JOIN inv_stock_requisition AS istreq ON istreq.isr_id = iisn.iin_reqid
                                            INNER JOIN facility AS frf ON frf.id = iisn.iin_from_clinic
                                            INNER JOIN facility AS tof ON tof.id = iisn.iin_to_clinic " .
                                            "WHERE iisn.iin_isdeleted = '0'";

                                    if (!empty($facility)) { // if facility exists
                                        $res .= " AND iisn.iin_to_clinic = '" . $facility . "'";
                                    }
                                    if (!empty($reqid)) { // If from dates exists
                                        $res .= " AND iisn.iin_reqid = '$reqid'";
                                    }
                                    if (!empty($reqnumber)) {
                                        $res .= " AND istreq.isr_number LIKE '%".$reqnumber."%'";
                                    }
                                    
                                    $res .= " ORDER BY iisn.iin_id DESC";
                                    //echo $res;
                                    $num_rows = $pdoobject->custom_query($res,null,1); // total no. of rows
                                    $per_page = $GLOBALS['encounter_page_size'];
                                    $page = $_GET["page"];
                                    $curr_URL = $_SERVER['SCRIPT_NAME'] . "?facility=$facility&reqid=$reqid&reqnumber=$reqnumber&magorderid=$magorderid&";
                                    $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                    ?>
                                    <div id='' class='tableWrp pb-2'>
                                        <!-- pagination -->
                                        <?php if ($num_rows > 0) { ?>
                                            <?php echo $pa_data; ?>
                                        <?php } ?>
                                        <!-- pagination -->
                                        <div class='dataTables_wrapper no-footer'>
                                            <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                <?php
                                                $pageCount = ($page == 0 ? 1 : $page);
                                                $page_start = ($pageCount - 1) * $per_page;
                                                $res .= " LIMIT $page_start , $per_page";
                                                $result = $pdoobject->custom_query($res);
                                                $lastid = "";
                                                $encount = 0;

                                                if (!empty($result)) {?>
                                                <thead>
                                                    <tr>
                                                        <th width="15%">
                                                            <?php echo xlt('Requisition#'); ?>
                                                        </th>
                                                        <th width="15%">
                                                            <?php echo xlt('From Facility'); ?>
                                                        </th>
                                                        <th width="15%">
                                                            <?php echo xlt('To Facility'); ?>
                                                        </th>
                                                        <th width="10%">
                                                            <?php echo xlt('Issue#'); ?>
                                                        </th>
                                                        <th width="12%">
                                                            <?php echo xlt('Issue Status'); ?>
                                                        </th>
                                                        <th width="10%">
                                                            <?php echo xlt('Issue Date'); ?>
                                                        </th>
                                                        <th width="10%">
                                                            <?php echo xlt('Receipt Date'); ?>
                                                        </th>
                                                        <th width="10%" class='txtCenter'>
                                                            <?php echo xlt('Action'); ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <?php
                                                
                                                    foreach ($result as $key => $row) {
                                                        ++$encount;
                                                        $bgcolor = "#" . (($encount & 1) ? "f7d4be" : "f1f1f1");
                                                        $lastid = $row['iin_id'];
                                                        if ($row['iin_isdeleted'] == 1) {
                                                            echo "<tr class='detail strikeThrough'>\n";
                                                        } else {
                                                            echo "<tr class='detail' role='row'>\n";
                                                        }
                                                        //echo "  <td>" .$encount. "</td>\n";
                                                        echo "  <td>" . text($row['isr_number']) . "</td>\n";
                                                        echo "  <td>" . text($row['toFacility']) . "</td>\n";
                                                        echo "  <td>" . text($row['fromFacility']) . "</td>\n";
                                                        echo "  <td>" . text($row['iin_number']) . "</td>\n";
                                                        echo "  <td>";
                                                        if ($row['iin_status'] == 1) {
                                                            echo 'Completed';
                                                        } else {
                                                            echo 'In Progress';
                                                        }
                                                        echo "</td>\n";
                                                        echo "  <td>" . text(date($datePhpFormat, strtotime($row['iin_date']))) . "</td>\n";
                                                        $rsdate= !empty($row['receipt']) ? text(date($datePhpFormat, strtotime($row['receipt']))) : '&nbsp;';
                                                        echo "  <td> " .$rsdate. " </td>\n";
                                                        echo "  <td>";
                                                        if ($row['iin_status'] == 1) {
                                                        if ($invgacl->acl_check('inventory', 'invf_issuein_view','users',$_SESSION['authUser'])) {
                                                            echo "<div class='listbtn viewTableDataList' data-pageUrl='view_issue_in.php?issueid=" . attr($lastid) . "&facility=" . attr($facility) . "'><span class='dashboardViewIcon'></span>View</div> &nbsp; ";
                                                        }}
                                                        if ($row['iin_status'] != 1) {
                                                            if ($invgacl->acl_check('inventory', 'invf_issuein_recpt','users',$_SESSION['authUser'])) {
                                                                echo "<div style='width: 38px' class='listbtn addEditTableData' data-pageUrl='accept_issued_items.php?issueid=" . attr($lastid) . "&facility=" . attr($facility) . "&reqnumber=" . $row['isr_number'] . "'><span class='dashboardApproveIcon'></span>Accept</div> &nbsp;  ";
                                                            }
                                                        }
                                                        echo "</td>\n";
                                                        echo " </tr>\n";
                                                    } // end while
                                                } else {
                                                    ?>
                                                    <tr>
                                                    <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                    </tr>
                                                    <?php
                                                } // end If
                                                ?>
                                            </table>
                                        </div>
                                        <!-- pagination -->
                                        <?php if ($num_rows > 0) { ?>
                                            <?php echo $pa_data; ?>
                                        <?php } ?>
                                        <!-- pagination -->
                                    </div>
                                <?php } else { ?>
                                    <div class='text dnone'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                                    </div>
                                <?php } ?>
                                <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                                <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                           
                            <!-- Datatable -->
                        </div>
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
        <!-- page -->
        <a style='margin-top:-6px' class='btn dnone btn-default btn-sm newfloatright' href='requisitionFromClinic.php?facility=<?php echo attr($facility); ?>'>Request Out</a></h3>
    <script>
        
        $(document).ready(function() { 
   check();
    //code
})
        
        $('#view_all').change(function() {
            check();
        });
        
        function check(){
            if ($('#view_all').is(':checked')) {
              $("#reqnumber").attr("disabled", "disabled"); 
              $('#reqnumber').val('');
            } else {
              $("#reqnumber").removeAttr("disabled"); 
              //$('#reqnumber').val('');
            }
        }
        </script>
</body>
</html>
