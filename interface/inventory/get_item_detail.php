<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.
require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
$postData = json_decode($_POST);
//print_r($_REQUEST);
$itemquery = "SELECT inv_im_id, inv_im_isExpiry, inv_im_sale FROM inv_item_master WHERE inv_im_status='1' AND inv_im_deleted='0' AND inv_im_id = '".$_REQUEST['itemId']."'";
$itemres = sqlStatement($itemquery);
$itemrow = sqlFetchArray($itemres);
if(!empty($itemrow)){
	$itemData = array();
	$itemData['expiry'] = $itemrow['inv_im_isExpiry'];
	$itemData['sale'] = $itemrow['inv_im_sale'];	
	echo json_encode(array('status' => 1, 'msg' => 'fetched item data', 'itemData' => $itemData));
}else{
	echo json_encode(array('status' => 0, 'msg' => 'not fetched item data', 'itemData' => array()));
}

?>