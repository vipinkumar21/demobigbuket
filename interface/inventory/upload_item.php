<?php
 /**
*
* Upload Item CSV : (Swati Jain)
*
**/

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 
 if(!strpos($_SERVER['HTTP_REFERER'],"upload_csv_process")){	// Reset CSV uploaded msg display
 	$_SESSION['msg'] = '';
 }
 if ($_POST['form_csv_sample'] && $_POST['form_csv_sample']=='sample') {	// Sample CSV Format
 	header("Pragma: public");
 	header("Expires: 0");
 	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 	header("Content-Type: application/force-download");
 	header("Content-Disposition: attachment; filename=item.csv");
 	header("Content-Description: File Transfer");
 	echo xl('Item Name') . ',';
 	echo xl('Code') . ',';
 	echo xl('Category') . ',';
 	echo xl('SubCategory') . ',';
 	echo xl('Description') . ',';
 	echo xl('Uom') . ',';
 	echo xl('Saleable') . ','; 	
 	echo '"' . xl('Expirable') . '"' . "\n";
 	echo '"Gloves Surgical No. 6.5-Surgicare",';
 	echo '"DD00026",';
 	echo '"Disposables",';
 	echo '"gloves",';
 	echo '"Gloves Surgical No. 6.5-Surgicare Description",';
 	echo '"Pair",';
 	echo '"0",';	
 	echo '"1",'."\n";
 	
 	echo '"Barbed Broaches No.20-60-Dentsply",';
 	echo '"DC00545",';
 	echo '"consumables",';
 	echo '"Broaches",';
 	echo '"Barbed Broaches No.20-60-Dentsply Description",';
 	echo '"Pack",';
 	echo '"0",';
 	echo '"1",'."\n";
 	exit;
 }
?>
<html>
<head>
<?php html_header_show();?>
<title><?php xl('Item CSV Upload','e'); ?></title>
 <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script> 
 <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script type="text/javascript">
// On UOM CSV upload
function submitItemCsv() {	
	
	if (document.forms['itemInsert'].item_file.value.length>0) {
		top.restoreSession();
		document.forms['itemInsert'].submit();
	} else {
		if (document.forms['itemInsert'].item_file.value.length<=0)
		{
			document.forms['itemInsert'].item_file.style.backgroundColor="red";
			//alert("<?php xl('Required field missing: Please browse the Item file','e');?>");
			 jAlert ('Required field missing: Please browse the Item file', 'Alert');
			document.forms['itemInsert'].item_file.focus();
			return false;
		}		
	}
}
</script>
<link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<style type="text/css">

/* specifically include & exclude from printing */
@media print {
    #report_parameters {
        visibility: hidden;
        display: none;
    }
    #report_parameters_daterange {
        visibility: visible;
        display: inline;
		margin-bottom: 10px;
    }
    #report_results table {
       margin-top: 0px;
    }
}

/* specifically exclude some from the screen */
@media screen {
	#report_parameters_daterange {
		visibility: hidden;
		display: none;
	}
	#report_results {
		width: 100%;
	}
}

</style>

</head>
<body class="body_top">
<div class="panel panel-warning">
    <div class="panel-heading boldtxt"><?php xl('Import','e'); ?> - <?php xl('Item CSV','e'); ?></div>

 <div class="panel-body">
<form name='itemInsert' id='itemInsert' method='post' enctype='multipart/form-data' action='upload_csv_process.php'>
<input type='hidden' name='mode' id='mode' value='import_item'/>
<div class="row">
  <div class="col-xs-4"><?php xl('Item','e'); ?>: <input type="file" name="item_file" style="display:inline-block;"></div>
  <div class="col-xs-4" id="mergePatientDataButton">
  <a href='#' class='btn btn-warning btn-sm' onclick='return submitItemCsv();'>
					<span>
						<?php xl('Upload Item','e'); ?>
					</span>
					</a>	
  </div>
      
</div>

</form>
 <!-- end of parameters -->
<form name='itemcsv' id='itemcsv' method='post' enctype='multipart/form-data' action='upload_item.php'>
<input type='hidden' name='form_csv_sample' id='form_csv_sample' value=''/>
<div class="row">
  <div class="col-xs-4">Note: Please Find Sample CSV Format</div>
  <div class="col-xs-4"><a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csv_sample").attr("value","sample");$("#itemcsv").submit();'>
<span><?php xl('Sample CSV','e'); ?></span>
</a>	</div>
  <div><?php echo $_SESSION['msg'];?></div>
</div>

</form>
</div>
</div>