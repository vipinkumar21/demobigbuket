<?php

// Copyright (C) 2014 Virendra Kumar Dubey <virendra.dubey@instantsys.com>

$substitute_array = array('', xl('Allowed'), xl('Not Allowed'));

function send_drug_email($subject, $body) {
    require_once ($GLOBALS['srcdir'] . "/classes/class.phpmailer.php");
    $recipient = $GLOBALS['practice_return_email_path'];
    if (empty($recipient))
        return;
    $mail = new PHPMailer();
    $mail->SetLanguage("en", $GLOBALS['fileroot'] . "/library/");
    $mail->From = $recipient;
    $mail->FromName = 'In-House Pharmacy';
    $mail->isMail();
    $mail->Host = "localhost";
    $mail->Mailer = "mail";
    $mail->Body = $body;
    $mail->Subject = $subject;
    $mail->AddAddress($recipient);
    if (!$mail->Send()) {
        error_log("There has been a mail error sending to " . $recipient .
                " " . $mail->ErrorInfo);
    }
}

function sellDrug($drug_id, $quantity, $fee, $patient_id = 0, $encounter_id = 0, $prescription_id = 0, $sale_date = '', $user = '', $default_warehouse = '') {

    if (empty($patient_id))
        $patient_id = $GLOBALS['pid'];
    if (empty($sale_date))
        $sale_date = date('Y-m-d');
    if (empty($user))
        $user = $_SESSION['authUser'];

    // error_log("quantity = '$quantity'"); // debugging

    if (empty($default_warehouse)) {
        // Get the default warehouse, if any, for the user.
        $rowuser = sqlQuery("SELECT default_warehouse FROM users WHERE username = ?", array($user));
        $default_warehouse = $rowuser['default_warehouse'];
    }

    // Get relevant options for this product.
    $rowdrug = sqlQuery("SELECT allow_combining, reorder_point, name " .
            "FROM drugs WHERE drug_id = ?", array($drug_id));
    $allow_combining = $rowdrug['allow_combining'];

    // Combining is never allowed for prescriptions and will not work with
    // dispense_drug.php.
    if ($prescription_id)
        $allow_combining = 0;

    $rows = array();
    // $firstrow = false;
    $qty_left = $quantity;
    $bad_lot_list = '';
    $total_on_hand = 0;

    // If the user has a default warehouse, sort those lots first.
    $orderby = ($default_warehouse === '') ?
            "" : "di.warehouse_id != '$default_warehouse', ";
    $orderby .= "lo.seq, di.expiration, di.lot_number, di.inventory_id";

    // Retrieve lots in order of expiration date within warehouse preference.
    $res = sqlStatement("SELECT di.*, lo.option_id, lo.seq " .
            "FROM drug_inventory AS di " .
            "LEFT JOIN list_options AS lo ON lo.list_id = 'warehouse' AND " .
            "lo.option_id = di.warehouse_id " .
            "WHERE " .
            "di.drug_id = ? AND di.destroy_date IS NULL " .
            "ORDER BY $orderby", array($drug_id));

    // First pass.  Pick out lots to be used in filling this order, figure out
    // if there is enough quantity on hand and check for lots to be destroyed.
    while ($row = sqlFetchArray($res)) {
        // Warehouses with seq > 99 are not available.
        $seq = empty($row['seq']) ? 0 : $row['seq'] + 0;
        if ($seq > 99)
            continue;

        $on_hand = $row['on_hand'];
        $expired = (!empty($row['expiration']) && $row['expiration'] <= $sale_date);
        if ($expired || $on_hand < $quantity) {
            $tmp = $row['lot_number'];
            if (!$tmp)
                $tmp = '[missing lot number]';
            if ($bad_lot_list)
                $bad_lot_list .= ', ';
            $bad_lot_list .= $tmp;
        }
        if ($expired)
            continue;

        /*         * ***************************************************************
          // Note the first row in case total quantity is insufficient and we are
          // allowed to go negative.
          if (!$firstrow) $firstrow = $row;
         * *************************************************************** */

        $total_on_hand += $on_hand;

        if ($on_hand > 0 && $qty_left > 0 && ($allow_combining || $on_hand >= $qty_left)) {
            $rows[] = $row;
            $qty_left -= $on_hand;
        }
    }

    if ($bad_lot_list) {
        send_drug_email("Possible lot destruction needed", "The following lot(s) are expired or were too small to fill the " .
                "order for patient $patient_id: $bad_lot_list\n");
    }

    /*     * *****************************************************************
      if (empty($firstrow)) return 0; // no suitable lots exist
      // This can happen when combining is not allowed.  We will use the
      // first row and take it negative.
      if (empty($rows)) {
      $rows[] = $firstrow;
      $qty_left -= $firstrow['on_hand'];
      }date
     * ***************************************************************** */

    // The above was an experiment in permitting a negative lot quantity.
    // We decided that was a bad idea, so now we just error out if there
    // is not enough on hand.
    if ($qty_left > 0)
        return 0;

    $sale_id = 0;
    $qty_final = $quantity; // remaining unallocated quantity
    $fee_final = $fee;      // remaining unallocated fee
    // Second pass.  Update the database.
    foreach ($rows as $row) {
        $inventory_id = $row['inventory_id'];

        /*         * ***************************************************************
          $thisqty = $row['on_hand'];
          if ($qty_left > 0) {
          $thisqty += $qty_left;
          $qty_left = 0;
          }
          else if ($thisqty > $qty_final) {
          $thisqty = $qty_final;
          }
         * *************************************************************** */
        $thisqty = min($qty_final, $row['on_hand']);

        $qty_final -= $thisqty;

        // Compute the proportional fee for this line item.  For the last line
        // item take the remaining unallocated fee to avoid round-off error.
        if ($qty_final)
            $thisfee = sprintf('%0.2f', $fee * $thisqty / $quantity);
        else
            $thisfee = sprintf('%0.2f', $fee_final);
        $fee_final -= $thisfee;

        // Update inventory and create the sale line item.
        sqlStatement("UPDATE drug_inventory SET " .
                "on_hand = on_hand - ? " .
                "WHERE inventory_id = ?", array($thisqty, $inventory_id));
        $sale_id = sqlInsert("INSERT INTO drug_sales ( " .
                "drug_id, inventory_id, prescription_id, pid, encounter, user, sale_date, quantity, fee ) " .
                "VALUES (?,?,?,?,?,?,?,?,?)", array($drug_id, $inventory_id, $prescription_id, $patient_id, $encounter_id, $user, $sale_date, $thisqty, $thisfee));
    }

    // If appropriate, generate email to notify that re-order is due.
    if (($total_on_hand - $quantity) <= $rowdrug['reorder_point']) {
        send_drug_email("Product re-order required", "Product '" . $rowdrug['name'] . "' has reached its reorder point.\n");
    }

    // If combining is allowed then $sale_id will be just the last inserted ID,
    // and it serves only to indicate that everything worked.  Otherwise there
    // can be only one inserted row and this is its ID.
    return $sale_id;
}

function checkCatStatus($cat_id) {
    $row = sqlQuery("SELECT COUNT(cat.id) AS count FROM invcategories AS cat  WHERE " .
            "cat.deleted = '0' AND cat.id = ?", array($cat_id));
    $imrow = sqlQuery("SELECT COUNT(cat.id) AS count FROM invcategories AS cat INNER JOIN invcategories_invitemmasters AS im ON im.inv_im_id =cat.id WHERE " .
            "cat.deleted = '0' AND cat.id = ?", array($cat_id));
    if ($row['count'] || $imrow['count']) {
        return 0;
    } else {
        return 1;
    }
}

function checkCatItem($cat_id) {
    $row = sqlQuery("SELECT 
  count(*) as count  
  FROM invcategories_invitemmasters AS im 
  INNER JOIN invcategories AS cat 
    ON cat.id = im.inv_im_id 
  WHERE im.inv_im_id = ?", array($cat_id));


    if ($row['count']) {
        return 0;
    } else {
        return 1;
    }
}

function checkSubCatStatus($scat_id) {
    /*
      $catRow = sqlQuery("SELECT COUNT(scat.id) AS count FROM invcategories AS scat WHERE scat.deleted = '0' and  scat.id = ?", array($scat _id));
      $row = sqlQuery("SELECT COUNT(scat.id) AS count FROM invcategories AS scat INNER JOIN invcategories_invitemmasters AS im ON im.inv_im_id =scat.id WHERE " .
      "im.deleted = '0' AND scat.id = ?", array($scat_id));
      if ($row['count'] || $catRow['COUNT']) {
      return 0;
      } else {
      return 1;
      }
     */
}

function checkUomStatus($uom_id) {
    $row = sqlQuery("SELECT COUNT(uom.invuom_id) AS count FROM inv_uom AS uom INNER JOIN inv_item_master AS im ON im.inv_im_uomId =uom.invuom_id WHERE " .
            "im.inv_im_deleted = '0' AND uom.invuom_id = ?", array($uom_id));
    if ($row['count']) {
        return 0;
    } else {
        return 1;
    }
}

function getItemStockDetail($itemStockId, $flag=0, $string=0) {
    $sql="SELECT im.oneivory_id, ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name FROM inv_item_stock AS ist INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0'";
if(!$flag){
    $sql.=" AND ist.invist_quantity > 0";  
}   
    $sql.=" AND ist.invist_id = ?";
    $sql.= empty($string) ? '': $string;
    $row = $GLOBALS['pdoobject']->custom_query($sql, array($itemStockId),'',"fetch");
    if (!empty($row)) {
        return $row;
    } else {
        return array();
    }
}

function getUserStockDetail($userStockId , $flag=0) {
    $sql = "SELECT iir.invir_id,iir.invir_item_id, iir.invir_stock_id, iir.invir_quantity, iir.invir_return_qty, iir.invir_cons_qty, ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name " .
            "FROM inv_issue_return AS iir INNER JOIN inv_item_stock AS ist ON ist.invist_id = iir.invir_stock_id 
		INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid  
                WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0'"; 
    
    if(!$flag){
    $sql.=" AND ist.invist_quantity > 0";  
}   
    $sql.=" AND iir.invir_id = ?";
                     
//AND ist.invist_quantity > 0 AND iir.invir_id = ?" ;
    
    
    
    $row = $GLOBALS['pdoobject']->custom_query($sql, array($userStockId),'',"fetch");

    if (!empty($row)) {
        return $row;
    } else {
        return array();
    }
}

function getClincItemStockDetail($itemId, $batchNo, $expiry, $price, $clinicId) {
    $row = $GLOBALS['pdoobject']->custom_query("SELECT ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name " .
            "FROM inv_item_stock AS ist
		INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid  		
                WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invist_quantity > 0 AND ist.invist_itemid= ? AND ist.invist_batch=? AND " . (!empty($expiry) ? "ist.invist_expiry= DATE('". $expiry."')" : 'ist.invist_expiry IS NULL') . " AND " . (!empty($price) ? "ist.invist_price = " . $price : 'ist.invist_price IS NULL') . " AND ist.invist_clinic_id=?", array($itemId, $batchNo, $clinicId));
    if (!empty($row)) {
        return $row;
    } else {
        return array();
    }
}

function getUserIssueStockDetail($transId) {
    $row = sqlQuery("SELECT iist.invistr_id, iist.invistr_itemid, iist.invistr_batch, iist.invistr_expiry, iist.invistr_price, iist.invistr_quantity, iist.invistr_clinic_id, iist.invistr_isdeleted, iist.invistr_createdby, im.inv_im_name, iir.invir_id, iir.invir_stock_id, iir.invir_quantity, iir.invir_return_qty, iir.invir_cons_qty  
		FROM inv_item_stock_transaction AS iist
		INNER JOIN inv_user_issue_consumption AS iuic ON iuic.iuic_tranid = iist.invistr_id 
		INNER JOIN inv_issue_return AS iir ON iir.invir_id = iuic.iuic_issueid 
		INNER JOIN inv_item_master AS im ON im.inv_im_id = iist.invistr_itemid	
		WHERE im.inv_im_deleted = '0' AND iist.invistr_id = ?", array($transId));
    if (!empty($row)) {
        return $row;
    } else {
        return array();
    }
}

function insertUpdateItemStock($data) {
   
    
    $row = $GLOBALS['pdoobject']->custom_query("SELECT ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name
			FROM inv_item_stock AS ist
			INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid                        	
                        WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invist_quantity > 0 AND ist.invist_itemid= ? AND ist.invist_batch=? AND ist.invist_expiry=? AND " . (!empty($data['invist_price']) ? "ist.invist_price = " . $data['invist_price'] : '0.00') . " AND ist.invist_clinic_id=?", array($data['invist_itemid'], $data['invist_batch'], $data['invist_expiry'], $data['invist_clinic_id']),'','fetch');
    if (!empty($row)) {
         $stockData = array(
                            'invist_quantity' => $row['invist_quantity']+$data['invist_quantity'],
                            'invist_createdby' => $data['invist_createdby']
                    );
                    $where = array('invist_id'=>$row['invist_id']);
                    
                    $sresult=$GLOBALS['pdoobject']->update("inv_item_stock",$stockData,$where);
        
        /*sqlStatement("UPDATE inv_item_stock SET 
			invist_quantity = invist_quantity+" . $data['invist_quantity'] . ", 
  			invist_createdby = " . $data['invist_createdby'] . " 
			WHERE invist_id=? AND invist_itemid= ? AND invist_batch=? AND invist_expiry=? AND " . (!empty($data['invist_price']) ? "invist_price = " . $data['invist_price'] : '0.00') . " AND invist_clinic_id=?", array($row['invist_id'], $data['invist_itemid'], $data['invist_batch'], $data['invist_expiry'], $data['invist_clinic_id']));*/
        //return $row['invist_id'];
        return array('invist_id'=>$row['invist_id'],'invist_itemid'=>$row['invist_itemid'],'invist_batch'=>$row['invist_batch'],'invist_expiry'=>$row['invist_expiry'],'invist_price'=>$row['invist_price'],'invist_quantity'=>$row['invist_quantity']);
    } else {
        /*if (empty($data['invistr_price']) || $data['invistr_price'] == '') {
            unset($data['invistr_price']);
        }*/
        $stockid = $GLOBALS['pdoobject']->insert('inv_item_stock', $data);
        
        return array('invist_id'=>$stockid,'invist_itemid'=>$data['invist_itemid'],'invist_batch'=>$data['invist_batch'],'invist_expiry'=>$data['invist_expiry'],'invist_price'=>$data['invist_price'],'invist_quantity'=>'0');
        //return $stockid;
    }
}

function insertStockReq($data, $cond = null) {

    if (empty($cond)) {
        $data['isr_number'] = checkReqNumExist($data['isr_number'], $data['isr_from_clinic']);
        $requId = $GLOBALS['pdoobject']->insert('inv_stock_requisition', $data);
    } else {
        $whereCond = array("isr_id" => $cond);
        $GLOBALS['pdoobject']->update('inv_stock_requisition', $data, $whereCond);
    }
    return $requId;
}

function insertUpdateReqItems($allData) {
    if (!empty($allData)) {
        foreach ($allData as $data) {
            if (!empty($data['iri_id'])) {
                $whereCond = array("iri_id" => $data['iri_id']);
                $GLOBALS['pdoobject']->update('inv_requisition_item', $data, $whereCond);
            } else {
                $GLOBALS['pdoobject']->insert('inv_requisition_item', $data);
                //return $requId;
            }
        }
    }
}

function checkReqNumExist($reqNum, $clinicId) {
    $row = $GLOBALS['pdoobject']->custom_query("SELECT COUNT(isr_id) AS reqnum FROM inv_stock_requisition WHERE isr_number = '" . $reqNum . "'");
    if ($row[0]['reqnum'] > 0) {
        return getReqNumber($clinicId);
    } else {
        return $reqNum;
    }
}

function getReqNumber($clinicId,$string="") {
    $row = $GLOBALS['pdoobject']->custom_query("SELECT COUNT(isr_id) AS reqnum FROM inv_stock_requisition WHERE isr_from_clinic = " . $clinicId.$string);
    $facilityRow = $GLOBALS['pdoobject']->custom_query("SELECT id, facility_npi, mask_patient_id_pre FROM facility WHERE id = " . $clinicId);
    $reqNumber = '';
    if (!empty($facilityRow[0]['facility_npi'])) {
        $reqNumber = 'REQ-' . $facilityRow[0]['facility_npi'] . '-' . ($row[0]['reqnum'] + 1);
    } else {
        $reqNumber = 'REQ-' . $facilityRow[0]['mask_patient_id_pre'] . '-' . ($row[0]['reqnum'] + 1);
    }
    return $reqNumber;
}

function getUserGroup($what = NULL) {
    $what = (is_null($what)) ? 'name' : $what;
    $sql= "SELECT u.id, gag.$what AS groupname FROM users AS u INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id
                            WHERE u.id = ?";
    $res = $GLOBALS['pdoobject']->custom_query($sql, array($_SESSION['authId']));
    $userGroupData = array();
    if ($GLOBALS['pdoobject']->custom_query($sql, array($_SESSION['authId']),1)) {
        foreach($res as $key =>$row) {
            $userGroupData[$key] = $row['groupname'];
        }
    }
    return $userGroupData;
}

function checkReqIssueStatus($reqId) {
    $row = sqlQuery("SELECT COUNT(iin_id) AS issuenum FROM inv_issue_notes WHERE iin_reqid = " . $reqId);
    if ($row['issuenum'] > 0) {
        return false;
    } else {
        return true;
    }
}

//Fisrt Nested
function getStockLedgelDetail($whereClause, $dateMysqlFormat, $flag = 0, $reportlog = null, $zeroItem = null, $sellable, $facility, $fromDate, $toDate, $provider, $transactionType, $callType, $page) {
    //Get clinic based on stock transaction 
    
    if (empty($reportlog)) {
        $res = "SELECT f.name AS clinicname, invistr.invistr_clinic_id, invistr.invistr_itemid, invistr.invistr_batch, im.inv_im_code, CONCAT_WS('#', im.inv_im_code, invistr.invistr_batch, invistr.invistr_expiry, invistr.invistr_price) AS stockKey
                FROM inv_item_stock_transaction AS invistr
                INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
                INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
                LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
                WHERE  " . $whereClause . " AND invistr.invistr_isdeleted = '0' GROUP BY clinicname ORDER BY invistr.invistr_created_date ASC ";
        $sql_num = sqlStatement($res,null, $GLOBALS['adodb']['dbreport']);
        $num_rows = sqlNumRows($sql_num); // total no. of rows
        $per_page =$GLOBALS['encounter_page_size'];
        $curr_URL = $_SERVER['SCRIPT_NAME'] . '?form_refresh=viewStockLedger &' . 'form_facility=' . $facility . '&form_from_date=' . $fromDate . '&form_to_date=' . $toDate . '&form_sellable=' .$sellable . '&form_provider=' .$provider .'&form_transaction_type=' .$transactionType .'&zero_quantity_items=' .$zeroItem .'&';
        $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);  
        $page = ($page == 0 ? 1 : $page);
        $page_start = ($page - 1) * $per_page;
        if($callType ==1 && $flag==0)
        {
          $res.= " LIMIT $page_start , $per_page";
        }
        $res = sqlStatement($res,null, $GLOBALS['adodb']['dbreport']);
        $result = array();
        if (sqlNumRows($res)) {
            while ($row = sqlFetchArray($res)) {

                if ($flag) {
                    $firstNestedData = getItemtransactionBalanceBasedOnClinic($row['invistr_clinic_id'], $row['invistr_itemid'], $row['invistr_batch'], $row['inv_im_code'], $whereClause, $dateMysqlFormat, $zeroItem, $callType, $page);
                } else {
                    $firstNestedData = getItemtransactionBasedOnClinic($row['invistr_clinic_id'], $whereClause, $dateMysqlFormat, $zeroItem, $callType, $page);
                }
                $pagination = (!empty($firstNestedData['paginationData'])) ? $firstNestedData['paginationData'] : $pa_data;
                if(!empty($firstNestedData['paginationData']))
                unset($firstNestedData['paginationData']);
                $stockLedgerData[] = array(
                    'item_id' => $row['invistr_itemid'],
                    'clinic_name' => $row['clinicname'],
                    'item_name' => $row['inv_im_name'],
                    'item_code' => $resNested['inv_im_code'],
                    'item_stock_batch' => $row['invistr_batch'],
                    'item_stock_expiry' => $row['invistr_batch'],
                    'opening_stock_balance_running_qty' => '0',
                    'item_transaction_type' => 'Opening Balance',
                    'transaction_date' => $row['invistr_created_date'],
                    'transaction_type' => $row['invtrt_name'],
                    'clinicWiseDetail' => $firstNestedData
                );
                $result = array('paginationData' => $pagination, 'reportResult' => $stockLedgerData);
            }
        }
    } else {

        $result = array('paginationData' => $pagination, 'reportResult' => $reportlog);
        //$stockLedgerData = $reportlog;
    }
    return $result;
}

//Third Nested Section
function getStockTransactionBasedOnItem($clinicId, $itemId, $whereClause, $dateMysqlFormat) {
    $secondNestedData = array();
    //Get stock transaction based on item
    $resSecondNested = sqlStatement("SELECT invistr.*, DATE_FORMAT(invistr.invistr_created_date , '" . $dateMysqlFormat . "') AS formatcreateddate, DATE_FORMAT(invistr.invistr_expiry , '" . $dateMysqlFormat . "') AS formatExpiryDate, im.inv_im_name, im.inv_im_code, invtrt.invtrt_name, invtrt.invtrt_color_name, invtrt.invtrt_color, f.name AS clinicname, u.fname, u.lname, u.nickname
							FROM inv_item_stock_transaction AS invistr INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
							INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
							LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
							LEFT JOIN `users` AS u ON u.id = invistr.invistr_createdby
							WHERE invistr.invistr_isdeleted = '0' AND invistr.invistr_clinic_id = " . $clinicId . " AND invistr_itemid = " . $itemId . " " . $whereClause . " ORDER BY invistr.invistr_created_date ASC ");
    if (sqlNumRows($resSecondNested)) {
        while ($rowSecondNested = sqlFetchArray($resSecondNested)) {
            $secondNestedData[] = array(
                'item_id' => $rowSecondNested['invistr_itemid'],
                'item_name' => $rowSecondNested['inv_im_name'],
                'item_code' => $rowSecondNested['inv_im_code'],
                'item_stock_batch' => $rowSecondNested['invistr_batch'],
                'item_stock_expiry' => $rowSecondNested['formatExpiryDate'],
                'item_stock_price' => $rowSecondNested['invistr_price'],
                'item_stock_qty' => $rowSecondNested['invistr_quantity'],
                'item_stock_running_qty' => $rowSecondNested['invistr_after_qty'],
                'item_transaction_date' => $rowSecondNested['formatcreateddate'],
                'item_transaction_type' => $rowSecondNested['invtrt_name'],
                'item_clinic_name' => $rowSecondNested['clinicname'],
                'item_received_by' => $rowSecondNested['nickname'],
                'item_transaction_color_name' => $rowSecondNested['invtrt_color_name'],
                'item_transaction_color_code' => $rowSecondNested['invtrt_color']
            );
        }
    }
    return $secondNestedData;
}

function getStockTransactionBasedOnItemNew($clinicId, $itemId, $itemBatch, $itemCode, $itemExpiry, $itemPrice, $whereClause, $dateMysqlFormat) {
    $secondNestedData = array();
    //Get stock transaction based on item

    $resSecondNested = sqlStatement("SELECT invistr.*, DATE_FORMAT(invistr.invistr_created_date , '" . $dateMysqlFormat . "') AS formatcreateddate, DATE_FORMAT(invistr.invistr_expiry , '" . $dateMysqlFormat . "') AS formatExpiryDate, im.inv_im_name, im.inv_im_code, invtrt.invtrt_name, invtrt.invtrt_color_name, invtrt.invtrt_color, f.name AS clinicname, u.fname, u.lname, u.nickname
							FROM inv_item_stock_transaction AS invistr INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
							INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
							LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
							LEFT JOIN `users` AS u ON u.id = invistr.invistr_createdby
							WHERE " . $whereClause . "  AND invistr.invistr_isdeleted = '0' AND invistr.invistr_clinic_id = " . $clinicId . " AND invistr_itemid = " . $itemId . " AND im.inv_im_code = '" . $itemCode . "' AND invistr.invistr_batch = '" . $itemBatch . "' AND invistr.invistr_expiry = '" . $itemExpiry . "' AND (invistr.invistr_price = '" . $itemPrice . "' OR invistr.invistr_price IS NULL) ORDER BY invistr.invistr_created_date ASC ", null, $GLOBALS['adodb']['dbreport']);

    if (sqlNumRows($resSecondNested)) {
        while ($rowSecondNested = sqlFetchArray($resSecondNested)) {
            $secondNestedData[] = array(
                'item_id' => $rowSecondNested['invistr_itemid'],
                'item_name' => $rowSecondNested['inv_im_name'],
                'item_code' => $rowSecondNested['inv_im_code'],
                'item_stock_batch' => $rowSecondNested['invistr_batch'],
                'item_stock_expiry' => $rowSecondNested['formatExpiryDate'],
                'item_stock_price' => $rowSecondNested['invistr_price'],
                'item_stock_qty' => $rowSecondNested['invistr_quantity'],
                'item_stock_running_qty' => $rowSecondNested['invistr_after_qty'],
                'item_transaction_date' => $rowSecondNested['formatcreateddate'],
                'item_transaction_type' => $rowSecondNested['invtrt_name'],
                'item_clinic_name' => $rowSecondNested['clinicname'],
                'item_received_by' => $rowSecondNested['nickname'],
                'item_transaction_color_name' => $rowSecondNested['invtrt_color_name'],
                'item_transaction_color_code' => $rowSecondNested['invtrt_color']
            );
        }
    }
    return $secondNestedData;
}

//Second Nested Section(Report - Stock Ledger)
function getItemtransactionBasedOnClinic($clinicId, $whereClause, $dateMysqlFormat , $zeroItem = Null) {
    //Get team detail based on clinic wise stock transaction
    $firstNestedData = array();


    $resFirstNested = sqlStatement("SELECT invistr.*, DATE_FORMAT(invistr.invistr_expiry , '" . $dateMysqlFormat . "') AS formatExpiryDate, im.inv_im_name, im.inv_im_code, invtrt.invtrt_name, f.name AS clinicname, u.fname, u.lname, u.nickname, concat_ws('#', im.inv_im_code, invistr.invistr_batch, invistr.invistr_expiry, invistr.invistr_price) as stockKey
		FROM inv_item_stock_transaction AS invistr INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
		INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
		LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
		LEFT JOIN `users` AS u ON u.id = invistr.invistr_createdby
		WHERE " . $whereClause . " AND  invistr.invistr_isdeleted = '0' AND invistr.invistr_clinic_id = " . $clinicId . "  GROUP BY stockKey ORDER BY im.inv_im_name ASC ", null, $GLOBALS['adodb']['dbreport']);
    if (sqlNumRows($resFirstNested)) {
        while ($rowFirstNested = sqlFetchArray($resFirstNested)) {
            $secondNestedData = getStockTransactionBasedOnItemNew($clinicId, $rowFirstNested['invistr_itemid'], $rowFirstNested['invistr_batch'], $rowFirstNested['inv_im_code'], $rowFirstNested['invistr_expiry'], $rowFirstNested['invistr_price'], $whereClause, $dateMysqlFormat);
            $item_stock_qty = openingItemBalance($clinicId, $rowFirstNested['invistr_itemid'], $rowFirstNested['invistr_batch'], $rowFirstNested['inv_im_code'], $rowFirstNested['invistr_expiry'] , $zeroItem) ;
            //echo $item_stock_qty  ;  
            //exit('helloo');
            if($zeroItem == 1){
            
                $firstNestedData[] = array(
                    'item_id' => $rowFirstNested['invistr_itemid'],
                    'clinic_name' => $rowFirstNested['clinicname'],
                    'item_name' => $rowFirstNested['inv_im_name'],
                    'item_code' => $rowFirstNested['inv_im_code'],
                    'item_stock_batch' => $rowFirstNested['invistr_batch'],
                    'item_stock_expiry' => $rowFirstNested['formatExpiryDate'],
                    'item_stock_price' => $rowFirstNested['invistr_price'],
                    //'opening_stock_balance_running_qty'=>openingBalance($rowFirstNested['invistr_id']),
                'opening_stock_balance_running_qty' => $item_stock_qty , //openingBalanceFinal($clinicId, $rowFirstNested['invistr_itemid'], $whereClause, $dateMysqlFormat),
                    'item_transaction_type' => 'Opening Balance',
                    'transaction_date' => $rowFirstNested['invistr_created_date'],
                    'transaction_type' => $rowFirstNested['invtrt_name'],
                    'itemWiseDetail' => $secondNestedData
                );
            }else{
                if($item_stock_qty > 0){
                    $firstNestedData[] = array(
                        'item_id' => $rowFirstNested['invistr_itemid'],
                        'clinic_name' => $rowFirstNested['clinicname'],
                        'item_name' => $rowFirstNested['inv_im_name'],
                        'item_code' => $rowFirstNested['inv_im_code'],
                        'item_stock_batch' => $rowFirstNested['invistr_batch'],
                        'item_stock_expiry' => $rowFirstNested['formatExpiryDate'],
                        'item_stock_price' => $rowFirstNested['invistr_price'],
                        //'opening_stock_balance_running_qty'=>openingBalance($rowFirstNested['invistr_id']),
                        'opening_stock_balance_running_qty' => $item_stock_qty ,//openingBalanceFinal($clinicId, $rowFirstNested['invistr_itemid'], $whereClause, $dateMysqlFormat),
                        'item_transaction_type' => 'Opening Balance',
                        'transaction_date' => $rowFirstNested['invistr_created_date'],
                        'transaction_type' => $rowFirstNested['invtrt_name'],
                        'itemWiseDetail' => $secondNestedData
                    );
                }
            }
        }
    }
   // echo '<pre>';
   // print_r($firstNestedData); exit;
    return $firstNestedData;
}

function getItemtransactionBalanceBasedOnClinic($clinicId, $itemId, $batchNumber, $itemCode, $whereClause, $dateMysqlFormat, $zeroItem, $callType, $page) {
    //Get team detail based on clinic wise stock transaction
    $firstNestedData = array();
    $resFirstNested = "SELECT invistr.*, DATE_FORMAT(invistr.invistr_created_date , '" . $dateMysqlFormat . "') AS formatcreateddate, DATE_FORMAT(invistr.invistr_expiry , '" . $dateMysqlFormat . "') AS formatExpiryDate, im.inv_im_name, im.inv_im_code, invtrt.invtrt_name, invtrt.invtrt_color_name, invtrt.invtrt_color, f.name AS clinicname, u.fname, u.lname, u.nickname, SUM(invistr_quantity) AS totqty
		FROM inv_item_stock_transaction AS invistr INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
		INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
		LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
		LEFT JOIN `users` AS u ON u.id = invistr.invistr_createdby
		WHERE " . $whereClause . " AND invistr.invistr_isdeleted = '0' AND invistr.invistr_clinic_id = " . $clinicId . "  GROUP BY im.inv_im_name,im.inv_im_code,invistr.invistr_batch,invistr.invistr_expiry ORDER BY im.inv_im_name ASC ";

    $sql_num = sqlStatement($resFirstNested, null, $GLOBALS['adodb']['dbreport']);
    $num_rows = sqlNumRows($sql_num); // total no. of rows
    $per_page = $GLOBALS['encounter_page_size'];
    $curr_URL = $_SERVER['SCRIPT_NAME'] . '?form_refresh=viewStockBalance  &' . 'form_facility=' . $clinicId . '&zero_quantity_items=' . $zeroItem . '&';
    $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
    $page = ($page == 0 ? 1 : $page);
    $page_start = ($page - 1) * $per_page;
    if ($callType == 1) {
        $resFirstNested.= " LIMIT $page_start , $per_page";
    }

    $resFirstNested = sqlStatement($resFirstNested, null, $GLOBALS['adodb']['dbreport']);
    if (sqlNumRows($resFirstNested)) {
        while ($rowFirstNested = sqlFetchArray($resFirstNested)) {
            $item_stock_qty = openingItemBalance($clinicId, $rowFirstNested['invistr_itemid'], $rowFirstNested['invistr_batch'], $rowFirstNested['inv_im_code'], $rowFirstNested['invistr_expiry'], $zeroItem);
            if ($zeroItem == 1) {
                $firstNestedData[] = array(
                    'item_id' => $rowFirstNested['invistr_itemid'],
                    'item_name' => $rowFirstNested['inv_im_name'],
                    'item_code' => $rowFirstNested['inv_im_code'],
                    'item_stock_batch' => $rowFirstNested['invistr_batch'],
                    'item_stock_expiry' => $rowFirstNested['formatExpiryDate'],
                    'item_stock_price' => $rowFirstNested['invistr_price'],
                    'item_stock_qty' => $item_stock_qty,
                    'item_stock_running_qty' => 'calculate',
                    'item_transaction_date' => $rowFirstNested['formatcreateddate'],
                    'item_transaction_type' => $rowFirstNested['invtrt_name'],
                    'item_clinic_name' => $rowFirstNested['clinicname'],
                    'item_received_by' => $rowFirstNested['nickname'],
                    'item_transaction_color_name' => $rowFirstNested['invtrt_color_name'],
                    'item_transaction_color_code' => $rowFirstNested['invtrt_color']
                );
            } else {
                if ($item_stock_qty > 0) {
                    $firstNestedData[] = array(
                        'item_id' => $rowFirstNested['invistr_itemid'],
                        'item_name' => $rowFirstNested['inv_im_name'],
                        'item_code' => $rowFirstNested['inv_im_code'],
                        'item_stock_batch' => $rowFirstNested['invistr_batch'],
                        'item_stock_expiry' => $rowFirstNested['formatExpiryDate'],
                        'item_stock_price' => $rowFirstNested['invistr_price'],
                        'item_stock_qty' => $item_stock_qty,
                        'item_stock_running_qty' => 'calculate',
                        'item_transaction_date' => $rowFirstNested['formatcreateddate'],
                        'item_transaction_type' => $rowFirstNested['invtrt_name'],
                        'item_clinic_name' => $rowFirstNested['clinicname'],
                        'item_received_by' => $rowFirstNested['nickname'],
                        'item_transaction_color_name' => $rowFirstNested['invtrt_color_name'],
                        'item_transaction_color_code' => $rowFirstNested['invtrt_color']
                    );
                }
            }
        }
    }
    if($callType == 1)
    {    $firstNestedData['paginationData'] = $pa_data;
    }
    return $firstNestedData;
}

function openingItemBalance($clinicId, $itemId, $batchNumber, $itemCode, $itemExpiry) {
    $sql = "SELECT invistr.invistr_id, invistr.invistr_before_qty, invistr_after_qty FROM inv_item_stock_transaction AS invistr
	WHERE invistr.invistr_isdeleted = '0' AND invistr.invistr_clinic_id = " . $clinicId . " AND invistr_itemid = " . $itemId . " AND invistr.invistr_batch = '" . $batchNumber . "' AND invistr_expiry = '" . $itemExpiry . "'
	ORDER BY invistr.invistr_id DESC LIMIT 0, 1 ";
    $stockTransactionRow = sqlQuery($sql, null, $GLOBALS['adodb']['dbreport']);
    if (!empty($stockTransactionRow['invistr_after_qty'])) {
        return $stockTransactionRow['invistr_after_qty'];
    } else {
        return 0;
    }
}

function openingBalanceFinal($clinicId, $itemId, $whereClause, $dateMysqlFormat) {
    //Get stock transaction based on item
    $sBQuery = "SELECT invistr.invistr_before_qty, im.inv_im_name, im.inv_im_code, invtrt.invtrt_name, invtrt.invtrt_color_name, invtrt.invtrt_color, f.name AS clinicname, u.fname, u.lname, u.nickname
				FROM inv_item_stock_transaction AS invistr INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
				INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
				LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
				LEFT JOIN `users` AS u ON u.id = invistr.invistr_createdby
				WHERE " . $whereClause . "  AND invistr.invistr_isdeleted = '0' AND invistr.invistr_clinic_id = " . $clinicId . " AND invistr_itemid = " . $itemId . "  ORDER BY invistr.invistr_created_date ASC LIMIT 0, 1";

    $stockTransactionRow = sqlQuery($sBQuery);
    if (!empty($stockTransactionRow['invistr_before_qty'])) {
        return $stockTransactionRow['invistr_before_qty'];
    } else {
        return 0;
    }
}

function getActiveMappedSubCat($catId) {
    $res = sqlStatement("SELECT id AS icsm_id, parent_id AS icsm_catid, deleted AS icsm_deleted FROM invcategories WHERE deleted = '0'  AND parent_id=" . $catId);
    $result = array();
    for ($iter = 0; $row = sqlFetchArray($res); $iter++)
        $result[$iter] = $row;
    return $result;
}

function getAllMappedSubCat($catId) {
    //echo "SELECT `icsm_id`, `icsm_catid`, `icsm_scatid`, `icsm_deleted` FROM inv_cat_subcat_map WHERE icsm_catid=".$catId;
    /*
      $res = sqlStatement("SELECT `id` as `icsm_id`, `parent_id` as `icsm_catid`, id` as `icsm_scatid`, `deleted` as `icsm_deleted` FROM invcategories WHERE parent_id=" . $catId);
      $result = array();
      for ($iter = 0; $row = sqlFetchArray($res); $iter++)
      $result[$iter] = $row;
      return $result;
     */
}

function checkMappedSubCat($subCatID, $allMappedId) {
    $returnFlag = 0;
    foreach ($allMappedId as $mappedId) {
        if ($mappedId['icsm_scatid'] == $subCatID) {
            $returnFlag = 1;
        }
    }
    return $returnFlag;
}

function deleteMappedSubCat($allSubCats, $catId) {
    $allMappedSubCats = getAllMappedSubCat($catId);
    foreach ($allMappedSubCats as $mappedSubCat) {
        if (!in_array($mappedSubCat['icsm_scatid'], $allSubCats)) {
            idSqlStatement("UPDATE inv_cat_subcat_map SET icsm_deleted = '1' WHERE icsm_id = " . $mappedSubCat['icsm_id']);
        }
    }
}

function checkItemStock($itemId) {
    $row = sqlQuery("SELECT COUNT(invist_itemid) AS count FROM inv_item_stock  WHERE " .
            "invist_isdeleted = '0' AND invist_itemid = ?", array($itemId));
    if ($row['count']) {
        return 0;
    } else {
        return 1;
    }
}

function checkItemStockStatus($itemId) {
    $row = sqlQuery("SELECT COUNT(invist_itemid) AS count FROM inv_item_stock  WHERE " .
            "invist_isdeleted = '0' AND invist_quantity > 0 AND invist_itemid = ?", array($itemId));
    if ($row['count']) {
        return 1;
    } else {
        return 0;
    }
}

function getClinicItemStock($itemId, $clinicId) {

    $itemStocks = array();
    $sql = "SELECT ist.invist_id, ist.invist_itemid, ist.invist_batch, ist.invist_expiry, ist.invist_price, ist.invist_quantity, ist.invist_clinic_id, ist.invist_isdeleted, ist.invist_createdby, im.inv_im_name " .
            "FROM inv_item_stock AS ist
            INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid " .
            "WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invist_quantity > 0 AND ist.invist_itemid= ? AND ist.invist_clinic_id=? ORDER BY ist.invist_expiry ASC";
    $itemStockRes = $GLOBALS['pdoobject']->custom_query($sql, array($itemId, $clinicId));
    if ($GLOBALS['pdoobject']->custom_query($sql, array($itemId, $clinicId),1)) {
        foreach($itemStockRes as $itemStockRow) {
            $itemStocks[] = $itemStockRow;
        }
    }
    return $itemStocks;
}

function getStockIssueQty($reqId, $itemId, $stockId) {
    if (!empty($stockId)) {
        $row = $GLOBALS['pdoobject']->custom_query("SELECT SUM(iist.iii_quantity) AS issuedqty FROM inv_issue_notes AS iisn INNER JOIN inv_issue_item AS iist ON iist.iii_issueid = iisn.iin_id 
					WHERE iisn.iin_reqid = ? AND iist.iii_itemid=? AND iist.iii_stock_id=?", array($reqId, $itemId, $stockId));
    } else {
        $row = $GLOBALS['pdoobject']->custom_query("SELECT SUM(iist.iii_quantity) AS issuedqty FROM inv_issue_notes AS iisn INNER JOIN inv_issue_item AS iist ON iist.iii_issueid = iisn.iin_id
					WHERE iisn.iin_reqid = ? AND iist.iii_itemid=? ", array($reqId, $itemId));
    }
    if (empty($row[0]['issuedqty']) || $row[0]['issuedqty'] == '') {
        return 0;
    } else {
        return $row[0]['issuedqty'];
    }
}

function checkIssueNumExist($issueNum, $clinicId) {
    $row = sqlQuery("SELECT COUNT(iin_id) AS issuenum FROM inv_issue_notes WHERE iin_number = '" . $issueNum . "'");
    if ($row[issuenum] > 0) {
        return getIssueNumber($clinicId);
    } else {
        return $issueNum;
    }
}

function getIssueNumber($clinicId) {
    $row = sqlQuery("SELECT COUNT(iin_id) AS issuenum FROM inv_issue_notes WHERE iin_from_clinic = " . $clinicId);
    $facilityRow = sqlQuery("SELECT id, facility_npi, mask_patient_id_pre FROM facility WHERE id = " . $clinicId);
    $issueNumber = '';
    if (!empty($facilityRow['facility_npi'])) {
        $issueNumber = 'ISSUE-' . $facilityRow['facility_npi'] . '-' . ($row['issuenum'] + 1);
    } else {
        $issueNumber = 'ISSUE-' . $facilityRow['mask_patient_id_pre'] . '-' . ($row['issuenum'] + 1);
    }
    return $issueNumber;
}

function insertStockIssue($data) {
    $data['iin_number'] = checkIssueNumExist($data['iin_number'], $data['iin_from_clinic']);
 
    $requId = dbRowInsertId('inv_issue_notes', $data);
    return $requId;
}

function insertUpdateIssueItems($allData) {
    if (!empty($allData)) {
        foreach ($allData as $data) {
            $itemStockDetails = getItemStockDetail($data['iii_stock_id']);
            if ($itemStockDetails['invist_quantity'] >= $data['iii_quantity']) {
                dbRowInsert('inv_issue_item', $data);
                sqlStatement("update inv_item_stock set invist_quantity=invist_quantity-".$data['iii_quantity']." where invist_id = '" . $data['iii_stock_id'] . "'");
                 }
        }
    }
}

function insertUpdateIssueItemsTran($allData) {
    if (!empty($allData)) {
        foreach ($allData as $data) {
            $itemStockDetails = getClincItemStockDetail($data['invistr_itemid'], $data['invistr_batch'], $data['invistr_expiry'], $data['invistr_price'], $data['invistr_clinic_id']);
            if ($itemStockDetails['invist_quantity'] >= $data['invistr_quantity']) {
                if (empty($data['invistr_price']) || $data['invistr_price'] == '') {
                    unset($data['invistr_price']);
                }
                dbRowInsert('inv_item_stock_transaction', $data);
                //sqlStatement("update inv_item_stock set invist_quantity=invist_quantity-" . $data['invistr_quantity'] . " where invist_id = '" . $itemStockDetails['invist_id'] . "'");
                sqlStatement("update inv_item_stock set invist_quantity='".$data['invistr_after_qty']. "' where invist_id = '" . $itemStockDetails['invist_id'] . "'");
            }
        }
    }
}
function insertStockTransactionIssueReturn($data,$stockid) {
    if (empty($data['invistr_price']) || $data['invistr_price'] == '') {
        unset($data['invistr_price']);
    }
    $transactionId = $GLOBALS['pdoobject']->insert('inv_item_stock_transaction', $data);
    $GLOBALS['pdoobject']->update("inv_item_stock",array("invist_quantity"=>$data['invistr_after_qty']),array("invist_id"=>$stockid));
     //sqlStatement("update inv_item_stock set invist_quantity='".$data['invistr_after_qty']. "' where invist_id = '" . $stockid . "'");
    return $transactionId;
}
function insertStockTransaction($data) {
    if (empty($data['invistr_price']) || $data['invistr_price'] == '') {
        unset($data['invistr_price']);
    }
    $transactionId = $GLOBALS['pdoobject']->insert('inv_item_stock_transaction', $data);
    return $transactionId;
}

function insertStockTransfer($data) {
    $transferId = dbRowInsertId('inv_transfer', $data);
    return $transferId;
}

function updateReqStatus() {
    
}

function getReqItemQtyIssuedQty($reqId) {
    $reqRow = $GLOBALS['pdoobject']->custom_query( "SELECT SUM(iri_quantity) AS reqQty FROM inv_requisition_item WHERE iri_reqid = ? ", array($reqId));
    $issueRow = $GLOBALS['pdoobject']->custom_query("SELECT SUM(iist.iii_quantity) AS issuedqty, SUM(iist.iii_accept_quantity) AS acceptedqty, SUM(iist.iii_return_quantity) AS returnedqty FROM inv_issue_notes AS iisn INNER JOIN inv_issue_item AS iist ON iist.iii_issueid = iisn.iin_id
				WHERE iisn.iin_reqid = ? ", array($reqId));
    $returndata = array('reqQty' => $reqRow[0]['reqQty'], 'issuedqty' => $issueRow[0]['issuedqty'], 'acceptedqty' => $issueRow[0]['acceptedqty'], 'returnedqty' => $issueRow[0]['returnedqty']);
    return $returndata;
}

function insertUpdateAdjustment($data) {
    if (!empty($data['ina_id'])) {
        $whereCond = 'WHERE ina_id = ' . $data['ina_id'];
        dbRowUpdate('inv_adjustments', $data, $whereCond);
        return $data['ina_id'];
    } else {
        $adjId = dbRowInsertId('inv_adjustments', $data);
        return $adjId;
    }
}

function insertUpdateAdjustmentRel($data) {
    $adjRelId = dbRowInsertId('inv_adj_trans_rel', $data);
    return $adjRelId;
}

function addStockQuantity($qty, $stockid) {
    sqlStatement("update inv_item_stock set invist_quantity=invist_quantity+" . $qty . " where invist_id = '" . $stockid . "'");
    return 1;
}

function removeStockQuantity($qty, $stockid) {
    sqlStatement("update inv_item_stock set invist_quantity=invist_quantity-" . $qty . " where invist_id = '" . $stockid . "'");
    return 1;
}

function updateStockTransaction($data) {
    if (!empty($data['invistr_id'])) {
        $whereCond = 'WHERE invistr_id = ' . $data['invistr_id'];
        if (empty($data['invistr_price']) || $data['invistr_price'] == '') {
            unset($data['invistr_price']);
        }
        dbRowUpdate('inv_item_stock_transaction', $data, $whereCond);
        return $data['invistr_id'];
    }
}

function getAdjustmentDetail($adjid, $facility) {
    $row = sqlQuery("SELECT adj.ina_id, adj.ina_item_id, adj.ina_stock_id, adj.ina_quantity, adj.ina_type, adj.ina_facility_id, adjrel.iatr_id, tran.invistr_id, tran.invistr_before_qty, tran.invistr_after_qty, tran.invistr_comment
	FROM inv_adjustments AS adj
	INNER JOIN inv_adj_trans_rel AS adjrel ON adjrel.iatr_adj_id = adj.ina_id
	INNER JOIN inv_item_stock_transaction AS tran ON tran.invistr_id = adjrel.iatr_trans_id
	WHERE adj.ina_id= ? AND adj.ina_facility_id= ?", array($adjid, $facility));
    return $row;
}

function insertUpdateIssueReturn($data) {
    if (!empty($data['invir_id'])) {
        $updateId = $GLOBALS['pdoobject']->update("inv_issue_return",$data,array("invir_id"=>$data['invir_id']));      
        return $updateId;
    } else {
        $adjId = $GLOBALS['pdoobject']->insert("inv_issue_return",$data);
        return $adjId;
    }
}

function getStockTransDetail($transId,$pdoobject='') {
    $sql="SELECT iist.invistr_id, iist.invistr_itemid, iist.invistr_batch, iist.invistr_expiry, iist.invistr_price, iist.invistr_quantity, iist.invistr_before_qty, iist.invistr_after_qty, iist.invistr_clinic_id, iist.invistr_isdeleted, iist.invistr_createdby, im.inv_im_name
		FROM inv_item_stock_transaction AS iist		
		INNER JOIN inv_item_master AS im ON im.inv_im_id = iist.invistr_itemid
		WHERE im.inv_im_deleted = '0' AND iist.invistr_id = ?";
    if(!empty($pdoobject))
    {
        $row = $pdoobject->custom_query($sql,array($transId));
        $result = $row[0];
    }
    else{
        $result = sqlQuery($sql, array($transId));
    }
    if (!empty($result)) {
        return $result;
    } else {
        return array();
    }
}

function updateStockTransfer($data) {
    if (!empty($data['invtran_id'])) {
        $whereCond = 'WHERE invtran_id = ' . $data['invtran_id'];
        dbRowUpdate('inv_transfer', $data, $whereCond);
        return $data['invtran_id'];
    }
}

function getDateDisplayFormat($forData = 0) {
    switch ($GLOBALS['date_display_format']) {
        case 5:
            if ($forData == 1)
                return '%d %b %Y';
            else if ($forData == 2)
                return 'dd M yy';
            else
                return 'd M Y';
            break;
        case 4:
            if ($forData == 1)
                return '%d %b %y';
            else if ($forData == 2)
                return 'dd M y';
            else
                return 'd M y';
            break;
        case 3:
            if ($forData == 1)
                return '%d-%m-%y';
            else if ($forData == 2)
                return 'dd-mm-y';
            else
                return 'd-m-y';
            break;
        case 2:
            if ($forData == 1)
                return '%d/%m/%Y';
            else if ($forData == 2)
                return 'dd/mm/yy';
            else
                return 'd/m/Y';
            break;
        case 1:
            if ($forData == 1)
                return '%m/%d/%Y';
            else if ($forData == 2)
                return 'mm/dd/yy';
            else
                return 'm/d/Y';
            break;
        default:
            if ($forData == 1)
                return '%Y-%m-%d';
            else if ($forData == 2)
                return 'yy/mm/dd';
            else
                return 'Y-m-d';
            break;
    }
}

function getLoggedUserAssignedClinics() {
    
    $query="select uf.facility_id as id, f.name, f.color from users_facility uf left join facility f on (uf.facility_id = f.id) where uf.tablename='users' and uf.table_id = ? "; 
    $rs = $GLOBALS['pdoobject']->custom_query($query,array($_SESSION['authId']));
    $assignedClinics = array();
    foreach ($rs as $facrow) {
        $assignedClinics[] = $facrow['id'];
    }
    return implode(",", $assignedClinics);
}

// update adjustment table
function updateAdjustment($data) {
    if (!empty($data['ina_id'])) {
        $whereCond = 'WHERE ina_id = ' . $data['ina_id'];
        dbRowUpdate('inv_adjustments', $data, $whereCond);
        return $data['ina_id'];
    }
}

function userFacilitys() {
    $qsql = sqlStatement("select uf.facility_id as id from users_facility uf left join facility f on (uf.facility_id = f.id) where uf.tablename='users' and uf.table_id = ? ", array($_SESSION['authId']));
    $facilityIdList = '';
    while ($facrow = sqlFetchArray($qsql)) {
        if (!empty($facilityIdList)) {
            $facilityIdList .= ',' . $facrow['id'];
        } else {
            $facilityIdList = $facrow['id'];
        }
    }
    return $facilityIdList;
}

// Check doctor facility list facility for one facility
function ckeckFacilityList() {
    $newArray = array();
    $qsql = sqlStatement("
			  select uf.facility_id as id, f.name, f.color
			  from users_facility uf
			  left join facility f on (uf.facility_id = f.id)
			  where uf.tablename='users'
			  and uf.table_id = ?
			", array($_SESSION['authId']));
    $facrow = sqlFetchArray($qsql);
    while ($facrow = sqlFetchArray($qsql)) {
        $newArray[] = $facrow['name'];
    }
    return count($newArray);
}

function getStatus($role = 'admin', $type = '1', $value = '') {
    if ($value != '') {
        return $GLOBALS['requisition_status'][$value];
    }

    if ($type == '100') {

        if ($role == 'admin') {
            $status = 1;
        } elseif ($role == 'clin') {
            $status = 1;
        } else {
            $status = 0;
        }
    } elseif ($type == '1') {

        if ($role == 'admin') {
            $status = 1;
        } elseif ($role == 'qadmin') {
            $status = 1;
        } elseif ($role == 'clin') {
            $status = 1;
        } elseif ($role == 'Dentist') {
            $status = '0';
        } else {
            $status = 0;
        }
    } else {
        $status = 2;
        if ($role == 'admin') {
            $status = 2;
        } elseif ($role == 'qadmin') {
            $status = 2;
        } elseif ($role == 'clin') {
            $status = 2;
        } else {
            $status = 2;
        }
    }

    return $status;
}

function getReqReDate($reqid = '') {
    $row = $GLOBALS['pdoobject']->custom_query("SELECT iin_date FROM inv_issue_notes WHERE iin_reqid = " . $reqid." ORDER BY iin_id DESC LIMIT 1");
    //print_r($row);die();
    return $row['0']['iin_date'];
}

function getCategoryList($itemids) {
    $catlist = array();
    if ($itemids) {
        $query = "SELECT `category`.`id`,`category`.`name` FROM `invcategories` AS `category` WHERE `category`.`id` IN (" . $itemids . ")";
        //$rs = sqlStatement($query);
        $rs = $GLOBALS['pdoobject']->custom_query($query);
        foreach ($rs as $rows) {
            $catlist[] = $rows;
        }
    }

    return $catlist;
}


function validateStock($filterList=array(),$pdoobject){
       $dl="";
       $sql="";
       foreach($filterList as $filterKey=>$filterValue){ 
            $sql=$sql.$dl.$filterKey." = '".$filterValue."'";
                 $dl=" AND ";
       }
       $sql=!empty($sql) ? " WHERE ".$sql : "";
       echo $query="SELECT invist_quantity,invist_id FROM inv_item_stock".$sql;
       return $GLOBALS['pdoobject']->custom_query($query);
       
       
       //return sqlQuery($query);
}
/**
 * usersFacilityDropdown Function
 * @author: Brajraj Agrawal
 * @Created : 6 May 2016
 * @email : brajraj.agarwal@instantsys.com
 * @Description : This function is used in all the inventry facility dropdown.Users can see only 
 * facilities which areassigned to he/she.His/Her default facility will be selected automatically
 *  based on sessionId.  
 * @param type $name
 * @param type $class
 * @param type $id
 * @param type $selected
 * @param type $userId
 * @param type $restrictUserFacility
 */
function usersFacilityDropdown($name, $class = '', $id, $selected = '', $userId, $restrictUserFacility = 0,$pdoobject,$showall=1,$jsFunction='') {
    $selected_attr = '';
    $option_value = '';
    $option_content = '';
    $alreadySelected = 0;
    $allFacilitiesContent = "All Clinics";
    echo "<select name='" . htmlspecialchars($name, ENT_QUOTES) . "'" . (!empty($class) ? "class=" . $class : '') . " id=" . htmlspecialchars($id, ENT_QUOTES) . " $jsFunction>";
    if ($showall == 1) {
        if ($selected == 0 && $selected != '') {
            echo "<option value='0' selected='selected'>$allFacilitiesContent</option>";
            $alreadySelected = 1;
        } else {
            echo "<option value='0'>$allFacilitiesContent</option>";
        }
    }
    $sql = $pdoobject->fetch_multi_row('users',array('facility_id AS id'), array('id'=>$userId));
    $default_facility_id =$sql[0]['id'];
    if (!$restrictUserFacility) {
        $qsql = "select id, name FROM facility WHERE service_location != 0 ORDER BY name";
    } else {
        $qsql = "select uf.facility_id as id, f.name FROM users_facility uf "
                . "LEFT JOIN facility f on (uf.facility_id = f.id) "
                . "WHERE uf.tablename='users' AND uf.table_id = ? ORDER BY f.name";
    }
    $res = $pdoobject->custom_query($qsql,array($userId));
   foreach($res As $row) {
        $facility_id = $row['id'];
        $selected_attr = '';
        if ($facility_id==$selected) {
            $selected_attr = (!$alreadySelected) ? ' selected="selected"' : '';
            $option_value = htmlspecialchars($facility_id, ENT_QUOTES);
            $alreadySelected=1;
        } elseif ($facility_id ==$default_facility_id) {
            $selected_attr = (!$alreadySelected) ? ' selected="selected"' : '';
            $option_value = htmlspecialchars($default_facility_id, ENT_QUOTES);
        } else{
            $option_value = htmlspecialchars($facility_id, ENT_QUOTES); 
        }
        $option_content = htmlspecialchars($row['name'], ENT_NOQUOTES);
        echo "<option value=\"$option_value\" $selected_attr>" . $option_content . "</option>\n";
    }
    echo "</select>\n";
}


function InvUsersFacilityDropdown($name, $class = '', $id, $selected = '', $userId, $restrictUserFacility = 0,$pdoobject,$showall=1,$jsFunction='') {
    $selected_attr = '';
    $option_value = '';
    $option_content = '';
    $alreadySelected = 0;
    $allFacilitiesContent = "All Clinics";
    echo "<select name='" . htmlspecialchars($name, ENT_QUOTES) . "'" . (!empty($class) ? "class=" . $class : '') . " id=" . htmlspecialchars($id, ENT_QUOTES) . " $jsFunction>";
    if ($showall == 1) {
        if ($selected == 0 && $selected != '') {
            echo "<option value='0' selected='selected'>$allFacilitiesContent</option>";
            $alreadySelected = 1;
        } else {
            echo "<option value='0'>$allFacilitiesContent</option>";
        }
    }
    $sql = $pdoobject->fetch_multi_row('users',array('facility_id AS id'), array('id'=>$userId));
    $default_facility_id =$sql[0]['id'];
    if (!$restrictUserFacility) {
        $qsql = "select id, name FROM facility WHERE service_location != 0 ORDER BY name";
    } else {
        $qsql = "select uf.facility_id as id, f.name FROM inventory_users uf "
                . "LEFT JOIN facility f on (uf.facility_id = f.id) "
                . "WHERE uf.user_id = ? ORDER BY f.name";
    }
    $res = $pdoobject->custom_query($qsql,array($userId));
   foreach($res As $row) {
        $facility_id = $row['id'];
        $selected_attr = '';
        if ($facility_id==$selected) {
            $selected_attr = (!$alreadySelected) ? ' selected="selected"' : '';
            $option_value = htmlspecialchars($facility_id, ENT_QUOTES);
            $alreadySelected=1;
        } elseif ($facility_id ==$default_facility_id) {
            $selected_attr = (!$alreadySelected) ? ' selected="selected"' : '';
            $option_value = htmlspecialchars($default_facility_id, ENT_QUOTES);
        } else{
            $option_value = htmlspecialchars($facility_id, ENT_QUOTES); 
        }
        $option_content = htmlspecialchars($row['name'], ENT_NOQUOTES);
        echo "<option value=\"$option_value\" $selected_attr>" . $option_content . "</option>\n";
    }
    echo "</select>\n";
}


function getFacilityProvider($userId, $facilList, $restrictFacility) {
    if ($restrictFacility) {
        $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
        $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
        $sql .= " WHERE ";
        if (!empty($facilList)) {
            $sql .= "uf.facility_id = $facilList ";
        } else {
            $fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
		FROM users_facility AS uf
		LEFT JOIN users AS us ON uf.table_id = us.id
		LEFT JOIN facility AS f ON uf.facility_id = f.id
		WHERE uf.table_id = " . $userId . " AND us.authorized !=0
		AND us.active = 1 ORDER BY id ";
            $fRes = sqlStatement($fsql);
            $facilityIds = '';
            while ($fRow = sqlFetchArray($fRes)) {
                if (!empty($facilityIds)) {
                    $facilityIds .= ', ' . $fRow['facility_id'];
                } else {
                    $facilityIds = $fRow['facility_id'];
                }
            }
            $sql .= "uf.facility_id IN (" . $facilityIds . ") ";
        }
        $sql .= "AND gag.id IN (12, 13, 18) AND u.active='1'";
        $sql .= " GROUP BY u.id DESC";
        $ures = sqlStatement($sql);
    } else {
        $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename ";
        $sql .= "FROM users AS u  ";
        $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username ";
        $sql .= "INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id ";
        $sql .= "INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
        $sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";
    }
    $returnData = '';
    //$returnData.= "<option value='' selected='selected'>All Doctors</option>";
    while ($urow = sqlFetchArray($ures)) {
        $returnData.= "<option value='" . attr($urow['id']) . "'";
        $returnData.= ">" . text($urow['completename']);
        $returnData.= "</option>";
    }
    echo $returnData;
}

    
function getDatesDiff($toDate,$fromDate,$format="days"){
    return $diff=date_diff(date_create($toDate),date_create($fromDate))->$format;
}

function getDateRange($fromDate,$toDate,$clinic=0){
    $range=empty($clinic) ? $GLOBALS['form_facility_all_time_range'] : $GLOBALS['form_facility_time_range'];
    if(empty($fromDate) & empty($toDate)){
        return $date=array("FromDate"=>date('Y-m-d', strtotime("-".$range." day", strtotime(date('Y-m-d')))),"ToDate"=>date('Y-m-d'),"Cron"=>0,'Days'=>getDatesDiff(date('Y-m-d'),date('Y-m-d', strtotime("-".$range." day", strtotime(date('Y-m-d'))))));
    }elseif(!empty($fromDate) & empty($toDate)){
       return $date=array("FromDate"=>$fromDate,"ToDate"=>date('Y-m-d', strtotime("+".$range." day", strtotime($fromDate))),"Cron"=>0,'Days'=> getDatesDiff(date('Y-m-d', strtotime("+".$range." day", strtotime($fromDate))), $fromDate));        
    }elseif(empty($fromDate) & !empty($toDate)){
       return $date=array("FromDate"=>date('Y-m-d', strtotime("-".$range." day", strtotime($toDate))),"ToDate"=>$toDate,"Cron"=>0,'Days'=>getDatesDiff($toDate,date('Y-m-d', strtotime("-".$range." day", strtotime($toDate)))));       
    }else{
      $diff=getDatesDiff($toDate,$fromDate);
      $cron=$diff > $range ? 1 : 0;  
      return $date=array("FromDate"=>$fromDate,"ToDate"=>$toDate,"Cron"=>$cron,'Days'=>$diff); 
      
    }
    
}

function scheduleReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description, $query, $pdoobjectnew=0) {

    ########### get user name and email, having privelege to access emr##########
    $userName = $_SESSION{"authUser"};
    $row = $GLOBALS['pdoobject']->fetch_multi_row("users", array("fname", "mname", "lname", "email"), array("username" => $userName));

    ########### Default value used with all req##########
    $rcsl_requester_name = $row[0]['fname'] . " " . $row[0]['mname'] . " " . $row[0]['lname'];
    $rcsl_requester_email = $row[0]['email'];
    $rcsl_requested_date = date('Y-m-d H:i:s');
    $rcsl_requester_ip = $_SERVER['REMOTE_ADDR'];
    $rcsl_mail_status = 0;
    $rcsl_report_description .= " requested by $userName";
    #####check the status of report request###########    
    $rcsl = $GLOBALS['pdoobject']->fetch_multi_row("report_cron_service_log", array("rcsl_name_type"), array("rcsl_name_type" => $rcsl_name_type, "rcsl_requester_email" => $rcsl_requester_email, "rcsl_requested_filter" => $rcsl_requested_filter, "rcsl_mail_status" => 0));
    if (!empty($rcsl)) {
        $msgForReportLog = "Report with similar filter criteria is already scheduled for delivery today. Similar search criteria reports can only be generated once in 24 hours.";
    } else {
        if(!$pdoobjectnew){
        $GLOBALS['pdoobject']->insert("report_cron_service_log", array('rcsl_name_type' => $rcsl_name_type, 'rcsl_requester_name' => $rcsl_requester_name, 'rcsl_requester_email' => $rcsl_requester_email, 'rcsl_requested_filter' => $rcsl_requested_filter, 'rcsl_requested_date' => $rcsl_requested_date, 'rcsl_report_description' => $rcsl_report_description, 'rcsl_requester_ip' => $rcsl_requester_ip, 'rcsl_mail_status' => $rcsl_mail_status, 'query' => $query));
        }else{
            $pdoobjectnew->insert("report_cron_service_log", array('rcsl_name_type' => $rcsl_name_type, 'rcsl_requester_name' => $rcsl_requester_name, 'rcsl_requester_email' => $rcsl_requester_email, 'rcsl_requested_filter' => $rcsl_requested_filter, 'rcsl_requested_date' => $rcsl_requested_date, 'rcsl_report_description' => $rcsl_report_description, 'rcsl_requester_ip' => $rcsl_requester_ip, 'rcsl_mail_status' => $rcsl_mail_status, 'query' => $query));
        }
        
    }
    return $msgForReportLog;
}

function invDateFormat($date) {
    $formattedDate = '';
    if (!empty($date) && $date != '0000-00-00') {
        $formattedDate = date("d-m-y", strtotime($date));
    }
    return $formattedDate;
}

function getFacilityName($id) {
    if ($id == 0) {
        $facility = 'All Facility';
    } else {
        $response = $GLOBALS['pdoobject']->fetch_multi_row('facility', array('NAME'), array('id' => $id));
        $facility = $response['0']['NAME'];
    }

    return $facility;
}

function getDoctorsName($id) {
    if ($id == 0) {
        $doctor = 'All Doctors';
    } else {
        $response = $GLOBALS['pdoobject']->fetch_multi_row('users', array('fname', 'lname'), array('id' => $id));
        $doctor = $response['0']['fname'] . ' ' . $response['0']['lname'];
    }

    return $doctor;
}

function getTransactionType($id) {
    if ($id == 0) {
        $transaction = 'All Transaction';
    } else {
        $response = $GLOBALS['pdoobject']->fetch_multi_row('inv_transaction_type', array('invtrt_name'), array('invtrt_id' => $id));
        $transaction = $response['0']['invtrt_name'];
    }

    return $transaction;
}
/**
 * getCategoryName
 * @author : Brajraj Agrawal
 * @Created : 6 July 2016
 * @email : brajraj.agarwal@instantsys.com
 * @Description : getCategoryName function is used to get the Category name based on category id.
 */
function getCategoryName($id) {
    if ($id == 0) {
        $category = " All Category";
    } else {
        $response = $GLOBALS['pdoobject']->fetch_multi_row('invcategories', array('name'), array('id' => $id));
        $category = $response['0']['name'];
    }
    return $category;
}

        
        function getCategory($id){
            if($id==0){
               $category = 'All Category';  
            }else{
                $response=$GLOBALS['pdoobject']->fetch_multi_row('invcategories',array('name'),array('id'=>$id));
                $category=$response['0']['name'];
            }
            
            return $category;
            
        }
        
        
        
        function getItemDetail($id) {
            $response=$GLOBALS['pdoobject']->fetch_multi_row('inv_item_master',array('inv_im_isExpiry'),array('inv_im_id'=>$id));
            return $response['0']['inv_im_isExpiry'];
    
}

        function getFacilityType($id) {
            $response=$GLOBALS['pdoobject']->fetch_multi_row('facility',array('type'),array('id'=>$id));
            return $response['0']['type'];
    
}


function getRequisitionNo($isr_magento_orderid) {
            $response=$GLOBALS['pdoobject']->fetch_multi_row('inv_stock_requisition',array('isr_number'),array('isr_magento_orderid'=>$isr_magento_orderid));
            return $response['0']['isr_number'];
    
}
        
        
?>
