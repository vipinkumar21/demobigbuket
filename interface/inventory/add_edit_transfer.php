<?php
// Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");

$alertmsg = '';
//$stockid = $_REQUEST['stockid'];
$info_msg = "";
$tmpl_line_no = 0;

//if (!acl_check('inventory', 'invf_tran_out_add'))
//    die(xlt('Not authorized'));
if (!$invgacl->acl_check('inventory','invf_tran_out_add','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            function submitform() {
                if (document.forms[0].facility.value > 0 && document.forms[0].tofacilityid.value > 0 && document.forms[0].itemId.value.length > 0 && document.forms[0].form_qty.value.length > 0 && parseInt(document.forms[0].form_qty.value) > 0) {
                    var itemStockValue = document.forms[0].itemId.value;
                    var itemStockArr = itemStockValue.split('#');
                    if (itemStockArr[2] >= parseInt(document.forms[0].form_qty.value)) {
                        return true;
                    } else {
                        document.forms[0].form_qty.style.backgroundColor = "red";
                        alert('Number of quantity for this items you are trying to transfer is not available in stock.');
                        document.forms[0].form_qty.focus();
                        return false;
                    }
                } else {
                    if (document.forms[0].facility.value <= 0)
                    {
                        document.forms[0].facility.style.backgroundColor = "red";
                        alert('Required field missing: Please choose the facility.');
                        document.forms[0].facility.focus();
                        return false;
                    }
                    if (document.forms[0].tofacilityid.value <= 0)
                    {
                        document.forms[0].tofacilityid.style.backgroundColor = "red";
                        alert('Required field missing: Please choose the to facility.');
                        document.forms[0].tofacilityid.focus();
                        return false;
                    }
                    if (document.forms[0].itemId.value.length <= 0)
                    {
                        document.forms[0].itemField.style.backgroundColor = "red";
                        alert('Required field missing: Please choose the item.');
                        document.forms[0].itemField.focus();
                        return false;
                    }
                    if (document.forms[0].form_qty.value.length <= 0 || parseInt(document.forms[0].form_qty.value) <= 0)
                    {
                        document.forms[0].form_qty.style.backgroundColor = "red";
                        alert('Required field missing: Please enter the Item quantity.');
                        document.forms[0].form_qty.focus();
                        return false;
                    }
                }
            }
        </script>
    </head>
    <body class="ui-bg-wht">
        <?php
        // If we are saving, then save and close the window.
        // First check for duplicates.
        //
	if ($_POST['form_save']) {
            
            $message="<div class='alert alert-success alert-dismissable'>Transfer request initiated successfully!</div>";
            $itemStockValue = $_POST['itemid'];
            $itemStockArr = explode('#', $itemStockValue);
            try{
            $pdoobject->begin_transaction();     
            $itemStockDetails = getItemStockDetail($itemStockArr[0],0," FOR UPDATE ");
                
            if (!empty($itemStockDetails) && $itemStockDetails['invist_quantity'] >= $_POST['form_qty']) {
               
                //sqlStatement("update inv_item_stock set invist_quantity=invist_quantity-" . $_POST['form_qty'] . " where invist_id = '" . $itemStockArr[0] . "'");
                
                 $stockData = array(
                            'invist_quantity' => $itemStockDetails['invist_quantity']-$_POST['form_qty']                            
                    );
                    $where = array('invist_id'=>$itemStockArr[0]);
                    
                $pdoobject->update("inv_item_stock",$stockData,$where);
                $tranferData = array(
                    'invtran_item_id' => $itemStockArr[1],
                    'invtran_stock_id' => $itemStockArr[0],
                    'invtran_transfer_quantity' => escapedff('form_qty'),
                    'invtran_transfer_user_id' => $_SESSION['authId'],
                    'invtran_from_facility_id' => escapedff('facility'),
                    'invtran_to_facility_id' => escapedff('tofacilityid'),
                    'invtran_transfer_notes' => escapedff('form_desc'),
                    'invtran_transfer_date' => date("Y-m-d"),
                    'invtran_accept_user_id' => '',
                    'invtran_accept_notes' => ''
                );
                //$stockTransid = insertStockTransfer($tranferData);
                $sresult=$pdoobject->insert("inv_transfer",$tranferData);
                $tranData = array(
                    'invistr_itemid' => $itemStockDetails['invist_itemid'],
                    'invistr_batch' => $itemStockDetails['invist_batch'],
                    'invistr_expiry' => $itemStockDetails['invist_expiry'],
                    'invistr_price' => $itemStockDetails['invist_price'],
                    'invistr_quantity' => escapedff('form_qty'),
                    'invistr_before_qty' => $itemStockDetails['invist_quantity'],
                    'invistr_after_qty' => $itemStockDetails['invist_quantity'] - escapedff('form_qty'),
                    'invistr_clinic_id' => escapedff('facility'),
                    'invistr_tran_type' => 4,
                    'invistr_comment' => escapedff('form_desc'),
                    'invistr_createdby' => $_SESSION['authId'],
                    'invistr_created_date' => date("Y-m-d H:i:s"),
                    'invistr_action' => 'inventory/add_edit_transfer',
                    'invistr_action_id' => $sresult
                );
                //$transId = insertStockTransaction($tranData);
                $tresult=$pdoobject->insert("inv_item_stock_transaction",$tranData); 
                $pdoobject->commit();
            
            } else {
                $pdoobject->rollback();
                $message = '<div class="alert alert-warning alert-dismissable">Entered quantity not available in selected item stock. Please try again!</div>';
            }
             
            }catch (PDOException $e) {
               //echo $message;
               $pdoobject->rollback();  
               echo  $error=$e->getMessage(); 
               $message="<div class='alert alert-danger alert-dismissable'>Error in saving data!<div>";
            }
                
                $_SESSION['INV_MESSAGE'] = $message;
                // if ($message) echo " alert('$message');\n";
                 echo "<script language='JavaScript'>\n";
                 echo "parent.location.reload();\n";  
                 echo "</script></body></html>\n";
                exit();
        }
        ?>
        <div class="infopop"><?php xl('Add Transfer Item', 'e'); ?></div>
        <!-- tableData -->
        <div id="popUpformWrap">
            <form method='post' id='addEditTransferForm' name='theform' action='add_edit_transfer.php'>
                <!-- Row 1 -->
                <div class='row'>
                    <div class='col-sm-6'>
                        <div class='form-group cmboxTmrgn'>
                            <label><?php echo xlt('From Facility'); ?><sup class="required">*</sup></label>

                            <?php
                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                            InvUsersFacilityDropdown('facility', '', 'facility', $facility, $_SESSION['authId'],$userFacilityRestrict, $pdoobject,0);
                            ?>
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <div class='form-group'>
                            <label><?php echo xlt('To Facility');?>:<sup class="required">*</sup></label>
                            <select name='tofacilityid' id="tofacilityidField" class='formEle'>
                                <option value='0' selected="selected">Select Other Facility</option>
                                <?php
                                
                                $qsql = $pdoobject->custom_query("SELECT id, name FROM facility WHERE id != ? ORDER BY name ASC ", array($_SESSION['authId']),'','fetchAll');
                                foreach ($qsql as $facrow) {
                                    $selected = ( $facrow['id'] == $row['invist_clinic_id'] ) ? 'selected="selected"' : '';
                                    echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";
                                }
                                ?>
                            </select> 
                        </div>
                    </div>
                </div>
                <!-- Row 1 -->
                <!-- Row 2 -->
                <div class='row'>
                    <div class='col-sm-6'>
                        <div class='form-group posrel'>
                            <label>Item:<sup class="required">*</sup></label>
                            <input type="text" class="formEle" id="itemField" name="itemField" readonly>
                            <img id="loading" style="display:none" src="../../images/loader.gif" />
                            <input type="hidden" class="formEle" id="itemId" name="itemid">
                            <input type="hidden" value="0" id="form_sale" name="form_sale">
                            <input type="hidden" value="0" id="form_expiry" name="form_expiry">
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <div class='form-group'>
                            <label>Quantity:<sup class="required">*</sup></label>
                            <input type='number' size='10' name='form_qty' maxlength='7' value='0' onkeypress="return isNumber(event);" class='formEle'/>
                            <input type='hidden' id='itemQty' name='itemQty' value='' />
                        </div>
                    </div>
                </div>
                <!-- Row 2 -->
                <!-- Row 3 -->
                <div class='row'>
                    <div class='col-sm-6 txtarea'>
                        <div class='form-group'>
                            <label>Notes:<sup class="required">*</sup></label>
                            <textarea name='form_desc' rows="5" cols="34"></textarea><span class="pull-right counter-text">Max. 768 character(s)</span>
                        </div>
                    </div>
                </div>
                <!-- Row 3 -->
                <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                <input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' onclick="return submitform();" value='<?php echo xla('Save'); ?>' />
            </form>
        </div>
        <!-- tableData -->
        <div id="placeholder">
            <img src='<?php echo $inventry_url; ?>emr/images/oi-wm-logo.jpg' class='autohgt' alt='' />
        </div>
    </body>
</html>