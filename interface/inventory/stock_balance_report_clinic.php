<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.

require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Check authorization.
$thisauth = acl_check('inventory', 'icr_stock_balance');
if (!$thisauth)
    die(xlt('Not authorized'));

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
//$patient = $_REQUEST['patient'];
$from_date = fixDate($_POST['form_from_date'], '');

$searchParam = '';

$show_available_times = false;

$ORDERHASH = array(
    'invistr_id' => 'invistr_id',
    'im.inv_im_name' => 'im.inv_im_name',
    'invistr.invistr_batch' => 'invistr.invistr_batch',
    'invistr.invistr_created_date' => 'invistr.invistr_created_date'
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'im.inv_im_name';
$orderby = $ORDERHASH[$form_orderby] . ' DESC';

$form_orderby1 = $ORDERHASH[$_REQUEST['form_orderby1']] ? $_REQUEST['form_orderby1'] : 'invistr.invistr_batch';
$orderby .= ', ' . $ORDERHASH[$form_orderby1] . ' DESC';


$form_orderby2 = $ORDERHASH[$_REQUEST['form_orderby2']] ? $_REQUEST['form_orderby2'] : 'invistr.invistr_created_date';
$orderby .= ', ' . $ORDERHASH[$form_orderby2] . ' DESC';

$form_facility = $_POST['form_facility'];
$zero_quantity_items = $_POST['zero_quantity_items'];


$sellable = $_POST['form_sellable'];




// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockBalance') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=stock_balance.csv");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>

        <head>
            <?php html_header_show(); ?>

            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">

            <title><?php xl('Stock Leadger', 'e'); ?></title>

            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
            <link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
            <script type="text/javascript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

                function dosort(orderby) {
                    var f = document.forms[0];
                    f.form_orderby.value = orderby;
                    f.submit();
                    return false;
                }

                function oldEvt(eventid) {
                    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
                }

                function refreshme() {
                    document.forms[0].submit();
                }


                $(document).ready(function () {
                     var facilityID = $("#form_facility").val();
                    if(facilityID != '' ){
                    var providerID =$('#form_provider').val();
                    $.ajax({
                            url: "get_facility_provider.php?facilityId=" + facilityID,
                            success: function (result) {
                                $("#form_provider").html(result);
                                 $('#form_provider').val(providerID);
                            }
                        });

                    }
                    $("#form_facility").change(function () {
                        var facilityID = $("#form_facility").val();
                        $.ajax({
                            url: "get_facility_provider.php?facilityId=" + facilityID,
                            success: function (result) {
                                $("#form_provider").html(result);
                            }
                        });
                    });
                });
            </script>

            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">
<div class="panel panel-warning">
   
            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
            <div class="panel-heading"><?php xl('Report', 'e'); ?> - <?php xl('Stock Balance', 'e'); ?></div>
            <div class="panel-body">
            <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form method='post' name='theform' id='theform' action='stock_balance_report_clinic.php'>
                <div id="report_parameters">
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php xl('Facility', 'e'); ?>:<br>
                                <select name="form_facility" id="form_facility" class="form-control input-sm" >
                                          <?php 
                                          if(empty($row['invist_clinic_id'])){
                                              ?>
                                              <option value='0' selected="selected">Select Facility</option>
                                              <?php
                                          }
                                          $countUserFacilities = 0;
                                          if (!$GLOBALS['restrict_user_facility']) {
                                              $qsql = sqlStatement("
                                                 select id, name, color
                                                 from facility
                                                 where service_location != 0
                                                 ");
                                          } else {
                                            $qsql = sqlStatement("
                                               select uf.facility_id as id, f.name, f.color
                                               from users_facility uf
                                               left join facility f on (uf.facility_id = f.id)
                                               where uf.tablename='users' 
                                               and uf.table_id = ? 
                                               ", array($_SESSION['authId']) );
                                        }      
                                        while ($facrow = sqlFetchArray($qsql)) {       
                                          $selected = ( $facrow['id'] == $form_facility ) ? 'selected="selected"' : '' ;
                                          echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";       
                                          $countUserFacilities++;
                                          /************************************************************/
                                      }      
                                      ?>      
                                </select>
                                <?php //dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php xl('Balance On Date', 'e'); ?>:
                                <input type='text' name='form_from_date' id="form_from_date"
                                                               size='10' value='<?php echo $from_date ?>'
                                                               onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                               title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                               align='absbottom' width='24' height='22' id='img_from_date'
                                                               border='0' alt='[?]' style='cursor: pointer'
                                                               title='<?php xl('Click here to choose a date', 'e'); ?>'>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-top: 32px;">
                            <?php xl('Saleable Items', 'e'); ?>:
                            <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_POST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
                        </div>
                        <div class="col-md-3" style="margin-top: 32px;">
                            <?php xl('Zero quantity items', 'e'); ?>:
                            <input type='checkbox' name='zero_quantity_items' id="zero_quantity_items" value='1' <?php echo (isset($_POST['zero_quantity_items']) && $_POST['zero_quantity_items'] == 1) ? 'checked' : ''; ?> >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewStockBalance"); $("#theform").submit();'>
                                    <span> <?php xl('Submit', 'e'); ?> </span>
                                </a> 
                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvStockBalance"); $("#theform").submit();'>
                                    <span>
                                        <?php xl('Export to CSV', 'e'); ?>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of search parameters --> <?php
            } // end not form_csvexport
            if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockBalance') {
                    // CSV headers:
                    echo '"' . xl('S. No.') . '",';
                    echo '"' . xl('Clinic Name') . '",';
                    echo '"' . xl('Item Name') . '",';
                    echo '"' . xl('Item Code') . '",';
                    echo '"' . xl('Batch #') . '",';
                    echo '"' . xl('Expiry Date') . '",';
                    if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                        echo '"' . xl('Price') . '",';
                    }                  
                    echo '"' . xl('Quantity') . '"' . "\n";
                } else {
                    ?>
                    <div id="report_results">
                        <table class="table table-bordered table-striped">
                            <tr class='head'>
                            <thead>
                            <th><?php xl('S.No.', 'e'); ?></th>
                            <th><?php xl('Clinic Name', 'e'); ?></th>
                            <th><?php xl('Item Name', 'e'); ?></th>
                            <th><?php xl('Item Code', 'e'); ?></th>
                            <th><?php xl('Batch #', 'e'); ?></th>
                            <th><?php xl('Expiry Date', 'e'); ?></th>
                            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                <th><?php xl('Price', 'e'); ?></th>
                            <?php } ?>
                            <th><?php xl('Quantity', 'e'); ?></th>       
                            </thead>
                            <tbody>
                                <!-- added for better print-ability -->
                                <?php
                            }
//Get Stock Ledgel Detail 
                            $dateMysqlFormat = getDateDisplayFormat(1);
                            
                            if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvStockBalance') {
                                $event = "Report Stock Balance Export";
                            } else {
                                $event = "Report Stock Balance View";
                            }
                            
                            if (empty($form_facility)) {
                                ##########Cron Request####################section only for all FACILITY#########################
                                // following value will be change according to report
                                $rcsl_name_type = "stock_balance_report"; // changable
                                $dateRange = $from_date!=""?"Balance On Date ".date('d-M-y', strtotime($from_date)): "Balance On Date ".date("d-M-y");
                                $rcsl_requested_filter = addslashes(serialize(array("balanceondate" => $from_date, "saleable" => $sellable,"noDateRange"=>"$dateRange"))); // changable
                                $rcsl_report_description = "Stock Balance report "; // changable
                                //allFacilityReports() defined with globals.php
                                $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                if($msgForReportLog){
                                    $msgForReportLog;
                                }
                                else{
                                $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                }
                                ##########Cron Request####################section only for all FACILITY######################### 
                                $searchParam .= ' Balance On Date = ' . ((!empty($from_date)?$from_date:'Null')) . ' | Saleable Items = ' . ((!empty($sellable)?'Yes':'No')) . ' | Clinic = All | '; /// Auditing Section Param
                                debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                            } else {
                                
                                if (isset($form_facility) && $form_facility != '') {
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where = " invistr.invistr_clinic_id = '" . $form_facility . "'";
                                    $searchParam .= ' Clinic Id = '. $form_facility.' | ';
                                } else {
                                    $searchParam .= ' Clinic  = All | ';
                                }
                                
                                if (!empty($from_date)) {
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " invistr.invistr_created_date >= '$from_date 00:00:00' AND " . "invistr.invistr_created_date <= '$from_date 23:59:59' ";
                                   $searchParam .= ' Balance On Date = '. $from_date .'00:00:00 To =  '.$from_date .'23:59:59 | ';
                                } 
                                if (isset($sellable) && $sellable != 0) {

                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " im.inv_im_sale = '" . $sellable . "'";
                                    $searchParam .= ' Saleable Items = Yes | ';
                                } else {
                                 $searchParam .= ' Saleable Items = No | ';
                                }
                            }
                            
                            if(isset($zero_quantity_items) && $zero_quantity_items == 1){
                                 $zeroItem = 1 ;
                            }else{
                                $zeroItem = 0 ;
                            }
                            

                            $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                            
                            $stockLedgerData = getStockLedgelDetail($where, $dateMysqlFormat, 1, $msgForReportLog , $zeroItem);

                            if(count($stockLedgerData) > 0){ debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); } /// Update Report Auditing Section
                            
                            /*
                              TRUNCATE TABLE inv_item_stock;
                              TRUNCATE TABLE inv_item_stock_transaction;
                              TRUNCATE TABLE inv_issue_return;
                              TRUNCATE TABLE inv_issue_notes;
                              TRUNCATE TABLE inv_issue_return;
                              TRUNCATE TABLE inv_stock_requisition;
                              TRUNCATE TABLE inv_requisition_item;
                              TRUNCATE TABLE inv_issue_item;
                             */
                            $srCount = 1;
                            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockBalance') {
                                foreach ($stockLedgerData as $key => $val) {
                                    //CSV EXPORT - First Nested
                                    echo '"' . qescape($srCount) . '",';
                                    echo '"' . qescape($val['clinic_name']) . '",';
                                    echo '"",';
                                    echo '"",';
                                    echo '"",';
                                    echo '"",';
                                    if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                        echo '"",';
                                    }
                                    echo '""' . "\n";

                                    foreach ($val['clinicWiseDetail'] as $subKey => $subVal) {
                                        //CSV EXPORT - Second Nested
                                        echo '"",';
                                        echo '"",';
                                        echo '"' . qescape($subVal['item_name']) . '",';
                                        echo '"' . qescape($subVal['item_code']) . '",';
                                        echo '"' . qescape($subVal['item_stock_batch']) . '",';
                                        echo '"' . qescape($subVal['item_stock_expiry']) . '",';
                                        if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                            echo '"' . qescape($subVal['item_stock_price']) . '",';
                                        }
                                        echo '"' . qescape($subVal['item_stock_qty']) . '"' . "\n";
                                    }
                                    $srCount++;
                                }
                            } else {


                                foreach ($stockLedgerData as $key => $val) {
                                    ?>
                                    <tr bgcolor='<?php echo $row['invtrt_color'] ?>'>
                                        <td class="detail">&nbsp;<?php echo $srCount; ?></td>
                                        <td class="detail"><?php echo $val['clinic_name']; ?></td>
                                        <td class="detail"><?php echo $val['item_name']; ?></td>
                                        <td class="detail"></td>
                                        <td class="detail"></td>
                                        <td class="detail">	</td>
                                        <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                            <td class="detail">	</td>
                                        <?php } ?>
                                        <td class="detail">&nbsp;</td>
                                    </tr>
                                    <?php
                                    $itemNameRepet = '';
                                    $countItemBasedRunningQty = 0;
                                    //echo '<pre>';
                                    //print_r($val['clinicWiseDetail']);
                                    foreach ($val['clinicWiseDetail'] as $subKey => $subVal) {
                                        ?>
                                        <tr>
                                            <td class="detail">&nbsp;<?php echo ''; ?></td>
                                            <td class="detail"></td>
                                            <td class="detail"><?php echo $subVal['item_name']; ?></td>
                                            <td class="detail"><?php echo $subVal['item_code']; ?>	</td>
                                            <td class="detail"><?php echo $subVal['item_stock_batch']; ?></td>
                                            <td class="detail"><?php echo $subVal['item_stock_expiry']; ?>	</td>
                                            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                <td class="detail"><?php echo $subVal['item_stock_price']; ?>	</td>
                                            <?php } ?>
                                            <td class="detail">&nbsp;<?php echo $subVal['item_stock_qty']; ?></td>
                                        </tr>
                                        <?php
                                    }

                                    $srCount++;
                                }
                            }
                            // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.

                            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvStockBalance') {
                                ?>

                                <?php if (0) { ?>
                                    <tr class="report_totals">
                                        <td colspan='7' class='newtextcenter'><?php xl('Total Number of Stock Ledger Report', 'e'); ?> : <?php echo --$srCount; ?></td>
                                    </tr>
                                <?php } ?>
                                <?php
                                $srCountFlag = $srCount - 1;
                                if (!$srCountFlag) {
                                    ?>
                                    <tr>
                                        <td colspan='7' class='newtextcenter'><?php echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- end of search results --> <?php }
                } else {
                            ?>
                <div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>
                <!-- stuff for the popup calendar -->
                <style type="text/css">
                    @import url(../../library/dynarch_calendar.css);
                </style>
                <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
                <script type="text/javascript"
                src="../../library/dynarch_calendar_setup.js"></script>
                <script type="text/javascript">
                                                    Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});

                </script>

            <?php } ?> 
            <?php
            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvStockBalance') {
                ?>
                <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
                <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
                <input type='hidden' name='form_refresh' id='form_refresh' value='' /></form>

            <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
            </script>
            </div>
</div>
        </body>

        <!-- stuff for the popup calendar -->
        <style type="text/css">
            @import url(../../library/dynarch_calendar.css);
        </style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript"
        src="../../library/dynarch_calendar_setup.js"></script>
        <script type="text/javascript">
                Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});

        </script>

    </html>
    <?php
}
?>