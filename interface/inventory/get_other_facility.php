<?php
$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 $facilList = $_REQUEST['facilityId'];
 $select=0;
 if(isset($_REQUEST['select']) && $_REQUEST['select']!=""){
 echo $select=$_REQUEST['select'];	 
 }
 if ($facilList) {
 	$res = sqlStatement("
			SELECT id, name, color
			FROM facility
			WHERE id != ? ORDER BY name ASC 
		", array($facilList));
 	if(sqlNumRows($res)){
 		$optionString = "<option value='0' selected='selected'>Select Other Facility</option>";
 		while ($row = sqlFetchArray($res)) { 	
			if($row['id']==$select){
		    $optionString .= "<option value='".$row['id']."' selected='selected'>".$row['name']."</option>"; 		
			}else{		
 			$optionString .= "<option value='".$row['id']."' >".$row['name']."</option>"; 
		    }			
 		}
 		echo $optionString;
 	}else{
 		echo "<option value='0' selected='selected'>Select Other Facility</option>";
 	}
 }else {
 	echo "<option value='0' selected='selected'>Select Other Facility</option>";
 }
