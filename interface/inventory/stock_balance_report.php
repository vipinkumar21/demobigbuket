<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");


$thisauth = $invgacl->acl_check('inventory','iar_stock_blance','users', $_SESSION['authUser']);
$thisAuthDoctor = $invgacl->acl_check('inventory','icr_stock_balance','users', $_SESSION['authUser']);
if (!$thisauth && !$thisAuthDoctor) {
    die(xlt('Not authorized'));
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
//$patient = $_REQUEST['patient'];
$from_date = fixDate($_REQUEST['form_from_date'], '');
$searchParam = '';
// Get the order hash array value and key for this request.
if(isset($_REQUEST['form_facility'])){
     $_SESSION['cid']=$_REQUEST['form_facility'];
}else{
   $_REQUEST['form_facility']= $_SESSION['cid']; 
}
$form_facility = isset($_REQUEST['form_facility'])? $_REQUEST['form_facility'] : $_SESSION['Auth']['User']['facility_id'];
$zero_quantity_items = $_REQUEST['zero_quantity_items'];
$sellable = $_REQUEST['form_sellable'];
$item = $_REQUEST['form_item'];
$catid = $_REQUEST['catid'];
$fileName = "stock_balance_" . date("Ymd_his") . ".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockBalance') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Stock Balance Report', 'e'); ?></title>
            <?php include_once("themestyle.php"); ?>
            <?php include_once("scriptcommon.php"); ?>
        </head>        
        <body>
            <!-- page -->
            <div id="page" data-role="page" class="ui-content">
                <!-- header -->
                <?php include_once("oi_header.php"); ?>
                <!-- header -->
                <!-- contentArea -->
                <div id="wrapper" data-role="content" role="main">
                    <!-- wrapper -->
                    <div class='themeWrapper' id='rightpanel'>  
                        <div class='containerWrap'>
                            <!-- pageheading -->
                            <div class='col-sm-12 borbottm'>
                                <?php include_once("inv_links.html"); ?>
                                <h1><?php xl('Stock Balance Report', 'e'); ?> <span class="floatRight">Balance As On: <?php echo date("d F Y")?></span></h1>
                            </div>
                            <!-- pageheading -->
                            <!-- mdleCont -->
                            <form method='post' name='theform' id='theform' action='stock_balance_report.php' class="botnomrg">                   
                                <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                                <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                                <input type='hidden' name='form_cron' id='' value='1'/>
                                    <!-- leftPart -->
                                    <div class="filterWrapper">
                                        <!-- Facility --> 
                                        <div class="ui-block">
                                            <?php
                                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                            usersFacilityDropdown('form_facility','' ,'form_facility',$form_facility,$_SESSION['authId'],$userFacilityRestrict,$pdoobject);
                                             ?>
                                        </div>
                                        <!-- Facility --> 
                                        <!-- Balance -->
                                        <div class="ui-block form_to_date_bx">
                                            <!-- <input type='text' placeholder='Balance On Date' id='form_from_date' name='form_from_date' value='<?php //echo date("Y-m-d"); ?>' readonly="true"/> -->
<!--                                            <input type='text' placeholder='Balance On Date' name='form_from_date_stock' id="form_to_date_stock" size='10' title='' />-->
                                            <input type='hidden' name='form_from_date' id="dateFormat" size='10' value='<?php echo $from_date; ?>' />
                                        </div>
                                        <!-- Balance -->
                                        <!-- Zero -->
                                        <div class="ui-block ui-chkbx">
                                            <label for="zero_quantity_items" class="interButt">
                                                <?php xl('Zero quantity items', 'e'); ?>
                                            </label>
                                            <input type='checkbox' name='zero_quantity_items' id="zero_quantity_items" value='1' <?php echo (isset($_REQUEST['zero_quantity_items']) && $_REQUEST['zero_quantity_items'] == 1) ? 'checked' : ''; ?> />
                                        </div> 
                                        <!-- Zero --> 
                                        <!-- Saleable -->
                                        <div class="ui-block ui-chkbx">
                                            <label for="form_sellable" class="interButt">
                                                <?php xl('Saleable Items', 'e'); ?>
                                            </label>
                                            <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_REQUEST['form_sellable']) && $_REQUEST['form_sellable'] == 1) ? 'checked' : ''; ?> />
                                        </div> 
                                        <!-- Saleable -->
                                        <!-- Saleable -->
                                        <div class="ui-block ui-chkbx">
                                            <label for="form_item_wise_balance" class="interButt">
                                                <?php xl('Item Wise Balance', 'e'); ?>
                                            </label>
                                            <input type='checkbox' name='form_item_wise_balance' id="form_item_wise_balance" value='1' <?php echo (isset($_REQUEST['form_item_wise_balance']) && $_REQUEST['form_item_wise_balance'] == 1) ? 'checked' : ''; ?> />
                                        </div> 
                                        <!-- Saleable -->
                                        <div class='ui-block wdth25'>                                                                      
                                            <a class="pull-right btn_bx" id='reset_form1' href="stock_balance_report.php?form_facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                            <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "csvStockBalance");
                                                        $("#theform").submit();'>
                                                <span class="new-btnWrap btn">
                                                    <span class="csv_icon"></span>
                                                </span>
                                                <b class="btn-text"><?php xl('Export', 'e'); ?></b>
                                            </a>
                                           <a id='advanceOptions' class="pull-right" href="javascript:void(0)">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-zoom-in icon"></span>
                                            </span>
                                            <b class="btn-text">Advance</b>
                                            </a>
                                            <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "viewStockBalance");
                                                        $("#theform").submit();'>
                                                <span class="new-btnWrap btn">
                                                    <span class="glyphicon glyphicon-search icon5"></span>
                                                </span>
                                                <b class="btn-text">Search</b>
                                            </a>
                                        </div>
                                        <div class='<?php if( empty($provider) && empty($catid) && empty($item)){ echo "dnone";} ?>' id='advanceOptionsToggle'>
                                       <!-- providor -->
                                        <div class="ui-block"  id="doctorsList">
<!--                                            <select name='form_provider' id="form_provider" class='form-control input-sm'>
                                                    <option value='' selected='selected'>All Doctors</option>
                                            </select>-->
                                        </div>
                                        <!-- providor --> 
                                        <!-- category --> 
                                        <div class='ui-block'>
                                            <select class='form-control  input-sm'  name='catid' id='catid'>
                                                <?php
                                                if (empty($row['inv_im_catId'])) {
                                                    ?>
                                                    <option value='0' selected="selected">All Category</option>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                $cres=$pdoobject->fetch_multi_row("invcategories",array("id as invcat_id","name as invcat_name"),array('is_active'=>'1','deleted'=>'0')); 
                                                    foreach($cres as $crow) {
                                                        ?>
                                                        <option value='<?php echo $crow['invcat_id'] ?>' <?php
                                                        if ($_POST['catid'] == $crow['invcat_id']) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $crow['invcat_name'] ?></option>
                                                                <?php
                                                            }
                                                       // }
                                                        ?>
                                            </select>
                                        </div>
                                        <!-- category --> 
                                        <!-- items --> 
                                        <div class='ui-block'>
                                            <input type='text' placeholder='Item' name='form_item' id="form_item" size='20' value='<?php echo ($item != '') ? $item : ''; ?>' title=''>
                                        </div>
                                        <!-- items --> 
                                        
                                    </div>
                                    </div>
                                    <?php
                                } // end not form_csvexport
                                if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_POST['form_csvexport'] || empty($form_facility) || $_REQUEST['form_facility']) {
                                    //echo "--------------";
                                    if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvStockBalance') {
                                        // CSV headers:
                                        echo '"' . xl('Clinic Name') . '",';
                                        echo '"' . xl('Item Name') . '",';
                                        echo '"' . xl('Item Code') . '",';
                                        if(empty($_REQUEST['form_item_wise_balance'])){
                                        echo '"' . xl('Batch #') . '",';
                                        echo '"' . xl('Expiry Date') . '",';
                                        }
                                        if (isset($_REQUEST['form_sellable']) && $_REQUEST['form_sellable'] != 0) {
                                            echo '"' . xl('Price') . '",';
                                        }
                                        echo '"' . xl('Quantity') . '"' . "\n";
                                    } else {
                                        ?>
                                    <div id="" class='tableWrp pb-2'>
                                                <div class="dataTables_wrapper no-footer">
                                                    <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                        
                                                            <!-- added for better print-ability -->
                                                            <?php
                                                        }
                                                        $where = " 1=1 ";
                                                        $facilityIdList = $form_facility == 0 ? userFacilitys() : $form_facility;
                                                        $where .= " AND invis.invist_clinic_id IN (" . $facilityIdList . ")";
                                                        $searchParam .= $form_facility == 0 ? "Facility = All |" : "Facility = " . $facilityIdList . " | ";
                                                        $where .= (isset($sellable) && $sellable != 0) ? " AND im.inv_im_sale = '" . $sellable . "'" : '' ;
                                                        $searchParam .= (isset($sellable) && $sellable != 0) ? ' Saleable Items = Yes | ' : ' Saleable Items = No | ';
                                                        $where .= (isset($zero_quantity_items) && $zero_quantity_items == 1) ? " AND invis.`invist_quantity` ='0'" :  " AND invis.`invist_quantity` >'0'" ;
                                                        $searchParam .= (isset($zero_quantity_items) && $zero_quantity_items == 1) ? ' Zero Quantity Items = Yes | ' : ' Zero Quantity Items = No | ' ;
                                                        //$where .= (!empty($from_date)) ? " AND invis.invist_created_date <='".$from_date." 23:59:59'" : "" ;
                                                        $where .=!empty($catid) ? " AND FIND_IN_SET(" . $catid . ",im.inv_im_catId)" : "";
                                                        $searchParam .=!empty($catid) ? ' Category = ' . $catid . ' | ' : "Category = All | ";
                                                        $where .=!empty($item) ? " AND im.inv_im_name LIKE '" . $item . "%'" : "";
                                                        $searchParam .=!empty($item) ? ' Item = ' . $item . ' | ' : "";
                                                        
                                                        $res = "SELECT `invist_clinic_id` AS clinicId,
                                                                        f.name AS clinic_name,
                                                                        invis.invist_itemid,
                                                                        im.inv_im_name,
                                                                        `invist_price`,
                                                                        im.inv_im_code AS item_code,";
                                                        if(empty($_REQUEST['form_item_wise_balance'])){
                                                        $res =  $res. "invist_batch,
                                                                        CASE WHEN(invis.invist_expiry='0000-00-00') THEN ''
                                                                            ELSE DATE_FORMAT(invis.invist_expiry,'%d-%m-%y')
                                                                        END AS Expiry, `invist_quantity` ";
                                                        $group="";
                                                        $order=" ORDER BY clinicId,`invist_itemid`,`invist_batch`";
                                                        }else{
                                                             $res =  $res. " sum(`invist_quantity`) as `invist_quantity` ";                                                 $group=" GROUP BY item_code";
                                                             $order=" ORDER BY clinicId,`invist_itemid`";
                                                        }                

                                                                $res =  $res. " FROM `inv_item_stock` AS invis
                                                                 INNER JOIN `inv_item_master` AS im ON invis.invist_itemid = im.inv_im_id
                                                                 INNER JOIN facility AS f ON f.id = invis.invist_clinic_id
                                                                 WHERE $where $group ";
                                                        //Get Stock Ledgel Detail 
                                                                //echo $res; 
                                                        if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvStockBalance') {
                                                            $event = "Report Stock Balance Export";
                                                        } else {
                                                            $event = "Report Stock Balance View";
                                                        }
                                                        if (!empty($form_facility)) {

                                                            $callType = ($_POST['form_csvexport'] != 'csvStockBalance') ? 1 : 0;
                                                            $pageNo = $_GET['page'];

                                                            $num_rows = $pdoobject->custom_query($res, NULL, 1);
                                                            $per_page = $GLOBALS['encounter_page_size'];
                                                            $page = $_GET["page"];
                                                            $curr_URL = $_SERVER['SCRIPT_NAME'] . "?form_refresh=viewStockBalance&form_facility=$form_facility&form_sellable=$sellable&zero_quantity_items=$zero_quantity_items&form_item=$item&catid=$catid&";
                                                            $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                                            $page = ($page == 0 ? 1 : $page);
                                                            $page_start = ($page - 1) * $per_page;
                                                            if ($callType == 1) {
                                                                $res.= " LIMIT $page_start , $per_page";
                                                            }
                                                            $new_num_rows = $pdoobject->custom_query($res, NULL, 1);
                                                            
                                                            $result = $pdoobject->custom_query($res);
                                                            if ($_POST['form_csvexport'] != 'csvStockBalance') {
                                                                echo $pa_data;
                                                            }
                                                            if($new_num_rows>0){
                                                            if ($_REQUEST['form_csvexport'] && $_REQUEST['form_csvexport'] == 'csvStockBalance') {
                                                                $prevClinic = '';
                                                                $currClinic = '';
                                                                $currItem = '';
                                                                $prevItem = '';
                                                                $currItemcode = '';
                                                                $prevItemcode = '';
                                                                if ($new_num_rows) {
                                                                    $srCount = 1;
                                                                    foreach ($result as $row) {
                                                                        $currClinic = $row['clinicId'];
                                                                        $currItem = $row['invist_itemid'];
                                                                        $currItemcode = $row['item_code'];
                                                                        if ($currClinic != $prevClinic) {
                                                                            echo '"' . qescape($row['clinic_name']) . '",';
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            if (isset($_REQUEST['form_sellable']) && $_REQUEST['form_sellable'] != 0) {
                                                                                echo '"",';
                                                                            }
                                                                            echo '""' . "\n";

                                                                            echo '"",';
                                                                            echo '"' . qescape($row['inv_im_name']) . '",';
                                                                            echo '"' . qescape($row['item_code']) . '",';
                                                                            if(empty($_REQUEST['form_item_wise_balance'])){
                                                                            echo '"' . qescape($row['invist_batch']) . '",';
                                                                            echo '"' . qescape($row['Expiry']) . '",';
                                                                            }
                                                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                                                                echo '"' . qescape($row['invist_price']) . '",';
                                                                            }
                                                                            echo '"' . qescape($row['invist_quantity']) . '"' . "\n";
                                                                        } else {
                                                                            //CSV EXPORT - Second Nested
                                                                            echo '"",';
                                                                            if ($currItem != $prevItem) {
                                                                                echo '"' . qescape($row['inv_im_name']) . '",';
                                                                            } else {
                                                                                echo '"",';
                                                                            }
                                                                            echo '"' . qescape($row['item_code']) . '",';
                                                                            if(empty($_REQUEST['form_item_wise_balance'])){
                                                                            echo '"' . qescape($row['invist_batch']) . '",';
                                                                            echo '"' . qescape($row['Expiry']) . '",';
                                                                            }
                                                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) {
                                                                                echo '"' . qescape($row['invist_price']) . '",';
                                                                            }
                                                                            echo '"' . qescape($row['invist_quantity']) . '"' . "\n";
                                                                        }
                                                                        $prevClinic = $row['clinicId'];
                                                                        $prevItem = $row['invist_itemid'];
                                                                        $prevItemcode = $row['item_code'];
                                                                        $srCount++;
                                                                    }
                                                                }
                                                            } else {?>
                                                            <thead>
                                                            <?php if (empty($form_facility)) { ?>
                                                            <th width='12%'><?php xl('Clinic Name', 'e'); ?></th><?php } ?>
                                                        <th><?php xl('Item Name', 'e'); ?></th>
                                                        <th><?php xl('Item Code', 'e'); ?></th>
                                                        <?php if(empty($_REQUEST['form_item_wise_balance'])){?>
                                                        <th width='15%'><?php xl('Batch #', 'e'); ?></th>
                                                        <th width='10%'><?php xl('Expiry Date', 'e'); ?></th>
                                                        <?php }?>
                                                        <?php if (isset($_REQUEST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                            <th><?php xl('Price', 'e'); ?></th>
                                                        <?php } ?>
                                                        <th width='10%'><?php xl('Quantity', 'e'); ?></th>       
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                foreach ($result as $row) {
                                                                    $currClinic = $row['clinicId'];
                                                                    $currItem = $row['invist_itemid'];
                                                                    $currItemcode = $row['item_code'];
                                                                    if ($currClinic != $prevClinic && empty($form_facility)) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $row['clinic_name']; ?></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td> </td>
                                                                            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                                                <td> </td>
                                                                            <?php } ?>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <?php if (empty($form_facility)) { ?>
                                                                                <td></td><?php } ?>
                                                                            <?php if ($currItem != $prevItem && $currClinic != $prevClinic) { ?>
                                                                                <td><?php echo $row['inv_im_name']; ?></td>
                                                                            <?php } else if ($currItem == $prevItem && $currClinic != $prevClinic) { ?>
                                                                                <td><?php echo $row['inv_im_name']; ?></td>
                                                                            <?php } else {
                                                                                ?>
                                                                                <td></td>
                                                                            <?php } ?>
                                                                            <?php if ($currItemcode != $prevItemcode) { ?>
                                                                                <td><?php echo $row['item_code']; ?></td>
                                                                            <?php } else { ?>
                                                                                <td></td>

                                                                            <?php } 
                                                                            if(empty($_REQUEST['form_item_wise_balance'])){
                                                                            ?>
                                                                                
                                                                                <td><?php echo $row['invist_batch']; ?></td>
                                                                            <td><?php echo $row['Expiry']; ?>  </td>
                                                                            <?php }?>
                                                                            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                                                <td><?php echo $row['invist_price']; ?>   </td>
                                                                            <?php } ?>
                                                                            <td>&nbsp;<?php echo $row['invist_quantity']; ?></td>
                                                                        </tr>
                                                                    <?php } else {
                                                                        ?>
                                                                        <tr>
                                                                            <?php if (empty($form_facility)) { ?>
                                                                                <td></td><?php } ?>
                                                                            <?php if ($currItem != $prevItem) { ?>
                                                                                <td><?php echo $row['inv_im_name']; ?></td>
                                                                            <?php } else { ?>
                                                                                <td></td>

                                                                            <?php } if ($currItemcode != $prevItemcode) { ?>
                                                                                <td><?php echo $row['item_code']; ?></td>
                                                                            <?php } else { ?>
                                                                                <td></td>

                                                                            <?php } 
                                                                            if(empty($_REQUEST['form_item_wise_balance'])){
                                                                            ?>
                                                                            <td><?php echo $row['invist_batch']; ?></td>
                                                                            <td><?php echo $row['Expiry']; ?>  </td>
                                                                            <?php } ?>
                                                                            <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != 0) { ?>
                                                                                <td><?php echo $row['invist_price']; ?>   </td>
                                                                            <?php } ?>
                                                                            <td>&nbsp;<?php echo $row['invist_quantity']; ?></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    $prevClinic = $row['clinicId'];
                                                                    $prevItem = $row['invist_itemid'];
                                                                    $prevItemcode = $row['item_code'];
                                                                    $srCount++;
                                                                }
                                                            }
                                                        } 
                                                        
                                                    }else {
                                                        
                                                            if($_REQUEST['form_cron']){
                                                            ##########Cron Request####################section only for all FACILITY#########################
                                                            // following value will be change according to report
                                                            $rcsl_name_type = "stock_balance_report"; // changable
                                                            $rcsl_requested_filter = addslashes(serialize(array("saleable" => $sellable, "zero_quantity_items" => $zero_quantity_items, "facility" => getLoggedUserAssignedClinics()))); // changable
                                                            $rcsl_report_description = "Stock Balance report "; // changable
                                                            $getFacility = getFacilityName($form_facility);
                                                            $subject = "Stock Balance Report" . "_" . $getFacility . "_" . date('Y-m-d H:i:s');
                                                            $filters = "saleable =" . $sellable . " zero_quantity_items =" . $zero_quantity_items . " facility=" .$getFacility;
                                                            $sellebleHeader = !empty($sellable) ? ",Price," : ',';
                                                            $includeArr = array('clinic_name' ,'inv_im_name', 'item_code');
                                                            if(empty($_REQUEST['form_item_wise_balance'])){
                                                               
                                                               $includeArr[]='invist_batch';
                                                               $includeArr[]='Expiry';
                                                               $head=",Batch #,Expiry Date";
                                                            }else{
                                                              $filters=$filters. " Item Wise Balance = Yes ";   
                                                            }
                                                            if($sellable) 
                                                            {
                                                                array_push($includeArr,"invist_price","invist_quantity");
                                                            }
                                                            else
                                                            {
                                                                array_push($includeArr,"invist_quantity");
                                                            }
                                                            
                                                            
                                                             $report_data = json_encode(array(
                                                                'header' => 'Clinic Name,Item Name,Item Code'.$head.$sellebleHeader.'Quantity',
                                                                'include' =>$includeArr,
                                                                'name' => 'Stock Balance Report', 'subject' => $subject, 'facility' => $getFacility, 'filters' => $filters, 'query' => $res)
                                                            );
                                                            
                                                            $msgForReportLog .=scheduleReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description, $report_data);
                                                            if ($msgForReportLog) {
                                                                $msgForReportLog;
                                                            } else {
                                                                $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                                            }
                                                            ##########Cron Request####################section only for all FACILITY######################### 
                                                            $searchParam .= ' Balance On Date = ' . ((!empty($from_date) ? $from_date : 'Null')) . ' | Saleable Items = ' . ((!empty($sellable) ? 'Yes' : 'No')) . ' | Clinic = All | '; /// Auditing Section Param
                                                            debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                                                            }else{
                                                              $msgForReportLog="All clinic report will be sent to you over email. Pls click on Search to schedule the report!";  
                                                            }
                                                        }
                                                        if ($_POST['form_csvexport'] != 'csvStockBalance') {
                                                            if (!$srCount && empty($msgForReportLog)) {
                                                                ?>
                                                                <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                                <?php
                                                            }
                                                        }
                                                        if ($msgForReportLog) {
                                                            ?>
                                                            <tr>
                                                                <td colspan="8" class='newtextcenter' style="font-size:10pt;"> <?php echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        if ($_POST['form_csvexport'] != 'csvStockBalance') {
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php echo $pa_data; ?>
                                            </div>
                                        <!-- end of search results --> <?php
                                        }
                                    }
                                    if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvStockBalance') {
                                        ?>
                                                    <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
                                                    <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
                                                    <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            </form>
                            <!-- mdleCont -->
                        </div>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- contentArea -->
            </div>
            <!-- page -->
            <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
            </script>
        </body>
    </html>
<?php } ?>