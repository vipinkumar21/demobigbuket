<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
$thisauth = acl_check('inventory', 'invf_adj_list');
if (!$thisauth)
    die(xlt('Not authorized'));
// For each sorting option, specify the ORDER BY argument.

if (isset($_GET["mode"])) {
    if ($_GET["mode"] == "delete") {
        $trans_arr = array();
        $ajustmentDetail = getAdjustmentDetail($_GET["adjid"], $_GET["facility"]);
        $id = $ajustmentDetail['ina_item_id'];
        $stock_result = sqlQuery("SELECT invist_itemid,invist_batch,invist_expiry,invist_price,invist_quantity FROM inv_item_stock WHERE invist_clinic_id = '" . $ajustmentDetail['ina_facility_id'] . "' AND invist_id = '" . $ajustmentDetail['ina_stock_id'] . "' AND invist_isdeleted ='0' AND invist_itemid = '$id'");
        if ($ajustmentDetail['ina_type'] == 'In') {
            $qty_num = $stock_result['invist_quantity'] - $ajustmentDetail['ina_quantity'];
            if ($qty_num < 0) { // incase of less qty in stock
                $alertmsg = '<div class="alert alert-warning alert-dismissable">You should not able delete selected adjustment because of less stock!</div>';
                $_SESSION['INV_MESSAGE'] = $alertmsg;
            } else { // insert in transcation table
                $trans_arr['invistr_itemid'] = $ajustmentDetail['ina_item_id'];
                $trans_arr['invistr_batch'] = $stock_result['invist_batch'];
                $trans_arr['invistr_expiry'] = $stock_result['invist_expiry'];
                $trans_arr['invistr_price'] = $stock_result['invist_price'];
                $trans_arr['invistr_quantity'] = $ajustmentDetail['ina_quantity'];
                $trans_arr['invistr_before_qty'] = $stock_result['invist_quantity'];
                if ($ajustmentDetail['ina_type'] == 'In') {
                    $trans_arr['invistr_after_qty'] = $stock_result['invist_quantity'] - $ajustmentDetail['ina_quantity']; //deducting qty
                } else {
                    $trans_arr['invistr_after_qty'] = $stock_result['invist_quantity'] + $ajustmentDetail['ina_quantity']; //adding qty
                }
                $trans_arr['invistr_clinic_id'] = $ajustmentDetail['ina_facility_id'];
                $trans_arr['invistr_tran_type'] = 16;
                $trans_arr['invistr_comment'] = "Adjustment Deleted.";
                $trans_arr['invistr_isdeleted'] = 0;
                $trans_arr['invistr_createdby'] = 1;
                $trans_arr['invistr_created_date'] = date('Y-m-d H:i:s');

                insertStockTransaction($trans_arr); // insert in transaction table
            }
        }

        if ($ajustmentDetail['ina_type'] == 'In') {
            $itemStockDetails = getItemStockDetail($ajustmentDetail['ina_stock_id']);
            if ($itemStockDetails['invist_quantity'] >= $ajustmentDetail['ina_quantity']) {
                removeStockQuantity($ajustmentDetail['ina_quantity'], $ajustmentDetail['ina_stock_id']);
            } else {
                $alertmsg = '<div class="alert alert-warning alert-dismissable">You should not able delete selected adjustment because of less stock!</div>';
                $_SESSION['INV_MESSAGE'] = $alertmsg;
                header('Location: adjustmentList.php?facility=' . $_GET["facility"]);
                exit;
            }
        } else {
            addStockQuantity($ajustmentDetail['ina_quantity'], $ajustmentDetail['ina_stock_id']);
        }
        $adjData = array(
            'ina_id' => $ajustmentDetail['ina_id'],
            'ina_isdeleted' => '1'
        );
        $adjId = insertUpdateAdjustment($adjData);
        $transData = array(
            'invistr_id' => $ajustmentDetail['invistr_id'],
            'invistr_isdeleted' => '1'
        );
        $alertmsg = '<div class="alert alert-warning alert-dismissable">Adjustment deleted successfully!</div>';
        $_SESSION['INV_MESSAGE'] = $alertmsg;
    }
}
//
$ORDERHASH = array(
    'invtran_id' => 'invtran_id DESC'
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[urldecode($_REQUEST['form_orderby'])] ? urldecode($_REQUEST['form_orderby']) : 'invtran_id';
$orderby = $ORDERHASH[$form_orderby];
if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
}else{
   $_REQUEST['facility']= $_SESSION['cid']; 
}
$facility = isset($_REQUEST['facility']) ? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$form_item = urldecode($_REQUEST['form_item']);
$from_date = urldecode($_REQUEST['form_from_date']);
$to_date = urldecode($_REQUEST['form_to_date']);
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Adjustments'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body>
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <h1><?php xl('Adjustments', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='adjustmentList.php' name='theform' id='theform' class="botnomrg">                    
                            <div class="filterWrapper">
                                <!-- first column starts --> 
                                <div class="ui-block">
                                    <?php
                                    $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                    $userId = $_SESSION['authId'];
                                    usersFacilityDropdown('facility', '', 'facility', $facility, $userId, $userFacilityRestrict, $pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends -->
                                <!-- fifth column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' placeholder='Item' name='form_item' id="form_item" value='<?php echo $form_item ?>' title='' />
                                </div>
                                <!-- fifth column ends -->

                                <!-- second column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                </div>
                                <!-- second column ends --> 

                                <!-- third column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                </div>
                                <!-- third column ends -->

                                <!-- fourth column starts -->
                                <div class="ui-block wdth20">                                                                        
                                    <a class="pull-right btn_bx" id='reset_form1' href="adjustmentList.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true");
                                            $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- fourth column ends -->
                            </div>

                            <!-- tableData -->
                            <!-- <table border='0' cellpadding='1' cellspacing='2' width='100%'>
                            <?php
                            //if (!empty($_SESSION['alertmsg'])) {
                            ?>
                                    <tr >
                                        <td colspan="2"><?php //echo $_SESSION['alertmsg']; $_SESSION['alertmsg'] = "";  ?></td>     
                                    </tr>
                            <?php
                            //}
                            ?>
                            </table> -->
                            <?php
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_from_date'] || $_REQUEST['form_to_date'] || $_REQUEST['form_item'] || empty($facility)) {
                                $sql = "SELECT adj.ina_id, adj.ina_item_id, adj.ina_stock_id, adj.ina_quantity, adj.notes, CASE WHEN (adj.ina_type='In') THEN 'Add' ELSE 'Remove' END  AS ina_type, adj.ina_facility_id, adj.ina_addedby, adj.ina_added_date, adj.ina_isdeleted, ist.invist_id, im.inv_im_name, im.inv_im_catId AS `CatId`,  CONCAT_WS(' ', iseu.fname, iseu.lname) AS iseName, fa.name as facility_name " .
                                        "FROM inv_adjustments AS adj
                                        INNER JOIN users AS iseu ON iseu.id = adj.ina_addedby       
                                        INNER JOIN inv_item_stock AS ist ON ist.invist_id = adj.ina_stock_id
                                        INNER JOIN facility As fa ON  fa.id = adj.ina_facility_id
                                        INNER JOIN inv_item_master AS im ON im.inv_im_id = adj.ina_item_id" .
                                        " WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' ";
                                
                                //AND adj.ina_addedby = " . $_SESSION['authId']


                                if (!empty($facility)) { // if facility exists
                                    $sql .= " AND adj.ina_facility_id = '" . $facility . "'";
                                } else {
                                    $sql .= " AND adj.ina_facility_id IN(" . getLoggedUserAssignedClinics() . ")";
                                }

                                if (!empty($from_date) && empty($to_date)) { // If from dates exists
                                    $sql .= " AND adj.ina_added_date >= '$from_date 00:00:00'";
                                }
                                if (!empty($to_date) && empty($from_date)) { // If to dates exists
                                    $sql .= " AND " . "adj.ina_added_date <= '$to_date  23:59:59' ";
                                }
                                if (!empty($from_date) && !empty($to_date)) {
                                    $sql .= " AND " . "adj.ina_added_date BETWEEN '$from_date  00:00:01' AND '$to_date  23:59:59' ";
                                }
                                if (!empty($form_item)) {
                                    $sql .= " AND im.inv_im_name like '%$form_item%'";
                                }
                                $sql .= " ORDER BY adj.ina_id DESC";
                                $num_rows = $pdoobject->custom_query($sql, NULL, 1);
                                $per_page = $GLOBALS['encounter_page_size'];
                                $page = $_GET["page"];

                                $page = ($page == 0 ? 1 : $page);
                                $page_start = ($page - 1) * $per_page;
                                $curr_URL = $_SERVER[SCRIPT_NAME] . '?form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&facility=' . $facility . '&form_item=' . urldecode($_REQUEST['form_item']) . '&form_to_date=' . urldecode($_REQUEST['form_to_date']) . '&form_from_date=' . urldecode($_REQUEST['form_from_date']) . '&';
                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                $sql .= " LIMIT $page_start , $per_page";
                                $res = $pdoobject->custom_query($sql);
                                
                                ?>
                                <div id="" class='tableWrp pb-2'>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                    <div class='dataTables_wrapper no-footer'>
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                            <?php if ($num_rows > 0) { ?>
                                                <thead>
                                                    <tr>
                                                        <?php if (empty($facility)) { ?>
                                                            <th width="15%">   
                                                                <?php echo xlt('Clinic Name'); ?>
                                                            </th><?php } ?>
                                                        <th>   
                                                            <?php echo xlt('Name'); ?>
                                                        </th>
                                                        <th width="10%">
                                                            <?php echo xlt('Quantity'); ?>
                                                        </th>  
                                                        <th width="10%">
                                                            <?php echo xlt('Adj. Type'); ?>
                                                        </th> 
                                                        <th width="35%">
                                                            <?php echo xlt('Notes'); ?>
                                                        </th>
                                                        <th width="10%">
                                                            <?php echo xlt('Added Date'); ?>
                                                        </th>
        <!--                                                        <th width="10%" class="txtRight">
                                                        <?php //echo xlt('Action'); ?>
                                                        </th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php } ?>
                                                <?php
                                                $lastid = "";
                                                $encount = 0;
                                               
                                                //if ($pdoobject->custom_query($sql, NULL, 1)) {
                                                if(!empty($res)){
                                                    $srCount = 1;
                                                    $prevClinic = '';
                                                    $currClinic = '';
                                                    $currItem = '';
                                                    $prevItem = '';
                                                    foreach ($res As $row) {
                                                        $currClinic = $row['ina_facility_id'];
                                                        $currItem = $row['ina_item_id'];
                                                        if (empty($facility)) {
                                                            if($currClinic!=$prevClinic){
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $row['facility_name']; ?></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <?php }?>
                                                            <tr>
                                                                <?php //if (empty($facility)) { ?>
                                                                    <td></td>
                                                                <?php //} ?>
                                                                <?php 
                                                                    
                                                               if($currClinic!=$prevClinic){
                                                                    ?>
                                                                    <td><?php echo $row['inv_im_name']; ?></td>
                                                                     
                                                                <?php }else{
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }}?>  
                                                                <td><?php echo $row['ina_quantity']; ?></td>
                                                                <td><?php echo $row['ina_type']; ?></td>
                                                                <td><?php echo $row['notes']; ?></td>
                                                                <td><?php echo invDateFormat($row['ina_added_date']); ?></td>
                                                            </tr>
                                                        <?php } else {
                                                            
                                                            ?>
                                                            <tr>
                                                                
                                                                   <?php 
                                                                    
                                                               
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }//}?>
                                                              
                                                                <td><?php echo $row['ina_quantity']; ?></td>
                                                                <td><?php echo $row['ina_type']; ?></td>
                                                                <td><?php echo $row['notes']; ?></td>
                                                                <td><?php echo invDateFormat($row['ina_added_date']); ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        $prevClinic = $row['ina_facility_id'];
                                                        $prevItem = $row['ina_item_id'];


//                                                    if ($row['ina_isdeleted'] == 0) {
//                                                        if (acl_check('inventory', 'invf_adj_edit')) {
//                                                            echo "<div class='listbtn addEditDataSml' data-pageUrl='edit_adj.php?adjid=" . attr($lastid) . "&facility=" . attr($facility) . "'><span class='dashboardEditIcon'></span>Edit</div> &nbsp; ";
//                                                        }
//                                                        //if(acl_check('inventory', 'invf_adj_del')){ echo "<a class='iconanchor' data-target='#myModal' data-toggle='modal' href='#' onClick='javascript: confirmPopUp (\"Delete\", \"Are you sure?\", \"adjustmentList.php?mode=delete&facility=".$facility."&adjid=".attr($lastid)."\")' title='Delete'><span class='glyphicon glyphicon-trash'></span></a> &nbsp; ";}
//                                                        if (acl_check('inventory', 'invf_adj_del')) {
//                                                            echo "<div class='listbtn dialg' data-title='Do you want to delete the adjustment??' data-action='adjustmentList.php?mode=delete&facility=" . $facility . "&adjid=" . attr($lastid) . "'><span class='dashboardDeleteIcon'></span>Delete</div> &nbsp; ";
//                                                        }
//
//                                                        // if(acl_check('inventory', 'invf_reqout_del')){ echo "<a data-target='#myModal' data-toggle='modal' href='#' onClick='javascript: confirmPopUp (\"Delete\", \"Are you sure?\", \"requisitionFromClinic.php?mode=delete&facility=".$facility."&reqid=".attr($lastid)."\")' title='Delete'><span class='glyphicon glyphicon-trash'></span></a> &nbsp; ";}
//                                                    }
//                                                    echo "</td>\n";
                                                    } // end while
                                                } else {
                                                    ?>
                                                <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                </div>
                            <?php } else { ?>
                                <div class='text leftmrg6 dnone'><?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                                </div>
                            <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            <!-- tableData -->
                        </form>
                        <!-- mdleCont -->
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->	
        </div>
        <!-- page -->
    </body>
</html>