<?php
    // Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
    //
     // This program is free software; you can redistribute it and/or
    // modify it under the terms of the GNU General Public License
    // as published by the Free Software Foundation; either version 2
    // of the License, or (at your option) any later version.

    $sanitize_all_escapes = true;
    $fake_register_globals = false;

    require_once("../globals.php");
    require_once("./lib/database.php");
    require_once("./lib/inv.gacl.class.php");
    require_once("./lib/inv.users.class.php");
    require_once("$srcdir/acl.inc");
    require_once("drugs.inc.php");
    require_once("$srcdir/options.inc.php");
    require_once("$srcdir/formatting.inc.php");
    require_once("$srcdir/htmlspecialchars.inc.php");
    //echo "<pre>";
    //print_r($_SESSION);die();
    
    if(isset($_REQUEST['cid'])){
      $_SESSION['cid']=$_REQUEST['cid'];
      //if(empty($_SESSION['reset_cid'])){
         $_SESSION['reset_cid']=$_REQUEST['cid'];
     //}
    }
    
    $type=$invfac->getFacilityType($_SESSION['cid']);
    $access=$invfac->validateFacilityId($_SESSION['cid']);
    
    $invaccess=$invfac->getFacilityId($_SESSION['Auth']['User']['id']);
    
    
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <link rel='shortcut icon' href='<?php echo $inventry_url; ?>emr/interface/img/favicon-oneivory.png' type='image/x-icon'/ >
        <title>Inventory Dashboard</title> 
        <!-- CSS Libraries --> 
        <link rel="stylesheet" href="<?php echo $inventry_url; ?>web/css/bootstrap.css" />        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo $inventry_url; ?>emr/interface/themes/oi_theme/myStyle.css" />
    </head>    
    <body class="inventorypage"> 

    <!-- START: loader -->
    <div id="customLoading"><img src="<?php echo $inventry_url; ?>emr/interface/img/oi_loader.gif" /></div>
    <div class="ui-widget-overlay"></div>
    <!-- END: loader -->

	  <!-- header -->
	  <div class='floatingheader transition transition-hidden'>
	    <?php include_once("im_header.html"); ?>
	    <div id='collapse-bar'></div>
	  </div>
	  <!-- header -->

        <!-- Brand Name --> 
        <section class="section1">
            <section class="section1a">
            </section>
            <section class="section1b">
                <img src="../img/logo-home.jpg" >       
            </section> 
        </section> 

        <!-- Main data --> 
        <section class="section4 dnone">
            
               <?php
                  if($invaccess){
               ?>
                <p class="liner"></p> 
            <h1> Welcome to OneIvory Inventory</h1><br/>
            <div id="mainBox" class="mainBox">
                <div id="managerBlock" class="boxes col-xs-3 newHide">
                    <div class="greenbox">
                        <a href="#" id="managerBtn" class="newHide">
                            <span class="icon dummyIcon manager"></span> 
                            <span>Managers</span>
                            <span class="topBar"></span>
                            <span class="triangle"></span>   
                        </a> 
                    </div>                   
                </div> 

                <div id="reportsBlock" class="boxes col-xs-3 newHide">
                    <div class="greenbox">
                        <a href="#" id="reportsBtn" class="newHide">
                            <span class="icon dummyIcon reports"></span> 
                            <span>Reports</span> 
                            <span class="topBar"></span>
                            <span class="triangle"></span>
                        </a> 
                    </div> 
                </div> 

               <?php
               if($type['type']=='1' && $access){
               ?>
                <div id="marketplaceLink" class="boxes col-xs-3 newHide">
                    <div class="greenbox">
                        <a data-ajax='false' href="<?php echo $inventry_url; ?>emr/interface/inventory/marketplace.php">
                            <span class="icon dummyIcon marketplace"></span> 
                            <span>Marketplace</span> 
                        </a> 
                    </div> 
                </div> 
               <?php }?>

                <div class="boxes col-xs-3">
                    <div class="greenbox">
                        <a data-ajax='false' href="<?php echo $inventry_url; ?>webapp/view/home.shtml#/workArea" id="prmBtn">
                            <span class="icon dummyIcon prmLink"></span> 
                            <span>Back to PRM</span> 
                        </a> 
                    </div> 
                </div> 


                <div id="managerOptionsBar">
                    <div id="stockLink" class="yellowbox newHide">
                        <a href="stocks.php" id="stock">
                            <span class="icon dummyIcon stock"></span> 
                            <span>Stock</span>
                        </a> 
                    </div> 

                    <div id="requisitionLink" class="yellowbox newHide">
                        <a href="requisitionFromClinic.php" id="requisition">
                            <span class="icon dummyIcon reqs"></span> 
                            <span>Requisition</span>
                        </a> 
                    </div> 

                    <div id="shipmentLink" class="yellowbox newHide">
                        <a href="requisitionToClinic.php" id="shipment">
                            <span class="icon dummyIcon shipment"></span> 
                            <span>Shipment</span>
                        </a> 
                    </div> 

                    <div id="transferRecieptLink" class="yellowbox newHide">
                        <a href="transferToClinic.php" id="transferReciept">
                            <span class="icon dummyIcon transin"></span> 
                            <span>Transfer<br />Receipt</span>
                        </a> 
                    </div> 

                    <div id="transferShipmentLink" class="yellowbox newHide">
                        <a href="transferFromClinic.php" id="transferShipment">
                            <span class="icon dummyIcon transout"></span> 
                            <span>Transfer<br />Shipment</span>
                        </a> 
                    </div> 

                    <div id="issueReturnLink" class="yellowbox newHide">
                        <a href="issue_return.php" id="returns">
                            <span class="icon dummyIcon issue_return"></span> 
                            <span>Issue/Return</span>
                        </a> 
                    </div> 

                    <div id="adjustmentsLink" class="yellowbox newHide">
                        <a href="adjustmentList.php" id="adjustments">
                            <span class="icon dummyIcon adjustment"></span> 
                            <span>Adjustments</span>
                        </a> 
                    </div> 

                    <div id="consumptionLink" class="yellowbox newHide">
                        <a href="consumption.php" id="consumptions">
                            <span class="icon dummyIcon consumption"></span> 
                            <span>Consumptions</span>
                        </a> 
                    </div> 
                </div>

                <div id="reportsOptionsBar">
                    <div id="stockLedgerLink" class="yellowbox newHide">
                        <a href="stock_leadger_report.php" id="managerBtn">
                            <span class="icon dummyIcon stockLedger"></span> 
                            <span>Stock Leadger</span> 
                        </a> 
                    </div> 

                    <div id="consumptionReportLink" class="yellowbox newHide">
                        <a href="consumption_report.php" id="managerBtn">
                            <span class="icon dummyIcon consumption"></span> 
                            <span>Consumption</span> 
                        </a> 
                    </div> 

                    <div id="batchExpiryLink" class="yellowbox newHide">
                        <a href="batch_expiry_report.php" id="managerBtn">
                            <span class="icon dummyIcon manager2"></span> 
                            <span>Batch and Expiry</span> 
                        </a> 
                    </div> 

                    <div id="adjustmentReportLink" class="yellowbox newHide">
                        <a href="adjustment_report.php" id="managerBtn">
                            <span class="icon dummyIcon adjustment"></span> 
                            <span>Adjustment</span> 
                        </a> 
                    </div> 

                    <div id="stockBalanceLink" class="yellowbox newHide">
                        <a href="stock_balance_report.php" id="managerBtn">
                            <span class="icon dummyIcon stockBalance"></span> 
                            <span>Stock Balance</span> 
                        </a> 
                    </div> 
                </div>
                    <?php }else{?>
                <p class="liner"></p> 
            <h1> OneIvory Inventory is Disabled for your Clinic!</h1><br/>
            <div id="mainBox" class="mainBox">
                <div class="boxes col-xs-3">
                    <div class="greenbox">
                        <a data-ajax='false' href="<?php echo $inventry_url; ?>webapp/view/home.shtml#/workArea" id="prmBtn">
                            <span class="icon dummyIcon prmLink"></span> 
                            <span>Back to PRM</span> 
                        </a> 
                    </div> 
                </div> 
                    <?php }?>
            </div> 

        </section> 

        <!-- Footer --> 
        <section class="section5">
            <p> &copy; 2017 One Ivory, India. All Rights Reserved.</p> 
        </section>

        <!-- jquery lib_new -->
        <script type="text/javascript" src="<?php echo $inventry_url; ?>webapp/js/jquery/jquery-1.11.1.min.js"></script>
        <!-- jqueryui -->
        <script type="text/javascript" src="<?php echo $inventry_url; ?>web/js/jquery-ui.min.js"></script>
        <!-- global vairables declaration -->
        <script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/contains.js"></script>
        <!-- floatingheader -->
        <script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/transition-plug.js"></script>
        <!-- commonscript -->
        <script type="text/javascript" src="<?php echo $inventry_url; ?>emr/library/inventory-db.js"></script>
    </body>
</html>