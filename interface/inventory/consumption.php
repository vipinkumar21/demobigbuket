<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
 // This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
$thisauth = $invgacl->acl_check('inventory', 'invf_cons_list', 'users', $_SESSION['authUser']);
if (!$thisauth)
    die(xlt('Not authorized'));

/* if (isset($_GET["mode"])) {
  if ($_GET["mode"] == "delete") {
  if(checkUomStatus($_GET["itemid"])){
  sqlStatement("update inv_item_master set inv_im_deleted='1' where inv_im_id = '" . $_GET["itemid"] . "'");
  $_SESSION['alertmsg'] = 'Item deleted successfully.';
  header('Location: items.php');
  exit;
  }else {
  $_SESSION['alertmsg'] = 'You should not able delete selected item.';
  header('Location: items.php');
  exit;
  }

  }
  if ($_GET["mode"] == "activate") {
  sqlStatement("update inv_item_master set inv_im_status='1' where inv_im_id = '" . $_GET["itemid"] . "'");
  $_SESSION['alertmsg'] = 'Item activated successfully.';
  header('Location: items.php');
  exit;
  }
  if ($_GET["mode"] == "deactivate") {
  sqlStatement("update inv_item_master set inv_im_status='0' where inv_im_id = '" . $_GET["itemid"] . "'");
  $_SESSION['alertmsg'] = 'Item deactivated successfully.';
  header('Location: items.php');
  exit;
  }
  } */
// For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
    'inv_im_id' => 'inv_im_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[urldecode($_REQUEST['form_orderby'])] ? urldecode($_REQUEST['form_orderby']) : 'inv_im_id';
$orderby = $ORDERHASH[$form_orderby];
if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
}else{
   $_REQUEST['facility']= $_SESSION['cid']; 
}
$facility = isset($_REQUEST['facility']) ? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$form_item = urldecode($_REQUEST['form_item']);
$from_date = urldecode($_REQUEST['form_from_date']);
$to_date = urldecode($_REQUEST['form_to_date']);

// get drugs
/* echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " .
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby"; */
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Item Consumption'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body class="consumptionManagerPage">
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        
        <!-- forGlobalMessages -->
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>				
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <h1><?php xl('Item Consumption', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='consumption.php'  name='theform' id='theform'>
                            <!-- leftPart -->
                            <div class="filterWrapper">
                                <!-- first column starts --> 
                                <div class="ui-block">
                                    <?php
                                    $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                    $userId = $_SESSION['authId'];
                                    usersFacilityDropdown('facility', '', 'facility', $facility, $userId, $userFacilityRestrict, $pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends --> 
                                <!-- fifth column starts -->
                                <div class="ui-block">
                                    <input type='text' placeholder='Item' name='form_item' id="form_item" value='<?php echo $form_item ?>' title='' />
                                </div>
                                <!-- fifth column ends -->

                                <!-- second column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                </div>
                                <!-- second column ends --> 

                                <!-- third column starts --> 
                                <div class='ui-block form_to_date_bx'>
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                </div>
                                <!-- third column ends --> 

                                <!-- fourth column starts -->
                                <div class="ui-block wdth20">                                                                        
                                    <a class="pull-right btn_bx" id='reset_form1' href="consumption.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" id="patientDashboardBtn" onclick='$("#form_refresh").attr("value", "true");
                                                            $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- fourth column ends -->
                            </div>
                            <!-- leftPart -->

                            <!-- tabledata -->
                            <table border='0' cellpadding='0' cellspacing='0' width='100%' class='emrtable'>
                                <?php
                                if (!empty($_SESSION['alertmsg'])) {
                                    ?>
                                    <tr >
                                        <td colspan="2"><?php echo $_SESSION['alertmsg'];
                                $_SESSION['alertmsg'] = "";
                                    ?></td>		
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            <?php
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_from_date'] || $_REQUEST['form_to_date'] || $_REQUEST['form_item'] || empty($facility)) {
                                $sql = "SELECT ist.invistr_id, ist.invistr_itemid, ist.invistr_batch, ist.invistr_tran_type, ist.invistr_expiry, ist.invistr_price, ist.invistr_quantity, ist.invistr_clinic_id, ist.invistr_isdeleted, ist.invistr_createdby, ist.invistr_comment, ist.invistr_tran_type, ist.invistr_created_date, im.inv_im_name ,fa.name as facility_name " .
                                        "FROM inv_item_stock_transaction AS ist  
                                                        INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invistr_itemid 
                                                        INNER JOIN facility As fa ON  fa.id = ist.invistr_clinic_id
                                                        WHERE ist.invistr_isdeleted = '0' AND im.inv_im_deleted = '0' AND ist.invistr_tran_type IN (8,12,17,19) ";//AND ist.invistr_createdby = '" . $_SESSION['authId'] . "' ";
                                if (!empty($facility)) { // if facility exists
                                    $sql .= " AND ist.invistr_clinic_id = '" . $facility . "'";
                                } else {
                                    $sql .= " AND ist.invistr_clinic_id IN(" . getLoggedUserAssignedClinics() . ")";
                                }
                                if (!empty($from_date)) { // If from dates exists
                                    $sql .= " AND ist.invistr_created_date >= '$from_date 00:00:00'";
                                }
                                if (!empty($to_date)) { // If to dates exists
                                    $sql .= " AND " . "ist.invistr_created_date <= '$to_date  23:59:59' ";
                                }
                                if (!empty($form_item)) {
                                    $sql .= " AND im.inv_im_name like'%" . $form_item . "%'";
                                }
                                $sql .= " ORDER BY ist.invistr_id DESC";
                                $num_rows = $pdoobject->custom_query($sql, null, 1); // total no. of rows
                                $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                //$per_page=2;
                                $page = $_GET["page"];
                                $page = ($page == 0 ? 1 : $page);
                                $page_start = ($page - 1) * $per_page;
                                $curr_URL = $_SERVER[SCRIPT_NAME] . '?form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&facility=' . $facility . '&form_item=' . urldecode($_REQUEST['form_item']) . '&form_to_date=' . urldecode($_REQUEST['form_to_date']) . '&form_from_date=' . urldecode($_REQUEST['form_from_date']) . '&';
                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                $sql .= " LIMIT $page_start , $per_page";
                                $res = $pdoobject->custom_query($sql);
                                ?>
                            <div id='' class='tableWrp pb-2'>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                    <div class="dataTables_wrapper no-footer">
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                            <?php if ($num_rows > 0) { ?>
                                                <thead>
                                                    <tr>
                                                        <?php if (empty($facility)) { ?>
                                                            <th>   
                                                                <?php echo xlt('Clinic Name'); ?>
                                                            </th>
                                                        <?php } ?>
                                                        <th>   
                                                            <?php echo xlt('Item Name'); ?>
                                                        </th>  
                                                        <th width='10%'>
                                                            <?php echo xlt('Batch#'); ?>
                                                        </th>
                                                        <th width='10%'>
                                                            <?php echo xlt('Type'); ?>
                                                        </th>
                                                        <th width='15%'>
                                                            <?php echo xlt('Notes'); ?>
                                                        </th>
                                                        <th >
                                                            <?php echo xlt('Quantity'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo xlt('Expiry Date'); ?>
                                                        </th>  
                                                        <th>
                                                            <?php echo xlt('Date'); ?>
                                                        </th>
        <!--                                                                        <th class='txtCenter' width="8%">
                                                        <?php // echo xlt('Action'); ?>
                                                        </th>-->
                                                    </tr>
                                                </thead>
                                            <?php } ?>

                                            <?php
                                            $lastid = "";
                                            $encount = 0;
                                            if (!empty($res)) {
                                                $prevClinic = '';
                                                $currClinic = '';
                                                $currItem = '';
                                                $prevItem = '';
                                                foreach ($res as $row) {
                                                    $lastid = $row['invistr_id'];
                                                    $currClinic = $row['invistr_clinic_id'];
                                                    $currItem = $row['invistr_itemid'];
                                                    $itemExpiry = invDateFormat($row['invistr_expiry']);
                                                    if (empty($facility)) {
                                                         if($currClinic!=$prevClinic){
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $row['facility_name']; ?></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                         <?php }?>
                                                        <tr>
                                                            <?php //if (empty($facility)) { ?>
                                                                    <td></td>
                                                                <?php //} ?>
                                                             <?php 
                                                                    
                                                                if($currClinic!=$prevClinic){
                                                                    ?>
                                                                    <td><?php echo $row['inv_im_name']; ?></td>
                                                                     
                                                                <?php }else{
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }}?> 
                                                            <td><?php echo $row['invistr_batch']; ?></td>
                                                            <td><?php echo ($row['invistr_tran_type'] == 19) ? 'Bulk' : ' Individual'; ?></td>
                                                            <td><?php echo $row['invistr_comment']; ?></td>
                                                            <td><?php echo $row['invistr_quantity']; ?></td>
                                                            <td>&nbsp;<?php echo $itemExpiry; ?></td>
                                                            <td><?php echo invDateFormat($row['invistr_created_date']); ?></td>
                                                        </tr>
                                                    <?php } else { ?>
                                                        <tr>
                                                           
                                                            <?php 
                                                                    
                                                               
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }//}?>
                                                            <td><?php echo $row['invistr_batch']; ?></td>
                                                            <td><?php echo ($row['invistr_tran_type'] == 19) ? 'Bulk' : ' Individual'; ?></td>
                                                            <td><?php echo $row['invistr_comment']; ?></td>
                                                            <td><?php echo $row['invistr_quantity']; ?></td>
                                                            <td>&nbsp;<?php echo $itemExpiry; ?></td>
                                                            <td><?php echo invDateFormat($row['invistr_created_date']); ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    $prevClinic = $row['invistr_clinic_id'];
                                                    $prevItem = $row['invistr_itemid'];
                                                }
                                            } else {
                                                ?>
                                                <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                </div>
                                <?php } else { ?>
                                <div class='text dnone leftmrg6'>
                                    <?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                                </div>
                            <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            <!-- tabledata -->
                        </form>
                        <!-- mdleCont -->
                    </div>				
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
        <!-- page -->
        <?php
if(isset($_SESSION['errorcsv'])){
               
$url=$GLOBALS['cakePhpFullUrl']."InvoiceItemStocks/download";  
?>
           <script language='JavaScript'>
               //alert('sadasdasdasd');
              window.location.href='<?php echo $url;?>';
              //window.location.reload();
              setTimeout(function() {
                            $('#customLoading').hide();
                            $('.ui-widget-overlay').hide();
                        }, 3000);
              
            </script>
 <?php              
  }
?>
    </body>
</html>


