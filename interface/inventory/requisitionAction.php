<?php
require_once("../globals.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("mag_update_status.php");
require_once("$srcdir/classes/class.phpmailer.php");

$alertmsg = '';
$reqid = $_REQUEST['reqid'];
$facility = $_REQUEST['facility'];
$magorderid=$_REQUEST['magorderid'];
$info_msg = "";
$tmpl_line_no = 0;
//$_SESSION['erroracction'] ="";

if (!acl_check('inventory', 'invf_reqout_view')) die(xlt('Not authorized'));
$userGroupsdata = getUserGroup('value');
if(empty($reqid)){
echo "<script language='JavaScript'>\n";
echo " alert('You have not selected requisition.');\n";
echo " if (opener.refreshme) opener.refreshme();\n";
echo " window.close();\n";
echo "</script></body></html>\n";
exit();
}

if(isset($_POST['action'])) {
if ($_POST["action"] == "2" || $_POST["action"] == "1") {
$response=0;
$message="";
$status=getStatus($userGroupsdata[0],$_POST["action"]);
$newmeaasge[]=array('id'=>$_SESSION['authId'],'Action'=>$_POST["action"],'message'=>$_POST['reason']);


$rs=sqlStatement("select isr_number, message, isr_from_clinic, isr_to_clinic from inv_stock_requisition where isr_id = '" . $_POST["reqid"] . "'");
if(sqlNumRows($rs)){
$row = sqlFetchArray($rs);
//echo $row['message'];
$message =  unserialize($row['message']);
$orderid = $row['isr_number'];
$from_clinic=$row['isr_from_clinic'];
$to_clinic=$row['isr_to_clinic'];
} 
if(!empty($message)){
//$message=$message; 
$message= serialize(array_merge($message , $newmeaasge));
}else{
$message= serialize($newmeaasge);  
}
$response=sqlStatement("update inv_stock_requisition set isr_isapproved='".$status."', message='".$message."' where isr_id = '" . $_POST["reqid"] . "'");
//$response=1;
if($response){
	
if($status=='1')
	{
	   if($to_clinic=='35'){
		   sendMail($to_clinic,'WarehouseRequisitionAlert', $from_clinic,$_POST["reqid"],$orderid);
	   }else{
         sendMail($to_clinic,'WarehouseRequisitionAlert', $from_clinic,$_POST["reqid"],$orderid,'clin');
		} 
       sendMail('0','UserRequisitionAccept',$_POST["facility"],$_POST["reqid"],$orderid);
    }else{
       sendMail('0','UserRequisitionReject',$_POST["facility"],$_POST["reqid"],$orderid);
}		
		
echo "<script>parent.$('body').fancybox.close();</script>";  
echo "<script>parent.$('#theform').submit();</script>"; 
}else{
//$_SESSION['erroracction'] = 'Error in Action';
header('Location: requisitionAction.php?reqid='.$_POST["reqid"].'&facility='.$_POST["facility"].'&errer=1&magorderid='.$_POST["magorderid"]);
}		       
//
exit;
} 




}
?>
<html>
<head>
<?php html_header_show(); ?>
<title><?php xlt("View"); echo ' ' . xlt('Stock Requisition'); ?></title>
<link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<style>
td { font-size:10pt; }
</style>
<script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../../library/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="../../library/topdialog.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/js/underscore.1.5.2.js"></script>-->
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script language="JavaScript">

<?php echo require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

// This is for callback by the find-code popup.
// Appends to or erases the current list of related codes.
function set_related(codetype, code, selector, codedesc) {
var f = document.forms[0];
var s = f.form_related_code.value;
if (code) {
if (s.length > 0) s += ';';
s += codetype + ':' + code;
} else {
s = '';
}
f.form_related_code.value = s;
}


function formSubmit(){
if($("#action").val()=='100'){
$("#action").css("background-color","red");
jAlert ("<?php xl('Required field missing: Please choose action.','e');?>", "Alert");
$("#action").focus();
return false;
}
if($("#action").val()=='2'){
if($("#reason").val().trim()==''){
$("#reason").css("background-color","red");
jAlert ("<?php xl('Required field missing: Please enter reason.','e');?>", "Alert");
$("#reason").focus();
return false;
}


}

$('#theform').submit(function() {
	$('#addRequisition').attr("disabled", true);
	$("#loader").show();
});		
//$('#theform').submit();
//$('#addRequisition').attr("disabled", true);
return true;

}

</script>

</head>

<body class="body_top">
<?php
$row = sqlQuery("SELECT istreq.isr_id, istreq.isr_number, istreq.isr_from_clinic, istreq.isr_to_clinic, istreq.isr_isapproved, istreq.isr_status, istreq.isr_isdeleted, isr_createdby, istreq.isr_created_date, frf.name AS fromFacility, tof.name AS toFacility " .
"FROM inv_stock_requisition AS istreq 
INNER JOIN facility AS frf ON frf.id = istreq.isr_from_clinic
INNER JOIN facility AS tof ON tof.id = istreq.isr_to_clinic " .
"WHERE istreq.isr_id = ? AND istreq.isr_from_clinic = ? ORDER BY istreq.isr_id DESC", array($reqid, $facility));
?>
<table border='0' width='100%' class='emrtable'>
<h3 class="emrh3">View Requisition</h3>
<tr>
<td nowrap width="25%"><b><?php echo xlt('Facility'); ?>:</b></td>
<td>	 
<span><?php echo $row['fromFacility'];?></span>

</td>
</tr>
<tr>
<td valign='top' nowrap><b><?php echo xlt('Other Facility'); ?>:</b></td>
<td>
<span><?php echo $row['toFacility'];?></span>   	
</td>
</tr>
<tr>
<td valign='top' nowrap><b><?php echo xlt('Requisition#'); ?>:</b></td>
<td>
<span><?php echo $row['isr_number'];?></span>
</td>
</tr>
<tr>
<td valign='top' nowrap><b><?php echo xlt('Approve Status'); ?>:</b></td>
<td>
<?php if($row['isr_isapproved'] == 2){ echo 'Rejected';}else if($row['isr_isapproved'] == 1){echo 'Approved';} else {echo 'Waiting';}?>
</td>
</tr>
<tr>
<td valign='top' nowrap><b><?php echo xlt('Requisition Status'); ?>:</b></td>
<td>
<?php if($row['isr_status'] == 2){ echo 'Completed';}else if($row['isr_status'] == 1){echo 'Partially Completed';} else {echo 'In Progress';} ?>	
</td>
</tr>  
</table>


<div id="requisitionListContainer" style="margin-top:20px;">
<table id='requisitionList' width='100%' cellpadding='0' cellspacing='0' border='1' class='emrtable'>
<thead>
<tr>
<!-- <th width='16%'>Category</th>
<th width='16%'>Sub-Category</th> -->
<th width='50%'>Item Name</th>
<th width='30%'>Item Code</th>
<th width='20%'>Quantity</th>					
</tr>
</thead>
<tbody>
<?php 

$res = sqlStatement("SELECT irit.iri_itemid, irit.iri_reqid, irit.iri_quantity, irit.iri_isdeleted, im.inv_im_name, im.inv_im_code FROM inv_requisition_item AS irit 
        INNER JOIN inv_item_master AS im ON im.inv_im_id = irit.iri_itemid 
        WHERE irit.iri_reqid = ?", array($reqid));

if(sqlNumRows($res)){
while ($itemrow = sqlFetchArray($res)) {
?>
<tr <?php if($itemrow['iri_isdeleted'] == 1){echo 'class="strikeThrough"';}?>>				
<!-- <td>
<?php //echo $itemrow['invcat_name'];?>					
</td>
<td>
<?php //if(!empty($itemrow['invsubcat_name'])){ echo $itemrow['invsubcat_name'];}else{echo 'NA';}?>						
</td> -->
<td>
<?php echo $itemrow['inv_im_name'];?>					
</td>
<td>
<?php echo $itemrow['inv_im_code'];?>					
</td>
<td>
<?php echo $itemrow['iri_quantity'];?>					
</td>

</tr>
<?php 
}
}
?>
</tbody>
</table>
</div>
<br>
<form method='post' id='theform' name='theform' class='emrtable' action="requisitionAction.php">
<input id='reqid' type='hidden' name="reqid" value="<?php echo $reqid;?>">
<input id='facility' type='hidden' name="facility" value="<?php echo $facility;?>">
<input id='magorderid' type='hidden' name="magorderid" value="<?php echo $magorderid;?>">

<h3 class="emrh3">Requisition Action</h3>
<table border='0' width='100%' cellpadding="3" cellspacing="0" style="">
<?php if(isset($_GET['errer']) && $_GET['errer']=="1"){?>
<tr>
<td  colspan="2" bgcolor="#FF0000" align="center"><b>Error in Action</b></td>
</tr>
<?php } ?>
<tr>
<td width='30%' nowrap><b><?php echo xlt('Action'); ?>:</b></td>
<td width='80%'>	 
<select name="action" id="action" class='formEle' >
<option value='100'>Select Action</option>
<option value='1'>Approved</option> 
<option value='2'>Rejected </option>
</select>
</td>
</tr>
<tr>
<td valign='top' nowrap><b><?php echo xlt('Reason'); ?>:</b></td>
<td>
<textarea name="reason" id="reason" rows="4" cols="50"></textarea>
</td>
</tr>


<tr>
<td colspan='2' align='center'>
<input class="btn btn-warning btn-sm" id='addRequisition' type="submit" value="Add" onclick="return formSubmit();"><img id="loader" style="display: none;" src="../../images/ui-anim_basic_16x16.gif"/>

</td>
</tr> 
</table>
</form>
<script language="JavaScript">
<?php
if ($alertmsg) {
echo "alert('" . htmlentities($alertmsg) . "');\n";
}
?></script>

</body>
</html>
