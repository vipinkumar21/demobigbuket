<?php
$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");

if (isset($_REQUEST['page']) && !empty($_REQUEST['page']) && isset($_REQUEST['clinic']) && $_REQUEST['clinic'] != "") {



    $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
    $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
    $sql .= " WHERE ";

    $sql .= "uf.facility_id IN (" . $_REQUEST['clinic'] . " )";

    switch ($_REQUEST['page']) {
        case "stock_ladger":
            $sql .= "AND gag.id IN (11,12,18,24,25) ";
            break;
        case "consumption":
            //$sql .= "AND gag.id IN (11,23) ";
            $sql .= "AND gag.id IN (11,12,18,24,25) ";
            break;
        
        default:
            echo "Your favorite color is neither red, blue, nor green!";
    }
    $sql .= "  AND u.active='1' ORDER BY completename";


    $rs = $pdoobject->custom_query($sql);
    $slected = "selected='selected'";
    foreach ($rs as $rows) {
        if ($rows['id'] == $_REQUEST['selected']) {
            $slected = " selected='selected' ";
        } else {
            $slected = "";
        }
        $returnData.= "<option value='" . attr($rows['id']) . "'" . $slected;
        $returnData.= ">" . text($rows['completename']);
        $returnData.= "</option>";
    }

//exit;
}
?> 
<select name='form_provider' id="form_provider" class='form-control input-sm'>
    <option value='' <?php echo $slected ?>>All Doctors</option>
    <?php echo $returnData; ?>

</select>


