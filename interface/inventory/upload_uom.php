<?php
/**
 *
 * Upload Unit Of Measurement CSV : (Swati Jain)
 *
 * */
require_once("../globals.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");

if (!strpos($_SERVER['HTTP_REFERER'], "upload_csv_process")) { // Reset CSV uploaded msg display
    $_SESSION['msg'] = '';
}
if ($_POST['form_csv_sample'] && $_POST['form_csv_sample'] == 'sample') { // Sample CSV Format
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=uom.csv");
    header("Content-Description: File Transfer");
    echo xl('Uom Name') . ',';
    echo '"' . xl('Description') . '"' . "\n";
    echo '"Packet",';
    echo '"Packet Description",' . "\n";
    echo '"Unit	",';
    echo '"Unit Description",' . "\n";
    exit;
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php xl('Unit Of Measurement CSV Upload', 'e'); ?></title>
        <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script> 
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
        <script type="text/javascript">
        // On UOM CSV upload
            function submitUomCsv() {

                if (document.forms['uomInsert'].uom_file.value.length > 0) {
                    top.restoreSession();
                    document.forms['uomInsert'].submit();
                } else {
                    if (document.forms['uomInsert'].uom_file.value.length <= 0)
                    {
                        document.forms['uomInsert'].uom_file.style.backgroundColor = "red";
                        //alert("<?php xl('Required field missing: Please browse the Unit Of Measurement file', 'e'); ?>");
                         jAlert ('Required field missing: Please browse the Unit Of Measurement file', 'Alert');
                        document.forms['uomInsert'].uom_file.focus();
                        return false;
                    }
                }
            }
        </script>

        <link rel='stylesheet' href='<?php echo $css_header ?>' type='text/css'>
        <link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
        <style type="text/css">

            /* specifically include & exclude from printing */
            @media print {
                #report_parameters {
                    visibility: hidden;
                    display: none;
                }
                #report_parameters_daterange {
                    visibility: visible;
                    display: inline;
                    margin-bottom: 10px;
                }
                #report_results table {
                    margin-top: 0px;
                }
            }

            /* specifically exclude some from the screen */
            @media screen {
                #report_parameters_daterange {
                    visibility: hidden;
                    display: none;
                }
                #report_results {
                    width: 100%;
                }
            }

        </style>

    </head>
    <body class="body_top">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <?php xl('Import', 'e'); ?> - <?php xl('Unit Of Measurement CSV', 'e'); ?></div>

            <div class="panel-body">
                <form name='uomInsert' id='uomInsert' method='post' enctype='multipart/form-data' action='upload_csv_process.php'>
                    <input type='hidden' name='mode' id='mode' value='import_uom'/>

                    <div class="row">
                        <div class="col-xs-4"><?php xl('UOM', 'e'); ?>:	<input type="file" name="uom_file" style="display:inline-block;"></div>
                        <div class="col-xs-4" id="mergePatientDataButton"><a href='#' class='btn btn-warning btn-sm' onclick='return submitUomCsv();'>
                                <span>
                                    <?php xl('Upload Unit Of Measurement', 'e'); ?>
                                </span>
                            </a>	</div>
                    </div>
                </form>
                <!-- end of parameters -->
                <form name='uomcsv' id='uomcsv' method='post' enctype='multipart/form-data' action='upload_uom.php'>
                    <input type='hidden' name='form_csv_sample' id='form_csv_sample' value=''/>
                    <div class="row">
                        <div class="col-xs-4">Note: Please Find Sample CSV Format</div>
                        <div class="col-xs-4"><a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csv_sample").attr("value", "sample");
            $("#uomcsv").submit();'>
                                <span><?php xl('Sample CSV', 'e'); ?></span>
                            </a></div>
                        <div><b><?php echo $_SESSION['msg']; ?></b></div>
                    </div>


                </form>

            </div>

        </div>