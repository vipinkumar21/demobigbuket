<?php
// Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
//
 // This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");

$alertmsg = '';
$issueid = $_REQUEST['issueid'];
$facility = $_REQUEST['facility'];
$info_msg = "";
$tmpl_line_no = 0;

if (!$invgacl->acl_check('inventory', 'invf_issued_item_return_consume', 'users', $_SESSION['authUser']))
    die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo $item_id ? xlt("Edit") : xlt("Add");
        echo ' ' . xlt('Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
                function isNumber(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                }
            // $(document).ready(function() {
            //     $('#addeditreturn').submit(function(){
            //         if($("#form_rqty, #form_cqty").val().trim() == ""){
            //             $(this).val(0);
            //         }
            //       $("#form_rqty, #form_cqty").on("keyup", function() {
            //         if ($(this).val().trim() == "")
            //           $(this).val(0);
            //       });
            //     });


            //     if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

            //       $("#form_rqty, #form_cqty").on("keyup", function() {
            //         if ($(this).val().trim() == "")
            //           $(this).val(0);
            //       });
            //     }
            // });

        </script>
    </head>
    <body class="ui-bg-wht">
        <?php
        // If we are saving, then save and close the window.
        // First check for duplicates.
        //
        
	if ($_POST['form_save']) {
            
            if(preg_match("/^[0-9]+$/",$_POST['form_cqty']) && preg_match("/^[0-9]+$/",$_POST['form_rqty']) && (!empty($_POST['form_cqty'] || !empty($_POST['form_rqty'])))){
            $itemStockDetails = getUserStockDetail($_POST['form_id'],'1');
            //print_r($itemStockDetails);
            $invirRemainQty = $itemStockDetails['invir_quantity'] - ($itemStockDetails['invir_return_qty'] + $itemStockDetails['invir_cons_qty']);
            $totalEnteredQty = $_POST['form_rqty'] + $_POST['form_cqty'];
            
            if ($invirRemainQty >= $totalEnteredQty) {
                $issueData = array(
                    'invir_id' => $_POST['form_id'],
                    'invir_return_qty' => $itemStockDetails['invir_return_qty'] + $_POST['form_rqty'],
                    'invir_cons_qty' => $itemStockDetails['invir_cons_qty'] + $_POST['form_cqty'],
                    'invir_return_date' => date("Y-m-d")
                );
                try {
                    $pdoobject->begin_transaction();
                    $issueId = insertUpdateIssueReturn($issueData);
                    if ($_POST['form_rqty'] > 0) {
                        $clinicItemStockDetails = getItemStockDetail($itemStockDetails['invir_stock_id'],'1');
                        $beforeQty = $clinicItemStockDetails['invist_quantity'];
                        $tranData = array(
                            'invistr_itemid' => $itemStockDetails['invist_itemid'],
                            'invistr_batch' => $itemStockDetails['invist_batch'],
                            'invistr_expiry' => $itemStockDetails['invist_expiry'],
                            'invistr_price' => $itemStockDetails['invist_price'],
                            'invistr_quantity' => escapedff('form_rqty'),
                            'invistr_before_qty' => $beforeQty,
                            'invistr_after_qty' => $beforeQty + escapedff('form_rqty'),
                            'invistr_clinic_id' => escapedff('form_facility_id'),
                            'invistr_tran_type' => 7,
                            'invistr_comment' => '',
                            'invistr_createdby' => $_SESSION['authId'],
                            'invistr_created_date' => date("Y-m-d H:i:s"),
                            'invistr_action' => 'inventory/issue_return',
                            'invistr_action_id' => $_POST['form_id']
                        );
                        $stockTranid = insertStockTransactionIssueReturn($tranData, $itemStockDetails['invir_stock_id']);
                        $userIssueReturnData = array(
                            "iuir_issueid" => $itemStockDetails['invir_id'],
                            "iuir_quantity" => escapedff('form_rqty'),
                            "iuir_tranis" => $stockTranid
                        );
                        $stockRelid = $pdoobject->insert("inv_user_issue_return", $userIssueReturnData);
                    }
                    if ($_POST['form_cqty'] > 0) {
                        $clinicItemStockDetails = getItemStockDetail($itemStockDetails['invir_stock_id'],'1');
                        $beforeQty = $clinicItemStockDetails['invist_quantity'];
                        $tranData = array(
                            'invistr_itemid' => $itemStockDetails['invist_itemid'],
                            'invistr_batch' => $itemStockDetails['invist_batch'],
                            'invistr_expiry' => $itemStockDetails['invist_expiry'],
                            'invistr_price' => $itemStockDetails['invist_price'],
                            'invistr_quantity' => escapedff('form_cqty'),
                            'invistr_before_qty' => $beforeQty,
                            'invistr_after_qty' => $beforeQty + escapedff('form_cqty'),
                            'invistr_clinic_id' => escapedff('form_facility_id'),
                            'invistr_tran_type' => 7,
                            'invistr_comment' => '',
                            'invistr_createdby' => $_SESSION['authId'],
                            'invistr_created_date' => date("Y-m-d H:i:s")
                        );
                        
                        $stockTranid = insertStockTransactionIssueReturn($tranData, $itemStockDetails['invir_stock_id']);
                        $beforeQty = $beforeQty + escapedff('form_cqty');
                        $tranData = array(
                            'invistr_itemid' => $itemStockDetails['invist_itemid'],
                            'invistr_batch' => $itemStockDetails['invist_batch'],
                            'invistr_expiry' => $itemStockDetails['invist_expiry'],
                            'invistr_price' => $itemStockDetails['invist_price'],
                            'invistr_quantity' => escapedff('form_cqty'),
                            'invistr_before_qty' => $beforeQty,
                            'invistr_after_qty' => $beforeQty - escapedff('form_cqty'),
                            'invistr_clinic_id' => escapedff('form_facility_id'),
                            'invistr_tran_type' => 8,
                            'invistr_comment' => '',
                            'invistr_createdby' => $_SESSION['authId'],
                            'invistr_created_date' => date("Y-m-d H:i:s"),
                            'invistr_action' => 'inventory/issue_return',
                            'invistr_action_id' => $_POST['form_id']
                        );
                        $stockTranid = insertStockTransactionIssueReturn($tranData, $itemStockDetails['invir_stock_id']);
                        $userConsumptionData = array(
                            "iuic_issueid" => $itemStockDetails['invir_id'],
                            "iuic_quantity" => escapedff('form_rqty'),
                            "iuic_tranid" => $stockTranid
                        );
                    }
                    $pdoobject->commit();
                    $info_msg = "<div class='alert alert-success alert-dismissable'>Item has been returned successfully!</div>";
                } catch (Exception $exc) {
                    $pdoobject->rollback();
                    $info_msg ="<div class='alert alert-danger alert-dismissable'>Error in saving Data!</div>";
                }

                // Close this window and redisplay the updated list of drugs.
                //  
                $_SESSION['INV_MESSAGE'] = $info_msg;

                
            } else {
                $alertmsg = '<div class="alert alert-warning alert-dismissable">Entered quantity not available in selected issue item stock. Please try again!</div>';
                $_SESSION['INV_MESSAGE'] = $alertmsg;
            }
        }else{
            $_SESSION['INV_MESSAGE'] ="<div class='alert alert-danger alert-dismissable'>Please enter valid data!</div>";
        }
         echo "<script language='JavaScript'>\n";
                echo " parent.location.reload();\n";
                echo "</script></body></html>\n";
                exit();   
            
        }
        if ($issueid && $facility) {
             $sql = "SELECT im.inv_im_name,invir.invir_id, invir.invir_item_id, invir.invir_stock_id,"
                     . " invir.invir_facility_id,invir.invir_quantity,"
                     . "(SUM(invir.invir_return_qty)+SUM(invir.invir_cons_qty)) as total, invir.invir_return_qty ,invir.invir_cons_qty "
                    . "FROM inv_issue_return AS invir "
                    . " Inner Join inv_item_master as im on im.inv_im_id = invir.invir_item_id"
                    . " WHERE invir.invir_id = ? AND invir.invir_facility_id = ? ";
            $row = $pdoobject->custom_query($sql, array($issueid, $facility), '', 'fetch');
        } else {
            $row = array(
                'invir_id' => 0,
                'invir_item_id' => 0,
                'invir_stock_id' => 0,
                'invir_facility_id' => 0
            );
        }
        ?>
<div class="infopop"><?php  xl('Consume/Return Item','e'); ?></div>

<!-- tableData -->
<div id='popUpformWrap'>
    <form class='nomrgn' method='post' name='theform' id='addeditreturn' action='add_edit_return.php?issueid=<?php echo $issueid; ?>&facility=<?php echo $facility; ?>'>
        <input type='hidden' name='form_id' value='<?php echo attr($row['invir_id']) ?>' />
        <input type='hidden' name='form_item_id' value='<?php echo attr($row['invir_item_id']) ?>' />
        <input type='hidden' name='form_stock_id' value='<?php echo attr($row['invir_stock_id']) ?>' />
        <input type='hidden' name='form_facility_id' value='<?php echo attr($row['invir_facility_id']) ?>' />	
        <input type='hidden' name='invir_quantity' value='<?php echo attr($row['invir_quantity']) ?>' />
        <input type='hidden' name='total' value='<?php echo attr($row['total'])  ?>' />
        
        <div class="row labelsdata">
            <div class="col-sm-6">
                <div class="form-group">
                    <b>Item Name:</b>
                    <span><?php echo $row['inv_im_name'];?></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <b>Issued Quantity:</b>
                    <span><?php echo $row['invir_quantity'];?></span>
                </div>
            </div>
        </div>

        <div class="row labelsdata">
            <div class="col-sm-6">
                <div class="form-group">
                    <b>Returned Quantity:</b>
                    <span><?php echo $row['invir_return_qty'];?></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <b>Consumed Quantity:</b>
                    <span><?php echo $row['invir_cons_qty'];?></span>
                </div>
            </div>
        </div>
        
        <div class="row labelsdata">
            <div class="col-sm-6">
                <div class="form-group">
                    <b>Balance Quantity :</b>
                    <span><?php echo ($row['invir_quantity']-$row['total']);?></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    
                </div>
            </div>
        </div>
        <!-- Row 1 -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Return Quantity</label>
                    <input type='number' size='10' name='form_rqty' id='form_rqty' onkeypress="return isNumber(event);" maxlength='7' value='0' />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Consumption Quantity</label>
                    <input type='number' size='10' name='form_cqty' id='form_cqty' onkeypress="return isNumber(event);" maxlength='7' value='0' />

                </div>
            </div>
        </div>
        <!-- Row 1 -->
        <a id="subform" href="javascript:void(0)" class="save-btn"></a>
        <input type='submit' class="btn btn-warning btn-sm" name='form_save' id='form_save' value='<?php echo xla('Save'); ?>' />
    </form>
</div>
<!-- tableData -->
<script language="JavaScript">
//$("#form_save").click(function(event) {
//    parent.location.reload();
//});
</script>
</body>
</html>