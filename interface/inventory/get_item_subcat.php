<?php
$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 $catid = $_REQUEST['catid'];
 if ($catid) {
 	$res = sqlStatement("SELECT scat.invsubcat_id, scat.invsubcat_name FROM inv_cat_subcat_map AS cmscat INNER JOIN inv_subcategory AS scat ON scat.invsubcat_id = cmscat.icsm_scatid WHERE scat.invsubcat_status='1' AND scat.invsubcat_deleted='0' AND cmscat.icsm_deleted = '0' AND cmscat.icsm_catid = ?", array($catid));
 	if(sqlNumRows($res)){
 		$optionString = "<option value='0' >Select Sub Category</option>";
 		while ($row = sqlFetchArray($res)) { 			
 			$optionString .= "<option value='".$row['invsubcat_id']."'>".$row['invsubcat_name']."</option>"; 			
 		}
 		echo $optionString;
 	}else{
 		echo "<option value='0' selected='selected'>Select Sub Category</option>";
 	}
 }else {
 	echo "<option value='0' selected='selected'>Select Sub Category</option>";
 }