<?php

function checkCustomAcl($sectionValue) {

    $groupId = $_SESSION['Auth']['User']['group_id'];
    $invQuery = "SELECT CustomSectionMap.detail_value, CustomSectionDetail.value, CustomSectionDetail.type, CustomSectionDetail.hour_details 
                    FROM custom_section_maps AS CustomSectionMap 
                    INNER JOIN custom_section_details AS CustomSectionDetail ON (CustomSectionMap.detail_value = CustomSectionDetail.id)  
                    WHERE CustomSectionMap.section_value = '$sectionValue' AND CustomSectionMap.aro_id = '$groupId'";

    $invSql = sqlStatement($invQuery);
    $invRow = sqlFetchArray($invSql);
    if ($invRow['value'] == 'view' && $invRow['type'] == '3') {
        return true;
    } else {
        return false;
    }
}
?>

<!-- START: prmLeftNav -->
<div id='prmleftnav' class='dnone'>
    <div id="effect" class="ui-widget-content ui-corner-all">
        <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left">
            <ul class="nav navmenu-nav sidemenu invlftnav" id='master'>
                <?php if(checkCustomAcl('inventory_stock')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="stocks.php">
                        <span class="new-btnWrap btn">
                            <span class="stock lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Stock</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_req_out')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="requisitionFromClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="reqs lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Requisition</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_req_in')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="requisitionToClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="shipment lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Shipment</b>
                    </a>
                </li>
                
                <?php if(checkCustomAcl('inventory_trans_in')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="transferToClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="transin lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Trans. Rcpt</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_trans_out')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="transferFromClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="transout lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Trans . Ship</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_int_issue')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="issue_return.php">
                        <span class="new-btnWrap btn">
                            <span class="issue_return lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Issue/Return</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_adjustments')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="adjustmentList.php">
                        <span class="new-btnWrap btn">
                            <span class="adjustment lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Adjustment</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_consumption')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="consumption.php">
                        <span class="new-btnWrap btn">
                            <span class="consumption lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Consumption</b>
                    </a>
                </li>
            </ul>
            <ul class="nav navmenu-nav sidemenu dnone" id='reports'>
                <?php if(checkCustomAcl('inventory_stock_ledger')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="stock_leadger_report.php">
                        <span class="new-btnWrap btn">
                            <span class="sledger lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Stock<br />Leadger</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_consumption_report')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="consumption_report.php">
                        <span class="new-btnWrap btn">
                            <span class="consumption lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Consum-<br />ption</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_batch_expiry')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="batch_expiry_report.php">
                        <span class="new-btnWrap btn">
                            <span class="batch_expiry lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Batch/<br />Expiry</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_adjustments_report')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="adjustment_report.php">
                        <span class="new-btnWrap btn">
                            <span class="adjust_report lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Adjustment</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_batch_expiry')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="stock_balance_report.php">
                        <span class="new-btnWrap btn">
                            <span class="sbalance lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Stock<br />Balance</b>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <span id="leftMenuToggle" class="ui-state-default ui-corner-all left-nav"></span>
</div>
<!-- END: prmLeftNav -->

<!-- START: oneivoryLeftNav -->
<div id='oileftnav' class='dnone'>
    <div class="navigation">
        <nav class="navmenu">
            <ul class="" id='oi_master'>
                <?php if(checkCustomAcl('inventory_stock')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="stocks.php">
                        <span class="new-btnWrap btn">
                            <span class="stock lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Stock</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_req_out')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="requisitionFromClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="reqs lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Requisition</b>
                    </a>
                </li>


                <?php if(checkCustomAcl('inventory_req_in')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="requisitionToClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="shipment lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Shipment</b>
                    </a>
                </li>

                <?php if(checkCustomAcl('inventory_trans_in')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="transferToClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="transin lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Trans. Rcpt</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_trans_out')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="transferFromClinic.php">
                        <span class="new-btnWrap btn">
                            <span class="transout lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Trans . Ship</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_int_issue')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="issue_return.php">
                        <span class="new-btnWrap btn">
                            <span class="issue_return lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Issue/Return</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_adjustments')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="adjustmentList.php">
                        <span class="new-btnWrap btn">
                            <span class="adjustment lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Adjustment</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_consumption')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="consumption.php">
                        <span class="new-btnWrap btn">
                            <span class="consumption lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Consumption</b>
                    </a>
                </li>
            </ul>
            <ul class="dnone" id='oi_reports'>
                <?php if(checkCustomAcl('inventory_stock_ledger')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="stock_leadger_report.php">
                        <span class="new-btnWrap btn">
                            <span class="sledger lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Stock<br />Leadger</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_consumption_report')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="consumption_report.php">
                        <span class="new-btnWrap btn">
                            <span class="consumption lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Consum-<br />ption</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_batch_expiry')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="batch_expiry_report.php">
                        <span class="new-btnWrap btn">
                            <span class="batch_expiry lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Batch/<br />Expiry</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_adjustments_report')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="adjustment_report.php">
                        <span class="new-btnWrap btn">
                            <span class="adjust_report lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Adjustment</b>
                    </a>
                </li>
                <?php if(checkCustomAcl('inventory_batch_expiry')): ?>
                <li>
                <?php else: ?> 
                <li class="dnone">
                <?php endif; ?>
                    <a data-ajax="false" href="stock_balance_report.php">
                        <span class="new-btnWrap btn">
                            <span class="sbalance lftico"></span>
                        </span>
                        <b class="btn-text mb-02">Stock<br />Balance</b>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <span class="nav_puller"></span>
</div>
<!-- END: oneivoryLeftNav -->
