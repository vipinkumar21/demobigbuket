<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $cat_id = $_REQUEST['cat'];
 $scat_id = $_REQUEST['scat'];
 $info_msg = "";
 $tmpl_line_no = 0;

 if (!acl_check('inventory', 'im_scat_add') || !acl_check('inventory', 'im_scat_edit')) die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
<?php html_header_show(); ?>
<title><?php echo $cat_id ? xlt("Edit") : xlt("Add New"); echo ' ' . xlt('Sub Category'); ?></title>
<link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>
<link rel='stylesheet' href="../themes/bootstrap.css" type="text/css">
<style>
td { font-size:10pt; }
</style>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="../../library/topdialog.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/commonScript.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script language="JavaScript">

<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

// This is for callback by the find-code popup.
// Appends to or erases the current list of related codes.
function set_related(codetype, code, selector, codedesc) {
 var f = document.forms[0];
 var s = f.form_related_code.value;
 if (code) {
  if (s.length > 0) s += ';';
  s += codetype + ':' + code;
 } else {
  s = '';
 }
 f.form_related_code.value = s;
}
</script>
<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
	if (document.forms[0].form_name.value.length>0) {
   return true;
	} else {		
		if (document.forms[0].form_name.value.length<=0)
		{
			document.forms[0].form_name.style.backgroundColor="red";
			//alert("<?php xl('Required field missing: Please enter the Sub Category Name.','e');?>");
       jAlert ('Required field missing: Please enter the Sub Category Name.', 'Alert');
			document.forms[0].form_name.focus();
			return false;
		}			
	}
}
</script>
</head>

<body class="body_top">
<?php
// If we are saving, then save and close the window.
// First check for duplicates.
//
if ($_POST['form_save']) {
  $crow = sqlQuery("SELECT COUNT(invsubcat_id) AS count FROM inv_subcategory WHERE " .
    "invsubcat_name = '"  . escapedff('form_name')  . "' AND invsubcat_deleted = '0' AND " .    
    "invsubcat_id != ?", array($scat_id));
  if ($crow['count']) {
    $alertmsg = addslashes(xl('Cannot add this sub category because it already exists!'));
  }
}

if (($_POST['form_save'] || $_POST['form_delete']) && !$alertmsg) {
  $new_drug = false;
  if ($scat_id) {
   if ($_POST['form_save']) { // updating an existing drug
    sqlStatement("UPDATE inv_subcategory SET " .
     "invsubcat_name = '"           . escapedff('form_name')          . "', " .
     "invsubcat_desc = '"     . escapedff('form_desc')    . "', " .     
     "invsubcat_status = '"       . (empty($_POST['form_active']) ? 0 : 1) . "', " .     
     "invsubcat_createdby = "          . $_SESSION['authId'] . " " .
     "WHERE invsubcat_id = ?", array($scat_id));    
   }
   else { // deleting
    if (acl_check('admin', 'super') && checkSubCatStatus($scat_id)) {
     sqlStatement("update inv_subcategory set invsubcat_deleted='1' WHERE invsubcat_id = ?", array($scat_id));     
    }
   }
  }
  else if ($_POST['form_save']) { // saving a new drug
   $new_drug = true;
   $cat_id = sqlInsert("INSERT INTO inv_subcategory ( " .
    "invsubcat_name, invsubcat_desc, invsubcat_status, invsubcat_createdby " .    
    ") VALUES ( " .
    "'" . escapedff('form_name')          . "', " .
    "'" . escapedff('form_desc')    . "', " .	
    "'" . (empty($_POST['form_active']) ? 0 : 1) . "', " . $_SESSION['authId'] .
    ")");
  } 

  // Close this window and redisplay the updated list of drugs.
  //
  echo "<script language='JavaScript'>\n";
  if ($info_msg) echo " alert('$info_msg');\n";
  echo "parent.location.reload();\n";  
  // echo " window.close();\n";  
  echo "</script></body></html>\n";
  echo "<script> parent.$.fn.fancybox.close();
    parent.location.reload();</script>";
  exit();
}

if ($scat_id) {  
$row = sqlQuery("SELECT invsubcat_id, invsubcat_name, invsubcat_desc, invsubcat_status, invsubcat_deleted, invsubcat_createdby FROM inv_subcategory WHERE invsubcat_id = ?", array($scat_id));
}else {
$row = array(
    'invsubcat_name' => '',
    'invsubcat_desc' => '',
    'invsubcat_status' => '1'    
  );
}
?>

<form method='post' name='theform' action='add_edit_subcategory.php?cat=<?php echo $cat_id; ?>&scat=<?php echo $scat_id; ?>'>
<h3 class='emrh3'>Add Sub Category</h3>
<table border='0' width='100%' class='emrtable'>

  <!-- tr>
  <td valign='top' nowrap><b><?php echo xlt('Category'); ?>:</b></td>
  <td>
  	<select name='catid'>
  		<?php 
  		/*if(empty($cat_id)){
  		?>
  			<option value='0' selected="selected">Select Category</option>
  		<?php 
  		}
  		?>
  		<?php 
  		$catSql = "SELECT invcat_id, invcat_name FROM inv_category WHERE invcat_status='1' AND invcat_deleted='0'";
  		$cres = sqlStatement($catSql);
  		if(sqlNumRows($cres)){
  			while ($crow = sqlFetchArray($cres)) {
				?>
					<option value='<?php echo $crow['invcat_id']?>'<?php if($cat_id==$crow['invcat_id']){ echo 'selected="selected"';}?>><?php echo $crow['invcat_name']?></option>
				<?php
			}
		}*/
  		?>
  	</select>   
  </td>
 </tr -->
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Name'); ?>:</b></td>
  <td>
   <input type='text' size='40' name='form_name' maxlength='80' value='<?php echo attr($row['invsubcat_name']) ?>' style='width:75%' />
  </td>
 </tr>
<tr>
  <td valign='top' nowrap><b><?php echo xlt('Description'); ?>:</b></td>
  <td>
   <textarea name='form_desc' rows="5" style='width:75%' ><?php echo attr($row['invsubcat_desc']);?></textarea>
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Active'); ?>:</b></td>
  <td>
   <input type='checkbox' name='form_active' value='1'<?php if ($row['invsubcat_status']) echo ' checked'; ?> />
  </td>
 </tr>  
 <tr>
  <td colspan='2' align='center'>
<input type='submit' class="btn btn-warning btn-sm" name='form_save' onclick="return submitform();" value='<?php echo xla('Save'); ?>' />
&nbsp;
<input id="cancel" class="btn btn-warning btn-sm" type='button' value='<?php echo xla('Cancel'); ?>'  />
  </td>
 </tr>
</table>
</form>

<script language="JavaScript">
<?php
 if ($alertmsg) {
  echo "alert('" . htmlentities($alertmsg) . "');\n";
 }
?>
</script>

</body>
</html>
