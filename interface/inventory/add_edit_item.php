<?php
 // Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");

 $alertmsg = '';
 $item_id = $_REQUEST['itemid'];
 $info_msg = "";
 $tmpl_line_no = 0;

 if (!acl_check('inventory', 'im_item_add') || !acl_check('inventory', 'im_item_edit')) die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
  if ($amount) {
    $amount = sprintf("%.2f", $amount);
    if ($amount != 0.00) return $amount;
  }
  return '';
}
// Translation for form fields used in SQL queries.
//
function escapedff($name) {
  return add_escape_custom(trim($_POST[$name]));
}
function numericff($name) {
  $field = trim($_POST[$name]) + 0;
  return add_escape_custom($field);
}
?>
<html>
<head>
<?php html_header_show(); ?>
<title><?php echo $item_id ? xlt("Edit") : xlt("Add New"); echo ' ' . xlt('Item'); ?></title>
<link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>
<link rel='stylesheet' href="../themes/bootstrap.css" type="text/css">
<style>
td { font-size:10pt; }
</style>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="../../library/topdialog.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/textformat.js"></script>
<script type="text/javascript" src="../../library/commonScript.js"></script>

<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />


<script language="JavaScript">

<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

// This is for callback by the find-code popup.
// Appends to or erases the current list of related codes.
function set_related(codetype, code, selector, codedesc) {
 var f = document.forms[0];
 var s = f.form_related_code.value;
 if (code) {
  if (s.length > 0) s += ';';
  s += codetype + ':' + code;
 } else {
  s = '';
 }
 f.form_related_code.value = s;
}
</script>
<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
    
          if (document.getElementById('category_id').value<=0)
		{
			document.getElementById('category_id').style.backgroundColor="red";
			jAlert("<?php xl('Required field missing: Please choose the category.','e');?>");
			document.getElementById('category_id').focus();
			return false;
		}else if (document.forms[0].form_name.value.length<=0)
		{
			document.forms[0].form_name.style.backgroundColor="red";
			jAlert("<?php xl('Required field missing: Please enter the Item Name.','e');?>");
			document.forms[0].form_name.focus();
			return false;
		}else if (document.forms[0].form_code.value.length<=0)
		{
			document.forms[0].form_code.style.backgroundColor="red";
			jAlert("<?php xl('Required field missing: Please enter the Item Code.','e');?>");
			document.forms[0].form_code.focus();
			return false;
		}else if (document.forms[0].uomid.value<=0)
		{
			document.forms[0].uomid.style.backgroundColor="red";
			jAlert("<?php xl('Required field missing: Please choose the UOM.','e');?>");
			document.forms[0].uomid.focus();
			return false;
		}
                return true;
	
}
</script>
</head>

<body class="body_top">
<?php
// If we are saving, then save and close the window.
// First check for duplicates.
//
if ($_POST['form_save']) {
  
if(!isset($_POST['category']) || $_POST['category']['0']<=0){
    $alertmsg ="Category is Required"; 
}elseif(trim($_POST['form_name'])==""){
   $alertmsg ="Name is Required";  
}elseif(trim($_POST['form_code'])==""){
   $alertmsg ="Code is Required";  
}elseif(trim($_POST['uomid'])<=0){
   $alertmsg ="Unit Of Measurement is Required";  
}else{   
  
$crow = sqlQuery("SELECT COUNT(inv_im_id) AS count FROM inv_item_master WHERE " .    
	"inv_im_code = '"  . escapedff('form_code')  . "' AND inv_im_deleted = '0' AND " .   
    "inv_im_id != ?", array($item_id));
  if ($crow['count']) {
    $alertmsg = addslashes(xl('Cannot add this item because it already exists!'));
  }
}

  }

if (($_POST['form_save'] || $_POST['form_delete']) && !$alertmsg) {
    
  if ($item_id) {
   if ($_POST['form_save']) { // updating an existing drug
       $category=  implode(',', $_POST['category']);
    sqlStatement("UPDATE inv_item_master SET " .
     "inv_im_catId = '".$category. "', " .
     "inv_im_name = '".escapedff('form_name'). "', " .
     "inv_im_code = '".escapedff('form_code'). "', " .
     "inv_im_desc = '".escapedff('form_desc'). "', " .
     "inv_im_uomId = '".escapedff('uomid'). "', " .       
     "inv_im_status = '".(empty($_POST['form_active']) ? 0 : 1) . "', " .
     "inv_im_isExpiry = '".(empty($_POST['form_expiry']) ? 0 : 1) . "', " .
     "inv_im_sale = '".(empty($_POST['form_sale']) ? 0 : 1) . "', " .
     "inv_im_createdby = ".$_SESSION['authId'] . " " .
     "WHERE inv_im_id = ?", array($item_id)); 
      
      sqlStatement("DELETE FROM `invcategories_invitemmasters` WHERE `inv_im_id` = ?", array($item_id)); 
      
      foreach($_POST['category'] as $cat){
        $insertCat="INSERT INTO `invcategories_invitemmasters` SET `invcategory_id`='".$cat."',`inv_im_id`='".$item_id."'";
        sqlInsert($insertCat);
        
    }
      
   }
   else { // deleting
    if (acl_check('admin', 'super')) {
		if(checkUomStatus($item_id)){
     		sqlStatement("update inv_item_master set inv_im_deleted='1' WHERE inv_im_id = ?", array($item_id)); 
     	}    
    }
   }
  }
  else if ($_POST['form_save']) { // saving a new drug
    $category=  implode(',', $_POST['category']);
    $postArray=array('inv_im_name'=> addslashes(trim($_POST['form_name'])),'inv_im_code'=>addslashes(trim($_POST['form_code'])),'inv_im_catId'=>$category,'inv_im_desc'=>addslashes(trim($_POST['form_desc'])),'inv_im_isExpiry'=>empty($_POST['form_expiry']) ? 0 : 1,'inv_im_status'=>empty($_POST['form_active']) ? 0 : 1,'inv_im_sale'=>empty($_POST['form_sale']) ? 0 : 1,'inv_im_uomId'=>addslashes(trim($_POST['uomid'])),'inv_im_deleted'=>'0','inv_im_createdby'=>$_SESSION['authId']);
    
    $insertid = dbRowInsertId('inv_item_master',$postArray);
    if(!empty($insertid)){
    foreach($_POST['category'] as $cat){
        $insertCat="INSERT INTO `invcategories_invitemmasters` SET `invcategory_id`='".$cat."',`inv_im_id`='".$insertid."'";
            
        $insertid=sqlInsert($insertCat);
        
    }}
  } 

  echo "<script language='JavaScript'>\n";
  if ($info_msg) echo " alert('$info_msg');\n";
  echo "parent.location.reload();\n";  
  echo "</script></body></html>\n";
  exit();
}

if ($item_id) {
  $row = sqlQuery("SELECT inv_im_id, inv_im_name, inv_im_code, inv_im_catId, inv_im_desc, inv_im_isExpiry, inv_im_status, inv_im_sale, inv_im_uomId, inv_im_deleted, inv_im_createdby FROM inv_item_master WHERE inv_im_id = ?", array($item_id));
}else {
  $row = array(
    'inv_im_catId' => 0,
	'inv_im_name' => '',
	'inv_im_code' => '',
    'inv_im_desc' => '',
    'inv_im_status' => '1',
	'inv_im_sale' => '1',
	'inv_im_isExpiry' => '1',
	'inv_im_uomId' => 0    
  );
}
function getSlect($cat,$id){
    
    foreach($cat as $catid){
      if($catid==$id){
          return 1;
      }  
    }
    
    return 0;
    
}
?>

<form method='post' name='theform' action='add_edit_item.php?itemid=<?php echo $item_id; ?>'>
<table border='0' width='100%' class='emrtable'>
 <?php
 if ($alertmsg) {
 ?>    
 <tr>
     <td bgcolor="red" colspan="2" align="center"><?php echo $alertmsg;?></td>
 </tr>
<?php  
 }
?>   
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Category'); ?>:</b></td>
  <td>
  	<select name='category[]' id='category_id' class='formEle' multiple>
  		<?php 
  		if(empty($row['inv_im_catId'])){
  		?>
  			<option value='0' selected="selected">Select Category</option>
  		<?php 
                }else{
                ?>
                <option value='0'>Select Category</option>
                <?php   
                }
  		?>
  		<?php 
                $cat=explode(',',$row['inv_im_catId']);
                $query= "SELECT CONCAT( REPEAT(' -', COUNT(parent.name) - 1), node.name) AS name , node.`id` FROM `invcategories` AS node, `invcategories` AS parent WHERE node.lft BETWEEN parent.lft AND parent.rght GROUP BY node.id ORDER BY node.lft";
  		$cres = sqlStatement($query);
  		if(sqlNumRows($cres)){
  			while ($crow = sqlFetchArray($cres)) {
				?>
					<option value='<?php echo $crow['id']?>'<?php if(getSlect($cat,$crow['id'])){ echo 'selected="selected"';}?>><?php echo $crow['name']?></option>
				<?php
			}
		}
  		?>
  	</select>   
  </td>
 </tr>
 
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Name'); ?>:</b></td>
  <td>
   <input type='text' size='40' name='form_name' maxlength='80' value='<?php echo attr($row['inv_im_name']) ?>' class='formEle' />
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Code'); ?>:</b></td>
  <td>
   <input type='text' size='40' name='form_code' maxlength='80' value='<?php echo attr($row['inv_im_code']) ?>' class='formEle' />
  </td>
 </tr>
<tr>
  <td valign='top' nowrap><b><?php echo xlt('Description'); ?>:</b></td>
  <td>
   <textarea name='form_desc' rows="5" cols="34" class='formEle'><?php echo attr($row['inv_im_desc']);?></textarea>
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Unit Of Measurement'); ?>:</b></td>
  <td>
  	<select name='uomid' class='formEle'>
  		<?php 
  		if(empty($row['inv_im_uomId'])){
  		?>
  			<option value='0' selected="selected">Select UOM</option>
  		<?php 
  		}
  		?>
  		<?php 
  		$uomSql = "SELECT invuom_id, invuom_name FROM inv_uom WHERE invuom_status='1' AND invuom_deleted='0'";
  		$uomres = sqlStatement($uomSql);
  		if(sqlNumRows($uomres)){
  			while ($uomrow = sqlFetchArray($uomres)) {
				?>
					<option value='<?php echo $uomrow['invuom_id']?>'<?php if($row['inv_im_uomId']==$uomrow['invuom_id']){ echo 'selected="selected"';}?>><?php echo $uomrow['invuom_name']?></option>
				<?php
			}
		}
  		?>
  	</select>   
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Has Expiry'); ?>:</b></td>
  <td>
   <input type='checkbox' name='form_expiry' value='1'<?php if ($row['inv_im_isExpiry']) echo ' checked'; ?> />
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('For Sale'); ?>:</b></td>
  <td>
   <input type='checkbox' name='form_sale' value='1'<?php if ($row['inv_im_sale']) echo ' checked'; ?> />
  </td>
 </tr>
 <tr>
  <td valign='top' nowrap><b><?php echo xlt('Active'); ?>:</b></td>
  <td>
   <input type='checkbox' name='form_active' value='1'<?php if ($row['inv_im_status']) echo ' checked'; ?> />
  </td>
 </tr>  
<tr>
<td align='center' colspan='2'>
<input type='submit' class="btn btn-warning btn-sm" name='form_save' onclick="return submitform();" value='<?php echo xla('Save'); ?>' />
&nbsp;
<input id='cancel' class="btn btn-warning btn-sm" type='button' value='<?php echo xla('Cancel'); ?>' />
</td>
</tr>
</table>
</form>

<!--<script language="JavaScript">
<?php
 /*if ($alertmsg) {
  echo "alert('" . htmlentities($alertmsg) . "');\n";
 }*/
?>
</script>-->

</body>
</html>
<script>
	$( document ).ready(function() {						
		//alert('Hello Viren');
		$("#catid").change(function(){
			var catid = $("#catid").val();			
			$.ajax({
				type: 'GET',
				url:"get_item_subcat.php?catid="+catid,
				success:function(data){
					//alert('Hello Viren');
					$("#itemSubCatField").html(data);					
				}
			});			
		});		
	});
</script>