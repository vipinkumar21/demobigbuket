<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("mag_update_status.php");
require_once("$srcdir/classes/class.phpmailer.php");

$usergd = getUserGroup('value');
//$datePhpFormat = getDateDisplayFormat(0);

// Check authorization.
$thisauth = $invgacl->acl_check('inventory', 'invf_reqout_list','users',$_SESSION['authUser']);
if (!$thisauth)
    die(xlt('Not authorized'));
// For each sorting option, specify the ORDER BY argument.
$userGroupsdata = getUserGroup('value');
/*if (isset($_GET["mode"])) {

    if ($_GET["mode"] == "delete") {
        if (checkReqIssueStatus(urldecode($_GET["reqid"]))) {
            $deleteSql= $pdoobject->update("inv_stock_requisition",array("isr_isdeleted"=>"1"),array("isr_id"=>urldecode($_GET["reqid"])));
          
            $alertmsg = '<div class="alert alert-success alert-dismissable">Requisition deleted successfully!</div>';
            $_SESSION['INV_MESSAGE'] = $alertmsg;
            header('Location: requisitionFromClinic.php?facility=' . urldecode($_GET["facility"]));
            exit;
        } else {
            $alertmsg = '<div class="alert alert-info alert-dismissable">You should not able delete selected requisition. Because the selected requisition have proccessed!</div>';
            $_SESSION['INV_MESSAGE'] = $alertmsg;
            header('Location: requisitionFromClinic.php?facility=' . urldecode($_GET["facility"]));
            exit;
        }

    }
    if (urldecode($_GET["mode"]) == "approved") {
        $status = getStatus($userGroupsdata[0], 'approved');
        if (isset($_REQUEST['magorderid']) && $status == '1') {
            sendMail('35', 'WarehouseRequisitionAlert', $_REQUEST['facility'], $_REQUEST['reqnumber'], $_REQUEST['magorderid']);
        }
        $approved =$pdoobject->update("inv_stock_requisition",array("isr_isapproved"=>$status),array("isr_id"=>$_GET["reqid"]));
        $alertmsg = '<div class="alert alert-success alert-dismissable">Requisition approved successfully!</div>';
        $_SESSION['INV_MESSAGE'] = $alertmsg;
        
        header('Location: requisitionFromClinic.php?facility=' . $_GET["facility"]);
        exit;
    }
    if ($_GET["mode"] == "copy") {
        if ($_GET["facility"]) {
            $reqNo = getReqNumber(urldecode($_GET["facility"]));
            $getDataRes = "SELECT * FROM `inv_stock_requisition` where isr_id = '" . urldecode($_GET["reqid"]) . "'";
            $getDataExec = $pdoobject-> custom_query($getDataRes);
            $getData = $getDataExec[0];

            $isr_created_date = date('Y-m-d H:i:s');
            $insertData= array(
                'isr_number'=>$reqNo,
                'isr_from_clinic'=>urldecode($_GET["facility"]),
                'isr_to_clinic'=>$getData['isr_to_clinic'],
                'isr_isapproved'=>'1',
                'isr_createdby'=>$getData['isr_createdby'],
                'isr_created_date'=>$isr_created_date

            );
            $idInserted = $pdoobject->insert("inv_stock_requisition",$insertData);
            $getDataResRel = "SELECT * FROM `inv_requisition_item` where iri_reqid = '" . urldecode($_GET["reqid"]) . "'";
            $getDataRelation = $pdoobject->custom_query($getDataResRel);
            foreach ($getDataRelation as $getRelationData) {
                $relaionInsertData = array(
                    'iri_reqid'=>$idInserted,
                    'iri_itemid'=>$getRelationData['iri_itemid'],
                    'iri_quantity'=>$getRelationData['iri_quantity']

                );
                $relationInsertId = $pdoobject ->insert("inv_requisition_item",$relaionInsertData);
            }
            $alertmsg = '<div class="alert alert-success alert-dismissable">Requisition has been copied successfully!</div>';
            $_SESSION['INV_MESSAGE'] = $alertmsg;
        }
        header('Location: requisitionFromClinic.php?facility=' . urldecode($_GET["facility"]));
        exit;
    }
    if ($_GET["mode"] == "rejected") {
        $message = "";
        $status = getStatus($userGroupsdata[0], 'rejected');
        $newmeaasge[] = array('id' => $_SESSION['authId'], 'message' => $_REQUEST['message']);
        $rejectSql ="select message from inv_stock_requisition where isr_id = '" . $_GET["reqid"] . "'";
        $rs = $pdoobject->custom_query($rejectSql);
        if ($pdoobject->custom_query($rejectSql,null,1)) {
            foreach($rs as $row) {
                $message = unserialize($row['message']);
            }
        }
        if (!empty($message)) {
            $message = serialize(array_merge($message, $newmeaasge));
        } else {
            $message = serialize($newmeaasge);
        }
       $rejectUpdateSql =  "update inv_stock_requisition set isr_isapproved='" . $status . "', message='" . $message . "' where isr_id = '" . $_GET["reqid"] . "'";
       $rejectUpdateResult = $pdoobject->custom_query($rejectUpdateSql);
        $alertmsg = '<div class="alert alert-success alert-dismissable">Requisition rejected successfully.</div>';
        $_SESSION['INV_MESSAGE'] = $alertmsg;
       
        header('Location: requisitionFromClinic.php?facility=' . $_GET["facility"]);
        exit;
    }
}*/
$ORDERHASH = array(
    'invtran_id' => 'invtran_id DESC'
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[urldecode($_REQUEST['form_orderby'])] ? urldecode($_REQUEST['form_orderby']) : 'invtran_id';
$orderby = $ORDERHASH[$form_orderby];

if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
     
}else{
   $_REQUEST['facility']= $_SESSION['cid']; 
}

$facility = isset($_REQUEST['facility'])? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$from_date = urldecode($_REQUEST['form_from_date']);
$to_date = urldecode($_REQUEST['form_to_date']);
$transaction_id = urldecode($_REQUEST['form_transaction_id']);
$requistion_status = urldecode($_REQUEST['form_requistion_status']);
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Requisitions'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
            // function confirmation4(link) {
            //     jConfirm('Are you sure to Copy?', 'Confirmation', function (r) {
            //         if (r) {
            //             location.href = link;
            //         }
            //     });
            // }
            // function confirmation5(link) {
            //     jConfirm('Are you sure to Approve?', 'Confirmation', function (r) {
            //         if (r) {
            //             location.href = link;
            //         }
            //     });
            // }
            // function confirmation6(link) {
            //     jPrompt('Are you sure to Reject?', '', 'Confirmation With Reason', function (r) {
            //         if (r) {
            //             var message = encodeURI(r);
            //             location.href = link + '&message=' + message;
            //         } else {
            //             if (r != null) {

            //                 jAlert('Please enter text', 'Post Message', function () {
            //                     confirmation6(link);
            //                 });
            //             }

            //             return false;

            //         }
            //     });
            // }
            // function confirmation7(link) {
            //     jConfirm('Are you sure to Delete?', 'Confirmation', function (r) {
            //         if (r) {
            //             location.href = link;
            //         }
            //     });
            // }
        </script>
    </head>
    <body class="requisitionFromClinic">
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
<!--                            <div class="mrgnSpc_oi floatRight">
                                <?php //if($invgacl->acl_check('inventory', 'invf_reqout_add','users',$_SESSION['authUser'])){?>
                                <div class="addEditData primary-button btn-right ui-link" data-pageUrl="add_edit_requisition.php">
                                    <span class="icon-container">
                                            <span class="glyphicon glyphicon-plus icon"></span>
                                    </span>
                                    <b class="btn-text"><?php //xl('Add','e');   ?></b>
                                </div>
                                <?php //} ?>
                            </div>-->
                            <h1><?php xl('Requisitions', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='requisitionFromClinic.php' name='theform' id='theform' class="botnomrg">
                            <div class="filterWrapper">
                                <!-- first column starts -->
                                <div class="ui-block">
                                    <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('facility', '', 'facility', $facility, $_SESSION['authId'], $userFacilityRestrict,$pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends -->
                                <!-- fifth column starts -->
                                <div class="ui-block">
                                    <input type='text' placeholder='Requisition Id' name='form_transaction_id' id="form_transaction_id" value='<?php echo $transaction_id ?>' title='' />
                                </div>
                                <!-- fifth column ends -->
                                <!-- sevnth column starts -->
                                <div class="ui-block">
                                    <select class='form-control input-sm' name="form_requistion_status" id="form_requistion_status" >
                                        <option value="" <?php echo $selected = ( $requistion_status == "" ) ? 'selected="selected"' : '';?>>All Status</option>
                                        <option value="3" <?php echo $selected = ( $requistion_status == "3" ) ? 'selected="selected"' : '';?>>Cancelled</option>
                                        <option value="2" <?php echo $selected = ( $requistion_status == "2" ) ? 'selected="selected"' : '';?>>Completed</option>
                                        <option value="0" <?php echo $selected = ( $requistion_status == "0" ) ? 'selected="selected"' : '';?>>In Progress</option>
                                        <option value="1" <?php echo $selected = ( $requistion_status == "1" ) ? 'selected="selected"' : '';?>>Partially Completed</option>
                                    </select>
                                </div>
                                <!-- sevnth column ends -->
                                <!-- second column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                </div>
                                <!-- second column ends -->
                                <!-- third column starts -->
                                <div class='ui-block form_to_date_bx'>
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                </div>
                                <!-- third column ends -->
                                <!-- forth column starts -->
                                <div class="ui-block wdth15">
                                   
                                    <a class="pull-right btn_bx" id='reset_form1' href="requisitionFromClinic.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true");
                                            $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- forth column ends -->
                            </div>
                            <!-- leftPart -->
                            <!-- <table border='0' cellpadding='1' cellspacing='2' width='100%'>
                                <?php
                                //if (!empty($_SESSION['alertmsg'])) {
                                    ?>
                                    <tr >
                                        <td colspan="2"><?php //echo $_SESSION['alertmsg'];
                                //$_SESSION['alertmsg'] = "";
                                    ?></td>
                                    </tr>
                                    <?php
                               // }
                                ?>
                            </table> -->
                            <?php
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_from_date'] || $_REQUEST['form_to_date'] || $_REQUEST['form_requistion_status'] || $_REQUEST['form_transaction_id'] || empty($facility)) {
                                $res = "SELECT istreq.isr_id, istreq.isr_magento_orderid, istreq.isr_number, istreq.isr_from_clinic, istreq.isr_to_clinic, istreq.isr_isapproved, istreq.isr_status, istreq.isr_isdeleted, istreq.message, isr_createdby, istreq.isr_created_date, frf.name AS fromFacility, tof.name AS toFacility " .
                                        "FROM inv_stock_requisition AS istreq
                                        INNER JOIN facility AS frf ON frf.id = istreq.isr_from_clinic
                                        INNER JOIN facility AS tof ON tof.id = istreq.isr_to_clinic " ;
                                $where = '';
                                if (!empty($facility)) { // if facility exists
                                    $where.= (empty($where)) ? " WHERE " : "AND ";
                                    $where .= " istreq.isr_from_clinic = '" . $facility . "'";
                                } else {
                                    $where.= (empty($where)) ? " WHERE " : "AND ";
                                    $where .= " istreq.isr_from_clinic IN(" . getLoggedUserAssignedClinics() . ")";
                                }
                                if(empty($to_date) && !empty($from_date))
                                {
                                    $where.= (empty($where)) ? " WHERE " : " AND ";
                                    $where .= "istreq.isr_created_date >= '$from_date  00:00:00' ";
                                }
                                if (!empty($from_date) && !empty($to_date)) { // If from dates exists
                                    $where.= (empty($where)) ? " WHERE " : " AND ";
                                    $where .= " istreq.isr_created_date BETWEEN '$from_date 00:00:00' AND '$to_date  23:59:59'";
                                }
                                if (!empty($to_date) && empty($from_date)) { // If to dates exists
                                    $where.= (empty($where)) ? " WHERE " : "AND ";
                                    $where .= "istreq.isr_created_date <= '$to_date  23:59:59' ";
                                }
                                if ($requistion_status == "0" || !empty($requistion_status)) { // If to dates exists

                                    $res .= " AND istreq.isr_status = '" . $requistion_status . "'";
                                }
                                if (!empty($transaction_id)) { // If to dates exists

                                    $res .= " AND istreq.isr_number like'%" . $transaction_id . "%'";
                                }
                                $res .= " $where ORDER BY istreq.isr_id DESC";
                                //$sql_num = sqlStatement($res);
                                 $num_rows = $pdoobject->custom_query($res,null,1); // total no. of rows
                                $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                $page = $_GET["page"];
                                $page = ($page == 0 ? 1 : $page);
                                $page_start = ($page - 1) * $per_page;
                                $curr_URL = $_SERVER['SCRIPT_NAME'] . '?' . 'form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&facility=' . $facility . '&form_item=' . urldecode($_REQUEST[form_item]) . '&form_to_date=' . urldecode($_REQUEST[form_to_date]) . '&form_transaction_id=' . urldecode($_REQUEST['form_transaction_id']) .  '&form_requistion_status=' . urldecode($_REQUEST['form_requistion_status']) . '&';

                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                $res .= " LIMIT $page_start , $per_page";
                                $result = $pdoobject->custom_query($res);
                                ?>
                                <div id='' class='tableWrp pb-2'>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                    <div class="dataTables_wrapper no-footer">
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                <?php if ($num_rows > 0) { ?>
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <?php echo xlt('Requisition#'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('From Facility'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('To Facility'); ?>
                                                            </th>
                                                            <!-- <th>
                                                                <?php //echo xlt('Appr. Status'); ?>
                                                            </th> -->
                                                            <th>
                                                                <?php echo xlt('Req. Status'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Req. Date'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Receipt Date'); ?>
                                                            </th>
                                                            <th width="15%">
                                                                <?php echo xlt('Action'); ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                <?php } ?>
                                                <?php
                                                $lastid = "";
                                                $encount = 0;
                                                if (!empty($result)) {
                                                    foreach ($result as $key => $row) {
                                                        ++$encount;
                                                        $bgcolor = "#" . (($encount & 1) ? "f7d4be" : "f1f1f1");
                                                        $lastid = $row['isr_id'];
                                                        $rrdate = getReqReDate($lastid);
                                                        $magorderid = $row['isr_magento_orderid'];
                                                        $reqnumber = $row['isr_number'];

                                                        if ($row['isr_isdeleted'] == 1) {
                                                            echo "<tr class='detail strikeThrough'>\n";
                                                        } else {
                                                            echo "<tr class='detail' role='row'>\n";
                                                        }
                                                        echo "  <td>" .
                                                        text($row['isr_number']) . "</td>\n";
                                                        echo "  <td>" . text($row['fromFacility']) . "</td>\n";
                                                        echo "  <td>" . text($row['toFacility']) . "</td>\n";
                                                        //echo "  <td>";
                                                        //echo getStatus($userGroupsdata[0], 'list', $row['isr_isapproved']);
                                                        //echo "</td>\n";
                                                        echo "  <td>";
                                                        if ($row['isr_status'] == 2) {
                                                            echo 'Completed';
                                                        } else if ($row['isr_status'] == 1) {
                                                            echo 'Partially Completed';
                                                        } else if ($row['isr_status'] == 3) {
                                                            echo 'Cancelled';
                                                        } else {
                                                            echo 'In Progress';
                                                        }
                                                        echo "</td>\n";
                                                        echo "  <td>" . invDateFormat($row['isr_created_date']) . "</td>\n";

                                                        echo "  <td>";
                                                        if ($rrdate)
                                                            echo invDateFormat($rrdate);
                                                        else
                                                            echo "---";
                                                        echo "</td>\n";

                                                        echo "  <td>";
                                                        if ($invgacl->acl_check('inventory', 'invf_reqout_view','users',$_SESSION['authUser'])) {
                                                            echo "<div class='listbtn viewTableDataList' data-pageUrl='view_requisition_out.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_from_clinic']) . "'><span class='dashboardViewIcon'></span>View</div> &nbsp;";
                                                            //if($row['isr_isdeleted'] != 1){
                                                            	 //echo "<div class='listbtn dialg' id='copy".attr($lastid)."' data-title='Do you want to copy the requisition(s)?' data-action='requisitionFromClinic.php?mode=copy&facility=".$facility."&reqid=".attr($lastid)."'><span class='dashboardCopyIcon'></span>Copy</div> &nbsp; ";
                                                            //}
                                                        }
                                                        
                                                         if($invfac->validateFacilityId($row['isr_from_clinic'])){
                                                         if ($row['isr_status'] == '2' || $row['isr_status'] == '1') {
                                                            if ($invgacl->acl_check('inventory', 'invf_issuein_list','users',$_SESSION['authUser'])) {
                                                                echo "<a style='width: 38px;' class='listbtn' data-ajax='false' href='issueNotesToClinic.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_from_clinic']) . "&reqnumber=" . $reqnumber . "&magorderid=" . $magorderid. "'><span class='dashboardRecieptIcon1'></span>Receipt</a> &nbsp; ";
                                                            }
                                                        } else if ($row['isr_status'] == 0  /*&& $usergd['0'] == 'admin'*/) {
                                                            if($invgacl->acl_check('inventory', 'invf_reqout_del','users',$_SESSION['authUser'])){
                                                            echo "<div style='width: 38px;' class='listbtn addEditTableData' data-pageUrl='req_item_cancel.php?reqid=" . attr($lastid) . "&facility=" . attr($row['isr_from_clinic']) . "'><span class='dashboardDeleteIcon'></span>Cancel</div> &nbsp; ";
                                                            }
                                                           }
                                                         }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        /*if(($row['isr_isapproved'] == 0  && (in_array('admin', $userGroupsdata) || in_array('clin', $userGroupsdata))) && ($row['isr_isdeleted'] != 1)){
                                                        	if($invgacl->acl_check('inventory', 'invf_reqout_approve','users',$_SESSION['authUser'])){
                                                        	//echo "<a class='iframe iconanchor' href='requisitionAction.php?facility=".$facility."&reqid=".attr($lastid)."&reqnumber=".$reqnumber."&magorderid=".$magorderid."' title='Approved/Reject'><span class='glyphicon glyphicon-cog'></span></a> &nbsp; ";
                                                            //echo '<div style="width:auto;" class="addEditData listbtn" data-pageurl="requisitionAction.php?facility=".$facility."&reqid=".attr($lastid)."&reqnumber=".$reqnumber."&magorderid=".$magorderid.""><span class="dashboardApproveIcon"></span>Appr/Rej</div>';
                                                        	}
                                                        }

                                                        if (($row['isr_isapproved'] == 0) && ($row['isr_isdeleted'] != 1) && ($userGroupsdata[0] == 'admin' || $userGroupsdata[0] == 'clin')) {
                                                            if($invgacl->acl_check('inventory', 'invf_reqout_edit','users',$_SESSION['authUser'])){
                                                            	//echo "<a class='iconanchor iframe' href='add_edit_requisition.php?reqid=".attr($lastid)."' class='iframe' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a> &nbsp; ";
                                                                //echo "<div class='listbtn addEditData' data-pageUrl='add_edit_requisition.php?reqid=".attr($lastid)."'><span class='dashboardEditIcon'></span>Edit</div> &nbsp; ";
                                                            }
                                                              	if($row['isr_isdeleted'] == 0)	{
                                                            	if($invgacl->acl_check('inventory', 'invf_reqout_del','users',$_SESSION['authUser'])){
                                                            		//echo "<div class='listbtn dialg' id='delete".attr($lastid)."' data-title='Do you want to delete the requisition(s)?' data-action='requisitionFromClinic.php?mode=delete&facility=".$facility."&reqid=".attr($lastid)."'><span class='dashboardDeleteIcon'></span>Delete</div> &nbsp; ";
                                                            	}
                                                              	}
                                                        } elseif (($row['isr_isapproved'] == 2) && ($row['isr_isdeleted'] != 1) && ($userGroupsdata[0] == 'admin' || $userGroupsdata[0] == 'Dentist')) {
                                                            if($invgacl->acl_check('inventory', 'invf_reqout_edit','users',$_SESSION['authUser'])){
                                                            	//echo "<a class='iconanchor iframe' href='add_edit_requisition.php?reqid=".attr($lastid)."' class='iframe' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a> &nbsp; ";
                                                                echo "<div class='listbtn addEditData' data-pageUrl='add_edit_requisition.php?reqid=".attr($lastid)."'><span class='dashboardEditIcon'></span>Edit</div> &nbsp; ";
                                                            }
                                                        }*/
                                                        echo "</td>\n";
                                                        echo " </tr>\n";
                                                    } // end while
                                                } else {
                                                    ?>
                                                    <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                <?php } ?>
                                            </table>
                                    </div>
                                    <!-- pagination -->
                                    <?php if ($num_rows > 0) { ?>
                                        <?php echo $pa_data; ?>
                                    <?php } ?>
                                    <!-- pagination -->
                                </div>
                                <?php } else { ?>
                                <div class='text leftmrg6 dnone'>
                                <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                                </div>
                                <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                        </form>
                        <!-- mdleCont -->
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
        <!-- page -->
    </body>
</html>
