<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
//$thisauth = acl_check('inventory', 'invf_issueout_list');

if (!$invgacl->acl_check('inventory', 'invf_issueout_list','users', $_SESSION['authUser']))
    die(xlt('Not authorized'));
// For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
    'invtran_id' => 'invtran_id DESC'
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invtran_id';
$orderby = $ORDERHASH[$form_orderby];
//$facility  = $_POST['facility'];
$facility = $_REQUEST['facility'];
$reqid = $_REQUEST['reqid'];


// get drugs
/* echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " .
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby"; */
?>
<html>

    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Issue Notes Out'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>
        <!-- for oldversion -->
        <script language="JavaScript">
            // callback from add_edit_drug.php or add_edit_drug_inventory.php:
            function refreshme() {
                location.reload();
            }

            // Process click on drug title.
            function dodclick() {
                dlgopen('add_edit_transfer.php', '_blank', 460, 375);
            }
            function doViewclick(id, facility) {
                dlgopen('view_issue_out.php?issueid=' + id + '&facility=' + facility, '_blank', 860, 375);
            }
            /*function doReturnClick(id, facility) {
             dlgopen('return_transfer.php?tranid=' + id+ '&facility=' + facility, '_blank', 460, 375);
             }*/
            function doIssueClick(id, facility) {
                dlgopen('accept_issued_items.php?issueid=' + id + '&facility=' + facility, '_blank', 860, 375);
            }
            // Process click on a column header for sorting.
            function dosort(orderby) {
                var f = document.forms[0];
                f.form_orderby.value = orderby;
                top.restoreSession();
                f.submit();
                return false;
            }
        </script>
        <!-- for oldversion -->
    </head>
    <body class="body_top">
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
            <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <div class="mrgnSpc mrgnSpc_oi floatRight">
                                <?php if ($invgacl->acl_check('inventory', 'invf_stock_add','users', $_SESSION['authUser'])) { ?>
                                    <a href="requisitionToClinic.php?facility=<?php echo attr($facility); ?>" data-ajax="false" class="primary-button btn-right ui-link">
                                        <span class="icon-container">
                                            <span class="icon dummyIcon ship"></span>
                                        </span>
                                        <b class="btn-text"><?php xl('Shipment', 'e'); ?></b>
                                    </a>
                                <?php } ?>
                            </div>
                            <h1><?php xl('Issued Against Requisition', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <div class="">
                            <table border='0' cellpadding='0' cellspacing='0' width='100%' class='emrtable'>
                                <?php
                                if (!empty($_SESSION['alertmsg'])) {
                                    ?>
                                    <tr >
                                        <td><?php
                                            echo $_SESSION['alertmsg'];
                                            $_SESSION['alertmsg'] = "";
                                            ?></td>		
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            <!-- DataTable -->
                            <form method='post' action='issueNotesFromClinic.php'  name='theform' id='theform'>
                                <?php
                                if ($_REQUEST['facility'] || $_REQUEST['reqid']) {
                                    $res = "SELECT iisn.iin_id, iisn.iin_reqid, istreq.isr_number, iisn.iin_from_clinic, iisn.iin_to_clinic, iisn.iin_number, iisn.iin_status, iisn.iin_isdeleted, iisn.iin_createdby, iisn.iin_date, frf.name AS fromFacility, tof.name AS toFacility, users.nickname as name " .
                                            "FROM inv_issue_notes AS iisn INNER JOIN inv_stock_requisition AS istreq ON istreq.isr_id = iisn.iin_reqid INNER JOIN users ON users.id =iisn.iin_createdby
									INNER JOIN facility AS frf ON frf.id = iisn.iin_from_clinic
									INNER JOIN facility AS tof ON tof.id = iisn.iin_to_clinic " .
                                            "WHERE iisn.iin_isdeleted = '0'";

                                    if (!empty($facility)) { // if facility exists
                                        $res .= " AND iisn.iin_from_clinic = '" . $facility . "'";
                                    }
                                    if (!empty($reqid)) { // If from dates exists    
                                        $res .= " AND iisn.iin_reqid = '$reqid'";
                                    }


                                    $res .= " ORDER BY iisn.iin_id DESC";
                                    $num_rows = $pdoobject->custom_query($res,null,1);  // total no. of rows
                                    $per_page = $GLOBALS['encounter_page_size'];
                                    $page = $_GET["page"];
                                    $curr_URL = $_SERVER['SCRIPT_NAME'] . "?facility=$facility&reqid=$reqid&";
                                    $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                    ?>
                                    <div id='' class='tableWrp pb-2'>
                                        <!-- pagination -->
                                        <?php
                                        if ($num_rows > 0) {
                                            echo $pa_data;
                                        }
                                        ?>
                                        <!-- pagination -->
                                        <div class='dataTables_wrapper no-footer'>
                                            <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                <?php if ($num_rows > 0) { ?>
                                                    <thead>
                                                        <tr>
                                                            <th width="15%">   
                                                                <?php echo xlt('Issue#'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('From Facility'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('To Facility'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Requisition#'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Issue Status'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Issue By'); ?>
                                                            </th> 
                                                            <th>
                                                                <?php echo xlt('Issue Date'); ?>
                                                            </th>  
                                                            <th width="6%" class="txtCenter">
                                                                <?php echo xlt('Action'); ?>
                                                            </th> 
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                }
                                                $pageCount = ($page == 0 ? 1 : $page);
                                                $page_start = ($pageCount - 1) * $per_page;
                                                $res .= " LIMIT $page_start , $per_page";
                                                $result = $pdoobject->custom_query($res);
                                                $lastid = "";
                                                $encount = 0;
                                                if ($pdoobject->custom_query($res)) {
                                                    foreach ($result as $key => $row) {
                                                        ++$encount;
                                                        $bgcolor = "#" . (($encount & 1) ? "f7d4be" : "f1f1f1");
                                                        $lastid = $row['iin_id'];
                                                        if ($row['iin_isdeleted'] == 1) {
                                                            echo "<tr class='detail strikeThrough'>\n";
                                                        } else {
                                                            echo "<tr class='detail' role='row'>\n";
                                                        }
                                                        //echo "  <td>" .$encount. "</td>\n";
                                                        echo "  <td>" .
                                                        text($row['iin_number']) . "</td>\n";
                                                        echo "  <td>" . text($row['toFacility']) . "</td>\n";
                                                        echo "  <td>" . text($row['fromFacility']) . "</td>\n";
                                                        echo "  <td>" .
                                                        text($row['isr_number']) . "</td>\n";
                                                        echo "  <td>";
                                                        if ($row['iin_status'] == 1) {
                                                            echo 'Completed';
                                                        } else {
                                                            echo 'In Progress';
                                                        }
                                                        echo "</td>\n";
                                                         echo "  <td>" .
                                                        text($row['name']) . "</td>\n";
                                                        echo "  <td>" . text(date($datePhpFormat, strtotime($row['iin_date']))) . "</td>\n";
                                                        echo "  <td>";
                                                        if ($invgacl->acl_check('inventory', 'invf_reqout_view','users', $_SESSION['authUser'])) {
                                                            echo "<div data-pageUrl='view_issue_out.php?issueid=" . attr($lastid) . "&facility=" . attr($facility) . "' class='viewTableDataList listBtn'><span class='dashboardViewIcon'></span>View</div> &nbsp; ";
                                                        }
                                                        /* if($row['iin_status'] != 1){
                                                          echo "<a href='javascript:void(0);' onclick='doIssueClick(".attr($lastid).", ".attr($facility).")'>Receipt</a> &nbsp; ";
                                                          //echo "<a href='javascript:void(0);' onclick='doReturnClick(".attr($lastid).", ".attr($facility).")'>Return</a> &nbsp; ";
                                                          } */

                                                        echo "</td>\n";
                                                        echo " </tr></tbody>\n";
                                                    } // end while
                                                } else {
                                                    ?>
                                                    <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                <?php } // end If
                                                ?>
                                            </table>
                                        </div>
                                        <!-- pagination -->
                                        <?php
                                        if ($num_rows > 0) {
                                            echo $pa_data;
                                        }
                                        ?>
                                        <!-- pagination -->
                                    </div>
                                <?php } else { ?>
                                    <div class='text dnone'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                                    </div>
                                <?php } ?>
                                <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                                <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            </form>
                            <!-- DataTable -->

                        </div>
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->
        </div>
        <!-- page -->
    </body>
</html>