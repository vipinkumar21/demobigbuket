<?php
$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("drugs.inc.php");
$issueId = $_REQUEST['issueid'];
$facility = $_REQUEST['facility'];
?>
<html>
    <head>
        <title>View Consume/Return</title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body class="body_top">
        <!-- page -->
        <div class="infopop"><?php xl('View Consume/Return', 'e'); ?></div>
        <div class="popupTableWrp mt-0">
            <!-- tableData -->
            <table id='issue_return_list' cellspacing='0' class="popupTable ui-table">
                <thead>
                    <tr>
                        <th width='40%'>Item Name</th>
                        <th width='15%'>Issued Qty.</th>
                        <th width='15%'>Returned</th>
                        <th width='15%'>Consumed</th>
                        <th width='15%'>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql = "SELECT im.inv_im_name,
                            SUM(CASE invistr_tran_type WHEN 7 THEN invistr_quantity ELSE 0 END) AS returnqty,
                            SUM(CASE invistr_tran_type WHEN 8 THEN invistr_quantity ELSE 0 END) AS consqty,
                            invir.invir_quantity ,invistr.invistr_id, invistr_tran_type,
                            invistr_created_date
                            FROM `inv_item_stock_transaction` AS invistr
                            INNER JOIN `inv_item_master` AS im ON im.inv_im_id = invistr_itemid
                            INNER JOIN `inv_issue_return` AS invir ON invir.invir_id = invistr.invistr_action_id
                            WHERE invistr.invistr_action = 'inventory/issue_return'";
                    if (!empty($issueId) && !empty($facility)) {
                        $sql.="AND invistr.invistr_action_id ='$issueId'
                                        AND invistr.invistr_clinic_id ='$facility'";
                    }
                    $sql.= "GROUP BY invistr_created_date";
                    $res = $pdoobject->custom_query($sql);
                    if (!empty($res)) {
                        $count = 1;
                        foreach ($res as $row) {
                            $transType = $row['invistr_tran_type'];
                            if ($count == 1) {
                                ?>
                                <tr>	
                                    <td><?php echo $row['inv_im_name']; ?></td>
                                    <td><?php echo $row['invir_quantity']; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>	
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $row['returnqty']; ?></td>
                                    <td><?php echo $row['consqty']; ?></td>
                                    <td><?php echo invDateFormat($row['invistr_created_date']); ?></td>
                                </tr>
                            <?php } else {
                                ?>
                                <tr>	
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $row['returnqty']; ?></td>
                                    <td><?php echo $row['consqty']; ?></td>
                                    <td><?php echo invDateFormat($row['invistr_created_date']); ?></td>
                                </tr>
                            <?php
                            }
                            $count++;
                        }
                    }
                    else {?>
                        <tr>	
                            <td colspan="5" style="text-align: center">No Result Found</td>
                        </tr>
                    <?php }
                    ?>


                </tbody>
            </table>
            <!-- tableData -->
        </div>
        <!-- page -->
    </body>
</html>