<?php
// $Id: gacl.class.php 422 2006-09-03 22:52:20Z ipso $

/**
 * phpGACL - Generic Access Control List
 * Copyright (C) 2002,2003 Mike Benoit
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For questions, help, comments, discussion, etc., please join the
 * phpGACL mailing list. http://sourceforge.net/mail/?group_id=57103
 *
 * You may contact the author of phpGACL by e-mail at:
 * ipso@snappymail.ca
 *
 * The latest version of phpGACL can be obtained from:
 * http://phpgacl.sourceforge.net/
 *
 * @package phpGACL
 */

/*
 * Path to ADODB.
 */

/**
* phpGACL main class
*
* Class gacl should be used in applications where only querying the phpGACL
* database is required.
*
* @package phpGACL
* @author Mike Benoit <ipso@snappymail.ca>
*/
class invusers {
    
        /** @var object An PDO database connector object */
	var $_db = '';
        var $_facality = '';
        
	function __construct($pdoobject) {
            
                
                $this->db=$pdoobject;
                
		
		return true;
	}
        
        function getFacility($uid){
            
               $query="SELECT `InvUserFacility`.`id`, `InvUserFacility`.`name`,`InvUserFacility`.`type` FROM `facility` AS `InvUserFacility` JOIN `inventory_users` AS `InventoryUser` ON( `InventoryUser`.`user_id` = '".$uid."' AND `InventoryUser`.`facility_id` = `InvUserFacility`.`id`)";
               $row = $this->db->custom_query($query);
               if(empty($row)){
                 return 0;  
               }else{
               return $row;
               }
               
               //print_r($row);
               
        }
        
        
        function getFacilityType($fid){
            
               $query="SELECT `InvUserFacility`.`type` FROM `facility` AS `InvUserFacility` WHERE `InvUserFacility`.`id` = '".$fid."'";
               $row = $this->db->custom_query($query,null,'','fetch');
               if(empty($row)){
                 return 0;  
               }else{
               return $row;
               }
               
               //print_r($row);
               
        }
        
        
        function getFacilityId($uid){
            
               $query="SELECT `InvUserFacility`.`id` FROM `facility` AS `InvUserFacility` JOIN `inventory_users` AS `InventoryUser` ON( `InventoryUser`.`user_id` = '".$uid."' AND `InventoryUser`.`facility_id` = `InvUserFacility`.`id`)";
               $rows = $this->db->custom_query($query);
               if(empty($rows)){
                 return array();  
               }else{
                    foreach($rows as $row){
                        $rowId[]=$row['id'];
                    }
               return $rowId;
               //print_r($row);
               
        }
  	
}

function validateFacilityId($fid){
            
          $rtn=trim(array_search($fid,$_SESSION['InvFacilityId']));  
          
          if($rtn==""){
            return FALSE;  
          }else{
            return TRUE;    
          }
          
          
        }

                    }

$invfac=new invusers($pdoobject);
if(empty($_SESSION['InvFacilityId'])){
    $_SESSION['InvFacilityId']=$invfac->getFacilityId($_SESSION['Auth']['User']['id']);    

}


//print_r( $_SESSION['InvFacilityId']);
?>
