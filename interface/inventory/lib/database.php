<?php
/**
 * PDO mysql database helper class
 * 
 * @author Pawan Bhat <pawan.bhat@instantsys.com>
 * @copyright May 2016
 */

$pos = strpos($_SERVER['HTTP_REFERER'], "/webapp/");
if($pos){
    setcookie("server", "web");   
}
require_once '../../../api/lib/connection.php'; 

class database extends connection {
  
    public $pdo;
    
    public function __construct($type="write")
    {
	$this->pdo=parent::getConnection($type);
    }

    /**
    * custom query ,update,delete,insert,or fetch, joining multiple table etc, aritmathic etc
    * @param  string $sql  custom query
    * @param  array $data associative array 
    * @param int $count 1 for the result to be num of rows and 0 or null if needed full result set
    * @return array  recordset 
    */
    public function custom_query( $sql,$data=null,$count='',$fetch="fetchAll") {
        if ($data!==null) {
        $dat=array_values($data);
        }
        $sel = $this->pdo->prepare( $sql );
        if ($data!==null) {
            $sel->execute($dat);
        } else {
            $sel->execute();
        }
        if($count == 1)
        {
            $result = $sel->rowCount();
        }
        else {
            $result = $sel->$fetch(PDO::FETCH_ASSOC);
        }
        return $result;
        //$sel->setFetchMode( PDO::FETCH_OBJ );
        //return $sel;
    }


    /**
     * begin a transaction.
     */
    public function begin_transaction()
    {
        $this->pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
        $this->pdo->beginTransaction();
    }

    /**
     * commit the transaction.
     */
    public function commit()
    {
        $this->pdo->commit();
        $this->pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    }

    /**
     * rollback the transaction.
     */
    public function rollback()
    {
        $this->pdo->rollBack();
        $this->pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    }



    /**
    * fetch only one row 
    * @param  string $table table name
    * @param  string $col condition column
    * @param  string $val value column
    * @return array recordset
    */
    public function fetch_single_row($table,$col,$val)     
    {       
        $nilai=array($val);
        $sel = $this->pdo->prepare("SELECT * FROM $table WHERE $col=?");
        $sel->execute($nilai);
        $sel->setFetchMode( PDO::FETCH_OBJ );
        $obj = $sel->fetch();
        return $obj;
    }

    /**
    * fetch all data 
    * @param  string $table table name
    * @return array recordset
    */
    public function fetch_all($table)
    {
        $sel = $this->pdo->prepare("SELECT * FROM $table");
        $sel->execute();
        $result = $sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        //$sel->setFetchMode(PDO::FETCH_ASSOC );
        //$sel->fetch();
        //return $sel;
    }
    /**
    * fetch multiple row
    * @param  string $table table name
    * @param  array $dat specific column selection
    * @return array recordset
    */
    public function fetch_col($table,$dat)
    {
        if( $dat !== null )
        $cols= array_values( $dat );
        $col=implode(', ', $cols);
        $sel = $this->pdo->prepare("SELECT $col from $table");
        $sel->execute();
        $sel->setFetchMode( PDO::FETCH_OBJ );
        return $sel;
    }

    /**
    * fetch row with condition
    * @param  string $table table name
    * @param  array $col which columns name would be select 
    * @param  array $where what column will be the condition
    * @return array recordset
    */
    public function fetch_multi_row($table,$col,$where,$string="")
    {

        $data = array_values( $where ); 
        //grab keys 
        $cols=array_keys($where);
        $colum=implode(', ', $col);
        foreach ($cols as $key) {
          $keys=$key."=?";
          $mark[]=$keys;
        }
      $jum=count($where);
        if ($jum>1) {
            $im=implode(' and ', $mark);
            //echo "SELECT $colum from $table WHERE $im ".$string;
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im ".$string);
        } else {
          
          $im=implode('', $mark);
          $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        }
        
        $sel->execute( $data );
        //$sel->setFetchMode( PDO::FETCH_OBJ );
        //return  $sel;
        $result = $sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
    * check if there is exist data
    * @param  string $table table name 
    * @param  array $dat array list of data to find
    * @return true or false
    */
    public function check_exist($table,$dat) {

        $data = array_values( $dat ); 
       //grab keys 
        $cols=array_keys($dat);
        $col=implode(', ', $cols);

        foreach ($cols as $key) {
          $keys=$key."=?";
          $mark[]=$keys;
        }

        $jum=count($dat);
        if ($jum>1) {
            $im=implode(' and  ', $mark);
             $sel = $this->pdo->prepare("SELECT $col from $table WHERE $im");
        } else {
          $im=implode('', $mark);
             $sel = $this->pdo->prepare("SELECT $col from $table WHERE $im");
        }
        $sel->execute( $data );
        $sel->setFetchMode( PDO::FETCH_OBJ );
        $jum=$sel->rowCount();
        if ($jum>0) {
            return true;
        } else {
            return false;
        }     
    }
    /**
    * search data
    * @param  string $table table name
    * @param  array $col   column name
    * @param  array $where where condition 
    * @return array recordset
    */
    public function search($table,$col,$where) {
        $data = array_values( $where );
        foreach ($data as $key) {
           $val = '%'.$key.'%';
           $value[]=$val;
        }
       //grab keys 
        $cols=array_keys($where);
        $colum=implode(', ', $col);

        foreach ($cols as $key) {
          $keys=$key." LIKE ?";
          $mark[]=$keys;
        }
        $jum=count($where);
        if ($jum>1) {
            $im=implode(' OR  ', $mark);
             $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        } else {
          $im=implode('', $mark);
             $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        }
           
        $sel->execute($value);
        $sel->setFetchMode( PDO::FETCH_OBJ );
        return  $sel;
    }
    /**
    * insert data to table
    * @param  string $table table name
    * @param  array $dat   associative array 'column_name'=>'val'
    */
    public function insert($table,$dat) {
        
        if( $dat !== null )
        $data = array_values( $dat );
        //grab keys 
        $cols=array_keys($dat);
        $col=implode(', ', $cols);

        //grab values and change it value
        $mark=array();
        foreach ($data as $key) {
          $keys='?';
          $mark[]=$keys;
        }
        $im=implode(', ', $mark);
        //echo "INSERT INTO $table ($col) values ($im)";die();
        $ins = $this->pdo->prepare("INSERT INTO $table ($col) values ($im)");
        $ins->execute( $data );
        return $this->pdo->lastInsertId();
        


    }

    /**
    * update record
    * @param  string $table table name
    * @param  array $dat   associative array 'col'=>'val'
    * @param  string $id    primary key column name
    * @param  int $val   key value
    */
    public function update($table,$dat,$where) {
        
        if( $dat !== null )
        $data = array_values( $dat ); 
        
        $cols=array_keys($dat);
        $mark=array();
        foreach ($cols as $col) {
          $mark[]=$col."=?"; 
        }
        $im=implode(', ', $mark);
        
        $dl="";
        $sql="";
        foreach($where as $filterKey=>$filterValue){ 
             $sql=$sql.$dl.$filterKey." = '".$filterValue."'";
             $dl=" AND ";
        }
        $ins = $this->pdo->prepare("UPDATE $table SET $im where $sql");
        return $ins->execute( $data );
        
    }
    
    
    public function updateold($table,$dat,$id,$val) {
        if( $dat !== null )
        $data = array_values( $dat ); 
        array_push($data,$val);
        
        //grab keys
        $cols=array_keys($dat);
        $mark=array();
        foreach ($cols as $col) {
        $mark[]=$col."=?"; 
        }
        $im=implode(', ', $mark);
        
        $ins = $this->pdo->prepare("UPDATE $table SET $im where $id=?");
        var_dump($ins->execute( $data ));
        
    }

    /**
    * delete record
    * @param  string $table table name
    * @param  string $where column name for condition (commonly primay key column name)
    * @param   int $id   key value
    */
    public function delete( $table, $where,$id ) { 
        $data = array( $id ); 
        $sel = $this->pdo->prepare("Delete from $table where $where=?" );
        $sel->execute( $data );
    }
    public function custom_query_execute( $sql,$data=null) {
        if ($data!==null) {
        $dat=array_values($data);
        }
        $sel = $this->pdo->prepare( $sql );
        if ($data!==null) {
           return $sel->execute($dat);
        } else {
           return $sel->execute();
        }
    }
   
    public function __destruct() {
    $this->pdo = null;
    }
}
//echo "<pre>";
$reportList=array('stock_leadger_report.php','batch_expiry_report.php','adjustment_report.php');
$page=end(explode('/',$_SERVER['REQUEST_URI']));
if (in_array($page, $reportList)) {
    $pdoobject=new database('report');
}else{
    $pdoobject=new database();  
}



//function handleException( $exception ) {
//  echo  $exception->getMessage();
//}
//
//set_exception_handler( 'handleException' );


/*$p=$pdoobject->fetch_all('inv_item_stock_transaction');

print_r($p);die();
foreach ($p as $key) {
	print_r($key);
}


die();*/



?>
