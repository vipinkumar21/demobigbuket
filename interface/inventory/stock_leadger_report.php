<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Fetch the Provide based on facility (Clinic ID)
/*
  function restrict_user_facility($facilList) {
  $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename FROM users_facility AS uf LEFT JOIN users AS u ON u.id = uf.table_id ";
  $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
  $sql .= " WHERE ";
  if (isset($facilList) && $facilList !== '') {
  $sql .= "uf.facility_id = $facilList ";
  } else {
  $fsql = "SELECT us.id, us.username, us.fname, us.lname, uf.facility_id, f.name
  FROM users_facility AS uf
  LEFT JOIN users AS us ON uf.table_id = us.id
  LEFT JOIN facility AS f ON uf.facility_id = f.id
  WHERE uf.table_id = " . $userid . " AND us.authorized !=0
  AND us.active = 1 ORDER BY id ";
  $fRes = sqlStatement($fsql, null, $GLOBALS['adodb']['dbreport']);
  $facilityIds = '';
  while ($fRow = sqlFetchArray($fRes)) {
  if (!empty($facilityIds)) {
  $facilityIds .= ', ' . $fRow['facility_id'];
  } else {
  $facilityIds = $fRow['facility_id'];
  }
  }
  $sql .= "uf.facility_id IN (" . $facilityIds . ") ";
  }
  $sql .= "AND gag.id IN (12, 13, 18) ";
  return $sql .= " GROUP BY u.id DESC";
  //echo $sql;
  //$ures = sqlStatement($sql);
  }

  function non_restrict_user_facility($facilList) {
  $sql = "SELECT u.id, CONCAT_WS(' ', u.fname, u.lname) as completename ";
  $sql .= "FROM users AS u  ";
  $sql .= "INNER JOIN gacl_aro AS ga ON ga.value=u.username INNER JOIN gacl_groups_aro_map AS ggam ON ggam.aro_id = ga.id INNER JOIN gacl_aro_groups AS gag ON gag.id = ggam.group_id ";
  return $sql .= "WHERE gag.id IN (12, 13, 18) GROUP BY u.id DESC";
  }
 */
// Check authorization.

$thisauth = $invgacl->acl_check('inventory', 'iar_stock_leadger', 'users', $_SESSION['authUser']);
$thisAuthDoctor = $invgacl->acl_check('inventory', 'icr_stock_leadger', 'users', $_SESSION['authUser']);
if (!$thisauth && !$thisAuthDoctor) {
    die(xlt('Not authorized'));
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$searchParam = '';

//$form_facility = $_REQUEST['form_facility'];
if(isset($_REQUEST['form_facility'])){
     $_SESSION['cid']=$_REQUEST['form_facility'];
}else{
   $_REQUEST['form_facility']= $_SESSION['cid']; 
}
$form_facility = isset($_REQUEST['form_facility'])? $_REQUEST['form_facility'] : $_SESSION['Auth']['User']['facility_id'];
$from_date = $_REQUEST['form_from_date'];
$to_date = $_REQUEST['form_to_date'];
$provider = $_REQUEST['form_provider'];
$transactionId = $_REQUEST['form_transaction_type'];
$sellable = $_REQUEST['form_sellable'];
//$zero_quantity_items = $_REQUEST['zero_quantity_items'];
$pageNo = $_GET["page"];
$show_available_times = false;
$userFacilityRestrict = $GLOBALS['restrict_user_facility'];

$fileName = "stock_leadger_" . date("Ymd_his") . ".csv";
$catid=$_REQUEST['catid'];
$item=$_REQUEST['form_item'];
$sellable=$_REQUEST['form_sellable'];
// In the case of CSV export only, a download will be forced.
if ($_REQUEST['form_csvexport'] && $_REQUEST['form_csvexport'] == 'csvStockLedger') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <!doctype html>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Stock Ledger Report', 'e'); ?></title>
            <?php include_once("themestyle.php"); ?>
            <?php include_once("scriptcommon.php"); ?>

        </head>
        <body class="stock_leadger_report.php" id="reportsstockledgerbody">
            <!-- page -->
            <div id="page" data-role="page" class="ui-content">
                <!-- header -->
                <?php include_once("oi_header.php"); ?>
                <!-- header -->
                <!-- contentArea -->
                <div id="wrapper" data-role="content" role="main">
                    <!-- wrapper -->
                    <div class='themeWrapper' id='rightpanel'> 
                        <div class='containerWrap'>
                            <!-- pageheading -->
                            <div class='col-sm-12 borbottm'>
                                <?php include_once("inv_links.html"); ?>
                                <h1><?php xl('Stock Ledger Report', 'e'); ?></h1>
                                <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?></div>
                            </div>
                            <!-- pageheading -->
                            <!-- mdleCont -->                        
                            <form method='get' name='theform' id='theform' action='stock_leadger_report.php' class='botnomrg' >
                                <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                                <input type='hidden' name='form_csvexport' id='form_csvexport' value='viewStockLegder'/>
                                <input type="hidden" name="default_facility" value="<?php echo $form_facility ?>" />
                                <input type="hidden" name="default_provider" id="providerId" value="<?php echo $provider ?>" />
                                <!-- leftPart -->
                                <div class="filterWrapper stk">
                                    <!-- facility --> 
                                    <div class="ui-block">
                                        <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('form_facility', '', 'form_facility', $form_facility, $_SESSION['authId'], $userFacilityRestrict, $pdoobject);
                                        ?>
                                    </div>
                                    <!-- facility --> 
                                    <!-- from --> 
                                    <div class='ui-block form_to_date_bx'>
                                        <input type='text' name='form_from_date_in_sl' placeholder='From Date' id="form_from_date_in_sl" size='10' value='' title='' />
                                        <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                    </div>
                                    <!-- from --> 
                                    <!-- to --> 
                                    <div class='ui-block form_to_date_bx'>
                                        <input type='text' name='form_to_date_in_sl' placeholder='To Date' id="form_to_date_in_sl" size='10' value='' title='' />
                                        <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                    </div>
                                    <!-- to --> 
                                    <!-- saleable -->
                                    <div class="ui-block ui-chkbx">
                                        <label for="form_sellable" class="interButt"><?php xl('Saleable', 'e'); ?></label>
                                        <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($sellable) && $sellable == 1) ? 'checked' : ''; ?> >
                                    </div> 
                                    <!-- from -->
                                    <div class='ui-block wdth25'>                                                                      
                                        <a class="pull-right btn_bx" id='reset_form1' href="stock_leadger_report.php?form_facility=<?php echo $_SESSION['reset_cid']; ?>">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-retweet icon"></span>
                                            </span>
                                            <b class="btn-text">Reset</b>
                                        </a>
                                        <?php if (1) { ?>
                                            <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "csvStockLedger");
                                                            $("#theform").submit();'>
                                                <span class="new-btnWrap btn">
                                                    <span class="csv_icon"></span>
                                                </span>
                                                <b class="btn-text"><?php xl('Export', 'e'); ?></b>
                                            </a>
                                        <?php } ?> 
                                        <a id='advanceOptions' class="pull-right" href="javascript:void(0)">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-zoom-in icon"></span>
                                            </span>
                                            <b class="btn-text">Advance</b>
                                        </a>
                                        <a class="pull-right cnsmptn_rep_srch" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "viewConsumption");
                                                    $("#theform").submit();'>
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-search icon5"></span>
                                            </span>
                                            <b class="btn-text">Search</b>
                                        </a>
                                    </div>
                                    <div class='<?php
                                    if (empty($catid) && empty($item)) {
                                        echo "dnone";
                                    }
                                    ?>' id='advanceOptionsToggle'>
                                        <!-- providor -->
                                       <!--  <div class="ui-block" id="doctorsList">
                                           <select name='form_provider' id="form_provider" class='form-control input-sm'>
                                                <option value='' selected='selected'>All Doctors</option>
                                                <?php
//                                                $userId = $_SESSION['authId'];
//                                                $psql = "Select id FROM users WHERE id=$userId ";
//                                                $defaultFac = sqlQuery($psql);
//                                                getFacilityProvider($userId, $defaultFac['id'], $userFacilityRestrict)
                                                ?>
                                            </select>
                                        </div>-->
                                        <!-- providor --> 
                                        <!-- category --> 
                                        <div class='ui-block'>
                                            <select class='form-control input-sm' name='catid' id='catid'>
                                                <?php
                                                if (empty($catid)) {
                                                    ?>
                                                    <option value='0' selected="selected">All Category</option>
                                                <?php } else {
                                                    ?>
                                                    <option value='0'>All Category</option>
                                                <?php } ?>
                                                <?php
                                                $catSql = "SELECT id as invcat_id, name as invcat_name FROM invcategories WHERE is_active='1' AND deleted='0'";
                                                $cres = sqlStatement($catSql, null, $GLOBALS['adodb']['dbreport']);
                                                if (sqlNumRows($cres)) {
                                                    while ($crow = sqlFetchArray($cres)) {
                                                        ?>
                                                        <option value='<?php echo $crow['invcat_id'] ?>' <?php
                                                        if ($catid == $crow['invcat_id']) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $crow['invcat_name'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </div>
                                        <!-- category --> 
                                        <!-- items --> 
                                        <div class='ui-block'>
                                            <input type='text' placeholder='Item' name="form_item" id="form_item" value='<?php echo ($item != '') ? $item : ''; ?>' title=''>
                                        </div>
                                        <!-- items --> 
                                        <!-- price -->
                                                <?php if (0) { ?>
                                            <div class="ui-block ui-chkbx">
                                                <label for="form_price" class="interButt">
                                            <?php xl('Price', 'e'); ?>
                                                </label>
                                                <input type='checkbox' name='form_price' id="form_price" value='1' <?php echo (isset($_POST['form_price']) && $_POST['form_price'] == 1) ? 'checked' : ''; ?> >
                                            </div> 
                                        <?php } ?>
                                        <!-- price -->
                                    </div>
                                </div>
                                <!-- leftPart --> 
                                <!-- table -->
                                <?php
                                    } // end not form_csvexport
                                    if (!empty($_REQUEST['form_csvexport']) && $_REQUEST['form_csvexport'] == 'csvStockLedger') {
                                        $event = "Report Stock Ledger Export";
                                    } else {
                                        $event = "Report Stock Ledger View";
                                    }
                                    if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['form_csvexport'] || empty($form_facility) || $_REQUEST['form_facility']) {
                                        if ($_REQUEST['form_csvexport'] && $_REQUEST['form_csvexport'] == 'csvStockLedger') {
                                            ob_clean();
                                            // CSV headers:
                                            echo '"' . xl('Clinic Name') . '",';
                                            echo '"' . xl('Item Name') . '",';
                                            echo '"' . xl('Batch Number') . '",';
                                            echo '"' . xl('Expiry Date') . '",';
                                            echo '"' . xl('Transaction Type') . '",';
                                            echo '"' . xl('Transcation Date') . '",';
                                            if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) {
                                                echo '"' . xl('Issue Qty') . '",';
                                            }
                                            if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) {
                                                echo '"' . xl('Receipt Qty') . '",';
                                            }
                                            if (empty($transactionId)) {
                                                echo '"' . xl('Balance') . '"';
                                            }
                                            echo "\n";
                                        } else {
                                    ?>
                                    <div id='' class='tableWrp pb-2'>
                                        <div class="dataTables_wrapper no-footer">
                                            <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                
                                                    <!-- added for better print-ability -->
                                                    <?php
                                                }
                                                $message = "";
                                                $rang = getDateRange($from_date, $to_date, $form_facility);

                                                $where = " DATE(istr.invistr_created_date) >= '" . $rang["FromDate"] . "' AND DATE(istr.invistr_created_date) <= '" . $rang["ToDate"] . "'";
                                                $searchParam .= ' From Date = ' . $rang["FromDate"] . ' | To Date = ' . $rang["ToDate"] . ' | ';
                                                $facilityIdList = $form_facility == 0 ? userFacilitys() : $form_facility;
                                                $where .= " AND istr.invistr_clinic_id IN (" . $facilityIdList . ")";
                                                $searchParam .= $form_facility == 0 ? "Facility = All |" : "Facility = " . $facilityIdList . " | ";
                                                $where .=!empty($catid) ? "  AND cat.invcategory_id = '" . $catid . "'" : "";
                                                $fil=!empty($catid) ? " INNER JOIN `invcategories_invitemmasters` AS cat ON cat.inv_im_id = im.inv_im_id " : "";
                                                $searchParam .=!empty($catid) ? ' Category id = ' . $catid . ' | ' : "Category Id = All | ";
                                                $where .=!empty($item) ? " AND im.inv_im_name LIKE '%" . $item . "%'" : "";
                                                $searchParam .=!empty($item) ? ' Item Name = ' . $item . ' | ' : "Item Name= No Item | ";
                                                       
                                                $where .=!empty($sellable) ? " AND im.inv_im_sale = '" . $sellable . "'" : "";
                                                $searchParam .=!empty($sellable) ? " Sellable = 1 | " : "Sellable = 0";
                                                $callType = ($_REQUEST['form_csvexport'] != 'csvStockLedger') ? 1 : 0;
                                                //echo $where;
                                                if (empty($transactionId)) {
                                                    $query = "SELECT f.name AS Facility,istr.invistr_id AS `ID`,
                                                                im.inv_im_name AS `Item Name`,
                                                                istr.invistr_batch AS Batch,
                                                                CONCAT_WS('#', im.inv_im_code, istr.invistr_batch, istr.invistr_expiry, istr.invistr_price) AS stockKey,    
                                                                CASE
                                                                    WHEN(istr.invistr_expiry='0000-00-00') THEN '' ELSE DATE_FORMAT(istr.invistr_expiry,'%d-%m-%y') END AS Expiry,
                                                                DATE_FORMAT(istr.invistr_created_date,'%d-%m-%y') AS `Transaction Date`,
                                                                itrt.invtrt_name AS `Transaction Type`,
                                                                istr.invistr_before_qty,
                                                                CASE
                                                                    WHEN (istr.invistr_before_qty < istr.invistr_after_qty) THEN istr.invistr_quantity
                                                                    ELSE '0'
                                                                END AS receipt,
                                                                CASE
                                                                    WHEN (istr.invistr_before_qty > istr.invistr_after_qty) THEN istr.invistr_quantity
                                                                    ELSE '0'
                                                                END AS issue,
                                                                istr.invistr_after_qty AS 'Balance',
                                                                '#####' AS transid,
                                                                istr.invistr_created_date AS CRDATE
                                                         FROM `inv_item_stock_transaction` AS istr
                                                         LEFT JOIN `inv_item_master` AS im ON im.inv_im_id = istr.invistr_itemid
                                                         INNER JOIN `inv_transaction_type` AS itrt ON itrt.invtrt_id = istr.invistr_tran_type
                                                         INNER JOIN `facility` AS f ON f.id = istr.invistr_clinic_id ".$fil."
                                                         WHERE " . $where . " AND istr.invistr_tran_type !='18'
                                                         UNION ALL
                                                         SELECT DISTINCT f1.name AS Facility,
                                                                         0,
                                                                         im.inv_im_name AS `Item Name`,
                                                                         istr.invistr_batch AS Batch,
                                                                         '######' AS stockKey,
                                                                         DATE(NULL) AS Expiry,
                                                                         DATE(NULL) AS `Transaction Date`,
                                                                         'Opening',
                                                                         0,
                                                                         0,
                                                                         0,

                                                           (SELECT istr1.invistr_before_qty
                                                            FROM inv_item_stock_transaction istr1
                                                            WHERE istr1.invistr_itemid=istr.invistr_itemid
                                                              AND istr1.invistr_batch=istr.invistr_batch
                                                              AND DATE(istr1.invistr_created_date)>='" . $from_date . "'
                                                                  AND DATE(istr1.invistr_created_date)<='" . $to_date . "'
                                                                  
                                                            ORDER BY istr1.invistr_created_date LIMIT 0,
                                                                                                      1) AS Balance,
                                                                         '#####' AS transid,
                                                                         DATE(NULL) AS CRDATE
                                                         FROM `inv_item_stock_transaction` AS istr
                                                         LEFT JOIN `inv_item_master` AS im ON im.inv_im_id = istr.invistr_itemid
                                                         INNER JOIN `inv_transaction_type` AS itrt ON itrt.invtrt_id = istr.invistr_tran_type
                                                         INNER JOIN `facility` AS f1 ON f1.id = istr.invistr_clinic_id ".$fil." WHERE $where AND istr.invistr_tran_type !='18'
                                                         ORDER BY Facility,`Item Name`,Batch,CRDATE,ID";
                                                } else {
                                                    $query = "SELECT istr.invistr_id AS `ID`, f.name AS Facility,im.inv_im_name AS `Item Name`, istr.invistr_batch AS Batch,CASE
                                                                    WHEN(istr.invistr_expiry='0000-00-00') THEN '' ELSE DATE_FORMAT(istr.invistr_expiry,'%d-%m-%y') END AS Expiry,
                                                                DATE_FORMAT(istr.invistr_created_date,'%d-%m-%y') AS `Transaction Date`,istr.invistr_tran_type,itrt.invtrt_name AS `Transaction Type`, istr.invistr_before_qty, 
                                                            CASE WHEN (istr.invistr_before_qty < istr.invistr_after_qty) THEN istr.invistr_quantity ELSE '0' END AS receipt, 
                                                            CASE WHEN (istr.invistr_before_qty > istr.invistr_after_qty) THEN istr.invistr_quantity ELSE '0' END AS issue
                                                            FROM `inv_item_stock_transaction` AS istr 
                                                            LEFT JOIN `inv_item_master` AS im ON im.inv_im_id = istr.invistr_itemid 
                                                            INNER JOIN `inv_transaction_type` AS itrt ON itrt.invtrt_id = istr.invistr_tran_type 
                                                            INNER JOIN `facility` AS f ON f.id = istr.invistr_clinic_id WHERE " . $where;
                                                }
                                                $daysRangeNotAllowed = $form_facility == 0 ? $form_facility_all_time_range_valid : $form_facility_time_range_valid;
                                                $daysRangeNotAllowedMessage = $form_facility == 0 ? $form_facility_all_time_range_valid_errorMsg : $form_facility_time_range_valid_errorMsg;
                                                $cronScheduleMessage = $form_facility == 0 ? $form_facility_all_time_range_errorMsg : $form_facility_time_range_errorMsg;
                                                $pdoobjectr=new database("write"); 
                                                if ($rang['Days'] > $daysRangeNotAllowed) {

                                                    $message = $daysRangeNotAllowedMessage;
                                                } else {
                                                    if ($rang['Cron']) {
                                                        
                                                        $rcsl_name_type = "stock_leadger_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $rang["FromDate"], "toDate" => $rang["ToDate"], "category" => $catid,"Item"=>$item,"sellable"=>$sellable ,"facility" => getLoggedUserAssignedClinics()))); // changable
                                                        $rcsl_report_description = "Stock Ledger report from " . $rang["FromDate"] . " to " . $rang["ToDate"];
                                                        //cron log email//
                                                        
                                                        $getFacelty=getFacilityName($form_facility);
                                                        $subject="Stock Ledger"."_".$getFacelty."_".date('Y-m-d H:i:s');
                                                        $filters="Frome Date = ".$rang["FromDate"]. " To Date = ".$rang["ToDate"]."Facility = ".$getFacelty." Category = ".getCategoryName($catid)." Transaction Type = ".getTransactionType($transactionId)." Item = ".$item." Sellable= ".$sellable;                                        
                                                        
                                                        $report_data=  json_encode(array('header'=>'Clinic Name,Item Name,Batch No.,Expiry Date,Tran. Type,Tran. Date,Issue Qty,Receipt Qty,Balance','include'=>array('Facility','Item Name','Batch','Expiry','Transaction Type','Transaction Date','issue','receipt','Balance'),'name'=>'Stock Ledger Report','subject'=>$subject,'filters'=>$filters, "facility" => $getFacelty,'query'=>$query));
                                                        //cron log email//
                                                        $msgForReportLog = scheduleReports("stock_leadger_report", $rcsl_requested_filter, $rcsl_report_description, $report_data,$pdoobjectr);
                                                        $message = empty($msgForReportLog) ? $cronScheduleMessage : $msgForReportLog;
                                                    } else {

                                                        $message = "";
                                                    }
                                                }
                                                if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvStockLedger') {
                                                    $event = "Report Stock Ledger Export";
                                                } else {
                                                    $event = "Report Stock Ledger View";
                                                }


                                                if ($message) {

                                                    debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2, $pdoobjectr); // Cron Schedule Report Auditing Section
                                                } else {
                                                    //echo $query;
                                                    $page = $_GET['page'];
                                                    $num_rows = $pdoobject->custom_query($query, NULL, 1);
                                                    $per_page = $GLOBALS['encounter_page_size'];
                                                    $curr_URL = $_SERVER['SCRIPT_NAME'] . '?form_refresh=viewStockLedger&' . 'form_facility=' . $form_facility . '&form_from_date=' . $from_date . '&form_to_date=' . $to_date . '&form_sellable=' . $sellable . '&form_provider=' . $provider . '&form_transaction_type=' . $transactionId . '&zero_quantity_items=' . $zeroItem . '&catid='.$catid.'&form_item='.$item.'&';
                                                    $page = ($page == 0 ? 1 : $page);
                                                    $page_start = ($page - 1) * $per_page;
                                                    $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);

                                                    if ($callType == 1 && $flag == 0) {
                                                        $query.= " LIMIT $page_start , $per_page";
                                                    }
                                                    $res = $pdoobject->custom_query($query);
                                                    if ($_REQUEST['form_csvexport'] != 'csvStockLedger') {
                                                        echo $pa_data;
                                                    }
                                                    if (!empty($res)) {
                                                        $prevClinic = '';
                                                        $currClinic = '';
                                                        $currItem = '';
                                                        $prevItem = '';
                                                        $currBatch = '';
                                                        $prevBatch = '';
                                                        $srCount = 1;
                                                        //while ($row = sqlFetchArray($res)) {
                                                        foreach ($res as $row) {
                                                            $currClinic = $row['Facility'];
                                                            $currItem = $row['Item Name'];
                                                            $currBatch = $row['Batch'];
                                                            $expiry = $row['Expiry'];
                                                            if ($_REQUEST['form_csvexport'] && $_REQUEST['form_csvexport'] == 'csvStockLedger') {
                                                                if ($currClinic != $prevClinic) {//CSV EXPORT - First Nested
                                                                    echo '"' . qescape($row['Facility']) . '",';
                                                                    echo '"",';
                                                                    echo '"",';
                                                                    echo '"",';
                                                                    echo '"",';
                                                                    echo '"",';
                                                                    if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) {
                                                                        echo '"",';
                                                                    }
                                                                    if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) {
                                                                        echo '"",';
                                                                    }
                                                                    if (empty($transactionId)) {
                                                                        echo '"",';
                                                                    }
                                                                    echo "\n";
                                                                    //First record after facility
                                                                    echo '"",';
                                                                    echo '"' . qescape($row['Item Name']) . '",';
                                                                    echo '"' . qescape($row['Batch']) . '",';
                                                                    echo '"' . qescape($expiry) . '",';
                                                                    echo '"' . qescape($row['Transaction Type']) . '",';
                                                                    echo '"' . qescape($row['Transaction Date']) . '",';
                                                                    if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) {
                                                                        echo '"' . qescape(($row['issue']) == 0 ? '' : $row['issue']) . '",';
                                                                    }
                                                                    if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) {
                                                                        echo '"' . qescape(($row['receipt']) == 0 ? '' : $row['receipt']) . '",';
                                                                    }if (empty($transactionId)) {
                                                                        echo '"' . qescape($row['Balance']) . '",';
                                                                    }
                                                                    echo "\n";
                                                                } else {
                                                                    //First record after facility
                                                                    echo '"",';
                                                                    if ($currItem != $prevItem) {
                                                                        echo '"' . qescape($row['Item Name']) . '",';
                                                                    } else {
                                                                        echo '"",';
                                                                    }
                                                                    if ($currBatch != $prevBatch) {
                                                                        echo '"' . qescape($row['Batch']) . '",';
                                                                    } else {
                                                                        echo '"",';
                                                                    }

                                                                    echo '"' . qescape($expiry) . '",';
                                                                    echo '"' . qescape($row['Transaction Type']) . '",';
                                                                    echo '"' . qescape($row['Transaction Date']) . '",';
                                                                    if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) {
                                                                        echo '"' . qescape(($row['issue']) == 0 ? '' : $row['issue']) . '",';
                                                                    }
                                                                    if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) {
                                                                        echo '"' . qescape(($row['receipt']) == 0 ? '' : $row['receipt']) . '",';
                                                                    }if (empty($transactionId)) {
                                                                        echo '"' . qescape($row['Balance']) . '",';
                                                                    }
                                                                    echo "\n";
                                                                }
                                                            } else {?>
                                                                        <?php if ($srCount == 1) {
                                                                            ?>
                                                                            <thead>
                                                                                <tr>  
                                                                                    <?php if (empty($form_facility)) { ?>
                                                                                        <th><?php xl('Clinic Name', 'e'); ?></th>
                                                                                    <?php } ?>
                                                                                    <th width='22%'><?php xl('Item Name', 'e'); ?></th>
                                                                                    <th><?php xl('Batch No.', 'e'); ?></th>
                                                                                    <th><?php xl('Expiry Date', 'e'); ?></th>
                                                                                    <th><?php xl('Tran. Type', 'e'); ?></th>
                                                                                    <th><?php xl('Tran. Date', 'e'); ?></th>
                                                                                    <?php if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) { ?>
                                                                                        <th><?php xl('Issue Qty', 'e'); ?></th>
                                                                                    <?php }if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) { ?>
                                                                                        <th><?php xl('Receipt Qty', 'e'); ?></th>
                                                                                        <?php
                                                                                    }
                                                                                    if (empty($transactionId)) {
                                                                                        ?>
                                                                                        <th><?php xl('Balance', 'e'); ?></th>
                                                                                    <?php } ?>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                }
                                                                                if ($currClinic != $prevClinic && empty($form_facility)) {
                                                                                ?>
                                                                                    <tr>
                                                                                        <td><?php echo $row['Facility'] ?></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td>&nbsp;</td>
                                                                                        <?php if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) { ?>
                                                                                            <td>&nbsp;</td>
                                                                                        <?php }if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) { ?>
                                                                                            <td></td>
                                                                                        <?php } ?>
                                                                                        <?php if (empty($transactionId)) { ?>
                                                                                            <td></td><?php } ?>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <?php if (empty($form_facility)) { ?>
                                                                                            <td></td> <?php } ?>
                                                                                        <?php if ($currItem != $prevItem && $currClinic != $prevClinic) { ?>
                                                                                            <td><?php echo $row['Item Name']; ?></td>
                                                                                        <?php } else if ($currItem == $prevItem && $currClinic != $prevClinic) { ?>
                                                                                            <td><?php echo $row['Item Name']; ?></td>
                                                                                        <?php } else {
                                                                                            ?>
                                                                                            <td></td>
                                                                                        <?php } ?>

                                                                                        <td><?php echo $row['Batch']; ?>
                                                                                        <td><?php echo $expiry; ?></td>
                                                                                        <td><?php echo $row['Transaction Type']; ?></td>
                                                                                        <td><?php echo $row['Transaction Date']; ?></td>
                                                                                        <?php if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) { ?>
                                                                                            <td><?php echo ($row['issue']) == 0 ? '' : $row['issue']; ?></td>
                                                                                        <?php }if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) { ?>
                                                                                            <td><?php echo ($row['receipt']) == 0 ? '' : $row['receipt']; ?></td>
                                                                                        <?php } if (empty($transactionId)) { ?>
                                                                                            <td><?php echo ($row['Balance']) == '' ? 0 : $row['Balance']; ?></td><?php } ?>
                                                                                    </tr>
                                                                                    <?php } else {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <?php if (empty($form_facility)) { ?>
                                                                                                    <td></td> <?php } ?>
                                                                                                <?php if ($currItem != $prevItem) { ?>
                                                                                                    <td><?php echo $row['Item Name']; ?></td><?php } else { ?>
                                                                                                    <td></td>
                                                                                                <?php }
                                                                                                ?>
                                                                                                <?php if ($currBatch != $prevBatch) { ?>
                                                                                                    <td><?php echo $row['Batch']; ?></td><?php } else { ?>
                                                                                                    <td></td>
                                                                                                <?php }
                                                                                                ?>
                                                                                                <td><?php echo $expiry; ?></td>
                                                                                                <td><?php echo $row['Transaction Type']; ?></td>
                                                                                                <td><?php echo $row['Transaction Date']; ?></td>
                                                                                                <?php if (empty($transactionId) || in_array($transactionId, array(4, 5, 6, 8, 12, 14))) { ?>
                                                                                                    <td><?php echo ($row['issue']) == 0 ? '' : $row['issue']; ?></td>
                                                                                                <?php }if (empty($transactionId) || in_array($transactionId, array(1, 5, 7, 9, 15))) { ?>
                                                                                                    <td><?php echo ($row['receipt']) == 0 ? '' : $row['receipt']; ?></td>
                                                                                                <?php }if (empty($transactionId)) { ?>
                                                                                                    <td><?php echo ($row['Balance']) == '' ? 0 : $row['Balance']; ?></td><?php } ?>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                            $prevClinic = $row['Facility'];
                                                            $prevItem = $row['Item Name'];
                                                            $prevBatch = $row['Batch'];
                                                            $srCount++;
                                                        }
                                                    }
                                                    debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog'], NULL, $pdoobjectr);
                                                }
                                                    // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
                                                    if ($_POST['form_csvexport'] != 'csvStockLedger') {
                                                        if (!$srCount && empty($message)) {
                                                        ?>
                                                            <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                        <?php
                                                        }
                                                    }
                                                if ($message) {
                                                    
                                                    ?>

                                                        <tr>
                                                            <td colspan="9" class='newtextcenter' style="font-size:10pt;"> <?php echo ((!empty($message)) ? $message : 'No Results Found'); ?></td>
                                                        </tr>
                                                    <?php 
                            }}
                            
                                                    if ( $_REQUEST['form_csvexport'] != 'csvStockLedger') {
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- pagination --> <?php echo $pa_data; ?> <!-- pagination -->
                                        </div>
                                    </form>
                                    <!-- mdleCont -->
                                </div>
                            </div>
                            <!-- wrapper -->
                        </div>
                        <!-- contentArea -->
                    </div>
                    <!-- page -->



                </body>
            </html> 
            <?php
       
            }
            
            
?>
