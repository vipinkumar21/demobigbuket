<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

$thisauth = $invgacl->acl_check('inventory','iar_adj','users', $_SESSION['authUser']);
$thisAuthDoctor = $invgacl->acl_check('inventory','icr_adj','users', $_SESSION['authUser']);
if (!$thisauth && !$thisAuthDoctor) {
    die(xlt('Not authorized'));
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
if(isset($_REQUEST['form_facility'])){
     $_SESSION['cid']=$_REQUEST['form_facility'];
}else{
   $_REQUEST['form_facility']= $_SESSION['cid']; 
}
$form_facility = isset($_REQUEST['form_facility'])? $_REQUEST['form_facility'] : $_SESSION['Auth']['User']['facility_id'];
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
$provider = $_REQUEST['form_provider'];
$item = $_REQUEST['form_item'];
$sellable = $_REQUEST['form_sellable'];
$catid = $_REQUEST['catid'];
$searchParam = '';
$show_available_times = false;
$ORDERHASH = array(
    'invistr_id' => 'invistr_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invistr_id';
$orderby = $ORDERHASH[$form_orderby];

$fileName = "adjustment_report_" . date("Ymd_his") . ".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvAdjustment') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Adjustment Report', 'e'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        </head>
        <body>
            <!-- page -->
            <div id="page" data-role="page" class="ui-content">
                <!-- header -->
                <?php include_once("oi_header.php"); ?>
                <!-- header -->
                <!-- contentArea -->
                <div id="wrapper" data-role="content" role="main">
                    <!-- wrapper -->
                    <div class='themeWrapper' id='rightpanel'>  
                        <div class='containerWrap'>
                            <!-- pageheading -->
                            <div class='col-sm-12 borbottm'>
                                <?php include_once("inv_links.html"); ?>
                                <h1><?php xl('Adjustment Report', 'e'); ?></h1>
                                <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?></div>
                            </div>
                            <!-- pageheading -->
                            <!-- mdleCont -->
                            <form method='post' name='theform' id='theform' action='adjustment_report.php' class='botnomrg'>
                                <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                                <input type='hidden' name='form_csvexport' id='form_csvexport' value='viewAdjustment'/>
                                <input type='hidden' name='providerId' id='providerId' value='<?php echo $provider ?>'/>
                                <!-- leftPart -->
                                <div class="filterWrapper">
                                    <!-- facility --> 
                                    <div class="ui-block">
                                            <?php 
                                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                          //  echo $userFacilityRestrict;
                                            usersFacilityDropdown('form_facility','' ,'form_facility',$form_facility,$_SESSION['authId'],$userFacilityRestrict,$pdoobject);
                                            ?>
                                        </div>
                                    <!-- facility --> 
                                    <!-- from --> 
                                    <div class='ui-block form_to_date_bx'>
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $form_date; ?>' />
                                    </div>
                                    <!-- from --> 
                                    <!-- to --> 
                                    <div class='ui-block form_to_date_bx'>
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $form_to_date; ?>' />
                                    </div>
                                    <!-- to --> 
                                    <!-- saleable -->
                                    <div class="ui-block ui-chkbx">
                                        <label for="form_sellable" class="interButt"><?php xl('Saleable', 'e'); ?></label>
                                        <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_POST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
                                    </div> 
                                    <!-- from -->
                                    <div class='ui-block wdth25'>                                                                      
                                        <a class="pull-right btn_bx" id='reset_form1' href="adjustment_report.php?form_facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                        <?php if (1) { ?>
                                            <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "csvAdjustment");
                                                            $("#theform").submit();'>
                                                <span class="new-btnWrap btn">
                                                    <span class="csv_icon"></span>
                                                </span>
                                                <b class="btn-text"><?php xl('Export', 'e'); ?></b>
                                            </a>
                                        <?php } ?> 

                                        <a id='advanceOptions' class="pull-right" href="javascript:void(0)">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-zoom-in icon"></span>
                                            </span>
                                            <b class="btn-text">Advance</b>
                                        </a>

                                        <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "viewAdjustment");
                                                    $("#theform").submit();'>
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-search icon"></span>
                                            </span>
                                            <b class="btn-text">Search</b>
                                        </a>
                                    </div>
                                    <div class='<?php if(empty($catid) && empty($item)){ echo "dnone";} ?>' id='advanceOptionsToggle'>
                                       <!-- providor -->
                                        <div class="ui-block"  id="doctorsList">
<!--                                            <select name='form_provider' id="form_provider" class='form-control input-sm'>
                                                    <option value='' selected='selected'>All Doctors</option>
                                            </select>-->
                                        </div>
                                        <!-- providor --> 
                                        <!-- category --> 
                                        <div class='ui-block'>
                                            <select class='form-control  input-sm'  name='catid' id='catid'>
                                                <?php
                                                if (empty($row['inv_im_catId'])) {
                                                    ?>
                                                    <option value='0' selected="selected">All Category</option>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                $cres=$pdoobject->fetch_multi_row("invcategories",array("id as invcat_id","name as invcat_name"),array('is_active'=>'1','deleted'=>'0')); 
                                                    foreach($cres as $crow) {
                                                        ?>
                                                        <option value='<?php echo $crow['invcat_id'] ?>' <?php
                                                        if ($_POST['catid'] == $crow['invcat_id']) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $crow['invcat_name'] ?></option>
                                                                <?php
                                                            }
                                                       // }
                                                        ?>
                                            </select>
                                        </div>
                                        <!-- category --> 
                                        <!-- items --> 
                                        <div class='ui-block'>
                                            <input type='text' placeholder='Item' name='form_item' id="form_item" size='20' value='<?php echo ($item != '') ? $item : ''; ?>' title=''>
                                        </div>
                                        <!-- items --> 
                                        <!-- price -->
                                        <?php if (0) { ?>
                                            <div class="ui-block ui-chkbx7">
                                                <label for="form_price" class="interButt">
                                                    <?php xl('Price', 'e'); ?>
                                                </label>
                                                <input type='checkbox' name='form_price' id="form_price" value='1' <?php echo (isset($_POST['form_price']) && $_POST['form_price'] == 1) ? 'checked' : ''; ?> >
                                            </div> 
                                        <?php } ?>
                                        <!-- price -->
                                    </div>
                                </div>
                                <!-- leftPart -->                                            
                                <!-- table -->
                                <?php
                            } // end not form_csvexport
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['form_csvexport'] || empty($form_facility) || $_REQUEST['form_facility']) {
                                if ($_REQUEST['form_csvexport'] && $_REQUEST['form_csvexport'] == 'csvAdjustment') {
                                    // CSV headers:
                                    echo '"' . xl('Clinic Name') . '",';
                                    echo '"' . xl('Name') . '",';
                                    echo '"' . xl('Batch Number') . '",';
                                    echo '"' . xl('Expiry') . '",';

                                    if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
                                        echo '"' . xl('Price') . '",';
                                    }
                                    echo '"' . xl('Transaction Date') . '",';
                                    echo '"' . xl('Adjustment Type') . '",';
                                    echo '"' . xl('Notes') . '",';
                                    echo '"' . xl('Adj. Qty') . '"' . "\n";
                                } else {
                                    ?>
                                    <div id='' class='tableWrp pb-2'>
                                        <div class="dataTables_wrapper no-footer">
                                            <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                        
                                                            <!-- added for better print-ability -->
                                                            <?php
                                                        }

                                                        $message = "";
                                                        $rang = getDateRange($form_date, $form_to_date, $form_facility);


                                                        $where = " AND DATE(tran.invistr_created_date) >= '" . $rang["FromDate"] . "' AND DATE(tran.invistr_created_date) <= '" . $rang["ToDate"] . "'";
                                                        $searchParam .= ' From Date = ' . $rang["FromDate"] . ' | To Date = ' . $rang["ToDate"] . ' | ';

                                                        $facilityIdList = $form_facility == 0 ? userFacilitys() : $form_facility;

                                                        $where .= " AND tran.invistr_clinic_id IN (" . $facilityIdList . ")";
                                                        $searchParam .= $form_facility == 0 ? "Facility = All |" : "Facility = " . $facilityIdList . " | ";
                                                        $where .=!empty($provider) ? "  AND tran.invistr_createdby = '" . $provider . "'" : "";
                                                        $searchParam .=!empty($provider) ? ' Provider = ' . $provider . ' | ' : "Provider = All | ";
                                                        $where .=!empty($catid) ? " AND FIND_IN_SET(" . $catid . ",im.inv_im_catId)" : "";
                                                        $searchParam .=!empty($catid) ? ' Category = ' . $catid . ' | ' : "Category = All | ";
                                                        $where .=!empty($sellable) ? " AND im.inv_im_sale = '" . $sellable . "'" : "";
                                                        $searchParam .=!empty($sellable) ? ' Saleable Items = Yes | ' : "Saleable Items = No | ";
                                                        $where .=!empty($item) ? " AND im.inv_im_name LIKE '" . $item . "%'" : "";
                                                        $searchParam .=!empty($item) ? ' Item = ' . $item . ' | ' : "";
                                                        $query = "SELECT adj.ina_id, adj.ina_item_id, adj.ina_stock_id, adj.ina_quantity,adj.notes,  CASE WHEN (adj.ina_type='In') THEN 'Add' ELSE 'Remove' END  AS ina_type, adj.ina_facility_id as clinicId, adjrel.iatr_id, tran.invistr_id, tran.invistr_before_qty, tran.invistr_after_qty, tran.invistr_comment,
                                                        CASE 
                                                            WHEN (tran.invistr_expiry = '0000-00-00') THEN ''
                                                            ELSE DATE_FORMAT(tran.invistr_expiry, '%d-%m-%y') 
                                                        END
                                                            AS expiryformateddate, 
                                                        CASE 
                                                            WHEN (tran.invistr_created_date = '0000-00-00') THEN ''
                                                            ELSE DATE_FORMAT(tran.invistr_created_date , '%d-%m-%y')
                                                        END AS batchformateddate, 
                                                        u.fname, u.lname, u.nickname, im.inv_im_name, tran.invistr_batch, tran.invistr_price, tran.invistr_quantity, f.name AS clinicname  
                                                        FROM inv_item_stock_transaction AS tran  
                                                        INNER JOIN inv_adj_trans_rel AS adjrel ON tran.invistr_id = adjrel.iatr_trans_id 
                                                        INNER JOIN inv_adjustments AS adj ON adjrel.iatr_adj_id = adj.ina_id 
                                                        INNER JOIN facility AS f ON f.id = tran.invistr_clinic_id
                                                        INNER JOIN inv_item_master AS im ON im.inv_im_id = tran.invistr_itemid
                                                        LEFT JOIN inv_transaction_type AS invtrt ON tran.invistr_tran_type = invtrt.invtrt_id 
                                                        LEFT JOIN `users` AS u ON u.id = im.inv_im_createdby 
                                                        WHERE tran.invistr_isdeleted = '0' AND tran.invistr_tran_type = 5 " . $where . " ORDER BY $orderby";
                                                        $daysRangeNotAllowed = $form_facility == 0 ? $form_facility_all_time_range_valid : $form_facility_time_range_valid;
                                                        $daysRangeNotAllowedMessage = $form_facility == 0 ? $form_facility_all_time_range_valid_errorMsg : $form_facility_time_range_valid_errorMsg;
                                                        $cronScheduleMessage = $form_facility == 0 ? $form_facility_all_time_range_errorMsg : $form_facility_time_range_errorMsg;
														 $pdoobjectr=new database("write"); 
                                                        if ($rang['Days'] > $daysRangeNotAllowed) {

                                                            $message = $daysRangeNotAllowedMessage;
                                                        } else {
                                                            if ($rang['Cron']) {
                                                                $rcsl_name_type = "adjustment_report"; // changable
                                                                $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $rang["FromDate"], "toDate" => $rang["ToDate"], "provider" => $provider, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "facility" => getLoggedUserAssignedClinics()))); // changable
                                                                $rcsl_report_description = "Adjustment report from" . $rang["FromDate"] . " to " . $rang["ToDate"];
                                                                $getFacility = getFacilityName($form_facility);
                                                                $subject = "Adjustment Report" . "_" . $getFacility . "_" . date('Y-m-d H:i:s');
                                                                $sellebleCondition = ($sellable==1) ? "Yes" : "No";
                                                                $filters = "From Date =" . $rang["FromDate"] . " ,To Date =" . $rang["ToDate"] . " ,Provider =" .getDoctorsName($provider) . " ,Item Text =" . $item . " ,Saleable =" . $sellebleCondition ." ,Category  =". getCategoryName($catid). " ,Facility =" . $getFacility;
                                                                $sellebleHeader = ($sellable == 1) ? ",Price," : ",";
                                                                $header = "Clinic Name,Name,Batch Number,Expiry $sellebleHeader Transaction Date,Type,Quantity,Notes";
                                                                $includeArr = array('clinicname', 'inv_im_name', 'invistr_batch', 'expiryformateddate');
                                                                if ($sellable) {
                                                                    array_push($includeArr, "invistr_price", "batchformateddate", "ina_type", "invistr_quantity","notes");
                                                                } else {
                                                                    array_push($includeArr, "batchformateddate", "ina_type", "invistr_quantity","notes");
                                                                }
                                                                $report_data = json_encode(array(
                                                                    'header' => $header,
                                                                    'include' => $includeArr,
                                                                    'name' => 'Adjustment Report', 
                                                                    'subject' => $subject, 
                                                                    'facility' => $getFacility, 
                                                                    'filters' => $filters, 
                                                                    'query' => $query)
                                                                );
                                                                //allFacilityReports() defined with globals.php
                                                                $msgForReportLog = scheduleReports("adjustment_report", $rcsl_requested_filter, $rcsl_report_description, $report_data, $pdoobjectr);
                                                                $message = empty($msgForReportLog) ? $cronScheduleMessage : $msgForReportLog;
                                                            } else {

                                                                $message = "";
                                                            }
                                                        }
                                                        if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvAdjustment') {
                                                            $event = "Report Adjustment Export";
                                                        } else {
                                                            $event = "Report Adjustment View";
                                                        }

                                                        
                                                        
                                                        if ($message) {
                                                            debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2, $pdoobjectr); // Cron Schedule Report Auditing Section
                                                        } else {
                                                            $auditid = debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 0, $pdoobjectr); // Report Auditing Section

                                                            $num_rows = $pdoobject->custom_query($query, NULL, 1);

                                                            $per_page = $GLOBALS['encounter_page_size'];
                                                            $page = $_GET["page"];

                                                            $curr_URL = $_SERVER['SCRIPT_NAME'] . "?form_refresh=viewAdjustment&form_facility=$form_facility&form_from_date=$form_date&form_to_date=$form_to_date&form_sellable=$sellable&form_provider=$provider&form_item=$item&catid=$catid&";
                                                            $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                                            $page = ($page == 0 ? 1 : $page);
                                                            $page_start = ($page - 1) * $per_page;
                                                            if ($_POST['form_csvexport'] != 'csvAdjustment' && $num_rows > 0) {
                                                                $query.= " LIMIT $page_start , $per_page";
                                                                echo $pa_data;
                                                            }
                                                            $new_num_rows = $pdoobject->custom_query($query, NULL, 1);
                                                            $res = $pdoobject->custom_query($query);
                                                            if ($new_num_rows) {
                                                                $srCount = 1;
                                                                $prevClinic = '';
                                                                $currClinic = '';
                                                                $currItem = '';
                                                                $prevItem = '';
                                                                foreach ($res as $row) {
                                                                    $currClinic = $row['clinicId'];
                                                                    $currItem = $row['ina_item_id'];
                                                                    if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvAdjustment') {
                                                                        if ($currClinic != $prevClinic) {
                                                                            echo '"' . qescape($row['clinicname']) . '",';
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
                                                                                echo '"",';
                                                                            }
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            echo '"",';
                                                                            echo '""' . "\n";

                                                                            echo '"",';
                                                                            echo '"' . qescape($row['inv_im_name']) . '",';
                                                                            echo '"' . qescape($row['invistr_batch']) . '",';
                                                                            echo '"' . qescape($row['expiryformateddate']) . '",';
                                                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
                                                                                echo '"' . qescape($row['invistr_price']) . '",';
                                                                            }
                                                                            echo '"' . qescape($row['batchformateddate']) . '",';
                                                                            echo '"' . qescape($row['ina_type']) . '",';
                                                                            echo '"'. qescape($row['notes']) .'",';
                                                                            echo '"' . qescape($row['invistr_quantity']) . '"' . "\n";
                                                                        } else {
                                                                            echo '"",';
                                                                            if ($currItem != $prevItem) {
                                                                                echo '"' . qescape($row['inv_im_name']) . '",';
                                                                            } else {
                                                                                echo '"",';
                                                                            }
                                                                            echo '"' . qescape($row['invistr_batch']) . '",';
                                                                            echo '"' .  qescape($row['expiryformateddate']) . '",';
                                                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
                                                                                echo '"' . qescape($row['invistr_price']) . '",';
                                                                            }
                                                                            echo '"' . qescape($row['batchformateddate']) . '",';
                                                                            echo '"' . qescape($row['ina_type']) . '",';
                                                                            echo '"'. qescape( $row['notes']).'",';
                                                                            echo '"' . qescape($row['invistr_quantity']) . '"' . "\n";
                                                                        }
                                                                    } else {
                                                                        if($srCount==1) {
                                                                        ?>
                                                                        <thead>
                                                                            <tr>
                                                                                <?php if (empty($form_facility)) { ?>
                                                                                <th width='15%'><?php xl('Clinic Name', 'e'); ?></th><?php } ?>
                                                                                <th width='15%'><?php xl('Name', 'e'); ?></th>
                                                                                <th width='10%'><?php xl('Batch Number', 'e'); ?></th>
                                                                                <th width='9%'><?php xl('Expiry', 'e'); ?></th>
                                                                                <?php if (isset($_REQUEST['form_sellable']) && $_REQUEST['form_sellable'] != '') { ?>
                                                                                    <th width='9%'><?php xl('Price', 'e'); ?></th>
                                                                                <?php } ?>
                                                                                <th width='10%'><?php xl('Transaction Date', 'e'); ?></th>
                                                                                <th width='9%'><?php xl('Type', 'e'); ?></th>
                                                                                <th width='15%'><?php xl('Notes', 'e'); ?></th>
                                                                                <th width='8%'><?php xl('Quantity', 'e'); ?></th>
                                                                                <!--<th width='10%' class='txtCenter'><?php xl('Running Qty', 'e'); ?></th>-->
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php }
                                                                        if ($currClinic != $prevClinic && empty($form_facility)) { ?>
                                                                            <tr>
                                                                                <td><?php echo $row['clinicname']; ?></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                                    <td></td> 
                                                                                <?php } ?>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <?php if (empty($form_facility)) { ?>
                                                                                    <td></td><?php } ?>
                                                                                <?php if ($currItem != $prevItem && $currClinic != $prevClinic) { ?>
                                                                                    <td><?php echo $row['inv_im_name']; ?></td>
                                                                                <?php } else if ($currItem == $prevItem && $currClinic != $prevClinic) { ?>
                                                                                    <td><?php echo $row['inv_im_name']; ?></td>
                                                                                <?php } else {
                                                                                    ?>
                                                                                    <td></td>
                                                                                <?php } ?>
                                                                                <td><?php echo $row['invistr_batch']; ?></td>
                                                                                <td><?php echo $row['expiryformateddate']; ?></td>
                                                                                <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                                    <td>&nbsp;<?php echo $row['invistr_price']; ?></td> 
                                                                                <?php } ?>
                                                                                <td><?php echo $row['batchformateddate']; ?></td>
                                                                                <td><?php echo $row['ina_type']; ?></td>
                                                                                <td><?php echo $row['notes']; ?></td>
                                                                                <td><?php echo $row['invistr_quantity']; ?></td>
                                                                            </tr>
                                                                            <?php } else { ?>
                                                                            <tr>
                                                                                 <?php if (empty($form_facility)) { ?>
                                                                                <td></td><?php }?>
                                                                            <?php if ($currItem != $prevItem) { ?>
                                                                                <td><?php echo $row['inv_im_name']; ?></td>
                                                                            <?php } else { ?>
                                                                                <td></td>

                                                                            <?php }?>
                                                                                
                                                                                <td><?php echo $row['invistr_batch']; ?></td>
                                                                                <td><?php echo $row['expiryformateddate']; ?></td>
                                                                                <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                                    <td>&nbsp;<?php echo $row['invistr_price']; ?></td> 
                                                                                <?php } ?>
                                                                                    <td><?php echo $row['batchformateddate']; ?></td>
                                                                                <td><?php echo $row['ina_type']; ?></td>
                                                                                <td><?php echo $row['notes']; ?></td>
                                                                                <td><?php echo $row['invistr_quantity']; ?></td>
                                                                               </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    $prevClinic = $row['clinicId'];
                                                                    $prevItem = $row['ina_item_id'];
                                                                    $srCount++;
                                                                }
                                                            }
                                                            debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog'], NULL, $pdoobjectr);
                                                            // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
                                                        }
                                                        if ($_POST['form_csvexport'] != 'csvAdjustment') {
                                                            ?>

                                                            <?php
                                                            if (!$srCount && empty($message)) {
                                                                ?>
                                                               <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                                <?php
                                                            }
                                                        }
                                                        if ($message) {
                                                            ?>

                                                            <tr>
                                                                <td colspan="8" class='newtextcenter' style="font-size:10pt;"> <?php echo ((!empty($message)) ? $message : 'No Results Found'); ?></td>
                                                            </tr>
                                                            <?php
                                                        }if ($_POST['form_csvexport'] != 'csvAdjustment' && $num_rows > 0) {
                                                            ?>
                                                        </tbody>
                                                    </table>
                                        </div>
                                        <?php echo $pa_data; ?>
                                        <!-- pagination -->
                                    </div>
                                    <!-- end of search results --> <?php
                                    }
                                }
                                ?> 
                            <?php
                            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvAdjustment') {
                                ?>
                                <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
                                <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
                                <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                                <!-- table -->
                            </form>
                            <!-- mdleCont -->
                        </div>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- contentArea -->
            </div>
            <!-- page -->
            <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
            </script>
        </body>
    </html>
    <?php
}
?>
