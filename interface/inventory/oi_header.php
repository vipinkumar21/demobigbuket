<?php
$res = sqlQuery("select * from users where username='".$_SESSION{"authUser"}."'");
?>
<!-- START: loader -->
<div id="customLoading"></div>
<div class="ui-widget-overlay"></div>
<!-- END: loader -->

<div class="hdr-height">
	<!-- PRM theme -->
	<div id="headtab" class="ui-header ui-bar-a dnone" data-role="header">
		<div class="ui-grid-d pull-left">
			<div id="logoContainer"><a href="<?php echo $inventry_url; ?>web/home.shtml" data-ajax="false"><img alt="logo" src="<?php echo $inventry_url; ?>webapp/img/clove-logo.jpg" /></a></div>
		</div>
		<div class="logout-wrp">
			<div class="col-xs-6 col-sm-1 pull-right plr-0">
				<a class="btn btn-warning btn-small pull-right logoutBtn" id="logoutBtn" href="#">
					<span class="glyphicon glyphicon-off"></span>
				</a>
			</div>
			<div id="username" class="pull-right user">
				<span class="" title="<?php echo htmlspecialchars( xl('Authorization group') .': '.$_SESSION['authGroup'], ENT_QUOTES); ?>"><?php echo htmlspecialchars($res{"fname"}.' '.$res{"lname"},ENT_NOQUOTES); ?></span>
			</div>
		</div>
	</div>
	<!-- PRM theme -->
	
	<!-- oneivory theme -->
    <!-- forfloatingHeader -->
    <?php if ($oi_theme == 1) { ?>
    <div class='floatingheader transition'>
        <?php include_once("im_header.html"); ?>
        <div id='collapse-bar'></div>
    </div>
    <?php } ?>
    <!-- forfloatingHeader -->
	<div id='ivory_header' class='dnone ivory_header'>
		<div class='blueBg'></div>
		<div class='header'>
			<a href="<?php echo $inventry_url; ?>emr/interface/inventory/inventoryPage.php"><img class="logo_oi" src="<?php echo $inventry_url; ?>emr/interface/img/logo-home.jpg" alt='' /></a>
		</div>
	</div>
	<!-- oneivory theme -->
</div>

<!-- laftnav -->
<?php include_once("oi_nav.php"); ?>