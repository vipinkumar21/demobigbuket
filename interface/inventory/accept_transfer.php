<?php
// Copyright (C) 2006-2011 Rod Roark <rod@sunsetsystems.com>
//
 // This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("oneivoryintg.php");

$alertmsg = '';
$tranid = $_REQUEST['tranid'];
//$facility = $_REQUEST['facility'];
$info_msg = "";
$tmpl_line_no = 0;

//if (!acl_check('inventory', 'invf_tran_in_accept'))
//    die(xlt('Not authorized'));
if (!$invgacl->acl_check('inventory','invf_tran_in_accept','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
<?php html_header_show(); ?>
        <title><?php echo $item_id ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script language="JavaScript">
<?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>

            // This is for callback by the find-code popup.
            // Appends to or erases the current list of related codes.
            function set_related(codetype, code, selector, codedesc) {
                var f = document.forms[0];
                var s = f.form_related_code.value;
                if (code) {
                    if (s.length > 0)
                        s += ';';
                    s += codetype + ':' + code;
                } else {
                    s = '';
                }
                f.form_related_code.value = s;
            }

            function trimAll(sString)
            {
                while (sString.substring(0, 1) == ' ')
                {
                    sString = sString.substring(1, sString.length);
                }
                while (sString.substring(sString.length - 1, sString.length) == ' ')
                {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body class="ui-bg-wht">
        <!-- START: loader -->
        <div id="customLoading"></div>
        <div class="ui-widget-overlay"></div>
        <!-- END: loader -->
        <?php
        // If we are saving, then save and close the window.
        // First check for duplicates.
        //
         //print_r($beforeStockDetails=validateStock(array('invist_id'=>'1')));die();
        if ($_POST['form_save']) {
            
            
            $message="<div class='alert alert-success alert-dismissable'>Transferred quantity received in your clinic!</div>";
            $itemId = $_POST['invtran_item_id'];
            $itemStockId = $_POST['invtran_stock_id'];
            
            //echo $itemStockId;
            //print_r($itemStockDetails);
            if ($_POST['invtran_transfer_quantity'] >= ($_POST['form_qty'] + $_POST['form_return_qty'])) {
                
                try{
                $pdoobject->begin_transaction();
                $itemStockDetails = getItemStockDetail($itemStockId,1," FOR UPDATE ");
                
                $stockData = array(
                    'invist_itemid' => $itemStockDetails['invist_itemid'],
                    'invist_batch' => $itemStockDetails['invist_batch'],
                    'invist_expiry' => $itemStockDetails['invist_expiry'],
                    'invist_price' => $itemStockDetails['invist_price'],
                    'invist_quantity' => escapedff('form_qty'),
                    'invist_clinic_id' => escapedff('invtran_to_facility_id'),
                    'invist_createdby' => $_SESSION['authId'],
                    'invist_created_date' => date("Y-m-d H:i:s")
                );
                
                //print_r($stockData);
                //$beforeStockDetails = validateStock(array('invist_id' => $_REQUEST['invtran_stock_id']),$pdoobject);
                //$beforeStockDetails=$pdoobject->fetch_multi_row("inv_item_stock",array("invist_itemid","invist_batch","invist_expiry","invist_price","invist_quantity","invist_id"),array('invist_id' => $stockid)," FOR UPDATE ");  
                $stockData = insertUpdateItemStock($stockData);
                
                //print_r($stockData);die();
                //echo $_REQUEST['invtran_stock_id'];
                //print_r($beforeStockDetails);
                
                $beforeQty = ($stockData['invist_quantity']) ? $stockData['invist_quantity'] : 0;
                
                
                //die();
                
                $updateTransferData = array(
                    'invtran_id' => escapedff('invtran_id'),
                    'invtran_accept_quantity' => escapedff('form_qty'),
                    'invtran_return_quantity' => escapedff('form_return_qty'),
                    'invtran_accept_user_id' => $_SESSION['authId'],
                    'invtran_accept_date' => date("Y-m-d H:i:s"),
                    'invtran_accept_notes' => escapedff('form_desc')
                );
                //$stockTransid = updateStockTransfer($updateTransferData);
                 $pdoobject->update("inv_transfer",$updateTransferData,array('invtran_id'=>$_POST['invtran_id']));
                 $sresult=$_POST['invtran_id'];
                
                //die();

                $tranData = array(
                    'invistr_itemid' => $stockData['invist_itemid'],
                    'invistr_batch' => $stockData['invist_batch'],
                    'invistr_expiry' => $stockData['invist_expiry'],
                    'invistr_price' => $stockData['invist_price'],
                    'invistr_quantity' => escapedff('form_qty'),
                    'invistr_before_qty' => $beforeQty,
                    'invistr_after_qty' => $beforeQty + escapedff('form_qty'),
                    'invistr_clinic_id' => escapedff('invtran_to_facility_id'),
                    'invistr_tran_type' => 9,
                    'invistr_comment' => escapedff('form_desc'),
                    'invistr_createdby' => $_SESSION['authId'],
                    'invistr_created_date' => date("Y-m-d H:i:s"),
                    'invistr_action' => 'inventory/accept_transfer',
                    'invistr_action_id' => $sresult
                );
               //$stockTranid = insertStockTransaction($tranData);
                $tresult=$pdoobject->insert("inv_item_stock_transaction",$tranData); 
                if($GLOBALS['order_item_cancle_api']){   
                     $type=getFacilityType($_REQUEST['facility']);
            if($type==2){
                    $comment = (escapedff('form_desc')) ? escapedff('form_desc') : 'Item Return';
                    $includeComment = (escapedff('form_desc')) ? '1' : '0';
                    oneivoryintg::save(array('module_name' => 'sales_order_item_cancle', 'endpoint' => 'apilogin_apireturn.add', 'action_id' => escapedff('invtran_id'), 'request' => json_encode(array($data = array('order_id' => escapedff('oneivory_order_id'), 'order_item_id' => escapedff('oneivory_order_item_id'), 'qty' => $_POST['invtran_transfer_quantity'], 'comment' => $comment, 'notify_customer' => true, 'include_comment' => $includeComment)))));
                }
                }
                //}

                /*
                if ($_POST['invtran_transfer_quantity'] > $_POST['form_qty']) {
                    $_POST['form_return_qty'] = $_POST['invtran_transfer_quantity'] - $_POST['form_qty'];
                }
                if (!empty($_POST['form_return_qty'])) {
                    $tranferData = array(
                        'invtran_item_id' => escapedff('invtran_item_id'),
                        'invtran_stock_id' => $stockid,
                        'invtran_transfer_quantity' => escapedff('form_return_qty'),
                        'invtran_transfer_user_id' => $_SESSION['authId'],
                        'invtran_from_facility_id' => escapedff('invtran_to_facility_id'),
                        'invtran_to_facility_id' => escapedff('invtran_from_facility_id'),
                        'invtran_transfer_notes' => escapedff('form_desc'),
                        'invtran_transfer_date' => date("Y-m-d"),
                        'invtran_accept_user_id' => '',
                        'invtran_accept_notes' => ''
                    );
                    $stockTransid = insertStockTransfer($tranferData);
                    $returnItemStockDetails = getItemStockDetail($stockid);
                    $returnBeforeQty = $beforeQty + escapedff('form_qty');
                    $tranData = array(
                        'invistr_itemid' => $returnItemStockDetails['invist_itemid'],
                        'invistr_batch' => $returnItemStockDetails['invist_batch'],
                        'invistr_expiry' => $returnItemStockDetails['invist_expiry'],
                        'invistr_price' => $returnItemStockDetails['invist_price'],
                        'invistr_quantity' => escapedff('form_return_qty'),
                        'invistr_before_qty' => $returnBeforeQty,
                        'invistr_after_qty' => $returnBeforeQty,
                        'invistr_clinic_id' => escapedff('invtran_to_facility_id'),
                        'invistr_tran_type' => 4,
                        'invistr_comment' => escapedff('form_desc'),
                        'invistr_createdby' => $_SESSION['authId'],
                        'invistr_created_date' => date("Y-m-d H:i:s")
                    );
                    $stockTranid = insertStockTransaction($tranData);
                    
                }*/
                $pdoobject->commit();
                }catch (PDOException $e) {
               //echo $message;
               $pdoobject->rollback();  
                $error=$e->getMessage(); 
                $message="<div class='alert alert-danger alert-dismissable'>Error in saving data!</div>";
               }
              
                // Close this window and redisplay the updated list of drugs.
                $_SESSION['INV_MESSAGE'] = $message;

                // if ($message)
                //     echo " alert('$message');\n";
                echo "<script language='JavaScript'>\n";
                echo "parent.location.reload();\n";
                echo "</script></body></html>\n";
                exit();
            }else {
                $alertmsg = '<div class="alert alert-warning alert-dismissable">Entered quantity not available in selected item stock. Please try again!</div>';
                $_SESSION['INV_MESSAGE'] = $alertmsg;
            }
        }
        if (!empty($tranid)) {
            $row = $pdoobject->custom_query("SELECT tran.invtran_id, tran.oneivory_order_id, tran.oneivory_order_item_id, tran.invtran_item_id, tran.invtran_stock_id, tran.invtran_transfer_quantity, tran.invtran_accept_quantity, tran.invtran_transfer_user_id, tran.invtran_accept_user_id, tran.invtran_from_facility_id, tran.invtran_to_facility_id, tran.invtran_transfer_date, tran.invtran_accept_date, tran.invtran_transfer_notes, tran.invtran_accept_notes, ist.invist_id, im.inv_im_name, frf.name AS fromFacility, tof.name AS toFacility,tof.id as facilityid, im.inv_im_catId AS `CatId` 
			FROM inv_transfer AS tran 
			INNER JOIN facility AS frf ON frf.id = tran.invtran_from_facility_id
			INNER JOIN facility AS tof ON tof.id = tran.invtran_to_facility_id
			INNER JOIN inv_item_stock AS ist ON ist.invist_id = tran.invtran_stock_id  
			INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid 
			WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND tran.invtran_id = ? ", array($tranid),'','fetch');
        } else {
            $row = array(
                'invtran_id' => 0,
                'invtran_item_id' => 0,
                'invtran_stock_id' => 0,
                'invtran_transfer_quantity' => 0,
                'invtran_accept_quantity' => 0,
                'invtran_transfer_user_id' => 0,
                'invtran_accept_user_id' => 0,
                'invtran_from_facility_id' => 0,
                'invtran_to_facility_id' => 0,
                'invtran_transfer_date' => '',
                'invtran_accept_date' => '',
                'invtran_transfer_notes' => '',
                'invtran_accept_notes' => '',
                'invist_id' => 0,
                'inv_im_name' => '',
                'invcat_name' => '',
                'invsubcat_name' => '',
                'fromFacility' => '',
                'toFacility' => ''
            );
        }

        $row['categories'] = getCategoryList($row['CatId']);
        ?>
        <div class="infopop"><?php xl('Transfer Receipt', 'e'); ?></div>
        <!-- tableData -->
        <div class="popupTableWrp">
            <form class='no-mrgn' id='acceptTransferForm' method='post' name='theform' action='accept_transfer.php?tranid=<?php echo $tranid; ?>&facility=<?php echo $row['facilityid']; ?>'>
                <input type='hidden' name='invtran_id' value='<?php echo attr($row['invtran_id']) ?>' />
                <input type='hidden' name='invtran_item_id' value='<?php echo attr($row['invtran_item_id']) ?>' />
                <input type='hidden' name='invtran_stock_id' value='<?php echo attr($row['invtran_stock_id']) ?>' />
                <input type='hidden' name='invtran_transfer_quantity' value='<?php echo attr($row['invtran_transfer_quantity']) ?>' />
                <input type='hidden' name='invtran_to_facility_id' value='<?php echo attr($row['invtran_to_facility_id']) ?>' />
                <input type='hidden' name='invtran_from_facility_id' value='<?php echo attr($row['invtran_from_facility_id']) ?>' />
                <input type='hidden' name='oneivory_order_id' value='<?php echo attr($row['oneivory_order_id']) ?>' />
                <input type='hidden' name='oneivory_order_item_id' value='<?php echo attr($row['oneivory_order_item_id']) ?>' />
                <table id='requisitionList' cellpadding='0' cellspacing='0' border='0' class='popupTable ui-table' width='100%'>
                    <thead>
                        <tr>
                            <th width='35%'><?php echo xlt('Item Name'); ?></th>
                            <th><?php echo xlt('Trans. Qty'); ?></th>
                            <th><?php echo xlt('Received Qty'); ?></th>
                            <th width='30%'><?php echo xlt('Receipt Notes'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo attr($row['inv_im_name']); ?></td>
                            <td><?php echo attr($row['invtran_transfer_quantity']); ?></td>
                            <td><?php echo attr($row['invtran_transfer_quantity']) ?>
                                <input type='hidden' size='10' name='form_qty' maxlength='7' onkeypress="return isNumber(event)" value='<?php echo attr($row['invtran_transfer_quantity']) ?>' />
                                <input type='hidden' size='10' name='form_return_qty' maxlength='7' onkeypress="return isNumber(event)" value='0' />
                            </td>
                            <td class='lrgErTxt'><textarea name="form_desc" class='sml-hght' rows="5" cols="34"></textarea></td>
                        </tr>
                    </tbody>
                </table>
                <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                <input type='submit' name='form_save' id='form_save' class="btn btn-warning btn-sm dnone" value='<?php echo xla('Save'); ?>' />
            </form>
        </div>
        <!-- tableData -->
    </body>
</html>
<script>
    $(document).ready(function () {
        $("#facility").change(function () {
            var facilityId = $("#facility").val();
            $.ajax({
                type: 'GET',
                url: "get_clinic_item.php?facilityId=" + facilityId,
                success: function (data) {
                    $("#itemField").html(data);
                }
            });
            $.ajax({
                type: 'GET',
                url: "get_other_facility.php?facilityId=" + facilityId,
                success: function (data) {
                    $("#tofacilityidField").html(data);
                }
            });
        });
    });
</script>