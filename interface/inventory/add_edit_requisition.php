<?php
// Copyright (C) 2013 Virendra Kumar Dubey <virendra.dubey@instantsys.com>
//
// This program is for PRM software.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
require_once("mag_update_status.php");
require_once("$srcdir/classes/class.phpmailer.php");


$alertmsg = '';
$reqid = $_REQUEST['reqid'];
$info_msg = "";
$tmpl_line_no = 0;

if (!$invgacl->acl_check('inventory', 'invf_reqout_add', 'users', $_SESSION['authUser']) || !$invgacl->acl_check('inventory', 'invf_reqout_edit', 'users', $_SESSION['authUser']))
    die(xlt('Not authorized'));

// Format dollars for display.
//
function bucks($amount) {
    if ($amount) {
        $amount = sprintf("%.2f", $amount);
        if ($amount != 0.00)
            return $amount;
    }
    return '';
}

// Translation for form fields used in SQL queries.
//
function escapedff($name) {
    return add_escape_custom(trim($_POST[$name]));
}

function numericff($name) {
    $field = trim($_POST[$name]) + 0;
    return add_escape_custom($field);
}
?>
<html>
    <head>
        <title><?php echo $reqid ? xlt("Edit") : xlt("Add"); echo ' ' . xlt('Stock Requisition'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
        <script type="text/javascript" src="../../library/js/underscore.1.5.2.js"></script>
        <script language="JavaScript">
            <?php require($GLOBALS['srcdir'] . "/restoreSession.php"); ?>
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
            function submitform(e) {
                if (document.forms[0].facility.value > 0 && document.forms[0].tofacilityid.value > 0 && document.forms[0].itemId.value !== "" && parseInt(document.forms[0].form_qty.value) > 0) {
                    return true;
                } else {
                }
            }
        </script>
    </head>
    <body>
        <!-- START: loader -->
        <div id="customLoading"></div>
        <div class="ui-widget-overlay"></div>
        <!-- END: loader -->
        <?php
            // If we are saving, then save and close the window.
            // First check for duplicates.
            //
            if ($_POST['postval']) {
                $message="<div class='alert alert-success alert-dismissable'>Requisition has been raised successfully!</div>";
                try{
                    $pdoobject->begin_transaction();
                   
                if (empty($reqid)) {
                    //always approved
                    /*$isApproved = 0;
                    $userGroupsdata = getUserGroup('value');
                    $isApproved = getStatus($userGroupsdata[0], 100);*/
                    
                    $isApproved= 1;
                    $reqnumber = getReqNumber($_POST['facilityVal'][0] ," FOR UPDATE ");
                    $reqData = array(
                        'isr_number' => $reqnumber,
                        'isr_from_clinic' => $_POST['facilityVal'][0],
                        'isr_to_clinic' => $_POST['tofacilityVal'][0],
                        'isr_isapproved' => $isApproved,
                        'isr_createdby' => $_SESSION['authId'],
                        'isr_created_date' => date("Y-m-d H:i:s")
                    );
                    $reqid = insertStockReq($reqData);

                    if (!empty($reqid) && $isApproved == 0) {
                        sendMail($_POST['facilityVal'][0], 'RequisitionAlert', $_POST['facilityVal'][0], $reqid, $reqnumber, 'clin');
                    }
                    if (!empty($reqid) && $isApproved == 1) {
                        sendMail($_POST['tofacilityVal'][0], 'WarehouseRequisitionAlert', $_POST['facilityVal'][0], $reqid, $reqnumber, 'clin');
                    }
                } else {

                    $isApproved = 0;
                    $userGroupsdata = getUserGroup('value');
                    $isApproved = getStatus($userGroupsdata[0], 100);
                    if ($userGroupsdata[0] == 'Dentist') {
                        $reqnumber = getReqNumber($_POST['facilityVal'][0]);
                        $reqData = array(
                            'isr_id' => $reqid,
                            'isr_isapproved' => $isApproved
                        );
                        insertStockReq($reqData, $reqid);
                        if (!empty($reqid) && $isApproved == 0) {
                            sendMail($_POST['facilityVal'][0], 'RequisitionAlert', $_POST['facilityVal'][0], $reqid, $reqnumber, 'clin');
                        }
                    }
                }

                $stockReqItemData = array();

                for ($countItems = 0; $countItems < count($_POST['facilityVal']); $countItems++) {
                    $itemData = array(
                        'iri_id' => $_POST['iri_id'][$countItems],
                        'iri_reqid' => $reqid,
                        'iri_itemid' => $_POST['itemVal'][$countItems],
                        'iri_quantity' => $_POST['quantity'][$countItems],
                        'iri_isdeleted' => $_POST['deleted'][$countItems]
                    );
                    $stockReqItemData[] = $itemData;
                }
                insertUpdateReqItems($stockReqItemData);
                $pdoobject->commit();
                }catch (PDOException $e) {
                   $pdoobject->rollback();  
                   echo  $error=$e->getMessage(); 
                   $message="<div class='alert alert-danger alert-dismissable'>Error in saving data!</div>";
                }
                // Close this window and redisplay the updated list of drugs.
                //  
                $_SESSION['INV_MESSAGE'] = $message;

                echo "<script>parent.location.reload();</script>";
                echo "<script>parent.$('#theform').submit();</script>";
                //echo "<script language='JavaScript'>\n";
                //echo "parent.location.reload();\n";  
                // echo " window.close();\n";  
                //echo "</script></body></html>\n";
                exit();
            }
            $reqItemsData = array();
            if ($reqid) {
                $sql = "SELECT sreq.isr_id,sreq.isr_number,sreq.isr_from_clinic,sreq.isr_to_clinic,sreq.isr_isapproved,sreq.isr_status,sreq.isr_isdeleted,sreq.isr_createdby,sreq.isr_created_date,reqit.iri_id,iri_reqid,reqit.iri_itemid,reqit.iri_quantity,reqit.iri_isdeleted,im.inv_im_name,im.inv_im_code,im.inv_im_catId,im.inv_im_subcatId,frf.name AS fromFacility, tof.name AS toFacility 
    			FROM inv_stock_requisition AS sreq 
    			INNER JOIN inv_requisition_item AS reqit ON reqit.iri_reqid = sreq.isr_id 
    			INNER JOIN inv_item_master AS im ON im.inv_im_id = reqit.iri_itemid 
    			INNER JOIN facility AS frf ON frf.id = sreq.isr_from_clinic 
    			INNER JOIN facility AS tof ON tof.id = sreq.isr_to_clinic 
    			WHERE sreq.isr_id =  ?";
                $res = $pdoobject->custom_query($sql, array($reqid));
                //print_r($res);
                if (!empty($res)) {
                    $count = 0;
                    foreach ($res as $row) {
                        $reqData = array(
                            "facility" => $row['fromFacility'],
                            "facilityVal" => $row['isr_from_clinic'],
                            "tofacility" => $row['toFacility'],
                            "tofacilityVal" => $row['isr_to_clinic'],
                            "category" => $row['invcat_name'],
                            "categoryVal" => $row['inv_im_catId'],
                            "subCategory" => $row['invsubcat_name'],
                            "subCategoryVal" => $row['inv_im_subcatId'],
                            "item" => $row['inv_im_name'],
                            "itemVal" => $row['iri_itemid'],
                            "quantity" => $row['iri_quantity'],
                            "deleted" => $row['iri_isdeleted'],
                            "requisitionIndex" => $count,
                            "reqItemId" => $row['iri_id']
                        );
                        $reqItemsData[] = $reqData;
                        $count++;
                    }
                }
            }       
            $reqItemsDataJson = json_encode($reqItemsData);
        ?>
        <div class="infopop"><?php xl('Add Requisition', 'e'); ?></div>
        <div id='popUpformWrap'>
            <form method='post' id='theform' name='theform' class='emrtable'>
                <!-- Row 1 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><?php echo xlt('From Facility'); ?></label>
                            <input id='facilityId' type='hidden' > 
                            <div class='dmInout' id='facilityLabel'></div>
                            <?php
                            $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                            $userId = $_SESSION['authId'];
                            usersFacilityDropdown('facility', '', 'facility', $facility, $userId, $userFacilityRestrict, $pdoobject, 0);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><?php echo xlt('To Facility'); ?><sup class="required">*</sup></label>
                            <input id='tofacilityId' type='hidden' >
                            <div class='dmInout' id='tofacilityLabel'></div>
                            <select name='tofacilityid' id="tofacilityidField" class='formEle'>
                                <option value='0' selected="selected">Select Other Facility</option>
                                <?php
                                if (!empty($reqItemsData)) {
                                    $qsql = $pdoobject->custom_query("SELECT id, name FROM facility WHERE id != ? ORDER BY name ASC ", array($_SESSION['authId']), '', 'fetchAll');
                                    foreach ($qsql as $facrow) {
                                        $selected = ( $facrow['id'] == $row['invist_clinic_id'] ) ? 'selected="selected"' : '';
                                        echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Row 1 -->
                <!-- Row 2 -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group posrel">
                            <label><?php echo xlt('Item'); ?><sup class="required">*</sup></label>
                            <input type="text" class="formEle" id="itemStock" name="itemStock" value='' />
                            <img id="loading" style="display:none" src="../../images/loader.gif" />
                            <input type="hidden" class="formEle" id="itemId" name="itemid">
                            <input type="hidden" value="0" id="form_sale" name="form_sale">
                            <input type="hidden" value="0" id="form_expiry" name="form_expiry">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a id="addRequisition" href="javascript: void(0)" data-ajax="false" class="addWrpBtn" data-role="button">
                            <span class="glyphicon glyphicon-plus icon"></span>
                        </a>
                        <div class="form-group widthsml">
                            <label id='tofacilityLabel'><?php echo xlt('Quantity'); ?><sup class="required">*</sup></label>
                            <input type="hidden" class="formEle" id="form_qtyval" name="form_qtyval">
                            <input type='number' size='10' id='form_qty' name='form_qty' maxlength='7' onkeypress="return isNumber(event);" value='<?php echo attr($row['invist_quantity']) ?>' class='formEle' />
                        </div>
                    </div>
                </div>
                <!-- Row 2 -->
            </form>
        </div>
        <div id="requisitionListContainer" <?php if (count($reqItemsData) <= 0) { ?>style='display:none'<?php } ?>>
            <form method='post' id='requisitionListForm' name='requisitionListForm' onsubmit="" action='add_edit_requisition.php?reqid=<?php echo $reqid; ?>'>
                <div class='popupTableWrp'>
                    <table id='requisitionList' cellpadding='0' cellspacing='0' border='0' class='popupTable ui-table' width='100%'>
                        <thead>
                            <tr>
                                <th>Facility</th>
                                <th>To Facility</th>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th width='4%' class='txtCenter'>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($reqItemsData) > 0) {
                                $countItems = 0;
                                //ptint_r($reqItemsData);
                                foreach ($reqItemsData as $reqItemData) {
                                    if ($reqItemData['iri_isdeleted'] == 0) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $reqItemData['fromFacility']; ?>
                                                <input name='facilityVal[<?php echo $countItems ?>]' id='facilityVal[<?php echo $countItems ?>]' value='<?php echo $reqItemData['isr_from_clinic']; ?>' type="hidden">
                                            </td>
                                            <td>
                                                <?php echo $reqItemData['toFacility']; ?>
                                                <input name='tofacilityVal[<?php echo $countItems ?>]' id='tofacilityVal[<?php echo $countItems ?>]' value='<?php echo $reqItemData['isr_from_clinic']; ?>' type="hidden">
                                            </td>
                                            <td>
                                                <?php echo $reqItemData['inv_im_name']; ?>
                                                <input name='itemVal[<?php echo $countItems ?>]' id='itemVal[<?php echo $countItems ?>]' value='<?php echo $reqItemData['iri_itemid']; ?>' type="hidden">
                                            </td>
                                            <td>
                                                <?php echo $reqItemData['iri_quantity']; ?>
                                                <input name='quantity[<?php echo $countItems ?>]' id='quantity[<?php echo $countItems ?>]' value='<?php echo $reqItemData['iri_quantity']; ?>' type="hidden">
                                            </td>
                                            <td>
                                                <a class='listbtn deleteRequisition' data-index='<?php echo $countItems ?>' href="javascript:void(0)"><span class="dashboardDeleteIcon"></span>Delete</a>
                                                <input id='deleted[<?php echo $countItems ?>]' name='deleted[<?php echo $countItems ?>]' value='0' type="hidden">
                                                <input id='iri_id[<?php echo $countItems ?>]' name='iri_id[<?php echo $countItems ?>]' value='<?php echo $reqItemData['iri_id']; ?>' type="hidden">
                                            </td>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td class='strikeThrough'>
                                                <?php echo $reqItemData['fromFacility']; ?>
                                                <input name='facilityVal[<?php echo $countItems ?>]' id='facilityVal[<?php echo $countItems ?>]' value='<?php echo $reqItemData['isr_from_clinic']; ?>' type="hidden">
                                            </td>
                                            <td>
                                                <?php echo $reqItemData['toFacility']; ?>
                                                <input name='tofacilityVal[<?php echo $countItems ?>]' id='tofacilityVal[<?php echo $countItems ?>]' value='<?php echo $reqItemData['isr_from_clinic']; ?>' type="hidden">
                                            </td>
                                            <td class='strikeThrough'>
                                                <?php echo $reqItemData['inv_im_name']; ?>
                                                <input name='itemVal[<?php echo $countItems ?>]' id='itemVal[<?php echo $countItems ?>]' value='<?php echo $reqItemData['iri_itemid']; ?>' type="hidden">
                                            </td>
                                            <td class='strikeThrough'>
                                                <?php echo $reqItemData['iri_quantity']; ?>
                                                <input name='quantity[<?php echo $countItems ?>]' id='quantity[<?php echo $countItems ?>]' value='<?php echo $reqItemData['iri_quantity']; ?>' type="hidden">
                                            </td>
                                            <td>
                                                <input id='deleted[<?php echo $countItems ?>]' name='deleted[<?php echo $countItems ?>]' value='1' type="hidden">
                                                <input id='iri_id[<?php echo $countItems ?>]' name='iri_id[<?php echo $countItems ?>]' value='<?php echo $reqItemData['iri_id']; ?>' type="hidden">
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    $countItems++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <p style="text-align: center; margin-top: 20px;">
                    <input name="postval" type='hidden' value="1" /> 
                    <a id="subform" href="javascript:void(0)" class="save-btn"></a>
                    <input type='submit' data-role='none' class="btn dnone" id='form_save_req' name='form_save_req' value='<?php echo xla('Save'); ?>' />
                </p>
            </form>
        </div>
        <div id="placeholder">
            <img src='<?php echo $inventry_url; ?>emr/images/oi-wm-logo.jpg' class='autohgt' alt='' />
        </div>
        <script type="text/template" class="addEditRequisitionList">
            <% _.each( items, function(item,key,list){ %>
            <% if(parseInt(item.deleted) === 0){%>
            <tr>
            <td>
            <%=item.facility%>
            <input name='facilityVal[<%=key%>]' id='facilityVal[<%=key%>]' value='<%=item.facilityVal%>' type="hidden">
            </td>
            <td>
            <%=item.tofacility%>
            <input name='tofacilityVal[<%=key%>]' id='tofacilityVal[<%=key%>]' value='<%=item.tofacilityVal%>' type="hidden">
            </td>
            <!--<td>
            <%=item.category%>
            <input name='categoryVal[<%=key%>]' id='categoryVal[<%=key%>]' value='<%=item.categoryVal%>' type="hidden">
            </td>
            <td>
            <%=item.subCategory%>
            <input name='subCategoryVal[<%=key%>]' id='subCategoryVal[<%=key%>]' value='<%=item.subCategoryVal%>' type="hidden">
            </td>-->
            <td>
            <%=item.item%>
            <input name='itemVal[<%=key%>]' id='itemVal[<%=key%>]' value='<%=item.itemVal%>' type="hidden">
            </td>
            <td>
            <%=item.quantity%>
            <input name='quantity[<%=key%>]' id='quantity[<%=key%>]' value='<%=item.quantity%>' type="hidden">
            </td>
            <td class='txtCenter'>
            <span id='rightpanel'><a class='listbtn deleteRequisition' data-index='<%=item.requisitionIndex%>' href="javascript:void(0)"><span class="dashboardDeleteIcon"></span>Delete</a></span>
            <input id='deleted[<%=key%>]' name='deleted[<%=key%>]' value='0' type="hidden">

            <% if(item.reqItemId){ %>
            <input id='iri_id[<%=key%>]' name='iri_id[<%=key%>]' value='<%=item.reqItemId%>' type="hidden">
            <% } else { %>
            <input id='iri_id[<%=key%>]' name='iri_id[<%=key%>]' value='' type="hidden">
            <% } %>
            </td>
            </tr>
            <% }else{ %>
            <tr>
            <td class='strikeThrough'>
            <%=item.facility%>
            <input name='facilityVal[<%=key%>]' id='facilityVal[<%=key%>]' value='<%=item.facilityVal%>' type="hidden">
            </td>
            <td class='strikeThrough'>
            <%=item.tofacility%>
            <input name='tofacilityVal[<%=key%>]' id='tofacilityVal[<%=key%>]' value='<%=item.tofacilityVal%>' type="hidden">
            </td>
            <!--<td class='strikeThrough'>
            <%=item.category%>
            <input name='categoryVal[<%=key%>]' id='categoryVal[<%=key%>]' value='<%=item.categoryVal%>' type="hidden">
            </td>
            <td class='strikeThrough'>
            <%=item.subCategory%>
            <input name='subCategoryVal[<%=key%>]' id='subCategoryVal[<%=key%>]' value='<%=item.subCategoryVal%>' type="hidden">
            </td>-->
            <td class='strikeThrough'>
            <%=item.item%>
            <input name='itemVal[<%=key%>]' id='itemVal[<%=key%>]' value='<%=item.itemVal%>' type="hidden">
            </td>
            <td class='strikeThrough'>
            <%=item.quantity%>
            <input name='quantity[<%=key%>]' id='quantity[<%=key%>]' value='<%=item.quantity%>' type="hidden">
            </td>
            <td>
            <input id='deleted[<%=key%>]' name='deleted[<%=key%>]' value='<%=item.deleted%>' type="hidden">
            <% if(item.reqItemId){ %>
            <input id='iri_id[<%=key%>]' name='iri_id[<%=key%>]' value='<%=item.reqItemId%>' type="hidden">
            <% } else { %>
            <input id='iri_id[<%=key%>]' name='iri_id[<%=key%>]' value='' type="hidden">
            <% } %>
            </td>
            </tr>
            <% } %>
            <% }); %>
        </script>
        <script type='text/javascript'>
            var objRequisition = objRequisition || {},
            jsObj = <?php echo $reqItemsDataJson; ?>;
            objRequisition = {
                requisitionIndex: 0,
                requisitionArr: <?php echo $reqItemsDataJson; ?>,
                addButtonClick: function (e) {
                    if (submitform(e)) {
                        $("#facilityLabel").next().next('.custom-combobox').addClass('dnone');
                        $("#tofacilityLabel").next('.ui-select').hide();
                        $("#tofacilityidField").hide();

                        var facilityName = $('#facility option:selected').text(),
                            facilityValue = $('#facility').val();
                        $("#facilityId").val(facilityValue);
                        $("#facilityLabel").text(facilityName).show();

                        var tofacilityName = $('#tofacilityidField option:selected').text(),
                            tofacilityValue = $('#tofacilityidField').val();
                        $("#tofacilityId").val(tofacilityValue);
                        $("#tofacilityLabel").text(tofacilityName).show();

                        var itemVal = $('#itemStock').val();
                        $('#itemStock').val(itemVal);

                        var qtyVal = $('#form_qty').val();
                        $('#form_qtyval').val(qtyVal);

                        $('#requisitionListContainer').show();
                        var data = {
                            facility: facilityName,
                            facilityVal: facilityValue,
                            tofacility: tofacilityName,
                            tofacilityVal: tofacilityValue,
                            category: $('#catName').val(),
                            categoryVal: $('#catId').val(),
                            subCategory: $('#subCatName').val(),
                            subCategoryVal: $('#itemSubCatField').val(),
                            item: $('#itemStock').val(),
                            itemVal: $('#itemId').val(),
                            quantity: $('#form_qty').val(),
                            deleted: 0,
                            requisitionIndex: objRequisition.requisitionIndex
                        },
                        dataCheck = {
                            facility: facilityName,
                            facilityVal: facilityValue,
                            tofacility: tofacilityName,
                            tofacilityVal: tofacilityValue,
                            category: $('#catName').val(),
                            categoryVal: $('#catId').val(),
                            subCategory: $('#subCatName').val(),
                            subCategoryVal: $('#itemSubCatField').val(),
                            item: $('#itemStock').val(),
                            itemVal: $('#itemId').val(),
                        };
                        dataCheckRef = _.where(objRequisition.requisitionArr, dataCheck);

                        if (dataCheckRef.length) {
                            var arrIndex = dataCheckRef[0].requisitionIndex;
                            objRequisition.requisitionArr[arrIndex].quantity = $('#form_qty').val();
                            if (objRequisition.requisitionArr[arrIndex].deleted === 1) {
                                objRequisition.requisitionArr[arrIndex].deleted = 0;
                            }
                        }
                        else {
                            objRequisition.requisitionArr.push(data);
                            objRequisition.requisitionIndex++;
                        }

                        if ($("script.addEditRequisitionList").length > 0) {
                            var template = $("script.addEditRequisitionList").html();
                            var templateData = objRequisition.requisitionArr;
                            $("#requisitionList tbody").html(
                                    _.template(template, {
                                        items: templateData
                                    }));
                            document.getElementById('theform').reset();
                            $("#facility").val(facilityValue);
                            $("#facilityId").val(facilityValue);
                            $("#tofacilityidField").val(tofacilityValue);
                            $("#tofacilityId").val(tofacilityValue);
                        }
                    }
                },
                deleteButtonClick: function () {
                    var delIndex = $(this).attr('data-index');
                    objRequisition.requisitionArr[delIndex].deleted = 1;
                    $(this).parents('tr').css('text-decoration', 'line-through');
                    $(this).next().val('1');
                    $(this).parent().next().val('1');
                    $(this).remove();
                },
                saveButtonClick: function () {
                    var deletedItems = _.filter(objRequisition.requisitionArr, function (item) {
                        return item.deleted === 1;
                    });
                    if (!$('#requisitionList > tbody > tr').length) {
                        $('body').append('<div class="alert alert-warning alert-dismissable">Add a requisition to save</div>');
                        return false;
                    }
                    // else if (deletedItems.length) {
                    else if(deletedItems.length === objRequisition.requisitionArr.length) {
                        $('body').append('<div class="alert alert-danger alert-dismissable">Add an udeleted requisition to save</div>');
                        return false;
                    }
                    $('#requisitionListForm').submit(function () {
                        var winW = $(window).width() - $('#customLoading').width();
                        var dlbW = winW/2;
                        var winH = $(window).height()/2;
                        var dlbH = winH - $('#customLoading').height();
                        $(document).find('.ui-widget-overlay').css('z-index', '9000').show();
                        $(document).find('#customLoading').css({'left': dlbW, 'top': dlbH, 'margin': '0'}).show();
                        $('#form_save_req').attr("disabled", true);
                    });

                },
                requisitionLoad: function () {
                    if (objRequisition.requisitionArr.length && $("script.addEditRequisitionList").length) {
                        var template = $("script.addEditRequisitionList").html(),
                                templateData = objRequisition.requisitionArr,
                                facilityLabel = objRequisition.requisitionArr[0].facility,
                                facilityVal = objRequisition.requisitionArr[0].facilityVal,
                                toFacilityLabel = objRequisition.requisitionArr[0].tofacility,
                                toFacilityVal = objRequisition.requisitionArr[0].tofacilityVal;
                        $("#requisitionList tbody").html(
                                _.template(template, {
                                    items: templateData
                                }));

                        //From facility settings
                        $("#facility").val(facilityVal).css('visibility', 'hidden');
                        $("#facility").val(facilityVal);
                        $("#facilityLabel").text(facilityLabel);
                        $("#facilityId").val(facilityVal);

                        //To facility settings
                        $("#tofacilityidField").val(toFacilityVal).css('visibility', 'hidden');
                        $("#tofacilityidField").val(toFacilityVal);
                        $("#tofacilityLabel").text(toFacilityLabel);
                        $("#tofacilityId").val(toFacilityVal);
                    }
                }
            }

            //Loading data in table at the bottom in edit case
            objRequisition.requisitionLoad();

            //Click of add button
            $('#addRequisition').click(objRequisition.addButtonClick);

            //Save button click
            $('#form_save_req').click(objRequisition.saveButtonClick);

            //Click of delete link
            $(document).on("click", ".deleteRequisition", objRequisition.deleteButtonClick);
        </script>
    </body>
</html>