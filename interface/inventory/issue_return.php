<?php
// Copyright (C) 2006-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

$sanitize_all_escapes = true;
$fake_register_globals = false;

require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("$srcdir/acl.inc");
require_once("drugs.inc.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/htmlspecialchars.inc.php");
//$datePhpFormat = getDateDisplayFormat(0);
// Check authorization.
if (!$invgacl->acl_check('inventory','invf_item_issue_list','users', $_SESSION['authUser']))
   die(xlt('Not authorized'));
//$thisauth = acl_check('inventory', 'invf_item_issue_list');
//if (!$thisauth)
//    die(xlt('Not authorized'));

/* if (isset($_GET["mode"])) {
  if ($_GET["mode"] == "delete") {
  if(checkUomStatus($_GET["itemid"])){
  sqlStatement("update inv_item_master set inv_im_deleted='1' where inv_im_id = '" . $_GET["itemid"] . "'");
  $_SESSION['alertmsg'] = 'Item deleted successfully.';
  header('Location: items.php');
  exit;
  }else {
  $_SESSION['alertmsg'] = 'You should not able delete selected item.';
  header('Location: items.php');
  exit;
  }

  }
  if ($_GET["mode"] == "activate") {
  sqlStatement("update inv_item_master set inv_im_status='1' where inv_im_id = '" . $_GET["itemid"] . "'");
  $_SESSION['alertmsg'] = 'Item activated successfully.';
  header('Location: items.php');
  exit;
  }
  if ($_GET["mode"] == "deactivate") {
  sqlStatement("update inv_item_master set inv_im_status='0' where inv_im_id = '" . $_GET["itemid"] . "'");
  $_SESSION['alertmsg'] = 'Item deactivated successfully.';
  header('Location: items.php');
  exit;
  }
  } */
// For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
    'invir_id' => 'invir_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[urldecode($_REQUEST['form_orderby'])] ? urldecode($_REQUEST['form_orderby']) : 'invir_id';
$orderby = $ORDERHASH[$form_orderby];
//$facility  = $_POST['facility'];

if(isset($_REQUEST['facility'])){
     $_SESSION['cid']=$_REQUEST['facility'];
}else{
   $_REQUEST['facility']= $_SESSION['cid']; 
}
$facility = isset($_REQUEST['facility'])? urldecode($_REQUEST['facility']) : $_SESSION['Auth']['User']['facility_id'];
$form_item = urldecode($_REQUEST['form_item']);
$from_date = urldecode($_REQUEST['form_from_date']);
$to_date = urldecode($_REQUEST['form_to_date']);

// get drugs
/* echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " .
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby"; */
?>
<html>
    <head>
        <?php html_header_show(); ?>
        <title><?php echo xlt('Inventory Internal Issue and Return'); ?></title>
        <?php include_once("themestyle.php"); ?>
        <?php include_once("scriptcommon.php"); ?>
    </head>
    <body>
        <!-- forGlobalMessages -->
        <?php include_once("inv_messages.php"); ?>
        <!-- forGlobalMessages -->
        <!-- page -->
        <div id="page" data-role="page" class="ui-content">
            <!-- header -->
            <?php include_once("oi_header.php"); ?>
            <!-- header -->
            <!-- contentArea -->
             <div id="wrapper" data-role="content" role="main">
                <!-- wrapper -->
                <div class='themeWrapper' id='rightpanel'>
                    <div class='containerWrap'>
                        <!-- pageheading -->
                        <div class='col-sm-12 borbottm'>
                            <?php include_once("inv_links.html"); ?>
                            <h1><?php xl('Item Issue/Return', 'e'); ?></h1>
                        </div>
                        <!-- pageheading -->
                        <!-- mdleCont -->
                        <form method='get' action='issue_return.php' name='theform' id='theform' class="botnomrg">
                           <div class="filterWrapper">
                                <!-- first column starts --> 
                                <div class="ui-block">
                                    <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('facility', '', 'facility', $facility, $_SESSION['authId'],$userFacilityRestrict,$pdoobject);
                                    ?>
                                </div>
                                <!-- first column ends -->
                                <!-- fifth column starts -->
                                <div class="ui-block">
                                    <input type='text' placeholder='Item' name='form_item' id="form_item" value='<?php echo $form_item ?>' title='' />
                                </div>
                                <!-- fifth column ends -->
                                <!-- second column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $from_date; ?>' />
                                </div>
                                <!-- second column ends --> 
                                <!-- third column starts -->
                                <div class="ui-block form_to_date_bx">
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $to_date; ?>' />
                                </div>
                                <!-- third column ends -->
                                <!-- fourth column starts -->
                                <div class="ui-block wdth20">                                                                        
                                    <a class="pull-right btn_bx" id='reset_form1' href="issue_return.php?facility=<?php echo $_SESSION['reset_cid']; ?>">
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-retweet icon"></span>
                                        </span>
                                        <b class="btn-text">Reset</b>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" onclick='$("#form_refresh").attr("value", "true");
                                            $("#theform").submit();'>
                                        <span class="new-btnWrap btn">
                                            <span class="glyphicon glyphicon-search icon5"></span>
                                        </span>
                                        <b class="btn-text">Search</b>
                                    </a>
                                </div>
                                <!-- fourth column ends -->
                            </div>
                            <!-- tableData -->
                            <!-- <table border='0' cellpadding='1' cellspacing='2' width='100%'>
                                <?php
                                //if (!empty($_SESSION['alertmsg'])) {
                                    ?>
                                    <tr >
                                        <td colspan="2"><?php //echo $_SESSION['alertmsg']; $_SESSION['alertmsg'] = "";
                                    ?></td>		
                                    </tr>
                                    <?php
                                //}
                                ?>
                            </table> -->
                            <?php
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_REQUEST['facility'] || $_REQUEST['form_from_date'] || $_REQUEST['form_to_date'] || $_REQUEST['form_item'] || empty($facility)) {
                                $sql = "SELECT iir.invir_id, iir.invir_item_id, iir.invir_stock_id, iir.invir_quantity, iir.invir_return_qty, iir.invir_cons_qty, iir.invir_issuee_id, iir.invir_issuer_id, iir.invir_facility_id, iir.invir_issue_date, iir.invir_return_date, ist.invist_id, im.inv_im_name, iseu.nickname AS iseName, isru.nickname AS isrName, fa.name as facility_name
                                        FROM inv_issue_return AS iir
                                        INNER JOIN users AS iseu ON iseu.id = iir.invir_issuee_id
                                        INNER JOIN users AS isru ON isru.id = iir.invir_issuer_id
                                        INNER JOIN inv_item_stock AS ist ON ist.invist_id = iir.invir_stock_id
                                        INNER JOIN inv_item_master AS im ON im.inv_im_id = ist.invist_itemid
                                        INNER JOIN facility As fa ON  fa.id = iir.invir_facility_id
                                        WHERE ist.invist_isdeleted = '0' AND im.inv_im_deleted = '0' ";
                                if(!$invgacl->acl_check('inventory','invf_issued_item_return_consume_accept_permission','users',$_SESSION['authUser']))
                                {   
                                    //$sql.= "AND (iir.invir_issuee_id = " . $_SESSION['authId'] . " || iir.invir_issuer_id = " . $_SESSION['authId'] . ")";
                                }
                                if (!empty($facility)) { // if facility exists
                                    $sql .= "AND iir.invir_facility_id = '" . $facility . "'";
                                } else {
                                    $sql .= " AND iir.invir_facility_id IN(" . getLoggedUserAssignedClinics() . ")";
                                }
                                if (!empty($from_date) && !empty($to_date)) { // If from dates exists
                                    $sql .= "AND iir.invir_issue_date BETWEEN '$from_date 00:00:00' AND '$to_date 23:59:59'";
                                }
                                if (!empty($to_date) && empty($from_date)) { // If to dates exists
                                    $sql .= " AND " . "iir.invir_issue_date >= '$from_date 00:00:00' ";
                                }
                                if (empty($to_date) && !empty($from_date)) { // If to dates exists
                                    $sql .= " AND " . "iir.invir_issue_date <= '$to_date 23:59:59' ";
                                }
                                if (!empty($form_item)) { // If to dates exists
                                    $sql .= " AND im.inv_im_name like'%" . $form_item . "%'";
                                }
                                $sql .= " ORDER BY $orderby";
                                //$sql_num = sqlStatement($sql);
                                //$num_rows = sqlNumRows($sql_num); // total no. of rows
                                $num_rows = $pdoobject->custom_query($sql,NULL,1);
                                $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                //$per_page=2;
                                $page = $_GET["page"];
                                $page = ($page == 0 ? 1 : $page);
                                $page_start = ($page - 1) * $per_page;
                                $curr_URL = $_SERVER[SCRIPT_NAME] . '?facility=' . $facility . '&form_item=' . urldecode($_REQUEST['form_item']). '&form_refresh=' . urldecode($_REQUEST['form_refresh']) . '&form_to_date=' . urldecode($_REQUEST['form_to_date']) . '&form_from_date=' . urldecode($_REQUEST['form_from_date']) . '&';
                                $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                $sql .= " LIMIT $page_start , $per_page";
                                //$res = sqlStatement($sql);
                                $res = $pdoobject->custom_query($sql);
                                ?>
                                <div id="" class='tableWrp pb-2'>
                                    <!-- pagination -->
                                    <?php
                                    if ($num_rows > 0) {
                                        echo $pa_data;
                                    }
                                    ?>
                                    <!-- pagination -->
                                    <div class='dataTables_wrapper no-footer'>     
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                                <?php if ($num_rows > 0) { ?>
                                                    <thead>
                                                        <tr>
                                                            <?php if(empty($facility)) {?>
                                                            <th>   
                                                                 <?php echo xlt('Clinic Name'); ?>
                                                            </th><?php }?>
                                                            <th width="18%">   
                                                                <?php echo xlt('Item Name'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Issued Qty.'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Return Qty.'); ?>
                                                            </th>
                                                            <th width="12%">
                                                                <?php echo xlt('Consumable Qty.'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Issuer'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Issuee'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Issue Date'); ?>
                                                            </th>
                                                            <th>
                                                                <?php echo xlt('Last Update'); ?>
                                                            </th>
                                                            <th width="15%" class='txtCenter'>
                                                                <?php echo xlt('Action'); ?>
                                                            </th> 
                                                        </tr>
                                                    </thead>
                                                <?php } ?>
                                                <?php
                                                $lastid = "";
                                                $prevClinic = '';
                                                $currClinic = '';
                                                $currItem = '';
                                                $prevItem = '';
                                                $encount = 0;
                                                if ($num_rows) {
                                                    foreach ($res As $row) { 
                                                        ++$encount;
                                                        //$bgcolor = "#" . (($encount & 1) ? "f7d4be" : "f1f1f1");
                                                        $lastid = $row['invir_id'];
                                                        $currClinic = $row['invir_facility_id'];
                                                        $currItem = $row['invir_item_id'];
                                                        if (empty($facility)) {
                                                             if($currClinic!=$prevClinic){
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $row['facility_name']; ?></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                             <?php }?>
                                                            <tr>
                                                                
                                                                    <td></td>
                                                                
                                                                <?php 
                                                                    
                                                                if($currClinic!=$prevClinic){
                                                                    ?>
                                                                    <td><?php echo $row['inv_im_name']; ?></td>
                                                                     
                                                                <?php }else{
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }}?> 
                                                                       
                                                                <td><?php echo $row['invir_quantity']; ?></td>
                                                                <td><?php echo $row['invir_return_qty']; ?></td>
                                                                <td><?php echo $row['invir_cons_qty']; ?></td>
                                                                <td><?php echo $row['isrName']; ?></td>
                                                                <td><?php echo $row['iseName']; ?></td>
                                                                <td><?php echo invDateFormat($row['invir_issue_date']); ?></td>
                                                                <td><?php echo invDateFormat($row['invir_return_date']) ; ?></td>
                                                                <td><?php 
                                                                    echo "<div class='listbtn viewTableDataList' data-pageUrl='view_issue_reutrn.php?issueid=" . attr($lastid) . "&facility=" . attr($row['invir_facility_id']) . "'><span class='dashboardViewIcon'></span>View</div> &nbsp;";
                                                        if ($row['invir_quantity'] > ($row['invir_return_qty'] + $row['invir_cons_qty']) && $row['invir_issuee_id'] == $_SESSION['authId'] && $invgacl->acl_check('inventory', 'invf_issued_item_return_consume','users', $_SESSION['authUser'])) {
                                                            echo "<div style='width: auto;' class='listbtn addEditDataISR' data-pageUrl='add_edit_return.php?issueid=" . attr($lastid) . "&facility=" . attr($row['invir_facility_id']) . "'><span class='dashboardEditIcon'></span>Consume / Return</div>";
                                                        }
                                                                ?></td>
                                                               
                                                            </tr>
                                                        <?php } else { ?>
                                                            <tr>
                                                               
                                                                 <?php 
                                                                    
                                                               
                                                                     if($currItem != $prevItem){
                                                                    ?>
                                                                       <td><?php echo $row['inv_im_name']; ?></td>
                                                                       <?php }else{?>  
                                                                       <td>&nbsp;</td>
                                                                <?php }//}?>
                                                                <td><?php echo $row['invir_quantity']; ?></td>
                                                                <td><?php echo $row['invir_return_qty']; ?></td>
                                                                <td><?php echo $row['invir_cons_qty']; ?></td>
                                                                <td><?php echo $row['isrName']; ?></td>
                                                                <td><?php echo $row['iseName']; ?></td>
                                                                <td><?php echo invDateFormat($row['invir_issue_date']); ?></td>
                                                                <td><?php echo invDateFormat($row['invir_return_date']) ; ?></td>
                                                                <td><?php echo "<div class='listbtn viewDataList' data-pageUrl='view_issue_reutrn.php?issueid=" . attr($lastid) . "&facility=" . attr($row['invir_facility_id']) . "'><span class='dashboardViewIcon'></span>View</div> &nbsp;";
                                                        if ($row['invir_quantity'] > ($row['invir_return_qty'] + $row['invir_cons_qty']) &&  $row['invir_issuee_id'] == $_SESSION['authId'] &&  $invgacl->acl_check('inventory', 'invf_issued_item_return_consume','users', $_SESSION['authUser'])) {
                                                            echo "<div style='width: auto;' class='listbtn addEditDataISR' data-pageUrl='add_edit_return.php?issueid=" . attr($lastid) . "&facility=" . attr($row['invir_facility_id']) . "'><span class='dashboardEditIcon'></span>Consume / Return</div>";
                                                        }
                                                                ?></td>
                                                               
                                                            </tr>
                                                            <?php
                                                        }
                                                        $prevClinic = $row['invir_facility_id'];
                                                        $prevItem = $row['invir_item_id'];
                                                    } // end while
                                                } else {
                                                    ?>
                                                    <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                <?php } ?>
                                            </table>
                                    </div>
                                    <!-- pagination -->
                                    <?php
                                    if ($num_rows > 0) {
                                        echo $pa_data;
                                    }
                                    ?>
                                    <!-- pagination -->
                                </div>
                                <?php } else { ?>
                                <div class='text leftmrg6 dnone'>
                                    <?php echo xl('* Please select a filter to see the results.', 'e'); ?>
                                </div>
                                    <?php } ?>
                            <input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />
                            <input type='hidden' name='form_refresh' id='form_refresh' value='' />
                            <!-- tableData -->
                        </form>
                        <!-- mdleCont -->
                    </div>
                </div>
                <!-- wrapper -->
            </div>
            <!-- contentArea -->	
        </div>
        <!-- page -->
    </body>
</html>