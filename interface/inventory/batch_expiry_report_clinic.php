<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Check authorization.
$thisauth = acl_check('inventory', 'icr_batch_exp');
if (!$thisauth)
    die(xlt('Not authorized'));

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$expiry_date = fixDate($_POST['form_expiry_date'], '');
$show_available_times = false;
$ORDERHASH = array(
    'invist_id' => 'invist_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invist_id';
$orderby = $ORDERHASH[$form_orderby];

$form_facility = $_POST['form_facility'];
$form_date = $_POST['form_expiry_date'];
$form_to_date = $_POST['to_expiry_date'];
$provider = $_POST['form_provider'];
$batch = $_POST['form_batch'];
$item = $_POST['form_item'];
$catid = $_POST['catid'];
$subcatid = $_POST['subcatid'];
$sellable = $_POST['form_sellable'];

$searchParam = '';

// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvBatchExpiry') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=batch_expiry_leadger.csv");
    header("Content-Description: File Transfer");
} else {
    ?>

    <html>
        <head>
    <?php html_header_show(); ?>
            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
            <link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
            <title><?php xl('Batch/Expiry', 'e'); ?></title>
            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

            <script type="text/javascript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
                function dosort(orderby) {
                    var f = document.forms[0];
                    f.form_orderby.value = orderby;
                    f.submit();
                    return false;
                }
                function oldEvt(eventid) {
                    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
                }
                function refreshme() {
                    document.forms[0].submit();
                }
                $(document).ready(function () {
                    var catid = $("#catid").val();
                    
                    if(catid != '' ){
                       var subCat = '<?php echo $_REQUEST['subcatid'] ?>';                  
                        $.ajax({
                                type: 'GET',
                                url: "get_item_subcat.php?catid=" + catid +"&subCat=" + subCat,
                                success: function (data) {
                                    $("#itemSubCatField").html(data); 
                                    $('#itemSubCatField').val(subCat);
                                }
                            });

                    }  
                    //Get Sub-Category based on Category
                    $("#catid").change(function () {
                        var catid = $("#catid").val();
                         var subCat = '<?php echo $_REQUEST['subcatid'] ?>';
                        $.ajax({
                            type: 'GET',
                            url: "get_item_subcat.php?catid=" + catid,
                            success: function (data) {
                                $("#itemSubCatField").html(data);
                                $('#itemSubCatField').val(subCat);
                            }
                        });
                    });
                });
            </script>
            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12 boldtxt"><?php xl('Report', 'e'); ?> - <?php xl('Batch/Expiry', 'e'); ?></div>
                    </div>
                </div>
                <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_expiry_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_expiry_date)); ?>
                </div>
                <div class="panel-body">
                    <form method='post' name='theform' id='theform' action='batch_expiry_report_clinic.php'>
                        <div id="report_parameters" class="searchReport">
                            <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                            <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                            <div class="row">
                                <div class="col-md-3">
                                        <?php xl('Facility', 'e'); ?>:
                                    <select name="form_facility" id="form_facility" class="form-control input-sm" >
                                        <?php
                                        if (empty($row['invist_clinic_id'])) {
                                            ?>
                                            <option value='0' selected="selected">Select Facility</option>
                                            <?php
                                        }
                                        $countUserFacilities = 0;
                                        if (!$GLOBALS['restrict_user_facility']) {
                                            $qsql = sqlStatement("
                                                         select id, name, color
                                                         from facility
                                                         where service_location != 0
                                                         ");
                                        } else {
                                            $qsql = sqlStatement("
                                                       select uf.facility_id as id, f.name, f.color
                                                       from users_facility uf
                                                       left join facility f on (uf.facility_id = f.id)
                                                       where uf.tablename='users' 
                                                       and uf.table_id = ? 
                                                       ", array($_SESSION['authId']));
                                        }
                                        while ($facrow = sqlFetchArray($qsql)) {
                                            $selected = ( $facrow['id'] == $form_facility ) ? 'selected="selected"' : '';
                                            echo "<option value='" . attr($facrow['id']) . "' $selected>" . text($facrow['name']) . "</option>";
                                            $countUserFacilities++;
                                            /*                                             * ********************************************************* */
                                        }
                                        ?>      
                                    </select>
                                </div>
                                <div class="col-md-3">
                                        <?php xl('Category', 'e'); ?>:
                                    <select name='catid' id='catid' class="form-control input-sm">
                                        <?php
                                        if (empty($row['inv_im_catId'])) {
                                            ?>
                                            <option value='0' selected="selected">All Category</option>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        $catSql = "SELECT id as invcat_id, name as invcat_name FROM invcategories WHERE is_active='1' AND deleted='0' ";
                                        $cres = sqlStatement($catSql, null, $GLOBALS['adodb']['dbreport']);
                                        if (sqlNumRows($cres)) {
                                            while ($crow = sqlFetchArray($cres)) {
                                                ?>
                                                <option value='<?php echo $crow['invcat_id'] ?>' <?php
                                                if ($_POST['catid'] == $crow['invcat_id']) {
                                                    echo 'selected="selected"';
                                                }
                                                ?>><?php echo $crow['invcat_name'] ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                        <?php xl('Sub-Category', 'e'); ?>:
                                    <select name='subcatid' id="itemSubCatField" class="form-control input-sm">
                                        <?php
                                        if (!empty($row['inv_im_catId'])) {
                                            if (empty($row['inv_im_subcatId'])) {
                                                ?>
                                                <option value='0' selected="selected">All Sub Category</option>         
                                                <?php
                                            }
                                            $scatSql = "SELECT id as invsubcat_id, name as invsubcat_name FROM invcategories WHERE is_active='1' AND deleted='0' AND id = " . $row['inv_im_catId'];
                                            $scres = sqlStatement($scatSql, null, $GLOBALS['adodb']['dbreport']);
                                            if (sqlNumRows($scres)) {
                                                while ($scrow = sqlFetchArray($scres)) {
                                                    ?>
                                                    <option value='<?php echo $scrow['invsubcat_id'] ?>' <?php
                                                    if ($_POST['subcatid'] == $scrow['invsubcat_id']) {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?>><?php echo $scres['invsubcat_name'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                } else {
                                                    ?>
                                            <option value='0' selected="selected">All Sub Category</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
    <?php xl('Item', 'e'); ?>:
                                    <input type='text' class="form-control input-sm" name='form_item' id="form_item" size='20' value='<?php echo ($item !='')?$item:'' ;?>' title=''>
                                </div>
                            </div>
                            <div class="row" style="padding-top:10px;">
                                <div class="col-md-3">
    <?php xl('Expiry From', 'e') ?><br><input type='text' name='form_expiry_date' id="form_expiry_date"
                                               size='24' value='<?php echo $form_date ?>'
                                               onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                               title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                               align='absbottom' width='24' height='22' id='img_expiry_form_date'
                                               border='0' alt='[?]' style='cursor: pointer'
                                               title='<?php xl('Click here to choose a date', 'e'); ?>'/> 
                                </div>
                                <div class="col-md-3">  
    <?php xl('Expiry To', 'e') ?>:<br><input type='text' name='to_expiry_date' id="to_expiry_date"
                                                size='25' value='<?php echo $form_to_date ?>'
                                                onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                align='absbottom' width='24' height='22' id='img_expiry_to_date'
                                                border='0' alt='[?]' style='cursor: pointer'
                                                title='<?php xl('Click here to choose a date', 'e'); ?>'/> 
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
    <?php xl('Batch#', 'e'); ?>:
                                        <input type='text' class="form-control input-sm" name='form_batch' id="form_batch" size='20' value='<?php echo ($batch !='')?$batch:'' ;?>' title=''>
                                    </div>
                                </div>
                                <div class="col-md-3" style="margin-top: 20px;">
    <?php xl('Saleable', 'e'); ?>:
                                    <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_POST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewBatchExpiry");
                                            $("#theform").submit();'>
                                        <span> <?php xl('Submit', 'e'); ?> </span> </a> 


                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvBatchExpiry");
                                                            $("#theform").submit();'>
                                        <span>
    <?php xl('Export to CSV', 'e'); ?>
                                        </span>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <!-- end of search parameters --> <?php
                    } // end not form_csvexport
                    if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
                        if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvBatchExpiry') {
                            // CSV headers:
                            echo '"' . xl('S. No.') . '",';
                            echo '"' . xl('Clinic Name') . '",';
                            echo '"' . xl('Item Name') . '",';
                            echo '"' . xl('Batch#') . '",';
                            echo '"' . xl('Expiry Date') . '",';
                            echo '"' . xl('Price') . '",';
                            echo '"' . xl('Quantity') . '",';
                            echo '"' . xl('Transaction Date') . '"' . "\n";
                        } else {
                            ?>
                            <br>
                            <div id="report_results">
                                <table class="table table-bordered table-striped" cellPadding='5' cellSpacing='0' border='0'>
                                    <tr class='head'>
                                    <thead>
                                    <th><?php xl('S.N.', 'e'); ?></th>
                                    <th><?php xl('Clinic Name', 'e'); ?></th>
                                    <th><?php xl('Item Name', 'e'); ?></th>
                                    <th><?php xl('Batch#', 'e'); ?></th>
                                    <th><?php xl('Expiry Date', 'e'); ?></th>
                                    <th align='right'><?php xl('Price', 'e'); ?></th>
                                    <th align='right'><?php xl('Quantity', 'e'); ?></th>
                                    <th><?php xl('Transaction Date', 'e'); ?></th>
                                    </thead>
                                    <tbody>
                                        <!-- added for better print-ability -->
                                        <?php
                                    }

                                    if ($form_facility) {
                                        //for individual facility
                                        if ($form_date) {
                                            //if from date selected
                                            if ($form_to_date) {
                                                // if $form_date && $form_to_date
                                                $toDate = date_create(date($form_to_date));
                                                $fromDate = date_create($form_date);
                                                $diff = date_diff($fromDate, $toDate);
                                                $days_between_from_to = $diff->days;


                                                if ($days_between_from_to <= $form_facility_time_range) {

                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= "  invis.invist_clinic_id = '$form_facility'";
                                                } else {
                                                    if ($days_between_from_to <= $form_facility_time_range_valid) {
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                        // following value will be change according to report
                                                        $rcsl_name_type = "batch_expiry_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "batch" => $batch, "facility" => $form_facility))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from $form_date to $form_to_date"; // changable
                                                        //allFacilityReports() defined with globals.php
                                                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                        if ($msgForReportLog) {
                                                            $msgForReportLog = $msgForReportLog;
                                                        } else {
                                                            $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                        }
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                    } else {
                                                        $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                                                    }
                                                }
                                            } else {
                                                //only from date: no TO date
                                                $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                                                $toDate = date_create(date($form_to_date));
                                                $fromDate = date_create($form_date);
                                                $diff = date_diff($fromDate, $toDate);
                                                $days_between_from_to = $diff->days;

                                                if ($days_between_from_to <= $form_facility_time_range) {
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_clinic_id = '$form_facility'";
                                                } else {
                                                    if ($days_between_from_to <= $form_facility_time_range_valid) {
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                        // following value will be change according to report
                                                        $rcsl_name_type = "batch_expiry_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "batch" => $batch, "facility" => $form_facility))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from $form_date to $form_to_date"; // changable
                                                        //allFacilityReports() defined with globals.php
                                                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                        if ($msgForReportLog) {
                                                            $msgForReportLog = $msgForReportLog;
                                                        } else {
                                                            $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                        }
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                    } else {
                                                        $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                                                    }
                                                }
                                            }
                                        } else {
                                            // if from date not selected
                                            if ($form_to_date) {
                                                $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                                $toDate = date_create(date($form_to_date));
                                                $fromDate = date_create($form_date);
                                                $diff = date_diff($fromDate, $toDate);
                                                $days_between_from_to = $diff->days;

                                                if ($days_between_from_to <= $form_facility_time_range) {
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_clinic_id = '$form_facility'";
                                                } else {
                                                    if ($days_between_from_to <= $form_facility_time_range_valid) {
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                        // following value will be change according to report
                                                        $rcsl_name_type = "batch_expiry_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "batch" => $batch, "facility" => $form_facility))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from $form_date to $form_to_date"; // changable
                                                        //allFacilityReports() defined with globals.php
                                                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                        if ($msgForReportLog) {
                                                            $msgForReportLog = $msgForReportLog;
                                                        } else {
                                                            $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                                                        }
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                    } else {
                                                        $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                                                    }
                                                }
                                            } else {
                                                if ($where) {
                                                    $where .= " AND ";
                                                }
                                                $form_to_date = date("Y-m-d");
                                                $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                                $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                if ($where) {
                                                    $where .= " AND ";
                                                }
                                                $where .= " invis.invist_clinic_id = '$form_facility'";
                                            }
                                        }

                                        $searchParam .= ' Expiry Date From  = ' . $form_date . ' | Expiry Date To = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param  
                                    } else {
                                        //for all facility
                                        if ($form_date) {
                                            //if from date selected
                                            if ($form_to_date) {
                                                // if $form_date && $form_to_date
                                                $toDate = date_create(date($form_to_date));
                                                $fromDate = date_create($form_date);
                                                $diff = date_diff($fromDate, $toDate);
                                                $days_between_from_to = $diff->days;

                                                if ($days_between_from_to <= $form_facility_all_time_range) {

                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                } else {
                                                    if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                        // following value will be change according to report
                                                        $rcsl_name_type = "batch_expiry_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "batch" => $batch))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from $form_date to $form_to_date"; // changable
                                                        //allFacilityReports() defined with globals.php
                                                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                        if ($msgForReportLog) {
                                                            $msgForReportLog = $msgForReportLog;
                                                        } else {
                                                            $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                        }
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                    } else {
                                                        $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                                                    }
                                                }
                                            } else {
                                                //only from date: no TO date
                                                $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                                                $toDate = date_create(date($form_to_date));
                                                $fromDate = date_create($form_date);
                                                $diff = date_diff($fromDate, $toDate);
                                                $days_between_from_to = $diff->days;

                                                if ($days_between_from_to <= $form_facility_all_time_range) {
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                } else {
                                                    if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                        // following value will be change according to report
                                                        $rcsl_name_type = "batch_expiry_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "batch" => $batch))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from $form_date to $form_to_date"; // changable
                                                        //allFacilityReports() defined with globals.php
                                                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                        if ($msgForReportLog) {
                                                            $msgForReportLog = $msgForReportLog;
                                                        } else {
                                                            $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                        }
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                    } else {
                                                        $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                                                    }
                                                }
                                            }
                                        } else {
                                            // if from date not selected
                                            if ($form_to_date) {
                                                $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                                $toDate = date_create(date($form_to_date));
                                                $fromDate = date_create($form_date);
                                                $diff = date_diff($fromDate, $toDate);
                                                $days_between_from_to = $diff->days;

                                                if ($days_between_from_to <= $form_facility_all_time_range) {
                                                    if ($where) {
                                                        $where .= " AND ";
                                                    }
                                                    $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                                } else {
                                                    if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                        // following value will be change according to report
                                                        $rcsl_name_type = "batch_expiry_report"; // changable
                                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "subcatid" => $subcatid, "batch" => $batch))); // changable
                                                        $rcsl_report_description = "Batch/Expiry report from $form_date to $form_to_date"; // changable
                                                        //allFacilityReports() defined with globals.php
                                                        $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                                        if ($msgForReportLog) {
                                                            $msgForReportLog = $msgForReportLog;
                                                        } else {
                                                            $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                                                        }
                                                        ##########Cron Request####################section only for all FACILITY#########################
                                                    } else {
                                                        $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                                                    }
                                                }
                                            } else {
                                                if ($where) {
                                                    $where .= " AND ";
                                                }
                                                $form_to_date = date("Y-m-d");
                                                $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                                $where .= " invis.invist_expiry >= '$form_date' AND invis.invist_expiry <= '$form_to_date'";
                                            }
                                        }

                                        $searchParam .= ' Expiry Date From = ' . $form_date . ' | Expiry Date To = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param  
                                    }


                                    if (isset($provider) && $provider != '') {
                                        $where .= " AND invistr.invistr_createdby = '" . $provider . "'";
                                        $searchParam .= ' Provider = ' . $provider . ' | ';
                                    } else {
                                        $searchParam .= ' Provider = All | ';
                                    }

                                    if (isset($batch) && $batch != '') {
                                        $where .= " AND invis.invist_batch = '" . $batch . "'";

                                        $searchParam .= ' Batch# = ' . $batch . ' | ';
                                    }
                                    if (isset($item) && $item != '') {
                                        $where .= " AND im.inv_im_name LIKE '" . $item . "%'";
                                        $searchParam .= ' Item = ' . $item . ' | ';
                                    }
                                    if (isset($catid) && $catid != 0) {
                                        $where .= " AND im.inv_im_catId = '" . $catid . "'";
                                        $searchParam .= ' Category = ' . $catid . ' | ';
                                    } else {
                                        $searchParam .= ' Category = All | ';
                                    }
                                    if (isset($subcatid) && $subcatid != 0) {
                                        $where .= " AND im.inv_im_subcatId = '" . $subcatid . "'";
                                        $searchParam .= ' Sub-Category = ' . $subcatid . ' | ';
                                    } else {
                                        $searchParam .= ' Sub-Category = All | ';
                                    }

                                    if (isset($sellable) && $sellable != 0 && !isset($sellable)) {
                                        $where .= " AND im.inv_im_sale = '" . $sellable . "'";
                                        $searchParam .= ' Saleable Items = Yes | ';
                                    } else {
                                        $searchParam .= ' Saleable Items = No | ';
                                    }


                                    if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvBatchExpiry') {
                                        $event = "Report Batch/Expiry Export";
                                    } else {
                                        $event = "Report Batch/Expiry View";
                                    }

                                    $dateMysqlFormat = getDateDisplayFormat(1);


                                    $query = "SELECT invis.invist_id, invis.invist_itemid, invis.invist_batch, invis.invist_expiry, DATE_FORMAT(invis.invist_expiry , '" . $dateMysqlFormat . "') AS formatexpirydate,
                                invis.invist_price, invis.invist_quantity, im.inv_im_name, f.name AS clinicname, invis.invist_created_date, DATE_FORMAT(invis.invist_created_date , '" . $dateMysqlFormat . "') AS formatcreateddate
                                FROM inv_item_stock AS invis
                                INNER JOIN facility AS f ON f.id = invis.invist_clinic_id
                                INNER JOIN inv_item_master AS im ON im.inv_im_id = invis.invist_itemid
                                WHERE invis.invist_isdeleted = '0' AND im.inv_im_deleted = '0' AND " . $where . " ORDER BY $orderby";

                                    
                                    if ($msgForReportLog) {

                                        $auditid = debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                                    } else {

                                        $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                                        $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);

                                        $lastid = "";
                                        $srCount = 1;
                                        if (sqlNumRows($res)) {
                                            while ($row = sqlFetchArray($res)) {
                                                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvBatchExpiry') {
                                                    echo '"' . qescape($srCount) . '",';
                                                    echo '"' . qescape($row['clinicname']) . '",';
                                                    echo '"' . qescape($row['inv_im_name']) . '",';
                                                    echo '"' . qescape($row['invist_batch']) . '",';
                                                    echo '"' . qescape($row['formatexpirydate']) . '",';
                                                    echo '"' . qescape($row['invist_price']) . '",';
                                                    echo '"' . qescape($row['invist_quantity']) . '",';
                                                    echo '"' . qescape($row['formatcreateddate']) . '"' . "\n";
                                                } else {
                                                    ?>
                                                    <tr bgcolor='<?php echo $bgcolor ?>'>
                                                        <td class="detail"><?php echo $srCount; ?></td>
                                                        <td class="detail"><?php echo $row['clinicname']; ?></td>
                                                        <td class="detail"><?php echo $row['inv_im_name']; ?></td>
                                                        <td class="detail"><?php echo $row['invist_batch']; ?></td>
                                                        <td class="detail"><?php echo $row['formatexpirydate']; ?></td>
                                                        <td align="right" class="detail"><?php
                                                            echo $row['invist_price'];
                                                            ;
                                                            ?></td>
                                                        <td align="right" class="detail"><?php
                                                            echo $row['invist_quantity'];
                                                            ;
                                                            ?></td>
                                                        <td class="detail"><?php echo $row['formatcreateddate']; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                $srCount++;
                                            }
                                        } else {

                                            $srCountFlag = $srCount - 1;
                                            if (!$srCountFlag) {
                                                /* echo " <tr>
                                                            <td colspan='8'>" . xl('No Record Found', 'e') . "</td>
                                                 </tr>"; */ 
                                            echo " <tr>
                                                <td class='newtextcenter' colspan='8'>" . 'No Results Found' . "</td>
                                            </tr>";

                                            }
                                        }

                                        debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']); /// Update Report Auditing Section
                                    }
                                    // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
                                    if ($msgForReportLog) {
                                        ?>
                                    <!--<table>-->
                                        <tr>
                                            <td colspan="5" style="text-align:center; font-size:10pt;"> <?php echo ((!empty($msgForReportLog)) ? $msgForReportLog : 'No Results Found'); ?></td>
                                        </tr>
                                    <!--</table> -->
                                    <?php
                                }
                                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvBatchExpiry') {
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <!-- end of search results --> <?php
            }
        } else {
            ?> 

            <div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
            </div>

            <!-- stuff for the popup calendar -->
            <style type="text/css">
                @import url(../../library/dynarch_calendar.css);
            </style>
            <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
            <script type="text/javascript"
            src="../../library/dynarch_calendar_setup.js"></script>
            <script type="text/javascript">
                                               // Calendar.setup({inputField: "form_expiry_date", ifFormat: "%Y-%m-%d", button: "img_expiry_form_date"});
                                                //Calendar.setup({inputField: "to_expiry_date", ifFormat: "%Y-%m-%d", button: "img_expiry_to_date"});

                                                    Calendar.setup({
                                                        inputField: "form_expiry_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_expiry_form_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('to_expiry_date').value,
                                                                    fromDate = document.getElementById('form_expiry_date').value;

                                                            if (toDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_expiry_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                                                    Calendar.setup({
                                                        inputField: "to_expiry_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_expiry_to_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('to_expiry_date').value,
                                                                    fromDate = document.getElementById('form_expiry_date').value;

                                                            if (fromDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('to_expiry_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });

            </script>

        <?php } ?> 
        <?php
        if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvBatchExpiry') {
            ?>
            <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
            <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
            <input type='hidden' name='form_refresh' id='form_refresh' value='' /></form>

        <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
        </script>

    </body>

    <!-- stuff for the popup calendar -->
    <style type="text/css">
        @import url(../../library/dynarch_calendar.css);
    </style>
    <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
    <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
    <script type="text/javascript"
    src="../../library/dynarch_calendar_setup.js"></script>
                      <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField: "form_expiry_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_expiry_form_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('to_expiry_date').value,
                                                                    fromDate = document.getElementById('form_expiry_date').value;

                                                            if (toDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('form_expiry_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                                                    Calendar.setup({
                                                        inputField: "to_expiry_date"
                                                        , ifFormat: "%Y-%m-%d"
                                                        , button: "img_expiry_to_date"
                                                        , onUpdate: function () {
                                                            var toDate = document.getElementById('to_expiry_date').value,
                                                                    fromDate = document.getElementById('form_expiry_date').value;

                                                            if (fromDate) {
                                                                var toDate = new Date(toDate),
                                                                        fromDate = new Date(fromDate);

                                                                if (fromDate > toDate) {
                                                                    jAlert('From date cannot be later than To date.');
                                                                    document.getElementById('to_expiry_date').value = '';
                                                                }
                                                            }
                                                        }
                                                    });
                </script>


    <script type="text/javascript">
          // Calendar.setup({inputField: "form_expiry_date", ifFormat: "%Y-%m-%d", button: "img_expiry_form_date"});
          // Calendar.setup({inputField: "to_expiry_date", ifFormat: "%Y-%m-%d", button: "img_expiry_to_date"});
    </script>

    </html>
    <?php
}
?>