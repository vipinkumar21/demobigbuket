<?php
$sanitize_all_escapes  = true;
$fake_register_globals = false;

 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formdata.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 $subcatId = $_REQUEST['subcatId'];
 $catid = $_REQUEST['catid'];
 if (!empty($catid)) {
 	$res = sqlStatement("SELECT inv_im_id, inv_im_name FROM inv_item_master WHERE inv_im_status='1' AND inv_im_deleted='0' AND inv_im_catId = ? AND inv_im_subcatId = ?", array($catid, $subcatId));
 	if(sqlNumRows($res)){
 		$optionString = "<option value='0' selected='selected'>Select Item</option>";
 		while ($row = sqlFetchArray($res)) { 			
 			$optionString .= "<option value='".$row['inv_im_id']."'>".$row['inv_im_name']."</option>"; 			
 		}
 		echo $optionString;
 	}else{
 		echo "<option value='0' selected='selected'>Select Item</option>";
 	}
 }else {
 	echo "<option value='0' selected='selected'>Select Item</option>";
 }