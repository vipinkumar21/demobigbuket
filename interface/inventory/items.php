<?php
 $sanitize_all_escapes  = true;
 $fake_register_globals = false;
 require_once("../globals.php");
 require_once("$srcdir/acl.inc");
 require_once("drugs.inc.php");
 require_once("$srcdir/options.inc.php");
 require_once("$srcdir/formatting.inc.php");
 require_once("$srcdir/htmlspecialchars.inc.php");
 $searchItem = $_REQUEST['searchItemNameId'];

 // Check authorization.
 $thisauth = acl_check('inventory', 'im_item_list');
 if (!$thisauth) die(xlt('Not authorized'));
 $targetpage = $_SERVER[SCRIPT_NAME];
 if (isset($_GET["mode"])) {
 	if ($_GET["mode"] == "delete") { 
            
 		if(checkItemStock($_GET["itemid"])){
 			sqlStatement("update inv_item_master set inv_im_deleted='1' where inv_im_id = '" . $_GET["itemid"] . "'");
 			$alertmsg = '<span style="color:green;font-weight:bold;">Item deleted successfully.</span>';
 			$retundata = array('alertmsg'=>$alertmsg, 'status'=>1, 'itemid'=>$_GET["itemid"]);
                        echo json_encode($retundata);
                        //header('Location: items.php');
 			exit;
 		}else {
 			$alertmsg = '<span style="color:red;font-weight:bold;">Item can not be deleted as it is already linked with a document!</span>';
 			$retundata = array('alertmsg'=>$alertmsg, 'status'=>0, 'itemid'=>$_GET["itemid"]);
                        echo json_encode($retundata);
                        //header('Location: items.php');
 			exit;
 		}
 		
 	}
 	if ($_GET["mode"] == "activate") { 		
 			sqlStatement("update inv_item_master set inv_im_status='1' where inv_im_id = '" . $_GET["itemid"] . "'"); 		
	 		$_SESSION['alertmsg'] = 'Item activated successfully.';
	 		header('Location: items.php');
	 		exit; 		
 	}
 	if ($_GET["mode"] == "deactivate") { 
 		if(checkItemStockStatus($_GET["itemid"])){		
	 		sqlStatement("update inv_item_master set inv_im_status='0' where inv_im_id = '" . $_GET["itemid"] . "'");
	 		$_SESSION['alertmsg'] = 'Item deactivated successfully.';
	 		header('Location: items.php');
	 		exit;
 		}else {
 			$_SESSION['alertmsg'] = 'You should not able deactivate selected item.';
 			header('Location: items.php');
 			exit;
 		}
 	}
 }
 // For each sorting option, specify the ORDER BY argument.
//
$ORDERHASH = array(
	'inv_im_id' => 'inv_im_id ',
	'inv_im_name' => 'inv_im_name ASC',
	'inv_im_status'  => 'inv_im_status',  
);

// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'inv_im_id';
$orderby = $ORDERHASH[$form_orderby];



 // get drugs
 /*echo "SELECT cat.invcat_id, cat.invcat_name, cat.invcat_desc, cat.invcat_status, cat.invcat_deleted, cat.invcat_createdby " .
  "FROM inv_category AS cat " . 
  "WHERE cat.invcat_deleted='0' " .
  "ORDER BY $orderby";*/
 $where = '';
 if(!empty($searchItem)){
     $where = "WHERE im.inv_im_name LIKE '%".trim($_REQUEST['searchItemNameId'])."%' OR im.inv_im_code = '".trim($_REQUEST['searchItemNameId'])."' ";
 }
 $res = "SELECT im.inv_im_id, im.inv_im_name, im.inv_im_code, im.inv_im_catId, im.inv_im_desc, im.inv_im_isExpiry, im.inv_im_status, inv_im_sale, im.inv_im_uomId, im.inv_im_deleted, im.inv_im_createdby " .
  "FROM inv_item_master AS im" . 
  //"WHERE cat.invcat_deleted='0' " .
  " $where ORDER BY $orderby";
// echo $res ;
 
$num_rows = sqlNumRows(sqlStatement($res));	// total no. of rows
$per_page = $GLOBALS['encounter_page_size'];			// Pagination variables processing
 //$per_page = 2;
 $page = $_GET["page"];
 
 if(!$_GET["page"])
 {
 	$page=1;
 }
 
 $prev_page = $page-1;
 $next_page = $page+1;
 
 $page_start = (($per_page*$page)-$per_page);
 if($num_rows<=$per_page)
 {
 	$num_pages =1;
 }
 else if(($num_rows % $per_page)==0)
 {
 	$num_pages =($num_rows/$per_page) ;
 }
 else
 {
 	$num_pages =($num_rows/$per_page)+1;
 	$num_pages = (int)$num_pages;
 }
 $res .= " LIMIT $page_start , $per_page";
 $res = sqlStatement($res);
 
$csvjson=json_encode(array('colomns'=>array('Item Name','Item Code','Category','Description','Status'),'limit'=>array($page_start,$per_page),'search'=>array(trim($_REQUEST['searchItemNameId']))));

 function getCategorys($itemid){
    $q1="SELECT  GROUP_CONCAT(`name`  SEPARATOR ', ') as `name` FROM `invcategories` JOIN `invcategories_invitemmasters`  ON  `invcategories` .`id`= `invcategories_invitemmasters`.`invcategory_id`  WHERE `inv_im_id` ='".$itemid."'"; 
    $r1=sqlQuery($q1);
    
    return $r1['name'];
 }
?>
<html>

<head>
<?php html_header_show();?>

<link rel="stylesheet" href='<?php  echo $css_header ?>' type='text/css'>
<title><?php echo xlt('Inventory Item Master'); ?></title>
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
<style>
tr.head   { font-size:10pt; background-color:#cccccc; text-align:center; }
tr.detail { font-size:10pt; }

</style>
<link rel="stylesheet" type="text/css" href="../../library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/commonScript.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

<script language="JavaScript">

// callback from add_edit_drug.php or add_edit_drug_inventory.php:
function refreshme() {
 location.reload();
}

// Process click on drug title.
function dodclick(id) {
 dlgopen('add_edit_item.php?itemid=' + id, '_blank', 460, 375);
}
// Process click on a column header for sorting.
function dosort(orderby) {
 var f = document.forms[0];
 f.form_orderby.value = orderby;
 top.restoreSession();
 f.submit();
 return false;
}

function confirmation(link){
	jConfirm('Are you sure to delete?', 'Confirmation', function(r) {	    
            
            if(r){
	    	//location.href=link;
                $.ajax({url: link, dataType: "json", success: function(result){
                    $("#alertmsg").html(result.alertmsg);
                    if(result.status) {
                        $('#item'+result.itemid).css({'text-decoration': 'line-through'});
                        $('#itemaction'+result.itemid).html("");
                    }   
                }});
            }                       
	});
}

function confirmation2(link){
	jConfirm('Are you sure to Activate?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}

function confirmation3(link){
    
	jConfirm('Are you sure to Deactivate?', 'Confirmation', function(r) {
	    if(r){
	    	location.href=link;
	    }
	});
}

function submit(){
    
    var url="items.php";
    location.href=url+'?page='+$("#topage").val()+'&searchItemNameId='+$("#searchItemNameId").val();
    
}

$(function () {
    $('button[type="submit"]').click(function () {
        var pageTitle = 'Item List';
        var stylesheet = '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css';
        win = window.open('', 'Print', 'width=500,height=300');
        win.document.write('<html><head><title>' + pageTitle + '</title>' + '<script type="text/javascript" src="../../library/js/jquery.1.3.2.js" ><\/script>' +
                '<link rel="stylesheet" href="' + stylesheet + '">'+
            '</head><body id="newbody">' + $('#tableid')[0].outerHTML + '<script>$("#newbody #tableid tr").find("th:last, td:last").remove();<\/script>'+ '</body></html>');   
        //win.document.close();
        win.print();
        //win.close();
        return false;
    });
});
</script>

</head>

<body class="body_top">
	<div class="panel panel-warning">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-7 boldtxt"><?php  xl('Items','e'); ?></div>
                                <div class="col-xs-5 text-right"><?php //if(acl_check('inventory', 'im_item_add')){?><!--<a href="add_edit_item.php?itemid=0" class="iframe btn btn-default btn-sm" ><span><?php //xl('Add Item','e'); ?></span></a>--><?php //}?>&nbsp;<span><input type="checkbox" class="btn btn-default btn-sm" value="" name="all" id="all"/></span>&nbsp;<a class="btn btn-default btn-sm" href="#" onclick="exportcsv()">csv</a>&nbsp;<button class="btn btn-sm pull-right btn-default" type="submit">Print</button></div>
			</div>
		</div>
		<div class='panel-body'>
		    <table border='0' cellpadding='1' cellspacing='2' width='100%'>
                                <tr > 
                                    <td colspan="2" id="alertmsg">
			  	<?php if(!empty($_SESSION['alertmsg'])){ ?>
					
						<?php echo $_SESSION['alertmsg']; $_SESSION['alertmsg']=""; ?>		
				  	
				<?php } ?>
                                      </td>
                                </tr>
			</table>
			<form name="searchItem" id="searchItem" method="post" action="<?php echo $full_url ; ?>/interface/inventory/items.php">
				<div id="report_parameters" style="width:100%">
					<div class="row">
						<div class="col-md-3">
							<input type="text" id="searchItemNameId" name="searchItemNameId" class="form-control input-sm" value="<?php echo ($_REQUEST['searchItemNameId'] != '')?$_REQUEST['searchItemNameId'] : '' ;?>" placeholder="Item Code or Item Name" maxlength="30"  size="30">
						</div>
						<div class="col-md-9">
							<input type="submit" name="searchItem" value="search" id="searchItem" class="btn btn-warning btn-sm" >
						</div>
					</div>
				</div>
			</form> 
		
    

<form id='report_results' method='post' action='items.php'>
<table border='0' id="tableid" class="table table-bordered" cellpadding='1' cellspacing='0' width='100%'>
<?php if($num_rows > 0){?>
	<thead>
		<tr>
			<!--<th width="2%">
				<?php echo xlt('S.N.'); ?>
			</th>-->
			<th width="24%">   
				<?php echo xlt('Item Name'); ?>
			</th>
                        
			<th width="14%">
				<?php echo xlt('Item Code'); ?>
			</th>
			<th width="10%">
				<?php echo xlt('Category'); ?>
			</th>
			
			<th width="20%">
				<?php echo xlt('Description'); ?>
			</th>
			<th width="5%">   
				<?php echo xlt('Status'); ?>
			</th>  
			<th width="15%" class="newtextc">
				<?php echo xlt('Action'); ?>
			</th>  
		</tr>
	</thead>
	<?php } ?>
<?php 
 $lastid = "";
 $encount = 0;
 if(sqlNumRows($res)){
	 while ($row = sqlFetchArray($res)) {  
           //$csvarray[]=array($row['inv_im_name'],$row['inv_im_code'],$row['invcat_name'],$row['invsubcat_name'],$row['inv_im_desc'],$row['inv_im_status']);
           $itemid = $row['inv_im_id'];
	   ++$encount;
	   $bgcolor = "#" . (($encount & 1) ? "f7d9ca" : "fdede5");
	   $lastid = $row['inv_im_id'];
	   if($row['inv_im_deleted'] == 1){
			echo " <tr class='detail strikeThrough' bgcolor='$bgcolor' id='item$itemid'>\n";
		}else {
	   		echo " <tr class='detail' bgcolor='$bgcolor' id='item$itemid'>\n";
	   }
	   //echo "  <td>" .$encount. "</td>\n";
	   echo "  <td>" .	    
	    text($row['inv_im_name']) . "</td>\n";
	   echo "  <td>" . text($row['inv_im_code']) . "</td>\n";
	   echo "  <td>" . getCategorys($row['inv_im_id']). "</td>\n";
	   echo "  <td>" . text($row['inv_im_desc']) . "</td>\n";
	   echo "  <td>" . ($row['inv_im_status'] ? xlt('Active') : xlt('Not Active')) . "</td>\n";
	   echo "  <td class='newtextc newvm' id='itemaction$itemid'>";
 	   if($row['inv_im_deleted'] == 0){
			echo "<a class='iframe iconanchor' href='".$cakePhpFullUrl.'InvItemMasters/view/'.attr($lastid)."' title='View'><span class='glyphicon glyphicon-eye-open'></span></a> &nbsp;";
 	   		
 	   		if(acl_check('inventory', 'im_item_edit')){ 
 	   			//echo "<a class='iframe iconanchor' href='add_edit_item.php?itemid=".attr($lastid)."' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a> &nbsp;";
 	   		}
 	   		
 	   		//if(acl_check('inventory', 'im_item_del')){ echo "<a class='iconanchor' href='items.php?mode=delete&itemid=".attr($lastid)."' onclick='return confirm(\"Are you sure to delete?\"); top.restoreSession()' title='Delete'><span class=' glyphicon glyphicon-trash'></span></a> &nbsp; ";}

 	   		if(acl_check('inventory', 'im_item_del')) {
 	   			//echo "<a class='iconanchor' href='#' onclick='return confirmation(\"items.php?mode=delete&itemid=".attr($lastid)."\"); top.restoreSession()' title='Delete'><span class=' glyphicon glyphicon-trash'></span></a> &nbsp; ";
 	   		} 	   		
 	   		if($row['inv_im_status'] == 0){
 	   			//if(acl_check('inventory', 'im_item_edit')){ echo "<a class='iconanchor' href='items.php?mode=activate&itemid=".attr($lastid)."' onclick='return confirm(\"Are you sure to activate?\"); top.restoreSession()' title='Activate'><span class=' glyphicon glyphicon-ok'></span></a>";}
	 	   		if(acl_check('inventory', 'im_item_edit')){ 
	 	   			//echo "<a class='iconanchor' href='#' onclick='return confirmation2(\"items.php?mode=activate&itemid=".attr($lastid)."\"); top.restoreSession()' title='Activate'><span class='newactivate'></span></a>";
	 	   		}
			}else {
 	   			//if(acl_check('inventory', 'im_item_edit')){ echo "<a class='iconanchor' href='items.php?mode=deactivate&itemid=".attr($lastid)."' onclick='return confirm(\"Are you sure to deactivate?\"); top.restoreSession()' title='Deactivate'><span class=' glyphicon glyphicon-remove'></span></a>";}
 	   			if(acl_check('inventory', 'im_item_edit')){ 
                                    //echo "<a class='iconanchor' href='#' onclick='return confirmation3(\"items.php?mode=deactivate&itemid=".attr($lastid)."\"); top.restoreSession()' title='Dectivate'><span class='newdeactivate'></span></a>";
                                    
                                }
 	   		} 	   		
 	   }
		echo "</td>\n";	     
	  	echo " </tr>\n";
	 } // end while
         //$csvarray= json_encode($csvarray,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
 }else{
	?>
	<tr>
		<td colspan="7" style="text-align:center; font-size:10pt;">No Items Found</td>
	</tr>
	<?php 
 } // end If
 //csvExport($csvarray,array('Item Name','Item Code','Category','Sub Category','Description','Status'));
?>
</table>
<input type="hidden" name="form_orderby" value="<?php echo attr($form_orderby) ?>" />

</form>
<?php // Pagination Displaying Section?>
<table>
	<tbody>
		
                <tr><td class="text">
                     
                   <?php
                   $limit=$per_page;
                   $stages = 3;
                   if ($page == 0){$page = 1;}
                                $prev = $page - 1;	
                                $next = $page + 1;							
                                $lastpage = ceil($num_rows/$limit);		
                                $LastPagem1 = $lastpage - 1;					


                                $paginate = '';
                                if($lastpage > 1)
                                {	

                                        $paginate .= "<div class='paging'><span>".'<span class="pagingInfo">Total Records:'. $num_rows.'</span> Page :'.$num_pages;
                                        ?>
                        
                                        <?php
                                        // Previous
                                        if ($page > 1){
                                                $paginate.= "<a href='$targetpage?page=$prev&searchItemNameId=$searchItem'>previous</a>";
                                        }else{
                                                $paginate.= "<span class='disabled'>previous</span>";	}



                                        // Pages	
                                        if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
                                        {	
                                                for ($counter = 1; $counter <= $lastpage; $counter++)
                                                {
                                                        if ($counter == $page){
                                                                $paginate.= "<span class='current'>$counter</span>";
                                                        }else{
                                                                $paginate.= "<a href='$targetpage?page=$counter&searchItemNameId=$searchItem'>$counter</a>";}					
                                                }
                                        }
                                        elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
                                        {
                                                // Beginning only hide later pages
                                                if($page < 1 + ($stages * 2))		
                                                {
                                                        for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
                                                        {
                                                                if ($counter == $page){
                                                                        $paginate.= "<span class='current'>$counter</span>";
                                                                }else{
                                                                        $paginate.= "<a href='$targetpage?page=$counter&searchItemNameId=$searchItem'>$counter</a>";}					
                                                        }
                                                        $paginate.= "...";
                                                        $paginate.= "<a href='$targetpage?page=$LastPagem1&searchItemNameId=$searchItem'>$LastPagem1</a>";
                                                        $paginate.= "<a href='$targetpage?page=$lastpage&searchItemNameId=$searchItem'>$lastpage</a>";		
                                                }
                                                // Middle hide some front and some back
                                                elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
                                                {
                                                        $paginate.= "<a href='$targetpage?page=1&searchItemNameId=$searchItem'>1</a>";
                                                        $paginate.= "<a href='$targetpage?page=2&searchItemNameId=$searchItem'>2</a>";
                                                        $paginate.= "...";
                                                        for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
                                                        {
                                                                if ($counter == $page){
                                                                        $paginate.= "<span class='current'>$counter</span>";
                                                                }else{
                                                                        $paginate.= "<a href='$targetpage?page=$counter&searchItemNameId=$searchItem'>$counter</a>";}					
                                                        }
                                                        $paginate.= "...";
                                                        $paginate.= "<a href='$targetpage?page=$LastPagem1&searchItemNameId=$searchItem'>$LastPagem1</a>";
                                                        $paginate.= "<a href='$targetpage?page=$lastpage&searchItemNameId=$searchItem'>$lastpage</a>";		
                                                }
                                                // End only hide early pages
                                                else
                                                {
                                                        $paginate.= "<a href='$targetpage?page=1&searchItemNameId=$searchItem'>1</a>";
                                                        $paginate.= "<a href='$targetpage?page=2&searchItemNameId=$searchItem'>2</a>";
                                                        $paginate.= "...";
                                                        for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
                                                        {
                                                                if ($counter == $page){
                                                                        $paginate.= "<span class='current'>$counter</span>";
                                                                }else{
                                                                        $paginate.= "<a href='$targetpage?page=$counter&searchItemNameId=$searchItem'>$counter</a>";}					
                                                        }
                                                }
                                        }

                                                        // Next
                                        if ($page < $counter - 1){ 
                                                $paginate.= "<a href='$targetpage?page=$next&searchItemNameId=$searchItem'>next</a>";
                                        }else{
                                                $paginate.= "<span class='disabled'>next</span>";
                                                }

                                        $paginate.= "&nbsp Go To:&nbsp<input type='number' name='topage' id='topage' value='".$page."' min='1' max='".$num_pages."'/>&nbsp<input type='button' name='tobutton' value='Go' onclick='submit()' /></span></div>";		


                        }
                         
                        echo $paginate;
                   $page="item_list";
                   ?> 
                        
                    </td></tr>
	</tbody>
</table>
<script>
    function exportcsv(){
        var uri = "export.php";
        var urlex="page_<?php echo $page;?>.csv";
        var type;
        if ($("#all").is(":checked")) {
             type ="all";
        }else{
            //csvdata =JSON.stringify(<?php echo $csvarray;?>);
            type ="page";
        }
        //alert(type);
        $.ajax({
            url: uri,
            type: "POST",
            
             //data: 'name=<?php //echo $page;?>&csvdata='+csvdata+'&csvcolomn='+JSON.stringify(<?php //echo json_encode(array('Item Name','Item Code','Category','Sub Category','Description','Status'));?>),
            data: 'name=<?php echo $page;?>&type='+type+'&csvcolomn='+JSON.stringify(<?php echo $csvjson;?>),
            /*beforeSend: function() {
               jQuery('#lod').html("<img src='/assets/analytics/images/loader.gif' />");
            },*/
            success: function(data){
                window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'];?>/emr/attachments/" + urlex;
            },
            error:function(){
                alert("failure");
                $("#result").html('there is error while submit');
            }   
          }); 
      }

</script>
<!-- stuff for the popup calendar -->
</div>
</div>
</body>
</html>
