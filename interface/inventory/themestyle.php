<?php
    $oi_theme = isset($GLOBALS['inventory_oneivory_theme']) ? $GLOBALS['inventory_oneivory_theme'] : 0;
?>
<!-- commonstyles -->
<link rel="stylesheet" href="<?php echo $inventry_url; ?>web/css/jqueryUI/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $inventry_url; ?>emr/library/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" href="<?php echo $inventry_url; ?>webapp/css/font-awesome.css" />
<!-- commonstyles -->

<?php if ($oi_theme == 0) { ?>
	<!-- required for: prmthemestyles || "0" -->
    <link rel="stylesheet" href="<?php echo $inventry_url; ?>web/css/jqm/jquery.mobile-1.3.2.min.css" />
    <link rel="stylesheet" href="<?php echo $inventry_url; ?>emr/interface/themes/inventory-module.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $inventry_url; ?>web/css/left-nav.css" />
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $inventry_url; ?>emr/interface/inventory/favicon.png" />
    <!-- required for: prmthemestyles || "0" -->
<?php } ?>

<!-- commonstyles -->
<link rel="stylesheet" href="<?php echo $inventry_url; ?>web/css/style_sheet.css" />
<!-- commonstyles -->

<?php if ($oi_theme == 1) { ?>
    <!-- required for: oneivorythemestyles || "1" -->
    <link rel="stylesheet" type="text/css" href="<?php echo $inventry_url; ?>emr/interface/themes/oneivory.css" />
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $inventry_url; ?>emr/interface/inventory/oi_favicon.png" />
    <!-- required for: oneivorythemestyles -->
<?php } ?>
