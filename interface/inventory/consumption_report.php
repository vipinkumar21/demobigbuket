<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
require_once("../globals.php");
require_once("./lib/database.php");
require_once("./lib/inv.gacl.class.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

// Check authorization.
$thisauth = $invgacl->acl_check('inventory', 'iar_consum', 'users', $_SESSION['authUser']);
$thisAuthDoctor = $invgacl->acl_check('inventory', 'icr_consum', 'users', $_SESSION['authUser']);
if (!$thisauth && !$thisAuthDoctor) {
    die(xlt('Not authorized'));
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
$searchParam = '';
//echo "<pre>";
//print_r($_REQUEST);exit;
//$form_facility = $_REQUEST['form_facility'];
if(isset($_REQUEST['form_facility'])){
     $_SESSION['cid']=$_REQUEST['form_facility'];
}else{
   $_REQUEST['form_facility']= $_SESSION['cid']; 
}
$form_facility = isset($_REQUEST['form_facility']) ? $_REQUEST['form_facility'] : $_SESSION['Auth']['User']['facility_id'];
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
$provider = $_REQUEST['form_provider'];
$item = $_REQUEST['form_item'];
$sellable = $_REQUEST['form_sellable'];
$catid = $_REQUEST['catid'];
$form_price = $_REQUEST['form_price'];
$show_available_times = false;
$fileName = "consumption_report_" . date("Ymd_his") . ".csv";
// In the case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvConsumption') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <title><?php xl('Consumption Report', 'e'); ?></title>
            <?php include_once("themestyle.php"); ?>
            <?php include_once("scriptcommon.php"); ?>
        </head>
        <body>
            <!-- page -->
            <div id="page" data-role="page" class="ui-content">
                <!-- header -->
                <?php include_once("oi_header.php"); ?>
                <!-- header -->
                <!-- contentArea -->
                <div id="wrapper" data-role="content" role="main">
                    <!-- wrapper -->
                    <div class='themeWrapper' id='rightpanel'>  
                        <div class='containerWrap'>
                            <!-- pageheading -->
                            <div class='col-sm-12 borbottm'>
                                <?php include_once("inv_links.html"); ?>
                                <h1><?php xl('Consumption Report', 'e'); ?></h1>
                                <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?></div>
                            </div>
                            <!-- pageheading -->
                            <!-- mdleCont -->
                            <form method='post' name='theform' id='theform' action='consumption_report.php' class="botnomrg">
                                <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                                <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                                <input type='hidden' name='providerId' id='providerId' value='<?php echo $provider ?>'/>
                                <!-- leftPart -->
                                <div class="filterWrapper">
                                    <!-- facility --> 
                                    <div class="ui-block">
                                        <?php
                                        $userFacilityRestrict = $GLOBALS['restrict_user_facility'];
                                        usersFacilityDropdown('form_facility', '', 'form_facility', $form_facility, $_SESSION['authId'], $userFacilityRestrict, $pdoobject);
                                        ?>
                                    </div>
                                    <!-- facility --> 

                                    <!-- from --> 
                                    <div class='ui-block form_to_date_bx'>
                                    <input type='text' name='form_from_date_in' placeholder='From Date' id="form_from_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_from_date' id='form_from_date' value='<?php echo $form_date; ?>' />
                                    </div>
                                    <!-- from --> 
                                    <!-- to --> 
                                    <div class='ui-block form_to_date_bx'>
                                    <input type='text' name='form_to_date_in' placeholder='To Date' id="form_to_date_in" size='10' value='' title='' />
                                    <input type='hidden' name='form_to_date' id='form_to_date' value='<?php echo $form_to_date; ?>' />
                                    </div>
                                    <!-- to --> 
                                    <!-- saleable -->
                                    <div class="ui-block ui-chkbx">
                                        <label for="form_sellable" class="interButt"><?php xl('Saleable', 'e'); ?></label>
                                        <input type='checkbox' name='form_sellable' id="form_sellable" value='1' <?php echo (isset($_POST['form_sellable']) && $_POST['form_sellable'] == 1) ? 'checked' : ''; ?> >
                                    </div> 
                                    <!-- from -->
                                    <div class='ui-block wdth25'>                                                                      
                                        <a class="pull-right btn_bx" id='reset_form1' href="consumption_report.php?form_facility=<?php echo $_SESSION['reset_cid']; ?>">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-retweet icon"></span>
                                            </span>
                                            <b class="btn-text">Reset</b>
                                        </a>
                                        <?php if (1) { ?>
                                            <a class="pull-right" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "csvConsumption");
                                                            $("#theform").submit();'>
                                                <span class="new-btnWrap btn">
                                                    <span class="csv_icon"></span>
                                                </span>
                                                <b class="btn-text"><?php xl('Export', 'e'); ?></b>
                                            </a>
                                        <?php } ?> 
                                        <a id='advanceOptions' class="pull-right" href="javascript:void(0)">
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-zoom-in icon"></span>
                                            </span>
                                            <b class="btn-text">Advance</b>
                                        </a>
                                        <a class="pull-right cnsmptn_rep_srch" href="javascript:void(0)" onclick='$("#form_csvexport").attr("value", "viewConsumption");
                                                    $("#theform").submit();'>
                                            <span class="new-btnWrap btn">
                                                <span class="glyphicon glyphicon-search icon5"></span>
                                            </span>
                                            <b class="btn-text">Search</b>
                                        </a>
                                    </div>
                                    <div class='<?php
                                    if (empty($provider) && empty($catid) && empty($item)) {
                                        echo "dnone";
                                    }
                                    ?>' id='advanceOptionsToggle'>
                                        <!-- providor -->
                                        <div class="ui-block" id="doctorsList">
<!--                                            <select name='form_provider' id="form_provider" class='form-control input-sm'>
                                                <option value='' selected='selected'>All Doctors</option>
                                                <?php
//                                                $userId = $_SESSION['authId'];
//                                                $psql = "Select id FROM users WHERE id=$userId ";
//                                                $defaultFac = sqlQuery($psql);
//                                                getFacilityProvider($userId, $defaultFac['id'], $userFacilityRestrict)
                                                ?>
                                            </select>-->
                                        </div>
                                        <!-- providor --> 
                                        <!-- category --> 
                                        <div class='ui-block'>
                                            <select class='form-control input-sm' name='catid' id='catid'>
                                                <?php
                                                if (empty($catid)) {
                                                    ?>
                                                    <option value='0' selected="selected">All Category</option>
                                                <?php } else {
                                                    ?>
                                                    <option value='0'>All Category</option>
                                                <?php } ?>
                                                <?php
                                                $catSql = "SELECT id as invcat_id, name as invcat_name FROM invcategories WHERE is_active='1' AND deleted='0'";
                                                $cres = sqlStatement($catSql, null, $GLOBALS['adodb']['dbreport']);
                                                if (sqlNumRows($cres)) {
                                                    while ($crow = sqlFetchArray($cres)) {
                                                        ?>
                                                        <option value='<?php echo $crow['invcat_id'] ?>' <?php
                                                        if ($_POST['catid'] == $crow['invcat_id']) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $crow['invcat_name'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </div>
                                        <!-- category --> 
                                        <!-- items --> 
                                        <div class='ui-block'>
                                            <input type='text' placeholder='Item' name="form_item" id="form_item" value='<?php echo ($item != '') ? $item : ''; ?>' title=''>
                                        </div>
                                        <!-- items --> 
                                        <!-- price -->
                                                <?php if (0) { ?>
                                            <div class="ui-block ui-chkbx">
                                                <label for="form_price" class="interButt">
                                            <?php xl('Price', 'e'); ?>
                                                </label>
                                                <input type='checkbox' name='form_price' id="form_price" value='1' <?php echo (isset($_POST['form_price']) && $_POST['form_price'] == 1) ? 'checked' : ''; ?> >
                                            </div> 
                                        <?php } ?>
                                        <!-- price -->
                                    </div>
                                </div>
                                <!-- leftpart -->
                                <!-- table -->
                                <?php
                            } // end not form_csvexport
                            if ($_REQUEST['form_refresh'] || $_REQUEST['form_orderby'] || $_POST['form_csvexport'] || empty($form_facility) || $_REQUEST['form_facility']) {
                                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvConsumption') {
                                            ob_clean();
                                            // CSV headers:
                                            echo '"' . xl('Clinic Name') . '",';
                                            echo '"' . xl('Item Name') . '",';
                                            echo '"' . xl('Batch #') . '",';
                                            echo '"' . xl('Consumption Type') . '",';
                                            echo '"' . xl('Notes') . '",';
                                            echo '"' . xl('Expiry Date') . '",';
                                            echo '"' . xl('Consumption Date') . '",';
                                            if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
                                                echo '"' . xl('Price') . '",';
                                            }
                                            echo '"' . xl('Quantity') . '"' . "\n";
                                        } else {
                                ?>
                                 <div id='' class='tableWrp pb-2'>
                                    <div class="dataTables_wrapper no-footer">
                                        <table cellpadding='0' cellspacing='0' border='0' class='display ui-responsive table-stroke ui-table ui-table-reflow dataTable no-footer' width='100%'>
                                        <?php   }
                                $message = "";
                                $rang = getDateRange($form_date, $form_to_date, $form_facility);
                                $where = " DATE(invistr.invistr_created_date) >= '" . $rang["FromDate"] . "' AND DATE(invistr.invistr_created_date) <= '" . $rang["ToDate"] . "'";
                                $searchParam .= ' Expiry Date From = ' . $rang["FromDate"] . ' | Expiry Date To = ' . $rang["ToDate"] . ' | ';
                                $facilityIdList = $form_facility == 0 ? userFacilitys() : $form_facility;
                                $where .= " AND invistr.invistr_clinic_id IN (" . $facilityIdList . ")";
                                $searchParam .= $form_facility == 0 ? "Facility = All |" : "Facility = " . $facilityIdList . " | ";
                                $where .=!empty($provider) ? "  AND invistr.invistr_createdby = '" . $provider . "'" : "";
                                $searchParam .=!empty($provider) ? ' Provider = ' . $provider . ' | ' : "Provider = All | ";
                                $where .=!empty($catid) ? " AND FIND_IN_SET(" . $catid . ",im.inv_im_catId)" : "";
                                $searchParam .=!empty($catid) ? ' Category = ' . $catid . ' | ' : "Category = All | ";
                                $where .=!empty($sellable) ? " AND im.inv_im_sale = '" . $sellable . "'" : "";
                                $searchParam .=!empty($sellable) ? ' Saleable Items = Yes | ' : "Saleable Items = No | ";
                                $where .=!empty($item) ? " AND im.inv_im_name LIKE '" . $item . "%'" : "";
                                $searchParam .=!empty($item) ? ' Item = ' . $item . ' | ' : "";
                                $dateMysqlFormat = getDateDisplayFormat(1);

                                $query = " SELECT invistr.invistr_itemid,invistr.invistr_batch,
                                            invistr.invistr_quantity,
                                            invistr.invistr_price,
                                            im.inv_im_name,
                                            f.name AS clinicname,invistr.invistr_clinic_id As clinicId,
                                            CASE
                                                 WHEN(invistr.invistr_expiry='0000-00-00') THEN '' ELSE DATE_FORMAT(invistr.invistr_expiry,'%d-%m-%y') END AS expiryformateddate,
                                            DATE_FORMAT(invistr.invistr_created_date, '%d-%m-%y') AS batchformateddate,                          CASE WHEN(invistr.invistr_tran_type='19') THEN 'Bulk' ELSE 'Individual' END AS invistr_tran_type,invistr.invistr_comment
                                    FROM inv_item_stock_transaction AS invistr
                                    INNER JOIN facility AS f ON f.id = invistr.invistr_clinic_id
                                    INNER JOIN inv_item_master AS im ON im.inv_im_id = invistr.invistr_itemid
                                    LEFT JOIN inv_transaction_type AS invtrt ON invistr.invistr_tran_type = invtrt.invtrt_id
                                    LEFT JOIN `users` AS u ON u.id = im.inv_im_createdby
                                    WHERE $where
                                      AND invistr.invistr_isdeleted = '0'
                                      AND invistr.invistr_tran_type IN (8,12,17,19)
                                    ORDER BY invistr.invistr_id ";
                                 $daysRangeNotAllowed = $form_facility == 0 ? $form_facility_all_time_range_valid : $form_facility_time_range_valid;
                                $daysRangeNotAllowedMessage = $form_facility == 0 ? $form_facility_all_time_range_valid_errorMsg : $form_facility_time_range_valid_errorMsg;
                                $cronScheduleMessage = $form_facility == 0 ? $form_facility_all_time_range_errorMsg : $form_facility_time_range_errorMsg;
                                if ($rang['Days'] > $daysRangeNotAllowed) {
                                    $message = $daysRangeNotAllowedMessage;
                                } else {
                                    if ($rang['Cron']) {
                                        $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $rang["FromDate"], "toDate" => $rang["ToDate"], "itemtxt" => $item, "saleable" => $sellable, "catid" => $catid, "facility" => $form_facility, "provider" => $provider))); // changable
                                        $rcsl_report_description = "Consumption report from " . $rang["FromDate"] . " to " . $rang["ToDate"]; // changable  
                                        //cron log email//
                                                        
                                        $getFacelty=getFacilityName($form_facility);
                                        $subject="Consumption Report"."_".$getFacelty."_".date('Y-m-d H:i:s');
                                        $filters="Frome Date = ".$rang["FromDate"]. " To Date = ".$rang["ToDate"]."Facility = ".$getFacelty." Sellable=".$sellable." Doctor = ".getDoctorsName($provider)." Category = ".getCategory($catid)." Item = ".$item; 
                                        $report_data=  json_encode(array('header'=>'Clinic Name,Item Name,Batch #,Consumption Type,Expiry Date,Consumption Date,Price,Quantity,Notes','include'=>array('clinicname','inv_im_name','invistr_batch','invistr_tran_type','expiryformateddate','batchformateddate','invistr_price','invistr_quantity','invistr_comment'),'name'=>'Consumption Report','subject'=>$subject,'facility'=>$getFacelty,'filters'=>$filters,'query'=>$query));
                                        //cron log email//
                                        
                                        $msgForReportLog = scheduleReports("consumption_report", $rcsl_requested_filter, $rcsl_report_description, $report_data);
                                        $message = empty($msgForReportLog) ? $cronScheduleMessage : $msgForReportLog;
                                    } else {

                                        $message = "";
                                    }
                                }
                                if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvConsumption') {
                                    $event = "Report Consumption Export";
                                } else {
                                    $event = "Report Consumption View";
                                }
                                if ($message) {
                                    $auditid = debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2, $pdoobject); // Cron Schedule Report Auditing Section
                                } else {

                                    $auditid = debugADOReports($query, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                                    $num_rows = $pdoobject->custom_query($query, NULL, 1); // total no. of rows
                                    $per_page = $GLOBALS['encounter_page_size'];
                                    //$per_page=2;
                                    $page = $_GET["page"];
                                    $curr_URL = $_SERVER['SCRIPT_NAME'] . '?form_refresh=viewConsumption &' . 'form_facility=' . $form_facility . '&form_from_date=' . $rang["FromDate"] . '&form_to_date=' . $rang["ToDate"] . '&form_sellable=' . $sellable . '&form_provider=' . $provider . '&form_item=' . $item . '&catid=' . $catid . '&';
                                    $pa_data = pagination_prm($num_rows, $per_page, $page, $curr_URL);
                                    $page = ($page == 0 ? 1 : $page);
                                    $page_start = ($page - 1) * $per_page;

                                    if ($_POST['form_csvexport'] != 'csvConsumption') {
                                        $query.=" LIMIT $page_start , $per_page";
                                    }
                                    $res = $pdoobject->custom_query($query, null);
                                    ?>

                                    <?php
                                    if ($num_rows) {
                                                                $srCount = 1;
                                                                $prevClinic = '';
                                                                $currClinic = '';
                                                                $currItem = '';
                                                                $prevItem = '';
                                                                foreach ($res as $row) {
                                                                    $currClinic = $row['clinicId'];
                                                                    $currItem = $row['invistr_itemid'];
                                                                    $itemExpiry = $row['expiryformateddate'];
                                                                    if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvConsumption') {
                                                                        echo '"' . qescape($row['clinicname']) . '",';
                                                                        echo '"' . qescape($row['inv_im_name']) . '",';
                                                                        echo '"' . qescape($row['invistr_batch']) . '",';
                                                                        echo '"' . $row['invistr_tran_type'].'",';
                                                                        echo '"'. qescape($row['invistr_comment']).'",';
                                                                        echo '"' . qescape($itemExpiry) . '",';
                                                                        echo '"' . qescape($row['batchformateddate']) . '",';
                                                                        if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') {
                                                                            echo '"' . qescape($row['invistr_price']) . '",';
                                                                        }
                                                                        
                                                                       echo '"' . qescape($row['invistr_quantity']) . '"' . "\n";
                                                                        
                                                                    } else {
                                                                        if($srCount==1){
                                                                            
                                                                        ?><?php if ($_POST['form_csvexport'] != 'csvConsumption' && $num_rows > 0) { ?>
                                        <!-- pagination --> <?php echo $pa_data; ?> <!-- pagination -->
                                    <?php } ?>
                                        <thead>
                                                            <tr>
                                                            <?php if (empty($form_facility)) { ?>
                                                                <th width='20%'><?php xl('Clinic Name', 'e'); ?></th><?php } ?>
                                                                <th width='20%'><?php xl('Item Name', 'e'); ?></th>
                                                                <th width='10%'><?php xl('Batch #', 'e'); ?></th>
                                                                <th width='10%'><?php xl('Type', 'e'); ?></th>
                                                                <th width='5%'><?php xl('Notes', 'e'); ?></th>
                                                                <th width='10%'><?php xl('Expiry Date', 'e'); ?></th>
                                                                <th width='10%'><?php xl('Date', 'e'); ?></th>
                                                                <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                    <th width='5%'><?php xl('Price', 'e'); ?></th>
                                                                <?php } ?>
                                                                <th width='10%'><?php xl('Quantity', 'e'); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                        <?php }
                                                                        if ($currClinic != $prevClinic && empty($form_facility)) { ?>
                                                                    <tr>
                                                                        <td><?php echo $row['clinicname']; ?></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                            <td></td>
                                                                        <?php } ?>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <?php if ($currItem != $prevItem && $currClinic != $prevClinic) { ?>
                                                                            <td><?php echo $row['inv_im_name']; ?></td>
                                                                        <?php } else if ($currItem == $prevItem && $currClinic != $prevClinic) { ?>
                                                                            <td><?php echo $row['inv_im_name']; ?></td>
                                                                        <?php } else {
                                                                            ?>
                                                                            <td></td>
                                                                        <?php } ?>
                                                                        <td><?php echo $row['invistr_batch']; ?></td>
                                                                        <td><?php echo $row['invistr_tran_type']; ?></td>
                                                                        <td>Notes</td>
                                                                        <td>&nbsp;<?php echo $itemExpiry; ?></td>
                                                                        <td>&nbsp;<?php echo $row['batchformateddate']; ?></td>
                                                                        <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                            <td>&nbsp;<?php echo $row['invistr_price']; ?></td>
                                                                            <?php } ?>
                                                                        <td>&nbsp;<?php
//                                                                            if ($row['invistr_tran_type'] == 17) {
//                                                                                echo '-';
//                                                                            } 
                                                                           echo $row['invistr_quantity'];
                                                                            ?></td>
                                                                    </tr>
                                                                    <?php } else { ?>
                                                                    <tr>
                                                                        <?php if (empty($form_facility)) { ?>
                                                                            <td></td><?php } ?>
                                                                            <?php if($currItem != $prevItem) 
                                                                            {?>
                                                                                <td><?php echo $row['inv_im_name']; ?></td>
                                                                            <?php } else 
                                                                            {?>
                                                                                <td></td>
                                                                            <?php }?>
                                                                            
                                                                        <td><?php echo $row['invistr_batch']; ?></td>
                                                                         <td><?php echo $row['invistr_tran_type'] ; ?></td>
                                                                        <td><?php echo $row['invistr_comment']; ?></td>                                                                       
                                                                        <td>&nbsp;<?php echo $itemExpiry; ?></td>
                                                                        <td>&nbsp;<?php echo $row['batchformateddate']; ?></td>
                                                                        <?php if (isset($_POST['form_sellable']) && $_POST['form_sellable'] != '') { ?>
                                                                            <td>&nbsp;<?php echo $row['invistr_price']; ?></td>
                                                                            <?php } ?>
                                                                        <td>&nbsp;<?php
//                                                                            if ($row['invistr_tran_type'] == 17) {
//                                                                                echo '-';
//                                                                            } 
                                                                              echo $row['invistr_quantity'];
                                                                            ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            $prevClinic = $row['clinicId'];
                                                            $prevItem = $row['invistr_itemid'];
                                                            $srCount++;
                                                        }
                                                    }
                                                    debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']);
                                                }
                                                // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
                                                if ($_POST['form_csvexport'] != 'csvConsumption') {
                                                    ?>
                                                    <?php
                                                    if (!$srCount && empty($message)) {
                                                        ?>
                                                             <div style="display: block;" id="dailynorecord">No Records Found!</div>
                                                    <?php
                                                }
                                            }if ($message) {
                                                ?>
                                                <div style="display: block;" id="dailynorecord"><?php echo $message; ?></div>
                                                    <?php } if ($_POST['form_csvexport'] != 'csvConsumption' && $num_rows > 0) {
                                                        ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- pagination --> <?php echo $pa_data; ?> <!-- pagination -->
                                    </div>
                                    <!-- end of search results --> <?php
                                }
                            } if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvConsumption') {
                                ?>
                                <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
                                <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
                                <input type='hidden' name='form_refresh' id='form_refresh' value='' />                        
                                <!-- table -->
                            </form>
                            <!-- mdleCont -->
                        </div>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- contentArea -->
            </div>
            <!-- page -->
            <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
            </script>
        </body>
    </html>
    <?php
} 