<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
ob_start();
require_once("../globals.php");
require_once("../../library/globals.inc.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once ("$audting_webroot/auditlog.php");
include_once("../pagination.php");
if ($_REQUEST['page']) {
    $pageSelection = $_REQUEST['page'];
}

$form_facility = empty($_REQUEST['form_facility']) ? 0 : intval($_REQUEST['form_facility']);
$form_region = empty($_REQUEST['form_region']) ? 0 : intval($_REQUEST['form_region']);
$form_zone = empty($_REQUEST['form_zone']) ? 0 : intval($_REQUEST['form_zone']);
$form_provider = trim($_REQUEST['form_provider']);
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];

function treatmentplanListByPlangroup($invId = Null) {

    //Received Amount			
    $sql = "SELECT ii.invit_inv_id , b.id ,b.work_status, b.toothno,  b.remarks, b.code_text, b.discount_type, b.discount, b.units, ROUND(b.discount) AS rounddiscount, b.fee, b.unit_price AS pr_price,  b.code, b.code_type, b.deletestate, b.units, b.plangroup ,b.treatmenttype, DATE_FORMAT(b.date, '" . $dateMysqlFormat . "') AS treatmentformateddate , ii.invit_deleted   ";
    $sql .= "FROM invoice_items AS ii INNER JOIN billing AS b ON ii.invit_tp_id = b.id ";
    $sql .= "WHERE ii.invit_inv_id = $invId";
    $pLists = sqlStatement($sql);

    $treatmentplanListByPlangroup = array();
    while ($prow = sqlFetchArray($pLists)) {
        $treatmentplanListByPlangroup[] = $prow;
    }
    return $treatmentplanListByPlangroup;
}

/*
  Function to calculate total discount by invoice id
 */

function totalDiscount($invId = Null, $tretment = Null) {

    $oldDiscount = 0;
    if (!empty($tretment)) {
        foreach ($tretment as $k => $v) {
            $oldDiscount += trtmentDiscount($v['discount_type'], $v['discount'], $v['pr_price'], $v['units']);
            $cost += ($v['pr_price'] * $v['units']);
        }
    }
      
    return array('oldDiscount' => $oldDiscount, 'cost' => $cost);
}

function trtmentDiscount($discount_type = Null, $discount = Null, $pr_price = null, $units = Null) {

    if ($discount_type == 'Amt') {
        $discountAmt = $discount;
    } else {
        $discountAmt = ($pr_price * $units) * $discount / 100;
    }
    return $discountAmt;
}

function getAllParentsInv($invoiceId) {
    $returnData = '';
    $sql = "SELECT inv.inv_number FROM parent_invoice AS pinv INNER JOIN invoice AS inv ON inv.inv_id = pinv.pinv_parent_id WHERE pinv_inv_id = $invoiceId";
    $pLists = sqlStatement($sql);
    while ($prow = sqlFetchArray($pLists)) {
        if (empty($returnData)) {
            $returnData = $prow['inv_number'];
        } else {
            $returnData .= ', ' . $prow['inv_number'];
        }
    }
    return $returnData;
}

function patientProductSalesTotalFee($patientId, $invoiceId, $clinicId) {
         
        $invoiceCostSum = 0;
        $dataValue = array();
        
        $sql = "SELECT i.inv_deletestate, ii.invit_deleted, ps.ps_discount, ROUND(ps.ps_discount) AS rounddiscount, ps.ps_discount_type, ps.ps_fee, FORMAT(ROUND(ps.ps_fee),2) AS roundfee, ii.invit_deleted, invis.invist_price, ps.ps_quantity, ps.ps_deletestate ";
        $sql .= "FROM invoice AS i ";
        $sql .= "LEFT JOIN invoice_items AS ii ON ii.invit_inv_id = i.inv_id ";
        $sql .= "LEFT JOIN product_sales AS ps ON ii.invit_ps_id = ps.ps_id ";
        $sql .= "LEFT JOIN inv_item_stock AS invis ON ps.ps_stockid = invis.invist_id ";
        $sql .= "LEFT JOIN inv_item_master AS invim ON invim.inv_im_id = invis.invist_itemid ";
        $sql .= "WHERE i.inv_clinic_id = $clinicId AND i.inv_pid = $patientId AND (ii.invit_tp_id = 0 OR ii.invit_tp_id IS NULL) AND i.inv_id = $invoiceId ";

        $pLists = sqlStatement($sql);
        while ($iter = sqlFetchArray($pLists)) {
             if ($iter['ps_deletestate'] == 0) { //NON-Deleted Invoice Only
                        if ($iter['ps_discount_type'] == 'amt') {
                            $invoiceDiscountVal += $iter['ps_discount'];
                        } else {
                            $invoiceDiscountVal += round((($iter['invist_price'] * $iter['ps_quantity']) / 100) * $iter['ps_discount']);
                        }
                        $invoiceTotalCost += $iter['invist_price'] * $iter['ps_quantity'];
                        $invoiceDiscountCost += $iter['ps_fee'];
                    }
        }
         
           
        $dataValue = $invoiceTotalCost - $invoiceDiscountVal ; 
        return $dataValue;
    }
// Prepare a string for CSV export.
function qescape($str) {
  $str = str_replace('\\', '\\\\', $str);
  return str_replace('"', '\\"', $str);
}

?>
<?php 

$fileName = "invoice_dashboard_".date("Ymd_his").".csv";
if ($_REQUEST['form_csvinvoiceexport'] == 'exportinvoice') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
}else{ ?>
<html>

    <head>
        
<?php html_header_show(); ?>

        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">

        <title><?php xl('Invoice Audit - Dashboard', 'e'); ?></title>

        <script type="text/javascript" src="../../library/overlib_mini.js"></script>
        <script type="text/javascript" src="../../library/textformat.js"></script>
        <script type="text/javascript" src="../../library/dialog.js"></script>
        <script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>
        <link rel=stylesheet href="../themes/bootstrap.css" type="text/css">

        <script type="text/javascript">
            var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

            function dosort(orderby) {
                var f = document.forms[0];
                f.form_orderby.value = orderby;
                f.submit();
                return false;
            }

            function oldEvt(eventid) {
                dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
            }

            function refreshme() {
                document.forms[0].submit();
            }


            $(document).ready(function () {
                var facilityID = $("#form_facility").val();
               
                $('.popover_div,.reason_pop').hover(function () {
                    $(this).children('.popover').show();
                }, function () {
                    $(this).children('.popover').hide();
                });
                if(facilityID != '' ){
                     var providerID =$('#form_provider').val();
                $.ajax({
                        url: "get_facility_provider.php?facilityId=" + facilityID,
                        success: function (result) {
                            $("#form_provider").html(result);
                             $('#form_provider').val(providerID);
                        }
                    });
                
                } 
                                   
                $("#form_facility").change(function () {
                    var facilityID = $("#form_facility").val();
                       $.ajax({
                        url: "get_facility_provider.php?facilityId=" + facilityID,
                        success: function (result) {
                            $("#form_provider").html(result);
                        }
                    });
                });
            });
            $(document).ready(function(){
                var selectedRegionAjax='<?php echo $form_region; ?>';
                    $("#form_region").change(function(){
                        changeRegion();
                    });
                    $("#form_zone").change(function(){
                        changeZone();
                    });
                    var selectedRegionAjax='<?php echo $form_region; ?>';
                    var selectedZoneAjax='<?php echo $form_zone; ?>';
                    var selectedFacilityAjax='<?php echo $form_facility; ?>';
                    if(selectedRegionAjax=='' && selectedZoneAjax!=''){
                        changeZone();
                    }else if(selectedRegionAjax!='' && selectedZoneAjax=='')
                    {
                        changeRegion();
                    }else if(selectedRegionAjax!='' && selectedZoneAjax!=''){
                        changeZone();
                    }
                    function changeRegion()
                    {
                        var regionId = $("#form_region").val();
                        $.ajax({
                                url:"../reports/get_facility_zones.php?regionId="+regionId+"&facilityId="+selectedFacilityAjax,
                                success:function(result){
                                        var result =  $.parseJSON(result);
                                        $("#form_zone").html(result[0]);
                                        $("#form_facility").html(result[1]);
                                }
                        });
                    }
                    function changeZone()
                    {
                        var zoneId = $("#form_zone").val();
                        var regionId = $("#form_region").val();
                        $.ajax({
                                url:"../reports/get_facility_by_zones.php?zoneId="+zoneId+"&facilityId="+selectedFacilityAjax+"&regionId="+regionId,
                                success:function(result){
                                        var result =  $.parseJSON(result);
                                            $("#form_zone").html(result[0]);
                                            $("#form_facility").html(result[1]);
                                }
                        });
                    }
            });
            function exportCSV() {
                //this.document.frm.act.value="show_home";
                document.forms[0].form_csvexport.value = "csvStockLedger";
                //this.document.frm.action="index.php"
                //this.document.frm.submit();
            }
            function viewStockLedger() {
                //this.document.frm.act.value="show_home";
                document.forms[0].form_csvexport.value = "viewStockLedger";
                //this.document.frm.action="index.php"
                //this.document.frm.submit();
            }
        </script>

        <style type="text/css">
            .popover_div,.reason_pop{position: relative;cursor: pointer;}
            .clearfix:before {
                clear: both;
                content: " ";
                display: block;
                height: 0;
                visibility: hidden;
            }
            .clearfix:after {
                clear: both;
                content: " ";
                display: block;
                height: 0;
                visibility: hidden;
            }
            .popoverDetailList {
                background: none repeat scroll 0 0 #fff;
                list-style: none outside none;
            }
            .popoverDetailList li {
                float: left;
                width: 48%;
                border-bottom: 1px solid #452311;
                padding: 1%;
            }
            .dashbList th {
                background: linear-gradient(to bottom, #be631d 0%, #e5791f 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
                color: #fff;
                text-shadow: 0 1px 0 #000;
            }
            .dashbList td {
                background: none repeat scroll 0 0 #fff;
                border-bottom: 1px solid #431f19;
            }
            .dashbList th, .dashbList td {
                padding: 0.5em;
                text-align: left;
                vertical-align: middle;
            }
            .popover {
                 background:#fff;
                border: 1px solid #9a460b;
                border-radius: 0.3em;
                box-shadow: 0 0 18px 1px rgba(50, 50, 50, 0.75);
                color: #000;
                display: none;  word-wrap: break-word; word-break:break-all;
                position: absolute;
                line-height:18px;
                left:0px;
                padding: 4px;  
                text-align: center;
                width: 400px;
                z-index: 4;
            }
            /* specifically include & exclude from printing */
            @media print {
                #report_parameters {
                    visibility: hidden;
                    display: none;
                }
                #report_parameters_daterange {
                    visibility: visible;
                    display: inline;
                }
                #report_results table {
                    margin-top: 0px;
                }
            }

            /* specifically exclude some from the screen */
            @media screen {
                #report_parameters_daterange {
                    visibility: hidden;
                    display: none;
                }
            }



        </style>
<?php echo paginationCss(); ?>
    </head>

    <body class="body_top">
<div class="panel panel-warning">
        <!-- Required for the popup date selectors -->
        <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
       <div class="panel-heading boldtxt"><?php xl('Invoice Audit', 'e'); ?> - <?php xl('Dashboard', 'e'); ?>
        <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
        </div>
       </div>
        <div class="panel-body">
        <form method='get' name='theform' id='theform' action='dashboard.php'>
            <div id="report_parameters">
                <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                <input type='hidden' name='form_csvinvoiceexport' id='form_csvinvoiceexport' value=''/>
                <input type='hidden' name='form_csvexport' id='form_csvexport' value='viewStockLegder'/>
                <input type="hidden" name="default_facility" value="<?php echo $form_facility ?>" />
                <input type="hidden" name="default_provider" value="<?php echo $form_provider ?>" />
                <div class="row">
                    <div class="col-md-4">
                            <?php xl('Region', 'e'); ?>:<?php dropdownRegions(strip_escape_custom($form_region), 'form_region'); ?></div>
                        <div class="col-md-4">
                            <?php xl('Zone', 'e'); ?>:<?php dropdownZones(strip_escape_custom($form_zone), 'form_zone'); ?></div>
                        <div class="col-md-4">
                            <?php xl('Facility', 'e'); ?>:<?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php xl('Provider', 'e'); ?>:
                        <input class='form-control input-sm' type='text' name='form_provider' id="form_provider" size='5' value='<?php echo $form_provider; ?>'>
                    </div>
                    <div class="col-md-4"><?php xl('By Invoice', 'e'); ?>:<input type='text'  class="form-control input-sm"  name='invoice' id="invoice" size='5' value='<?php echo $invoice ?>'></div>
                    <div class="col-md-4"><?php xl('By Invoice Type', 'e'); ?>:<select  class="form-control input-sm"  name="inv_type" id="inv_type">
                                <option value="">-Select Invoice Type-</option>
                                <option value="1" <?php echo($_REQUEST['inv_type'] == '1') ? 'selected="selected"' : ''; ?>>New</option>
                                <option value="0" <?php echo($_REQUEST['inv_type'] == '0') ? 'selected="selected"' : ''; ?>>Deleted</option>
                                <option value="2" <?php echo($_REQUEST['inv_type'] == '2') ? 'selected="selected"' : ''; ?>>Credit</option>
                            </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"><?php xl('By Patient', 'e'); ?>:<input type='text' class="form-control input-sm" name='patient' id="patient" size='5' value='<?php echo $patient ?>'></div>
                    <div class="col-md-4"><?php xl('From', 'e'); ?>:<br><input type='text'form-control input-sm name='form_from_date' id="form_from_date"
                                   size='10' value='<?php echo $form_date ?>'
                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                   align='absbottom' width='24' height='22' id='img_from_date'
                                   border='0' alt='[?]' style='cursor: pointer'
                                   title='<?php xl('Click here to choose a date', 'e'); ?>'>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                               <?php xl('To', 'e'); ?><br>
                                <input type='text' name='form_to_date' id="form_to_date"
                                                                               size='10' value='<?php echo $form_to_date ?>'
                                                                               onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                               title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                               align='absbottom' width='24' height='22' id='img_to_date'
                                                                               border='0' alt='[?]' style='cursor: pointer'
                                                                               title='<?php xl('Click here to choose a date', 'e'); ?>'>
                            </div>
                    </div>
                </div>
                <div>
                    <div style="margin-top:15px;">

                        <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewStockLedger");$("#form_csvinvoiceexport").attr("value", "");
                                    $("#theform").submit();'>
                                        <span> <?php xl('Submit', 'e'); ?> </span> </a> 
                            <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvinvoiceexport").attr("value", "exportinvoice");
                                                            $("#theform").submit();'>
                                                        <span>
                                                            <?php xl('Export to CSV', 'e'); ?>
                                                        </span>
                                                    </a>                                       
                    </div>
                    
                </div>
            </div>
            <!-- end of search parameters --> 

            
    <?php } 
    if (!($_REQUEST['form_refresh'] || $_REQUEST['form_csvexport'] || $_REQUEST['form_csvinvoiceexport'])){ 
    ?>   
            <div>
                * Please select a filter to see the results.
            </div>
    <?php } ?>        
<?php
if ($_REQUEST['form_refresh'] || $_REQUEST['form_csvexport'] || $_REQUEST['form_csvinvoiceexport']){ 
    
    if ($form_facility) {
        //for individual facility
        if ($form_date) {
            //if from date selected
            if ($form_to_date) {
                // if $form_date && $form_to_date
                $toDate = date_create(date($form_to_date));
                $fromDate = date_create($form_date);
                $diff = date_diff($fromDate, $toDate);
                $days_between_from_to = $diff->days;


                if ($days_between_from_to <= $form_facility_time_range) {

                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";

                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_clinic_id = '$form_facility'";
                } else {
                    $msgForReportLog = "Only $form_facility_time_range day's report is applicable!";
                }
            } else {
                //only from date: no TO date
                $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                $toDate = date_create(date($form_to_date));
                $fromDate = date_create($form_date);
                $diff = date_diff($fromDate, $toDate);
                $days_between_from_to = $diff->days;

                if ($days_between_from_to <= $form_facility_time_range) {
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_clinic_id = '$form_facility'";
                } else {
                    $msgForReportLog = "Only $form_facility_time_range day's report is applicable!";
                }
            }
        } else {
            // if from date not selected
            if ($form_to_date) {
                $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                $toDate = date_create(date($form_to_date));
                $fromDate = date_create($form_date);
                $diff = date_diff($fromDate, $toDate);
                $days_between_from_to = $diff->days;

                if ($days_between_from_to <= $form_facility_time_range) {
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_clinic_id = '$form_facility'";
                } else {
                    $msgForReportLog = "Only $form_facility_time_range day's report is applicable!";
                }
            } else {
                if ($where) {
                    $where .= " AND ";
                }
                $form_to_date = date("Y-m-d");
                $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                if ($where) {
                    $where .= " AND ";
                }
                $where .= " i.inv_clinic_id = '$form_facility'";
            }
        }
        
        $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param
    } else {
        if($form_region)
        {
            $where.=(!empty($where)) ? " AND f.region_id='".$form_zone."'" : " f.region_id='".$form_region."'"; 
        }
        if($form_zone)
        {
            $where.=(!empty($where)) ? " AND f.zone_id='".$form_zone."'" : " f.zone_id='".$form_zone."'"; 
        }
        //for all facility
        if ($form_date) {
            //if from date selected
            if ($form_to_date) {
                // if $form_date && $form_to_date
                $toDate = date_create(date($form_to_date));
                $fromDate = date_create($form_date);
                $diff = date_diff($fromDate, $toDate);
                $days_between_from_to = $diff->days;

                if ($days_between_from_to <= $form_facility_all_time_range) {

                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                } else {
                    //$msgForReportLog = "<b>Only $form_facility_all_time_range day's report is applicable! </b> <br>";
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "audit_dashboard_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $form_provider,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                    $rcsl_report_description = "Audit Dashboard report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog;
                    } else {
                        $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                    echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
                    exit();
                }
            } else {
                //only from date: no TO date
                $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                $toDate = date_create(date($form_to_date));
                $fromDate = date_create($form_date);
                $diff = date_diff($fromDate, $toDate);
                $days_between_from_to = $diff->days;

                if ($days_between_from_to <= $form_facility_all_time_range) {
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                } else {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "audit_dashboard_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $form_provider,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                    $rcsl_report_description = "Audit Dashboard report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog;
                    } else {
                        $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                    echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
                    exit();
                }
            }
            
        } else {
            // if from date not selected
            if ($form_to_date) {
                $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                $toDate = date_create(date($form_to_date));
                $fromDate = date_create($form_date);
                $diff = date_diff($fromDate, $toDate);
                $days_between_from_to = $diff->days;

                if ($days_between_from_to <= $form_facility_all_time_range) {
                    if ($where) {
                        $where .= " AND ";
                    }
                    $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                } else {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "audit_dashboard_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $form_provider,"facRegionId" =>$form_region,"facZoneId" => $form_zone))); // changable
                    $rcsl_report_description = "Audit Dashboard report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog;
                    } else {
                        $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                    echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
                   exit();
                }
            } else {
                if ($where) {
                    $where .= " AND ";
                }
                $form_to_date = date("Y-m-d");
                $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                $where .= " i.inv_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
            }
        }
        
        $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param
    }


    if (isset($form_provider) && $form_provider != '') {
        if ($where) {
            $where .= " AND ";
        }
        $where .= " ((u.fname LIKE '%" . $form_provider . "%') OR  (u.lname LIKE '%" . $form_provider . "%')) ";
        $searchParam .= ' Provider = '. $provider.' | ';
    } else {
        $searchParam .= ' Provider = All | ';
    }

    if (!$where) {
        $where = "1 = 1";
    }

    if (isset($_REQUEST['invoice']) && $_REQUEST['invoice'] != '') {
        $where .= " AND i.inv_number LIKE '%" . trim($_REQUEST['invoice']) . "%'";
        $searchParam .= ' Invoice = '. trim($_REQUEST['invoice']).' | ';
    }
    if (isset($_REQUEST['inv_type']) && $_REQUEST['inv_type'] != '') {
        $where .= " AND i.inv_deletestate = '" . $_REQUEST['inv_type'] . "'";
        $searchParam .= ' Invoice Type = '. $_REQUEST['inv_type'].' | ';
    }
    $join = '';
    if (isset($_REQUEST['patient']) && $_REQUEST['patient'] != '') {
        $where .= " AND (PA.fname LIKE '%" . trim($_REQUEST['patient']) . "%' OR PA.lname LIKE '%" . trim($_REQUEST['patient']) . "%' OR PA.pubpid LIKE '%" . trim($_REQUEST['patient']) . "%')";
        $searchParam .= ' Patient Name = '. trim($_REQUEST['patient']).' | ';
    }

    //Pagination
    //$page = (int) (!empty($_REQUEST["page"]) ? $_REQUEST["page"] : 1 );    
    //$limit = $GLOBALS['encounter_page_size'];   
    //$startpoint = ($page * $limit) - $limit;

    //$url = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'] . '&';
    if($_REQUEST['form_csvexport']=='viewStockLedger'){ ?>
        <div id="report_results" class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                    <th><?php xl('S.No', 'e'); ?></th>    
                    <th><?php xl('Date', 'e'); ?></th>
                    <th><?php xl('#Invoice', 'e'); ?></th>
                    <th><?php xl('Net Amount', 'e'); ?></th>
                    <th><?php xl('Coupon Value', 'e'); ?></th>                     
                    <th><?php xl('Amt. Payable', 'e'); ?></th>
                    <th><?php xl('Invoice Type', 'e'); ?></th>
                    <th><?php xl('Remark', 'e'); ?></th>
                    <th><?php xl('Patient Id', 'e'); ?></th>
                    <th><?php xl('Patient Name', 'e'); ?></th>
                    <th><?php xl('Clinic', 'e'); ?></th>		
                    <th><?php xl('Doctor', 'e'); ?></th> 
                    </tr>               
                    </thead>
                    <tbody>
                    <tr> 
        
    <?php } 

     $query = "SELECT i.inv_id , i.inv_number, i.inv_pid , i.parent_inv_id , i.inv_clinic_id , i.inv_created_by , i.inv_notes,
                i.inv_deletestate , i.delete_reseon , DATE_FORMAT(i.inv_created_date, '%Y-%m-%d') AS inv_created_date,
                PA.id AS patientid , PA.fname AS PA_fname ,PA.lname AS PA_lname, PA.pubpid,
                f.name AS facilityName, CONCAT(u.fname,' ',u.lname) AS providerName
                FROM invoice AS i
                LEFT JOIN parent_invoice PI ON PI.pinv_inv_id  = i.inv_id
                INNER JOIN patient_data PA ON PA.id=i.inv_pid
                INNER JOIN facility AS f ON f.id = i.inv_clinic_id 
                INNER JOIN users AS u ON u.id = i.inv_created_by
                WHERE " . $where . " AND i.parent_inv_id > 0 ORDER BY i.inv_id DESC ";
    $minInvAmt = $GLOBALS['gbl_ac_audit_invoice_min_amt'];    
    
    ////////////////
    $csvList = sqlStatement($query);
    $num_rows = sqlNumRows($csvList); // total no. of rows
    $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
    $page = $pageSelection;
    if (!$_REQUEST["page"]) {
        $page = 1;
    }
    $prev_page = $page - 1;
    $next_page = $page + 1;
    $page_start = (($per_page * $page) - $per_page);
    if ($num_rows <= $per_page) {
        $num_pages = 1;
    } else if (($num_rows % $per_page) == 0) {
        $num_pages = ($num_rows / $per_page);
    } else {
        $num_pages = ($num_rows / $per_page) + 1;
        $num_pages = (int) $num_pages;
    }
    $query .= " LIMIT $page_start , $per_page";
    //////////////////////
    
    $inLists = sqlStatement($query);
    $auditid = debugADOReports($sql, '', 'Report Invoice Audit Dashboard', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
    if (sqlNumRows($inLists)) {

        $Snumber=$page_start+1;
        while ($urow = sqlFetchArray($inLists)) {
           
            if ($urow['inv_deletestate'] == '2') {
                $invType = "Credit";
            } elseif ($urow['inv_deletestate'] == '1') {
                $invType = "New";
            } else {
                $invType = "Deleted";
            }
            $invoiceId = $urow['inv_id'];
            $parent = getAllParentsInv($invoiceId);
            
            $treatmentplanListByPlangroup = treatmentplanListByPlangroup($invoiceId);
            
            $totalDiscountOld = totalDiscount($invoiceId , $treatmentplanListByPlangroup);
            $discount = $totalDiscountOld['oldDiscount'];
            $cost = $totalDiscountOld['cost'];
            $patientProductSales = patientProductSalesTotalFee($urow['patientid'], $invoiceId, $urow['inv_clinic_id']);
            $amtDue = ($cost + $patientProductSales )- $discount ; 
            
            if($amtDue < 0){
                $amtDueChk = $amtDue *(-1);
            }else{
                $amtDueChk = $amtDue;
            }
            if($amtDueChk >= $minInvAmt){ 
            
            ?>                  <?php 
                                
                                if($_REQUEST['form_csvexport']=='viewStockLedger' || $_REQUEST["page"] >=1){ ?>
                                <tr>
                                <td ><?php echo $Snumber; ?></td>    
                                <td ><?php echo $urow['inv_created_date']; ?></td>
                                <td class="popover_div_test"><?php echo $urow['inv_number']; ?>

                                    <div class="popover">
                                        <ul class="popoverDetailList clearfix">
                                            <li class="clearfix">
                                                <div class="labelPopUp">Patient Name</div>
                                                <div class="valuePopUp" id="urgentPatientName"><?php echo $pname; ?></div>
                                            </li>
                                            <li class="clearfix ">
                                                <div class="labelPopUp">Patient Id</div>
                                                <div class="valuePopUp" id="urgentPatientPID"><?php echo $urow['inv_pid']; ?></div>
                                            </li>
                                            <li class="clearfix">
                                                <div class="labelPopUp">Invoice no.</div>
                                                <div class="valuePopUp" id="urgentBillNumber"><?php echo $urow['inv_number']; ?></div>
                                            </li>
                                            <li class="clearfix popUpBorderBottom">
                                                <div class="labelPopUp">Date</div>
                                                <div class="valuePopUp" id="urgentBillDate"><?php echo $urow['date']; ?></div>
                                            </li>
                                        </ul>
                                        <table id="dashbList">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: left;">Treatment/Product Name</th>
                                                    <th style="text-align: right;">Unit Cost (INR)</th>
                                                    <th style="text-align: center;">Quantity</th>
                                                    <th style="text-align: right;">Discount</th>
                                                    <th style="text-align: right;">Net Cost (INR)</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr>
                                                    <td style="text-align: left;">Consultation</td>
                                                    <td style="text-align: right;">200.00</td>
                                                    <td style="text-align: center;">2</td>
                                                    <td style="text-align: right;">0.00</td>
                                                    <td style="text-align: right;">400.00</td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: left;">Diagnostic casts</td>
                                                    <td style="text-align: right;">350.00</td>
                                                    <td style="text-align: center;">2</td>
                                                    <td style="text-align: right;">0.00</td>
                                                    <td style="text-align: right;">700.00</td>
                                                </tr>





                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td style="text-align: right;" colspan="4"><strong>Total Cost</strong></td>
                                                    <td style="text-align: right;"><span id="invoiceCostSum">1,100.00</span></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div><br /><?php echo ($parent != '') ? 'Parent :' . $parent : ''; ?></td>
                                <td ><?php echo $cost; ?></td>
                                <td ><?php echo $discount; ?></td>
                                <td >
                                <?php
                                echo $amtDue ;

                                ?>
                                </td>

                                <td ><?php echo $invType; ?></td>
                                <td class="reason_pop"><?php echo (qescape($urow['inv_notes']) != '') ? ucfirst(substr(qescape($urow['inv_notes']), 0, 10)) . ".." : ''; ?>
                                    <div class="popover"><?php echo (qescape($urow['inv_notes']) != '') ? ucfirst(qescape($urow['inv_notes'])) : 'No Remark'; ?></div>
                                </td>
                                <td ><?php echo $urow['pubpid']; ?></td>
                                <td ><?php echo $urow['PA_fname'] . " " . $urow['PA_lname']; ?></td>
                                <td ><?php echo $urow['facilityName']; ?></td>	
                                <td ><?php echo $urow['providerName']; ?></td>            
                                
                                </tr> 
                                    <?php
                                }    
            }               
                            $Snumber++;
        }
    }    
       debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']);
}                               if($_REQUEST['form_csvinvoiceexport']=='exportinvoice'){
                                    ob_end_clean();
                                    echo '"' . xl('S. No.') . '",';
                                    echo '"' . xl('Date') . '",';
                                    echo '"' . xl('Invoice') . '",';
                                    echo '"' . xl('Parent Invoice') . '",';
                                    echo '"' . xl('Cost') . '",';
                                    echo '"' . xl('Discount') . '",';
                                    echo '"' . xl('Amount') . '",';
                                    echo '"' . xl('Invoice Type') . '",';
                                    echo '"' . xl('Remarks') . '",';
                                    echo '"' . xl('Patient ID') . '",';
                                    echo '"' . xl('Patient Name') . '",';
                                    echo '"' . xl('Clinic') . '",';
                                    echo '"' . xl('Doctor') . '"' . "\n";
                                        $sno = 1;
                                        while ($crow = sqlFetchArray($csvList)) {
                                            if ($crow['inv_deletestate'] == '2') {
                                                    $invType = "Credit";
                                            } elseif ($crow['inv_deletestate'] == '1') {
                                                    $invType = "New";
                                            } else {
                                                    $invType = "Deleted";
                                            }
                                            $invoiceId = $crow['inv_id'];
                                            $parent = getAllParentsInv($invoiceId);
                                            $treatmentplanListByPlangroup = treatmentplanListByPlangroup($invoiceId);
                                            $totalDiscountOld = totalDiscount($invoiceId , $treatmentplanListByPlangroup);
                                            $discount = $totalDiscountOld['oldDiscount'];
                                            $cost = $totalDiscountOld['cost'];
                                            $patientProductSales = patientProductSalesTotalFee($crow['patientid'], $invoiceId, $crow['inv_clinic_id']);
                                            $amtDue = ($cost + $patientProductSales )- $discount ; 
                                                
                                            echo '"' . qescape($sno) . '",';
                                            echo '"' . qescape($crow['inv_created_date']) . '",';
                                            echo '"' . qescape($crow['inv_number']) . '",';
                                            echo '"' . qescape($parent) . '",';        
                                            echo '"' . qescape($cost) . '",';
                                            echo '"' . qescape($discount) . '",';
                                            echo '"' . qescape($amtDue) . '",';        
                                            echo '"' . qescape($invType) . '",';
                                            echo '"' . qescape($crow['inv_notes']) . '",';                        
                                            echo '"' . qescape($crow['pubpid']) . '",';
                                            echo '"' . qescape($crow['PA_fname'] . " " . $crow['PA_lname']) . '",';                        
                                            echo '"' . qescape($crow['facilityName']) . '",'; 
                                            echo '"' . qescape($crow['providerName']) .'"'."\n";
                                            $sno++;
                                        }
                                }else{
                                ?>
                    </tbody>                        
                </table>
            </div>
            <div>    
                <?php
                if ($num_rows > 0) {
                    ?>
                    <table id="auditingtablepagination" style="margin-top:8px" class="emrpagination">
                        <tbody>
                            <tr>
                                <td class="text" >
                                    <?php
                                    echo "Total Records: $num_rows";
                                    $url = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'] . '&';
                                    echo pagination($num_rows, $per_page, $page, $url);
                                    ?> 
                                </td>
                            </tr>
                        </tbody>                   
                    </table>
                    <?php
                }
                ?> 
            </div>
    </body>

    <!-- stuff for the popup calendar -->
    <style type="text/css">
        @import url(../../library/dynarch_calendar.css);
    </style>
    <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
    <script type="text/javascript"
    src="../../library/dynarch_calendar_setup.js"></script>
    <script type="text/javascript">
                                                Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
                                                Calendar.setup({inputField: "form_to_date", ifFormat: "%Y-%m-%d", button: "img_to_date"});
    </script>
    <script type="text/javascript">
        Calendar.setup({
            inputField: "form_from_date"
            , ifFormat: "%Y-%m-%d"
            , button: "img_from_date"
            , onUpdate: function () {
                var toDate = document.getElementById('form_to_date').value,
                        fromDate = document.getElementById('form_from_date').value;

                if (toDate) {
                    var toDate = new Date(toDate),
                            fromDate = new Date(fromDate);

                    if (fromDate > toDate) {
                        alert('From date cannot be later than To date');
                        document.getElementById('form_from_date').value = '';
                    }
                }
            }
        });
        Calendar.setup({
            inputField: "form_to_date"
            , ifFormat: "%Y-%m-%d"
            , button: "img_to_date"
            , onUpdate: function () {
                var toDate = document.getElementById('form_to_date').value,
                        fromDate = document.getElementById('form_from_date').value;

                if (fromDate) {
                    var toDate = new Date(toDate),
                            fromDate = new Date(fromDate);

                    if (fromDate > toDate) {
                        alert('From date cannot be later than To date');
                        document.getElementById('form_to_date').value = '';
                    }
                }
            }
        });

<?php echo paginationScript(); ?>
    </script>
</div>
</div>
        </body>
        </html>
<?php }?>