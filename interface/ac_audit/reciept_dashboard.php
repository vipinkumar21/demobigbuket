<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
// This program is for PRM software.
ob_start();
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2080M');
require_once("../globals.php");
require_once("../../library/globals.inc.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once ("$audting_webroot/auditlog.php");
include_once("../pagination.php");
$mniRecptAmt = $GLOBALS['gbl_ac_audit_reciept_min_amt'];
$form_facility = $_REQUEST['form_facility'];
$form_provider = trim($_REQUEST['form_provider']);
$form_date = $_REQUEST['form_from_date'];
$form_to_date = $_REQUEST['form_to_date'];
if ($_REQUEST['page']) {
    $pageSelection = $_REQUEST['page'];
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}
?>
<?php
$fileName = "receipt_dashboard_" . date("Ymd_his") . ".csv";
if ($_REQUEST['form_csvrecieptexport'] == 'exportReciept') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>

            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
            <link type="text/css" href="../themes/bootstrap.css" rel="stylesheet">

            <title><?php xl('Reciept Audit - Dashboard', 'e'); ?></title>

            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery-1.8.3.min.js"></script>

            <script type="text/javascript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';

                function dosort(orderby) {
                    var f = document.forms[0];
                    f.form_orderby.value = orderby;
                    f.submit();
                    return false;
                }

                function oldEvt(eventid) {
                    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
                }

                function refreshme() {
                    document.forms[0].submit();
                }


                $(document).ready(function () {
                    var facilityID = $("#form_facility").val();

                    $('.popover_div,.reason_pop').hover(function () {
                        $(this).children('.popover').show();
                    }, function () {
                        $(this).children('.popover').hide();
                    });

                });
                $(document).ready(function () {
                    var selectedRegionAjax = '<?php echo $form_region; ?>';
                    $("#form_region").change(function () {
                        changeRegion();
                    });
                    $("#form_zone").change(function () {
                        changeZone();
                    });
                    var selectedRegionAjax = '<?php echo $form_region; ?>';
                    var selectedZoneAjax = '<?php echo $form_zone; ?>';
                    var selectedFacilityAjax = '<?php echo $form_facility; ?>';
                    if (selectedRegionAjax == '' && selectedZoneAjax != '') {
                        changeZone();
                    } else if (selectedRegionAjax != '' && selectedZoneAjax == '')
                    {
                        changeRegion();
                    } else if (selectedRegionAjax != '' && selectedZoneAjax != '') {
                        changeZone();
                    }
                    function changeRegion()
                    {
                        var regionId = $("#form_region").val();
                        $.ajax({
                            url: "../reports/get_facility_zones.php?regionId=" + regionId + "&facilityId=" + selectedFacilityAjax,
                            success: function (result) {
                                var result = $.parseJSON(result);
                                $("#form_zone").html(result[0]);
                                $("#form_facility").html(result[1]);
                            }
                        });
                    }
                    function changeZone()
                    {
                        var zoneId = $("#form_zone").val();
                        var regionId = $("#form_region").val();
                        $.ajax({
                            url: "../reports/get_facility_by_zones.php?zoneId=" + zoneId + "&facilityId=" + selectedFacilityAjax + "&regionId=" + regionId,
                            success: function (result) {
                                var result = $.parseJSON(result);
                                $("#form_zone").html(result[0]);
                                $("#form_facility").html(result[1]);
                            }
                        });
                    }
                });
                function exportCSV() {
                    document.forms[0].form_csvexport.value = "csvStockLedger";
                }
                function viewRecieptAudit() {
                    document.forms[0].form_csvexport.value = "viewRecieptAudit";
                }
            </script>

            <style type="text/css">
                .popover_div,.reason_pop{position: relative;cursor: pointer;}
                .clearfix:before {
                    clear: both;
                    content: " ";
                    display: block;
                    height: 0;
                    visibility: hidden;
                }
                .clearfix:after {
                    clear: both;
                    content: " ";
                    display: block;
                    height: 0;
                    visibility: hidden;
                }
                .popoverDetailList {
                    background: none repeat scroll 0 0 #fff;
                    list-style: none outside none;
                }
                .popoverDetailList li {
                    float: left;
                    width: 48%;
                    border-bottom: 1px solid #452311;
                    padding: 1%;
                }
                .dashbList th {
                    background: linear-gradient(to bottom, #be631d 0%, #e5791f 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
                    color: #fff;
                    text-shadow: 0 1px 0 #000;
                }
                .dashbList td {
                    background: none repeat scroll 0 0 #fff;
                    border-bottom: 1px solid #431f19;
                }
                .dashbList th, .dashbList td {
                    padding: 0.5em;
                    text-align: left;
                    vertical-align: middle;
                }
                .popover {
                    background:#fff;
                    border: 1px solid #9a460b;
                    border-radius: 0.3em;
                    box-shadow: 0 0 18px 1px rgba(50, 50, 50, 0.75);
                    color: #000;
                    display: none;  word-wrap: break-word; word-break:break-all;
                    position: absolute;
                    line-height:18px;
                    left:0px;
                    padding: 4px;  
                    text-align: center;
                    width: 400px;
                    z-index: 4;
                }
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
            <?php echo paginationCss(); ?>
        </head>
        <body class="body_top">
            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
            <div class="panel panel-warning">
                <div class="panel-heading boldtxt"><?php xl('Reciept Audit', 'e'); ?> - <?php xl('Dashboard', 'e'); ?></div>
                <div class="panel-body">
                    <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
                    </div>

                    <form method='get' name='theform' id='theform' action='reciept_dashboard.php'>
                        <div id="report_parameters">
                            <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                            <input type='hidden' name='form_csvrecieptexport' id='form_csvrecieptexport' value=''/>
                            <input type='hidden' name='form_csvexport' id='form_csvexport' value='viewStockLegder'/>
                            <input type="hidden" name="default_facility" value="<?php echo $form_facility ?>" />
                            <div class="row">
                                <div class="col-md-4">
                                    <?php xl('Region', 'e'); ?>:<?php dropdownRegions(strip_escape_custom($form_region), 'form_region'); ?></div>
                                <div class="col-md-4">
                                    <?php xl('Zone', 'e'); ?>:<?php dropdownZones(strip_escape_custom($form_zone), 'form_zone'); ?></div>
                                <div class="col-md-4">
                                    <?php xl('Facility', 'e'); ?>:<?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php xl('Provider', 'e'); ?>:
                                    <input class='form-control input-sm' type='text' name='form_provider' id="form_provider" size='5' value='<?php echo $form_provider; ?>'>
                                </div>
                                <div class="col-md-4"><?php xl('By Reciept', 'e'); ?>:<input class='form-control input-sm' type='text' name='reciept' id="reciept" size='5' value='<?php echo $reciept ?>'></div>
                                <div class="col-md-4"><?php xl('By Reciept Type', 'e'); ?>:<select class='form-control input-sm' name="rect_type" id="rect_type">
                                        <option value="">-Select Receipt Type-</option>
                                        <option value="Credit" <?php echo($_REQUEST['rect_type'] == 'Credit') ? 'selected="selected"' : ''; ?>>Credit</option>
                                        <option value="Advance" <?php echo($_REQUEST['rect_type'] == 'Advance') ? 'selected="selected"' : ''; ?>>Advance</option>
                                        <option value="1" <?php echo($_REQUEST['rect_type'] == '1') ? 'selected="selected"' : ''; ?>>Deleted</option> 
                                        <option value="Payment" <?php echo($_REQUEST['rect_type'] == 'Payment') ? 'selected="selected"' : ''; ?>>New</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"><?php xl('By Patient', 'e'); ?>:<input class='form-control input-sm' type='text' name='patient' id="patient" size='5' value='<?php echo $patient ?>'></div>
                                <div class="col-md-4"><?php xl('From', 'e'); ?>:<br/><input type='text' name='form_from_date' id="form_from_date" size='10' value='<?php echo $form_date ?>' onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                                            title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                                            align='absbottom' width='24' height='22' id='img_from_date'
                                                                                            border='0' alt='[?]' style='cursor: pointer'
                                                                                            title='<?php xl('Click here to choose a date', 'e'); ?>'></div>
                                <div class="col-md-4"><?php xl('To', 'e'); ?>:<br/><input type='text' name='form_to_date' id="form_to_date"
                                                                                          size='10' value='<?php echo $form_to_date ?>'
                                                                                          onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                                          title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                                          align='absbottom' width='24' height='22' id='img_to_date'
                                                                                          border='0' alt='[?]' style='cursor: pointer'
                                                                                          title='<?php xl('Click here to choose a date', 'e'); ?>'></div>
                                
                            </div>

                        <div style="margin-top:15px;"><a href='#'  class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewRecieptAudit");
                                            $("#form_csvrecieptexport").attr("value", "");
                                            $("#theform").submit();'><span> <?php xl('Submit', 'e'); ?> </span> </a>
                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvrecieptexport").attr("value", "exportReciept");
                                                $("#theform").submit();'>
                                        <span>
                                            <?php xl('Export to CSV', 'e'); ?>
                                        </span>
                                    </a>   
                                </div>

                        </div>
                    </form>
                    <!-- </div> -->
                    <!-- </div> -->
                    <!-- end of search parameters --> 


                    <?php
                }
                if (!($_REQUEST['form_refresh'] || $_REQUEST['form_csvexport'] || $_REQUEST['form_csvrecieptexport'] || $_REQUEST['page'])) {
                    ?>   
                    <div>
                        * Please select a filter to see the results.
                    </div>
                <?php } ?>       
                <?php
                if ($_REQUEST['form_refresh'] || $_REQUEST['form_csvexport'] || $_REQUEST['form_csvrecieptexport'] || $_REQUEST['page']) {

                    if ($form_facility) {
                        //for individual facility
                        if ($form_date) {
                            //if from date selected
                            if ($form_to_date) {
                                // if $form_date && $form_to_date
                                $toDate = date_create(date($form_to_date));
                                $fromDate = date_create($form_date);
                                $diff = date_diff($fromDate, $toDate);
                                $days_between_from_to = $diff->days;


                                if ($days_between_from_to <= $form_facility_time_range) {

                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";

                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_clinic_id = '$form_facility'";
                                } else {
                                    $msgForReportLog = "Only $form_facility_time_range day's report is applicable!";
                                }
                            } else {
                                //only from date: no TO date
                                $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
                                $toDate = date_create(date($form_to_date));
                                $fromDate = date_create($form_date);
                                $diff = date_diff($fromDate, $toDate);
                                $days_between_from_to = $diff->days;

                                if ($days_between_from_to <= $form_facility_time_range) {
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_clinic_id = '$form_facility'";
                                } else {
                                    $msgForReportLog = "Only $form_facility_time_range day's report is applicable!";
                                }
                            }
                        } else {
                            // if from date not selected
                            if ($form_to_date) {
                                $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                $toDate = date_create(date($form_to_date));
                                $fromDate = date_create($form_date);
                                $diff = date_diff($fromDate, $toDate);
                                $days_between_from_to = $diff->days;

                                if ($days_between_from_to <= $form_facility_time_range) {
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_clinic_id = '$form_facility'";
                                } else {
                                    $msgForReportLog = "Only $form_facility_time_range day's report is applicable!";
                                }
                            } else {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $form_to_date = date("Y-m-d");
                                $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
                                $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $where .= " r.rect_clinic_id = '$form_facility'";
                            }
                        }

                        $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param
                    } else {
                        if ($form_region) {
                            $where.=(!empty($where)) ? " AND f.region_id='" . $form_zone . "'" : " f.region_id='" . $form_region . "'";
                        }
                        if ($form_zone) {
                            $where.=(!empty($where)) ? " AND f.zone_id='" . $form_zone . "'" : " f.zone_id='" . $form_zone . "'";
                        }
                        //for all facility
                        if ($form_date) {
                            //if from date selected
                            if ($form_to_date) {
                                // if $form_date && $form_to_date
                                $toDate = date_create(date($form_to_date));
                                $fromDate = date_create($form_date);
                                $diff = date_diff($fromDate, $toDate);
                                $days_between_from_to = $diff->days;

                                if ($days_between_from_to <= $form_facility_all_time_range) {

                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                } else {
                                    ###########Cron Request####################section only for all FACILITY#########################
                                    // following value will be change according to report
                                    $rcsl_name_type = "audit_dashboard_rect_report"; // changable
                                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $form_provider, "facRegionId" => $form_region, "facZoneId" => $form_zone))); // changable
                                    $rcsl_report_description = "Audit Dashboard Reciept report from $form_date to $form_to_date"; // changable
                                    //allFacilityReports() defined with globals.php
                                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                    }
                                    ##########Cron Request####################section only for all FACILITY#########################
                                    echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
                                    exit();
                                }
                            } else {
                                //only from date: no TO date
                                $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
                                $toDate = date_create(date($form_to_date));
                                $fromDate = date_create($form_date);
                                $diff = date_diff($fromDate, $toDate);
                                $days_between_from_to = $diff->days;

                                if ($days_between_from_to <= $form_facility_all_time_range) {
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                } else {
                                    ###########Cron Request####################section only for all FACILITY#########################
                                    // following value will be change according to report
                                    $rcsl_name_type = "audit_dashboard_rect_report"; // changable
                                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $form_provider, "facRegionId" => $form_region, "facZoneId" => $form_zone))); // changable
                                    $rcsl_report_description = "Audit Dashboard Reciept report from $form_date to $form_to_date"; // changable
                                    //allFacilityReports() defined with globals.php
                                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                    }
                                    ##########Cron Request####################section only for all FACILITY#########################
                                    echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
                                    exit();
                                }
                            }
                        } else {
                            // if from date not selected
                            if ($form_to_date) {
                                $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                $toDate = date_create(date($form_to_date));
                                $fromDate = date_create($form_date);
                                $diff = date_diff($fromDate, $toDate);
                                $days_between_from_to = $diff->days;

                                if ($days_between_from_to <= $form_facility_all_time_range) {
                                    if ($where) {
                                        $where .= " AND ";
                                    }
                                    $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                                } else {
                                    ############Cron Request####################section only for all FACILITY#########################
                                    // following value will be change according to report
                                    $rcsl_name_type = "audit_dashboard_rect_report"; // changable
                                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "provider" => $form_provider, "facRegionId" => $form_region, "facZoneId" => $form_zone))); // changable
                                    $rcsl_report_description = "Audit Dashboard Reciept report from $form_date to $form_to_date"; // changable
                                    //allFacilityReports() defined with globals.php
                                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                    if ($msgForReportLog) {
                                        $msgForReportLog;
                                    } else {
                                        $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                    }
                                    ##########Cron Request####################section only for all FACILITY#########################
                                    echo "<tr><td colspan=\"7\" align=\"center\">$msgForReportLog</td> </tr>";
                                    exit();
                                }
                            } else {
                                if ($where) {
                                    $where .= " AND ";
                                }
                                $form_to_date = date("Y-m-d");
                                $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
                                $where .= " r.rect_created_date BETWEEN '$form_date 00:00:00' AND '$form_to_date 23:59:59'";
                            }
                        }
                        $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param
                    }

                    if (isset($form_provider) && $form_provider != '') {
                        if ($where) {
                            $where .= " AND ";
                        }
                        $where .= " ((u.fname LIKE '%" . $form_provider . "%') OR  (u.lname LIKE '%" . $form_provider . "%')) ";
                        $searchParam .= ' Provider = ' . $provider . ' | ';
                    } else {
                        $searchParam .= ' Provider = All | ';
                    }

                    if (!$where) {
                        $where = " 1 = 1";
                    }

                    if (isset($_REQUEST['reciept']) && $_REQUEST['reciept'] != '') {
                        $where .= " AND r.rect_number LIKE '%" . trim($_REQUEST['reciept']) . "%'";
                        $searchParam .= ' Reciept = ' . trim($_REQUEST['reciept']) . ' | ';
                    }
                    if (isset($_REQUEST['rect_type']) && $_REQUEST['rect_type'] != '') {
                        if ($_REQUEST['rect_type'] == '1') {
                            $where .= " AND r.rect_deletestate = '" . $_REQUEST['rect_deletestate'] . "'";
                            $searchParam .= ' Reciept Type = ' . $_REQUEST['rect_deletestate'] . ' | ';
                        } elseif ($_REQUEST['rect_type'] == 'Advance' || $_REQUEST['rect_type'] == 'Payment') {
                            $where .= " AND r.rect_type = '" . $_REQUEST['rect_type'] . "' AND r.rect_deletestate = '1'";
                            $searchParam .= ' Reciept Type = ' . $_REQUEST['rect_type'] . ' | ';
                        } else {
                            $where .= " AND r.rect_type = '" . $_REQUEST['rect_type'] . "'";
                            $searchParam .= ' Reciept Type = ' . $_REQUEST['rect_type'] . ' | ';
                        }
                    }

                    if (isset($_REQUEST['patient']) && $_REQUEST['patient'] != '') {
                        $where .= " AND (PA.fname LIKE '%" . trim($_REQUEST['patient']) . "%' OR PA.lname LIKE '%" . trim($_REQUEST['patient']) . "%' OR PA.pubpid LIKE '%" . trim($_REQUEST['patient']) . "%')";
                        $searchParam .= ' Patient Name = ' . trim($_REQUEST['patient']) . ' | ';
                    }

                    if (($_REQUEST['form_csvexport'] == 'viewRecieptAudit') || $_REQUEST['page']) {
                        ?>
                        <div id="report_results" class="table-responsive">
                            <table border="0" cellpadding="1" cellspacing="0" width="100%" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?php xl('S.No', 'e'); ?></th>
                                        <th><?php xl('Date', 'e'); ?></th>
                                        <th><?php xl('#Receipt', 'e'); ?></th>
                                        <th><?php xl('Amount Paid', 'e'); ?></th>
                                        <th><?php xl('Receipt Type', 'e'); ?></th>
                                        <th><?php xl('Payment Method', 'e'); ?></th>
                                        <th><?php xl('Remark', 'e'); ?></th>
                                        <th><?php xl('Patient Id', 'e'); ?></th>
                                        <th><?php xl('Patient Name', 'e'); ?></th>
                                        <th><?php xl('Clinic', 'e'); ?></th>		
                                        <th><?php xl('Doctor', 'e'); ?></th>  
                                    </tr>              
                                </thead>
                                <tbody>
                                    <tr class="detail"> 
                                        <?php
                                    }
                                    $query = "SELECT r.rect_id, r.rect_created_date, r.rect_created_date, r.rect_number,
                                                r.rect_amount, ROUND(r.rect_amount) AS roundrect_amount, r.rect_created_by ,
                                                r.rect_mode,r.rect_remarks, r.rect_deletestate, r.rect_clinic_id, i.inv_id, 
                                                i.inv_number , r.rect_type, r.collected_type , r.released_inv_id, r.delete_reseon ,
                                                CASE WHEN r.released_inv_id IS NOT NULL 
                                                     THEN 
                                                       (SELECT inv_number FROM invoice WHERE inv_id = r.released_inv_id )
                                                     ELSE r.released_inv_id 
                                                   END releasedInvoiceNumber,
                                                CASE WHEN r.rect_prect_id IS NOT NULL 
                                                     THEN 
                                                       (SELECT rect_number FROM reciept WHERE rect_id = r.rect_prect_id )
                                                     ELSE r.rect_prect_id 
                                                  END releasedReceiptNumber,
                                                PA.fname AS PA_fname ,PA.lname AS PA_lname ,PA.pubpid, f.name AS facilityName, 
                                                CONCAT(u.fname,' ',u.lname) AS providerName
                                                FROM reciept AS r 
                                                LEFT JOIN invoice_reciepts AS ir ON r.rect_id = ir.invrect_rect_id 
                                                LEFT JOIN invoice AS i ON i.inv_id = ir.invrect_inv_id                       
                                                INNER JOIN patient_data PA ON PA.id=r.rect_pid  
                                                INNER JOIN facility AS f ON f.id = r.rect_clinic_id 
                                                INNER JOIN users AS u ON u.id = r.rect_created_by
                                                WHERE " . $where . "  ORDER BY r.rect_id DESC ";

                                    ////////////////
                                    $csvLists = sqlStatement($query);
                                    $num_rows = sqlNumRows($csvLists); // total no. of rows
                                    $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                    $page = $pageSelection;
                                    if (!$_REQUEST["page"]) {
                                        $page = 1;
                                    }
                                    $prev_page = $page - 1;
                                    $next_page = $page + 1;
                                    $page_start = (($per_page * $page) - $per_page);
                                    if ($num_rows <= $per_page) {
                                        $num_pages = 1;
                                    } else if (($num_rows % $per_page) == 0) {
                                        $num_pages = ($num_rows / $per_page);
                                    } else {
                                        $num_pages = ($num_rows / $per_page) + 1;
                                        $num_pages = (int) $num_pages;
                                    }
                                    $query .= " LIMIT $page_start , $per_page";

                                    ////////////////
                                    // echo $query; //exit;
                                    $auditid = debugADOReports($sql, '', 'Report Reciept Audit Dashboard', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
                                    $inLists = sqlStatement($query);
                                    $num_row = sqlNumRows($inLists);
                                    if ($num_row) {
                                        $dataCsvMade = array();
                                        $dataCsvPush = array();
                                        $Snumber = $page_start + 1;
                                        while ($urow = sqlFetchArray($inLists)) {

                                            if ($urow['rect_amount'] < 0) {
                                                $recptAmt = $urow['rect_amount'] * (-1);
                                            } else {
                                                $recptAmt = $urow['rect_amount'];
                                            }
                                            if ($urow['rect_type'] == 'Payment' && $urow['rect_deletestate'] == '1') {
                                                $rectType = 'New';
                                            } elseif ($urow['rect_type'] == 'Advance' && $urow['rect_deletestate'] == '1') {
                                                $rectType = 'Advance';
                                            } elseif ($urow['rect_type'] == 'Credit') {
                                                $rectType = 'Credit';
                                            } else {
                                                $rectType = 'Deleted';
                                            }
                                            if ($recptAmt >= $mniRecptAmt) {
                                                ?>
                                                <?php if (($_REQUEST['form_csvexport'] == 'viewRecieptAudit') || $_REQUEST['page']) { ?>
                                                <tr class="detail">
                                                    <td ><?php echo $Snumber; ?></td>
                                                    <td ><?php echo $urow['rect_created_date']; ?></td>
                                                    <td class="popover_div_test"><?php echo $urow['rect_number']; ?><br />
                                                        <?php
                                                        echo ($urow['releasedReceiptNumber'] != '') ? 'Parent:' . $urow['releasedReceiptNumber'] : '';
                                                        echo ($urow['releasedInvoiceNumber'] != '') ? '<br /> Invoice:' . $urow['releasedInvoiceNumber'] : '';
                                                        ?>
                                                        <div class="popover">
                                                            <ul class="popoverDetailList clearfix">
                                                                <li class="clearfix">
                                                                    <div class="labelPopUp">Patient Name</div>
                                                                    <div class="valuePopUp" id="urgentPatientName"><?php echo $pname; ?></div>
                                                                </li>
                                                                <li class="clearfix ">
                                                                    <div class="labelPopUp">Patient Id</div>
                                                                    <div class="valuePopUp" id="urgentPatientPID"><?php echo $urow['inv_pid']; ?></div>
                                                                </li>
                                                                <li class="clearfix">
                                                                    <div class="labelPopUp">Invoice no.</div>
                                                                    <div class="valuePopUp" id="urgentBillNumber"><?php echo $urow['inv_number']; ?></div>
                                                                </li>
                                                                <li class="clearfix popUpBorderBottom">
                                                                    <div class="labelPopUp">Date</div>
                                                                    <div class="valuePopUp" id="urgentBillDate"><?php echo $urow['date']; ?></div>
                                                                </li>
                                                            </ul>
                                                            <table id="dashbList">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="text-align: left;">Treatment/Product Name</th>
                                                                        <th style="text-align: right;">Unit Cost (INR)</th>
                                                                        <th style="text-align: center;">Quantity</th>
                                                                        <th style="text-align: right;">Discount</th>
                                                                        <th style="text-align: right;">Net Cost (INR)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="text-align: left;">Consultation</td>
                                                                        <td style="text-align: right;">200.00</td>
                                                                        <td style="text-align: center;">2</td>
                                                                        <td style="text-align: right;">0.00</td>
                                                                        <td style="text-align: right;">400.00</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: left;">Diagnostic casts</td>
                                                                        <td style="text-align: right;">350.00</td>
                                                                        <td style="text-align: center;">2</td>
                                                                        <td style="text-align: right;">0.00</td>
                                                                        <td style="text-align: right;">700.00</td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td style="text-align: right;" colspan="4"><strong>Total Cost</strong></td>
                                                                        <td style="text-align: right;"><span id="invoiceCostSum">1,100.00</span></td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div></td>
                                                    <td>
                                                        <?php echo qescape($urow['rect_amount']); ?>
                                                    </td>
                                                    <td>
                                                        <?php echo qescape($rectType); ?>
                                                    </td>
                                                    <td>
                                                        <?php echo qescape($urow['rect_mode']); ?>
                                                    </td>
                                                    <td class="reason_pop"><?php echo (qescape($urow['rect_remarks']) != '') ? ucfirst(substr(qescape($urow['rect_remarks']), 0, 10)) . ".." : ''; ?>
                                                        <div class="popover"><?php echo (qescape($urow['rect_remarks']) != '') ? ucfirst(qescape($urow['rect_remarks'])) : 'No Remark'; ?></div>
                                                    </td>
                                                    <td ><?php echo qescape($urow['pubpid']); ?></td>
                                                    <td ><?php echo qescape($urow['PA_fname'] . " " . $urow['PA_lname']); ?></td>
                                                    <td ><?php echo qescape($urow['facilityName']); ?></td>	
                                                    <td ><?php echo qescape($urow['providerName']); ?></td>            
                                                <tr> 

                                                    <?php
                                                }
                                            }
                                            $dataCsvMade['rect_created_date'] = qescape($urow['rect_created_date']);
                                            $dataCsvMade['rect_number'] = qescape($urow['rect_number']);
                                            $dataCsvMade['releasedReceiptNumber'] = qescape($urow['releasedReceiptNumber']);
                                            $dataCsvMade['releasedInvoiceNumber'] = qescape($urow['releasedInvoiceNumber']);
                                            $dataCsvMade['rect_amount'] = qescape($urow['rect_amount']);
                                            $dataCsvMade['rect_type'] = qescape($rectType);
                                            $dataCsvMade['rect_mode'] = qescape($urow['rect_mode']);
                                            $dataCsvMade['delete_reseon'] = qescape($urow['delete_reseon']);
                                            $dataCsvMade['pubpid'] = qescape($urow['pubpid']);
                                            $dataCsvMade['patientName'] = qescape($urow['PA_fname'] . " " . $urow['PA_lname']);
                                            $dataCsvMade['clinicName'] = qescape($name);
                                            $dataCsvMade['drName'] = qescape($dname);
                                            array_push($dataCsvPush, $dataCsvMade);
                                            $Snumber++;
                                        }
                                    }
                                    debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']);
                                }
                                if ($_REQUEST['form_csvrecieptexport'] == 'exportReciept') {
                                    ob_end_clean();
                                    echo '"' . xl('S. No.') . '",';
                                    echo '"' . xl('Date') . '",';
                                    echo '"' . xl('Receipt No') . '",';
                                    echo '"' . xl('Parent Receipt No') . '",';
                                    echo '"' . xl('Parent Invoice No') . '",';
                                    echo '"' . xl('Amount Paid') . '",';
                                    echo '"' . xl('Receipt Type') . '",';
                                    echo '"' . xl('Payment Method') . '",';
                                    echo '"' . xl('Remarks') . '",';
                                    echo '"' . xl('Patient Id') . '",';
                                    echo '"' . xl('Patient Name') . '",';
                                    echo '"' . xl('Clinic') . '",';
                                    echo '"' . xl('Doctor') . '"' . "\n";
                                    $sno = 1;

                                    while ($crow = sqlFetchArray($csvLists)) {
                                        if ($crow['rect_amount'] < 0) {
                                            $recptAmt = $crow['rect_amount'] * (-1);
                                        } else {
                                            $recptAmt = $crow['rect_amount'];
                                        }
                                        if ($crow['rect_type'] == 'Payment' && $crow['rect_deletestate'] == '1') {
                                            $rectType = 'New';
                                        } elseif ($crow['rect_type'] == 'Advance' && $crow['rect_deletestate'] == '1') {
                                            $rectType = 'Advance';
                                        } elseif ($crow['rect_type'] == 'Credit') {
                                            $rectType = 'Credit';
                                        } else {
                                            $rectType = 'Deleted';
                                        }

                                        echo '"' . qescape($sno) . '",';
                                        echo '"' . qescape($crow['rect_created_date']) . '",';
                                        echo '"' . qescape($crow['rect_number']) . '",';
                                        echo '"' . qescape($crow['releasedReceiptNumber']) . '",';
                                        echo '"' . qescape($crow['releasedInvoiceNumber']) . '",';
                                        echo '"' . qescape($crow['rect_amount']) . '",';
                                        echo '"' . qescape($rectType) . '",';
                                        echo '"' . qescape($crow['rect_mode']) . '",';
                                        echo '"' . qescape($crow['rect_remarks']) . '",';
                                        echo '"' . qescape($crow['pubpid']) . '",';
                                        echo '"' . qescape($crow['PA_fname'] . " " . $crow['PA_lname']) . '",';
                                        echo '"' . qescape($crow['facilityName']) . '",';
                                        echo '"' . qescape($crow['providerName']) . '"' . "\n";
                                        $sno++;
                                    }
                                } else {
                                    ?> 
                            </tbody>
                        </table>
                    </div>
                    <?php
                    if ($num_rows > 0) {
                        ?>
                        <table id="auditingtablepagination" style="margin-top:8px" class="emrpagination">
                            <tbody>
                                <tr>
                                    <td class="text" >
                                        <?php
                                        echo "Total Records: $num_rows";
                                        $url = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'] . '&';
                                        echo pagination($num_rows, $per_page, $page, $url);
                                        ?> 
                                    </td>
                                </tr>
                            </tbody>                   
                        </table>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </body>

        <!-- stuff for the popup calendar -->
        <style type="text/css">
            @import url(../../library/dynarch_calendar.css);
        </style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
        <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript"
        src="../../library/dynarch_calendar_setup.js"></script>
        <script type="text/javascript">
                                        Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
                                        Calendar.setup({inputField: "form_to_date", ifFormat: "%Y-%m-%d", button: "img_to_date"});
        </script>
        <script type="text/javascript">
            Calendar.setup({
                inputField: "form_from_date"
                , ifFormat: "%Y-%m-%d"
                , button: "img_from_date"
                , onUpdate: function () {
                    var toDate = document.getElementById('form_to_date').value,
                            fromDate = document.getElementById('form_from_date').value;

                    if (toDate) {
                        var toDate = new Date(toDate),
                                fromDate = new Date(fromDate);

                        if (fromDate > toDate) {
                            alert('From date cannot be later than To date');
                            document.getElementById('form_from_date').value = '';
                        }
                    }
                }
            });
            Calendar.setup({
                inputField: "form_to_date"
                , ifFormat: "%Y-%m-%d"
                , button: "img_to_date"
                , onUpdate: function () {
                    var toDate = document.getElementById('form_to_date').value,
                            fromDate = document.getElementById('form_from_date').value;

                    if (fromDate) {
                        var toDate = new Date(toDate),
                                fromDate = new Date(fromDate);

                        if (fromDate > toDate) {
                            alert('From date cannot be later than To date');
                            document.getElementById('form_to_date').value = '';
                        }
                    }
                }
            });
    <?php echo paginationScript(); ?>
        </script>
    </html>
<?php } ?>                