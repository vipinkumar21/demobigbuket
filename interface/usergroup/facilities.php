<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");

$alertmsg = '';
//echo "<pre>add";print_r($_POST);
/*		Inserting New facility					*/
if (isset($_POST["mode"]) && $_POST["mode"] == "facility" && $_POST["newmode"] != "admin_facility") {
  $latLong = explode(',', trim(formData('lat_long'))); 
  $insert_id=sqlInsert("INSERT INTO facility SET " .
  "name = '"         . trim(formData('facility'    )) . "', " .
  "phone = '"        . trim(formData('phone'       )) . "', " .
  "mobile = '"        . trim(formData('mobile'       )) . "', " .
  "fax = '"          . trim(formData('fax'         )) . "', " .
  "street = '"       . trim(formData('street'      )) . "', " .
  "city = '"         . trim(formData('city'        )) . "', " .
  "state = '"        . trim(formData('state'       )) . "', " .
  "postal_code = '"  . trim(formData('postal_code' )) . "', " .
  "country_code = '" . trim(formData('country_code')) . "', " .
  "federal_ein = '"  . trim(formData('federal_ein' )) . "', " .
  "website = '"      . trim(formData('website'     )) . "', " .
  "email = '"      	 . trim(formData('email'       )) . "', " .
  "color = '"  . trim(formData('ncolor' )) . "', " .
  "service_location = '"  . trim(formData('service_location' )) . "', " .
  "billing_location = '"  . trim(formData('billing_location' )) . "', " .
  "accepts_assignment = '"  . trim(formData('accepts_assignment' )) . "', " .
  "pos_code = '"  . trim(formData('pos_code' )) . "', " .
  "domain_identifier = '"  . trim(formData('domain_identifier' )) . "', " .
  "attn = '"  . trim(formData('attn' )) . "', " .
  "tax_id_type = '"  . trim(formData('tax_id_type' )) . "', " .
  "primary_business_entity = '"  . trim(formData('primary_business_entity' )) . "', ".
  "facility_npi = '" . trim(formData('facility_npi')) . "', ".
  "work_off_day = '" . trim(formData('work_off_day')) . "', ".
  "weekly_off_enabled = '" . trim(formData('weekly_off_enabled')) . "', ".
  "operatory = '" . trim(formData('operatory')) . "', ".
  "start_hour = '" . trim(formData('start_hour')) . "', ".
  "end_hour = '" . trim(formData('end_hour')) . "', ".
  "latitude = '" . trim($latLong[0]) . "', ".
  "longitude = '" . trim($latLong[1]) . "', ".
  "mask_patient_id_pre = '" . trim(formData('mask_patient_id_pre')) . "', ".
  "mask_patient_id_seed = '" . trim(formData('mask_patient_id_seed')) . "', ".
  "mask_invoice_number_pre = '" . trim(formData('mask_invoice_number_pre')) . "', ".
  "mask_invoice_number_seed = '" . trim(formData('mask_invoice_number_seed')) . "', ".
  "mask_reciept_number_pre = '" . trim(formData('mask_reciept_number_pre')) . "', ".
  "mask_reciept_number_seed = '" . trim(formData('mask_reciept_number_seed')) . "', ".
  "overbooking_enabled = '" . trim(formData('overbooking_enabled')) . "', ".
  "overbooking_seats = '" . trim(formData('overbooking_seats'))  . "', ".
  "treatment_group = '" . trim(formData('form_tr_group'))  . "'");
  /*********** OverBooking Field Added (Swati) On 01/07/2014 ***********/
  $insert_seed_id=sqlInsert("INSERT INTO facility_ids SET " .
  		"fi_fid = '". trim($insert_id) . "', " .
  		"fi_pseq = '1', " .
  		"fi_inseq = '1', " .
  		"fi_rseq = '1'");
  $result = sqlStatement("SELECT `hol_id`, `hol_date`, `hol_name`, `hol_is_recc`, `hol_year`, `hol_month`, `hol_day`, `hol_created_date`, `hol_created_by` FROM global_holidays WHERE hol_year >= ".date('Y'));
  if(sqlNumRows($result)) {
  	while($erow = sqlFetchArray($result)){
  		$fhol_id = sqlInsert("insert into facility_holidays set " .
  				"fhol_facility_id = '"         . trim($insert_id) .
  				"', fhol_date = '"         . trim($erow['hol_date']) .
  				"', fhol_name = '"      . trim(mysql_real_escape_string($erow['hol_name'])) .
  				"', fhol_is_recc = '"         . trim($erow['hol_is_recc']) .
  				"', fhol_year = '"         . trim($erow['hol_year']) .
  				"', fhol_month = '"         . trim($erow['hol_month']) .
  				"', fhol_day = '"         . trim($erow['hol_day']) .
  				"', fhol_created_by = '"         . $_SESSION['authId'] .
  				"', fhol_created_date = '" . date('Y-m-d') .
  				"', fhol_hol_id = '" . $erow['hol_id'] .
  				"'");
  	}
  }
}

/*		Editing existing facility					*/
if ($_POST["mode"] == "facility" && $_POST["newmode"] == "admin_facility")
{
	$latLong = explode(',', trim(formData('lat_long')));
	sqlStatement("update facility set
		name='" . trim(formData('facility')) . "',
		phone='" . trim(formData('phone')) . "',
		mobile='" . trim(formData('mobile')) . "',
		fax='" . trim(formData('fax')) . "',
		street='" . trim(formData('street')) . "',
		city='" . trim(formData('city')) . "',
		state='" . trim(formData('state')) . "',
		postal_code='" . trim(formData('postal_code')) . "',
		country_code='" . trim(formData('country_code')) . "',
		federal_ein='" . trim(formData('federal_ein')) . "',
		website='" . trim(formData('website')) . "',
		email='" . trim(formData('email')) . "',
		color='" . trim(formData('ncolor')) . "',
		service_location='" . trim(formData('service_location')) . "',
		billing_location='" . trim(formData('billing_location')) . "',
		accepts_assignment='" . trim(formData('accepts_assignment')) . "',
		pos_code='" . trim(formData('pos_code')) . "',
		domain_identifier='" . trim(formData('domain_identifier')) . "',
		facility_npi='" . trim(formData('facility_npi')) . "',
		attn='" . trim(formData('attn')) . "' ,
		primary_business_entity='" . trim(formData('primary_business_entity')) . "' ,
		tax_id_type='" . trim(formData('tax_id_type')) . "' ,
		work_off_day='" . trim(formData('work_off_day')) . "' ,
		weekly_off_enabled = '" . trim(formData('weekly_off_enabled')) . "', 
		operatory='" . trim(formData('operatory')) . "' ,
		start_hour='" . trim(formData('start_hour')) . "' ,
		end_hour='" . trim(formData('end_hour')) . "' ,
		latitude='" . trim($latLong[0]) . "' ,
		longitude='" . trim($latLong[1]) . "' ,
		mask_patient_id_pre='" . trim(formData('mask_patient_id_pre')) . "' ,
		mask_patient_id_seed='" . trim(formData('mask_patient_id_seed')) . "' ,
		mask_invoice_number_pre='" . trim(formData('mask_invoice_number_pre')) . "' ,
		mask_invoice_number_seed='" . trim(formData('mask_invoice_number_seed')) . "' ,
		mask_reciept_number_pre='" . trim(formData('mask_reciept_number_pre')) . "' ,
		mask_reciept_number_seed='" . trim(formData('mask_reciept_number_seed')) . "',
		overbooking_enabled = '" . trim(formData('overbooking_enabled')) . "' ,
  		overbooking_seats = '" . trim(formData('overbooking_seats'))  . "' ,
  		treatment_group = '" . trim(formData('form_tr_group'))  . "'    
	where id='" . trim(formData('fid')) . "'" );
	/*********** OverBooking Field Added (Swati) On 01/07/2014 ***********/
}

?>
<html>
<head>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>

<script type="text/javascript">


$(document).ready(function(){

    // fancy box
    enable_modals();

    // special size for
	$(".addfac_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 460,
		'frameWidth' : 650
	});

    // special size for
	$(".medium_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 460,
		'frameWidth' : 650
	});

});

</script>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel=stylesheet href="../themes/bootstrap.css" type="text/css">
</head>

<body class="body_top">

<div class="panel panel-warning">
    <div class="panel-heading">
 <?php xl('Facilities','e'); ?> <a href="facilities_add.php" class="iframe addfac_modal btn btn-default btn-sm"><span><?php xl('Add','e');?></span></a>
    </div>
   <div class="panel-body">
        <div>
<table cellpadding="1" cellspacing="0" class="table table-bordered">
	<tr class="showborder_head" height="22">
		<th style="border-style:1px solid #000" width="140px"><?php xl('Name','e'); ?></th>
		<th style="border-style:1px solid #000" width="320px"><?php xl('Address','e'); ?></th>
		<th style="border-style:1px solid #000"><?php xl('Phone','e'); ?></th>
    </tr>
     <?php
        $fres = 0;
        $query = "select * from facility order by name";
        $sql_num = sqlStatement($query) ;	// total no. of rows
        $num_rows = sqlNumRows($sql_num);
        
        $per_page = $GLOBALS['encounter_page_size'];		// Pagination variables processing
        $page = $_GET["page"];
        
        if(!$_GET["page"])
        {
        	$page=1;
        }
        
        $prev_page = $page-1;
        $next_page = $page+1;
        
        $page_start = (($per_page*$page)-$per_page);
        if($num_rows<=$per_page)
        {
        	$num_pages =1;
        }
        else if(($num_rows % $per_page)==0)
        {
        	$num_pages =($num_rows/$per_page) ;
        }
        else
        {
        	$num_pages =($num_rows/$per_page)+1;
        	$num_pages = (int)$num_pages;
        }
        
        $query .= " LIMIT $page_start , $per_page ";	// limit as per pagination
        $fres = sqlStatement($query);
        if ($fres) {
          $result2 = array();
          for ($iter3 = 0;$frow = sqlFetchArray($fres);$iter3++)
            $result2[$iter3] = $frow;
          foreach($result2 as $iter3) {
			$varstreet="";//these are assigned conditionally below,blank assignment is done so that old values doesn't get propagated to next level.
			$varcity="";
			$varstate="";
          $varstreet=$iter3{street };
          if ($iter3{street }!="")$varstreet=$iter3{street }.",";
          if ($iter3{city}!="")$varcity=$iter3{city}.",";
          if ($iter3{state}!="")$varstate=$iter3{state}.",";
    ?>
    <tr height="22">
       <td valign="top" class="text"><b><a href="facility_admin.php?fid=<?php echo $iter3{id};?>" class="iframe medium_modal"><span><?php echo htmlspecialchars($iter3{name});?></span></a></b>&nbsp;</td>
       <td valign="top" class="text"><?php echo htmlspecialchars($varstreet.$varcity.$varstate.$iter3{country_code}." ".$iter3{postal_code}); ?>&nbsp;</td>
       <td><?php echo htmlspecialchars($iter3{phone});?>&nbsp;</td>
       <td><a href='facility_holiday_list.php?fid=<?php echo $iter3{id};?>' class='iframe medium_modal' onclick='top.restoreSession()'>Holiday List</a>&nbsp;</td>
    </tr>
<?php
  }
}
 if (count($result2)<=0)
  {?>
  <tr height="25">
		<td colspan="3"  style="text-align:center;font-weight:bold;"> <?php echo xl( "Currently there are no facilities." ); ?></td>
	</tr>
  <?php }
?>
	</table>
	<?php // Pagination Displaying Section?>
	<br>
	<table><tbody><tr><td class="text" >
Total <?php echo $num_rows;?> Records : Page :
<?php
if($prev_page)
{
echo " <a href='$_SERVER[SCRIPT_NAME]?page=$prev_page'><< Back</a> ";
}

for($i=1; $i<=$num_pages; $i++){
if($i != $page)
{
echo "[ <a href='$_SERVER[SCRIPT_NAME]?page=$i'>$i</a> ]";
}
else
{
echo "<b> $i </b>";
}
}
if($page!=$num_pages)
{
echo " <a href ='$_SERVER[SCRIPT_NAME]?page=$next_page'>Next>></a> ";
}?>

</td></tr></tbody></table>
        </div>
    </div>
</div>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
</script>

</body>
</html>
