<?php 
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/auth.inc");
require_once("$srcdir/formdata.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");

$alertmsg = '';
$bg_msg = '';
$set_active_msg=0;

/* To refresh and save variables in mail frame  - Arb*/
if (isset($_POST["mode"])) {
	if ($_POST["mode"] == "new_holiday") {
		//echo checkExisting('new', formData('hol_date'));
		if(checkExisting('new', formData('hol_date'), $_POST["fid"]) == 1) {
			$day = date('j', strtotime(formData('hol_date')));
			$month = date('n', strtotime(formData('hol_date')));
			$year =date('Y', strtotime(formData('hol_date')));
			$prov_id = 0;
			$fhol_id = sqlInsert("insert into facility_holidays set " .
			"fhol_facility_id = '"         . trim($_POST["fid"]) .
			"', fhol_date = '"         . trim(formData('hol_date')) .
			"', fhol_name = '"      . trim(formData('hol_name')) .
			"', fhol_is_recc = '"         . trim(formData('hol_type')) .
			"', fhol_year = '"         . trim($year) .
			"', fhol_month = '"         . trim($month) .
			"', fhol_day = '"         . trim($day) .
			"', fhol_created_by = '"         . $_SESSION['authId'] .
			"', fhol_created_date = '" . date('Y-m-d') .
			"', fhol_hol_id = '" . $prov_id .
			"'");
			if ($fhol_id) {				
				$alertmsg = 'Holiday date save successfuly.';
				//die();
				header('Location: facility_holiday_list.php?fid='.$_POST["fid"]);
				exit;
			} else {
				$alertmsg = 'There is some problem occurred in holiday save. Please try again';
				//die();
			}
		}else {
			$alertmsg = 'Holiday date already added. Please try again';
		}
			
	}	  
	if ($_POST["mode"] == "update_holiday") {		
		if(checkExisting('update', formData('hol_date'), $_POST["fid"]) == 1) {  
			if ($_POST["hol_name"]) {				
				$tqvar = trim(formData('hol_name'));
				sqlStatement("update facility_holidays set fhol_name='$tqvar' where fhol_id={$_POST["id"]}");
			}
			/*if ($_POST["hol_type"]) {
				$tqvar = formData('hol_type');
				sqlStatement("update global_holidays set hol_is_recc='$tqvar' where hol_id={$_POST["id"]}");
			}*/
			if ($_POST["hol_date"]) {
				$tqvar = formData('hol_date');
				$day = date('j', strtotime(formData('hol_date')));
				$month = date('n', strtotime(formData('hol_date')));
				$year =date('Y', strtotime(formData('hol_date')));
				sqlStatement("update facility_holidays set fhol_date='$tqvar', fhol_year = '". trim($year) ."', fhol_month = '". trim($month)."', fhol_day = '". trim($day)."' where fhol_id={$_POST["id"]}");
			}			
			$alertmsg = 'Holiday date updated successfully.';
			header('Location: facility_holiday_list.php?fid='.$_POST["fid"]);
			exit;
		}else {
			$alertmsg = 'Holiday date already exist. Please try again';
		}
		
	}
}
if (isset($_GET["mode"])) {
	if ($_GET["mode"] == "delete") {		
		sqlStatement("update facility_holidays set fhol_is_deleted=1 where fhol_id = '" . $_GET["id"] . "'");
		//sqlStatement("delete from facility_holidays where fhol_hol_id = '" . $_GET["id"] . "'");
		$alertmsg = 'Holiday deleted successfully.';
		header('Location: facility_holiday_list.php?fid='.$_GET["fid"]);
		exit;
	}
}
function checkExisting($actionType, $hDate, $facilityId){
	//echo "SELECT `hol_id` FROM global_holidays WHERE hol_date = '".$hDate."'";
	$result = sqlStatement("SELECT `fhol_id` FROM facility_holidays WHERE fhol_date = '".$hDate."' AND `fhol_facility_id` = ".$facilityId);
	if($actionType == 'new'){		
		if(sqlNumRows($result)) {
			return 0;
		}else {
			return 1;
		}
	}else if($actionType == 'update') {
		if(sqlNumRows($result)>1) {
			return 0;
		}else {
			return 1;
		}
	}
}
$form_year = empty($_POST['form_year']) ? date('Y') : intval($_POST['form_year']);
?>
<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.easydrag.handler.beta2.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    // fancy box
    enable_modals();

    tabbify();

    // special size for
	$(".iframe_medium").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 450,
		'frameWidth' : 660
	});
	
	$(function(){
		// add drag and drop functionality to fancybox
		$("#fancy_outer").easydrag();
	});
});

</script>
</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<div>
       <table>
	  <tr >
		<td><b><?php  xl('Holidays','e'); ?></b></td>
		<td><a href="facility_holiday_add.php?fid=<?php echo $_GET["fid"];?>" class="css_button"><span><?php xl('Add Holiday','e'); ?></span></a>
		</td>
		
	  </tr>
	</table>
    </div>
<form name='thesearchform' id='thesearchform' method='post' action='facility_holiday_list.php?fid=<?php echo $_GET["fid"];?>'>

<div id="report_parameters">
<input type='hidden' name='form_refresh' id='form_refresh' value=''/>
<table>
 <tr>
  <td width='60%'>
	<div style='float:left'>

	<table class='text'>
		<tr>
      <td class='label'>
        <?php xl('Holiday Year','e'); ?>:
      </td>
      <td>	      
	      <select id="form_year" name="form_year">
			<option value="">-- Select Year --</option>
			<?php 
			$yres = sqlStatement("SELECT `fhol_year` FROM facility_holidays WHERE `fhol_facility_id` = ".$_GET['fid']." AND `fhol_is_deleted` = 0 GROUP BY fhol_year ORDER BY fhol_year ASC");
			if(sqlNumRows($yres)){	
				while ($yrow = sqlFetchArray($yres)) { 
				?>
					<option value="<?php echo $yrow['fhol_year'];?>" <?php if($yrow['fhol_year'] == $form_year){ echo 'selected="selected"';}?>><?php echo $yrow['fhol_year'];?></option>
				<?php 
				}			
			}else {
				?>
				<option value="">-- Select Year --</option>
				<?php 
			}
			?>
			</select>
      </td>			
		</tr>
	</table>

	</div>

  </td>
  <td align='left' valign='middle' height="100%">
	<table style='border-left:1px solid; width:100%; height:100%' >
		<tr>
			<td>
				<div style='margin-left:15px'>
					<a href='#' class='css_button' onclick='$("#form_refresh").attr("value","true"); $("#thesearchform").submit();'>
					<span>
						<?php xl('Submit','e'); ?>
					</span>
					</a>					
				</div>
			</td>
		</tr>
	</table>
  </td>
 </tr>
</table>
</div> <!-- end of parameters -->
</form>
<table border='0' cellpadding='1' cellspacing='2' width='98%'>
	<tr bgcolor="#dddddd">
		<td class="dehead" width="15%">
			<?php  xl('Holiday Date','e'); ?>
		</td>
		<td class="dehead" width="10%">
			<?php  xl('Holiday Name','e'); ?>
		</td>
		<td class='dehead' width="10%">
			<?php  xl('Is Recursive','e'); ?>
		</td>
		<td class='dehead' width="15%">
			<?php  xl('Action','e'); ?>
		</td>
	</tr>
<?php 
  $eres = sqlStatement("SELECT `fhol_id`, `fhol_date`, `fhol_name`, `fhol_is_recc`, `fhol_year`, `fhol_month`, `fhol_day` " .
    "FROM facility_holidays " .    
    "WHERE `fhol_year` = ".$form_year." AND `fhol_facility_id` = ".$_GET["fid"]." AND `fhol_is_deleted` = 0 ORDER BY fhol_date ASC");

  while ($erow = sqlFetchArray($eres)) { 
?>
	<tr class='text' style='border-bottom: 1px dashed;'>
		<td class='detail'>
			<?php echo $erow['fhol_date']; ?>
		</td>
		<td class='detail'>
			<?php echo $erow['fhol_name']; ?>
		</td>
		<td class='detail'>
			<?php if($erow['fhol_is_recc'] == 1) echo 'Fixed'; else echo 'Changeable'; ?>
		</td>				
		<td class='detail'>
			<?php 
			if((date('Y-m-d') < $erow['fhol_date']) && $erow['fhol_is_recc'] == 0) {
			?>
			<a href='facility_holiday_forms_edit.php?id=<?php echo $erow['fhol_id']; ?>&fid=<?php echo $_GET["fid"];?>' onclick='top.restoreSession()'>Edit</a>			
			<a href='facility_holiday_list.php?mode=delete&fid=<?php echo $_GET["fid"];?>&id=<?php echo $erow['fhol_id']; ?>' onclick='return confirm("Are you sure to delete?"); top.restoreSession()'>Delete</a>
			<?php 
			}			
			?>
		</td>
	</tr>
<?php 
  }
?>

</table>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
</script>
</body>

</html>
