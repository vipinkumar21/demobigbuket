<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");

$alertmsg = '';
/*echo '<pre>';
print_r($_SESSION);
echo '</pre>';*/
?>
<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>


<script language="JavaScript">
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
} 

function submitform() {
	if (document.forms[0].hol_name.value.length>0 && document.forms[0].hol_date.value.length>0) {
		top.restoreSession();		
		document.forms[0].submit();
	} else {
		if (document.forms[0].hol_name.value.length<=0)
		{
			document.forms[0].hol_name.style.backgroundColor="red";
			alert("<?php xl('Required field missing: Please enter the Holiday Name.','e');?>");
			document.forms[0].hol_name.focus();
			return false;
		}
		if (document.forms[0].hol_date.value =="")
		{
			document.forms[0].hol_date.style.backgroundColor="red";
			alert("<?php echo xl('Required field missing: Please choose holiday date.','e'); ?>");
			document.forms[0].hol_date.focus();
			return false;
		}		
	}
}
</script>

</head>
<body class="body_top">
<table><tr><td>
<span class="title"><?php xl('Add Holiday','e'); ?></span>&nbsp;</td>
<td>
<a class="css_button" name='form_save' id='form_save' href='#' onclick="return submitform()">
	<span><?php xl('Save','e');?></span></a>
<a class="css_button large_button" id='cancel' href='facility_holiday_list.php?fid=<?php echo $_GET["fid"];?>'>
	<span class='css_button_span large_button_span'><?php xl('Cancel','e');?></span>
</a>
</td></tr></table>
<br><br>

<table border=0>

<tr><td valign=top>
<form name='new_user' method='post' enctype="multipart/form-data" action="facility_holiday_list.php" onsubmit='return top.restoreSession()'>
<input type='hidden' name='mode' value='new_holiday'>
<input type='hidden' name='hol_type' value="0">
<INPUT TYPE="hidden" NAME="fid" VALUE="<?php echo $_GET["fid"]; ?>">
<span class="bold">&nbsp;</span>
</td><td>
<table border='0' cellpadding='5' cellspacing='5' style="width:600px;">
<tr>
<td style="width:60px;"><span class="text"><?php xl('Holiday Name','e'); ?>: </span></td>
<td  style="width:220px;"><input type='entry' name='hol_name' id="hol_name" style="width:220px;"> <span class="mandatory">&nbsp;*</span></td>
</tr>

<tr>
<td><span class="text"><?php xl('Holiday Date','e'); ?>: </span></td>
<td><input type='text' name='hol_date' id="hol_date" size='10' value='' title='yyyy-mm-dd' readonly='readonly' style="width:197px;">
   <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22' id='img_from_date' border='0' alt='[?]' style='cursor:pointer' title='<?php xl('Click here to choose a date','e'); ?>'><span class="mandatory">&nbsp;*</span></td>
</tr>
  <tr height="25"><td colspan="2">&nbsp;</td></tr>


</table>

<br>
<input type="hidden" name="newauthPass">
</form>
</td>

</tr>
</table>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
/*$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });

});*/
</script>
<table>

</table>

</body>
<!-- stuff for the popup calendar -->
<style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
<script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
<script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script language="Javascript">
 Calendar.setup({inputField:"hol_date", ifFormat:"%Y-%m-%d", button:"img_from_date"});
 //Calendar.setup({inputField:"form_to_date", ifFormat:"%Y-%m-%d", button:"img_to_date"});
</script>
</html>
