<?php
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sql.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/classes/POSRef.class.php");
require_once("$srcdir/options.inc.php");
require_once("$srcdir/erx_javascript.inc.php");
function getTreatmentGroupOption($selectedGroupID  = NULL){
    $sql = "SELECT ct_id, ct_label, ct_key FROM code_types WHERE ct_active=1";
    $eres = sqlStatement($sql, null, $GLOBALS['adodb']['dbreadonly']);
    $returnData = '<select id="form_tr_group" name="form_tr_group">';
    if(sqlNumRows($eres)){
        while ($erow = sqlFetchArray($eres)) {
            if($selectedGroupID == $erow['ct_id']){
                $returnData .= '<option value="'.$erow['ct_id'].'" selected = "selected">'.$erow['ct_key'].'</option>';
            }else {
               $returnData .= '<option value="'.$erow['ct_id'].'">'.$erow['ct_key'].'</option>'; 
            }
        }
    }
    $returnData .= '</select>';    
    return $returnData;
}
$alertmsg = '';
?>
<html>
<head>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
<script type="text/javascript" src="../main/calendar/modules/PostCalendar/pnincludes/AnchorPosition.js"></script>
<script type="text/javascript" src="../main/calendar/modules/PostCalendar/pnincludes/PopupWindow.js"></script>
<script type="text/javascript" src="../main/calendar/modules/PostCalendar/pnincludes/ColorPicker2.js"></script>
<?php
// Old Browser comp trigger on js

if (isset($_POST["mode"]) && $_POST["mode"] == "facility") {
  	echo '
<script type="text/javascript">
<!--
parent.$.fn.fancybox.close();
//-->
</script>

	';
}
?>
<script type="text/javascript">
/// todo, move this to a common library
function trimAll(sString)
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}
function submitform() {
	<?php if($GLOBALS['erx_enable']){ ?>
	alertMsg='';
	f=document.forms[0];
	for(i=0;i<f.length;i++){
		if(f[i].type=='text' && f[i].value)
		{
			if(f[i].name == 'facility' || f[i].name == 'Washington')
			{
				alertMsg += checkLength(f[i].name,f[i].value,35);
				alertMsg += checkFacilityName(f[i].name,f[i].value);
			}
			else if(f[i].name == 'street')
			{
				alertMsg += checkLength(f[i].name,f[i].value,35);
				alertMsg += checkAlphaNumeric(f[i].name,f[i].value);
			}
			else if(f[i].name == 'phone' || f[i].name == 'fax')
			{
				alertMsg += checkPhone(f[i].name,f[i].value);
			}
			else if(f[i].name == 'federal_ein')
			{
				alertMsg += checkLength(f[i].name,f[i].value,10);
				alertMsg += checkFederalEin(f[i].name,f[i].value);
			}
			else if(f[i].name == 'lat_long')
			{
				alertMsg += checkLatLong(f[i].name,f[i].value);
			}
		}		
	}
	if(alertMsg)
	{
		alert(alertMsg);
		return false;
	}
	<?php } ?>
    if (document.forms[0].facility.value.length>0 && document.forms[0].ncolor.value != '' && document.forms[0].lat_long.value != '' && checkLatLongField(document.forms[0].lat_long.value) && document.forms[0].mask_patient_id_pre.value.length>0 && document.forms[0].mask_patient_id_seed.value.length>0 && document.forms[0].mask_invoice_number_pre.value.length>0 && document.forms[0].mask_invoice_number_seed.value.length>0 && document.forms[0].mask_reciept_number_pre.value.length>0 && document.forms[0].mask_reciept_number_seed.value.length>0 && (trimAll(document.forms[0].phone.value) == "" || (trimAll(document.forms[0].phone.value) != "" && validatePhoneNum(trimAll(document.forms[0].phone.value)))) && (trimAll(document.forms[0].mobile.value) == "" || (trimAll(document.forms[0].mobile.value) != "" && validatePhoneCell(trimAll(document.forms[0].mobile.value))))) {
        top.restoreSession();
        document.forms[0].submit();
	} else {
        if(document.forms[0].facility.value.length<=0){
			document.forms[0].facility.style.backgroundColor="red";
			document.forms[0].facility.focus();
		}
        else if(trimAll(document.forms[0].phone.value) != ""){
			var phoneString = trimAll(document.forms[0].phone.value);
			if (!validatePhoneNum(phoneString)) {
				// Invalid phone number
				document.forms[0].phone.style.backgroundColor="red";
				document.forms[0].phone.focus();
			}
		}
        else if(trimAll(document.forms[0].mobile.value) != ""){
			var phonecellString = trimAll(document.forms[0].mobile.value);
			if (!validatePhoneCell(phonecellString)) {
				// Invalid phone number
				document.forms[0].mobile.style.backgroundColor="red";
				document.forms[0].mobile.focus();				
			}
		}
		else if(document.forms[0].ncolor.value == ''){
			document.forms[0].ncolor.style.backgroundColor="red";
			document.forms[0].ncolor.focus();	
		}
		else if(document.forms[0].lat_long.value == '' || checkLatLongField(document.forms[0].lat_long.value)){
			document.forms[0].lat_long.style.backgroundColor="red";
			document.forms[0].lat_long.focus();	
		}else if(document.forms[0].mask_patient_id_pre.value.length<=0){
			document.forms[0].mask_patient_id_pre.style.backgroundColor="red";
			document.forms[0].mask_patient_id_pre.focus();	
		}else if(document.forms[0].mask_patient_id_seed.value.length<=0){
			document.forms[0].mask_patient_id_seed.style.backgroundColor="red";
			document.forms[0].mask_patient_id_seed.focus();	
		}else if(document.forms[0].mask_invoice_number_pre.value.length<=0){
			document.forms[0].mask_invoice_number_pre.style.backgroundColor="red";
			document.forms[0].mask_invoice_number_pre.focus();	
		}else if(document.forms[0].mask_invoice_number_seed.value.length<=0){
			document.forms[0].mask_invoice_number_seed.style.backgroundColor="red";
			document.forms[0].mask_invoice_number_seed.focus();	
		}else if(document.forms[0].mask_reciept_number_pre.value.length<=0){
			document.forms[0].mask_reciept_number_pre.style.backgroundColor="red";
			document.forms[0].mask_reciept_number_pre.focus();	
		}else if(document.forms[0].mask_reciept_number_seed.value.length<=0){
			document.forms[0].mask_reciept_number_seed.style.backgroundColor="red";
			document.forms[0].mask_reciept_number_seed.focus();	
		}
    }
}

function toggle( target, div ) {

    $mode = $(target).find(".indicator").text();
    if ( $mode == "collapse" ) {
        $(target).find(".indicator").text( "expand" );
        $(div).hide();
    } else {
        $(target).find(".indicator").text( "collapse" );
        $(div).show();
    }

}

$(document).ready(function(){

    $("#dem_view").click( function() {
        toggle( $(this), "#DEM" );
    });

    // fancy box
    enable_modals();

    tabbify();

    // special size for
	$(".large_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 600,
		'frameWidth' : 1000
	});

    // special size for
	$(".medium_modal").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 260,
		'frameWidth' : 510
	});

});

$(document).ready(function(){
    $("#cancel").click(function() {
		  parent.$.fn.fancybox.close();
	 });
});
var cp = new ColorPicker('window');
  // Runs when a color is clicked
function pickColor(color) {
 	document.getElementById('ncolor').value = color;
}
var field;
function pick(anchorname,target) {
	var cp = new ColorPicker('window');
  	field=target;
        cp.show(anchorname);
}
function displayAlert()
{
	if(document.getElementById('primary_business_entity').checked==false)
	alert("<?php echo addslashes(xl('Primary Business Entity tax id is used as account id for NewCrop ePrescription. Changing the facility will affect the working in NewCrop.'));?>");
	else if(document.getElementById('primary_business_entity').checked==true)
	alert("<?php echo addslashes(xl('Once the Primary Business Facility is set, it should not be changed. Changing the facility will affect the working in NewCrop ePrescription.'));?>");
}
function allowPositiveNumber(e) {
	var charCode = (e.which) ? e.which : event.keyCode

	if (charCode > 31 && (charCode <= 46 || charCode > 57 )) {
		return false;
	}
	return true;
 
}
function show_overbooking(){
	if(document.getElementById('overbooking_enabled').checked){
		document.getElementById('overbooking_seats').style.visibility = "visible";
		document.getElementById('overbooking_lbl').style.visibility = "visible";
	}else{
		document.getElementById('overbooking_seats').style.visibility = "hidden";
		document.getElementById('overbooking_lbl').style.visibility = "hidden";
	}
}

</script>
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">

</head>
<body class="body_top">
<table>
<tr><td>
    <span class="title"><?php xl('Add Facility','e'); ?></span>&nbsp;&nbsp;&nbsp;</td>
    <td colspan=5 align=center style="padding-left:2px;">
        <a onclick="submitform();" class="css_button large_button" name='form_save' id='form_save' href='#'>
            <span class='css_button_span large_button_span'><?php xl('Save','e');?></span>
        </a>
        <a class="css_button large_button" id='cancel' href='#' >
            <span class='css_button_span large_button_span'><?php xl('Cancel','e');?></span>
        </a>
</td></tr>
</table>

<br>

<form name='facility' method='post' action="facilities.php" target='_parent'>
    <input type=hidden name=mode value="facility">
    <table border=0 cellpadding=0 cellspacing=0>
        <tr>
        <td><span class="text"><?php xl('Name','e'); ?>: </span></td><td colspan="4"><input type=entry name=facility size=45 value=""><span class="mandatory">&nbsp;*</span></td>
        
        </tr>
         <tr>
        <td><span class="text"><?php xl('Phone','e'); ?>: </span></td><td><input type=entry name=phone size=20 value=""><!-- <br><span class="text" style="font-size: 10px;">eg: 0 OR +91- 11 33553232 OR 120 4562628</span> --></td>
        <td width=20>&nbsp;</td>
        <td><span class="text"><?php xl('Mobile','e'); ?>: </span></td><td><input type=entry name=mobile size=20 value=""></td>
        </tr>
        <tr>
        <td><span class="text"><?php xl('Address','e'); ?>: </span></td><td><input type=entry size=20 name=street value=""></td>
        <td>&nbsp;</td>
        <td><span class="text"><?php xl('Fax','e'); ?>: </span></td><td><input type=entry name=fax size=20 value=""></td>
        </tr>
        <tr>
        <td><span class="text"><?php xl('City','e'); ?>: </span></td><td><input type=entry size=20 name=city value=""></td>
        <td>&nbsp;</td>
        <td><span class="text"><?php xl('Zip Code','e'); ?>: </span></td><td><input type=entry size=20 name=postal_code value=""></td>
        </tr>
        <tr>
        <td><span class="text"><?php xl('State','e'); ?>: </span></td><td><input type=entry size=20 name=state value=""></td>
        <td>&nbsp;</td>
        <td><span class="text"><?php xl('Tax ID','e'); ?>: </span></td><td><select name=tax_id_type><option value="EI"><?php xl('EIN','e'); ?></option><option value="SY"><?php xl('SSN','e'); ?></option></select><input type=entry size=11 name=federal_ein value=""></td>
        </tr>
        <tr>
        <td height="22"><span class="text"><?php xl('Country','e'); ?>: </span></td><td><input type=entry size=20 name=country_code value=""></td>
        <td>&nbsp;</td>
        <td><span class="text"><?php ($GLOBALS['simplified_demographics'] ? xl('Facility Code','e') : xl('Facility NPI','e')); ?>:
        </span></td><td><input type=entry size=20 name=facility_npi value=""></td>
        </tr>
		<tr>
        <td><span class="text"><?php xl('Website','e'); ?>: </span></td><td><input type=entry size=20 name=website value=""></td>
        <td>&nbsp;</td>
        <td><span class="text"><?php xl('Email','e'); ?>: </span></td><td><input type=entry size=20 name=email value=""></td>
        </tr>

        <tr>
          <td><span class='text'><?php xl('Billing Location','e'); ?>: </span></td><td><input type='checkbox' name='billing_location' value = '1'></td>
          <td>&nbsp;</td>
          <td><span class='text'><?php xl('Accepts Assignment','e'); ?><br>(<?php xl('only if billing location','e'); ?>): </span></td> <td><input type='checkbox' name='accepts_assignment' value = '1'></td>
        </tr>
        <tr>
          <td><span class='text'><?php xl('Service Location','e'); ?>: </span></td> <td><input type='checkbox' name='service_location' value = '1'></td>
          <td>&nbsp;</td>
          <td><span class='text'><?php echo htmlspecialchars(xl('Color'),ENT_QUOTES); ?>: </span><span class="mandatory">&nbsp;*</span></td> <td><input type=entry name=ncolor id=ncolor size=20 value="">[<a href="javascript:void(0);" onClick="pick('pick','newcolor');return false;" NAME="pick" ID="pick"><?php echo htmlspecialchars(xl('Pick'),ENT_QUOTES); ?></a>]</td>
        </tr>
	<?php
	 $disabled='';
	 $resPBE=sqlStatement("select * from facility where primary_business_entity='1' and id!='".$my_fid."'");
	 if(sqlNumRows($resPBE)>0)
	 $disabled='disabled';
	 ?>
	 <tr>
          <td><span class='text'><?php xl('Primary Business Entity','e'); ?>: </span></td>
          <td><input type='checkbox' name='primary_business_entity' id='primary_business_entity' value='1' <?php if ($facility['primary_business_entity'] == 1) echo 'checked'; ?> <?php if($GLOBALS['erx_enable']){ ?> onchange='return displayAlert()' <?php } ?> <?php echo $disabled;?>></td>
          <td>&nbsp;</td>
         </tr>
        <tr>
            <td><span class=text><?php xl('POS Code','e'); ?>: </span></td>
            <td colspan="6">
                <select name="pos_code">
                <?php
                $pc = new POSRef();

                foreach ($pc->get_pos_ref() as $pos) {
                    echo "<option value=\"" . $pos["code"] . "\" ";
                    echo ">" . $pos['code']  . ": ". $pos['title'];
                    echo "</option>\n";
                }

                ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><span class="text"><?php xl('Billing Attn','e'); ?>:</span></td>
            <td colspan="4"><input type="text" name="attn" size="45"></td>
        </tr>
        <tr>
            <td><span class="text"><?php xl('CLIA Number','e'); ?>:</span></td>
            <td colspan="4"><input type="text" name="domain_identifier" size="45"></td>
        </tr>
		<tr>
            <td><span class="text"><?php xl('Work Off Day','e'); ?>:&nbsp;*</span></td>
            <td>
				<select name="work_off_day">
					<option value="6"> Sun </option>
					<option value="0"> Mon </option>
					<option value="1"> Tue </option>
					<option value="2"> Wed </option>
					<option value="3"> Thu </option>
					<option value="4"> Fri </option>
					<option value="5"> Sat </option>
				</select>
			</td>
			<td>&nbsp;</td>
          	<td><span class='text'><?php xl('Weekly Off Enable','e'); ?>: </span></td> <td><input type='checkbox' name='weekly_off_enabled' checked='checked' value = '1'></td>
        </tr>
		<tr>
            <td><span class="text"><?php xl('OPERATORY#','e'); ?>:&nbsp;*</span></td>
            <td colspan="4">
				<select name="operatory">
					<option value="7"> Seven </option>
					<option value="6"> Six </option>
					<option value="5"> Five </option>
					<option value="4"> Four </option>
					<option value="3"> Three </option>
					<option value="2"> Two </option>
					<option value="1"> One </option>
				</select>
			</td>
        </tr>
		<tr>
            <td><span class="text"><?php xl('LatLong','e'); ?>:&nbsp;*</span></td>
            <td colspan="4"><input type="text" name="lat_long" size="60"></td>
        </tr>
		 <tr>
            <td><span class=text><?php xl('Starting Hour','e'); ?>: </span></td>
			<td>
				<select name='start_hour' id='start_hour'>
					<?php
					for ($h = 0; $h < 24; ++$h) {
						echo "<option value='$h'";
						if ($h == 9) echo " selected";
						echo ">";
						if      ($h ==  0) echo "12 AM";
						else if ($h <  12) echo "$h AM";
						else if ($h == 12) echo "12 PM";
						else echo ($h - 12) . " PM";
						echo "</option>\n";
					}
					?>
				</select>
			</td>
			<td>&nbsp;</td>
            <td><span class=text><?php xl('Ending Hour','e'); ?>: </span></td>
			<td>
				<select name='end_hour' id='end_hour'>
					<?php
					for ($h = 0; $h < 24; ++$h) {
						echo "<option value='$h'";
						if ($h == 21) echo " selected";
						echo ">";
						if      ($h ==  0) echo "12 AM";
						else if ($h <  12) echo "$h AM";
						else if ($h == 12) echo "12 PM";
						else echo ($h - 12) . " PM";
						echo "</option>\n";
					}
					?>
				</select>
			</td>
        </tr>
		 <tr>
          <td><span class='text'><?php xl('Mask for Patient IDs Prefix','e'); ?>: </span></td>
          <td><input type='entry' name='mask_patient_id_pre' size='20' value='' title="Specifies formatting for the external patient ID. # = digit, @ = alpha, * = any character. Empty if not used."><font class="mandatory">&nbsp;*</font></td>
          <td>&nbsp;</td>
		  <td><span class='text'><?php xl('Mask for Patient IDs Seed','e'); ?>:</span></td>
          <td><input type='entry' name='mask_patient_id_seed' size='20' maxlength='11' value='' onkeypress='return allowPositiveNumber(event);' title="Specifies formatting for the external patient ID. # = digit. Empty if not used."><font class="mandatory">&nbsp;*</font></td>
         </tr>
		  <tr>
          <td><span class='text'><?php xl('Mask for Invoice Numbers Prefix','e'); ?>: </span></td>
          <td><input type='entry' name='mask_invoice_number_pre' size='20' value='' title="Specifies formatting for invoice reference numbers prefix. # = digit, @ = alpha, * = any character. Empty if not used."><font class="mandatory">&nbsp;*</font></td>
          <td>&nbsp;</td>
		  <td><span class='text'><?php xl('Mask for Invoice Numbers Seed','e'); ?>:</span></td>
          <td><input type='entry' name='mask_invoice_number_seed' size='20' maxlength='11' value='' onkeypress='return allowPositiveNumber(event);' title="Specifies formatting for invoice reference numbers seed. # = digit. Empty if not used."><font class="mandatory">&nbsp;*</font></td>
         </tr>
		  <tr>
          <td><span class='text'><?php xl('Mask for Reciept Numbers Prefix','e'); ?>: </span></td>
          <td><input type='entry' name='mask_reciept_number_pre' size='20' value='' title="Specifies formatting for reciept reference numbers prefix. # = digit, @ = alpha, * = any character. Empty if not used."><font class="mandatory">&nbsp;*</font></td>
          <td>&nbsp;</td>
		  <td><span class='text'><?php xl('Mask for Reciept Numbers Seed','e'); ?>:</span></td>
          <td><input type='entry' name='mask_reciept_number_seed' size='20' maxlength='11' value='' onkeypress='return allowPositiveNumber(event);' title="Specifies formatting for reciept reference numbers seed. # = digit. Empty if not used."><font class="mandatory">&nbsp;*</font></td>
         </tr>
         <tr>
          <td><span class='text'><?php xl('OverBooking Allowed','e'); ?>: </span></td>
          <td><input type='checkbox' name='overbooking_enabled' id='overbooking_enabled' <?php if($facility['overbooking_enabled'] == 1) {echo 'checked=checked';}?> value = '1' onclick="show_overbooking();" ></td>
           <td>&nbsp;</td>
          <td><span class="text" id="overbooking_lbl" name="overbooking_lbl" style="visibility: hidden;"><?php xl('No Of OverBooking','e'); ?>:&nbsp;*</span></td>
            <td colspan="4">
				<select name="overbooking_seats" id="overbooking_seats" style="visibility: hidden;">
				<option value="1" <?php if($facility['overbooking_seats'] == 1) {echo 'selected=selected';}?>> One </option>
				<option value="2" <?php if($facility['overbooking_seats'] == 2) {echo 'selected=selected';}?>> Two </option>
				<option value="3" <?php if($facility['overbooking_seats'] == 3) {echo 'selected=selected';}?>> Three </option>
				<option value="4" <?php if($facility['overbooking_seats'] == 4) {echo 'selected=selected';}?>> Four </option>
				
					
					
					
					
				</select>
			</td>
         </tr>
         <tr>
          <td><span class='text'><?php xl('Treatment Group','e'); ?>: </span></td>
          <td><?php echo getTreatmentGroupOption(107);?></td>
           <td>&nbsp;</td>
          <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
        <tr height="25" style="valign:bottom;">
        <td><font class="mandatory">*</font><span class="text"> <?php echo xl('Required','e'); ?></span></td><td>&nbsp;</td><td>&nbsp;</td>
        <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
    </table>
</form>

<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
</script>

</body>
</html>
