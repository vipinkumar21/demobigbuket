<?php 
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/auth.inc");
require_once("$srcdir/formdata.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");

$alertmsg = '';
$bg_msg = '';
$set_active_msg=0;

/* To refresh and save variables in mail frame  - Arb*/
if (isset($_POST["mode"])) {
	  if ($_POST["mode"] == "new_holiday") {
	  		//echo checkExisting('new', formData('hol_date'));
	  		if(checkExisting('new', formData('hol_date')) == 1) {
		  		$day = date('j', strtotime(formData('hol_date')));
		  		$month = date('n', strtotime(formData('hol_date')));
		  		$year =date('Y', strtotime(formData('hol_date')));	
		     	$prov_id = sqlInsert("insert into global_holidays set " .
		        "hol_date = '"         . trim(formData('hol_date')) .
		        "', hol_name = '"      . trim(formData('hol_name')) .
		        "', hol_is_recc = '"         . trim(formData('hol_type')) .
		        "', hol_year = '"         . trim($year) .
		      	"', hol_month = '"         . trim($month) .
		      	"', hol_day = '"         . trim($day) .
		      	"', hol_created_by = '"         . $_SESSION['authId'] .
		        "', hol_created_date = '" . date('Y-m-d') .
		        "'");
				if ($prov_id) {
					$result = sqlStatement("select id, name as clinicname FROM facility where service_location != 0 order by name ASC");
					if(sqlNumRows($result)) {
						while($erow = sqlFetchArray($result)){
							$fhol_id = sqlInsert("insert into facility_holidays set " .
							"fhol_facility_id = '"         . trim($erow['id']) .
							"', fhol_date = '"         . trim(formData('hol_date')) .
							"', fhol_name = '"      . trim(formData('hol_name')) .
							"', fhol_is_recc = '"         . trim(formData('hol_type')) .
							"', fhol_year = '"         . trim($year) .
							"', fhol_month = '"         . trim($month) .
							"', fhol_day = '"         . trim($day) .
							"', fhol_created_by = '"         . $_SESSION['authId'] .
							"', fhol_created_date = '" . date('Y-m-d') .
							"', fhol_hol_id = '" . $prov_id .
							"'");
						}
					}
					$alertmsg = 'Holiday date save successfuly.';
					//die();
					header('Location: list.php');
					exit;
				} else {
					$alertmsg = 'There is some problem occurred in holiday save. Please try again';
					//die();
				}
	  		}else {
	  			$alertmsg = 'Holiday date already added. Please try again';
	  		}
			
	  }
	if ($_POST["mode"] == "update_holiday") {		
		if(checkExisting('update', formData('hol_date')) == 1) {  
			if ($_POST["hol_name"]) {
				// $tqvar = addslashes(trim($_GET["username"]));
				$tqvar = trim(formData('hol_name'));
				sqlStatement("update global_holidays set hol_name='$tqvar' where hol_id={$_POST["id"]}");
			}
			if ($_POST["hol_type"]) {
				$tqvar = formData('hol_type');
				sqlStatement("update global_holidays set hol_is_recc='$tqvar' where hol_id={$_POST["id"]}");
			}
			if ($_POST["hol_date"]) {
				$tqvar = formData('hol_date');
				$day = date('j', strtotime(formData('hol_date')));
				$month = date('n', strtotime(formData('hol_date')));
				$year =date('Y', strtotime(formData('hol_date')));
				sqlStatement("update global_holidays set hol_date='$tqvar', hol_year = '". trim($year) ."', hol_month = '". trim($month)."', hol_day = '". trim($day)."' where hol_id={$_POST["id"]}");
			}
			$result = sqlStatement("select id, name as clinicname FROM facility where service_location != 0 order by name ASC");
			if(sqlNumRows($result)) {
				while($erow = sqlFetchArray($result)){
					$fhol_id = sqlInsert("update facility_holidays set " .
							"fhol_date = '"         . trim(formData('hol_date')) .
							"', fhol_name = '"      . trim(formData('hol_name')) .
							"', fhol_is_recc = '"         . trim(formData('hol_type')) .
							"', fhol_year = '"         . trim($year) .
							"', fhol_month = '"         . trim($month) .
							"', fhol_day = '"         . trim($day) .
							"', fhol_created_by = '"         . $_SESSION['authId'] .
							"', fhol_created_date = '" . date('Y-m-d') .
							"' WHERE fhol_facility_id = ".$erow['id']." AND fhol_hol_id=".$_POST["id"]);
				}
			}
			$alertmsg = 'Holiday date updated successfully.';
			header('Location: list.php');
			exit;
		}else {
			$alertmsg = 'Holiday date already exist. Please try again';
		}
		
	}
}
if (isset($_GET["mode"])) {
	if ($_GET["mode"] == "delete") {		
		sqlStatement("delete from global_holidays where hol_id = '" . $_GET["id"] . "'");
		sqlStatement("delete from facility_holidays where fhol_hol_id = '" . $_GET["id"] . "'");
		$alertmsg = 'Holiday deleted successfully.';
	}
}
function checkExisting($actionType, $hDate){
	//echo "SELECT `hol_id` FROM global_holidays WHERE hol_date = '".$hDate."'";
	$result = sqlStatement("SELECT `hol_id` FROM global_holidays WHERE hol_date = '".$hDate."'");
	if($actionType == 'new'){		
		if(sqlNumRows($result)) {
			return 0;
		}else {
			return 1;
		}
	}else if($actionType == 'update') {
		if(sqlNumRows($result)>1) {
			return 0;
		}else {
			return 1;
		}
	}
}
$form_year = empty($_POST['form_year']) ? date('Y') : intval($_POST['form_year']);
?>
<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link rel="stylesheet" href="<?php echo $css_header;?>" type="text/css">
<link type="text/css" href="../themes/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/dialog.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.easydrag.handler.beta2.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    // fancy box
    enable_modals();

    tabbify();

    // special size for
	$(".iframe_medium").fancybox( {
		'overlayOpacity' : 0.0,
		'showCloseButton' : true,
		'frameHeight' : 450,
		'frameWidth' : 660
	});
	
	$(function(){
		// add drag and drop functionality to fancybox
		$("#fancy_outer").easydrag();
	});
});

</script>
</head>

<body class="body_top">

<!-- Required for the popup date selectors -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<div class="panel panel-warning">
	<div class="panel-heading">
	    <div class="row">
	        <div class="col-xs-3 boldtxt"><?php  xl('Holidays','e'); ?></div>
	        <div class="col-xs-9 text-right">
				<a href="forms_add.php" class="iframe btn btn-default btn-sm"><span><?php xl('Add Holiday','e'); ?></span></a>
	        </div>
	    </div>
	</div>
	<input type='hidden' name='form_refresh' id='form_refresh' value=''/>
	<div class='panel-body'>
		<form name='thesearchform' id='thesearchform' method='post' action='list.php'>
			<div class="col-xs-3">
				<?php  xl('Holidays','e'); ?>
				<select id="form_year" name="form_year" class="form-control input-sm">
				<option value="">-- Select Year --</option>
				<?php 
				$yres = sqlStatement("SELECT `hol_year` FROM global_holidays GROUP BY hol_year ORDER BY hol_year ASC");
				if(sqlNumRows($yres)){	
					while ($yrow = sqlFetchArray($yres)) { 
					?>
						<option value="<?php echo $yrow['hol_year'];?>" <?php if($yrow['hol_year'] == $form_year){ echo 'selected="selected"';}?>><?php echo $yrow['hol_year'];?></option>
					<?php 
					}			
				}
				?>
				</select>
			</div>
			<div class="col-xs-9">
				<a href='#' style="margin-top: 20px;" class='btn btn-warning btn-sm' onclick='$("#form_refresh").attr("value","true"); $("#thesearchform").submit();'>
						<span>
							<?php xl('Submit','e'); ?>
						</span>
						</a>
			</div>
		</form>
	</div>
</div>
<div>
       
    </div>


<!-- end of parameters -->
</form>
<table border='0' cellpadding='1' cellspacing='2' width='98%'>
	<tr bgcolor="#dddddd">
		<td class="dehead" width="15%">
			<?php  xl('Holiday Date','e'); ?>
		</td>
		<td class="dehead" width="10%">
			<?php  xl('Holiday Name','e'); ?>
		</td>
		<td class='dehead' width="10%">
			<?php  xl('Is Recursive','e'); ?>
		</td>		
		<td class='dehead' width="10%">
			<?php  xl('Created Date','e'); ?>
		</td>
		<td class='dehead' width="15%">
			<?php  xl('Action','e'); ?>
		</td>
	</tr>
<?php 
  $eres = sqlStatement("SELECT `hol_id`, `hol_date`, `hol_name`, `hol_is_recc`, `hol_year`, `hol_month`, `hol_day`, DATE(`hol_created_date`) AS createdDate, `hol_created_by`, `nickname` " .
    "FROM global_holidays AS gl " .
    "INNER JOIN users AS u ON " .
    "u.id = gl.hol_created_by " .
    "WHERE `hol_year` = ".$form_year." ORDER BY hol_date ASC");

  while ($erow = sqlFetchArray($eres)) { 
?>
	<tr class='text' style='border-bottom: 1px dashed;'>
		<td class='detail'>
			<?php echo $erow['hol_date']; ?>
		</td>
		<td class='detail'>
			<?php echo $erow['hol_name']; ?>
		</td>
		<td class='detail'>
			<?php if($erow['hol_is_recc'] == 1) echo 'Fixed'; else echo 'Changeable'; ?>
		</td>
		<td class='detail'>
			<?php echo $erow['createdDate']; ?>
		</td>		
		<td class='detail'>
			<?php 
			if(date('Y-m-d') < $erow['hol_date']) {
			?>
			<a href='forms_edit.php?id=<?php echo $erow['hol_id']; ?>' class='iframe_medium' onclick='top.restoreSession()'>Edit</a> <br>			
			<a href='list.php?mode=delete&id=<?php echo $erow['hol_id']; ?>' onclick='return confirm("Are you sure to delete?"); top.restoreSession()'>Delete</a>
			<?php 
			}
			?>
		</td>
	</tr>
<?php 
  }
?>

</table>
<script language="JavaScript">
<?php
  if ($alertmsg = trim($alertmsg)) {
    echo "alert('$alertmsg');\n";
  }
?>
</script>
</body>

</html>
