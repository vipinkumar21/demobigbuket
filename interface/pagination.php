<?php

function paginationCss() {

    $string = '<style type="text/css">
        ul.pagination{
	margin:0px;
	padding:0px;
	height:100%;
	overflow:hidden;
	font:12px \'Tahoma\';
	list-style-type:none;	
        }

        ul.pagination li.details{
            padding:7px 10px 7px 10px;
            font-size:14px;
        }

        ul.pagination li.dot{padding: 3px 0;}

        ul.pagination li{
                float:left;
                margin:0px;
                padding:0px;
                margin-left:5px;
        }

        ul.pagination li:first-child{
                margin-left:0px;
        }

        ul.pagination li a{
                color:black;
                display:block;
                text-decoration:none;
                padding:7px 10px 7px 10px;
        }

        ul.pagination li a img{
                border:none;
        }
        ul.pagination li.details{
           color:#888888;
        }

        ul.pagination li a
        {
                color:#FFFFFF;
                border-radius:3px;	
                -moz-border-radius:3px;
                -webkit-border-radius:3px;
        }

        ul.pagination li a
        {
                color:#474747;
                border:solid 1px #B6B6B6;
                padding:6px 9px 6px 9px;
                background:#E6E6E6;
                background:-moz-linear-gradient(top,#FFFFFF 1px,#F3F3F3 1px,#E6E6E6);
                background:-webkit-gradient(linear,0 0,0 100%,color-stop(0.02,#FFFFFF),color-stop(0.02,#F3F3F3),color-stop(1,#E6E6E6));
        }	

        ul.pagination li a:hover,
        ul.pagination li a.current
        {
                background:#FFFFFF;
        }
        .records {
            width: 510px;
            margin: 5px;
            padding:2px 5px;
            border:1px solid #B6B6B6;
        }
        
        .record {
            color: #474747;
            margin: 5px 0;
            padding: 3px 5px;
        	background:#E6E6E6;  
            border: 1px solid #B6B6B6;
            cursor: pointer;
            letter-spacing: 2px;
        }
        .record:hover {
            background:#D3D2D2;
        }
        
        
        .round {
        	-moz-border-radius:8px;
        	-khtml-border-radius: 8px;
        	-webkit-border-radius: 8px;
        	border-radius:8px;    
        }    
        
        p.createdBy{
            padding:5px;
            width: 510px;
        	font-size:15px;
        	text-align:center;
        }
        p.createdBy a {color: #666666;text-decoration: none;}        
    </style>';
    echo $string;
}

function paginationScript() {

    $script = "function gotoPage(url,lastpage){
                    var pagenumber = document.getElementById('tpage').value;
                    
                    if(pagenumber > lastpage)
                    {
                        alert('Page Number should be less than '+lastpage);
                        return false;  
                    }
                    
                    if(pagenumber < 0 ){
                        alert('Page Number should be grater than 0');
                        return false;  
                    }

                    var nurl = window.location.href;    
                    if (nurl.indexOf('?') > -1){
                      nurl += '&page='+pagenumber;
                    }else{
                      nurl += '?page='+pagenumber;
                    }
                    window.location.href = nurl;             
                 }";

    echo $script;
}

/**
 * 
 * @param type $recordtotal
 * @param type $per_page
 * @param type $page
 * @param type $url
 * @return string
 */
function pagination($recordtotal, $per_page = 10, $page = 1, $url = '?') {

    $total = $recordtotal;
    $adjacents = "2";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total / $per_page);
    $lpm1 = $lastpage - 1;

    $pagination = "";
    if ($lastpage > 1) {
        //$pagination .= "<ul class='pagination'>";
        $pagination .= "<div class='dataTables_info'>Showing $page of $lastpage entries</div>";
        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<li><a class='current'>$counter</a></li>";
                else
                    $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
            }
        }
        elseif ($lastpage > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";
            }
            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";
            }
            else {
                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
                }
            }
        }

        if ($page < $counter - 1) {
            $pagination.= "<li><a href='{$url}page=$next'>Next</a></li>";
            $pagination.= "<li><a href='{$url}page=$lastpage'>Last</a></li>";
        } else {
            $pagination.= "<li><a class='current'>Next</a></li>";
            $pagination.= "<li><a class='current'>Last</a></li>";
        }
        $pagination.= "<li><form action='" . $url . "' method='GET' id='thepage'>";
        $pagination.= "Go to page: ";
        $pagination.= "<input type='text' name='page' size='1' id='tpage' />";
        $pagination.= "<input type='button' value='Go' class='customBtn' onclick=\"gotoPage('" . $url . "'," . $lastpage . ")\" />";
        $pagination.= "</form></li>\n";
        //$pagination.= "</ul>\n";
    }


    return $pagination;
}

function pagination_prm($total, $per_page = 10,$page = 1, $url = '?'){        
        $adjacents = "2"; 
    	$page = ($page == 0 ? 1 : $page);  
    	$start = ($page - 1) * $per_page;								
	$end = $start + $per_page;	
    	$prev = $page - 1;							
    	$next = $page + 1;
        $lastpage = ceil($total/$per_page);
    	$lpm1 = $lastpage - 1;
    	
    	$pagination = "";
      
    	if($lastpage > 1)
    	{
            
            $pagination .= "<div class='paginationWrap'>";
            if(($end)> ($total)){
                $pagination .= "<div class='dataTables_info'>Showing ".($start+1)." to ".$total." of $total entries</div>";
            }else{    
            $pagination .= "<div class='dataTables_info'>Showing ".($start+1)." to $end of $total entries</div>";
            }
            $pagination .= "<div class='dataTables_paginate paging_full_numbers'>";

                if ($page > 1){ 
                             $prev = $page - 1;
                             $pagination.= "<a href='{$url}page=$firstpage' class='paginate_button first'>First</a>";
                             $pagination.= "<a href='{$url}page=$prev' class='paginate_button previous'>Prev</a>";
                }else{
                             $pagination.= "<a class='paginate_button previous'>Pre</a>";
                }

    		if ($lastpage < 7 + ($adjacents * 2))
    		{	
                $pagination .= "<span>";
    			for ($counter = 1; $counter <= $lastpage; $counter++)
    			{
    				if ($counter == $page)
    					$pagination.= "<a class='current'>$counter</a>";
    				else
    					$pagination.= "<a href='{$url}page=$counter'>$counter</a>";					
    			}
                $pagination .= "</span>";
    		}
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                $pagination .= "<span>";
    			if($page < 1 + ($adjacents * 2))		
    			{
    				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
    				{
    					if ($counter == $page)
    						$pagination.= "<a class='current'>$counter</a>";
    					else
    						$pagination.= "<a href='{$url}page=$counter' >$counter</a>";					
    				}
    				$pagination.= "<a href='' class='dot'>..</a>";
    				$pagination.= "<a href='{$url}page=$lpm1'>$lpm1</a>";
    				$pagination.= "<a href='{$url}page=$lastpage'>$lastpage</a>";		
    			}
    			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
    			{
    				$pagination.= "<a href='{$url}page=1'>1</a>";
    				$pagination.= "<a href='{$url}page=2'>2</a>";
    				$pagination.= "<a href='' class='dot'>..</a>";
    				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
    				{
    					if ($counter == $page)
    						$pagination.= "<a class='current'>$counter</a>";
    					else
    						$pagination.= "<a href='{$url}page=$counter'>$counter</a>";					
    				}
    				$pagination.= "<a href='' class='dot'>..</a>";
    				$pagination.= "<a href='{$url}page=$lpm1'>$lpm1</a>";
    				$pagination.= "<a href='{$url}page=$lastpage'>$lastpage</a>";		
    			}
    			else
    			{
    				$pagination.= "<a href='{$url}page=1'>1</a>";
    				$pagination.= "<a href='{$url}page=2'>2</a>";
    				$pagination.= "<a href='' class='dot'>..</a>";
    				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
    				{
    					if ($counter == $page)
    						$pagination.= "<a class='current'>$counter</a>";
    					else
    						$pagination.= "<a href='{$url}page=$counter'>$counter</a>";					
    				}
    			}
                $pagination .= "</span>";
            }
    		
    		if ($page < $counter - 1){ 
    			$pagination.= "<a href='{$url}page=$next' class='paginate_button next'>Next</a>";
                $pagination.= "<a href='{$url}page=$lastpage' class='paginate_button last'>Last</a>";
    		}else{
    			$pagination.= "<a class='paginate_button next'>Next</a>";
                $pagination.= "<a class='paginate_button last'>Last</a>";
            }            
    		$pagination.= "</div>";	
            $pagination.= "</div>\n"; 	
    	}
    
    
        return $pagination;
    } 
?>