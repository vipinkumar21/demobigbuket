<?php
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/calendar.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");

if (!$_GET["id"] || !acl_check('Feedback', 'fdbk_view'))
  exit();
$res = sqlStatement("select ft.ftrans_sr_no, ft.ftrans_id,ft.ftrans_auto_sr_no,ft.ftrans_createddate,ft.ftrans_pname,ft.ftrans_status,ft.pubpid, fc.name, lo.ft_name AS ft_name from feedback_transaction AS ft INNER JOIN facility AS fc ON fc.id = ft.ftrans_fid INNER JOIN feedback_type as lo ON (lo.ft_id = ft.ftrans_ft_id) WHERE ft.ftrans_id={$_GET["id"]}");
for ($iter = 0;$row = sqlFetchArray($res);$iter++)
{
    $result[$iter] = $row;
}
$iter = $result[0];
?>
<html>
<head>

<link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>

</head>
<body class="body_top">
    <h3 class="emrh3 fbackhead"><?php xl('Feedback Detail', 'e'); ?></h3>
    <div class='feedback_details'>
        <table id='feedback_details' class='emrtable' border=0 cellpadding='0' cellspacing=0>
            <thead>
                <tr>
                    <th width="15%"><?php xl('#ID & Sr.No.', 'e'); ?></th>
                    <th width="10%"><?php xl('Date', 'e'); ?></th>
                    <th width="15%"><?php xl('Title', 'e'); ?></th>
                    <th width="20%"><?php xl('Clinc Name', 'e'); ?></th>
                    <th width="30%"><?php xl('Patient :ID', 'e'); ?></th>
                    <th width="10%"><?php xl('Status', 'e'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <?php 
                        echo $iter["ftrans_auto_sr_no"]."<hr>"; 
                        echo $iter["ftrans_sr_no"]; 
                        ?>
                    </td>
                    <td><?php echo date("d-m-y", strtotime($iter['ftrans_createddate'])); ?></td>
                    <td><?php echo $iter["ft_name"]; ?></td>
                    <td><?php echo $iter["name"]; ?></td>
                    <td><?php echo $iter["ftrans_pname"] . " <strong>: " . $iter['pubpid'] . "</strong>"; ?></td>
                    <td><?php if ($iter['ftrans_status'] == 2) {
                                    echo 'Closed';
                                } else if ($iter['ftrans_status'] == 1) {
                                    echo 'In Progress';
                                } else {
                                    echo 'Open';
                                } ?>
                    </td>
                </tr>
                <?php
                $fosql = "SELECT fo.fo_desc,fo.fo_is_response, fod.fod_data FROM feedback_option AS fo INNER JOIN feedback_option_data AS fod ON fod.fod_fo_id = fo.fo_id WHERE fod.fod_ftrans_id = " . $iter["ftrans_id"] . " AND fod_status = '1' ORDER BY fo.fo_sequence asc";
                $fores = sqlStatement($fosql);
                if (sqlNumRows($fores) > 0) {
                    $foCount = 0;
                    while ($forow = sqlFetchArray($fores)) {
                        if ($forow['fo_is_response'] != '1') {
                            ?>
                            <tr>
                                <td colspan="6">
                                <?php echo $forow['fo_desc']; ?>
                                <?php if (!empty($forow['fod_data'])) { ?>
                                        <br/>
                                        <br/>
                                        <span class=text>
                                            <strong>Response: </strong><?php echo $forow['fod_data']; ?>
                                        </span>
                                </td>
                                <?php } ?>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5"><?php echo $forow['fo_desc']; ?></td>
                                <td colspan="1">	
                                    <span class=text>
                                        <strong> <?php echo $forow['fod_data']; ?> </strong>
                                    </span>
                                </td>
                            </tr>
                <?php
                                }
                    }
                }
                ?>
            </tbody>
        </table>
        <?php
            $frres = sqlStatement("select ftr.ftr_created_date,ftr.ftr_remark, CONCAT_WS(' ', u.fname, u.lname) AS uname from feedback_transaction_remark AS ftr INNER JOIN users AS u ON u.id=ftr.ftr_created_by where ftr.ftr_ftrans_id=" . $iter["ftrans_id"]);
            if (sqlNumRows($frres) > 0) {
        ?>
                <h3 class="emrh3 fbackhead"><?php xl('Feedback Follow Ups', 'e'); ?></h3>
                <table id="feedback_details" class='emrtable' border=0 cellpadding=0 cellspacing=0 width="100%">
                    <thead>
                        <tr>
                            <th><?php xl('Provider', 'e'); ?></th>
                            <th><?php xl('Date', 'e'); ?></th>
                            <th><?php xl('Remark', 'e'); ?></th>
                        </tr>
                    </thead>
                <?php
                    while ($frrow = sqlFetchArray($frres)) {
                ?>
                    <tbody>
                        <tr>
                            <td><?php echo $frrow["uname"]; ?></td>
                            <td><?php echo date('d-m-y', strtotime($frrow['ftr_created_date'])); ?></td>
                            <td><?php echo nl2br($frrow['ftr_remark']); ?></td>
                        </tr>
                    </tbody>
                <?php
                    }
                ?>
                </table>
        <?php
            } else {
        ?>
            <h3 class="emrh3 fbackhead"><?php xl('Feedback Follow Ups : No Follow ups found.', 'e'); ?></h3>
        <?php
                }
        ?>
    </div>
</body>
</html>
