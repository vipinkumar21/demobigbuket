<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.
require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
$postData = json_decode($_POST);
//print_r($_REQUEST);
$patientquery = "SELECT id, fname, lname, email, phone_cell, phone_home, phone_contact, street, pubpid FROM patient_data WHERE fname LIKE '%".$_REQUEST['q']."%' ORDER BY id DESC limit 0, 20";
$patientres = sqlStatement($patientquery);
$patientData = array();
while($patientrow = sqlFetchArray($patientres)){
    $patientTempData = array();
    $patientTempData['id'] = $patientrow['id'];
    $patientTempData['pname'] = $patientrow['fname']." ".$patientrow['lname'];
    $patientTempData['pemail'] = $patientrow['email'];
    if(!empty($patientrow['phone_cell'])){
            $patientTempData['pcontact'] = $patientrow['phone_cell'];
    } else if(!empty($patientrow['phone_home'])){
            $patientTempData['pcontact'] = $patientrow['phone_home'];
    } else{
            $patientTempData['pcontact'] = $patientrow['phone_contact'];
    }
    $patientTempData['paddress'] = $patientrow['street'];
    $patientTempData['pubpid'] = $patientrow['pubpid'];
    $patientData[] =     $patientTempData;

}
echo json_encode($patientData);
?>