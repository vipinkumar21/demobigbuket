<?php
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
require_once("../globals.php");
require_once("../../library/acl.inc");
require_once("$srcdir/sha1.js");
require_once("$srcdir/sql.inc");
require_once("$srcdir/calendar.inc");
require_once("$srcdir/formdata.inc.php");
require_once("$srcdir/options.inc.php");
require_once("../../library/pdo/lib/database.php");
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once("$srcdir/erx_javascript.inc.php");
if (!$_GET["id"] || !acl_check('Feedback', 'fdbk_followup_add_view'))
  exit();

    $query = "Select ft.ftrans_fid,ft.ftrans_id,ft.ftrans_auto_sr_no,ft.ftrans_createddate,f.name, 
                lo.ft_name as title,CONCAT_WS('-', ft.ftrans_pname, ft.pubpid) AS pname,
                    CASE
                        WHEN ft.ftrans_status = '1' THEN 'In-Progress'
                        WHEN ft.ftrans_status = '2' THEN 'Closed'
                        ELSE 'Open'
                    END AS ftrans_status 
                FROM feedback_transaction AS ft 
                INNER JOIN facility AS f ON f.id = ft.ftrans_fid 
                INNER JOIN feedback_type as lo ON (lo.ft_id = ft.ftrans_ft_id)
                WHERE ft.ftrans_id=?" ;
    $res = $pdoobject->custom_query($query,array($_GET["id"]),'','fetch');
?>

<html>
<head>
<link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
<script type="text/javascript" src="../../library/dialog.js"></script>
<script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
<script type="text/javascript" src="../../library/js/common.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
<script type="text/javascript">
	function submitform() {
	    if (document.forms[0].form_status.value.length>0 && document.forms[0].form_remark.value.length>0 && document.forms[0].form_bcc.value.length>0) {
	            //top.restoreSession();
	            document.forms[0].submit();
	    } else {
	            if (document.forms[0].form_status.value.length<=0)
	            {
	                    document.forms[0].form_status.style.backgroundColor="red";
	                    alert("<?php xl('Required field missing: Please choose the status', 'e'); ?>");
	                    document.forms[0].form_status.focus();
	                    return false;
	            }
	            if (document.forms[0].form_remark.value.length<=0)
	            {
	                    document.forms[0].form_remark.style.backgroundColor="red";
	                    //alert("<?php echo xl('Required field missing: Please enter feedback remark.'); ?>");
                        msg = "Required field missing: Please enter feedback remark."; 
                        jAlert(msg);
	                    document.forms[0].form_remark.focus();
	                    return false;
	            }
	            if (document.forms[0].form_bcc.value.length<=0)
	            {
	                    document.forms[0].form_bcc.style.backgroundColor="red";
	                    //alert("<?php echo xl('Required field missing: Please choose the bcc.'); ?>");
                        msg = "Required field missing: Please choose the bcc."; 
                        jAlert(msg);
	                    document.forms[0].form_bcc.focus();
	                    return false;
	            }		
	    }
	}

</script>
</head>
<body class="body_top">
	<h3 class="emrh3 fbackhead"><?php xl('Feedback Follow Ups','e'); ?></h3>
	<!-- <div class='emrwrapper'> -->
	<div class='feedback_details'>
            <form method='post' name='theform' id='theform' action='feedback_save.php'>
                <input type="hidden" name="ftrans_id" value="<?php echo $res["ftrans_id"]; ?>">
                <input type="hidden" name="ftrans_pid" value="<?php echo $res['pid'];?>">
                <input type="hidden" name="ftrans_pre_status" value="<?php echo $res['ftrans_status'];?>">
                <input type="hidden" name="mode" value="followup">
                <table id='feedback_details' class='emrtable' border=0 cellpadding=0 cellspacing=0>
                        <thead>
                                <tr>
                                        <th width="3%"><?php xl('ID #','e'); ?></th>
                                        <th width="18%"><?php xl('Follow-up Date','e'); ?></th>
                                        <th width="20%"><?php xl('Clinc Name','e'); ?></th>
                                        <th width="24%"><?php xl('Title','e'); ?></th>
                                        <th width="25%"><?php xl('Patient','e'); ?></th>
                                        <th width="10%"><?php xl('Status','e'); ?></th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr>
                                        <td><?php echo $res["ftrans_auto_sr_no"]; ?></td>
                                        <td><?php echo date('d-m-y', strtotime($res['ftrans_createddate'])); ?></td>
                                        <td><?php echo $res["name"]; ?></td>
                                        <td><?php echo $res["title"]; ?></td>
                                        <td><?php echo $res["pname"];?></td>
                                        <td><?php echo $res['ftrans_status']; ?></td>
                                </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan='2'><span class=text><?php xl('Status','e'); ?>: </span></td>
                                <td colspan='4' class='text'>
                                        <?php
                                                $statusArray = array('1' => 'In Progress', '2' => 'Close');
                                        ?>
                                        <select id="form_status" name="form_status" class="form-control" style="width:50%;">
                                                <?php
                                                        foreach($statusArray as $key=>$value){
                                                                echo "<option value=\"$key\">$value</option>\n";
                                                        }
                                                ?>
                                        </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2'><span class=text><?php xl('Remark','e'); ?>: </span></td>
                                <td colspan='4' class='text'>
                                        <textarea name='form_remark' id="form_remark" style="width:50%;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2'><span class=text><?php xl('Followed By','e'); ?>: </span></td>
                                <td colspan='4' class='text'>
                                        <select id="form_bcc" name="form_bcc" class="form-control" style="width:50%;">
                                        <?php   $resFacility = $res['ftrans_fid'];
                                                $usersQuery = "SELECT u.id, u.`nickname` AS name FROM users AS u 
                                                    INNER JOIN `users_facility` AS uf ON uf.table_id = u.id AND uf.`facility_id` = '$resFacility' 
                                                    WHERE u.active ='1' 
                                                    ORDER BY u.`nickname`";
                                                $fres = $pdoobject->custom_query($usersQuery);
                                                foreach ($fres as $frow) {
                                                        $facility_id = $frow['id'];
                                                        $option_value = htmlspecialchars($facility_id, ENT_QUOTES);
                                                        $option_selected_attr = '';
                                                        if ($_SESSION['authId'] == $facility_id) {
                                                          $option_selected_attr = ' selected="selected"';
                                                          $have_selected = true;
                                                        }
                                                        $option_content = htmlspecialchars($frow['name'], ENT_NOQUOTES);
                                                        echo "<option value=\"$option_value\" $option_selected_attr>$option_content</option>\n";
                                                }
                                        ?>
                                        </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="6">
                                        <a href='#' class='btn btn-warning btn-sm' onclick='return submitform();'><span> <?php xl('Submit','e'); ?> </span> </a> 
                                </td>
                            </tr>
                        </tbody>
                </table>
            </form>
        </div>
</body>
</html>
