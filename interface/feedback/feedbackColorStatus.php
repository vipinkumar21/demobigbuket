<?php
//calculate feedback & return color
require_once("../globals.php");

function feedbackColorStatus($ftrans_ids)
{
$res = sqlStatement("select ft.*, fc.name, fty.ft_name from feedback_transaction AS ft INNER JOIN facility AS fc ON fc.id = ft.ftrans_fid INNER JOIN feedback_type AS fty ON fty.ft_id = ft.ftrans_ft_id where ft.ftrans_id='$ftrans_ids'");
for ($iter = 0;$row = sqlFetchArray($res);$iter++)
$result[$iter] = $row;
$iter = $result[0];

$fosql = "SELECT fo.*, fod.* FROM feedback_option AS fo INNER JOIN feedback_option_data AS fod ON fod.fod_fo_id = fo.fo_id WHERE fo.fo_ft_id =".$iter['ftrans_ft_id']." AND fod.fod_ftrans_id = ".$iter["ftrans_id"]." ORDER BY fo.fo_sequence asc";
$fores = sqlStatement($fosql);

$allFeed=array();

if(sqlNumRows($fores) > 0){
	$foCount = 0;
	while($forow = sqlFetchArray($fores)){
					$fovsql = "SELECT * FROM feedback_option_value WHERE fov_id = ".$forow['fod_fov_id'];
					$fovres = sqlStatement($fovsql);
					$fovrow = sqlFetchArray($fovres);
					$retAllFeed=$fovrow['fov_title']; 
					array_push($allFeed,$retAllFeed);
					//echo $retAllFeed;
	}
}
$avgCal=array_sum($allFeed);
$colorState=round($avgCal/3);

if($colorState>=9)
{
	$colorTd= "Green";
}
elseif($colorState>=7 and $colorState<=8)
{
	$colorTd= "Yellow";
}
elseif($colorState>=0 and $colorState<=6)
{
	$colorTd= "red";
}

return $colorTd;
}
//echo feedbackColorStatus(3);
?>
