<?php

// Copyright (C) 2005-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report shows upcoming appointments with filtering and
// sorting by patient, practitioner, appointment type, and date.
// 2012-01-01 - Added display of home and cell phone and fixed header

require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once "$srcdir/appointments.inc.php";
require_once(dirname(__FILE__) . "/../../library/classes/WSProvider.class.php");
require_once ($GLOBALS['srcdir'] . "/classes/postmaster.php");
require_once("../../library/pdo/lib/database.php");
require_once("$srcdir/S3.php");
if (isset($_POST["mode"]) && $_POST["mode"] == "new") {
    $feedbackDate = (!empty(trim(formData('ftrans_feedback_date')))) ? date("Y-m-d", strtotime(trim(formData('ftrans_feedback_date')))) : '';
    $doit = true;
    $feedbackFileUrl = '';
    $errMessage = $message = '';
    $max_size = 2000;  // sets maximum file size allowed (in KB)
    $fid = trim(formData('form_facility'));
    $allowtype = array('jpeg', 'png', 'gif', 'jpg', 'pdf'); // file types allowed
    $allowedImageType = array('application/pdf', 'image/jpeg', 'image/png', 'image/bmp');
    $responseSum = 0;
    $count = 0;
    $patientName = trim(formData('ftrans_pname'));
    $patientContact = trim(formData('ftrans_pcontact'));
    $ptSql = " Select COUNT(id) AS id FROM patient_data WHERE fname='$patientName' AND (phone_home = '$patientContact' OR phone_biz = '$patientContact' OR phone_contact = '$patientContact' OR phone_cell = '$patientContact')";
    $isPatient = $pdoobject->custom_query($ptSql, null, '', 'fetch');
    if ($isPatient['id'] > 0) {
    if (!empty($_FILES['file_name']['name'])) {
        // check for errors
        if ($_FILES['file_name']['error'] > 0) {
            $doit = false;
        } else {
            // get the name, size (in kb) and type (the extension) of the file
            $fname = $_FILES['file_name']['name'];
            $fsize = $_FILES['file_name']['size'] / 1024;
            $fileInfo = new SplFileInfo($fname);
            $fileExt = $fileInfo->getExtension();
            $newFileName = time() . '_' . $fname;
            //check to see if the keys have been set
            if (empty($GLOBALS['s3_access_key_id']) && empty($GLOBALS['s3_secret_access_key']) && empty($GLOBALS['s3_full_url']) && empty($GLOBALS['s3_bucket_name'])) {
                $doit = false;
            } else {
                $s3 = new S3($GLOBALS['s3_access_key_id'], $GLOBALS['s3_secret_access_key']);
                // if the file not exists, check its type (by extension) and size
                if (in_array($_FILES['file_name']['type'], $allowedImageType)) {
                    // check the size
                    if ($fsize <= $max_size) {
                        if ($s3->putObject($s3->inputResource(fopen($_FILES['file_name']['tmp_name'], 'rb'), filesize($_FILES['file_name']['tmp_name'])), $GLOBALS['s3_bucket_name'], $newFileName, S3::ACL_PUBLIC_READ, array(), array("Content-Type" => $_FILES['file_name']['type']))) {
                            //return true as file was uploaded
                            $feedbackFileUrl = $newFileName;
                        } else {
                            //return false as there was a problem
                            $doit = false;
                            $errMessage = "There was some error in uploading file to server!";
                        }
                    } else {
                        $doit = false;
                        $errMessage = "File size is greater than 2 MB";
                    }
                } else {
                    $doit = false;
                    $errMessage = "Please upload image or pdf file only!";
                }
            }
        }
    } else {
        $doit = false;
        $errMessage = "Please upload file";
    }
    } else {
        $doit = false;
        $errMessage = "Patient does not exists!";
    }
    if ($doit == true) {
        $feeddbackTypeValue = trim(formData('ftrans_ft_id'));
        $foSql = $pdoobject->custom_query("Select fo_id,fo_is_response from feedback_option WHERE fo_ft_id = '$feeddbackTypeValue' Order by fo_sequence");
        foreach ($foSql as $key => $value) {
            if ($value['fo_is_response'] == 1) {
                $responseSum += trim(formData('feedback_response_value_' . $value['fo_id']));
                $count++;
            }
        }
        $avgResponse = $responseSum / $count;
        if ($avgResponse >= 9) {
            $color = "Green";
            $response = 1; //Promotors
        } elseif ($avgResponse >= 7 && $avgResponse <= 8) {
            $color = "Yellow";
            $response = 2; //Passives
        } else {
            $color = "Red";
            $response = 3; // Detractors
        }
        try {
            $pdoobject->begin_transaction();
            $facNpiAndMaxSrNo = $pdoobject->custom_query("SELECT f.name, f.facility_npi, fi.fi_feedback_seq FROM facility AS f INNER JOIN facility_ids AS fi ON fi.`fi_fid` = f.id WHERE f.id = ?", array($fid), '', 'fetch');
            $maxFeedbackSequence = $facNpiAndMaxSrNo['fi_feedback_seq'] + 1;
            $feedbackAutoSeq = $facNpiAndMaxSrNo['facility_npi'] . "-" . $maxFeedbackSequence;
            $facilityName = $facNpiAndMaxSrNo['name'];
            $feedbackTransData = array(
                "ftrans_fid" => trim(formData('form_facility')),
                "ftrans_pid" => trim(formData('ftrans_pname_vs')),
                "ftrans_pname" => trim(formData('ftrans_pname')),
                "ftrans_pemail" => trim(formData('ftrans_pemail')),
                "ftrans_pcontact" => trim(formData('ftrans_pcontact')),
                "ftrans_paddress" => trim(formData('ftrans_paddress')),
                "ftrans_createdby" => trim($_SESSION['authId']),
                "ftrans_createddate" => date("Y-m-d H:i:s"),
                "ftrans_sr_no" => trim(formData('ftrans_sr_no')),
                "pubpid" => trim(formData('ftrans_pubpid')),
                "ftrans_feedback_date" => $feedbackDate,
                "ftrans_ft_id" => trim(formData('ftrans_ft_id')),
                "ftrans_response" => $response,
                "ftrans_color" => $color,
                "ftrans_file_url" => $GLOBALS['s3_full_url'] . $feedbackFileUrl,
                "ftrans_auto_sr_no" => $feedbackAutoSeq
            );
            $pdoobject->update("facility_ids", array("fi_feedback_seq" => $maxFeedbackSequence), array("fi_fid" => $fid));
            $insertId = $pdoobject->insert("feedback_transaction", $feedbackTransData);
            $fdArray = array(
                "fd_ftrans_id" => $insertId,
                "fd_ftrans_file_url" => $GLOBALS['s3_full_url'] . $feedbackFileUrl,
                "created" => date("Y-m-d H:i:s"),
                "created_by" => trim($_SESSION['authId']),
                "status" => 1,
                "fd_file_name" => $fname
            );
            $pdoobject->insert("feedback_document_logs", $fdArray);
            $responseArr = array();
            foreach ($foSql as $key => $value1) {
                $fovSql = "select fov_id from feedback_option_value where fov_fo_id=? and fov_title =?";
                if ($value1['fo_is_response'] == 1) {
                    $optionData = trim(formData('feedback_response_value_' . $value1['fo_id']));
                    $fovValue = $pdoobject->custom_query($fovSql, array($value1['fo_id'], $optionData), '', 'fetch');
                    array_push($responseArr, $optionData);
                } else {
                    $optionData = trim(formData('feedback_option_data_' . $value1['fo_id']));
                    $fovValue = $pdoobject->custom_query($fovSql, array($value1['fo_id'], $optionData), '', 'fetch');
                    array_push($responseArr, $optionData);
                }
                $feedbackOptions = array(
                    "fod_ftrans_id" => $insertId,
                    "fod_fo_id" => $value1['fo_id'],
                    "fod_fov_id" => $fovValue['fov_id'],
                    "fod_data" => $optionData
                );
                $pdoobject->insert("feedback_option_data", $feedbackOptions);
            }
            list($fcr_q1r, $fcr_q2r, $fcr_q3r, $fcr_q4t, $fcr_q5r) = $responseArr;
            $clinicReportArr = array(
                "fcr_ftrans_id" => $insertId,
                "fcr_patient_name" => trim(formData('ftrans_pname')),
                "fcr_pid" => trim(formData('form_facility')), //Actually this field is for holding clinic Id not for Patient Id
                "fcr_clinic" => $facilityName,
                "fcr_date" => $feedbackDate,
                "fcr_fill" => "yes",
                "fcr_q1r" => $fcr_q1r,
                "fcr_q2r" => $fcr_q2r,
                "fcr_q3r" => $fcr_q3r,
                "fcr_q4t" => $fcr_q4t,
                "fcr_q5r" => $fcr_q5r
            );
            $pdoobject->insert("feedback_clinic_report", $clinicReportArr);
            $pdoobject->commit();
            $message = "Feedback has been saved successfully";
        } catch (Exception $ex) {
            $pdoobject->rollback();
            $errMessage = $ex->getMessage();
        }
    }
    $_SESSION['feedbackTypeWithError'] = '';
    if(!empty($errMessage))
    {
        $_SESSION['fbError'] = $errMessage ;
        $_SESSION['PostData'] = $_POST;
        $_SESSION['feedbackTypeWithError'] = formData('ftrans_ft_id');
         header("location:feedback_add.php?page=1");
    } else 
    {
        $_SESSION['fbSuccess'] = $message ;
        header("location:feedback_report.php?page=1");
    }
}
/*		Inserting feedback follow up					*/
if (isset($_POST["mode"]) && $_POST["mode"] == "followup") {
    try {
        $pdoobject->begin_transaction();
        $feedbackTransArr = array(
            "ftrans_status" => trim(formData('form_status')),
            "ftrans_modifiedby" => trim($_SESSION['authUser']),
            "ftrans_modifieddate" => date("Y-m-d H:i:s")
        );
        $where = array("ftrans_id" => formData("ftrans_id"));
        $pdoobject->update("feedback_transaction", $feedbackTransArr, $where);
        $fbRemarkArr = array(
            "ftr_ftrans_id" => trim(formData("ftrans_id")),
            "ftr_from_status" => trim(formData('ftrans_pre_status')),
            "ftr_to_status" => trim(formData('form_status')),
            "ftr_remark" => trim(formData('form_remark')),
            "ftr_created_by" => trim($_SESSION['authId']),
            "ftr_created_date" => date("Y-m-d H:i:s"),
        );
        $insert_id = $pdoobject->insert("feedback_transaction_remark", $fbRemarkArr);
        $pquery = "SELECT ftrans_pid AS id, ftrans_pname AS name, ftrans_pemail AS email FROM feedback_transaction WHERE ftrans_id=?";
        $pfrow = $pdoobject->custom_query($pquery, array(formData("ftrans_id")), '', 'fetch');
        if (!empty($pfrow['email']) && $pfrow['email'] != '') {

            $patientEmail = $pfrow['email'];
            $patientName = $pfrow['name'];
            $query = "SELECT id, CONCAT_WS(' ', fname, lname) AS name, email FROM users WHERE id=?";
            $frow = $pdoobject->custom_query($pquery, array(formData('form_bcc')), '', 'fetch');
            $bccEmail = $frow['email'];
            $bccName = $frow['name'];
            $mail = new MyMailer();
            $mail->SetLanguage("en", $GLOBALS['fileroot'] . "/library/");
            $mail->FromName = "Feedback Administrator";
            $mail->Sender = $GLOBALS['patient_reminder_sender_email'];    // required
            $mail->From = $GLOBALS['patient_reminder_sender_email'];    // required
            $resQuery = "SELECT ft.`ftrans_id`,ft.`ftrans_pname`,ft.`ftrans_createddate`, fc.`name`,fc.`phone`,fc.`website`,fc.`street`,fc.`city`,fc.`state`,fc.`country_code`,fc.`postal_code` FROM feedback_transaction AS ft INNER JOIN facility AS fc ON fc.id = ft.ftrans_fid WHERE ft.ftrans_id =?";
            $iter = $pdoobject->custom_query($resQuery, array(formData("ftrans_id")), '', 'fetch');
            $preStatus = '';
            switch (formData('ftrans_pre_status')) {
                case 0:
                    $preStatus = 'Open';
                    break;
                case 1:
                    $preStatus = 'In Progress';
                    break;
                case 2:
                    $preStatus = 'Closed';
                    break;
            }
            $currentStatus = '';
            switch (formData('form_status')) {
                case 0:
                    $currentStatus = 'Open';
                    break;
                case 1:
                    $currentStatus = 'In Progress';
                    break;
                case 2:
                    $currentStatus = 'Closed';
                    break;
            }
            $text_body = "<div style='border:solid #B8B8B8 4.5pt;padding:0in 0in 0in 0in'>  
              <div align=center>
              <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=580  style='width:435.0pt;background:#FDFDFD;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in' id=templateContainer>  
               <tr style='mso-yfti-irow:1'>
                    <td valign=top style='padding:0in 0in 0in 0in'>
                    <div align=center>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=600
                     style='width:6.25in;border-collapse:collapse;mso-yfti-tbllook:1184;
                     mso-padding-alt:0in 0in 0in 0in' id=templateBody>
                     <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                      <td valign=top style='background:#FDFDFD;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                      <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                       width='100%' style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:
                       1184;mso-padding-alt:0in 0in 0in 0in'>
                       <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                            <td width=10 style='width:7.5pt;background:whitesmoke;padding:7.5pt 7.5pt 7.5pt 7.5pt'></td>
                            <td valign=top style='padding:7.5pt 7.5pt 7.5pt 22.5pt'>
                            <p class=MsoNormal style='margin-bottom:12.0pt;text-align:justify;
                            line-height:150%'><strong><span style='font-size:10.5pt;line-height:
                            150%;font-family:'Arial','sans-serif';color:#505050'>Dear " . $iter['ftrans_pname'] . "</span></strong><span style='font-size:10.5pt;line-height:150%;font-family:'Arial','sans-serif';color:#505050'><br>
                            <br>
                            This is to notify you about your feedback Follow Up at Clove Dental - " . $iter['name'] . ".</span></p>
                            <table class=MsoNormalTable border=0 cellpadding=0 style='mso-cellspacing:
                             1.5pt;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in'>
                             <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                              <td width=150 valign=top style='width:112.5pt;padding:.75pt .75pt .75pt .75pt'>
                              <p class=MsoNormal align=right style='text-align:right'>WHEN:</p>
                              </td>
                              <td width=390 valign=top style='width:292.5pt;padding:.75pt .75pt .75pt .75pt'>
                              <p class=MsoNormal><strong>" . date('d-m-y', strtotime($iter['ftrans_createddate'])) . "</strong> </p>
                              </td>
                             </tr>
                             <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                              <td width=150 valign=top style='width:112.5pt;padding:.75pt .75pt .75pt .75pt'>
                              <p class=MsoNormal align=right style='text-align:right'>CURRENT STATUS:</p>
                              </td>
                              <td width=390 valign=top style='width:292.5pt;padding:.75pt .75pt .75pt .75pt'>
                              <p class=MsoNormal><strong>" . $currentStatus . "</strong> </p>
                              </td>
                             </tr>
                             <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                              <td width=150 valign=top style='width:112.5pt;padding:.75pt .75pt .75pt .75pt'>
                              <p class=MsoNormal align=right style='text-align:right'>PRE STATUS:</p>
                              </td>
                              <td width=390 valign=top style='width:292.5pt;padding:.75pt .75pt .75pt .75pt'>
                              <p class=MsoNormal><strong>" . $preStatus . "</strong> </p>
                              </td>
                             </tr>
                             <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
                              <td valign=top style='padding:7.5pt .75pt .75pt .75pt'>
                              <p class=MsoNormal align=right style='text-align:right'>REMARK:</p>
                              </td>
                              <td valign=top style='padding:7.5pt .75pt .75pt .75pt'>
                              <p class=MsoNormal>" . nl2br(formData('form_remark')) . "</p>
                              </td>
                             </tr>
                            </table>
                            <p class=MsoNormal style='text-align:justify;line-height:150%'><span
                            style='font-size:10.5pt;line-height:150%;font-family:'Arial','sans-serif';
                            color:#505050'><br>
                            For any queries, contact us at : <strong><span style='font-family:'Arial','sans-serif''>" . $iter['phone'] . "</span></strong> <br>
                            You can read more about Clove Dental at <a href='http://" . $iter['website'] . "'>" . $iter['website'] . "</a>
                            </span></p>
                            </td>
                       </tr>
                      </table>
                      </td>
                     </tr>
                    </table>
                    </div>
                    </td>
               </tr>
               <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
                    <td valign=top style='padding:0in 0in 0in 0in'>
                    <div align=center>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=580 style='width:435.0pt;background:#FAFAFA;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in' id=templateFooter>
                     <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                      <td valign=top style='border:none;border-top:solid #909090 2.25pt;
                      padding:0in 0in 0in 0in'>
                      <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                       width='100%' style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:
                       1184;mso-padding-alt:0in 0in 0in 0in'>
                       <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                            <td valign=top style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                            <p class=MsoNormal align=right style='text-align:right;line-height:125%'><strong><span style='font-size:8.5pt;line-height:125%;font-family:'Arial','sans-serif';color:#707070'>Clove Dental</span></strong>
                            <span style='font-size:8.5pt;line-height:125%;font-family:'Arial','sans-serif';color:#707070'> <br>
                            " . $iter['street'] . "<br>
                            " . $iter['name'] . "<br>
                            " . $iter['city'] . ", " . $iter['state'] . ", " . $iter['country_code'] . " - " . $iter['postal_code'] . " <br>
                            " . $iter['phone'] . " <br>
                            <a href='http://" . $iter['website'] . "'>" . $iter['website'] . "</a>
                            </span></p>
                            </td>
                       </tr>
                      </table>
                      </td>
                     </tr>
                    </table>
                    </div>
                    </td>
               </tr>
              </table>
              </div>
              </div>";
            $mail->Body = $text_body;
            $mail->Subject = "Feedback Follow Up Notification";
            $mail->AddAddress($patientEmail, $patientName);
            $mail->AddBCC($bccEmail, $bccName);
            $mail->Send();
        }
        $pdoobject->commit();
    } catch (Exception $exc) {
        $pdoobject->rollback();
        echo $exc->getMessage();
    }
    ?>
    <script type="text/javascript">
        parent.window.location.href = 'feedback_report.php?page=1';
    </script>
    <?php

}
if (isset($_POST["mode"]) && $_POST["mode"] == "edit") {
    $page = $_POST['page'];
    $doit = true;
    $feedbackFileUrl = '';
    $max_size = 2000;  // sets maximum file size allowed (in KB)
    $allowtype = array('jpeg', 'png', 'gif', 'jpg','pdf'); // file types allowed
    $allowedImageType = array('application/pdf', 'image/jpeg', 'image/png', 'image/bmp');
    $responseSum = 0;
    $count = 0;
    $message = '';
    $errMessage ='';
    try {
        $ftransId = $_POST['ftrans_id'];
        $feedbackDate = (!empty(trim(formData('ftrans_feedback_date')))) ? date("Y-m-d", strtotime(trim(formData('ftrans_feedback_date')))) : '';
        $patientName = trim(formData('ftrans_pname'));
        $patientContact = trim(formData('ftrans_pcontact'));
        $ptSql = "Select id AS id FROM patient_data WHERE fname='$patientName' AND (phone_home = '$patientContact' OR phone_biz = '$patientContact' OR phone_contact = '$patientContact' OR phone_cell = '$patientContact')";
        $isPatient = $pdoobject->custom_query($ptSql, null, '', 'fetch');
        if ($isPatient['id'] > 0) {
            if (!empty($_FILES['file_name']['name'])) {
                // check for errors
                if ($_FILES['file_name']['error'] > 0) {
                    $doit = false;
                } else {
                    // get the name, size (in kb) and type (the extension) of the file
                    $fname = $_FILES['file_name']['name'];
                    $fsize = $_FILES['file_name']['size'] / 1024;
                    $fileInfo = new SplFileInfo($fname);
                    $fileExt = $fileInfo->getExtension();
                    $newFileName = time() . '_' . $fname;
                    //check to see if the keys have been set
                    if (empty($GLOBALS['s3_access_key_id']) && empty($GLOBALS['s3_secret_access_key']) && empty($GLOBALS['s3_full_url']) && empty($GLOBALS['s3_bucket_name'])) {
                        $doit = false;
                    } else {
                        $s3 = new S3($GLOBALS['s3_access_key_id'], $GLOBALS['s3_secret_access_key']);
                        // if the file not exists, check its type (by extension) and size
                        if (in_array($_FILES['file_name']['type'], $allowedImageType)) {
                            // check the size
                            if ($fsize <= $max_size) {
                                if ($s3->putObject($s3->inputResource(fopen($_FILES['file_name']['tmp_name'], 'rb'), filesize($_FILES['file_name']['tmp_name'])), $GLOBALS['s3_bucket_name'], $newFileName, S3::ACL_PUBLIC_READ, array(), array("Content-Type" => $_FILES['file_name']['type']))) {
                                    //return true as file was uploaded
                                    $feedbackFileUrl = $newFileName;
                                } else {
                                    //return false as there was a problem
                                    $doit = false;
                                    $errMessage = "There was some error in uploading file to server!";
                                }
                            } else {
                                $doit = false;
                                $errMessage = "File size is greater than 2 MB";
                            }
                        } else {
                            $doit = false;
                            $errMessage = "Please upload image or pdf file only!";
                        }
                    }
                }
            } else {
                $doit = true;
            }
            
            if ($doit == true) {
                $feeddbackTypeValue = trim(formData('ftrans_ft_id'));
                $foSql = $pdoobject->custom_query("Select fo_id,fo_is_response from feedback_option WHERE fo_ft_id = '$feeddbackTypeValue' Order by fo_sequence");
            foreach ($foSql as $key => $value) {
                if ($value['fo_is_response'] == 1) {
                    $responseSum += trim(formData('feedback_response_value_' . $value['fo_id']));
                    $count++;
                }
            }
            $avgResponse = $responseSum / $count;
            if ($avgResponse >= 9) {
                $color = "Green";
                $response = 1; //Promotors
            } elseif ($avgResponse == 7 || $avgResponse == 8) {
                $color = "Yellow";
                $response = 2; //Passives
            } else {
                $color = "Red";
                $response = 3; // Detractors
            }
                try {
                    $pdoobject->begin_transaction();
                    $feedbackTransData = array(
                        "ftrans_fid" => trim(formData('form_facility')),
                        "ftrans_pid" => trim(formData('ftrans_pname_vs')),
                        "ftrans_pname" => trim(formData('ftrans_pname')),
                        "ftrans_pemail" => trim(formData('ftrans_pemail')),
                        "ftrans_pcontact" => trim(formData('ftrans_pcontact')),
                        "ftrans_paddress" => trim(formData('ftrans_paddress')),
                        "ftrans_modifiedby" => trim($_SESSION['authId']),
                        "ftrans_modifieddate" => date("Y-m-d H:i:s"),
                        "ftrans_sr_no" => trim(formData('ftrans_sr_no')),
                        "pubpid" => trim(formData('ftrans_pubpid')),
                        "ftrans_feedback_date" => $feedbackDate,
                        "ftrans_ft_id" => trim(formData('ftrans_ft_id')),
                        "ftrans_response" => $response,
                        "ftrans_color" => $color
                    );
                    if(!empty($_FILES['file_name']['name']) && ( $doit == true) && !empty($feedbackFileUrl))
                    {
                        $pdoobject->update("feedback_document_logs",array("status"=> '0',"modified"=>date("Y-m-d H:i:s"),"modified_by"=>$_SESSION['authId']),array("fd_ftrans_id"=>$ftransId));
                        $fdArray = array(
                            "fd_ftrans_id" => $ftransId,
                            "fd_ftrans_file_url" => $GLOBALS['s3_full_url'] . $feedbackFileUrl,
                            "created" => date("Y-m-d H:i:s"),
                            "created_by" => trim($_SESSION['authId']),
                            "status" => 1,
                            "fd_file_name" => $fname
                        );
                        $pdoobject->insert("feedback_document_logs", $fdArray);
                        
                        $feedbackTransData['ftrans_file_url']= $GLOBALS['s3_full_url'] . $feedbackFileUrl;                        
                    } 
                    $pdoobject->update("feedback_transaction", $feedbackTransData, array('ftrans_id' => $ftransId));
                    
                    $responseArr = array();
                    //Update Logs
                    $feedbackOptions = array(
                        "fod_status" => 0
                    );
                    $pdoobject->update("feedback_option_data", $feedbackOptions, array("fod_ftrans_id" => $ftransId, "fod_status" => 1));
                    
                    foreach ($foSql as $key => $value) {
                        $fovSql = "select fov_id from feedback_option_value where fov_fo_id=? and fov_title =?";
                        if ($value['fo_is_response'] == 1) {
                            $optionData = trim(formData('feedback_response_value_' . $value['fo_id']));
                            $fovValue = $pdoobject->custom_query($fovSql, array($value['fo_id'], $optionData), '', 'fetch');
                            array_push($responseArr, $optionData);
                        } else {
                            $optionData = trim(formData('feedback_option_data_' . $value['fo_id']));
                            $fovValue = $pdoobject->custom_query($fovSql, array($value['fo_id'], $optionData), '', 'fetch');
                            array_push($responseArr, $optionData);
                        }
                        $newFeedbackOptions = array(
                            "fod_ftrans_id" => $ftransId,
                            "fod_fo_id" => $value['fo_id'],
                            "fod_fov_id" => $fovValue['fov_id'],
                            "fod_data" => $optionData,
                            "fod_status" => 1
                        );
                        $pdoobject->insert("feedback_option_data", $newFeedbackOptions);
                    }
                    list($fcr_q1r, $fcr_q2r, $fcr_q3r, $fcr_q4t, $fcr_q5r) = $responseArr;
                    $clinicReportArr = array(
                        "fcr_patient_name" => trim(formData('ftrans_pname')),
                        "fcr_pid" => trim(formData('form_facility')),
                        //"fcr_clinic" => trim(formData('facilityName')),
                        "fcr_date" => $feedbackDate,
                        "fcr_fill" => "yes",
                        "fcr_q1r" => $fcr_q1r,
                        "fcr_q2r" => $fcr_q2r,
                        "fcr_q3r" => $fcr_q3r,
                        "fcr_q4t" => $fcr_q4t,
                        "fcr_q5r" => $fcr_q5r
                    );
                    $pdoobject->update("feedback_clinic_report", $clinicReportArr, array("fcr_ftrans_id" => $ftransId));
                    $pdoobject->commit();
                    $message = "Feedback has been updated successfully";
                } catch (Exception $ex) {
                    $pdoobject->rollback();
                    $errMessage = $ex->getMessage();
                    echo $errMessage;
                    exit();
                }
            }
        } else {
            $errMessage = "Patient does not exists!";
        }
    } catch (Exception $ex) {
        $errMessage = $ex->getMessage();
    }
    if(!empty($errMessage))
    {
        $_SESSION['fbError'] = $errMessage ;
        $_SESSION['PostData'] = $_POST;
         header("location:feedback_add.php?id=$ftransId&page=$page");
    } else 
    {
        $_SESSION['fbSuccess'] = $message ;
        header("location:feedback_report.php?page=$page");
    }
    ?>
    <?php
}
?>
