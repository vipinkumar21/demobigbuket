<?php
// Copyright (C) 2013 Prashant Shekher <prashant.shekher@instantsys.com>
//
// This program is for PRM software.
require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("../inventory/drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

$searchParam = '';

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

$alertmsg = ''; // not used yet but maybe later
//$patient = $_REQUEST['patient'];
$from_date = fixDate($_POST['form_from_date'], '');
$to_date = fixDate($_POST['form_to_date'], '');
//if (empty($to_date) && !empty($from_date)) $to_date = date('Y-12-31');
//if (empty($from_date) && !empty($to_date)) $from_date = date('Y-01-01');

$show_available_times = false;
$ORDERHASH = array(
    'invistr_id' => 'invistr_id DESC',
    'inv_im_name' => 'inv_im_name',
    'inv_im_status' => 'inv_im_status',
);
// Get the order hash array value and key for this request.
$form_orderby = $ORDERHASH[$_REQUEST['form_orderby']] ? $_REQUEST['form_orderby'] : 'invistr_id';
$orderby = $ORDERHASH[$form_orderby];



########################################################################################################
$form_facility = $_POST['form_facility'];
$form_date = $from_date;
$form_to_date = $to_date;
$conditions = "";
if ($form_facility) {
    //for individual facility
    if ($form_date) {
        //if from date selected
        if ($form_to_date) {
            // if $form_date && $form_to_date
            $toDate = date_create(date($form_to_date));
            $fromDate = date_create($form_date);
            $diff = date_diff($fromDate, $toDate);
            $days_between_from_to = $diff->days;

            if ($days_between_from_to <= $form_facility_time_range) {

                if ($conditions) {
                    $conditions .= " AND ";
                }
                //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
                $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
            } else {
                if ($days_between_from_to <= $form_facility_time_range_valid) {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "feedback_referral_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "facility" => $form_facility))); // changable
                    $rcsl_report_description = "Feedback referral report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog = $msgForReportLog;
                    } else {
                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                } else {
                    $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                }
            }
        } else {
            //only from date: no TO date
            $form_to_date = date('Y-m-d', strtotime("+$form_facility_time_range day", strtotime($form_date)));
            $toDate = date_create(date($form_to_date));
            $fromDate = date_create($form_date);
            $diff = date_diff($fromDate, $toDate);
            $days_between_from_to = $diff->days;

            if ($days_between_from_to <= $form_facility_time_range) {
                if ($conditions) {
                    $conditions .= " AND ";
                }
                //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
                $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
            } else {
                if ($days_between_from_to <= $form_facility_time_range_valid) {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "feedback_referral_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "facility" => $form_facility))); // changable
                    $rcsl_report_description = "Feedback referral report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog = $msgForReportLog;
                    } else {
                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                } else {
                    $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                }
            }
        }
    } else {
        // if from date not selected
        if ($form_to_date) {
            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
            $toDate = date_create(date($form_to_date));
            $fromDate = date_create($form_date);
            $diff = date_diff($fromDate, $toDate);
            $days_between_from_to = $diff->days;

            if ($days_between_from_to <= $form_facility_time_range) {
                if ($conditions) {
                    $conditions .= " AND ";
                }
                //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
                $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
            } else {
                if ($days_between_from_to <= $form_facility_time_range_valid) {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "feedback_referral_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date, "facility" => $form_facility))); // changable
                    $rcsl_report_description = "Feedback referral report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog = $msgForReportLog;
                    } else {
                        $msgForReportLog = "$form_facility_time_range_errorMsg <br>";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                } else {
                    $msgForReportLog = $form_facility_time_range_valid_errorMsg;
                }
            }
        } else {
            if ($conditions) {
                $conditions .= " AND ";
            }
            $form_to_date = date("Y-m-d");
            $form_date = date('Y-m-d', strtotime("-$form_facility_time_range day", strtotime($form_to_date)));
            //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
            $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date' AND ft.ftrans_fid = '$form_facility'";
        }
    }
    
    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | ClinicId = ' . $form_facility . ' | '; /// Auditing Section Param
} else {
    //for all facility
    if ($form_date) {
        //if from date selected
        if ($form_to_date) {
            // if $form_date && $form_to_date
            $toDate = date_create(date($form_to_date));
            $fromDate = date_create($form_date);
            $diff = date_diff($fromDate, $toDate);
            $days_between_from_to = $diff->days;

            if ($days_between_from_to <= $form_facility_all_time_range) {

                if ($conditions) {
                    $conditions .= " AND ";
                }
                //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date'";
                $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date'";
            } else {
                if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "feedback_referral_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date))); // changable
                    $rcsl_report_description = "Feedback referral report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog = $msgForReportLog;
                    } else {
                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                } else {
                    $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                }
            }
        } else {
            //only from date: no TO date
            $form_to_date = date('Y-m-d', strtotime("+$form_facility_all_time_range day", strtotime($form_date)));
            $toDate = date_create(date($form_to_date));
            $fromDate = date_create($form_date);
            $diff = date_diff($fromDate, $toDate);
            $days_between_from_to = $diff->days;

            if ($days_between_from_to <= $form_facility_all_time_range) {
                if ($conditions) {
                    $conditions .= " AND ";
                }
                //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date'";
                $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date'";
            } else {
                if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "feedback_referral_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date))); // changable
                    $rcsl_report_description = "Feedback referral report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog = $msgForReportLog;
                    } else {
                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                } else {
                    $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                }
            }
        }
    } else {
        // if from date not selected
        if ($form_to_date) {
            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
            $toDate = date_create(date($form_to_date));
            $fromDate = date_create($form_date);
            $diff = date_diff($fromDate, $toDate);
            $days_between_from_to = $diff->days;

            if ($days_between_from_to <= $form_facility_all_time_range) {
                if ($conditions) {
                    $conditions .= " AND ";
                }
                //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date'";
                $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date'";
            } else {
                if ($days_between_from_to <= $form_facility_all_time_range_valid) {
                    ##########Cron Request####################section only for all FACILITY#########################
                    // following value will be change according to report
                    $rcsl_name_type = "feedback_referral_report"; // changable
                    $rcsl_requested_filter = addslashes(serialize(array("fromDate" => $form_date, "toDate" => $form_to_date))); // changable
                    $rcsl_report_description = "Feedback referral report from $form_date to $form_to_date"; // changable
                    //allFacilityReports() defined with globals.php
                    $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                    if ($msgForReportLog) {
                        $msgForReportLog = $msgForReportLog;
                    } else {
                        $msgForReportLog = "$form_facility_all_time_range_errorMsg <br>";
                    }
                    ##########Cron Request####################section only for all FACILITY#########################
                } else {
                    $msgForReportLog = $form_facility_all_time_range_valid_errorMsg;
                }
            }
        } else {
            if ($conditions) {
                $conditions .= " AND ";
            }
            $form_to_date = date("Y-m-d");
            $form_date = date('Y-m-d', strtotime("-$form_facility_all_time_range day", strtotime($form_to_date)));
            //$conditions .= "ft.ftrans_createddate >= '$form_date' AND ft.ftrans_createddate <= '$form_to_date'";
            $conditions .= "DATE(ft.ftrans_createddate) BETWEEN '$form_date' AND '$form_to_date'";
        }
    }
    
    $searchParam .= ' From Date = ' . $form_date . ' | To Date = ' . $form_to_date . ' | Clinic = All | '; /// Auditing Section Param
}


########################################################################################################
// In the case of CSV export only, a download will be forced.
$fileName = "feedback_referral_".date("Ymd_his").".csv";
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('Report - Feedback Referral', 'e'); ?></title>

            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>

            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

            <script type="text/javascript">
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
                function dosort(orderby) {
                    var f = document.forms[0];
                    f.form_orderby.value = orderby;
                    f.submit();
                    return false;
                }
                function oldEvt(eventid) {
                    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
                }
                function refreshme() {
                    document.forms[0].submit();
                }
            </script>

            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>

        <body class="body_top">

            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Feedback Referral', 'e'); ?></h3>
            <div id="report_parameters_daterange"><?php //echo date("d F Y", strtotime($from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($to_date));  ?>
            </div>

            <form method='post' name='theform' id='theform' action='feedback_referral_report.php' class='emrform topnopad '>
                <div id="report_parameters" class='searchReport'>
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='82%'>
                            <!-- <td width='650px'> -->
                                <div style='float: left; width: 100%;'>
                                    <table class='text'>
                                        <tr>
                                            <td width="5%" class='label'><?php xl('Facility', 'e'); ?>:</td>
                                            <td width="25%"><?php dropdown_facility(strip_escape_custom($form_facility), 'form_facility', false, true); ?></td>

                                            <td class='label' width="5%"><?php xl('From', 'e'); ?>:</td>
                                            <td width="30%"><input class='emrdate' type='text' name='form_from_date' id="form_from_date"
                                                                   size='10' value='<?php echo $from_date ?>'
                                                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                   align='absbottom' width='24' height='22' id='img_from_date'
                                                                   border='0' alt='[?]' style='cursor: pointer'
                                                                   title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                            <td class='label' width="5%"><?php xl('To', 'e'); ?>:</td>
                                            <td width="30%"><input class='emrdate' type='text' name='form_to_date' id="form_to_date"
                                                                   size='10' value='<?php echo $to_date ?>'
                                                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                   align='absbottom' width='24' height='22' id='img_to_date'
                                                                   border='0' alt='[?]' style='cursor: pointer'
                                                                   title='<?php xl('Click here to choose a date', 'e'); ?>'></td>

                                        </tr>

                                    </table>
                                </div>
                            </td>
                            <td width='18%' align='left' valign='middle' height="100%">
                                <table style='border-left: 1px solid; width: 100%; height: 100%'>
                                    <tr>
                                        <td>
                                            <div style='margin-left: 15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewConsumption");
                                                        $("#theform").submit();'>
                                                    <span> <?php xl('Submit', 'e'); ?> </span> </a> 

    <?php if (1) { ?>
                                                    <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvFeedbackReferral");
                                                            $("#theform").submit();'>
                                                        <span>
                                                    <?php xl('Export to CSV', 'e'); ?>
                                                        </span>
                                                    </a>
                                                        <?php } ?>	
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
                <!-- end of search parameters --> <?php
                                                } // end not form_csvexport
                                                if ($_POST['form_refresh'] || $_POST['form_orderby'] || $_POST['form_csvexport']) {
                                                    if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
                                                        // CSV headers:
                                                        //echo '"' . xl('S. No.') . '",';
                                                        if ($msgForReportLog == "") {
                                                            echo '"' . xl('Name') . '",';
                                                            echo '"' . xl('Mobile #') . '",';
                                                            echo '"' . xl('Referral Name') . '",';
                                                            echo '"' . xl('Referral Mobile') . '",';
                                                            echo '"' . xl('Referral Relation') . '",';
                                                            echo '"' . xl('Referral Residence Area') . '",';
                                                            echo '"' . xl('Date') . '"' . "\n";
                                                        } else {
                                                            echo $msgForReportLog;
                                                        }
                                                    } else {
                                                            ?>
                    <div id="report_results">
                        <table class='emrtable'>
                            <tr class='head'>
                    <?php if ($msgForReportLog == "") { ?>
                                <thead>
                                <th><?php xl('Name', 'e'); ?></th>
                                <th><?php xl('Mobile #', 'e'); ?></th>
                                <th><?php xl('Referral Name', 'e'); ?></th>
                                <th><?php xl('Referral Mobile', 'e'); ?></th>
                                <th><?php xl('Referral Relation', 'e'); ?></th>
                                <th><?php xl('Referral Residence Area', 'e'); ?></th>
                                <th><?php xl('Date', 'e'); ?></th>
                                </thead>
                                <tbody>
        <?php } ?>
                                <!-- added for better print-ability -->
        <?php
    }
    $dateMysqlFormat = getDateDisplayFormat(1);

       if (!empty($_POST['form_csvexport']) && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
            $event = "Report Referral Feedback Export";
        } else {
            $event = "Report Referral Feedback View";
        }
        
    if ($msgForReportLog) {
        
         debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
         
    } else {
        
        $auditid = debugADOReports($sql, '', $event, $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section
        
        $query = "SELECT ft.ftrans_fid, ft.ftrans_pname, ft.ftrans_pcontact, ft.ftrans_pid, ft.ftrans_createddate, DATE_FORMAT(ft.ftrans_createddate, '%d-%m-%y') AS createddateformate, fr.fr_name, fr.fr_mobile, fr.fr_relation, fr.fr_area_of_residence
FROM feedback_referral AS fr LEFT JOIN  feedback_transaction AS ft ON fr.fr_ftrans_id = ft.ftrans_id 
WHERE  " . $conditions . " ORDER BY ft.ftrans_createddate DESC, ft.ftrans_id DESC";

        $res = sqlStatement($query, null, $GLOBALS['adodb']['dbreport']);

        $lastid = "";
        $srCount = 1;
        if (sqlNumRows($res)) {
            while ($row = sqlFetchArray($res)) {
                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
                    //echo '"' . qescape($srCount) . '",';
                    echo '"' . qescape($row['ftrans_pname']) . '",';
                    echo '"' . qescape($row['ftrans_pcontact']) . '",';
                    echo '"' . qescape($row['fr_name']) . '",';
                    echo '"' . qescape($row['fr_mobile']) . '",';
                    echo '"' . qescape($row['fr_relation']) . '",';
                    echo '"' . qescape($row['fr_area_of_residence']) . '",';
                    echo '"' . qescape($row['createddateformate']) . '"' . "\n";
                } else {
                    ?>
                                            <tr bgcolor='<?php echo $bgcolor ?>'>
                                            <?php if (0) { ?><td class="detail">&nbsp;<?php echo $srCount; ?></td><?php } ?>
                                                <td class="detail"><?php echo $row['ftrans_pname']; ?></td>
                                                <td class="detail"><?php echo $row['ftrans_pcontact']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $row['fr_name']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $row['fr_mobile']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $row['fr_relation']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $row['fr_area_of_residence']; ?></td>
                                                <td class="detail">&nbsp;<?php echo $row['createddateformate']; ?></td>
                                            </tr>
                <?php
                }
                $srCount++;
            }
        }
        
        debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']);
    }
    // assign the session key with the $pid_list array - note array might be empty -- handle on the printed_fee_sheet.php page.
    //$_SESSION['pidList'] = $pid_list;
    if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvFeedbackReferral') {
        ?>
                                <?php
                                $srCountFlag = $srCount - 1;
                                if (!$srCountFlag) {
                                    ?>
                                    <tr>
                                        <td colspan='8'><?php xl('No Record Found', 'e'); ?></td>
                                    </tr>
                                <?php }
                                if ($msgForReportLog) {
                                    ?>
                                    <tr>
                                        <td align='center' colspan='8'><?php xl($msgForReportLog, 'e'); ?></td>
                                    </tr>
        <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- end of search results --> <?php }
} else {
    ?>
                <div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>

                <!-- stuff for the popup calendar -->
                <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
                <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
                <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
                <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
                <script type="text/javascript">
                    Calendar.setup({inputField: "form_from_date", ifFormat: "%Y-%m-%d", button: "img_from_date"});
                    Calendar.setup({inputField: "form_to_date", ifFormat: "%Y-%m-%d", button: "img_to_date"});
                </script>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField: "form_from_date"
                        , ifFormat: "%Y-%m-%d"
                        , button: "img_from_date"
                        , onUpdate: function () {
                            var toDate = document.getElementById('form_to_date').value,
                                    fromDate = document.getElementById('form_from_date').value;

                            if (toDate) {
                                var toDate = new Date(toDate),
                                        fromDate = new Date(fromDate);

                                if (fromDate > toDate) {
                                    jAlert('From date cannot be later than To date');
                                    document.getElementById('form_from_date').value = '';
                                }
                            }
                        }
                    });

                    Calendar.setup({
                        inputField: "form_to_date"
                        , ifFormat: "%Y-%m-%d"
                        , button: "img_to_date"
                        , onUpdate: function () {
                            var toDate = document.getElementById('form_to_date').value,
                                    fromDate = document.getElementById('form_from_date').value;

                            if (fromDate) {
                                var toDate = new Date(toDate),
                                        fromDate = new Date(fromDate);

                                if (fromDate > toDate) {
                                    jAlert('From date cannot be later than To date');
                                    document.getElementById('form_to_date').value = '';
                                }
                            }
                        }
                    });
                </script>

<?php } ?> 
<?php
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvFeedbackReferral') {
    ?>
                <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>" /> 
                <input type="hidden" name="patient" value="<?php echo $patient ?>" /> 
                <input type='hidden' name='form_refresh' id='form_refresh' value='' /></form>

            <script type="text/javascript">
    <?php
    if ($alertmsg) {
        echo " alert('$alertmsg');\n";
    }
    ?>
            </script>

        </body>

        <!-- stuff for the popup calendar -->
        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
        <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
        <script type="text/javascript">

            Calendar.setup({
                inputField: "form_from_date"
                , ifFormat: "%Y-%m-%d"
                , button: "img_from_date"
                , onUpdate: function () {
                    var toDate = document.getElementById('form_to_date').value,
                            fromDate = document.getElementById('form_from_date').value;

                    if (toDate) {
                        var toDate = new Date(toDate),
                                fromDate = new Date(fromDate);

                        if (fromDate > toDate) {
                            jAlert('From date cannot be later than To date');
                            document.getElementById('form_from_date').value = '';
                        }
                    }
                }
            });

            Calendar.setup({
                inputField: "form_to_date"
                , ifFormat: "%Y-%m-%d"
                , button: "img_to_date"
                , onUpdate: function () {
                    var toDate = document.getElementById('form_to_date').value,
                            fromDate = document.getElementById('form_from_date').value;

                    if (fromDate) {
                        var toDate = new Date(toDate),
                                fromDate = new Date(fromDate);

                        if (fromDate > toDate) {
                            jAlert('From date cannot be later than To date.');
                            document.getElementById('form_to_date').value = '';
                        }
                    }
                }
            });
        </script>

    </html>
    <?php
}
?>