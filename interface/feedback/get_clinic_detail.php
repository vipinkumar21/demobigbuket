<?php
 // Copyright (C) 2006-2012 Rod Roark <rod@sunsetsystems.com>
 //
 // This program is free software; you can redistribute it and/or
 // modify it under the terms of the GNU General Public License
 // as published by the Free Software Foundation; either version 2
 // of the License, or (at your option) any later version.

 // This report lists patients that were seen within a given date
 // range, or all patients if no date range is entered.
require_once("../globals.php");
require_once("$srcdir/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once("$srcdir/options.inc.php");
$postData = json_decode($_POST);
//print_r($_REQUEST);
$query = "SELECT id, name FROM facility WHERE name LIKE '%".$_REQUEST['q']."%' limit 0, 20";
$res = sqlStatement($query);
$clinicData = array();
while($row = sqlFetchArray($res)){
    $tempData = array();
    $tempData['value'] = $row['id'];
    $tempData['label'] = $row['name'];
    $clinicData[] =     $tempData;

}
echo json_encode($clinicData);
?>