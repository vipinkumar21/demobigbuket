<?php
require_once("../../globals.php");
function q1($ym,$m,$clinicName, $feedbackType)
{
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    $select1 = "SELECT COUNT(fcr.fcr_q1r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q1r BETWEEN 9 AND 10 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export1 = mysql_query($select1) or die("Sql error : " . mysql_error());
    $resExp1 = mysql_fetch_array($export1);
    $feedback_nine_ten = $resExp1['count'];

    $select2 = "SELECT COUNT(fcr.fcr_q1r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q1r BETWEEN 7 AND 8 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export2 = mysql_query($select2) or die("Sql error : " . mysql_error());
    $resExp2 = mysql_fetch_array($export2);
    $feedback_seven_eight = $resExp2['count'];

    $select3 = "SELECT COUNT(fcr.fcr_q1r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q1r BETWEEN 0 AND 6 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export3 = mysql_query($select3) or die("Sql error : " . mysql_error());
    $resExp3 = mysql_fetch_array($export3);
    $feedback_zero_six = $resExp3['count'];

    $totalFeedbackByInd = $feedback_nine_ten + $feedback_seven_eight + $feedback_zero_six;

    $innerArray=array();
    $innerArray[3]=$totalFeedbackByInd;
    $innerArray[4]=$feedback_nine_ten;
    $innerArray[6]=$feedback_seven_eight;
    $innerArray[7]=$feedback_zero_six;

    global $outArrayQ1;
    array_push($outArrayQ1,$innerArray);
}

function q2($ym,$m,$clinicName, $feedbackType)
{
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    $select1 = "SELECT COUNT(fcr.fcr_q2r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q2r BETWEEN 9 AND 10 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export1 = mysql_query($select1) or die("Sql error : " . mysql_error());
    $resExp1 = mysql_fetch_array($export1);
    $feedback_nine_ten = $resExp1['count'];

    $select2 = "SELECT COUNT(fcr.fcr_q2r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q2r BETWEEN 7 AND 8 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export2 = mysql_query($select2) or die("Sql error : " . mysql_error());
    $resExp2 = mysql_fetch_array($export2);
    $feedback_seven_eight = $resExp2['count'];

    $select3 = "SELECT COUNT(fcr.fcr_q2r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q2r BETWEEN 0 AND 6 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export3 = mysql_query($select3) or die("Sql error : " . mysql_error());
    $resExp3 = mysql_fetch_array($export3);
    $feedback_zero_six = $resExp3['count'];

    $totalFeedbackByInd = $feedback_nine_ten + $feedback_seven_eight + $feedback_zero_six;

    $innerArray=array();
    $innerArray[11]=$totalFeedbackByInd;
    $innerArray[12]=$feedback_nine_ten;
    $innerArray[14]=$feedback_seven_eight;
    $innerArray[15]=$feedback_zero_six;

    global $outArrayQ2;
    array_push($outArrayQ2,$innerArray);
}

function q3($ym,$m,$clinicName, $feedbackType)
{
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    $select1 = "SELECT COUNT(fcr.fcr_q3r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q3r BETWEEN 9 AND 10 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export1 = mysql_query($select1) or die("Sql error : " . mysql_error());
    $resExp1 = mysql_fetch_array($export1);
    $feedback_nine_ten = $resExp1['count'];

    $select2 = "SELECT COUNT(fcr.fcr_q3r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q3r BETWEEN 7 AND 8 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export2 = mysql_query($select2) or die("Sql error : " . mysql_error());
    $resExp2 = mysql_fetch_array($export2);
    $feedback_seven_eight = $resExp2['count'];

    $select3 = "SELECT COUNT(fcr.fcr_q3r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q3r BETWEEN 0 AND 6 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export3 = mysql_query($select3) or die("Sql error : " . mysql_error());
    $resExp3 = mysql_fetch_array($export3);
    $feedback_zero_six = $resExp3['count'];

    $totalFeedbackByInd = $feedback_nine_ten + $feedback_seven_eight + $feedback_zero_six;

    $innerArray=array();

    $innerArray[19]=$totalFeedbackByInd;
    $innerArray[20]=$feedback_nine_ten;
    $innerArray[22]=$feedback_seven_eight;
    $innerArray[23]=$feedback_zero_six;
        
    global $outArrayQ3;
    array_push($outArrayQ3,$innerArray);
}

function npsYtd($clinicId, $yearMonth, $feedbackType){
    global $recArray;
    global $outArrayQ1;
    global $outArrayQ2;
    global $outArrayQ3;
    
    $recArray=array();
    $outArrayQ1=array();
    $outArrayQ2=array();
    $outArrayQ3=array();

    $clinicAll=$clinicId;
    $dateComp=$yearMonth;
    $SelectedYear=$yearMonth;

    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    if($clinicAll!="allClinics"){
            $clinicDistinct=sqlStatement("SELECT DISTINCT fcr.fcr_pid FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id  where $feedbackTypeCondition fcr.fcr_pid='$clinicId' and fcr.fcr_date LIKE '$dateComp%'");
    }
    if($clinicAll=="allClinics"){
            $clinicDistinct=sqlStatement("SELECT DISTINCT fcr.fcr_pid FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id  where $feedbackTypeCondition fcr.fcr_date LIKE '$dateComp%'");
    }

    while($noOfClinic = sqlFetchArray($clinicDistinct)) {
        $clinicName=$noOfClinic['fcr_pid'];

	for($i=1;$i<=12;$i++)
	{
            $ym = $m = ''; 
            switch ($i) {
                case 1:
                        $ym="$SelectedYear-01";
                        $m="D";
                        break;
                case 2:
                        $ym="$SelectedYear-02";
                        $m="E";
                        break;
                case 3:
                        $ym="$SelectedYear-03";
                        $m="F";
                        break;
                case 4:
                        $ym="$SelectedYear-04";
                        $m="G";
                        break;
                case 5:
                        $ym="$SelectedYear-05";
                        $m="H";
                        break;
                case 6:
                        $ym="$SelectedYear-06";
                        $m="I";
                        break;
                case 7:
                        $ym="$SelectedYear-07";
                        $m="J";
                        break;
                case 8:
                        $ym="$SelectedYear-08";
                        $m="K";
                        break;
                case 9:
                        $ym="$SelectedYear-09";
                        $m="L";
                        break;
                case 10:
                        $ym="$SelectedYear-10";
                        $m="M";
                        break;
                case 11:
                        $ym="$SelectedYear-11";
                        $m="N";
                        break;
                case 12:
                        $ym="$SelectedYear-12";
                        $m="O";
                        break;
            }
            
            q1($ym,$m,$clinicName, $feedbackType);
            q2($ym,$m,$clinicName, $feedbackType);
            q3($ym,$m,$clinicName, $feedbackType);
	}
        $singlePatientFeedbackArray=array();
        //$singlePatientFeedbackArray[$clinicName];
	$singlePatientFeedbackArray[$clinicName][0]=$outArrayQ1;
	$singlePatientFeedbackArray[$clinicName][1]=$outArrayQ2;
	$singlePatientFeedbackArray[$clinicName][2]=$outArrayQ3;
        array_push($recArray,$singlePatientFeedbackArray);
}
return $recArray;
}
?>
