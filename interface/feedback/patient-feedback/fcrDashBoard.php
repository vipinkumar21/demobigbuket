<?php
require_once("../../globals.php");
require_once("../../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("../../inventory/drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");


$year = $_POST['year'];
$month = $_POST['month'];
$facility = $_POST['formClinic'];
$feedbackType = $_POST['feedbackType'];

// In  case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
    $msg = "export clicked!";
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('Feedback Clinic Report', 'e'); ?></title>

            <script type="text/javascript" src="../../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../../library/textformat.js"></script>
            <script type="text/javascript" src="../../../library/dialog.js"></script>
            <script type="text/javascript" src="../../../library/js/jquery.1.3.2.js"></script>

            <script type="text/javascript">
                function refreshme() {
                    document.forms[0].submit();
                }
            </script>
        </head>

        <body class="body_top">
            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
            <h3 class='emrh3'><?php xl('Feedback Clinic Report', 'e'); ?></h3>
            <div id="report_parameters_daterange"><?php //echo date("d F Y", strtotime($from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($to_date));   ?>
            </div>

            <form method='post' name='theform' id='theform' action='fcrDashBoard.php' class='emrform'>
                <div class='searchReport'>
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table width='100%'>
                        <tr>
                            <td width='84%'>
                            <!-- <td width='650px'> -->
                                <div style='float: left'>
                                    <table class='text'>
                                        <tr>
                                            <td class='label newlabel'>Select Clinic:</td>
                                            <td>

                                                <?php dropdown_facility(strip_escape_custom($facility), 'formClinic', false, true); ?>
                                            </td>
                                            <td class='label newlabel'>Year:</td>
                                            <td valign='top'>
                                                <?php
                                                echo "<select name='year' class='emrinput'>";
                                                $curYear = date("Y");

                                                $sy = 2014;
                                                if(!empty($_POST['year'])) {
                                                    $selectYear = $_POST['year'];
                                                } else {
                                                    $selectYear = date("Y");
                                                }
                                                for ($yc = $sy; $yc <= $curYear; $yc++) {
                                                    if ($yc == $selectYear) {
                                                        echo "<option value='$yc' selected>$yc</option>";
                                                    } else {
                                                        echo "<option value='$yc'>$yc</option>";
                                                    }
                                                }
                                                echo "</select>";
                                                ?>
                                            </td>
                                            <td class='label newlabel'>Month:</td>
                                            <td valign='top'>
                                                <?php $monthsArray = array('01' => "Jan", '02' => "Feb", '03' => "March", '04' => "April", '05' => "May", '06' => "June", '07' => "July", '08' => "Aug", '09' => "Sept", '10' => "Oct", '11' => "Nov", '12' => "Dec"); ?>
                                                <select class='emrinput' name='month'>
                                                    <?php
                                                    if(!empty($_POST['month'])) {
                                                        $selectMonth = $_POST['month'];
                                                    } else {
                                                        $selectMonth = date("m");
                                                    }
                                                    foreach ($monthsArray as $km => $vm) {
                                                        if ($km == $selectMonth) {
                                                            echo "<option value='$km' selected>$vm</option>";
                                                        } else {
                                                            echo "<option value='$km'>$vm</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td class='label newlabel'>Feedback Type:</td>
                                            <td valign='top'>
                                                
                                                <select class='emrinput' name='feedbackType'>
                                                    <option value="">Select</option>
                                                    <?php
                                                        $ftQuery = "SELECT ft_id, ft_name FROM feedback_type WHERE ft_status = 1";
                                                        $ftRes = sqlStatement($ftQuery ,null, $GLOBALS['adodb']['dbreadonly']);
                                                        while ($ftRow = sqlFetchArray($ftRes)) {
                                                           ?>
                                                               <option value='<?php echo $ftRow['ft_id'];?>' <?php if($feedbackType == $ftRow['ft_id']) {echo "selected";}?> ><?php echo $ftRow['ft_name'];?></option> 
                                                           <?php 
                                                        }
                                                    ?>
                                                </select>
                                            </td>

                                        </tr>

                                    </table>
                                </div>
                            </td>
                            <td width='16%' align='left' valign='middle' height="100%">
                                <table style='border-left: 1px solid; width: 100%; height: 100%'>
                                    <tr>
                                        <td>
                                            <div style='margin-left: 15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewConsumption");
                                                            $("#theform").submit();'>
                                                    <span> <?php xl('Submit', 'e'); ?> </span> </a> 
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
                <!-- end of search parameters --> <?php
            } // end not form_csvexport
            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
                    // CSV goes headers:
                } else {
                    ?>
                    <div id="report_results" >
                        <table class="bor-Collapse">
                            <tr class='head'>

                            <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        if ($year != "" && $month != "" && $facility == "") {
                                            // all record of all clinic for current month
                                            $yearMonth = $year . "-" . $month;
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            // following value will be change according to report
                                            $rcsl_name_type = "feedback_clinic_report"; // changable
                                            $selectedMonth = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
                                            $months = $selectedMonth[(int)$month];
                                            $rcsl_requested_filter = addslashes(serialize(array("yearMonth" => $yearMonth, "facility" => "allClinics", "feedbackType" => $feedbackType, "noDateRange" => "For $months $year"))); // changable
                                            $rcsl_report_description = "Feedback clinic report for $yearMonth"; // changable
                                            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
                                            if ($msgForReportLog) {
                                                $msgForReportLog;
                                            } else {
                                                $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
                                            }
                                            ##########Cron Request####################section only for all FACILITY#########################
                                            echo "<div align='center'>$msgForReportLog</div>";
                                            $searchParam .= ' Clinic = All | Year = ' . $year . ' | Month = ' . $month . ' | Feedback Type = ' . $feedbackType . ' |';
                                            debugADOReports($sql, '', 'Report Feedback Clinic', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
                                        }
                                        if ($year != "" && $month != "" && $facility != "") {
                                            $queryClinic = "select name from facility where id='$facility'";
                                            $resClinic = sqlStatement($queryClinic);
                                            $rowClinic = sqlFetchArray($resClinic);
                                            $clinicName = $rowClinic['name'];

                                            // all record of all clinic for current month
                                            $yearMonth = $year . "-" . $month;
                                            $clinicNameF = strtoupper($clinicName);
                                            echo "<p style='text-align:center'>Would you like <b><u> $clinicNameF </u></b> Feedback report ($yearMonth)</p>";
                                            echo "<p style='text-align: center'><a href='fcrXls.php?yearMonth=$yearMonth&clincName=$clinicName&clinicId=$facility&feedbackType=$feedbackType'>Click Here!</a></p>";

                                            $searchParam .= ' Clinic = ' . $rowClinic['name'] . ' | Year = ' . $year . ' | Month = ' . $month . ' | Feedback Type = ' . $feedbackType . ' |';
                                            debugADOReports($queryClinic, '', 'Report Feedback Clinic', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 1); // Report Auditing Section
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end of search results --> <?php
                            }
                        } else {
                                    ?>
                <div class='text' style='padding:5px 0px 0px 5px;'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>

            <?php } ?> 
            <?php
            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvFeedbackReferral') {
                ?>
                <input type='hidden' name='form_refresh' id='form_refresh' value='' />
            </form>

        </body>

    </html>
    <?php
}
?>