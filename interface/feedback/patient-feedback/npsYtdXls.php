<?php
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
$yearMonth=$_REQUEST['yearMonth'];
$clincName=$_REQUEST['clincName'];
$clinicId=$_REQUEST['clinicId'];
$feedbackType = $_REQUEST['feedbackType'];
require_once("npsYtd.php");
$recArray=npsYtd($clinicId,$yearMonth,$feedbackType);

// Create new PHPExcel objects
$objPHPExcel = new PHPExcel();

foreach($recArray as $k=>$v)
{
	foreach($recArray[$k] as $clinicName=>$clinicArrayValue)
	{
                $selectClinicName = "SELECT name FROM facility where id='$clinicName'";
		$selectClinicNameQuery = mysql_query ( $selectClinicName ) or die ( "Sql error : " . mysql_error( ) );
		$facilityName = mysql_fetch_assoc ( $selectClinicNameQuery );
		$facilityNameS=$facilityName['name'];
	//return click name	"$clinicName"
		$objPHPExcel->setActiveSheetIndex($k);
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle($facilityNameS);
		// Create a new worksheet, after the default sheet
		$objPHPExcel->createSheet();
		
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16)->setBold(true);
        	$objPHPExcel->getActiveSheet()->getStyle('B2:P2')->getFont()->setBold(true);
        	
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Customer Feedback Report');
		$objPHPExcel->getActiveSheet()->setCellValue('B2', 'Questions');
		$objPHPExcel->getActiveSheet()->setCellValue('D2', 'Jan');
		$objPHPExcel->getActiveSheet()->setCellValue('E2', 'Feb');
		$objPHPExcel->getActiveSheet()->setCellValue('F2', 'Mar');
		$objPHPExcel->getActiveSheet()->setCellValue('G2', 'Apr');
		$objPHPExcel->getActiveSheet()->setCellValue('H2', 'May');
		$objPHPExcel->getActiveSheet()->setCellValue('I2', 'June');
		$objPHPExcel->getActiveSheet()->setCellValue('J2', 'July');
		$objPHPExcel->getActiveSheet()->setCellValue('K2', 'Aug');
		$objPHPExcel->getActiveSheet()->setCellValue('L2', 'Sept');
		$objPHPExcel->getActiveSheet()->setCellValue('M2', 'Oct');
		$objPHPExcel->getActiveSheet()->setCellValue('N2', 'Nov');
		$objPHPExcel->getActiveSheet()->setCellValue('O2', 'Dec');
		$objPHPExcel->getActiveSheet()->setCellValue('P2', 'YTD');
		
		$objPHPExcel->getActiveSheet()->getStyle('D2:P2')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$objPHPExcel->getActiveSheet()->getStyle('C3:P3')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C8:P8')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C3:C8')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('P2:P8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$objPHPExcel->getActiveSheet()->getStyle('C11:P11')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C16:P16')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C11:C16')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('P11:P16')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$objPHPExcel->getActiveSheet()->getStyle('C19:P19')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C24:P24')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C19:C24')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('P19:P24')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


		//FOR QUESTION 
		$objPHPExcel->getActiveSheet()->getStyle('B3:B6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('B3:B6')->getFill()->getStartColor()->setARGB('FFE4EAF4');
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'How likely are you to recommend Star Dental to your friends / family');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
		$objPHPExcel->setActiveSheetIndex($k)->mergeCells('B3:B6');
                $objPHPExcel->getActiveSheet()->getStyle('B3:B6')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setWrapText(true);
		//FOR QUESTION 2
		$objPHPExcel->getActiveSheet()->getStyle('B11:B14')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('B11:B14')->getFill()->getStartColor()->setARGB('FFE4EAF4');
		$objPHPExcel->getActiveSheet()->setCellValue('B11', 'How satisfied are you with your interaction with the Dentist?');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
		$objPHPExcel->setActiveSheetIndex($k)->mergeCells('B11:B14');
                $objPHPExcel->getActiveSheet()->getStyle('B11:B14')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B11')->getAlignment()->setWrapText(true);
		//FOR QUESTION 3
		$objPHPExcel->getActiveSheet()->getStyle('B19:B22')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('B19:B22')->getFill()->getStartColor()->setARGB('FFE4EAF4');
		$objPHPExcel->getActiveSheet()->setCellValue('B19', 'Please rate the overall Quality at Star Dental? [Front office welcome, Clinic hygiene, Ambience etc.]');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
		$objPHPExcel->setActiveSheetIndex($k)->mergeCells('B19:B22');
                $objPHPExcel->getActiveSheet()->getStyle('B19:B22')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B19')->getAlignment()->setWrapText(true);
		
		
		
		foreach($recArray[$k][$clinicName] as $quesArrayIndex=>$quesArrayValue)
		{
			foreach($recArray[$k][$clinicName][$quesArrayIndex] as $monthIndex=>$monthArrayFeedRes)
			{
				if($monthIndex==0) {$colNm="D";}
				if($monthIndex==1) {$colNm="E";}
				if($monthIndex==2) {$colNm="F";}
				if($monthIndex==3) {$colNm="G";}
				if($monthIndex==4) {$colNm="H";}
				if($monthIndex==5) {$colNm="I";}
				if($monthIndex==6) {$colNm="J";}
				if($monthIndex==7) {$colNm="K";}
				if($monthIndex==8) {$colNm="L";}
				if($monthIndex==9) {$colNm="M";}
				if($monthIndex==10) {$colNm="N";}
				if($monthIndex==11) {$colNm="O";}
					
					
				
				foreach($monthArrayFeedRes as $a=>$b)
				{ 
				//set valye month wise	consists like "D"=>array(3=>42,4=>30,6=>8,7=>4)
					$objPHPExcel->getActiveSheet()->setCellValue($colNm.$a,$b);
				}	
			}
		//QUESTION 1ST INFO
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getStyle('C3:C8')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Total Respondents ');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Promoter(9-10) ');
		$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Percentage Of Total');
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Passive(7-8)');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Detractor(0-6) ');		
		$objPHPExcel->getActiveSheet()->setCellValue('C8', 'Percentage Of Total ');		
                    
		$objPHPExcel->getActiveSheet()->setCellValue('P3', '=SUM(D3:O3)');		
		$objPHPExcel->getActiveSheet()->setCellValue('P4', '=SUM(D4:O4)');
		$objPHPExcel->getActiveSheet()->setCellValue('P6', '=SUM(D6:O6)');
		$objPHPExcel->getActiveSheet()->setCellValue('P7', '=SUM(D7:O7)');
		
		//QUESTION 2ND INFO
                $objPHPExcel->getActiveSheet()->getStyle('C11:C16')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C11', 'Total Respondents ');
		$objPHPExcel->getActiveSheet()->setCellValue('C12', 'Promoter(9-10) ');
		$objPHPExcel->getActiveSheet()->setCellValue('C13', 'Percentage Of Total');
		$objPHPExcel->getActiveSheet()->setCellValue('C14', 'Passive(7-8)');
		$objPHPExcel->getActiveSheet()->setCellValue('C15', 'Detractor(0-6) ');		
		$objPHPExcel->getActiveSheet()->setCellValue('C16', 'Percentage Of Total ');		

		$objPHPExcel->getActiveSheet()->setCellValue('P11', '=SUM(D11:O11)');		
		$objPHPExcel->getActiveSheet()->setCellValue('P12', '=SUM(D12:O12)');
		$objPHPExcel->getActiveSheet()->setCellValue('P14', '=SUM(D14:O14)');
		$objPHPExcel->getActiveSheet()->setCellValue('P15', '=SUM(D15:O15)');

		//QUESTION 2ND INFO
                $objPHPExcel->getActiveSheet()->getStyle('C19:C24')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C19', 'Total Respondents ');
		$objPHPExcel->getActiveSheet()->setCellValue('C20', 'Promoter(9-10) ');
		$objPHPExcel->getActiveSheet()->setCellValue('C21', 'Percentage Of Total');
		$objPHPExcel->getActiveSheet()->setCellValue('C22', 'Passive(7-8)');
		$objPHPExcel->getActiveSheet()->setCellValue('C23', 'Detractor(0-6) ');		
		$objPHPExcel->getActiveSheet()->setCellValue('C24', 'Percentage Of Total ');		

		$objPHPExcel->getActiveSheet()->setCellValue('P19', '=SUM(D19:O19)');		
		$objPHPExcel->getActiveSheet()->setCellValue('P20', '=SUM(D20:O20)');
		$objPHPExcel->getActiveSheet()->setCellValue('P22', '=SUM(D22:O22)');
		$objPHPExcel->getActiveSheet()->setCellValue('P23', '=SUM(D23:O23)');

		//Percentage Of Total PROMOTOR FOR 1ST QUES
		$objPHPExcel->getActiveSheet()->setCellValue('D5', '=(D4/D3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('E5', '=(E4/E3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('F5', '=(F4/F3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('G5', '=(G4/G3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('H5', '=(H4/H3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('I5', '=(I4/I3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('J5', '=(J4/J3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('K5', '=(K4/K3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('L5', '=(L4/L3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('M5', '=(M4/M3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('N5', '=(N4/N3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('O5', '=(O4/O3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('P5', '=(P4/P3)*100');
		//Percentage Of Total Detractor FOR 1ST QUES
		$objPHPExcel->getActiveSheet()->setCellValue('D8', '=(D7/D3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('E8', '=(E7/E3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('F8', '=(F7/F3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('G8', '=(G7/G3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('H8', '=(H7/H3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('I8', '=(I7/I3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('J8', '=(J7/J3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('K8', '=(K7/K3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('L8', '=(L7/L3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('M8', '=(M7/M3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('N8', '=(N7/N3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('O8', '=(O7/O3)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('P8', '=(P7/P3)*100');
		//Percentage Of Total PROMOTOR FOR 2ND QUES
		$objPHPExcel->getActiveSheet()->setCellValue('D13', '=(D12/D11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('E13', '=(E12/E11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('F13', '=(F12/F11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('G13', '=(G12/G11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('H13', '=(H12/H11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('I13', '=(I12/I11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('J13', '=(J12/J11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('K13', '=(K12/K11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('L13', '=(L12/L11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('M13', '=(M12/M11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('N13', '=(N12/N11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('O13', '=(O12/O11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('P13', '=(P12/P11)*100');
		//Percentage Of Total Detractor FOR 2ND QUES
		$objPHPExcel->getActiveSheet()->setCellValue('D16', '=(D15/D11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('E16', '=(E15/E11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('F16', '=(F15/F11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('G16', '=(G15/G11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('H16', '=(H15/H11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('I16', '=(I15/I11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('J16', '=(J15/J11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('K16', '=(K15/K11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('L16', '=(L15/L11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('M16', '=(M15/M11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('N16', '=(N15/N11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('O16', '=(O15/O11)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('P16', '=(P15/P11)*100');
		//Percentage Of Total PROMOTOR FOR 3RD QUES
		$objPHPExcel->getActiveSheet()->setCellValue('D21', '=(D20/D19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('E21', '=(E20/E19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('F21', '=(F20/F19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('G21', '=(G20/G19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('H21', '=(H20/H19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('I21', '=(I20/I19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('J21', '=(J20/J19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('K21', '=(K20/K19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('L21', '=(L20/L19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('M21', '=(M20/M19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('N21', '=(N20/N19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('O21', '=(O20/O19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('P21', '=(P20/P19)*100');
		//Percentage Of Total Detractor FOR 3RD QUES
		$objPHPExcel->getActiveSheet()->setCellValue('D24', '=(D23/D19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('E24', '=(E23/E19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('F24', '=(F23/F19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('G24', '=(G23/G19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('H24', '=(H23/H19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('I24', '=(I23/I19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('J24', '=(J23/J19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('K24', '=(K23/K19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('L24', '=(L23/L19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('M24', '=(M23/M19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('N24', '=(N23/N19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('O24', '=(O23/O19)*100');
		$objPHPExcel->getActiveSheet()->setCellValue('P24', '=(P23/P19)*100');
		
		//--------------------------------------------------NPS----------------------------------------------------
		//1ST nps
                $objPHPExcel->getActiveSheet()->getStyle('C9')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C9', 'NPS  ');
		$objPHPExcel->getActiveSheet()->setCellValue('D9', '=D5-D8');
		$objPHPExcel->getActiveSheet()->setCellValue('E9', '=E5-E8');
		$objPHPExcel->getActiveSheet()->setCellValue('F9', '=F5-F8');
		$objPHPExcel->getActiveSheet()->setCellValue('G9', '=G5-G8');
		$objPHPExcel->getActiveSheet()->setCellValue('H9', '=H5-H8');
		$objPHPExcel->getActiveSheet()->setCellValue('I9', '=I5-I8');
		$objPHPExcel->getActiveSheet()->setCellValue('J9', '=J5-J8');
		$objPHPExcel->getActiveSheet()->setCellValue('K9', '=K5-K8');
		$objPHPExcel->getActiveSheet()->setCellValue('L9', '=L5-L8');
		$objPHPExcel->getActiveSheet()->setCellValue('M9', '=M5-M8');
		$objPHPExcel->getActiveSheet()->setCellValue('N9', '=N5-N8');
		$objPHPExcel->getActiveSheet()->setCellValue('O9', '=O5-O8');
		$objPHPExcel->getActiveSheet()->setCellValue('P9', '=P5-P8');

		//2ND nps
                $objPHPExcel->getActiveSheet()->getStyle('C17')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C17', 'NPS  ');
		$objPHPExcel->getActiveSheet()->setCellValue('D17', '=D13-D16');
		$objPHPExcel->getActiveSheet()->setCellValue('E17', '=E13-E16');
		$objPHPExcel->getActiveSheet()->setCellValue('F17', '=F13-F16');
		$objPHPExcel->getActiveSheet()->setCellValue('G17', '=G13-G16');
		$objPHPExcel->getActiveSheet()->setCellValue('H17', '=H13-H16');
		$objPHPExcel->getActiveSheet()->setCellValue('I17', '=I13-I16');
		$objPHPExcel->getActiveSheet()->setCellValue('J17', '=J13-J16');
		$objPHPExcel->getActiveSheet()->setCellValue('K17', '=K13-K16');
		$objPHPExcel->getActiveSheet()->setCellValue('L17', '=L13-L16');
		$objPHPExcel->getActiveSheet()->setCellValue('M17', '=M13-M16');
		$objPHPExcel->getActiveSheet()->setCellValue('N17', '=N13-N16');
		$objPHPExcel->getActiveSheet()->setCellValue('O17', '=O13-O16');
		$objPHPExcel->getActiveSheet()->setCellValue('P17', '=P13-P16');

		//3RD nps
                $objPHPExcel->getActiveSheet()->getStyle('C25')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C25', 'NPS  ');
		$objPHPExcel->getActiveSheet()->setCellValue('D25', '=D21-D24');
		$objPHPExcel->getActiveSheet()->setCellValue('E25', '=E21-E24');
		$objPHPExcel->getActiveSheet()->setCellValue('F25', '=F21-F24');
		$objPHPExcel->getActiveSheet()->setCellValue('G25', '=G21-G24');
		$objPHPExcel->getActiveSheet()->setCellValue('H25', '=H21-H24');
		$objPHPExcel->getActiveSheet()->setCellValue('I25', '=I21-I24');
		$objPHPExcel->getActiveSheet()->setCellValue('J25', '=J21-J24');
		$objPHPExcel->getActiveSheet()->setCellValue('K25', '=K21-K24');
		$objPHPExcel->getActiveSheet()->setCellValue('L25', '=L21-L24');
		$objPHPExcel->getActiveSheet()->setCellValue('M25', '=M21-M24');
		$objPHPExcel->getActiveSheet()->setCellValue('N25', '=N21-N24');
		$objPHPExcel->getActiveSheet()->setCellValue('O25', '=O21-O24');
		$objPHPExcel->getActiveSheet()->setCellValue('P25', '=P21-P24');


		}	
	}
}
$xlsNm="NPS-".$yearMonth."-YTD.xls";
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=$xlsNm");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>
