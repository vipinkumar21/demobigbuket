<?php
require_once("../../globals.php");
require_once("../../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once("../../inventory/drugs.inc.php");
require_once ("$audting_webroot/auditlog.php");

$month = $_POST['month'];
$facility = $_POST['formClinic'];
$feedbackType = $_POST['feedbackType'];
$searchParam = '';
// In  case of CSV export only, a download will be forced.
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
    $msg = "export clicked!";
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>
            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
            <title><?php xl('NPS YTD - Feedback Report', 'e'); ?></title>

            <script type="text/javascript" src="../../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../../library/textformat.js"></script>
            <script type="text/javascript" src="../../../library/dialog.js"></script>
            <script type="text/javascript" src="../../../library/js/jquery.1.3.2.js"></script>

            <script type="text/javascript">
                function refreshme() {
                    document.forms[0].submit();
                }
            </script>
        </head>

        <body class="body_top">
            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
            <h3 class='emrh3'><?php xl('NPS YTD', 'e'); ?> - <?php xl('Feedback Report', 'e'); ?></h3>
            <div id="report_parameters_daterange"><?php //echo date("d F Y", strtotime($from_date)) ." &nbsp; to &nbsp; ". date("d F Y", strtotime($to_date));   ?>
            </div>

            <form method='post' name='theform' id='theform' action='npsYtdDashBoard.php' class='emrform topnopad'>
                <div id="report_parameters" class='searchReport'>
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>
                    <table>
                        <tr>
                            <td width='84%'>
                            <!-- <td width='650px'> -->
                                <div style='float: left'>
                                    <table class='text'>
                                        <tr>
                                            <td class='label'valign="top">Select Clinic:</td>
                                            <td>
                                                <?php dropdown_facility(strip_escape_custom($facility), 'formClinic', false, true); ?>
                                            </td>
                                            <td class='label' valign='top'>Year:</td>
                                            <td valign='top'>
                                                <?php
                                                echo "<select name='year' class='emrinput'>";
                                                $curYear = date("Y");

                                                $sy = 2014;
                                                if(!empty($_POST['year'])) {
                                                    $selectYear = $_POST['year'];
                                                } else {
                                                    $selectYear = date("Y");
                                                }
                                                for ($yc = $sy; $yc <= $curYear; $yc++) {
                                                    if ($yc == $selectYear) {
                                                        echo "<option value='$yc' selected>$yc</option>";
                                                    } else {
                                                        echo "<option value='$yc'>$yc</option>";
                                                    }
                                                }
                                                echo "</select>";
                                                ?>
                                            </td>
                                            <td class='label newlabel'>Feedback Type:</td>
                                            <td valign='top'>
                                                <select class='emrinput' name='feedbackType'>
                                                    <option value="">Select</option>
                                                    <?php
                                                        $ftQuery = "SELECT ft_id, ft_name FROM feedback_type WHERE ft_status = 1";
                                                        $ftRes = sqlStatement($ftQuery ,null, $GLOBALS['adodb']['dbreadonly']);
                                                        while ($ftRow = sqlFetchArray($ftRes)) {
                                                           ?>
                                                               <option value='<?php echo $ftRow['ft_id'];?>' <?php if($feedbackType == $ftRow['ft_id']) {echo "selected";}?> ><?php echo $ftRow['ft_name'];?></option> 
                                                           <?php 
                                                        }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </td>
                            <td width='16%' align='left' valign='middle' height="100%">
                                <table style='border-left: 1px solid; width: 100%; height: 100%'>
                                    <tr>
                                        <td>
                                            <div style='margin-left: 15px'>
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewConsumption");
                                                            $("#theform").submit();'>
                                                    <span> <?php xl('Submit', 'e'); ?> </span> </a> 
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
                <!-- end of search parameters --> <?php
            } // end not form_csvexport
            if ($_POST['form_refresh'] || $_POST['form_csvexport']) {
                if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvFeedbackReferral') {
                    // CSV goes headers:
                } else {
                    ?>
                    <div id="report_results">
                        <table class=emrtable>
                            <tr class='head'>

                            <tbody
                                <tr>
                                    <td>
        <?php
        if ($year != "" && $facility == "") {
            // all record of all clinic for current year
            $yearMonth = $year;
            ##########Cron Request####################section only for all FACILITY#########################
            // following value will be change according to report
            $rcsl_name_type = "npsytd_feedback_report"; // changable
            $rcsl_requested_filter = addslashes(serialize(array("yearMonth" => $yearMonth, "facility" => "allClinics", "feedbackType" => $feedbackType, "noDateRange" => "For $yearMonth"))); // changable
            $rcsl_report_description = "NPS YTD - Feedback report for $yearMonth"; // changable
            //allFacilityReports() defined with globals.php
            $msgForReportLog .=allFacilityReports($rcsl_name_type, $rcsl_requested_filter, $rcsl_report_description);
            if ($msgForReportLog) {
                $msgForReportLog;
            } else {
                $msgForReportLog = "Your request has been accepted. Report has been scheduled for delivery on your email by next day.";
            }
            ##########Cron Request####################section only for all FACILITY#########################
            echo "<div align='center'>$msgForReportLog</div>";
            $searchParam .= ' Clinic = All | Year = ' . $year . ' | Feedback Type = ' . $feedbackType . ' |';
            debugADOReports($sql, '', 'Report NPS YTD Feedback', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 2); // Cron Schedule Report Auditing Section
        }
        if ($year != "" && $facility != "") {
            $queryClinic = "select name from facility where id='$facility'";
            $resClinic = sqlStatement($queryClinic);
            $rowClinic = sqlFetchArray($resClinic);
            $clinicName = $rowClinic['name'];

            // all record of all clinic for current month
            $yearMonth = $year;
            $clinicNameF = strtoupper($clinicName);
            echo "<p style='color: black;text-align: center;font-size: 20px;'>Would you like <b><u> $clinicNameF </u></b> NPS YTD report ($yearMonth)</p>";
            echo "<p style='color: orange;text-align: center;font-size: 20px;'><a href='npsYtdXls.php?yearMonth=$yearMonth&clincName=$clinicName&clinicId=$facility&feedbackType=$feedbackType'>Click Here!</a></p>";

            $searchParam .= ' Clinic = ' . $rowClinic['name'] . ' | Year = ' . $year . ' | Feedback Type = ' . $feedbackType . ' |';
            debugADOReports($queryClinic, '', 'Report NPS YTD Feedback', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam, 1); // Report Auditing Section
        }
        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end of search results --> <?php
                            }
                        } else {
                            ?>
                <div class='text'><?php echo xl('Please input search criteria above, and click Submit to view results.', 'e'); ?>
                </div>

<?php } ?> 
            <?php
            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvFeedbackReferral') {
                ?>
                <input type='hidden' name='form_refresh' id='form_refresh' value='' />
            </form>

        </body>

    </html>
    <?php
}
?>