<?php

require_once("../../globals.php");

function q1($ym, $m, $clinicName, $feedbackType) {
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    $select1 = "SELECT COUNT(fcr.fcr_q1r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr_q1r BETWEEN 9 AND 10 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export1 = mysql_query($select1) or die("Sql error : " . mysql_error());
    $resExp1 = mysql_fetch_array($export1);
    $feedback_nine_ten = $resExp1['count'];

    $select2 = "SELECT COUNT(fcr.fcr_q1r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr_q1r BETWEEN 7 AND 8 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export2 = mysql_query($select2) or die("Sql error : " . mysql_error());
    $resExp2 = mysql_fetch_array($export2);
    $feedback_seven_eight = $resExp2['count'];

    $select3 = "SELECT COUNT(fcr.fcr_q1r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr_q1r BETWEEN 0 AND 6 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export3 = mysql_query($select3) or die("Sql error : " . mysql_error());
    $resExp3 = mysql_fetch_array($export3);
    $feedback_zero_six = $resExp3['count'];

    $totalFeedbackByInd = $feedback_nine_ten + $feedback_seven_eight + $feedback_zero_six;

    $innerArray = array();
    $innerArray[3] = $totalFeedbackByInd;
    $innerArray[4] = $feedback_nine_ten;
    $innerArray[6] = $feedback_seven_eight;
    $innerArray[7] = $feedback_zero_six;

    global $outArrayQ1;
    array_push($outArrayQ1, $innerArray);
}

function q2($ym, $m, $clinicName, $feedbackType) {
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    $select1 = "SELECT COUNT(fcr.fcr_q2r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q2r BETWEEN 9 AND 10 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export1 = mysql_query($select1) or die("Sql error : " . mysql_error());
    $resExp1 = mysql_fetch_array($export1);
    $feedback_nine_ten = $resExp1['count'];

    $select2 = "SELECT COUNT(fcr.fcr_q2r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q2r BETWEEN 7 AND 8 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export2 = mysql_query($select2) or die("Sql error : " . mysql_error());
    $resExp2 = mysql_fetch_array($export2);
    $feedback_seven_eight = $resExp2['count'];

    $select3 = "SELECT COUNT(fcr.fcr_q2r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q2r BETWEEN 0 AND 6 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export3 = mysql_query($select3) or die("Sql error : " . mysql_error());
    $resExp3 = mysql_fetch_array($export3);
    $feedback_zero_six = $resExp3['count'];

    $totalFeedbackByInd = $feedback_nine_ten + $feedback_seven_eight + $feedback_zero_six;

    $innerArray = array();
    $innerArray[11] = $totalFeedbackByInd;
    $innerArray[12] = $feedback_nine_ten;
    $innerArray[14] = $feedback_seven_eight;
    $innerArray[15] = $feedback_zero_six;
    global $outArrayQ2;
    array_push($outArrayQ2, $innerArray);
}

function q3($ym, $m, $clinicName, $feedbackType) {
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    $select1 = "SELECT COUNT(fcr.fcr_q3r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q3r BETWEEN 9 AND 10 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export1 = mysql_query($select1) or die("Sql error : " . mysql_error());
    $resExp1 = mysql_fetch_array($export1);
    $feedback_nine_ten = $resExp1['count'];

    $select2 = "SELECT COUNT(fcr.fcr_q3r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q3r BETWEEN 7 AND 8 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export2 = mysql_query($select2) or die("Sql error : " . mysql_error());
    $resExp2 = mysql_fetch_array($export2);
    $feedback_seven_eight = $resExp2['count'];

    $select3 = "SELECT COUNT(fcr.fcr_q3r) AS `count` FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id WHERE $feedbackTypeCondition fcr.fcr_q3r BETWEEN 0 AND 6 and fcr.fcr_date like '%$ym%' and fcr.fcr_pid='$clinicName'";
    $export3 = mysql_query($select3) or die("Sql error : " . mysql_error());
    $resExp3 = mysql_fetch_array($export3);
    $feedback_zero_six = $resExp3['count'];

    $totalFeedbackByInd = $feedback_nine_ten + $feedback_seven_eight + $feedback_zero_six;

    $innerArray = array();
    $innerArray[19] = $totalFeedbackByInd;
    $innerArray[20] = $feedback_nine_ten;
    $innerArray[22] = $feedback_seven_eight;
    $innerArray[23] = $feedback_zero_six;
    global $outArrayQ3;
    array_push($outArrayQ3, $innerArray);
}

// only for filters
function npsmonth($clinicId, $yearMonth, $feedbackType) {
    global $recArray;
    global $outArrayQ1;
    global $outArrayQ2;
    global $outArrayQ3;

    $recArray = array();
    $outArrayQ1 = array();
    $outArrayQ2 = array();
    $outArrayQ3 = array();

    $clinicAll = $clinicId;
    $dateComp = $yearMonth;
    $SelectedYearMonth = (explode("-", $yearMonth));
    $SelectedYear = $SelectedYearMonth[0];
    /* Changed according to the monthly report.
     * This File and the related files needs to be improve in terms of code level.
     * @Changed by: Deepak Aneja (12 June 2015)
    */
    $SelectedMonth = $SelectedYearMonth[1];
    
    $ym = $SelectedYear."-".$SelectedMonth;
    $m = "D"; // This is not using for any of the question for now. It can be used in future.
    
    $feedbackTypeCondition = "";
    if(!empty($feedbackType)) {
        $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
    }
    
    if ($clinicAll != "allClinics") {
        $clinicDistinct = sqlStatement("SELECT DISTINCT fcr.fcr_pid FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id where $feedbackTypeCondition fcr.fcr_pid='$clinicId' and fcr.fcr_date LIKE '$dateComp%'");
    }
    if ($clinicAll == "allClinics") {
        $clinicDistinct = sqlStatement("SELECT DISTINCT fcr.fcr_pid FROM feedback_clinic_report AS fcr INNER JOIN feedback_transaction AS ft ON ft.ftrans_id = fcr.fcr_ftrans_id where $feedbackTypeCondition fcr.fcr_date LIKE '$dateComp%'");
    }
    
    while ($noOfClinic = sqlFetchArray($clinicDistinct)) {
        $clinic_id = $noOfClinic['fcr_pid'];
        /*for ($i = 1; $i <= 12; $i++) {

            if ($i == 1) {
                $ym = "$SelectedYear-01";
                $m = "D";
            } else if ($i == 2) {
                $ym = "$SelectedYear-02";
                $m = "E";
            } else if ($i == 3) {
                $ym = "$SelectedYear-03";
                $m = "F";
            } else if ($i == 4) {
                $ym = "$SelectedYear-04";
                $m = "G";
            } else if ($i == 5) {
                $ym = "$SelectedYear-05";
                $m = "H";
            } else if ($i == 6) {
                $ym = "$SelectedYear-06";
                $m = "I";
            } else if ($i == 7) {
                $ym = "$SelectedYear-07";
                $m = "J";
            } else if ($i == 8) {
                $ym = "$SelectedYear-08";
                $m = "K";
            } else if ($i == 9) {
                $ym = "$SelectedYear-09";
                $m = "L";
            } else if ($i == 10) {
                $ym = "$SelectedYear-10";
                $m = "M";
            } else if ($i == 11) {
                $ym = "$SelectedYear-11";
                $m = "N";
            } else {
                $ym = "$SelectedYear-12";
                $m = "O";
            }


            q1($ym, $m, $clinic_id);
            q2($ym, $m, $clinic_id);
            q3($ym, $m, $clinic_id);
        }*/
        q1($ym, $m, $clinic_id, $feedbackType);
        q2($ym, $m, $clinic_id, $feedbackType);
        q3($ym, $m, $clinic_id, $feedbackType);
        
        $singlePatientFeedbackArray = array();
        $singlePatientFeedbackArray[$clinic_id];
        $singlePatientFeedbackArray[$clinic_id][0] = $outArrayQ1;
        $singlePatientFeedbackArray[$clinic_id][1] = $outArrayQ2;
        $singlePatientFeedbackArray[$clinic_id][2] = $outArrayQ3;

        array_push($recArray, $singlePatientFeedbackArray);
    }
    return $recArray;
}

?>
