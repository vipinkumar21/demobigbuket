<?php

require_once("../../globals.php");
if (!acl_check('admin', 'users'))
    exit();
$sql = "SELECT 
  ft.ftrans_id,
  ft.ftrans_pname,
  ft.ftrans_createddate,
  ft.ftrans_createdby,
  ft.ftrans_feedback_source,
  ft.pubpid,
  ft.ftrans_auto_sr_no,
  lo.ft_name AS title,
  fc.name ,
  CASE WHEN ft.ftrans_feedback_source = 0 THEN  u.`nickname` ELSE ft.ftrans_createdby END AS createdBy,
  ft.ftrans_file_url, ft.ftrans_feedback_date
FROM
  feedback_transaction AS ft 
  INNER JOIN facility AS fc 
    ON fc.id = ft.ftrans_fid 
  INNER JOIN feedback_type AS lo 
    ON (
      lo.ft_id = ft.ftrans_ft_id
    ) 
LEFT JOIN users AS u ON u.id = ft.`ftrans_createdby`";
//FILTER: year & months & Feedback type
$feedbackTypeCondition = "";
if(!empty($feedbackType)) {
    $feedbackTypeCondition = "ft.ftrans_ft_id = $feedbackType AND";
}

if ($clincName == "allClinics" && $yearMonth != "") {
    $sql .= " where $feedbackTypeCondition ft.ftrans_feedback_date like '$yearMonth%'";
    $res = sqlStatement($sql);
}
//FILTER: by clinic ID
if ($clincName != "allClinics" && $yearMonth != "") {
    $sql .= "where $feedbackTypeCondition ft.ftrans_fid='$clinicId' AND ft.ftrans_feedback_date like '$yearMonth%'";
    $res = sqlStatement($sql);
}

$listOfRes = array();

while ($iter = sqlFetchArray($res)) {
   
    $fosql = "SELECT fo.fo_id,fo.fo_type,fod.fod_fov_id, fod.fod_data FROM feedback_option AS fo INNER JOIN feedback_option_data AS fod ON fod.fod_fo_id = fo.fo_id WHERE fod.fod_ftrans_id = " . $iter["ftrans_id"] . " AND fod.fod_status = 1 ORDER BY fo.fo_sequence asc";
    $fores = sqlStatement($fosql);

    if (sqlNumRows($fores) > 0) {
        $foCount = 0;
        $qArray = array();
        while ($forow = sqlFetchArray($fores)) {
            if ($forow['fo_type'] == 'textarea') {
                $fod_data = $forow['fod_data'];
                array_push($qArray, $fod_data);
            } else {
                $fovsql = "SELECT fov_id,fov_title FROM feedback_option_value WHERE fov_id = " . $forow['fod_fov_id'];
                $fovres = sqlStatement($fovsql);
                $fovrow = sqlFetchArray($fovres);
                $fov_title = $fovrow['fov_title'];
                array_push($qArray, $fov_title);
            }
        }
    }

    list($fcr_q1r, $fcr_q2r, $fcr_q3r, $fcr_q4t,$fcr_q5r) = $qArray;

    $srNo = $iter['ftrans_auto_sr_no'];
    $patinetName = $iter["ftrans_pname"]."-".$iter['pubpid'];
    $date = date('d-m-y', strtotime($iter['ftrans_feedback_date']));  
    $fileattached = !empty($iter['ftrans_file_url']) ? "Yes" : "No";
    $createdDate = date('d-m-y', strtotime($iter['ftrans_createddate']));
    $createdBy = $iter['createdBy'];
    $ftype = $iter['title'];
    $recList = array();
    array_push($recList,$srNo, $patinetName, $date,$ftype, $fcr_q1r, $fcr_q2r, $fcr_q3r,$fcr_q4t, $fcr_q5r,$createdBy,$createdDate,$fileattached);

    array_push($listOfRes, $recList);
 
}
?>