<?php

require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

$yearMonth = $_REQUEST['yearMonth'];
$clincName = $_REQUEST['clincName'];
$clinicId = $_REQUEST['clinicId'];
$feedbackType = $_REQUEST['feedbackType'];

require_once("fcr.php");

if ($clincName == "allClinics") {
    $clincName = "All Clinics Report";
}
// Create new PHPExcel objects
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("feedbackClinicReport");

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);

$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getStyle('A2:L4')->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:A4');
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:B4');
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C2:C4');
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D2:D4');
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E2:E4');
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F2:F4');
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G2:G4');
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H2:H4');
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I2:I4');
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J2:J4');
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K2:K4');
$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setWrapText(true);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L2:L4');
$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setWrapText(true);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
$objPHPExcel->getActiveSheet()->getStyle('L1')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->setCellValue('A1', $clincName);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setSize(16)->setBold(true);

$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Clinic Id-Sr No.');
$objPHPExcel->getActiveSheet()->setCellValue('B2', 'PatientId-Name');
$objPHPExcel->getActiveSheet()->setCellValue('C2', 'Date');
$objPHPExcel->getActiveSheet()->setCellValue('D2', 'Feedback Type');
$objPHPExcel->getActiveSheet()->setCellValue('E2', 'How likely are you to recommend Star Dental to your friends or family?');
$objPHPExcel->getActiveSheet()->setCellValue('F2', 'How satisfied are you with your interaction with the Dentist?');
$objPHPExcel->getActiveSheet()->setCellValue('G2', 'Please rate the overall Quality at Star Dental? [Front office welcome, Clinic hygiene, Ambiance etc]');
$objPHPExcel->getActiveSheet()->setCellValue('H2', 'Other suggestions or comments?');
$objPHPExcel->getActiveSheet()->setCellValue('I2', 'Share Outside');
$objPHPExcel->getActiveSheet()->setCellValue('J2', 'Created By');
$objPHPExcel->getActiveSheet()->setCellValue('K2', 'Created Date');
$objPHPExcel->getActiveSheet()->setCellValue('L2', 'File attached');

$objPHPExcel->getActiveSheet()->getStyle('A2:L5')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('G2:L4')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A5:L5')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A2:A4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('B2:B4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('C2:C4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('D2:D4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('E2:E4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('F2:F4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('G2:G4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H2:H4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('I2:I4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('J2:J4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('K2:K4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('L2:L4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


foreach ($listOfRes as $monthIndex => $monthArrayFeedRes) {
    $counT = $monthIndex + 5;
    foreach ($monthArrayFeedRes as $a => $b) {
        switch ($a) {
            case 0:
                $colNm="A";
                break;
            case 1:
                $colNm="B";
                break;
            case 2:
                $colNm="C";
                break;
            case 3:
                $colNm="D";
                break;
            case 4:
                $colNm="E";
                break;
            case 5:
                $colNm="F";
                break;
            case 6:
                $colNm="G";
                break;
            case 7:
                $colNm="H";
                break;
            case 8:
                $colNm="I";
                break;
            case 9:
                $colNm="J";
                break;
            case 10:
                $colNm="K";
                break;
            case 11:
                $colNm="L";
                break;
            default:
                break;
        }

        $objPHPExcel->getActiveSheet()->setCellValue($colNm . $counT, $b);

        $objPHPExcel->getActiveSheet()->getStyle($colNm . $counT)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle($colNm . $counT)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    }
    $objPHPExcel->getActiveSheet()->getStyle($colNm . $counT)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
}

$xlsNm = "feedbackClinicReport-" . $yearMonth . ".xls";
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=$xlsNm");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>