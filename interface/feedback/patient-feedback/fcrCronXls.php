<?php
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

$yearMonth=$_REQUEST['yearMonth'];
$clincName=$_REQUEST['clincName'];
$clinicId=$_REQUEST['clinicId'];

require_once("fcr.php");

if($clincName=="allClinics"){
    $clincName="All Clinics Report";
}
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("feedbackClinicReport");
		
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);

        $objPHPExcel->getActiveSheet()->getStyle('A2:G4')->getFont()->setBold(true);
        
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:A4');
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:B4');
		$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C2:C4');
		$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D2:D4');
		$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E2:E4');
		$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F2:F4');
		$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G2:G4');
		$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
		$objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $clincName);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(16)->setBold(true);

		
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Patient Name');
		$objPHPExcel->getActiveSheet()->setCellValue('B2', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('C2', 'Feedback Form Filled');
		$objPHPExcel->getActiveSheet()->setCellValue('D2', 'How likely are you to recommend Star Dental to your friends or family?');
		$objPHPExcel->getActiveSheet()->setCellValue('E2', 'How satisfied are you with your interaction with the Dentist?');
		$objPHPExcel->getActiveSheet()->setCellValue('F2', 'Please rate the overall Quality at Star Dental? [Front office welcome, Clinic hygiene, Ambiance etc]');
		$objPHPExcel->getActiveSheet()->setCellValue('G2', 'Do you have any other suggestions or comments?');
		
		
		$objPHPExcel->getActiveSheet()->getStyle('A2:G5')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('G2:G4')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('A5:G5')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('A2:A4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('B2:B4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('C2:C4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('D2:D4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('E2:E4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('F2:F4')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		
			foreach($listOfRes as $monthIndex=>$monthArrayFeedRes){
				$counT=$monthIndex+5;
				foreach($monthArrayFeedRes as $a=>$b){ 
				if($a==0) {$colNm="A";}
				if($a==1) {$colNm="B";}
				if($a==2) {$colNm="C";}
				if($a==3) {$colNm="D";}
				if($a==4) {$colNm="E";}
				if($a==5) {$colNm="F";}
				if($a==6) {$colNm="G";}
				
				$objPHPExcel->getActiveSheet()->setCellValue($colNm.$counT,$b);
				
				$objPHPExcel->getActiveSheet()->getStyle($colNm.$counT)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle($colNm.$counT)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				}	
			$objPHPExcel->getActiveSheet()->getStyle($colNm.$counT)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);	
			}

$xlsNm="feedbackClinicReport-".$yearMonth.".xls";
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=$xlsNm");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>