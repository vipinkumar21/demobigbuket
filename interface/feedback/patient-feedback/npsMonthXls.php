<?php
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

$yearMonth=$_REQUEST['yearMonth'];
//$clincName=$_REQUEST['clincName'];
$clinicId=$_REQUEST['clinicId'];
$feedbackType = $_REQUEST['feedbackType'];

require_once("npsMonth.php");
$recArray=npsmonth($clinicId,$yearMonth,$feedbackType);
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->setActiveSheetIndex(0);
// Rename sheets
$objPHPExcel->getActiveSheet()->setTitle("feedbackNPS");
$objPHPExcel->getActiveSheet()->getStyle('B1:F1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A')->getFont()->setBold(true);
// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//B
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'How likely are you to recommend Star Dental to your friends / family');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setWrapText(true);
//C
$objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'How satisfied are you with your interaction with the Dentist?');
$objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setWrapText(true);
//D
$objPHPExcel->getActiveSheet()->getStyle('D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Please rate the overall Quality at Star Dental? [Front office welcome, Clinic hygiene, Ambience etc.]');
$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
//E
$objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Total Feedback');
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setWrapText(true);
//F
$objPHPExcel->getActiveSheet()->getStyle('F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(40);
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Total number of Patient');
$objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle('B1:F1')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('B1:F1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('F1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('C1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('D1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
$objPHPExcel->getActiveSheet()->getStyle('E1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);


foreach($recArray as $k=>$v)
{
	foreach($recArray[$k] as $clinicName=>$clinicArrayValue)
	{
		$aIndex=$k+2;

		$cellVal1="A".$aIndex;
		$cellVal2="B".$aIndex;
		$cellVal3="C".$aIndex;
		$cellVal4="D".$aIndex;
		$cellVal5="E".$aIndex;
		$cellVal6="F".$aIndex;
		
		
		$selectClinicName = "SELECT name FROM facility where id='$clinicName'";
		$selectClinicNameQuery = mysql_query ( $selectClinicName ) or die ( "Sql error : " . mysql_error( ) );
		$facilityName = mysql_fetch_assoc ( $selectClinicNameQuery );
		$facilityNameS=$facilityName['name'];
		
		$objPHPExcel->getActiveSheet()->getStyle($cellVal1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->setCellValue($cellVal1 ,$facilityNameS);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal1)->getAlignment()->setWrapText(true);
		//border
		$objPHPExcel->getActiveSheet()->getStyle($cellVal1.':'.$cellVal6)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal1)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal2)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal3)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal4)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal5)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal6)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		$objPHPExcel->getActiveSheet()->getStyle($cellVal6)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
		
		//---------------
		$totalNPS1=array();
			$totalT1=array();
		$totalNPS2=array();
			$totalT2=array();
		$totalNPS3=array();
			$totalT3=array();
		
		foreach($recArray[$k][$clinicName] as $quesArrayIndex=>$quesArrayValue)
		{
			foreach($recArray[$k][$clinicName][$quesArrayIndex] as $monthIndex=>$monthArrayFeedRes)
			{
				
					
				if($quesArrayIndex==0)
				{
					$t=$monthArrayFeedRes[3];	//42
					$p=$monthArrayFeedRes[4];	//30
					$d=$monthArrayFeedRes[7];	//4
					$countNps1=($p/$t*100)-($d/$t*100);
					$totalNPS1[$monthIndex]=$countNps1;
					$totalT1[$monthIndex]=$t;
						
				}			
				if($quesArrayIndex==1)
				{
					$t=$monthArrayFeedRes[11];	//42
					$p=$monthArrayFeedRes[12];	//30
					$d=$monthArrayFeedRes[15];	//4
					$countNps2=($p/$t*100)-($d/$t*100);
					$totalNPS2[$monthIndex]=$countNps2;
					$totalT2[$monthIndex]=$t;
				}					
				if($quesArrayIndex==2)
				{
					$t=$monthArrayFeedRes[19];	//42
					$p=$monthArrayFeedRes[20];	//30
					$d=$monthArrayFeedRes[23];	//4
					$countNps3=($p/$t*100)-($d/$t*100);
					$totalNPS3[$monthIndex]=$countNps3;
					$totalT2[$monthIndex]=$t;
				}		
			}
		}
			
		//---------------------
	}

	$qIndex=$k+2;
  	$objPHPExcel->getActiveSheet()->setCellValue('E'.$qIndex, array_sum($totalT1));
  
        $objPHPExcel->getActiveSheet()->getCell('B'.$qIndex)->setValue(array_sum($totalNPS1)/100);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$qIndex)->getNumberFormat()->setFormatCode('0%');
        
        $objPHPExcel->getActiveSheet()->getCell('C'.$qIndex)->setValue(array_sum($totalNPS2)/100);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$qIndex)->getNumberFormat()->setFormatCode('0%');
        
        $objPHPExcel->getActiveSheet()->getCell('D'.$qIndex)->setValue(array_sum($totalNPS3)/100);
        $objPHPExcel->getActiveSheet()->getStyle('D'.$qIndex)->getNumberFormat()->setFormatCode('0%');
        
        	
	$noOfPatient = "SELECT pf.pf_pid FROM patient_facilities AS pf INNER JOIN patient_data as pd ON pd.id = pf.pf_pid WHERE pf.pf_fid='$clinicName'";
	$noOfPatientQuery = mysql_query ( $noOfPatient ) or die ( "Sql error : " . mysql_error( ) );
	$totalnoOfPatient = mysql_num_rows ( $noOfPatientQuery );
	
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$qIndex, $totalnoOfPatient);
}

// last row start

if($k!=""){
$lastRec=$k+3;
$lastRecP=$k+2;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$lastRec, "All Clinics");
$x1B="B2";
$y1B="B".$lastRecP;

$conD=$lastRec-2;
$objPHPExcel->getActiveSheet()->getCell('B'.$lastRec)->setValue("=SUM($x1B:$y1B)/$conD");
$objPHPExcel->getActiveSheet()->getStyle('B'.$lastRec)->getNumberFormat()->setFormatCode('0%');

$x1C="C2";
$y1C="C".$lastRecP;
$objPHPExcel->getActiveSheet()->getCell('C'.$lastRec)->setValue("=SUM($x1C:$y1C)/$conD");
$objPHPExcel->getActiveSheet()->getStyle('C'.$lastRec)->getNumberFormat()->setFormatCode('0%');

$x1D="D2";
$y1D="D".$lastRecP;
$objPHPExcel->getActiveSheet()->getCell('D'.$lastRec)->setValue("=SUM($x1D:$y1D)/$conD");
$objPHPExcel->getActiveSheet()->getStyle('D'.$lastRec)->getNumberFormat()->setFormatCode('0%');

$x1E="E2";
$y1E="E".$lastRecP;
$objPHPExcel->getActiveSheet()->setCellValue('E'.$lastRec, "=SUM($x1E:$y1E)");

$d1="F2";
$d2="F".$lastRecP;
$objPHPExcel->getActiveSheet()->setCellValue('F'.$lastRec, "=SUM($d1:$d2)");
//last row end
}

$xlsNm="FeedbackNPS".$dateComp.".xls";
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=$xlsNm");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>
