<?php
require_once("../../globals.php");

if (!acl_check('admin', 'users'))
exit();

sqlStatement("truncate table feedback_clinic_report");

$res = sqlStatement("select ft.*, fc.name, fty.ft_name from feedback_transaction AS ft INNER JOIN facility AS fc ON fc.id = ft.ftrans_fid INNER JOIN feedback_type AS fty ON fty.ft_id = ft.ftrans_ft_id");

while($iter = sqlFetchArray($res)){
$fasId=$iter['ftrans_fid'];
$fosql = "SELECT fo.*, fod.* FROM feedback_option AS fo INNER JOIN feedback_option_data AS fod ON fod.fod_fo_id = fo.fo_id WHERE fo.fo_ft_id =".$iter['ftrans_ft_id']." AND fod.fod_ftrans_id = ".$iter["ftrans_id"]." ORDER BY fo.fo_sequence asc";
$fores = sqlStatement($fosql);
if(sqlNumRows($fores) > 0){
	$foCount = 0;
	$qArray=array();
	while($forow = sqlFetchArray($fores)){
		if($forow['fo_type'] == 'textarea'){
		$fod_data=$forow['fod_data'];
		array_push($qArray,$fod_data);
		}
		else{
		$fovsql = "SELECT * FROM feedback_option_value WHERE fov_id = ".$forow['fod_fov_id'];
					$fovres = sqlStatement($fovsql);
					$fovrow = sqlFetchArray($fovres);
					//echo $fovrow['fov_title']."<br>"; 
		$fov_title=$fovrow['fov_title'];
		array_push($qArray,$fov_title);
		}
	}
}


$fcr_ftrans_id=$iter["ftrans_id"];
$fcr_patient_name=$iter["ftrans_pname"];
$fcr_pid=$fasId; // facility Id
$fcr_clinic=$iter["name"];
$fcr_date=date('Y-m-d', strtotime($iter['ftrans_createddate']));
$fcr_fill="yes";
list($fcr_q1r,$fcr_q2r,$fcr_q3r,$fcr_q4t)=$qArray;
$fcr_q4t=  addslashes($fcr_q4t);
sqlStatement("insert into feedback_clinic_report values('$fcr_ftrans_id','$fcr_patient_name','$fcr_pid','$fcr_clinic','$fcr_date','$fcr_fill','$fcr_q1r','$fcr_q2r','$fcr_q3r','$fcr_q4t')");

$i++;
}
echo "$i record updated!";
?>