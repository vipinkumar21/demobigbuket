<?php
// Copyright (C) 2005-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report shows upcoming appointments with filtering and
// sorting by patient, practitioner, appointment type, and date.
// 2012-01-01 - Added display of home and cell phone and fixed header

require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once "$srcdir/appointments.inc.php";
require_once("../../library/pdo/lib/database.php");

$ftrans_id = $_REQUEST['id'];
$feedbackTypeWithError = '';
if ($_SESSION['fbError']) {
    echo "<h3 class='serverResponseSpan serverErrorMsg emr3'>" . $_SESSION['fbError'] . "</h3>";
    $feedbackTypeWithError = $_SESSION['feedbackTypeWithError'];
    unset($_SESSION['fbError']);
}

if (!empty($ftrans_id)) {
    $mode = 'edit';
    $sql = "Select ftrans_id,ftrans_ft_id,ftrans_fid,ftrans_pname,ftrans_pcontact,ftrans_pemail,ftrans_paddress,pubpid,ftrans_pid,ftrans_feedback_date,ftrans_ft_id,ftrans_sr_no,ftrans_feedback_source FROM feedback_transaction  WHERE ftrans_id=?";
    $ftransData = $pdoobject->custom_query($sql, array($ftrans_id), '', 'fetch');
    $facility = $ftransData['ftrans_fid'];
    $optionSql = "Select fod_ftrans_id,fod_fo_id,fod_data FROM feedback_option_data WHERE fod_ftrans_id=? AND fod_status = ?";
    $optionResult = $pdoobject->custom_query($optionSql, array($ftrans_id, 1));
}

if ($_SESSION['PostData']) {
    $data = $_SESSION['PostData'];
    $facility = $data['form_facility'];
    $pname = $data['ftrans_pname'];
    $pemail = $data['ftrans_pemail'];
    $pcontact = $data['ftrans_pcontact'];
    $paddress = $data['ftrans_paddress'];
    $fdate = $data['ftrans_feedback_date'];
    $fbType = $data['ftrans_ft_id'];
    $srNo = $data['ftrans_sr_no'];
    $pid = $data['ftrans_pname_vs'];
    $pubpid = $data['ftrans_pubpid'];
    unset($_SESSION['PostData']);
}
?>

<html>
    <head>
    <?php html_header_show(); ?>
        <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
        <title><?php xl('Feedback Report', 'e'); ?></title>
        <!-- CSS Libraries -->
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />
        <!-- custom CSS -->
        <style type="text/css">
            /* specifically include & exclude from printing */
            @media print {
                #report_parameters {
                    visibility: hidden;
                    display: none;
                }
                #report_parameters_daterange {
                    visibility: visible;
                    display: inline;
                }
                #report_results table {
                    margin-top: 0px;
                }
            }
            /* specifically exclude some from the screen */
            @media screen {
                #report_parameters_daterange {
                    visibility: hidden;
                    display: none;
                }
            }
        </style>
        <!-- JS Libraries -->
        <script type="text/javascript" src="../../library/overlib_mini.js"></script>
        <script type="text/javascript" src="../../library/textformat.js"></script>
        <script type="text/javascript" src="../../library/dialog.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.1.3.2.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.easydrag.handler.beta2.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/jquery.searchabledropdown-1.0.8.min.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript">
            jQuery.noConflict();
            function validateEmail(email) {
                var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
                if (reg.test(email) == false) {
                    return false;
                } else {
                    return true;
                }
            }

            function validatePhoneNum(phone) {
                var regex1 = /^([0]|\+[1-9]{1,4}[\-])\d{2}\s*\d{8}$/;
                var regex2 = /^([0]|\+[1-9]{1,4}[\-])\d{3}\s*\d{7}$/;
                var regex3 = /^([0]|\+[1-9]{1,4}[\-])\d{4}\s*\d{6}$/;
                if (regex1.test(phone) || regex2.test(phone) || regex3.test(phone)) {
                    return true;
                } else {
                    return false;
                }
            }

            function trimAll(sString) {
                while (sString.substring(0, 1) == ' ') {
                    sString = sString.substring(1, sString.length);
                }
                while (sString.substring(sString.length - 1, sString.length) == ' ') {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }

            function removeA(arr) {
                var what, a = arguments, L = a.length, ax;
                while (L > 1 && arr.length) {
                    what = a[--L];
                    while ((ax = arr.indexOf(what)) !== -1) {
                        arr.splice(ax, 1);
                    }
                }
                return arr;
            }

            function queryStringFetcher (queryString) {
                var queryStringVal = queryString.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]"), // jshint ignore:line
                        regex = new RegExp("[\\?&]" + queryStringVal + "=([^&#]*)"),
                        results = regex.exec(location.search);

                queryStringVal = results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, null));
                return queryStringVal;
            }

            //datevalitation fun

            function dateValidation(dateVal){
                    var regPattern = /^(19|20)\d\d(-)(0[1-9]|1[012])(-)(0[1-9]|[12][0-9]|3[01])$/;
                    var checkArray = dateVal.match(regPattern);
                    if (checkArray == null){
                        return false;
                    }else{
                        return true;
                    }
            }

            function submitform() {
                //inserting new conditions for validations
                //console.log(document.getElementById('form_facility').value);

                document.getElementById('facilityCheck').style.display = "none";
                document.getElementById('nameCheck').style.display = "none";
                document.getElementById('mobileCheck').style.display = "none";
                document.getElementById('submissionDateCheck').style.display = "none";
                document.getElementById('serialNoCheck').style.display = "none";
                document.getElementById('fileUploadCheck').style.display = "none";

                var errorInPage = false;
                var unanswered = new Array();
                var unenteredTa = new Array();

                var selectedFeedbackType = null;
                var inputElements = document.forms[0].ftrans_ft_id;
                for(var ii=0; inputElements[ii]; ++ii){
                      if(inputElements[ii].checked){
                           selectedFeedbackType = inputElements[ii].value;
                           break;
                      }
                }
                //console.log(selectedFeedbackType); //feedbackTypeQue_
                var checkFeedbackTypeFields = 'feedbackTypeQue_' + selectedFeedbackType;
                //console.log(checkFeedbackTypeFields);
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    //&& (document.forms[0].elements[i].getElementById)
                    if ((document.forms[0].elements[i].type == 'radio') && ((document.forms[0].elements[i].id===checkFeedbackTypeFields) || (document.forms[0].elements[i].name === 'ftrans_ft_id'))) {
                        //console.log(document.forms[0].elements[i].id);
                        if (document.forms[0].elements[i].checked == false) {
                            var preCount = i - 1;
                            if (document.forms[0].elements[i].name != document.forms[0].elements[preCount].name) {
                                unanswered.push(document.forms[0].elements[i].name);
                            }
                        } else {
                            removeA(unanswered, document.forms[0].elements[i].name);
                        }
                    } else if ((document.forms[0].elements[i].type == 'textarea') && (document.forms[0].elements[i].id===checkFeedbackTypeFields)) {
                        if (document.forms[0].elements[i].value == '') {
                            unenteredTa.push(document.forms[0].elements[i].name);
                        }
                    }
                }
                var editcase = '<?php echo $mode;?>';
                 //FAcility Check
                if (document.forms[0].form_facility.value.length <= 0)
                    {
                        errorInPage = true;
                        document.getElementById('facilityCheck').style.display = "block";
                        document.forms[0].form_facility.focus();
                        return false;
                    }
                //Name Check
                    if (document.forms[0].ftrans_pname.value.length <= 0)
                    {
                        errorInPage = true;
                        document.getElementById('nameCheck').style.display = "block";
                        document.forms[0].ftrans_pname.focus();
                        return false;
                    }
                    //Mobile Check
                    if (document.forms[0].ftrans_pcontact.value.length <= 0 )
                    {
                        errorInPage = true;
                        document.getElementById('mobileCheck').style.display = "block";
                        document.forms[0].ftrans_pcontact.focus();
                        return false;
                    }
                    //Feedback Date Check
                    if (document.forms[0].ftrans_feedback_date.value.length <= 0 || dateValidation(document.forms[0].ftrans_feedback_date.value) == false)
                    {
                        errorInPage = true;
                        document.getElementById('submissionDateCheck').style.display = "block";
                        document.forms[0].ftrans_feedback_date.focus();
                        return false;
                    }
                    //Serial No Check
                    if (document.forms[0].ftrans_sr_no.value.length <= 0 || document.forms[0].ftrans_sr_no.value == "")
                    {
                        errorInPage = true;
                        document.getElementById('serialNoCheck').style.display = "block";
                        document.forms[0].ftrans_sr_no.focus();
                        return false;
                    }
                     //Document Upload Check
                    if ((document.forms[0].file_name.value.length <= 0 ) && !queryStringFetcher('id'))
                    {
                        errorInPage = true;
                        document.getElementById('fileUploadCheck').style.display = "block";
                        document.forms[0].file_name.focus();
                        return false;
                    }
                    if (!!errorInPage) {
                        return false;
                    }
                    if (unanswered.length > 0) {
                        msg = "Please answer all the Clove Feedback Questions";
                        jAlert(msg);
                        return false;
                    }
                    if (unenteredTa.length > 0) {
                        msg = "Please provide your valuable feedback or suggestion", "Required field missing";
                        jAlert(msg);
                        return false;
                    }
                    if((document.forms[0].file_name.value.length > 0 || editcase == 'edit')){
                        document.forms[0].submit();
                    }
                    //return true;

                }

        </script>
    </head>
    <body class="body_top feedbackAddPage">
        <!-- Required for the popup date selectors -->
        <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>

        <div>
<?php
if ($_SESSION['fbError']) {
    echo $_SESSION['fbError'];
}
?>
        </div>
        <h3  class='emrh3'><?php
            if (empty($ftrans_id)) {
                xl('Add', 'e');
            } else {
                xl('Edit', 'e');
            }
            ?> - <?php xl('Feedback', 'e'); ?></h3>
        <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
        </div>

        <form method='post' name='theform' id='theform' action='feedback_save.php'  enctype="multipart/form-data" class='emrform'>
            <input type="hidden" name="mode" value="<?php echo (!empty($mode)) ? $mode : 'new'; ?>">
            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>">
            <?php if (!empty($ftrans_id)) { ?><input type="hidden" name="ftrans_id" value="<?php echo $ftrans_id; ?>">
                <input type="hidden" name="ftrans_feedback_source" value="<?php echo $ftransData['ftrans_feedback_source']; ?>"> <?php } ?>
            <div id="report_parameters">
                <table class="feedback-add">
                    <!-- 1st Row -->
                    <tr>
                        <td class='label'><?php xl('Facility', 'e'); ?>:<span class="newRequired">*</span> </td>
                        <td>
                            <?php dropdown_facility(strip_escape_custom($facility), 'form_facility', null, 0, 1); ?>
                            <span id="facilityCheck" class="validationSpan">Please select facility!</span>
                        </td>
                        <td class='label'><?php xl('Name', 'e'); ?>:<span class="newRequired">*</span></td>
                        <td>
                            <input class='emrdate' name="ftrans_pname" id="ftrans_pname" value="<?php echo!empty($pname) ? $pname : $ftransData['ftrans_pname']; ?>" type="text">
                            <input name="ftrans_pname_vs" id="ftrans_pname_vs" value="<?php echo!empty($pid) ? $pid : $ftransData['ftrans_pid']; ?>" type="hidden" />
                            <input name="ftrans_pubpid" id="ftrans_pubpid" value="<?php echo!empty($pubpid) ? $pubpid : $ftransData['pubpid']; ?>" type="hidden" />
                            <span id="nameCheck" class="validationSpan">Enter name!</span>
                        </td>
                    </tr>
                    <!-- 2nd Row -->
                    <tr>
                        <td class='label'><?php xl('Email Address', 'e'); ?>:</td>
                        <td>
                            <input class='emrdate' name="ftrans_pemail" id="ftrans_pemail" value="<?php echo!empty($pemail) ? $pemail : $ftransData['ftrans_pemail']; ?>" type="text">
                        </td>
                        <td class='label'><?php xl('Mobile', 'e'); ?>:<span class="newRequired">*</span></td>
                        <td>
                            <input class='emrdate' name="ftrans_pcontact" id="ftrans_pcontact" value="<?php echo!empty($pcontact) ? $pcontact : $ftransData['ftrans_pcontact']; ?>" type="number">
                            <span id="mobileCheck" class="validationSpan">Enter mobile!</span>
                        </td>
                    </tr>
                    <!-- 3rd Row -->
                    <tr>
                        <td class='label'><?php xl('Address', 'e'); ?>:</td>
                        <td>
                            <input class='emrdate' name="ftrans_paddress" id="ftrans_paddress" value="<?php echo!empty($paddress) ? $paddress : $ftransData['ftrans_address']; ?>" type="text">
                        </td>
                        <td class='label '><?php xl('Sub Date', 'e'); ?>:<span class="newRequired">*</span></td>
                        <td class='posRel'>
                            <input class='emrdate' required placeholder="yyyy-mm-dd" type='text' name='ftrans_feedback_date' id="ftrans_feedback_date" size='10' value='<?php echo!empty($fdate) ? $fdate : $ftransData['ftrans_feedback_date']; ?>'
                                    title='yyyy-mm-dd'>
                            <img src='../pic/show_calendar.gif' align='absbottom' width='24' height='22'id='img_feedback_date' border='0' alt='[?]' style='cursor:pointer' title='<?php xl('Click here to choose a date', 'e'); ?>'>
                            <span id="submissionDateCheck" class="validationSpan">Please enter valid Submission Date!</span>

                        </td>
                    </tr>
                    <!-- 4th Row -->
                    <tr>
                        <td class='label'><?php xl('Feedback Type', 'e'); ?>:<span class="newRequired">*</span></td>
                        <td>
                            <?php
                            $ftSql = "Select ft_id, ft_name FROM feedback_type WHERE ft_status = 1";
                            $ftResult = $pdoobject->custom_query($ftSql);
                            foreach ($ftResult as $value) {
                                ?>
                                <?php echo $value['ft_name']; ?> <input type="radio" value="<?php echo $value['ft_id']; ?>" name="ftrans_ft_id" <?php
                                if (!empty($fbType)) {
                                    echo ($fbType == $value['ft_id']) ? 'checked' : '';
                                } else {
                                    echo ($ftransData['ftrans_ft_id'] == $value['ft_id']) ? 'checked' : '';
                                }
                                ?>>

<?php } ?>
                            <span id="facilityCheck" class="validationSpan">Enter Feedback Type!</span>
                        </td>
                        <td class='label'><?php xl('Serial no.', 'e'); ?>:<span class="newRequired">*</span></td>
                        <td>
                            <input class='emrdate' name="ftrans_sr_no" id="ftrans_sr_no" value="<?php echo!empty($srNo) ? $srNo : $ftransData['ftrans_sr_no']; ?>" type="number">
                            <span id="serialNoCheck" class="validationSpan">Enter Serial No!</span>
                        </td>
                    </tr>

                    <!-- 5th to 8th Row -->

<?php
foreach ($ftResult as $feedbackValue) {
?>
<tr>
<?php
$fosql = "SELECT fo_id,fo_desc,fo_type,fo_is_response FROM feedback_option WHERE fo_ft_id = '".$feedbackValue['ft_id']."' ORDER BY fo_sequence asc";
$fores = sqlStatement($fosql);
if (sqlNumRows($fores) > 0) {
    $foCount = 0;
    while ($forow = sqlFetchArray($fores)) {
?>
        <td colspan='2' width="50%" id="feedbackQueType_<?php echo $feedbackValue['ft_id'];?>" class="feedbackQues">
            <table>
                <tr>
                    <td class='label' style="text-align:left;"><?php echo $forow['fo_desc']; ?><span class="newRequired">*</span></td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" name="feedback_option[]" value="<?php echo $forow['fo_id']; ?>">
<?php
        if (!empty($ftrans_id)) {
            $foArray = array();
            $foId = $foData = '';
            foreach ($optionResult as $result) {
                if ($result['fod_fo_id'] == $forow['fo_id']) {
                    $foId = $result['fod_fo_id'];
                    $foData = $result['fod_data'];
                    break;
                }
            }
        }
        if ($forow['fo_type'] == 'textarea') {
?>
            <textarea rows="4" cols="50" id="feedbackTypeQue_<?php echo $feedbackValue['ft_id'];?>" name="feedback_option_data_<?php echo $forow['fo_id']; ?>" value="">
<?php
            if ($foId == $forow['fo_id']) {
                echo $foData;
            } elseif (!empty($data["feedback_option_data_" . $forow['fo_id']])) {
                echo $data["feedback_option_data_" . $forow['fo_id']];
            };
?></textarea>
<?php
        } else if ($forow['fo_type'] == 'radionbutton') {
            $fovsql = "SELECT fov_id,fov_fo_id,fov_desc,fov_title FROM feedback_option_value WHERE fov_fo_id =" . $forow['fo_id'] . " ORDER BY fov_sequence asc";
            $fovres = sqlStatement($fovsql);
            if (sqlNumRows($fovres) > 0) {
                while ($fovrow = sqlFetchArray($fovres)) {
                    $rbName = ($forow['fo_is_response'] == 1) ? "feedback_response_value_" . $forow['fo_id'] : "feedback_option_data_" . $forow['fo_id'];
?>
                    <div style="float:left; position: relative" class="feedbackCheckboxWrap">
                    <input type="radio" id="feedbackTypeQue_<?php echo $feedbackValue['ft_id'];?>" name="<?php echo $rbName; ?>" value="<?php echo $fovrow['fov_title']; ?>" <?php if ((($foId == $forow['fo_id']) && ($fovrow['fov_title'] == $foData))) { echo 'checked'; } elseif ($data[$rbName] == $fovrow['fov_title']) { echo 'checked';} ?>>
                        <?php
                        echo $fovrow['fov_title'];
                        if (!empty($fovrow['fov_desc'])) {
                        ?>
                            <span style="position: absolute; top: 20px; left:0; width:110px; margin-left: 0;"><?php echo $fovrow['fov_desc']; ?></span>
                        <?php
                        }
                        ?>
                    </div>
<?php
                }
            }
        }
?>
                    </td>
                </tr>
            </table>
        </td>
<?php
        $foCount++;
        if ($foCount != 0 && $foCount % 2 == 0) {
            echo '</tr><tr>';
        }
        //All this need to be created again(It needs revamp)
        if($foCount == 5 && $feedbackValue['ft_id']==2) {
            echo '</tr><tr>';
        }
    }
}

}
?>
                        <td class='label'><?php xl('Upload File', 'e'); ?>:
                            <?php
                            if (empty($ftrans_id)) {
                                ?>
                                <span class="newRequired">*</span>
                                <?php
                            }
                            ?>
                        </td>
                        <td style="position:relative;">
                            <input class='emrdate' name="file_name" id="file_name" value="" type="file">
                            <span id="fileUploadCheck" class="validationSpan">Upload a file!</span>
                        </td>
                    </tr>

                    <!-- 8th Row -->
                    <tr>
                        <td align='right' valign='middle' height="100%" colspan='4'>
                            <div style='margin-left: 15px'>
                                <a href='#' class='btn btn-warning btn-sm' onclick='return submitform();'><span> <?php xl('Submit', 'e'); ?> </span> </a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </form>

        <script type="text/javascript">
            jQuery.noConflict();
            jQuery(document).ready(function ($) {
                $("#form_patient").searchable();
                $("#form_patient").change(function () {
                    var form_patient = $("#form_patient").val();
                    var string = JSON.stringify({
                        "patientId": form_patient,
                    });
                    $.ajax({
                        type: 'GET',
                        url: "get_patient_detail.php?patientId=" + form_patient,
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 1) {
                                $("#ftrans_pname").val(data.patientData.pname);
                                $("#ftrans_pemail").val(data.patientData.pemail);
                                $("#ftrans_pcontact").val(data.patientData.pcontact);
                                $("#ftrans_paddress").val(data.patientData.paddress);
                            } else {
                                $("#ftrans_pname").val('');
                                $("#ftrans_pemail").val('');
                                $("#ftrans_pcontact").val('');
                                $("#ftrans_paddress").val('');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('Patient ChiefComplaint Error: ' + textStatus);
                        }
                    });
                });
                //Feedback Questions block
                var feedbackId = "<?php echo $ftrans_id;?>";
                var feedbackTypeWithError  = "<?php echo $feedbackTypeWithError ;?>";

                if(feedbackId == '') {
                    $('tr > .feedbackQues').hide();
                } else {
                    var typeId = "<?php echo $ftransData['ftrans_ft_id'];?>";
                    $('tr > .feedbackQues').hide();
                    $('tr > #feedbackQueType_'+typeId).show();
                }
                $("input[type='radio'][name='ftrans_ft_id']").change(function () {
                    var queId = $("input[type='radio'][name='ftrans_ft_id']:checked").val();
                    $('tr > .feedbackQues').hide();
                    $('tr > #feedbackQueType_'+queId).show();
                });
                if(feedbackTypeWithError != ''){
                    $('tr > #feedbackQueType_'+feedbackTypeWithError).show();
                }

            });
        </script>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <!-- Major jQuery code is here -->
        <script type="text/javascript">

            var j12 = jQuery.noConflict();

            function autocompleteFacility() {

                j12("#facilityName").autocomplete({
                    minLength: 3,
                    source: function (request, response) {
                        j12('#facilityName_detect').val('0');
                        j12("#form_facility_detect").val('0');
                        j12.ajax({
                            url: 'get_clinic_detail.php/' + j12('#facilityName').val(),
                            type: 'GET',
                            contentType: 'application/json',
                            dataType: 'json',
                            data: {
                                q: request.term
                            },
                            success: function (data) {
                                console.log(data);
                                response(j12.map(data, function (item) {
                                    return {
                                        label: item.label,
                                        value: item.label,
                                        val: item.value
                                    };
                                }));
                            }
                        });
                    },
                    select: function (event, ui) {
                        j12('#facilityName_detect').val('1');
                        if (ui.item) {
                            j12("#form_facility").val(ui.item.val);
                        }
                    },
                    open: function () {
                        // taking width of input and assigning it to the autocomplete list
                        var inputWdth = j12(this).width();
                        var listWdth = inputWdth + 5;
                        j12(".ui-helper-hidden-accessible").hide();
                        j12(".ui-autocomplete.ui-front").css("width", listWdth);
                        j12(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                    },
                    close: function () {
                        j12(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                    }
                });

                j12("#facilityName").keyup(function (event) {
                    j12("#form_facility").val('');
                });
            }

            function autocompleteName() {
                j12("#ftrans_pname").autocomplete({
                    minLength: 3,
                    source: function (request, response) {
                        j12('#ftrans_pname_detect').val('0');
                        j12("#ftrans_pname_vs_detect").val('0');
                        j12("#ftrans_pubpid_detect").val('0');
                        j12.ajax({
                            url: "get_patient_detail.php", // Replace this with your JSON Struc.
                            type: 'GET',
                            contentType: 'application/json',
                            dataType: 'json',
                            data: {
                                q: request.term
                            },
                            success: function (data) {
                                console.log(data);
                                response(j12.map(data, function (item) {
                                    return {
                                        label: item.pname + ': ' + item.pcontact,
                                        value: item.pname,
                                        ftrans_ID: item.id,
                                        ftrans_pname: item.pname,
                                        ftrans_pemail: item.pemail,
                                        ftrans_pcontact: item.pcontact,
                                        ftrans_paddress: item.paddress,
                                        ftrans_pubpid: item.pubpid
                                    };
                                }));
                            }
                        });
                    },
                    select: function (event, ui) {
                        j12('#ftrans_pname_detect').val('1');
                        j12("#ftrans_pname_vs").val(ui.item.ftrans_ID);
                        j12("#ftrans_pubpid").val(ui.item.ftrans_pubpid);
                        if (ui.item) {
                            j12('#ftrans_pemail').val(ui.item.ftrans_pemail);
                            j12('#ftrans_pcontact').val(ui.item.ftrans_pcontact);
                            j12('#ftrans_paddress').val(ui.item.ftrans_paddress);
                        }
                    },
                    open: function () {
                        // taking width of input and assigning it to the autocomplete list
                        var inputWdth = j12(this).width();
                        var listWdth = inputWdth + 4;
                        j12(".ui-helper-hidden-accessible").hide();
                        j12(".ui-autocomplete.ui-front").css("width", listWdth);
                        j12(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                    },
                    close: function () {
                        j12(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                    }
                });

                j12("#ftrans_pname_").keyup(function (event) {
                    j12("#ftrans_pname_vs").val('');
                });
            }

            j12(document).ready(function () {
                autocompleteFacility();
                autocompleteName();
            });

        </script>

        <!-- stuff for the popup calendar -->
        <style type="text/css">@import url(../../library/dynarch_calendar.css);</style>
        <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
<?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
        <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>
        <script>
            Calendar.setup({
                inputField: "ftrans_feedback_date",
                ifFormat: "%Y-%m-%d",
                button: "img_feedback_date"
            });
        </script>
    </body>
</html>