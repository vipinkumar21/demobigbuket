<?php
// Copyright (C) 2005-2010 Rod Roark <rod@sunsetsystems.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// This report shows upcoming appointments with filtering and
// sorting by patient, practitioner, appointment type, and date.
// 2012-01-01 - Added display of home and cell phone and fixed header

require_once("../globals.php");
require_once("../../library/patient.inc");
require_once("$srcdir/formatting.inc.php");
require_once "$srcdir/options.inc.php";
require_once "$srcdir/formdata.inc.php";
require_once "$srcdir/appointments.inc.php";
require_once ("$audting_webroot/auditlog.php");
require_once("../../library/pdo/lib/database.php");
require_once("../inventory/drugs.inc.php");
$alertmsg = ''; // not used yet but maybe later
$searchParam = '';
$patient = $_REQUEST['patient'];

if($_REQUEST['page']) {
    $pageSelection = $_REQUEST['page'];
}

// Prepare a string for CSV export.
function qescape($str) {
    $str = str_replace('\\', '\\\\', $str);
    return str_replace('"', '\\"', $str);
}

if ($patient && !$_REQUEST['form_from_date']) {
    // If a specific patient, default to 2 years ago.
    $tmp = date('Y') - 2;
    $from_date = date("$tmp-m-d");
} else {
    $from_date = fixDate($_REQUEST['form_from_date'], date('Y-m-d'));
    $to_date = fixDate($_REQUEST['form_to_date'], date('Y-m-d'));
}

$show_available_times = false;
if ($_POST['form_show_available']) {
    $show_available_times = true;
}
//Pass params to pagination 
$requestData = $_REQUEST;
unset($requestData['form_refresh']);
unset($requestData['form_csvexport']);
$requestParams = '';
foreach($requestData as $requestKey => $requestValue) {
    if(!empty($requestParams)) {
        $requestParams .= "&";
    }
    $requestParams .=  $requestKey."=".$requestValue;
}

$provider = $_REQUEST['form_provider'];
$facility = $_REQUEST['form_facility'];  //(CHEMED) facility filter
$form_orderby = getComparisonOrder($_REQUEST['form_orderby']) ? $_REQUEST['form_orderby'] : 'date';
$form_response = $_REQUEST['form_response'];
$form_status = $_REQUEST['form_status'];
$form_followup = $_REQUEST['form_followup'];
$form_feedback_type = $_REQUEST['form_feedback_type'];
$form_patient_details = trim($_REQUEST['form_patient_details']);
$fileName = "feedback_report_" . date("Ymd_his") . ".csv";
$form_creator = $_REQUEST['form_creator'];
$form_date = $_REQUEST['form_date'];

switch($form_date){
    case 1:
        $formDateRes = 'ftrans_createddate';
        break;
    case 2:
        $formDateRes = 'ftrans_modifieddate';
        break;
    case 3:
        $formDateRes = 'ftrans_feedback_date';
        break;
    default:
        $formDateRes = 'ftrans_createddate';
        
}
if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvfeedback') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Description: File Transfer");
} else {
    ?>
    <html>
        <head>
            <?php html_header_show(); ?>


            <title><?php xl('Feedback Report', 'e'); ?></title>

            <link rel="stylesheet" href="<?php echo $css_header; ?>" type="text/css">
            <link rel=stylesheet href="../themes/bootstrap.css" type="text/css" />
            <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.css" media="screen" />
            <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['webroot'] ?>/css/jquery.alerts.css" />

            <!-- stuff for the popup calendar -->
            <link rel=stylesheet href="../../library/dynarch_calendar.css" type="text/css" />

            <script type="text/javascript" src="../../library/overlib_mini.js"></script>
            <script type="text/javascript" src="../../library/textformat.js"></script>
            <script type="text/javascript" src="../../library/dialog.js"></script>
            <script type="text/javascript" src="../../library/js/jquery.1.3.2.js"></script>
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/common.js"></script>
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/fancybox/jquery.fancybox-1.2.6.js"></script>
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery-ui.js"></script>
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/library/js/jquery.easydrag.handler.beta2.js"></script>
            <script type="text/javascript" src="<?php echo $GLOBALS['webroot'] ?>/js/jquery.alerts.js"></script>
            <script type="text/javascript" src="../../library/dynarch_calendar.js"></script>
            <?php include_once("{$GLOBALS['srcdir']}/dynarch_calendar_en.inc.php"); ?>
            <script type="text/javascript" src="../../library/dynarch_calendar_setup.js"></script>

            <script type="text/javascript">

                $(document).ready(function () {
                    // fancy box
                    enable_modals();
                    tabbify();
                    // special size for
                    $(".iframe_medium").fancybox({
                        'overlayOpacity': 0.0,
                        'showCloseButton': true,
                        'frameHeight': 450,
                        'frameWidth': 660
                    });
                    $(function () {
                        // add drag and drop functionality to fancybox
                        $("#fancy_outer").easydrag();
                    });
                });
                var mypcc = '<?php echo $GLOBALS['phone_country_code'] ?>';
                function dosort(orderby) {
                    var f = document.forms[0];
                    f.form_orderby.value = orderby;
                    f.submit();
                    return false;
                }

                function oldEvt(eventid) {
                    dlgopen('../main/calendar/add_edit_event.php?eid=' + eventid, 'blank', 550, 270);
                }
                function refreshme() {
                    document.forms[0].submit();
                }

            </script>
            <style type="text/css">
                /* specifically include & exclude from printing */
                @media print {
                    #report_parameters {
                        visibility: hidden;
                        display: none;
                    }
                    #report_parameters_daterange {
                        visibility: visible;
                        display: inline;
                    }
                    #report_results table {
                        margin-top: 0px;
                    }
                }

                /* specifically exclude some from the screen */
                @media screen {
                    #report_parameters_daterange {
                        visibility: hidden;
                        display: none;
                    }
                }
            </style>
        </head>
        <body class="body_top feedbackReportPage">
            <!-- Required for the popup date selectors -->
            <div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
                 <div>
                         <?php
                         if ($_SESSION['fbSuccess']) {
                             echo "<h3 class='serverResponseSpan emr3'>".$_SESSION['fbSuccess']."</h3>";
                             unset($_SESSION['fbSuccess']);
                         }
                         ?>
                 </div>
            <h3 class='emrh3'><?php xl('Report', 'e'); ?> - <?php xl('Feedback', 'e'); ?></h3>

            <div id="report_parameters_daterange"><?php echo date("d F Y", strtotime($form_from_date)) . " &nbsp; to &nbsp; " . date("d F Y", strtotime($form_to_date)); ?>
            </div>

            <form method='post' name='theform' id='theform' action='feedback_report.php' class='emrform topnopad botnopad'>
                    <input type='hidden' name='form_refresh' id='form_refresh' value=''/>
                    <input type='hidden' name='form_csvexport' id='form_csvexport' value=''/>

                    <div id="report_parameters">

                        <table>
                            <tr>
                                <td width='80%'>

                                    <table class='text'>
                                        <tr>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Facility', 'e'); ?>:</td>
                                            <td width="24%"><?php dropdown_facility(strip_escape_custom($facility), 'form_facility'); ?></td>
                                            <td width="5%" class='label newfontnormal newrel10'><?php xl('From', 'e'); ?>:</td>
                                            <td width="28%"><input class='emrdate' type='text' name='form_from_date' id="form_from_date"
                                                                   size='10' value='<?php echo $form_from_date ?>'
                                                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                   align='absbottom' width='24' height='22' id='img_from_date'
                                                                   border='0' alt='[?]' style='cursor: pointer'
                                                                   title='<?php xl('Click here to choose a date', 'e'); ?>'></td>
                                            <td width="3%" class='label newfontnormal newrel10'><?php xl('To', 'e'); ?>:</td>
                                            <td width="30%"><input class='emrdate' type='text' name='form_to_date' id="form_to_date"
                                                                   size='10' value='<?php echo $form_to_date ?>'
                                                                   onkeyup='datekeyup(this, mypcc)' onblur='dateblur(this, mypcc)'
                                                                   title='yyyy-mm-dd'> <img src='../pic/show_calendar.gif'
                                                                   align='absbottom' width='24' height='22' name='img_to_date' id='img_to_date'
                                                                   border='0' alt='[?]' style='cursor: pointer'
                                                                   title='<?php xl('Click here to choose a date', 'e'); ?>'></td>

                                        </tr>
                                        <tr>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Response', 'e'); ?>:</td>
                                            <td width="24%">
                                                <select class="form-control input-sm" name="form_response" id="form_response">
                                                    <option value="pleaseSelect" <?php echo ($form_response == 'pleaseSelect' || $form_response == '') ? "selected='selected'" : "" ?>>-- All Response --</option>
                                                    <option value="1" <?php echo ($form_response == '1') ? "selected='selected'" : "" ?>>Promoters</option>
                                                    <option value="2" <?php echo ($form_response == '2') ? "selected='selected'" : "" ?>>Passives</option>
                                                    <option value="3" <?php echo ($form_response == '3') ? "selected='selected'" : "" ?>>Detractors</option>
                                                </select>
                                            </td>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Status', 'e'); ?>:</td>
                                            <td width="24%">
                                                <select class="form-control input-sm" name="form_status" id="form_status">
                                                    <option value="pleaseSelect" <?php echo ($form_status == 'pleaseSelect' || $form_status == '') ? "selected='selected'" : "" ?>>-- All Status --</option>
                                                    <option value="0" <?php echo ($form_status == '0') ? "selected='selected'" : "" ?>>Open</option>
                                                    <option value="1" <?php echo ($form_status == '1') ? "selected='selected'" : "" ?>>In-Progress</option>
                                                    <option value="2" <?php echo ($form_status == '2') ? "selected='selected'" : "" ?>>Closed</option>
                                                </select>
                                            </td>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Follow-up', 'e'); ?>:</td>
                                            <td width="24%">
                                                <select class="form-control input-sm" name="form_followup" id="form_followup">
                                                    <option value="pleaseSelect" <?php echo ($form_followup == 'pleaseSelect' || $form_followup == '') ? "selected='selected'" : "" ?>>-- All Followup --</option>
                                                    <option value="1" <?php echo (($form_followup == '1') || ($form_followup == '2')) ? "selected='selected'" : "" ?>>Yes</option>
                                                    <option value="0" <?php echo ($form_followup == '0') ? "selected='selected'" : "" ?>>No</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Feedback Date', 'e'); ?>:</td>
                                            <td width="24%">
                                                <select class="form-control input-sm" name="form_date" id="form_date">
                                                    <option value="pleaseSelect" <?php echo ($form_date == 'pleaseSelect' || $form_date == '') ? "selected='selected'" : "" ?>>-- All --</option>
                                                    <option value="1" <?php echo ($form_date == '1') ? "selected='selected'" : "" ?>>Creation</option>
                                                    <option value="2" <?php echo ($form_date == '2') ? "selected='selected'" : "" ?>>Updation</option>
                                                    <option value="3" <?php echo ($form_date == '3') ? "selected='selected'" : "" ?>>Submission</option>
                                                </select>
                                            </td>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Creator', 'e'); ?>:</td>
                                            <td width="24%">
                                                <select class="form-control input-sm" name="form_creator" id="form_creator">
                                                    <option value="pleaseSelect" <?php echo ($form_creator == 'pleaseSelect' || $form_creator == '') ? "selected='selected'" : "" ?>>-- All Creator --</option>
                                                    <option value="1" <?php echo ($form_creator == '1') ? "selected='selected'" : "" ?>>App</option>
                                                    <option value="0" <?php echo ($form_creator == '0') ? "selected='selected'" : "" ?>>Doctor</option>
                                                </select>
                                            </td>
                                            <td calspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Feedback Type', 'e'); ?>:</td>
                                            <td width="24%">
                                                <select class="form-control input-sm" name="form_feedback_type" id="form_feedback_type">
                                                    <option value="pleaseSelect" <?php echo ($form_feedback_type == 'pleaseSelect' || $form_feedback_type == '') ? "selected='selected'" : "" ?>>-- All Feedback Type --</option>
                                                    <?php
                                                    $ftSql = "Select ft_id, ft_name FROM feedback_type WHERE ft_status = 1";
                                                    $ftList = sqlStatement($ftSql);
                                                    while ($ftRow = sqlFetchArray($ftList)) {
                                                        ?>
                                                        <option value="<?php echo $ftRow['ft_id']; ?>" <?php echo ($form_feedback_type == $ftRow['ft_id']) ? "selected='selected'" : "" ?>><?php echo $ftRow['ft_name']; ?></option>
                                                    <?php } ?>     
                                                </select>
                                            </td> 
                                            <td width="10%" class='label newfontnormal newrel10'><?php xl('Patient Details', 'e'); ?>:</td>
                                            <td width="24%"> <input class='emrdate' type="text" name="form_patient_details" id="form_patient_details" value="<?php echo $form_patient_details; ?>"></td>
                                            <td style="position:relative;">
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "viewfeedback");
                                                    $("#theform").submit();'>
                                                    <span> <?php xl('Submit', 'e'); ?> </span> 
                                                </a> 
                                                <a href='#' class='btn btn-warning btn-sm' onclick='$("#form_csvexport").attr("value", "csvfeedback");
                                                        $("#theform").submit();'>
                                                    <span><?php xl('Export to CSV', 'e'); ?>
                                                    </span>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                            </tr>
                        </table>
                    </div>
                </form>
                <!--  --> 
                <?php
            } // End Else 
            if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvfeedback') {
                echo '"' . xl('#ID.') . '",';
                echo '"' . xl('Sr. No.') . '",';
                echo '"' . xl('Date') . '",';
                echo '"' . xl('Type') . '",';
                echo '"' . xl('Clinic') . '",';
                echo '"' . xl('Patient Id') . '",';
                echo '"' . xl('Patient Name') . '",';
                echo '"' . xl('Email') . '",';
                echo '"' . xl('Contact#') . '",';
                echo '"' . xl('Follow-Up Status') . '",';
                echo '"' . xl('Created By') . '",';
                echo '"' . xl('Created Date') . '",';
                echo '"' . xl('Response') . '",';
                echo '"' . xl('Status') . '"' . "\n";
            } else if ($pageSelection || $_POST['form_csvexport'] == 'viewfeedback') {
                ?>
                <div id="report_results">
                    <table class='emrtable'>
                        <thead>
                        <th><?php xl('#ID', 'e'); ?></th>
                        <th><?php xl('Date', 'e'); ?></th>
                        <th><?php xl('Type', 'e'); ?></th>
                        <th><?php xl('Clinic', 'e'); ?></th>
                        <th><?php xl('Patient Id : Name', 'e'); ?></th>
                        <th><?php xl('Email', 'e'); ?></th>
                        <th><?php xl('Contact#', 'e'); ?></th>
                        <th><?php xl('Follow-Up Status', 'e'); ?></th>
                        <th><?php xl('Created By', 'e'); ?></th>
                        <th><?php xl('Created Date', 'e'); ?></th>
                        <th><?php xl('Response', 'e'); ?></th>
                        <th><?php xl('Status', 'e'); ?></th>
                        <th width="80px"><?php xl('Attachment', 'e'); ?></th>
                        <th><?php xl('Action', 'e'); ?></th>
                        </thead>
                        <tbody>
                            <!-- added for better print-ability -->
                            <?php
                        } // end else csv export
                        if ($pageSelection || ($_POST['form_csvexport'] && ($_POST['form_csvexport'] == 'csvfeedback' || $_POST['form_csvexport'] == 'viewfeedback'))) {
                            $message = "";
                            $range = getDateRange($_REQUEST['form_from_date'], $_REQUEST['form_to_date'], $facility);
                            $feedbackSql = "SELECT ft.ftrans_id, ft.ftrans_auto_sr_no AS srno, ft.ftrans_sr_no,
                                                DATE_FORMAT( ft.`ftrans_feedback_date`, '%d-%m-%y' ) AS feedbackDate,
                                                lo.ft_name as title, f.`name`, CONCAT_WS(' : ', ft.pubpid, ft.ftrans_pname) AS pname,
                                                ft.pubpid, ft.ftrans_pname, ft.ftrans_pemail, ft.ftrans_pcontact, IF( ft.ftrans_status != '0', 'Yes', 'No' ) AS followup,
                                                u.`nickname` AS createdBy, DATE_FORMAT( ft.`ftrans_createddate`, '%d-%m-%y' ) AS createdDate,
                                                ft.ftrans_response, ft.ftrans_color,
                                                CASE
                                                    WHEN u.`nickname` IS NULL THEN 'App'
                                                    ELSE u.`nickname`
                                                END AS exportCreatedBy,
                                                CASE
                                                    WHEN ft.ftrans_response = '1' THEN 'Promoters'
                                                    WHEN ft.ftrans_response = '2' THEN 'Passives'
                                                    ELSE 'Detractors'
                                                END AS ftrans_response_value,
                                                CASE
                                                    WHEN ft.ftrans_status = '1' THEN 'In-Progress'
                                                    WHEN ft.ftrans_status = '2' THEN 'Closed'
                                                    ELSE 'Open'
                                                END AS ftrans_status,ftrans_file_url, fdl.fd_file_name
                                         FROM `feedback_transaction` AS ft
                                         LEFT JOIN `feedback_document_logs` AS fdl ON fdl.fd_ftrans_id = ft.ftrans_id AND fdl.status = 1
                                         INNER JOIN `facility` AS f ON f.id = ft.`ftrans_fid`
                                         LEFT JOIN users AS u ON u.`id` = ft.`ftrans_createdby`
                                         INNER JOIN feedback_type as lo ON (lo.ft_id = ft.ftrans_ft_id)
                                         WHERE 1=1 ";

                            if (!empty($_REQUEST['form_facility']) && $_REQUEST['form_facility'] != '') {
                                $feedbackSql .= ' AND ft.ftrans_fid=' . $_REQUEST['form_facility'];
                                $searchParam .= "  ClinicId = " . $_REQUEST['form_facility'] . " | "; /// Auditing Section Param
                            } else {
                                $searchParam .= "Clinic = All | "; /// Auditing Section Param
                            }
                            if (!empty($_REQUEST['form_from_date']) && $_REQUEST['form_from_date'] != '' && !empty($_REQUEST['form_to_date']) && $_REQUEST['form_to_date'] != '') {
                                $feedbackSql .= " AND (DATE(ft.$formDateRes) BETWEEN '" . $_REQUEST['form_from_date'] . "' AND '" . $_REQUEST['form_to_date'] . "')";
                                $searchParam .= " From Date = " . $_REQUEST['form_from_date'] . " 00:00:00  | To Date = " . $_REQUEST['form_to_date'] . " 23:59:59 | "; /// Auditing Section Param
                            }
                            $feedbackSql .= ($form_feedback_type != 'pleaseSelect' && !empty($form_feedback_type)) ? " AND ft.ftrans_ft_id ='" . $form_feedback_type . "'" : "";
                            $searchParam .= ($form_feedback_type != 'pleaseSelect' && !empty($form_feedback_type)) ? "Feedback Type = " . $form_feedback_type . "" : "";
                            $feedbackSql .= ($form_response != 'pleaseSelect' && !empty($form_response)) ? " AND ft.ftrans_response ='" . $form_response . "'" : "";
                            $searchParam .= ($form_response != 'pleaseSelect' && !empty($form_response)) ? "Response = " . $form_response . "" : "";
                            $feedbackSql .= ($form_status != 'pleaseSelect' && !empty($form_status)) ? " AND ft.ftrans_status ='" . $form_status . "'" : "";
                            $searchParam .= ($form_status != 'pleaseSelect' && !empty($form_status)) ? "Status = " . $form_status . "" : "";
                            if($form_followup=='1') { $form_Followup_Cond = 'IN(1,2)';} else { $form_Followup_Cond = 'IN('.$form_followup.')';}
                            $feedbackSql .= ($form_followup != 'pleaseSelect' && !empty($form_followup)) ? " AND ft.ftrans_status " . $form_Followup_Cond . "" : "";
                            $searchParam .= ($form_followup != 'pleaseSelect' && !empty($form_followup)) ? "Follow-up = " . $form_followup . "" : "";
                            $feedbackSql .= ($form_creator != 'pleaseSelect' && $form_creator != '') ? " AND ft.ftrans_feedback_source ='" . $form_creator . "'" : "";
                            $searchParam .= ($form_creator != 'pleaseSelect' && !empty($form_creator)) ? "Creator = " . $form_creator . "" : "";                            
                            $feedbackSql .= (!empty($form_patient_details) && !empty($form_patient_details)) ? " AND ((ft.ftrans_pname LIKE '%" . $form_patient_details . "%') OR (ft.ftrans_pemail LIKE '%" . $form_patient_details . "%') OR (ft.ftrans_pcontact LIKE '%" . $form_patient_details . "%') OR (ft.pubpid LIKE '%" . $form_patient_details . "%') OR (ft.ftrans_pid LIKE '%" . $form_patient_details . "%'))" : "";
                            $searchParam .= (!empty($form_patient_details) && !empty($form_patient_details)) ? "Patient-Details = " . $form_patient_details . "" : "";
                            $feedbackSql .= " GROUP BY ft.ftrans_id ORDER BY ft.".$formDateRes." DESC, ft.ftrans_id DESC";
                            $daysRangeNotAllowed = $facility == 0 ? $GLOBALS['form_facility_all_time_range_valid'] : $GLOBALS['form_facility_time_range_valid'];
                            $daysRangeNotAllowedMessage = $facility == 0 ? $GLOBALS['form_facility_all_time_range_valid_errorMsg'] : $GLOBALS['form_facility_time_range_valid_errorMsg'];
                            $cronScheduleMessage = $facility == 0 ? $GLOBALS['form_facility_all_time_range_errorMsg'] : $GLOBALS['form_facility_time_range_errorMsg'];
                            if ($range['Days'] > $daysRangeNotAllowed) {
                                $message = $daysRangeNotAllowedMessage;
                            } else {
                                if ($range['Cron']) {
                                    $rcsl_name_type = "feedback_listing_report"; // changable
                                    $rcsl_requested_filter = serialize(array("fromDate" => $range["FromDate"], "toDate" => $range["ToDate"], "provider" => $provider, "facility" => getLoggedUserAssignedClinics())); // changable
                                    $rcsl_report_description = "Feedback Listing report from " . $range["FromDate"] . " to " . $range["ToDate"];
                                    //cron log email//

                                    $getFacility = getFacilityName($facility);
                                    $subject = "Feedback Listing" . "_" . $getFacility . "_" . date('Y-m-d H:i:s');
                                    //$filters = "Frome Date = " . $range["FromDate"] . " To Date = " . $range["ToDate"] . "Facility = " . $getFacility . " Doctor = " . getDoctorsName($provider) . " Transaction Type = " . getTransactionType($transactionId);
                                    $filters = "Frome Date = " . $range["FromDate"] . " To Date = " . $range["ToDate"];

                                    $report_data = json_encode(array(
                                        'header' => '#ID,Date,Type,Clinic,Patient-Id: Name,Email,Contact#,Followup Status,Created By,Created Date,Response,Status',
                                        'include' => array('srno', 'feedbackDate', 'title', 'name', 'pname', 'ftrans_pemail', 'ftrans_pcontact', 'followup', 'exportCreatedBy', 'createdDate', 'ftrans_response_value', 'ftrans_status'),
                                        'name' => 'Feedback Listing Report',
                                        'subject' => $subject,
                                        'filters' => $filters,
                                        "facility" => $getFacility,
                                        'query' => $feedbackSql
                                    ));
                                    //cron log email//
                                    $msgForReportLog = scheduleReports("feedback_listing_report", $rcsl_requested_filter, $rcsl_report_description, $report_data);
                                    $message = empty($msgForReportLog) ? $cronScheduleMessage : $msgForReportLog;
                                } else {
                                    $message = "";
                                }
                            }
                            if ($message) {
                                $auditid = debugADOReports($feedbackSql, '', 'Report Feedback', $GLOBALS['enable_auditlog'], $GLOBALS['_SESSION']['authUser'], 0, $searchParam); // Report Auditing Section                    
                            } else {
                                
                                    if($_POST['form_csvexport'] != 'csvfeedback'){
                                        $num_rows = sqlNumRows(sqlStatement($feedbackSql)); // total no. of rows
                                        $per_page = $GLOBALS['encounter_page_size'];   // Pagination variables processing
                                        $page = $pageSelection;
                                        if (!$_REQUEST["page"]) { $page = 1; }
                                        $prev_page = $page - 1;
                                        $next_page = $page + 1;
                                        $page_start = (($per_page * $page) - $per_page);
                                        if ($num_rows <= $per_page) {
                                            $num_pages = 1;
                                        } else if (($num_rows % $per_page) == 0) {
                                            $num_pages = ($num_rows / $per_page);
                                        } else {
                                            $num_pages = ($num_rows / $per_page) + 1;
                                            $num_pages = (int) $num_pages;
                                        }
                                        $feedbackSql .= " LIMIT $page_start , $per_page";
                                }
                                $feedbackResult = sqlStatement($feedbackSql);
                                $srCount = 0;
                                if (sqlNumRows($feedbackResult) > 0) {
                                    while ($feedbackRow = sqlFetchArray($feedbackResult)) {
                                        $feedbackRow['ftrans_status'] == "Open" ? $ftransFollowup = "No" : $ftransFollowup = "Yes";
                                        if ($_POST['form_csvexport'] && $_POST['form_csvexport'] == 'csvfeedback') {
                                            $ftrans_pemail = !empty(qescape($feedbackRow['ftrans_pemail'])) ? qescape($feedbackRow['ftrans_pemail']) : 'NA';
                                            $createdBy = !empty(qescape($feedbackRow['createdBy'])) ? qescape($feedbackRow['createdBy']) : 'App';
                                            $ftrans_color = qescape($feedbackRow['ftrans_response']);
                                            switch ($ftrans_color)
                                            { 
                                                case 1:
                                                    $ftrans_color = "Promoters";
                                                    break;
                                                case 2:
                                                    $ftrans_color = "Passives";
                                                    break;
                                                default:
                                                    $ftrans_color = "Detractors";
                                                    break;
                                            }
                                            echo '"' . qescape($feedbackRow['srno']). '",';
                                            echo '"' . qescape($feedbackRow['ftrans_sr_no']). '",';
                                            echo '"' . qescape($feedbackRow['feedbackDate']) . '",';
                                            echo '"' . qescape($feedbackRow['title']) . '",';
                                            echo '"' . qescape($feedbackRow['name']) . '",';
                                            echo '"' . qescape($feedbackRow['pubpid']) . '",';
                                            echo '"' . qescape($feedbackRow['ftrans_pname']) . '",';
                                            echo '"' . $ftrans_pemail. '",';
                                            echo '"' . qescape($feedbackRow['ftrans_pcontact']) . '",';
                                            echo '"' . qescape($ftransFollowup) . '",';
                                            echo '"' . $createdBy . '",'; 
                                            echo '"' . qescape($feedbackRow['createdDate']). '",';
                                            echo '"' . $ftrans_color . '",';
                                            echo '"' . qescape($feedbackRow['ftrans_status']) . '"';
                                            echo "\n";
                                        } else {
                                            ?>
                                            <tr bgcolor='<?php echo $bgcolor ?>'>
                                                <td class="detail"><?php echo !empty($feedbackRow['srno']) ? $feedbackRow['srno'] : 'NA'; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['feedbackDate']) ? $feedbackRow['feedbackDate']: 'NA'; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['title'])? $feedbackRow['title'] : 'NA'; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['name'])? $feedbackRow['name'] : 'NA'; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['pname'])? $feedbackRow['pname'] : 'NA'; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['ftrans_pemail'])? $feedbackRow['ftrans_pemail'] : 'NA'; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['ftrans_pcontact'])? $feedbackRow['ftrans_pcontact'] : 'NA'; ?></td>
                                                <td class="detail"><?php echo $ftransFollowup; ?></td>
                                                <td class="detail"><?php echo !empty($feedbackRow['createdBy'])? $feedbackRow['createdBy'] : 'App'; ?></td>
                                                <td class="detail"><?php echo $feedbackRow['createdDate']; ?></td>
                                                <td><div style="background-color: <?php echo $feedbackRow['ftrans_color']; ?>;border: 0px solid;border-radius: 10px; width: 40px; height: 20px;"></div></td>
                                                <td class="detail"><?php echo $feedbackRow['ftrans_status']; ?></td>
                                                <td class="detail wordbreak"><?php if (!empty($feedbackRow['ftrans_file_url'])) { ?><a href="<?php echo $feedbackRow['ftrans_file_url']; ?>" target="_blank"><?php echo $feedbackRow['fd_file_name']?></a> <?php } else{ echo 'NA'; }?></td>
                                                <td class="detail actions">&nbsp;
                                                    <?php if (acl_check('Feedback', 'fdbk_view')) { ?>
                                                        <a href='feedback_detail.php?id=<?php echo $feedbackRow['ftrans_id']; ?>' class='iframe_medium' onclick='top.restoreSession()' title='View'><span class="glyphicon glyphicon-eye-open"></span></a>
                                                    <?php } ?> 
                                                    <?php if ($feedbackRow['ftrans_status'] != 2 && acl_check('Feedback', 'fdbk_followup_add_view')) { ?>
                                                        &nbsp;<a href='feedback_followup.php?id=<?php echo $feedbackRow['ftrans_id']; ?>' class='iframe_medium' onclick='top.restoreSession()' title='Follow Up'><span class="glyphicon glyphicon-tasks"></span></a>
                                                    <?php } ?>
                                                    <a href='feedback_add.php?id=<?php echo $feedbackRow['ftrans_id']; ?>&page=<?php echo $page;?>' title="Edit"><span class="glyphicon glyphicon glyphicon-pencil"></span> </a></td>
                                            </tr>

                                            <?php
                                        }
                                        $srCount++;
                                    }
                                }
                                if($_POST['form_csvexport'] != 'csvfeedback' && $num_rows > 0){
                                ?>
                                            <tr>
                                            <td colspan="14">
                                                <?php if ($num_rows > 0) { ?>
                                                    <span class='paging'>
                                                        <span class="pagingInfo">Total Records: <?php echo $num_rows; ?></span> Page :
                                                        <?php
                                                        if ($prev_page) {
                                                            echo " <span class='prev' style='margin:0 4px 0 5px'><a href='$_SERVER[SCRIPT_NAME]?$requestParams&page=$prev_page'><< Prev</a></span> ";
                                                        }

                                                        if ($num_rows > 0) {
                                                            for ($i = 1; $i <= $num_pages; $i++) {
                                                                if ($i != $page) {
                                                                    echo "<span><a href='$_SERVER[SCRIPT_NAME]?$requestParams&page=$i'>$i</a></span>";
                                                                } else {
                                                                    echo "<span class='current'>$i</span>";
                                                                }
                                                            }
                                                        }
                                                        if ($page != $num_pages) {
                                                            echo "<span class='next'><a href ='$_SERVER[SCRIPT_NAME]?$requestParams&page=$next_page'>Next >></a></span>";
                                                        }
                                                        ?>
                                                    </span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                            
                                <?php          
                            }
                                debugADOReportsUpdate($auditid, $GLOBALS['enable_auditlog']);
                            }

                            if (($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvfeedback') || $_GET['page']) {
                                if (empty($message) && $srCount == 0) {
                                    ?>
                                    <tr bgcolor='<?php echo $bgcolor ?>'>
                                        <td class="detail" colspan='14' style="text-align:center;">&nbsp;<span style="color:red;">No Feedback Found</span></td>
                                    </tr>
                                    <?php
                                }
                            }
                            if(!empty($message)){ 
                                ?>
                                <tr bgcolor='<?php echo $bgcolor ?>'>
                                    <td class="detail" colspan='14' style="text-align:center;">&nbsp;<span style="color:red;"><?php echo $message; ?></span></td>
                                </tr> 
                                <?php
                            }
                            ?>
                            <?php
                           if ($_POST['form_csvexport'] && $_POST['form_csvexport'] != 'csvfeedback') {
                                ?>                                  
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="form_orderby" value="<?php echo $form_orderby ?>"/> 
                    <input type="hidden" name="patient" value="<?php echo $patient ?>"/> 
                    <input type='hidden' name='form_refresh' id='form_refresh' value='' />   
                    <?php }
}
        ?>
        
<?php if ($_POST['form_csvexport'] != 'csvfeedback') { ?>




    </body>
    <script type="text/javascript">
        <?php
        if ($alertmsg) {
            echo " alert('$alertmsg');\n";
        }
        ?>
        Calendar.setup({
            inputField: "form_from_date"
            , ifFormat: "%Y-%m-%d"
            , button: "img_from_date"
            , onUpdate: function () {
                var toDate = document.getElementById('form_to_date').value,
                        fromDate = document.getElementById('form_from_date').value;

                if (toDate) {
                    var toDate = new Date(toDate),
                            fromDate = new Date(fromDate);

                    if (fromDate > toDate) {
                        jAlert('From date cannot be later than To date.');
                        document.getElementById('form_from_date').value = '';
                    }
                }
            }
        });

        Calendar.setup({
            inputField: "form_to_date"
            , ifFormat: "%Y-%m-%d"
            , button: "img_to_date"
            , onUpdate: function () {
                var toDate = document.getElementById('form_to_date').value,
                        fromDate = document.getElementById('form_from_date').value;

                if (fromDate) {
                    var toDate = new Date(toDate),
                            fromDate = new Date(fromDate);

                    if (fromDate > toDate) {
                        jAlert('From date cannot be later than To date.');
                        document.getElementById('form_to_date').value = '';
                    }
                }
            }
        });
    </script>
    </html>

    <?php
    }?>